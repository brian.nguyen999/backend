﻿using Kempus.UserService.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Kempus.UserService
{
    public class ApplicationDbContext : IdentityDbContext<UserEntity, RoleEntity, long>
    {
        private readonly IConfiguration _configuration;

        public ApplicationDbContext(
            DbContextOptions options,
            IConfiguration configuration
            ) : base(options)
        {
            _configuration = configuration;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}

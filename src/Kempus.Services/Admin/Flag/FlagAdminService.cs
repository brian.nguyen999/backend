﻿
using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Admin.Flag;
using Kempus.Models.Public.School;
using Kempus.Services.Common.KempTransaction;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;
using static Kempus.Core.Constants.CoreConstants.OpenSearchEventType;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.Admin.Flag
{
  internal class FlagAdminService : IFlagAdminService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly IKempTransactionCommonService _kempTransactionService;

    public FlagAdminService(
      MasterDbContext masterDbContext,
      ReplicaDbContext replicaDbContext,
      IKempTransactionCommonService kempTransactionService
    )
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
      _kempTransactionService = kempTransactionService;
    }

    public PagingResult<FlagListViewModel> Search(FlagFilterModel filterModel, Pageable pageable, bool useMasterDb)
    {
      var query = BuildQueryable(filterModel, useMasterDb);

      var totalCount = query.LongCount();
      if (totalCount < 1)
        return new PagingResult<FlagListViewModel>(0, pageable.PageIndex, pageable.PageSize);

      var orderBy = QueryableUtils.GetOrderByFunction<FlagEntity>(pageable.GetSortables());

      var data = orderBy(query).Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new FlagListViewModel(x)).ToList();

      BuildResultData(data, useMasterDb);

      return new PagingResult<FlagListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    public long GetTotalFlagPending()
    {
      return _replicaDbContext.Flags.LongCount(x => x.Status == (byte)FlagStatus.Pending);
    }

    public bool Update(FlagCmsInputModel input)
    {
      var currentFlag = _replicaDbContext.Flags.FirstOrDefault(x => x.Guid == input.Id);
      if (currentFlag == null) throw new BadRequestException(ErrorMessages.Flag_NotFound);
      if (currentFlag.Status == (byte)FlagStatus.Actioned) throw new BadRequestException(ErrorMessages.Flag_ActionedFlag);

      currentFlag.Status = input.Status;
      currentFlag.UpdatedBy = input.UpdatedBy;
      currentFlag.UpdatedDate = DateTime.UtcNow;

      _masterDbContext.Flags.Update(currentFlag);
      var result = _masterDbContext.SaveChanges() > 0;
      if (result)
      {
        // Create SuccessfullyFlagReport transaction. Add 3 Kemp
        _kempTransactionService.CreateTransaction(new Models.Common.KempTransactionInputModel()
        {
          UserIds = new List<Guid>() { currentFlag.CreatedBy },
          TransactionType = (byte)KempTransactionType.SuccessfullyFlagReport,
          ReferenceGuid = currentFlag.CreatedBy,
          Amount = KempTransactionValue.SuccessfullyFlagReport,
          Action = (byte)KempTransactionAction.Add,
          CreatorId = input.UpdatedBy,
          SchoolId = currentFlag.SchoolId
        });

        // Create FlaggedByOtherUser transaction. Deduct 3 Kemp
        _kempTransactionService.CreateTransaction(new Models.Common.KempTransactionInputModel()
        {
          UserIds = new List<Guid>() { currentFlag.FlaggedUser },
          TransactionType = (byte)KempTransactionType.FlaggedByOtherUser,
          ReferenceGuid = currentFlag.Guid,
          Amount = KempTransactionValue.FlaggedByOtherUser,
          Action = (byte)KempTransactionAction.Deduct,
          CreatorId = input.UpdatedBy,
          SchoolId = currentFlag.SchoolId
        });
      }

      return result;
    }

    #region Private Methods
    private IQueryable<FlagEntity> BuildQueryable(FlagFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.Flags.AsNoTracking();
      query = FlagQueryBuilder<FlagEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    private void BuildResultData(List<FlagListViewModel> data, bool useMasterDb)
    {
      SetContent(data, useMasterDb);
      SetSchool(data, useMasterDb);
      SetFlaggedStudent(data, useMasterDb);
      SetFlaggedBy(data, useMasterDb);
    }

    private void SetContent(List<FlagListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var flagIds = data.Select(x => x.Id).ToList();
      var mapUserData = dbContext.Flags.Where(x => flagIds.Contains(x.Guid) && x.ObjectType == (byte)FlagType.User)
        .Join(dbContext.Users,
          flag => flag.ObjectId,
          user => user.UserId,
          (flag, user) => new { FlagId = flag.Guid, User = user })
        .ToDictionary(x => x.FlagId, x => x.User.UserName);

      var mapReviewData = dbContext.Flags.Where(x => flagIds.Contains(x.Guid) && x.ObjectType == (byte)FlagType.Review)
        .Join(dbContext.CourseReviews,
          flag => flag.ObjectId,
          review => review.Guid,
          (flag, review) => new { Flag = flag, CourseReview = review })
        .Join(dbContext.Courses,
          flagReview => flagReview.CourseReview.CourseId,
          course => course.Guid,
          (flagReview, course) => new { FlagReview = flagReview, Course = course })
        .Select(x => new { FlagId = x.FlagReview.Flag.Guid, CourseName = x.Course.Name })
        .ToDictionary(x => x.FlagId, x => x.CourseName);

      var mapPostPollData = dbContext.Flags.Where(x => flagIds.Contains(x.Guid) && (x.ObjectType == (byte)FlagType.Post || x.ObjectType == (byte)FlagType.Poll))
        .Join(dbContext.Posts,
          flag => flag.ObjectId,
          post => post.Guid,
          (flag, post) => new { FlagId = flag.Guid, Post = post })
        .ToDictionary(x => x.FlagId, x => x.Post.Title);

      var mapCommentData = dbContext.Flags.Where(x => flagIds.Contains(x.Guid) && x.ObjectType == (byte)FlagType.Comment)
        .Join(dbContext.PostComments,
          flag => flag.ObjectId,
          comment => comment.Guid,
          (flag, comment) => new { FlagId = flag.Guid, Content = comment.Content })
        .ToDictionary(x => x.FlagId, x => x.Content);

      data.ForEach(x =>
      {
        switch (x.ObjectType)
        {
          case (byte)FlagType.User:
            x.Content = mapUserData.GetValueOrDefault(x.Id);
            break;
          case (byte)FlagType.Review:
            x.Content = mapReviewData.GetValueOrDefault(x.Id);
            break;
          case (byte)FlagType.Post:
          case (byte)FlagType.Poll:
            x.Content = mapPostPollData.GetValueOrDefault(x.Id);
            break;
          case (byte)FlagType.Comment:
            x.Content = mapCommentData.GetValueOrDefault(x.Id);
            break;
        }
      });
    }
    private void SetSchool(List<FlagListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var flagIds = data.Select(x => x.Id).ToList();
      var mapData = dbContext.Flags.Where(x => flagIds.Contains(x.Guid))
        .Join(dbContext.Schools,
        flag => flag.SchoolId,
        school => school.Guid,
        (flag, school) => new { Flag = flag, School = school })
        .ToDictionary(x => x.Flag.Guid, x => x.School.Name);

      data.ForEach(x =>
      {
        x.School = mapData.GetValueOrDefault(x.Id);
      });
    }
    private void SetFlaggedStudent(List<FlagListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var flagIds = data.Select(x => x.Id).ToList();
      var mapData = dbContext.Flags.Where(x => flagIds.Contains(x.Guid))
        .Join(dbContext.Users,
        flag => flag.FlaggedUser,
        user => user.UserId,
        (flag, user) => new { Flag = flag, User = user })
        .ToDictionary(x => x.Flag.Guid, x => x.User.UserName);

      data.ForEach(x =>
      {
        x.FlaggedStudent = mapData.GetValueOrDefault(x.Id);
      });
    }
    private void SetFlaggedBy(List<FlagListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var flagIds = data.Select(x => x.Id).ToList();
      var mapData = dbContext.Flags.Where(x => flagIds.Contains(x.Guid))
        .Join(dbContext.Users,
        flag => flag.CreatedBy,
        user => user.UserId,
        (flag, user) => new { Flag = flag, User = user })
        .ToDictionary(x => x.Flag.Guid, x => x.User.UserName);

      data.ForEach(x =>
      {
        x.FlaggedBy = mapData.GetValueOrDefault(x.Id);
      });
    }
    #endregion
  }
}

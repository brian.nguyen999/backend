﻿using Kempus.Core.Models;
using Kempus.Models.Admin.Flag;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Admin.Flag
{
  public interface IFlagAdminService
  {
    PagingResult<FlagListViewModel> Search(FlagFilterModel filterModel, Pageable pageable, bool useMasterDb);
    long GetTotalFlagPending();
    bool Update(FlagCmsInputModel input);
  }
}

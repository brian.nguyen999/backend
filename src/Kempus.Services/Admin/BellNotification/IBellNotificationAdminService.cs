﻿using Kempus.Core.Models;
using Kempus.Models.Admin.BellNotification;
using Kempus.Models.Public.BellNotification;

namespace Kempus.Services.Admin.BellNotification
{
  public interface IBellNotificationAdminService
  {
    PagingResult<BellNotificationListViewCmsModel> Search(BellNotificationFilterCmsModel filterModel, Pageable pageable, bool useMasterDb);
    BellNotificationDetailCmsModel GetDetail(Guid id);
    Guid Create(BellNotificationInputModel model);
    void Update(BellNotificationInputModel model);
    void SendScheduledNotification();
  }
}

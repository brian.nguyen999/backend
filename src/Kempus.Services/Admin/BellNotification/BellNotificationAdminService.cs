﻿using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Admin.BellNotification;
using Kempus.Models.Public.BellNotification;
using Kempus.Services.Public.BellNotification;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;
using static Kempus.Core.Constants.CoreConstants;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.Admin.BellNotification
{
  public class BellNotificationAdminService : IBellNotificationAdminService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly IBellNotificationService _bellNotificationService;
    public BellNotificationAdminService(
      MasterDbContext masterDbContext,
      ReplicaDbContext replicaDbContext,
      IBellNotificationService bellNotificationService)
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
      _bellNotificationService = bellNotificationService;
    }

    public PagingResult<BellNotificationListViewCmsModel> Search(BellNotificationFilterCmsModel filterModel, Pageable pageable, bool useMasterDb)
    {
      var query = BuildQueryable(filterModel, useMasterDb);

      var totalCount = query.LongCount();
      if (totalCount < 1)
      {
        return new PagingResult<BellNotificationListViewCmsModel>(0, pageable.PageIndex, pageable.PageSize);
      }

      var orderBy = QueryableUtils.GetOrderByFunction<BellNotificationEntity>(pageable.GetSortables(nameof(BellNotificationEntity.CreatedDate)));

      var data = orderBy(query).Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new BellNotificationListViewCmsModel(x)).ToList();

      BuildResultData(data, useMasterDb);

      return new PagingResult<BellNotificationListViewCmsModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    public Guid Create(BellNotificationInputModel model)
    {
      ValidateBeforeCreateOrUpdate(model);

      using var transaction = _masterDbContext.Database.BeginTransaction();
      try
      {
        model.CreatedDate = DateTime.UtcNow;
        model.UserAction = UserAction.Create;
        model.BellNotificationType = (byte)BellNotificationType.CMS;

        var result = _bellNotificationService.SaveNotifyObjectChange(model);
        transaction.Commit();

        if (model.BellNotificationStatus == (byte)BellNotificationStatus.Sent)
        {
          // TODO: implement send SignalR for users later
          _bellNotificationService.SendBellAndPushMobileNotificationByGroupSchool(model.SchoolIds);
        }

        return result.Id;
      }
      catch
      {
        transaction.Rollback();
        throw;
      }
    }

    public void Update(BellNotificationInputModel model)
    {
      ValidateBeforeCreateOrUpdate(model);

      using var transaction = _masterDbContext.Database.BeginTransaction();
      try
      {
        model.UpdatedDate = DateTime.UtcNow;
        model.UserAction = UserAction.Update;
        model.BellNotificationType = (byte)BellNotificationType.CMS;

        var result = _bellNotificationService.SaveNotifyObjectChange(model);
        transaction.Commit();

        if (model.BellNotificationStatus == (byte)BellNotificationStatus.Sent)
        {
          // TODO: need apply realtime
          _bellNotificationService.SendBellAndPushMobileNotificationByGroupSchool(model.SchoolIds);
        }
      }
      catch
      {
        transaction.Rollback();
        throw;
      }

    }

    public BellNotificationDetailCmsModel GetDetail(Guid id)
    {
      var query = BuildQueryable(new BellNotificationFilterCmsModel { Ids = new List<Guid> { id } }, false);
      var bellNotification = query.Where(x => x.Status == (byte)BellNotificationStatus.Scheduled).FirstOrDefault();

      if (bellNotification == null)
        throw new NotFoundException(ErrorMessages.BellNotification_NotFound);

      var model = new BellNotificationDetailCmsModel
      {
        Id = bellNotification.Guid,
        Title = bellNotification.Title,
        UrlLink = bellNotification.UrlLink,
        BellNotificationStatus = bellNotification.Status,
        SchoolIds = JsonUtils.Convert<List<Guid>>(bellNotification.SchoolIds),
        ScheduledDate = bellNotification.ScheduledDate
      };
      return model;
    }

    public void SendScheduledNotification()
    {
      var notificationUserIdsModel = _replicaDbContext.BellNotifications.AsNoTracking().Where(x => x.Status == (byte)BellNotificationStatus.Scheduled && x.ScheduledDate != null)
        .Join(_replicaDbContext.BellNotificationUsers,
        bellNotification => bellNotification.Guid,
        bellNotificationUser => bellNotificationUser.BellNotificationId,
        (bellNotification, bellNotificationUser) => new
        {
          bellNotification.ScheduledDate,
          bellNotification.Guid,
          bellNotificationUser.CreatedUser
        }).ToList().Where(x => (int)(DateTime.UtcNow.Subtract(x.ScheduledDate.Value).TotalMinutes) == 0).ToLookup(x => x.Guid, x => x.CreatedUser);

      // Send realtime notification to users
      var sentBellNotificationIds = new List<Guid>();
      foreach (IGrouping<Guid, Guid> model in notificationUserIdsModel)
      {
        sentBellNotificationIds.Add(model.Key);
      }

      // Update status to sent for bellNotifications
      var sentBellNotifications = _masterDbContext.BellNotifications.Where(x => sentBellNotificationIds.Contains(x.Guid)).ToList();
      sentBellNotifications.ForEach(x => { x.Status = (byte)BellNotificationStatus.Sent; });
      _masterDbContext.UpdateRange(sentBellNotifications);
      _masterDbContext.SaveChanges();

      // TODO REFACTOR Logic send noti realtime then save noti to DB
      // => Change to Create Noti in DB before send noti realtime
      if (sentBellNotifications.Any())
      {
        foreach (var notification in sentBellNotifications)
        {
          if (!string.IsNullOrEmpty(notification.SchoolIds))
          {
            _bellNotificationService.SendBellAndPushMobileNotificationByGroupUser(JsonUtils.Convert<List<Guid>>(notification.SchoolIds));
          }
        }
      }
    }

    private void BuildResultData(List<BellNotificationListViewCmsModel> data, bool useMasterDb)
    {
      SetSchool(data, useMasterDb);
    }

    private void SetSchool(List<BellNotificationListViewCmsModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;

      List<Guid> schoolIdsList = new();
      foreach (var item in data)
      {
        if (!string.IsNullOrEmpty(item.SchoolIds))
          schoolIdsList.AddRange(JsonUtils.Convert<List<Guid>>(item.SchoolIds));
      }
      schoolIdsList = schoolIdsList.Distinct().ToList();

      var schools = dbContext.Schools.AsNoTracking().Where(x => schoolIdsList.Contains(x.Guid)).ToList();

      data.ForEach(x =>
      {
        if (x.SchoolIds == null) return;

        var schoolNames = schools.Where(y => x.SchoolIds.Contains(y.Guid.ToString())).Select(x => x.Name).ToList();
        if (schoolNames.Any())
          x.SchoolNames = schoolNames;
      });
    }

    private IQueryable<BellNotificationEntity> BuildQueryable(BellNotificationFilterCmsModel filterModel, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.BellNotifications.AsNoTracking().Where(x => x.Type == (byte)BellNotificationType.CMS);
      query = BellNotificationQueryBuilder<BellNotificationEntity>.BuildWhereClause(filterModel, query, dbContext);
      return query;
    }

    private void ValidateBeforeCreateOrUpdate(BellNotificationInputModel model)
    {
      if (model.Id != null)
      {
        var bellNotification = _replicaDbContext.BellNotifications.FirstOrDefault(x => x.Guid == model.Id && x.Status == (byte)BellNotificationStatus.Scheduled);
        if (bellNotification == null)
          throw new NotFoundException(ErrorMessages.BellNotification_NotFound);
      }

      if (model.BellNotificationStatus == (byte)BellNotificationStatus.Scheduled && model.ScheduledDate == null)
        throw new BadRequestException(ErrorMessages.BellNotification_ScheduledDate_CannotNull);

      if (model.ScheduledDate.HasValue && model.ScheduledDate < DateTime.UtcNow)
        throw new BadRequestException(ErrorMessages.BellNotification_ScheduledDate_ShouldBeLater_CurrentDate);
    }
  }
}

﻿using Kempus.Core.Errors;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Admin.Instructor;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.Admin.Instructor
{
  public class InstructorAdminService : IInstructorAdminService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;

    public InstructorAdminService(
      MasterDbContext masterDbContext,
      ReplicaDbContext replicaDbContext)
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
    }

    public Guid Create(InstructorInputCmsModel model)
    {
      ValidateBeforeCreateOrUpdate(model);

      var entity = new InstructorEntity
      {
        Name = model.Name,
        IsActivated = true,
        SchoolId = model.SchoolId.GetValueOrDefault(),
        GeneratedTypeId = (byte)GeneratedType.ManualInCMS,
        CreatedBy = model.CreatorId.GetValueOrDefault()
      };

      _masterDbContext.Instructors.Add(entity);
      _masterDbContext.SaveChanges();

      return entity.Guid;
    }

    public void Update(InstructorInputCmsModel model)
    {
      ValidateBeforeCreateOrUpdate(model);

      var instructor = _masterDbContext.Instructors.FirstOrDefault(x => x.Guid == model.Id);
      instructor.Name = model.Name;
      instructor.UpdatedBy = model.UpdaterId.GetValueOrDefault();
      instructor.UpdatedDate = DateTime.UtcNow;

      _masterDbContext.Instructors.Update(instructor);
      _masterDbContext.SaveChanges();
    }

    private void ValidateBeforeCreateOrUpdate(InstructorInputCmsModel model)
    {
      if (model.Id != null)
      {
        var instructor = _replicaDbContext.Instructors.FirstOrDefault(x => x.Guid == model.Id);
        if (instructor == null)
          throw new NotFoundException(ErrorMessages.Instructor_NotFound);
      }

      if (model.SchoolId != null)
      {
        var school = _replicaDbContext.Schools.FirstOrDefault(x => x.Guid == model.SchoolId);
        if (school == null)
          throw new NotFoundException(ErrorMessages.School_NotFound);
      }

      if (model.CreatorId != null)
      {
        var user = _replicaDbContext.Users.FirstOrDefault(x => x.UserId == model.CreatorId);
        if (user == null) throw new NotFoundException(ErrorMessages.User_NotFound);
      }

      if (model.UpdaterId != null)
      {
        var user = _replicaDbContext.Users.FirstOrDefault(x => x.UserId == model.UpdaterId);
        if (user == null) throw new NotFoundException(ErrorMessages.User_NotFound);
      }
    }
  }
}

﻿using Kempus.Models.Admin.Instructor;

namespace Kempus.Services.Admin.Instructor
{
  public interface IInstructorAdminService
  {
    Guid Create(InstructorInputCmsModel model);
    void Update(InstructorInputCmsModel model);
  }
}

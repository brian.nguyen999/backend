﻿

using Kempus.Models.Admin.PollOption;

namespace Kempus.Services.Admin.PollOption
{
  public interface IPollOptionAdminService
  {
    void Create(PollOptionInputModel input);
    void Update(PollOptionUpdateInputModel model);
  }
}

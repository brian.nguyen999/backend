﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Admin.Major;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;

namespace Kempus.Services.Admin.Major
{
  public class MajorAdminService : IMajorAdminService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;

    public MajorAdminService(
      MasterDbContext masterDbContext,
      ReplicaDbContext replicaDbContext
      )
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
    }


    public PagingResult<MajorListViewModel> Search(MajorFilterModel model, Pageable pageable, bool useMasterDb)
    {
      var query = BuildQueryable(model, useMasterDb);

      var totalCount = query.LongCount();
      if (totalCount < 1)
      {
        return new PagingResult<MajorListViewModel>(0, pageable.PageIndex, pageable.PageSize);
      }

      var orderBy =
        QueryableUtils.GetOrderByFunction<MajorEntity>(pageable.GetSortables(nameof(MajorEntity.CreatedDate)));

      var data = orderBy(query).Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new MajorListViewModel(x)).ToList();

      return new PagingResult<MajorListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    public MajorDetailModel GetDetail(Guid id)
    {
      var major = _replicaDbContext.Majors.FirstOrDefault(x => x.Guid == id);
      if (major == null)
        throw new NotFoundException(ErrorMessages.Major_NotFound);
      return new MajorDetailModel
      {
        Id = major.Id,
        Guid = major.Guid,
        Name = major.Name,
        IsActivated = major.IsActivated,
        SchoolId = major.SchoolId
      };
    }

    public Guid Create(MajorInputModel model)
    {
      ValidateBeforeCreate(model);

      var entity = new MajorEntity
      {
        CreatedBy = model.CreatedBy.GetValueOrDefault(),
        Name = model.Name,
        IsActivated = model.IsActivated,
        CreatedDate = DateTime.UtcNow
      };

      _masterDbContext.Majors.Add(entity);
      _masterDbContext.SaveChanges();

      return entity.Guid;
    }

    public void Update(MajorUpdateInputModel model)
    {
      var major = _replicaDbContext.Majors.FirstOrDefault(x => x.Guid == model.Id);
      if (major == null)
        throw new NotFoundException(ErrorMessages.Major_NotFound);

      if (model.Name != null)
      {
        var majorName = _replicaDbContext.Majors.FirstOrDefault(x => x.Name == model.Name);
        if (majorName != null)
          throw new BadRequestException(ErrorMessages.Major_NameExisted);
        major.Name = model.Name;
      }

      if (model.IsActivated != null)
      {
        major.IsActivated = model.IsActivated.Value;
      }

      major.UpdatedDate = DateTime.UtcNow;
      major.UpdatedBy = model.UpdatedBy.GetValueOrDefault();

      _masterDbContext.Majors.Update(major);
      _masterDbContext.SaveChanges();
    }

    #region Private methods

    public IQueryable<MajorEntity> BuildQueryable(MajorFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.Majors.AsNoTracking();
      query = MajorQueryBuilder<MajorEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    private void ValidateBeforeCreate(MajorInputModel model)
    {
      var majorName = _replicaDbContext.Majors.FirstOrDefault(x => x.Name == model.Name);
      if (majorName != null)
        throw new BadRequestException(ErrorMessages.Major_NameExisted);
    }

    #endregion
  }
}

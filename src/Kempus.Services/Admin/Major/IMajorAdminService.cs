﻿using Kempus.Core.Models;
using Kempus.Models.Admin.Major;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Admin.Major
{
  public interface IMajorAdminService
  {
    PagingResult<MajorListViewModel> Search(MajorFilterModel model, Pageable pageable, bool useMasterDb);
    Guid Create(MajorInputModel model);
    void Update(MajorUpdateInputModel model);
    MajorDetailModel GetDetail(Guid id);
  }
}

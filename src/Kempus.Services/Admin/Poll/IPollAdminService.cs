﻿using Kempus.Core.Models;
using Kempus.Models.Admin.Poll;
using Kempus.Models.Admin.Post;

namespace Kempus.Services.Admin.Poll
{
  public interface IPollAdminService
  {
    PagingResult<PollListViewModel> Search(PollFilterModel filter, Pageable pageable, bool useMasterDb,
      Guid? currentUserId = null);

    PollDetailModel GetDetail(Guid id, Guid? currentUserId = null);

    Guid Create(PollInputModel model);

    void Update(PollUpdateInputModel model);
  }
}

﻿using Kempus.Core.Constants;
using Kempus.Core.Enums;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Admin.Poll;
using Kempus.Models.Admin.PollOption;
using Kempus.Models.Admin.Post;
using Kempus.Services.Admin.PollOption;
using Kempus.Services.Admin.Post;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using static Kempus.Core.Enums.CoreEnums;
using PollFilterModel = Kempus.Models.Admin.Poll.PollFilterModel;
using PollInputModel = Kempus.Models.Admin.Poll.PollInputModel;
using PollListViewModel = Kempus.Models.Admin.Poll.PollListViewModel;

namespace Kempus.Services.Admin.Poll
{
  public class PollAdminService : IPollAdminService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly IConfiguration _configuration;
    private readonly IPostAdminService _postAdminService;
    private readonly IPollOptionAdminService _pollOptionAdminService;

    public PollAdminService(
      MasterDbContext masterDbContext,
      ReplicaDbContext replicaDbContext,
      IConfiguration configuration,
      IPostAdminService postAdminService,
      IPollOptionAdminService pollOptionAdminService
      )
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
      _configuration = configuration;
      _postAdminService = postAdminService;
      _pollOptionAdminService = pollOptionAdminService;
    }

    public PagingResult<PollListViewModel> Search(PollFilterModel filter, Pageable pageable, bool useMasterDb,
      Guid? currentUserId = null)
    {
      var query = BuildQueryable(filter, useMasterDb);

      var totalCount = query.LongCount();
      if (totalCount < 1)
        return new PagingResult<PollListViewModel>(0, pageable.PageIndex, pageable.PageSize);

      var orderBy =
        QueryableUtils.GetOrderByFunction<PostEntity>(pageable.GetSortables(nameof(PostEntity.CreatedDate)));

      var data = orderBy(query).Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new PollListViewModel(x)).ToList();

      BuildResultData(data, currentUserId, useMasterDb);

      return new PagingResult<PollListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    public PollDetailModel GetDetail(Guid id, Guid? currentUserId = null)
    {
      var post = _masterDbContext.Posts
        .FirstOrDefault(x => x.Guid == id && x.Type == (byte)PostType.Poll);
      if (post == null)
      {
        throw new NotFoundException(ErrorMessages.Poll_NotFound);
      }

      var poll = _replicaDbContext.Polls.FirstOrDefault(x => x.PostId == post.Guid);
      if (poll == null)
      {
        throw new NotFoundException(ErrorMessages.Poll_NotFound);
      }

      var pollOptions = _replicaDbContext.PollOptions.Where(x => x.PollId == poll.Guid).ToList();

      return new PollDetailModel
      {
        Id = post.Id,
        Guid = post.Guid,
        Title = post.Title,
        Keywords = post.Keywords != null
          ? JsonConvert.DeserializeObject<List<string>>(post.Keywords)
          : null,
        Content = post.Content,
        Type = Enum.GetName(typeof(PollType), poll.Type),
        IsPublished = post.IsPublished,
        Options = pollOptions.Select(x => new PollOptionDetailModel
        {
          Id = x.Guid,
          Content = x.Content
        }).ToList(),
        IsPinned = post.IsPinned
      };
    }

    public Guid Create(PollInputModel model)
    {

      ValidateBeforeCreate(model);
      var postId = _postAdminService.Create(new PostInputModel
      {
        Title = model.Title,
        Content = model.Content,
        Keywords = model.Keywords,
        Type = (byte)CoreEnums.PostType.Poll,
        IsPinned = model.IsPinned,
        IsPublished = model.IsPublished
      });
      _masterDbContext.SaveChanges();
      var poll = new PollEntity()
      {
        Type = (int)Enum.Parse<CoreEnums.PollType>(model.Type),
        PostId = postId,
      };

      _masterDbContext.Polls.Add(poll);
      _masterDbContext.SaveChanges();
      _pollOptionAdminService.Create(new PollOptionInputModel()
      {
        PollId = poll.Guid,
        Options = model.Options
      });

      _masterDbContext.SaveChanges();
      return postId;
    }

    public void Update(PollUpdateInputModel model)
    {
      ValidateBeforeUpdate(model);

      // Update post
      _postAdminService.Update(new PostInputModel
      {
        Id = model.Id,
        Title = model.Title,
        Content = model.Content,
        Keywords = model.Keywords,
        Type = (byte)CoreEnums.PostType.Poll,
        UpdatedBy = model.UpdatedBy,
        IsPinned = model.IsPinned,
        IsPublished = model.IsPublished
      });

      // TODO Fix bug KWA-897
      // Update poll
      //var poll = _replicaDbContext.Polls.FirstOrDefault(x => x.PostId == model.Id);
      //if (poll.Type != (int)Enum.Parse<CoreEnums.PollType>(model.Type))
      //{
      //  poll.Type = (int)Enum.Parse<CoreEnums.PollType>(model.Type);
      //  _masterDbContext.Polls.Update(poll);
      //}

      // Update poll options
      //_pollOptionAdminService.Update(new PollOptionUpdateInputModel
      //{
      //  PollId = poll.Guid,
      //  Options = model.Options
      //});

      _masterDbContext.SaveChanges();
    }

    #region Private methods

    private IQueryable<PostEntity> BuildQueryable(PollFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.Posts.AsNoTracking().Where(x => x.Type == (byte)PostType.Poll);
      query = PollQueryBuilder<PostEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    private void BuildResultData(List<PollListViewModel> data, Guid? userId, bool useMasterDb)
    {
      SetPolls(data, useMasterDb);
      SetUsers(data, useMasterDb);
      SetPollOptions(data, useMasterDb);
      SetNumberOfVote(data, useMasterDb);
      SetNumberOfLikes(data, useMasterDb);
      SetNumberOfViews(data, CoreConstants.UserTracking.ObjectType.Poll, useMasterDb);
      SetNumberOfComments(data, useMasterDb);
    }

    private void SetNumberOfVote(List<PollListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var pollIds = data.Select(x => x.PollId).ToList();
      var mapData = _replicaDbContext.Statistics
        .Where(x => x.ObjectType == (byte)StatisticsObjectType.Poll && pollIds.Contains(x.ObjectId))
        .ToDictionary(x => x.ObjectId, x => x.NumberOfVote);

      data.ForEach(x =>
      {
        x.NumberOfVote = mapData.GetValueOrDefault(x.PollId);
      });
    }

    private void SetPolls(List<PollListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var postIds = data.Select(x => x.Guid).ToList();
      var mapData = dbContext.Polls.Where(x => postIds.Contains(x.PostId))
        .ToDictionary(x => x.PostId, x => new { x.Guid, x.Type });

      data.ForEach(x =>
      {
        var item = mapData.GetValueOrDefault(x.Guid);
        if (item != null)
        {
          x.Type = Enum.GetName(typeof(PollType), item.Type);
          x.PollId = item.Guid;
        }
      });
    }

    private void SetPollOptions(List<PollListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;

      var postIds = data.Select(x => x.Guid).ToList();

      var mapData = dbContext.Polls
        .Where(x => postIds.Contains(x.PostId))
        .Join(dbContext.PollOptions,
        poll => poll.Guid,
        pollOption => pollOption.PollId,
        (poll, pollOption) => new
        {
          Id = poll.PostId,
          PollOptions = new PollOptionModel
          {
            Id = pollOption.Guid,
            Content = pollOption.Content
          },
        })
        .ToList()
        .ToLookup(x => x.Id, x => x.PollOptions);

      data.ForEach(x =>
      {
        x.PollOptions = mapData
        .Where(y => y.Key == x.Guid)
        .SelectMany(o => o).ToList();
      });
    }

    private void SetUsers(List<PollListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var userIds = data.Select(x => x.CreatedBy).ToList();
      var mapData = dbContext.Users.Where(x => userIds.Contains(x.UserId))
        .ToDictionary(x => x.UserId, x => x.UserName);

      data.ForEach(x =>
      {
        var name = mapData.GetValueOrDefault(x.CreatedBy);
        if (string.IsNullOrWhiteSpace(name)) return;

        x.UserName = name;
      });
    }

    public void ValidateBeforeCreate(PollInputModel model)
    {
      if (!model.Options.Any() || model.Options.Count() > 5)
      {
        throw new BadRequestException(ErrorMessages.Poll_OptionInvalid);
      }

      if (!model.Keywords.Any() || model.Keywords.Count() > 3)
      {
        throw new BadRequestException(ErrorMessages.Poll_KeywordsInvalid);
      }
    }

    public void ValidateBeforeUpdate(PollUpdateInputModel model)
    {
      var post = _masterDbContext.Posts.FirstOrDefault(x => x.Guid == model.Id);
      if (post == null)
        throw new NotFoundException(ErrorMessages.Poll_NotFound);

      // TODO Fix bug KWA-897
      //if (!model.Options.Any() || model.Options.Count() > 5)
      //{
      //  throw new BadRequestException(ErrorMessages.Poll_OptionInvalid);
      //}

      if (!model.Keywords.Any() || model.Keywords.Count() > 3)
      {
        throw new BadRequestException(ErrorMessages.Poll_KeywordsInvalid);
      }
    }

    public void SetNumberOfLikes(List<PollListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var postIds = data.Select(x => x.Guid).ToList();
      var mapData = dbContext.PostPollLikes
        .Where(x => postIds.Contains(x.ObjectId))
        .GroupBy(x => x.ObjectId)
        .Select(x => new
        {
          Id = x.Key,
          NumberOfLikes = x.Count()
        })
        .ToDictionary(x => x.Id, x => x.NumberOfLikes);

      data.ForEach(x =>
      {
        x.NumberOfLike = mapData.GetValueOrDefault(x.Guid);
      });
    }

    public void SetNumberOfComments(List<PollListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var postIds = data.Select(x => x.Guid).ToList();
      var mapData = dbContext.PostComments
        .Where(x => postIds.Contains(x.PostId))
        .GroupBy(x => x.PostId)
        .Select(x => new
        {
          Id = x.Key,
          NumberOfComments = x.Count()
        })
        .ToDictionary(x => x.Id, x => x.NumberOfComments);

      data.ForEach(x =>
      {
        x.NumberOfComment = mapData.GetValueOrDefault(x.Guid);
      });
    }

    public void SetNumberOfViews(List<PollListViewModel> data, string objectType, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var objectIds = data.Select(x => x.Guid).ToList();
      var mapData = dbContext.ViewTrackings
        .Where(x => objectIds.Contains(x.ObjectId.Value) && x.ObjectType == objectType)
        .GroupBy(x => x.ObjectId)
        .Select(x => new
        {
          Id = x.Key,
          NumberOfViews = x.Count()
        })
        .ToDictionary(x => x.Id, x => x.NumberOfViews);

      data.ForEach(x =>
      {
        x.NumberOfView = mapData.GetValueOrDefault(x.Guid);
      });
    }

    #endregion
  }
}

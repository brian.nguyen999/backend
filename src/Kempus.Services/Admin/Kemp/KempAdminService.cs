﻿using Kempus.Core.Enums;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Integration.Aws.ApiGateway;
using Kempus.Models.Admin.Kemp;
using Kempus.Models.Common;
using Kempus.Services.Common.KempTransaction;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.Admin.Kemp
{
  public class KempAdminService : IKempAdminService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly IKempTransactionCommonService _kempTransactionCommonService;
    private readonly ILogger _logger;

    public KempAdminService(
      MasterDbContext masterDbContext,
      ReplicaDbContext replicaDbContext,
      IKempTransactionCommonService kempTransactionCommonService,
      ILogger<KempAdminService> logger
    )
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
      _kempTransactionCommonService = kempTransactionCommonService;
      _logger = logger;
    }

    public PagingResult<KempListViewModel> Search(KempFilterModel filter, Pageable pageable, bool useMasterDb)
    {
      try
      {
        _logger.LogInformation("KempAdminService BuildQueryable");
        var query = BuildQueryable(filter, useMasterDb);
        _logger.LogInformation("KempAdminService BuildQueryable");
        var totalCount = query.LongCount();
        if (totalCount < 1)
        {
          return new PagingResult<KempListViewModel>(0, pageable.PageIndex, pageable.PageSize);
        }

        var data = query.Skip(pageable.Offset).Take(pageable.PageSize)
          .Select(x => new KempListViewModel(x)).ToList();
        _logger.LogInformation("KempAdminService BuildResultData");
        BuildResultData(data, useMasterDb);
        return new PagingResult<KempListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
      }
      catch(Exception ex)
      {
        _logger.LogInformation($"KempAdminService [Error]: {JsonUtils.ToJson(ex)}");
        throw new BadRequestException(ex.Message);
      }
    }

    public bool Create(KempTransactionInputModel input)
    {
      if (input.Action == (byte)KempTransactionAction.Deduct)
      {
        input.Amount = input.Amount * (-1);
      }

      return _kempTransactionCommonService.CreateTransaction(input);
    }

    #region Private Methods

    private IQueryable<KempTransactionEntity> BuildQueryable(KempFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.KempTransactions.AsNoTracking();
      query = KempTransactionQueryBuilder<KempTransactionEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    private void BuildResultData(List<KempListViewModel> data, bool useMasterDb)
    {
      _logger.LogInformation("KempAdminService BuildResultData SetStudent");
      SetStudent(data, useMasterDb);
      _logger.LogInformation("KempAdminService BuildResultData SetAdminName");
      SetAdminName(data, useMasterDb);
      _logger.LogInformation("KempAdminService BuildResultData SetReffereredId");
      SetReffereredId(data, useMasterDb);
      _logger.LogInformation("KempAdminService BuildResultData SetReporterId");
      SetReporterId(data, useMasterDb);
    }

    private void SetStudent(List<KempListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var transactionIds = data.Select(x => x.Id).ToList();
      var mapData = dbContext.KempTransactions.Where(x => transactionIds.Contains(x.Guid))
        .Join(dbContext.Users,
          transaction => transaction.UserId,
          user => user.UserId,
          (transaction, user) => new
          {
            TransactionGuid = transaction.Guid,
            Student = user.UserName
          })
        .ToDictionary(x => x.TransactionGuid, x => x.Student);

      data.ForEach(x =>
      {
        x.Student = mapData.GetValueOrDefault(x.Id);
      });
    }

    private void SetReffereredId(List<KempListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      data.ForEach(x =>
      {
        if (x.Type == (byte)CoreEnums.KempTransactionType.NewUserReferredSignedUp)
          x.TransactionReference = x.CreatorId;
      });
    }

    private void SetReporterId(List<KempListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var flagIds = data
        .Where(x => x.Type == (byte)CoreEnums.KempTransactionType.FlaggedByOtherUser)
        .Select(x => x.TransactionReference != null ? (Guid)(x.TransactionReference) : x.TransactionReference)
        .ToList();
      if (!flagIds.Any()) return;

      var flags = dbContext.Flags
        .Where(x => flagIds.Contains(x.Guid))
        .ToDictionary(x => x.Guid, x => x.CreatedBy);
      if (!flags.Any()) return;

      data.ForEach(x =>
      {
        if (x.Type == (byte)CoreEnums.KempTransactionType.FlaggedByOtherUser)
        {
          if(x.TransactionReference != null)
          {
             x.TransactionReference = flags.GetValueOrDefault((Guid)x.TransactionReference);
          }
        }
      });
    }

    private void SetAdminName(List<KempListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var transactionIds = data.Where(x => x.IsAdminCreated).Select(x => x.Id).ToList();
      var mapData = dbContext.KempTransactions.Where(x => transactionIds.Contains(x.Guid))
        .Join(dbContext.Users.Where(x => x.UserType == (byte)UserType.Admin),
          transaction => transaction.CreatedBy,
          admin => admin.UserId,
          (transaction, admin) => new
          {
            TransactionGuid = transaction.Guid,
            AdminName = (admin.FirstName + " " + admin.LastName).Trim()
          })
        .ToDictionary(x => x.TransactionGuid, x => x.AdminName);

      data.ForEach(x =>
      {
        if (x.IsAdminCreated)
          x.AdminName = mapData.GetValueOrDefault(x.Id);
      });
    }
    #endregion
  }
}

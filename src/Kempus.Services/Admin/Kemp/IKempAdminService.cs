﻿using Kempus.Core.Models;
using Kempus.Models.Admin.Kemp;
using Kempus.Models.Common;

namespace Kempus.Services.Admin.Kemp
{
  public interface IKempAdminService
  {
    PagingResult<KempListViewModel> Search(KempFilterModel filter, Pageable pageable, bool useMasterDb);
    bool Create(KempTransactionInputModel input);
  }
}

﻿using Kempus.Core.Models;
using Kempus.Models.Admin.Post;

namespace Kempus.Services.Admin.Post
{
  public interface IPostAdminService
  {
    PagingResult<PostListViewModel> Search(PostFilterModel filter, Pageable pageable, bool useMasterDb,
      Guid? currentUserId = null);
    PostDetailModel GetDetail(Guid id, Guid? currentUserId = null);
    void Update(PostInputModel model);
    Guid Create(PostInputModel model);
  }
}

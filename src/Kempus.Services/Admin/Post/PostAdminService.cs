﻿using Kempus.Core.Constants;
using Kempus.Core.Enums;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Admin.Post;
using Kempus.Models.Admin.Topic;
using Kempus.Models.Public.PostComments;
using Kempus.Models.Public.Tracking;
using Kempus.Services.Admin.Topic;
using Kempus.Services.Public.PostKeyword;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace Kempus.Services.Admin.Post
{
  public class PostAdminService : IPostAdminService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly IPostKeywordService _postKeywordService;
    private readonly IConfiguration _configuration;
    private readonly ITopicAdminService _topicAdminService;

    public PostAdminService(
      MasterDbContext masterDbContext,
      ReplicaDbContext replicaDbContext,
      IPostKeywordService postKeywordService,
      IConfiguration configuration,
      ITopicAdminService topicAdminService
      )
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
      _postKeywordService = postKeywordService;
      _configuration = configuration;
      _topicAdminService = topicAdminService;
    }

    public PagingResult<PostListViewModel> Search(PostFilterModel filter, Pageable pageable, bool useMasterDb, Guid? currentUserId = null)
    {
      var query = BuildQueryable(filter, useMasterDb);

      var totalCount = query.LongCount();
      if (totalCount < 1)
        return new PagingResult<PostListViewModel>(0, pageable.PageIndex, pageable.PageSize);

      var orderBy = QueryableUtils.GetOrderByFunction<PostEntity>(pageable.GetSortables(nameof(PostEntity.CreatedDate)));

      var data = orderBy(query).Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new PostListViewModel(x)).ToList();

      BuildResultData(data, currentUserId, useMasterDb);

      return new PagingResult<PostListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    public PostDetailModel GetDetail(Guid id, Guid? currentUserId = null)
    {
      var post = _replicaDbContext.Posts.FirstOrDefault(x => x.Guid == id);
      if (post == null)
        throw new NotFoundException(ErrorMessages.Post_NotFound);

      return new PostDetailModel
      {
        Id = post.Id,
        Guid = post.Guid,
        Title = post.Title,
        Content = post.Content,
        Keywords = JsonUtils.Convert<List<string>>(post.Keywords),
        TopicId = post.TopicId,
        IsPublished = post.IsPublished
      };
    }

    public void Update(PostInputModel model)
    {
      using var transaction = _masterDbContext.Database.BeginTransaction();
      try
      {
        var post = _replicaDbContext.Posts.FirstOrDefault(x => x.Guid == model.Id);
        if (post == null)
          throw new NotFoundException(ErrorMessages.Post_NotFound);

        post.Title = model.Title;
        post.Keywords = model.Keywords != null && model.Keywords.Any()
          ? JsonConvert.SerializeObject(model.Keywords)
          : CoreConstants.JsonData.EmptyArray;
        post.TopicId = model.TopicId;
        post.Content = model.Content;
        //post.Type = model.Type != null ? (byte)model.Type : (byte)CoreEnums.PostType.Post;
        post.UpdatedBy = model.UpdatedBy.GetValueOrDefault();
        post.UpdatedDate = DateTime.UtcNow;
        post.IsPublished = model.IsPublished;
        post.IsPinned = model.IsPinned;
        post.PinnedDate = post.IsPinned ? DateTime.UtcNow : null;
        _masterDbContext.Posts.Update(post);
        _masterDbContext.SaveChanges();

        if (model.Keywords != null && model.Keywords.Any())
        {
          _postKeywordService.Update(post.Guid, model.Keywords);
        }

        transaction.Commit();
      }
      catch (Exception)
      {
        transaction.Rollback();
        throw;
      }
    }

    public Guid Create(PostInputModel model)
    {
      using var transaction = _masterDbContext.Database.BeginTransaction();
      try
      {
        ValidateBeforeInsertOrUpdate(model);
        var entity = new PostEntity
        {
          Title = model.Title,
          Keywords = model.Keywords != null && model.Keywords.Any()
            ? JsonConvert.SerializeObject(model.Keywords)
            : CoreConstants.JsonData.EmptyArray,
          TopicId = model.TopicId,
          Content = model.Content,
          Type = model.Type != null ? (byte)model.Type : (byte)CoreEnums.PostType.Post,
          SchoolId = model.SchoolId,
          CreatedBy = model.CreatedBy,
          IsPublished = model.IsPublished,
          IsPinned = model.IsPinned,
          PinnedDate = model.IsPinned ? DateTime.UtcNow : null
        };

        _masterDbContext.Posts.Add(entity);
        _masterDbContext.SaveChanges();

        if (model.Keywords != null && model.Keywords.Any())
        {
          _postKeywordService.Insert(entity.Guid, model.Keywords);
        }

        _masterDbContext.SaveChanges();
        transaction.Commit();
        return entity.Guid;
      }
      catch (Exception)
      {
        transaction.Rollback();
        throw;
      }
    }

    #region Private methods

    private IQueryable<PostEntity> BuildQueryable(PostFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.Posts.AsNoTracking().Where(x => x.Type == (byte)CoreEnums.PostType.Post);
      query = PostQueryBuilder<PostEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    private void BuildResultData(List<PostListViewModel> data, Guid? userId, bool useMasterDb)
    {
      SetTopic(data, useMasterDb);
      SetUser(data, useMasterDb);
      SetNumberOfLikes(data, useMasterDb);
      SetNumberOfViews(data, CoreConstants.UserTracking.ObjectType.Post, useMasterDb);
      SetNumberOfComments(data, useMasterDb);
    }

    private void SetTopic(List<PostListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var postIds = data.Select(x => x.Guid).ToList();
      var mapData = dbContext.Posts
        .Join(dbContext.Topics,
          post => post.TopicId,
          topic => topic.Guid,
          (post, topic) => new { post, topic })
        .Where(x => postIds.Contains(x.post.Guid))
        .ToDictionary(x => x.post.Guid, x => x.topic?.Name);

      data.ForEach(x =>
      {
        var item = mapData.GetValueOrDefault(x.Guid);
        if (string.IsNullOrWhiteSpace(item)) return;

        x.Topic = item;
      });
    }

    private void SetUser(List<PostListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var userIds = data.Select(x => x.CreatedBy).ToList();
      var mapData = dbContext.Users.Where(x => userIds.Contains(x.UserId))
        .ToDictionary(x => x.UserId, x => x.UserName);

      data.ForEach(x =>
      {
        var name = mapData.GetValueOrDefault(x.CreatedBy);
        if (string.IsNullOrWhiteSpace(name)) return;

        x.UserName = name;
      });
    }

    public void ValidateBeforeInsertOrUpdate(PostInputModel model)
    {
      if (model.Id != null)
      {
        var post = _replicaDbContext.Posts.FirstOrDefault(x => x.Guid == model.Id);
        if (post == null)
          throw new NotFoundException(ErrorMessages.Post_NotFound);
      }

      if (model.TopicId != null)
      {
        var result = _topicAdminService.Search(new TopicFilterModel { TopicId = model.TopicId }, new Pageable(), true);
        if (result.TotalCount == 0)
          throw new NotFoundException(ErrorMessages.Topic_NotFound);
      }

      if (!model.Keywords.Any() || model.Keywords.Count() > 3)
      {
        throw new BadRequestException(ErrorMessages.Post_KeywordsInvalid);
      }
    }

    public void SetNumberOfLikes(List<PostListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var postIds = data.Select(x => x.Guid).ToList();
      var mapData = dbContext.PostPollLikes
        .Where(x => postIds.Contains(x.ObjectId))
        .GroupBy(x => x.ObjectId)
        .Select(x => new
        {
          Id = x.Key,
          NumberOfLikes = x.Count()
        })
        .ToDictionary(x => x.Id, x => x.NumberOfLikes);

      data.ForEach(x =>
      {
        x.NumberOfLike = mapData.GetValueOrDefault(x.Guid);
      });
    }

    public void SetNumberOfComments(List<PostListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var postIds = data.Select(x => x.Guid).ToList();
      var mapData = dbContext.PostComments
        .Where(x => postIds.Contains(x.PostId))
        .GroupBy(x => x.PostId)
        .Select(x => new
        {
          Id = x.Key,
          NumberOfComments = x.Count()
        })
        .ToDictionary(x => x.Id, x => x.NumberOfComments);

      data.ForEach(x =>
      {
        x.NumberOfComment = mapData.GetValueOrDefault(x.Guid);
      });
    }

    public void SetNumberOfViews(List<PostListViewModel> data, string objectType, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var objectIds = data.Select(x => x.Guid).ToList();
      var mapData = dbContext.ViewTrackings
        .Where(x => objectIds.Contains(x.ObjectId.Value) && x.ObjectType == objectType)
        .GroupBy(x => x.ObjectId)
        .Select(x => new
        {
          Id = x.Key,
          NumberOfViews = x.Count()
        })
        .ToDictionary(x => x.Id, x => x.NumberOfViews);

      data.ForEach(x =>
      {
        x.NumberOfView = mapData.GetValueOrDefault(x.Guid);
      });
    }

    #endregion


  }
}

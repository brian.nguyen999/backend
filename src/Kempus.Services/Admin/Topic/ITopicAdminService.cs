﻿using Kempus.Core.Models;
using Kempus.Models.Admin.Topic;

namespace Kempus.Services.Admin.Topic
{
  public interface ITopicAdminService
  {
    PagingResult<TopicListViewModel> Search(TopicFilterModel model, Pageable pageable, bool useMasterDb);
    TopicDetailModel GetDetail(Guid id);
    Guid Create(TopicInputModel model);
    void Update(TopicInputModel model);
  }
}

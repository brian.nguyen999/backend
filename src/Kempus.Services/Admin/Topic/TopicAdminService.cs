﻿using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Admin.Topic;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;

namespace Kempus.Services.Admin.Topic
{
  public class TopicAdminService : ITopicAdminService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;

    public TopicAdminService(
      MasterDbContext masterContext,
      ReplicaDbContext replicaDbContext
    )
    {
      _masterDbContext = masterContext;
      _replicaDbContext = replicaDbContext;
    }

    public PagingResult<TopicListViewModel> Search(TopicFilterModel model, Pageable pageable, bool useMasterDb)
    {
      var query = BuildQueryable(model, useMasterDb);

      var totalCount = query.LongCount();
      if (totalCount < 1)
      {
        return new PagingResult<TopicListViewModel>(0, pageable.PageIndex, pageable.PageSize);
      }

      var orderBy =
        QueryableUtils.GetOrderByFunction<TopicEntity>(pageable.GetSortables(nameof(TopicEntity.CreatedDate)));

      var data = orderBy(query).Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new TopicListViewModel(x)).ToList();

      BuildResultData(data, useMasterDb);

      return new PagingResult<TopicListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    public Guid Create(TopicInputModel model)
    {
      ValidateBeforeCreateOrEdit(model);

      var entity = new TopicEntity
      {
        SchoolId = model.SchoolId.Value,
        CreatedBy = model.CreatorId,
        Name = model.Name,
        IsActivated = model.IsActivated
      };

      _masterDbContext.Topics.Add(entity);
      _masterDbContext.SaveChanges();

      return entity.Guid;
    }

    public void Update(TopicInputModel model)
    {
      ValidateBeforeCreateOrEdit(model);
      var topic = _masterDbContext.Topics.FirstOrDefault(x => x.Guid == model.Id);
      topic.IsActivated = model.IsActivated;
      topic.UpdatedDate = DateTime.UtcNow;
      topic.UpdatedBy = model.CreatorId;

      _masterDbContext.Topics.Update(topic);
      _masterDbContext.SaveChanges();
    }

    public TopicDetailModel GetDetail(Guid id)
    {
      var query = BuildQueryable(new TopicFilterModel { TopicId = id }, false);

      var topic = query.FirstOrDefault();
      if (topic == null)
        throw new NotFoundException(ErrorMessages.Topic_NotFound);

      return new TopicDetailModel
      {
        Id = topic.Id,
        Guid = topic.Guid,
        Name = topic.Name,
        IsActivated = topic.IsActivated
      };
    }

    #region Private methods

    private void ValidateBeforeCreateOrEdit(TopicInputModel model)
    {
      if (model.Id != null)
      {
        var topic = _replicaDbContext.Topics.FirstOrDefault(x => x.Guid == model.Id);
        if (topic == null)
          throw new NotFoundException(ErrorMessages.Topic_NotFound);
      }

      if (model.SchoolId != null)
      {
        var school = _replicaDbContext.Schools.FirstOrDefault(x => x.Guid == model.SchoolId);
        if (school == null)
          throw new NotFoundException(ErrorMessages.School_NotFound);
      }

      if (model.Name != null)
      {
        var topicName = _replicaDbContext.Topics.FirstOrDefault(x => x.Name.ToLower() == model.Name.ToLower() && x.SchoolId == model.SchoolId);
        if (topicName != null)
          throw new BadRequestException(ErrorMessages.Topic_NameExisted);
      }
    }

    private void BuildResultData(List<TopicListViewModel> data, bool useMasterDb)
    {
      SetNumberOfPost(data, useMasterDb);
    }

    private void SetNumberOfPost(List<TopicListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var topicIds = data.Select(x => x.Guid).ToList();
      var mapData = dbContext.Posts.Where(x => topicIds.Contains(x.TopicId.Value))
        .GroupBy(x => x.TopicId,
          (key, data) => new
          {
            TopicId = key,
            NumberOfPost = data.Count()
          })
        .ToDictionary(x => x.TopicId, x => x.NumberOfPost);

      data.ForEach(x => { x.NumberOfPost = mapData.GetValueOrDefault(x.Guid); });
    }

    private IQueryable<TopicEntity> BuildQueryable(TopicFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.Topics.AsNoTracking();
      query = TopicQueryBuilder<TopicEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    #endregion
  }
}
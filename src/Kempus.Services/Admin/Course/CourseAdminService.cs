﻿using Kempus.Core.Errors;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Admin.Course;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.Admin.Course
{
  public class CourseAdminService : ICourseAdminService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly ICourseInstructorAdminService _courseInstructorAdminService;

    public CourseAdminService(
      MasterDbContext masterDbContext,
      ReplicaDbContext replicaDbContext,
      ICourseInstructorAdminService courseInstructorAdminService)
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
      _courseInstructorAdminService = courseInstructorAdminService;
    }

    public Guid Create(CourseInputCmsModel model)
    {
      ValidateBeforeCreateOrUpdate(model);

      using var transaction = _masterDbContext.Database.BeginTransaction();
      try
      {
        var entity = new CourseEntity
        {
          Name = model.Name,
          IsActivated = model.Status,
          SchoolId = model.SchoolId,
          DepartmentId = model.DepartmentProgramId,
          GeneratedTypeId = (byte)GeneratedType.ManualInCMS,
          CreatedBy = model.CreatorId.GetValueOrDefault()
        };

        _masterDbContext.Courses.Add(entity);
        _masterDbContext.SaveChanges();

        _courseInstructorAdminService.Create(new CourseInstructorInputCmsModel(entity.Guid, model.InstructorIds));
        transaction.Commit();

        return entity.Guid;
      }
      catch
      {
        transaction.Rollback();
        throw;
      }
    }

    public void Update(CourseInputCmsModel model)
    {
      ValidateBeforeCreateOrUpdate(model);

      using var transaction = _masterDbContext.Database.BeginTransaction();
      try
      {
        var course = _masterDbContext.Courses.FirstOrDefault(x => x.Guid == model.Id);
        course.Name = model.Name;
        course.IsActivated = model.Status;
        course.DepartmentId = model.DepartmentProgramId;
        course.UpdatedBy = model.UpdaterId.GetValueOrDefault();
        course.UpdatedDate = DateTime.UtcNow;

        _masterDbContext.Courses.Update(course);
        _masterDbContext.SaveChanges();

        _courseInstructorAdminService.Update(new CourseInstructorInputCmsModel(course.Guid, model.InstructorIds));
        transaction.Commit();
      }
      catch
      {
        transaction.Rollback();
        throw;
      }
    }

    private void ValidateBeforeCreateOrUpdate(CourseInputCmsModel model)
    {
      if (model.Id != null)
      {
        var course = _replicaDbContext.Courses.FirstOrDefault(x => x.Guid == model.Id);
        if (course == null)
          throw new NotFoundException(ErrorMessages.Course_NotFound);
      }

      if (model.SchoolId != null)
      {
        var school = _replicaDbContext.Schools.FirstOrDefault(x => x.Guid == model.SchoolId);
        if (school == null)
          throw new NotFoundException(ErrorMessages.School_NotFound);
      }

      if (model.CreatorId != null)
      {
        var user = _replicaDbContext.Users.FirstOrDefault(x => x.UserId == model.CreatorId);
        if (user == null) throw new NotFoundException(ErrorMessages.User_NotFound);
      }

      if (model.UpdaterId != null)
      {
        var user = _replicaDbContext.Users.FirstOrDefault(x => x.UserId == model.UpdaterId);
        if (user == null) throw new NotFoundException(ErrorMessages.User_NotFound);
      }
    }
  }
}

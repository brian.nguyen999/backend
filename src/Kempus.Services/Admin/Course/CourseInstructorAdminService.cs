﻿using Kempus.Core.Errors;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Admin.Course;

namespace Kempus.Services.Admin.Course
{
  public class CourseInstructorAdminService : ICourseInstructorAdminService
  {
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly MasterDbContext _masterDbContext;

    public CourseInstructorAdminService(
      ReplicaDbContext replicaDbContext,
      MasterDbContext masterDbContext)
    {
      _replicaDbContext = replicaDbContext;
      _masterDbContext = masterDbContext;
    }

    public void Create(CourseInstructorInputCmsModel model)
    {
      ValidateBeforeCreateOrUpdate(model);

      List<CourseInstructorEntity> courseInstructors = new();
      foreach (var instructorId in model.InstructorIds)
      {
        courseInstructors.Add(new CourseInstructorEntity
        {
          CourseId = model.CourseId,
          InstructorId = instructorId,
        });
      }

      _masterDbContext.CourseInstructor.AddRange(courseInstructors);
      _masterDbContext.SaveChanges();
    }

    public void Update(CourseInstructorInputCmsModel model)
    {
      ValidateBeforeCreateOrUpdate(model);

      var existCourseInstructors = _masterDbContext.CourseInstructor.Where(x => x.CourseId == model.CourseId);
      var deleteCourseInstructors = existCourseInstructors.Where(x => !model.InstructorIds.Contains(x.Guid));

      var existCourseInstructorIds = existCourseInstructors.Select(x => x.Guid).ToList();
      var newCourseInstructorIds = model.InstructorIds.Except(existCourseInstructorIds).ToList();

      if (deleteCourseInstructors.Any())
      {
        _masterDbContext.CourseInstructor.RemoveRange(deleteCourseInstructors);
        _masterDbContext.SaveChanges();
      }

      if (newCourseInstructorIds.Any())
        Create(new CourseInstructorInputCmsModel(model.CourseId, newCourseInstructorIds));
    }

    private void ValidateBeforeCreateOrUpdate(CourseInstructorInputCmsModel model)
    {
      var course = _masterDbContext.Courses.FirstOrDefault(x => x.Guid == model.CourseId);
      if (course == null)
        throw new NotFoundException(ErrorMessages.Course_NotFound);
    }
  }
}

﻿using Kempus.Models.Admin.Course;

namespace Kempus.Services.Admin.Course
{
  public interface ICourseAdminService
  {
    Guid Create(CourseInputCmsModel model);
    void Update(CourseInputCmsModel model);
  }
}

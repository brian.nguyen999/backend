﻿using Kempus.Models.Admin.Course;

namespace Kempus.Services.Admin.Course
{
  public interface ICourseInstructorAdminService
  {
    void Create(CourseInstructorInputCmsModel model);
    void Update(CourseInstructorInputCmsModel model);
  }
}

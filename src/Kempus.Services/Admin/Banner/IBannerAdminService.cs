﻿using Kempus.Core.Models;
using Kempus.Models.Admin.Banner;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Admin.Banner
{
  public interface IBannerAdminService
  {
    PagingResult<BannerListViewModel> Search(BannerCmsFilterModel filter, Pageable pageable, bool useMasterDb);
    Guid Create(BannerInputModel input);
    BannerDetailModel GetDetail(Guid id);
    Guid Update(BannerInputModel input);
  }
}

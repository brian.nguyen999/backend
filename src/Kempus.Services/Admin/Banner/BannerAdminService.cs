﻿using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Core.Models.Configuration;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Admin.Banner;
using Kempus.Models.Common;
using Kempus.Services.Common.Upload;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;
using static Kempus.Core.Utils.FileUtilities;

namespace Kempus.Services.Admin.Banner
{
  public class BannerAdminService : IBannerAdminService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly AwsS3IntegrationHelper _awsS3IntegrationHelper;
    private S3Config _s3Config;
    private readonly AdminConfig _adminConfig;

    public BannerAdminService(
      MasterDbContext masterDbContext,
      ReplicaDbContext replicaDbContext,
      AwsS3IntegrationHelper awsS3IntegrationHelper,
      AwsConfig awsConfig
,
      AdminConfig adminConfig)
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
      _awsS3IntegrationHelper = awsS3IntegrationHelper;
      _s3Config = awsConfig.S3;
      _adminConfig = adminConfig;
    }

    public PagingResult<BannerListViewModel> Search(BannerCmsFilterModel filter, Pageable pageable, bool useMasterDb)
    {
      var query = BuildQueryable(filter, useMasterDb);
      var totalCount = query.LongCount();
      if (totalCount < 1)
      {
        return new PagingResult<BannerListViewModel>(0, pageable.PageIndex, pageable.PageSize);
      }

      var data = query.Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new BannerListViewModel(x)).ToList();

      BuildResultData(data, useMasterDb);

      return new PagingResult<BannerListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    public Guid Create(BannerInputModel input)
    {
      ValidateBeforeCreateOrUpdate(input);
      // Upload file to s3 and save info to DB to track history
      var currentTime = DateTime.UtcNow;
      string s3UploadPath = $"file-uploads/{currentTime.Year}/{currentTime.Month}/{currentTime.Day}/";
      var uploadWebBannerResponse = _awsS3IntegrationHelper.UploadFileAsync(input.WebBanner, _s3Config.PublicBucket, s3UploadPath, input.WebBanner.FileName, false).Result;
      var uploadMobileBannerResponse = _awsS3IntegrationHelper.UploadFileAsync(input.MobileBanner, _s3Config.PublicBucket, s3UploadPath, input.MobileBanner.FileName, false).Result;

      var listTrackingEntity = new List<FileUploadTrackingEntity>()
      {
        new FileUploadTrackingEntity()
        {
          SchoolId = input.SchoolId,
          FileName = input.WebBanner.FileName,
          Type = input.WebBanner.ContentType,
          FileSize = input.WebBanner.Length,
          FileUrl = uploadWebBannerResponse.S3Url,
          CreatedBy = input.CreatedBy,
          CreatedDate = currentTime,
        },
        new FileUploadTrackingEntity()
        {
          SchoolId = input.SchoolId,
          FileName = input.MobileBanner.FileName,
          Type = input.MobileBanner.ContentType,
          FileSize = input.MobileBanner.Length,
          FileUrl = uploadMobileBannerResponse.S3Url,
          CreatedBy = input.CreatedBy,
          CreatedDate = currentTime
        }
      };

      BannerEntity newBanner = new BannerEntity()
      {
        WebUrl = $"{s3UploadPath}{input.WebBanner.FileName}",
        WebBannerFileName = input.WebBanner.FileName,
        MobileUrl = $"{s3UploadPath}{input.MobileBanner.FileName}",
        MobileBannerFileName = input.MobileBanner.FileName,
        RedirectUrl = input.Url,
        UrlType = input.UrlType.GetValueOrDefault(),
        Status = input.Status.GetValueOrDefault(),
        SchoolId = input.SchoolId,
        CreatedBy = input.CreatedBy,
        CreatedDate = currentTime
      };

      using var transaction = _masterDbContext.Database.BeginTransaction();
      try
      {
        _masterDbContext.Banners.Add(newBanner);
        _masterDbContext.FileUploadTrackings.AddRange(listTrackingEntity);
        _masterDbContext.SaveChanges();

        transaction.Commit();
        return newBanner.Guid;
      }
      catch (Exception)
      {
        transaction.Rollback();
        throw;
      }
    }

    public BannerDetailModel GetDetail(Guid id)
    {
      var query = BuildQueryable(new BannerCmsFilterModel() { Id = id }, false);
      var totalCount = query.LongCount();
      if (totalCount < 1)
      {
        throw new BadRequestException(ErrorMessages.CourseReview_NotFound);
      }

      var data = query.Select(x => new BannerListViewModel(x)).ToList();
      BuildResultData(data, false);

      return data.Select(x => new BannerDetailModel()
      {
        Id = x.Id,
        SchoolName = x.SchoolName,
        WebBannerFileName = x.WebBannerFileName,
        WebBannerUrl = x.WebImage,
        MobileBannerFileName = x.MobileBannerFileName,
        MobileBannerUrl = x.MobileImage,
        Status = x.Status,
        Url = x.UrlLink,
        UrlType = x.UrlType
      }).FirstOrDefault();
    }

    public Guid Update(BannerInputModel input)
    {
      ValidateBeforeCreateOrUpdate(input);
      var currentEntity = _replicaDbContext.Banners.Where(x => x.Guid == input.Id).FirstOrDefault();
      // Upload file to s3 and save info to DB to track history
      var currentTime = DateTime.UtcNow;
      string s3UploadPath = $"file-uploads/{currentTime.Year}/{currentTime.Month}/{currentTime.Day}/";
      UploadFileResponse uploadWebBannerResponse;
      UploadFileResponse uploadMobileBannerResponse;
      var listTrackingEntity = new List<FileUploadTrackingEntity>();
      if (input.WebBanner != null)
      {
        uploadWebBannerResponse = _awsS3IntegrationHelper.UploadFileAsync(input.WebBanner, _s3Config.PublicBucket, s3UploadPath, input.WebBanner.FileName, false).Result;
        listTrackingEntity.Add(new FileUploadTrackingEntity()
        {
          SchoolId = input.SchoolId,
          FileName = input.WebBanner.FileName,
          Type = input.WebBanner.ContentType,
          FileSize = input.WebBanner.Length,
          FileUrl = uploadWebBannerResponse.S3Url,
          CreatedBy = input.CreatedBy,
          CreatedDate = currentTime,
        });
        currentEntity.WebUrl = $"{s3UploadPath}{input.WebBanner.FileName}";
        currentEntity.WebBannerFileName = input.WebBanner.FileName;
      }

      if (input.MobileBanner != null)
      {
        uploadMobileBannerResponse = _awsS3IntegrationHelper.UploadFileAsync(input.MobileBanner, _s3Config.PublicBucket, s3UploadPath, input.MobileBanner.FileName, false).Result;
        listTrackingEntity.Add(new FileUploadTrackingEntity()
        {
          SchoolId = input.SchoolId,
          FileName = input.MobileBanner.FileName,
          Type = input.MobileBanner.ContentType,
          FileSize = input.MobileBanner.Length,
          FileUrl = uploadMobileBannerResponse.S3Url,
          CreatedBy = input.CreatedBy,
          CreatedDate = currentTime
        });
        currentEntity.MobileUrl = $"{s3UploadPath}{input.MobileBanner.FileName}";
        currentEntity.MobileBannerFileName = input.MobileBanner.FileName;
      }

      currentEntity.RedirectUrl = input.Url;
      currentEntity.UrlType = input.UrlType.GetValueOrDefault();
      currentEntity.Status = input.Status.GetValueOrDefault();
      currentEntity.UpdatedBy = input.UpdatedBy.GetValueOrDefault();
      currentEntity.UpdatedDate = DateTime.UtcNow;

      using var transaction = _masterDbContext.Database.BeginTransaction();
      try
      {
        if (listTrackingEntity.Any())
        {
          _masterDbContext.FileUploadTrackings.AddRange(listTrackingEntity);
        }
        _masterDbContext.Banners.Update(currentEntity);
        _masterDbContext.SaveChanges();
        transaction.Commit();
      }
      catch (Exception)
      {
        transaction.Rollback();
        throw;
      }

      return currentEntity.Guid;
    }

    #region Private Methods
    private IQueryable<BannerEntity> BuildQueryable(BannerCmsFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.Banners.AsNoTracking();
      query = BannerQueryBuilder<BannerEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    private void BuildResultData(List<BannerListViewModel> data, bool useMasterDb)
    {
      SetImageUrl(data);
      SetSchoolName(data, useMasterDb);
    }

    private void SetImageUrl(List<BannerListViewModel> data)
    {
      data.ForEach(x =>
      {
        x.WebImage = $"{_s3Config.PublicDomain}/{x.WebImage}";
        x.MobileImage = $"{_s3Config.PublicDomain}/{x.MobileImage}";
      });
    }

    private void SetSchoolName(List<BannerListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var courseIds = data.Select(x => x.Id).ToList();
      var mapData = dbContext.Banners.Where(x => courseIds.Contains(x.Guid))
        .Join(dbContext.Schools,
          banner => banner.SchoolId,
          school => school.Guid,
          (banner, school) => new
          {
            BannerId = banner.Guid,
            SchoolName = school.Name
          })
        .ToDictionary(x => x.BannerId, x => x.SchoolName);

      data.ForEach(x =>
      {
        x.SchoolName = mapData.GetValueOrDefault(x.Id);
      });
    }

    private void ValidateBeforeCreateOrUpdate(BannerInputModel input)
    {
      if (input.Id == null) // Create Validation
      {
        if (_replicaDbContext.Banners.Count(x => x.SchoolId == input.SchoolId) >= _adminConfig.MaxBannerPerSchool)
        {
          throw new BadRequestException(ErrorMessages.Banner_Upload_MaxQuatity);
        }
      }
      else // Update Validation
      {
        var currentEntity = _replicaDbContext.Banners.FirstOrDefault(x => x.Guid == input.Id);
        if (currentEntity == null)
        {
          throw new BadRequestException(ErrorMessages.Banner_NotFound);
        }
      }

      if (input.WebBanner != null)
      {
        if (!FileHelper.IsImageFile(input.WebBanner?.ContentType))
        {
          throw new BadRequestException(String.Format(ErrorMessages.Banner_Upload_UnsupportedFile));
        }

        if (input.WebBanner?.FileName.Length > 250)
        {
          throw new BadRequestException(String.Format(ErrorMessages.Banner_Upload_FileNameTooLong, 250));
        }

        if (input.WebBanner?.Length > _s3Config.MaximumFileSize)
        {
          throw new BadRequestException(ErrorMessages.Banner_Upload_FileSizeTooBig);
        }
      }

      if (input.MobileBanner != null)
      {
        if (!FileHelper.IsImageFile(input.MobileBanner?.ContentType))
        {
          throw new BadRequestException(String.Format(ErrorMessages.Banner_Upload_UnsupportedFile));
        }

        if (input.MobileBanner?.FileName.Length > 250)
        {
          throw new BadRequestException(String.Format(ErrorMessages.Banner_Upload_FileNameTooLong, 250));
        }

        if (input.MobileBanner?.Length > _s3Config.MaximumFileSize)
        {
          throw new BadRequestException(ErrorMessages.Banner_Upload_FileSizeTooBig);
        }
      }

    }

    #endregion
  }
}

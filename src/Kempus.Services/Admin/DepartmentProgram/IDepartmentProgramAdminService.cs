﻿using Kempus.Models.Admin.DepartmentProgram;

namespace Kempus.Services.Admin.DepartmentProgram
{
  public interface IDepartmentProgramAdminService
  {
    Guid Create(DepartmentProgramInputCmsModel model);
    void Update(DepartmentProgramInputCmsModel model);
  }
}

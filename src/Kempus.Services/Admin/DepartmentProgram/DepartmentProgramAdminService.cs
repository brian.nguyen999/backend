﻿using Kempus.Core.Errors;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Admin.DepartmentProgram;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.Admin.DepartmentProgram
{
  public class DepartmentProgramAdminService : IDepartmentProgramAdminService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;

    public DepartmentProgramAdminService(
      MasterDbContext masterDbContext,
      ReplicaDbContext replicaDbContext)
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
    }

    public Guid Create(DepartmentProgramInputCmsModel model)
    {
      ValidateBeforeCreateOrUpdate(model);

      var entity = new DepartmentProgramEntity
      {
        Name = model.Name,
        Status = (byte)DepartmentProgramStatus.Active,
        SchoolId = model.SchoolId.GetValueOrDefault(),
        GeneratedTypeId = (byte)GeneratedType.ManualInCMS,
        CreatedBy = model.CreatorId.GetValueOrDefault()
      };

      _masterDbContext.DepartmentPrograms.Add(entity);
      _masterDbContext.SaveChanges();

      return entity.Guid;
    }

    public void Update(DepartmentProgramInputCmsModel model)
    {
      ValidateBeforeCreateOrUpdate(model);

      var entity = _masterDbContext.DepartmentPrograms.FirstOrDefault(x => x.Guid == model.Id);
      entity.Name = model.Name;
      entity.UpdatedBy = model.UpdaterId.GetValueOrDefault();
      entity.UpdatedDate = DateTime.UtcNow;

      _masterDbContext.DepartmentPrograms.Update(entity);
      _masterDbContext.SaveChanges();
    }

    private void ValidateBeforeCreateOrUpdate(DepartmentProgramInputCmsModel model)
    {
      if (model.Id != null)
      {
        var departmentProgram = _replicaDbContext.DepartmentPrograms.FirstOrDefault(x => x.Guid == model.Id);
        if (departmentProgram == null)
          throw new NotFoundException(ErrorMessages.DepartmentProgram_NotFound);
      }

      if (model.SchoolId != null)
      {
        var school = _replicaDbContext.Schools.FirstOrDefault(x => x.Guid == model.SchoolId);
        if (school == null)
          throw new NotFoundException(ErrorMessages.School_NotFound);
      }

      if (model.CreatorId != null)
      {
        var user = _replicaDbContext.Users.FirstOrDefault(x => x.UserId == model.CreatorId);
        if (user == null) throw new NotFoundException(ErrorMessages.User_NotFound);
      }

      if (model.UpdaterId != null)
      {
        var user = _replicaDbContext.Users.FirstOrDefault(x => x.UserId == model.UpdaterId);
        if (user == null) throw new NotFoundException(ErrorMessages.User_NotFound);
      }
    }
  }
}

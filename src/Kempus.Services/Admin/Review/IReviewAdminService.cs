﻿using Kempus.Core.Models;
using Kempus.Models.Admin.Review;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Admin.Review
{
  public interface IReviewAdminService
  {
    PagingResult<ReviewListViewModel> Search(ReviewFilterModel filter, Pageable pageable, bool useMasterDb);
    Guid Update(ReviewInputModel input);
    ReviewDetailModel GetDetail(Guid id);
  }
}

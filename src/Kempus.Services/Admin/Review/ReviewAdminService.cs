﻿using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Admin.Review;
using Kempus.Services.Common.Queue;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;

namespace Kempus.Services.Admin.Review
{
  public class ReviewAdminService : IReviewAdminService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly IOpenSearchEventPublisher _openSearchEventPublisher;
    public ReviewAdminService(
      MasterDbContext masterDbContext,
      ReplicaDbContext replicaDbContext,
      IOpenSearchEventPublisher openSearchEventPublisher
      )
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
      _openSearchEventPublisher = openSearchEventPublisher;
    }

    public PagingResult<ReviewListViewModel> Search(ReviewFilterModel filter, Pageable pageable, bool useMasterDb)
    {
      var query = BuildQueryable(filter, useMasterDb);
      var totalCount = query.LongCount();
      if (totalCount < 1)
      {
        return new PagingResult<ReviewListViewModel>(0, pageable.PageIndex, pageable.PageSize);
      }

      var data = query.Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new ReviewListViewModel(x)).ToList();
      BuildResultData(data, useMasterDb);
      return new PagingResult<ReviewListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    public Guid Update(ReviewInputModel input)
    {
      var currentReview = _replicaDbContext.CourseReviews.FirstOrDefault(x => x.Guid == input.Id);
      if (currentReview == null)
      {
        throw new BadRequestException(ErrorMessages.CourseReview_NotFound);
      }

      if (input.Status.HasValue)
      {
        using var transaction = _masterDbContext.Database.BeginTransaction();
        try
        {
          currentReview.Status = input.Status.Value;
          currentReview.UpdatedBy = input.UpdatedBy.GetValueOrDefault();
          currentReview.UpdatedDate = DateTime.UtcNow;

          _masterDbContext.CourseReviews.Update(currentReview);
          _masterDbContext.SaveChanges();

          transaction.Commit();
        }
        catch (Exception)
        {
          transaction.Rollback();
          throw;
        }
      }

      _openSearchEventPublisher.PublishMessageAsync(CoreConstants.OpenSearchEventType.CourseReview.Update,
        new List<Guid>() { currentReview.Guid });
      return currentReview.Guid;
    }

    public ReviewDetailModel GetDetail(Guid id)
    {
      var query = BuildQueryable(new ReviewFilterModel() { Id = id }, false);
      var totalCount = query.LongCount();
      if (totalCount < 1)
      {
        throw new BadRequestException(ErrorMessages.CourseReview_NotFound);
      }

      var data = query.Select(x => new ReviewListViewModel(x)).ToList();
      BuildResultData(data, false);

      return data.Select(x => new ReviewDetailModel()
      {
        Guid = x.Guid,
        CourseName = x.CourseName,
        InstructorNames = x.InstructorNames,
        WriteenBy = x.ReviewedBy,
        Status = x.Status
      }).FirstOrDefault();
    }

    #region Private Methods
    private IQueryable<CourseReviewEntity> BuildQueryable(ReviewFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.CourseReviews.AsNoTracking();
      query = CourseReviewQueryBuilder<CourseReviewEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    private void BuildResultData(List<ReviewListViewModel> data, bool useMasterDb)
    {
      SetCourseName(data, useMasterDb);
      SetInstructorNames(data, useMasterDb);
      SetReviewedBy(data, useMasterDb);
    }

    private void SetCourseName(List<ReviewListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var courseIds = data.Select(x => x.Guid).ToList();
      var mapData = dbContext.CourseReviews.Where(x => courseIds.Contains(x.Guid))
        .Join(dbContext.Courses,
          courseReview => courseReview.CourseId,
          course => course.Guid,
          (courseReview, course) => new
          {
            CourseReviewId = courseReview.Guid,
            CourseName = course.Name
          })
        .ToDictionary(x => x.CourseReviewId, x => x.CourseName);

      data.ForEach(x =>
      {
        x.CourseName = mapData.GetValueOrDefault(x.Guid);
      });
    }

    private void SetInstructorNames(List<ReviewListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var courseIds = data.Select(x => x.Guid).ToList();
      var mapData = dbContext.CourseReviews.Where(x => courseIds.Contains(x.Guid))
        .Join(dbContext.CourseInstructor,
          courseReview => courseReview.CourseId,
          courseInstructor => courseInstructor.CourseId,
          (courseReview, courseInstructor) => new
          {
            CourseReviewId = courseReview.Guid,
            InstructorId = courseInstructor.InstructorId
          })
        .Join(dbContext.Instructors,
          joinCourseReviewInstructor => joinCourseReviewInstructor.InstructorId,
          instructor => instructor.Guid,
          (joinCourseReviewInstructor, instructor) => new
          {
            CourseReviewId = joinCourseReviewInstructor.CourseReviewId,
            InstructorName = instructor.Name
          })
        .ToList().ToLookup(x => x.CourseReviewId, x => x.InstructorName);

      data.ForEach(x =>
      {
        var names = mapData.Where(y => y.Key == x.Guid).SelectMany(o => o).ToList();
        if (!names.Any()) return;
        x.InstructorNames = names;
      });
    }

    private void SetReviewedBy(List<ReviewListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var courseIds = data.Select(x => x.Guid).ToList();
      var mapData = dbContext.CourseReviews.Where(x => courseIds.Contains(x.Guid))
        .Join(dbContext.Users,
          courseReview => courseReview.CreatedBy,
          user => user.UserId,
          (courseReview, user) => new
          {
            CourseReviewId = courseReview.Guid,
            ReviewedBy = user.UserName
          })
        .ToDictionary(x => x.CourseReviewId, x => x.ReviewedBy);

      data.ForEach(x =>
      {
        x.ReviewedBy = mapData.GetValueOrDefault(x.Guid);
      });
    }

    #endregion
  }
}

﻿using Kempus.Core.Models;
using Kempus.Models.Public.Degree;

namespace Kempus.Services.Public.Degree
{
  public interface IDegreeService
  {
    PagingResult<DegreeListViewModel> Search(DegreeFilterModel filter, Pageable pageable,
      bool useMasterDb);
  }
}
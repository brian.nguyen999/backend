﻿using Kempus.Core.Models;
using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Degree;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;

namespace Kempus.Services.Public.Degree
{
  public class DegreeService : IDegreeService
  {
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly MasterDbContext _masterDbContext;

    public DegreeService(
      ReplicaDbContext replicaDbContext,
      MasterDbContext masterDbContext
      )
    {
      _replicaDbContext = replicaDbContext;
      _masterDbContext = masterDbContext;
    }

    public PagingResult<DegreeListViewModel> Search(DegreeFilterModel filter, Pageable pageable,
      bool useMasterDb)
    {
      var query = BuildQueryable(filter, useMasterDb);

      var totalCount = query.LongCount();
      if (totalCount < 1)
        return new PagingResult<DegreeListViewModel>(0, pageable.PageIndex, pageable.PageSize);

      var orderBy = QueryableUtils.GetOrderByFunction<DegreeEntity>(pageable.GetSortables(nameof(DegreeEntity.Name)));

      var data = orderBy(query).Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new DegreeListViewModel(x)).ToList();


      return new PagingResult<DegreeListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    #region Private methods

    private IQueryable<DegreeEntity> BuildQueryable(DegreeFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.Degrees.AsNoTracking();
      query = DegreeQueryBuilder<DegreeEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    #endregion
  }
}
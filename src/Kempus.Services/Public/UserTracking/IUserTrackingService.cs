﻿using Kempus.Models.Public.PostPollLike;
using Kempus.Models.Public.Tracking;

namespace Kempus.Services.Public.UserTracking
{
  public interface IUserTrackingService
  {
    void Create(UserTrackingInputModel model);
    void Create(ViewTrackingInputModel model);
    void SetNumberOfViews(List<IHasNumberOfViews> data, string objectType, bool useMasterDb);
    void SetNumberOfViews(IHasNumberOfViews data, string objectType, bool useMasterDb);
  }
}

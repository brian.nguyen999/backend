﻿using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.PostPollLike;
using Kempus.Models.Public.Tracking;

namespace Kempus.Services.Public.UserTracking
{
  public class UserTrackingService : IUserTrackingService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;

    public UserTrackingService(
      MasterDbContext masterDbContext,
      ReplicaDbContext replicaDbContext)
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
    }

    public void Create(UserTrackingInputModel model)
    {
      ValidateBeforeCreate(model);
      var trackingEntity = new UserTrackingEntity
      {
        ActionName = model.ActionName,
        UserId = model.UserId,
        Page = model.Page,
        Platform = model.Platform,
        ObjectId = model.ObjectId,
        ObjectType = model.ObjectType,
      };
      _masterDbContext.UserTrackings.Add(trackingEntity);
      _masterDbContext.SaveChanges();
    }

    public void Create(ViewTrackingInputModel model)
    {
      if (model.ObjectId == null && model.ObjectType == null)
        throw new BadRequestException(ErrorMessages.Invalid_Request);

      ValidateObject(model.ObjectId, model.ObjectType);
      var viewEntity = new ViewTrackingEntity
      {
        UserId = model.UserId.Value,
        ObjectId = model.ObjectId,
        ObjectType = model.ObjectType,
        CreatedDate = DateTime.UtcNow
      };
      _masterDbContext.ViewTrackings.Add(viewEntity);
      _masterDbContext.SaveChanges();
    }

    public void SetNumberOfViews(List<IHasNumberOfViews> data, string objectType, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var objectIds = data.Select(x => x.Id).ToList();
      var mapData = dbContext.ViewTrackings
        .Where(x => objectIds.Contains(x.ObjectId.Value) && x.ObjectType == objectType)
        .GroupBy(x => x.ObjectId)
        .Select(x => new
        {
          Id = x.Key,
          NumberOfViews = x.Count()
        })
        .ToDictionary(x => x.Id, x => x.NumberOfViews);

      data.ForEach(x =>
      {
        x.NumberOfView = mapData.GetValueOrDefault(x.Id);
      });
    }

    public void SetNumberOfViews(IHasNumberOfViews data, string objectType, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var postId = data.Id;
      var numberOfViews = dbContext.ViewTrackings
        .Count(x => x.ObjectId == postId && x.ObjectType == objectType);
      data.NumberOfView = numberOfViews;
    }

    #region Private methods

    public void ValidateBeforeCreate(UserTrackingInputModel model)
    {
      if (model == null) throw new BadRequestException(ErrorMessages.Invalid_Request);
      ValidatePlatform(model.Platform);
      ValidateAction(model.ActionName);
      ValidateObject(model.ObjectId, model.ObjectType);
    }

    public void ValidateObject(Guid? objectId, string objectType)
    {
      if (objectId != null && objectType != null)
      {
        switch (objectType)
        {
          case CoreConstants.UserTracking.ObjectType.Poll:
          case CoreConstants.UserTracking.ObjectType.Post:
          case CoreConstants.UserTracking.ObjectType.CourseReview:
            {
              break;
            }
          default: throw new BadRequestException(ErrorMessages.Invalid_Request);
        }
      }
    }

    public void ValidatePlatform(string platform)
    {
      switch (platform)
      {
        case CoreConstants.UserTracking.Platform.Web:
        case CoreConstants.UserTracking.Platform.Mobile:
          {
            break;
          }
        default: throw new BadRequestException(ErrorMessages.Invalid_Request);
      }
    }

    public void ValidateAction(string actionName)
    {
      switch (actionName)
      {
        case CoreConstants.UserTracking.ActionName.WritePost:
        case CoreConstants.UserTracking.ActionName.CreatePoll:
        case CoreConstants.UserTracking.ActionName.WriteCourseReview:
        case CoreConstants.UserTracking.ActionName.LikePost:
        case CoreConstants.UserTracking.ActionName.LikePoll:
        case CoreConstants.UserTracking.ActionName.LikeCourseReview:
        case CoreConstants.UserTracking.ActionName.LoginSuccess:
        case CoreConstants.UserTracking.ActionName.LoginFailed:
        case CoreConstants.UserTracking.ActionName.Logout:
        case CoreConstants.UserTracking.ActionName.Bookmark:
        case CoreConstants.UserTracking.ActionName.Report:
        case CoreConstants.UserTracking.ActionName.Comment:
        case CoreConstants.UserTracking.ActionName.LockedByAdmin:
          {
            break;
          }
        default:
          {
            throw new BadRequestException(ErrorMessages.Invalid_Request);
          }
      }
    }

    #endregion

  }
}

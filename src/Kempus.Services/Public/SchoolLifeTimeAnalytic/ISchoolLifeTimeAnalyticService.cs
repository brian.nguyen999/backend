﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Public.SchoolLifeTimeAnalytic
{
  public interface ISchoolLifeTimeAnalyticService
  {
    Guid Create(Guid schoolId);
    long UpdateNumberOfWaitList(Guid schoolId);
    int RefreshNumber(Guid schoolId);
  }
}

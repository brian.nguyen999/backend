﻿using Kempus.Entities;
using Kempus.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Public.SchoolLifeTimeAnalytic
{
  public class SchoolLifeTimeAnalyticService : ISchoolLifeTimeAnalyticService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;

    public SchoolLifeTimeAnalyticService(
      MasterDbContext masterDbContext, 
      ReplicaDbContext replicaDbContext
    )
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
    }

    public Guid Create(Guid schoolId)
    {
      var newSchoolAnalytic = new SchoolLifeTimeAnalyticEntity()
      {
        SchoolId = schoolId,
      };

      _masterDbContext.SchoolLifeTimeAnalytics.Add(newSchoolAnalytic);
      _masterDbContext.SaveChanges();

      return newSchoolAnalytic.Guid;
    }

    public long UpdateNumberOfWaitList(Guid schoolId)
    {
      var currentSchoolAnalytic = _replicaDbContext.SchoolLifeTimeAnalytics.Where(x => x.SchoolId == schoolId).FirstOrDefault();
      long numberWaitList = _replicaDbContext.WaitList.LongCount(x => x.SchoolId == schoolId);
      if (currentSchoolAnalytic == null)
      {
        var newSchoolAnalytic = new SchoolLifeTimeAnalyticEntity()
        {
          SchoolId = schoolId,
          NoOfWaitList = numberWaitList
        };

        _masterDbContext.SchoolLifeTimeAnalytics.Add(newSchoolAnalytic);
      }
      else
      {
        currentSchoolAnalytic.NoOfWaitList = numberWaitList;
        _masterDbContext.Update(currentSchoolAnalytic);
      }

      _masterDbContext.SaveChanges();

      return numberWaitList;
    }

    public int RefreshNumber(Guid schoolId)
    {
      var currentSchoolAnalytic = _replicaDbContext.SchoolLifeTimeAnalytics.Where(x => x.SchoolId == schoolId).FirstOrDefault();
      int numberUser = _replicaDbContext.Users.Count(x => x.SchoolId == schoolId);
      int numberWaitList = _replicaDbContext.WaitList.Count(x => x.SchoolId == schoolId);
      if (currentSchoolAnalytic == null)
      {
        var newSchoolAnalytic = new SchoolLifeTimeAnalyticEntity()
        {
          SchoolId = schoolId,
          Users = numberUser,
          NoOfWaitList= numberWaitList
        };

        _masterDbContext.SchoolLifeTimeAnalytics.Add(newSchoolAnalytic);
      }
      else
      {
        currentSchoolAnalytic.Users = numberUser;
        currentSchoolAnalytic.NoOfWaitList = numberWaitList;
        _masterDbContext.Update(currentSchoolAnalytic);
      }

      _masterDbContext.SaveChanges();

      return numberUser;
    }
  }
}

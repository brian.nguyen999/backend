﻿using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Flag;
using Kempus.Services.Common.KempTransaction;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.Public.Flag
{
  public class FlagService : IFlagService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly ILogger _logger;
    private readonly IKempTransactionCommonService _kempTransactionService;

    public FlagService(
      MasterDbContext masterDbContext,
      ReplicaDbContext replicaDbContext,
      ILogger<FlagService> logger,
      IKempTransactionCommonService kempTransactionService
    )
    {
      _masterDbContext = masterDbContext;
      _logger = logger;
      _replicaDbContext = replicaDbContext;
      _kempTransactionService = kempTransactionService;
    }

    public Guid Create(FlagInputModel input)
    {
      ValidateBeforeCreateFlag(input);
      try
      {
        FlagEntity newFlag = new FlagEntity()
        {
          ObjectId = input.ObjectId,
          ObjectType = input.ObjectType,
          ContentUrl = input.ContentUrl,
          Reason = input.Reason,
          Status = (byte)FlagStatus.Pending,
          Description = input.Description,
          SchoolId = input.SchoolId,
          CreatedBy = input.CreatedBy,
        };

        newFlag.FlaggedUser = GetFlaggedUserId(input);

        int result;
        using var transaction = _masterDbContext.Database.BeginTransaction();
        try
        {
          _masterDbContext.Flags.Add(newFlag);
          result = _masterDbContext.SaveChanges();
          transaction.Commit();
        }
        catch (Exception)
        {
          transaction.Rollback();
          throw;
        }

        if (result > 0)
        {
          int amount = 0;
          byte? transactionType = null;
          switch (newFlag.ObjectType)
          {
            case (byte)FlagType.User:
              amount = KempTransactionValue.FlagReportUser;
              transactionType = (byte)KempTransactionType.FlagReportUser;
              break;
            case (byte)FlagType.Review:
              amount = KempTransactionValue.FlagReportReview;
              transactionType = (byte)KempTransactionType.FlagReportReview;
              break;
            case (byte)FlagType.Post:
              amount = KempTransactionValue.FlagReportPost;
              transactionType = (byte)KempTransactionType.FlagReportPost;
              break;
            case (byte)FlagType.Poll:
              amount = KempTransactionValue.FlagReportPoll;
              transactionType = (byte)KempTransactionType.FlagReportPoll;
              break;
            case (byte)FlagType.Comment:
              amount = KempTransactionValue.FlagReportComment;
              transactionType = (byte)KempTransactionType.FlagReportComment;
              break;
          }

          if (transactionType.HasValue)
          {
            // Create FlagReportContent transaction. Deduct 1 Kemp
            _kempTransactionService.CreateTransaction(new Models.Common.KempTransactionInputModel()
            {
              UserIds = new List<Guid>() { input.CreatedBy },
              TransactionType = transactionType.Value,
              ReferenceGuid = newFlag.ObjectId,
              Amount = amount,
              Action = (byte)KempTransactionAction.Deduct,
              CreatorId = input.CreatedBy,
              SchoolId = input.SchoolId
            });
          }
        }

        return newFlag.Guid;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message);
        throw new BadRequestException(ex.Message);
      }
    }

    #region Private Methods
    public void ValidateBeforeCreateFlag(FlagInputModel input)
    {
      var currentWallet = _replicaDbContext.KempWallets.FirstOrDefault(x => x.UserId == input.CreatedBy);
      if (currentWallet == null || currentWallet.Amount < 1)
      {
        throw new BadRequestException(ErrorMessages.Flag_NotEnoughKemp);
      }
    }

    private Guid GetFlaggedUserId(FlagInputModel input)
    {
      Guid result = new Guid();
      switch (input.ObjectType)
      {
        case (byte)FlagType.User:
          result = input.ObjectId;
          break;
        case (byte)FlagType.Review:
          var currentCourseReview = _replicaDbContext.CourseReviews.Where(x => x.Guid == input.ObjectId).FirstOrDefault();
          if (currentCourseReview == null)
          {
            throw new BadRequestException(ErrorMessages.CourseReview_NotFound);
          }

          result = currentCourseReview.CreatedBy;
          break;
        case (byte)FlagType.Post:
          var currentPost = _replicaDbContext.Posts
            .Where(x => x.Guid == input.ObjectId && x.Type == (byte)PostType.Post)
            .FirstOrDefault();
          if (currentPost == null)
          {
            throw new BadRequestException(ErrorMessages.Post_NotFound);
          }

          result = currentPost.CreatedBy;
          break;
        case (byte)FlagType.Poll:
          var currentPoll = _replicaDbContext.Posts
            .Where(x => x.Guid == input.ObjectId && x.Type == (byte)PostType.Poll)
            .FirstOrDefault();
          if (currentPoll == null)
          {
            throw new BadRequestException(ErrorMessages.Poll_NotFound);
          }

          result = currentPoll.CreatedBy;
          break;
        case (byte)FlagType.Comment:
          var currentComment = _replicaDbContext.PostComments.Where(x => x.Guid == input.ObjectId).FirstOrDefault();
          if (currentComment == null)
          {
            throw new BadRequestException(ErrorMessages.Comment_NotFound);
          }

          result = currentComment.CreatedBy;
          break;
      }

      return result;
    }
    #endregion
  }
}

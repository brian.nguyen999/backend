﻿using Kempus.Models.Public.Flag;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Public.Flag
{
  public interface IFlagService
  {
    Guid Create(FlagInputModel input);
  }
}

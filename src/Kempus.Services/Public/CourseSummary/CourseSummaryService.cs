﻿using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.CourseReview;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.Public.CourseSummary
{
  public class CourseSummaryService : ICourseSummaryService
  {
    private readonly MasterDbContext _masterDbContext;
    public CourseSummaryService(MasterDbContext masterDbContext)
    {
      _masterDbContext = masterDbContext;
    }

    public Guid SetCourseSummary(Guid courseId)
    {
      var courseSummary = _masterDbContext.CourseSummaries.FirstOrDefault(x => x.CourseId == courseId);

      if (courseSummary == null)
      {
        var entity = new CourseSummaryEntity();
        SetDataSummary(entity, courseId);
        _masterDbContext.CourseSummaries.Add(entity);
        _masterDbContext.SaveChanges();
        return entity.Guid;
      }
      else
      {
        SetDataSummary(courseSummary, courseId);
        _masterDbContext.CourseSummaries.Update(courseSummary);
        _masterDbContext.SaveChanges();
        return courseSummary.Guid;
      }
    }

    private void SetDataSummary(CourseSummaryEntity entity, Guid courseId)
    {
      var courseReviews = _masterDbContext.CourseReviews.Where(x => x.CourseId == courseId).ToList();
      var totalReviews = courseReviews.Count();
      entity.CourseId = courseId;
      entity.TotalReviews = totalReviews;
      entity.IsTextbookRequired = courseReviews.Count(x => x.IsRequiredTextBook) >=
                           courseReviews.Count(x => !x.IsRequiredTextBook);
      entity.NumberOfExams = courseReviews.GroupBy(x => x.NumberOfExams).OrderByDescending(x => x.Count()).FirstOrDefault()
        .Key;
      entity.RecommendedRate = (double)courseReviews.Sum(x => x.RecommendRating) / totalReviews;
      entity.GradingCriteriaRate = (double)courseReviews.Sum(x => x.GradingCriteriaRating) / totalReviews;
      entity.CourseDifficultyRate = (double)courseReviews.Sum(x => x.DifficultRating) / totalReviews;
      entity.IsGroupProjects = courseReviews.Count(x => x.IsRequiredGroupProject) >=
                                courseReviews.Count(x => !x.IsRequiredGroupProject);
      entity.HoursPerWeekRate = Math.Round(courseReviews.Sum(x => x.HoursSpend) / totalReviews, 1);
      entity.GoalsAchivedRate = (double)courseReviews.Sum(x => x.UserAchievementRating) / totalReviews;
      entity.StudentGradePercents = JsonUtils.ToJson(SetStudentGradePercents(courseId));
      entity.TopThreeCharacteristicInstructors = JsonUtils.ToJson(SetTopThreeCharacteristicInstructors(courseReviews));
    }

    private List<GradePercent> SetStudentGradePercents(Guid courseId)
    {
      var gradePercents = Enum.GetValues(typeof(UserGrading)).Cast<UserGrading>().Select(x => new GradePercent
      {
        Grade = x.ToString()
      }).ToList();

      var courseGradeMap = _masterDbContext.CourseReviews.Where(x => x.CourseId == courseId).GroupBy(
          x => x.UserGrade,
          (key, g) => new { Grade = ((UserGrading)key).ToString(), Count = g.ToList().Count })
        .ToDictionary(x => x.Grade, x => x.Count);

      var total = courseGradeMap.Sum(x => x.Value);

      gradePercents.ForEach(x =>
      {
        var item = courseGradeMap.GetValueOrDefault(x.Grade);
        if (item != 0)
          x.Percent = (int)Math.Round(item * 100.0 / total);
      });

      return gradePercents;
    }

    private List<string> SetTopThreeCharacteristicInstructors(
      List<CourseReviewEntity> courseReviews)
    {
      var characteristicData = courseReviews.Select(x => x.Characteristics).ToList();
      var characteristicItems = new List<string>();
      foreach (var item in characteristicData)
      {
        if (!string.IsNullOrEmpty(item))
          characteristicItems.AddRange(JsonUtils.Convert<List<string>>(item));
      }

      return characteristicItems.GroupBy(x => x).OrderByDescending(x => x.Count()).Take(3)
        .Select(x => x.Key).ToList();
    }
  }
}

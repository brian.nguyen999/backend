﻿namespace Kempus.Services.Public.CourseSummary
{
  public interface ICourseSummaryService
  {
    Guid SetCourseSummary(Guid courseId);
  }
}

﻿using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Admin.Course;
using Kempus.Models.Common;
using Kempus.Models.FullTextSearch.Course;
using Kempus.Models.Public.Course;
using Kempus.Models.Public.DepartmentProgram;
using Kempus.Models.Public.Instructor;
using Kempus.Services.Common.Queue;
using Kempus.Services.FullTextSearch.Course;
using Kempus.Services.Public.DepartmentProgram;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;

namespace Kempus.Services.Public.Course
{
  public class CourseService : ICourseService
  {
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly MasterDbContext _masterDbContext;
    private readonly ICourseFtsService _courseFtsService;
    private readonly IOpenSearchEventPublisher _openSearchEventPublisher;
    private readonly IDepartmentProgramService _departmentProgramService;

    public CourseService(
      ReplicaDbContext replicaDbContext,
      MasterDbContext masterDbContext,
      ICourseFtsService courseFtsService,
      IOpenSearchEventPublisher openSearchEventPublisher,
      IDepartmentProgramService departmentProgramService
      )
    {
      _replicaDbContext = replicaDbContext;
      _masterDbContext = masterDbContext;
      _courseFtsService = courseFtsService;
      _openSearchEventPublisher = openSearchEventPublisher;
      _departmentProgramService = departmentProgramService;
    }

    public PagingResult<CourseListViewModel> Search(CourseFilterModel filter, Pageable pageable,
      bool useMasterDb, bool useFullTextSearch)
    {
      if (!useFullTextSearch || string.IsNullOrEmpty(filter.Keyword))
        return Search(filter, pageable, useMasterDb);
      var ftsFilterModel = new CourseFtsFilterModel
      {
        Keyword = filter?.Keyword?.Trim(),
        SchoolId = filter.SchoolId.GetValueOrDefault()
      };

      return Search(ftsFilterModel, pageable, useMasterDb);
    }

    public CourseDetailModel GetDetail(Guid id)
    {
      var course = _replicaDbContext.Courses
        .Select(x => new CourseDetailModel
        {
          Id = x.Guid,
          Name = x.Name,
          DepartmentId = x.DepartmentId
        })
        .FirstOrDefault(x => x.Id == id);
      if (course == null) return null;

      SetInstructors(course, false);
      _departmentProgramService.SetDepartment(course, false);
      return course;
    }

    public CourseDetailCmsModel GetDetailCms(Guid id)
    {
      var courseEntity = _replicaDbContext.Courses
        .FirstOrDefault(x => x.Guid == id);

      if (courseEntity == null)
        throw new BadRequestException(ErrorMessages.Course_NotFound);

      var courseDetail = new CourseDetailCmsModel(courseEntity);
      courseDetail.Instructors = _replicaDbContext.CourseInstructor.Where(x => x.CourseId == courseDetail.Id)
        .Join(_replicaDbContext.Instructors,
          courseInstructor => courseInstructor.InstructorId,
          instructor => instructor.Guid,
          (courseInstructor, instructor) => new { courseInstructor, instructor })
        .Select(x => new SelectModel()
        {
          Id = x.instructor.Guid,
          Text = x.instructor.Name
        }).ToList();
      courseDetail.Department.Text = _replicaDbContext.DepartmentPrograms.Where(x => x.Guid == courseDetail.Department.Id).Select(x => x.Name).FirstOrDefault();

      return courseDetail;
    }

    #region CMS
    public PagingResult<CmsCourseListViewModel> CmsSearch(CmsCourseFilterModel filter, Pageable pageable,
      bool useMasterDb)
    {
      if (!filter.SchoolId.HasValue)
        return new PagingResult<CmsCourseListViewModel>(0, pageable.PageIndex, pageable.PageSize);

      var query = BuildQueryable(filter, useMasterDb);

      var totalCount = query.LongCount();
      if (totalCount < 1)
        return new PagingResult<CmsCourseListViewModel>(0, pageable.PageIndex, pageable.PageSize);

      var data = query.Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new CmsCourseListViewModel(x)).ToList();

      BuildResultData(data, useMasterDb);

      return new PagingResult<CmsCourseListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }
    #endregion

    #region Private Methods
    private void BuildResultData(List<CourseListViewModel> data, bool useMasterDb)
    {
      SetInstructorNames(data, useMasterDb);
      SetCourseSummary(data, useMasterDb);
      _departmentProgramService.SetDepartments(data.ToList<IHasDepartment>(), useMasterDb);
    }
    private void BuildResultData(List<CmsCourseListViewModel> data, bool useMasterDb)
    {
      SetInstructorNames(data, useMasterDb);
      SetCourseRelationshipInformation(data, useMasterDb);
    }
    private IQueryable<CourseEntity> BuildQueryable(CourseFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.Courses.AsNoTracking();
      query = CourseQueryBuilder<CourseEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }
    private IQueryable<CourseEntity> BuildQueryable(CmsCourseFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.Courses.AsNoTracking();
      query = CourseQueryBuilder<CourseEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    private void SetInstructorNames(List<CourseListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var courseIds = data.Select(x => x.Id).ToList();
      var mapData = dbContext.CourseInstructor.Where(x => courseIds.Contains(x.CourseId))
        .Join(dbContext.Instructors,
        courseInstructor => courseInstructor.InstructorId,
        instructor => instructor.Guid,
        (courseInstructor, instructor) => new
        {
          courseInstructor.CourseId,
          instructor.Name
        }).ToList().ToLookup(x => x.CourseId, x => x.Name);

      data.ForEach(x =>
      {
        var names = mapData.Where(y => y.Key == x.Id).SelectMany(o => o).ToList();
        if (!names.Any()) return;
        x.InstructorNames = names;
      });
    }
    private void SetInstructorNames(List<CmsCourseListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var courseIds = data.Select(x => x.Guid).ToList();
      var mapData = dbContext.CourseInstructor.Where(x => courseIds.Contains(x.CourseId))
        .Join(dbContext.Instructors,
        courseInstructor => courseInstructor.InstructorId,
        instructor => instructor.Guid,
        (courseInstructor, instructor) => new
        {
          courseInstructor.CourseId,
          instructor.Name
        }).ToList().ToLookup(x => x.CourseId, x => x.Name);

      data.ForEach(x =>
      {
        var names = mapData.Where(y => y.Key == x.Guid).SelectMany(o => o).ToList();
        if (!names.Any()) return;
        x.InstructorNames = names;
      });
    }

    private void SetCourseSummary(List<CourseListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var courseIds = data.Select(x => x.Id).ToList();
      var mapData = dbContext.CourseSummaries.Where(x => courseIds.Contains(x.CourseId)).ToDictionary(x => x.CourseId, x => x);

      data.ForEach(x =>
      {
        var item = mapData.GetValueOrDefault(x.Id);
        if (item == null) return;
        x.TotalReviews = item.TotalReviews;
        x.IsRequiredTextbook = item.IsTextbookRequired;
        x.IsRequiredGroupProjects = item.IsGroupProjects;
        x.NumberOfExams = item.NumberOfExams;
        x.RecommendedRate = item.RecommendedRate;
        x.GradingCriteriaRate = item.GradingCriteriaRate;
        x.CourseDifficultyRate = item.CourseDifficultyRate;
      });
    }
    private void SetCourseRelationshipInformation(List<CmsCourseListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var courseIds = data.Select(x => x.Guid).ToList();
      var mapCourseReviewData = dbContext.Courses.Where(x => courseIds.Contains(x.Guid))
        .Join(dbContext.CourseReviews,
          course => course.Guid,
          courseReview => courseReview.CourseId,
        (course, courseReview) => new { Guid = course.Guid, courseReview })
        .GroupBy(x => x.Guid,
          (key, data) => new
          {
            Guid = key,
            NumberOfReview = data.LongCount()
          })
        .ToDictionary(x => x.Guid, x => x.NumberOfReview);

      var mapDegreeData = dbContext.Courses.Where(x => courseIds.Contains(x.Guid))
        .Join(dbContext.Degrees,
        course => course.DegreeId,
        degree => degree.Guid,
        (course, degree) => new { course, degree })
        .ToDictionary(x => x.course.Guid, x => x.degree.Name);

      var mapDepartmentProgram = dbContext.Courses.Where(x => courseIds.Contains(x.Guid))
        .Join(dbContext.DepartmentPrograms,
        course => course.DepartmentId,
        department => department.Guid,
        (course, department) => new { course, department })
        .ToDictionary(x => x.course.Guid, x => x.department.Name);

      data.ForEach(x =>
      {
        x.NumberOfReviews = mapCourseReviewData.GetValueOrDefault(x.Guid);
        x.DeptProgram = mapDepartmentProgram.GetValueOrDefault(x.Guid);
        x.Degree = mapDegreeData.GetValueOrDefault(x.Guid);
      });
    }

    private void SetInstructors(CourseDetailModel data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var mapData = dbContext.CourseInstructor
        .Where(x => x.CourseId == data.Id)
        .Join(dbContext.Instructors,
          courseInstructor => courseInstructor.InstructorId,
          instructor => instructor.Guid,
          (courseInstructor, instructor) => new
          {
            CourseId = courseInstructor.CourseId,
            Instructor = new InstructorDetailModel
            {
              Id = instructor.Guid,
              Name = instructor.Name,
            }
          })
        .ToList()
        .ToLookup(x => x.CourseId, x => x.Instructor);

      data.Instructors = mapData
        .Where(q => q.Key == data.Id)
        .SelectMany(o => o)
        .ToList();
    }

    public PagingResult<CourseListViewModel> Search(CourseFtsFilterModel filter, Pageable pageable, bool useMasterDb)
    {
      var ftsResult = _courseFtsService.SearchDocumentAsync(filter, pageable).Result;
      if (ftsResult.TotalCount < 1 || ftsResult.TotalPages < pageable.PageIndex)
        return new PagingResult<CourseListViewModel>(ftsResult.TotalCount, ftsResult.PageIndex, ftsResult.PageSize);
      var courseIds = ftsResult.Data.Select(x => x.Id).ToList();
      var courseFilterModel = new CourseFilterModel
      {
        CourseIds = courseIds,
        SchoolId = filter.SchoolId
      };

      var query = BuildQueryable(courseFilterModel, useMasterDb);
      var orderBy = QueryableUtils.GetOrderByFunction<CourseEntity>(pageable.GetSortables(nameof(CourseEntity.CreatedDate)));
      var data = orderBy(query).Select(x => new CourseListViewModel(x)).ToList();
      BuildResultData(data, useMasterDb);

      if (data.Any())
        _openSearchEventPublisher.PublishMessageAsync(CoreConstants.OpenSearchEventType.Course.Update,
          data.Select(x => x.Id).Distinct().ToList());
      return new PagingResult<CourseListViewModel>(data, ftsResult.TotalCount, ftsResult.PageIndex, ftsResult.PageSize);
    }

    public PagingResult<CourseListViewModel> Search(CourseFilterModel filter, Pageable pageable, bool useMasterDb)
    {
      var query = BuildQueryable(filter, useMasterDb);
      var totalCount = query.LongCount();
      if (totalCount < 1)
        return new PagingResult<CourseListViewModel>(0, pageable.PageIndex, pageable.PageSize);

      var orderBy = QueryableUtils.GetOrderByFunction<CourseEntity>(pageable.GetSortables(nameof(CourseEntity.CreatedDate)));
      var data = orderBy(query).Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new CourseListViewModel(x)).ToList();
      BuildResultData(data, useMasterDb);

      if (data.Any())
        _openSearchEventPublisher.PublishMessageAsync(CoreConstants.OpenSearchEventType.Course.Update,
          data.Select(x => x.Id).Distinct().ToList());
      return new PagingResult<CourseListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    #endregion
  }
}
﻿using Kempus.Core.Models;
using Kempus.Models.Admin.Course;
using Kempus.Models.Public.Course;

namespace Kempus.Services.Public.Course
{
  public interface ICourseService
  {
    PagingResult<CourseListViewModel> Search(CourseFilterModel filterModel, Pageable requestModel,
      bool useMasterDb, bool useFullTextSearch);
    CourseDetailModel GetDetail(Guid id);

    PagingResult<CmsCourseListViewModel> CmsSearch(CmsCourseFilterModel filter, Pageable pageable,
      bool useMasterDb);
    CourseDetailCmsModel GetDetailCms(Guid id);
  }
}
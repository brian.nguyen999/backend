﻿using Kempus.Core.Models;
using Kempus.Entities;
using Kempus.Models.Public.BellNotification;

namespace Kempus.Services.Public.BellNotification
{
  public interface IBellNotificationService
  {
    PagingResult<BellNotificationListViewModel> Search(BellNotificationFilterModel filterModel, Guid userId, Pageable pageable, bool useMasterDb);
    BellNotificationCreateModel SaveNotifyObjectChange(BellNotificationInputModel model);
    void MarkAsRead(Guid bellNotificationUserId);
    void MarkAllAsRead(Guid userId);
    void UpdateLastViewTime(Guid userId);
    bool CheckNewNotificationFromLastView(Guid userId);
    void SendBellAndPushMobileNotificationByGroupSchool(List<Guid> schoolIds);
    void SendBellAndPushMobileNotificationByGroupUser(List<Guid> userIds);
  }
}

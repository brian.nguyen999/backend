﻿using Kempus.Core.Errors;
using Kempus.Core.Extensions;
using Kempus.Core.Models;
using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Common.WebSocket;
using Kempus.Models.Public.BellNotification;
using Kempus.Services.Common.WebSocket;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;
using static Kempus.Core.Constants.CoreConstants;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.Public.BellNotification
{
  public class BellNotificationService : IBellNotificationService
  {
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly MasterDbContext _masterDbContext;
    private readonly IWebSocketService _webSocketService;

    public BellNotificationService(
      ReplicaDbContext replicaDbContext, 
      MasterDbContext masterDbContext, 
      IWebSocketService webSocketService
    )
    {
      _replicaDbContext = replicaDbContext;
      _masterDbContext = masterDbContext;
      _webSocketService = webSocketService;
    }

    public PagingResult<BellNotificationListViewModel> Search(BellNotificationFilterModel filterModel, Guid userId, Pageable pageable, bool useMasterDb)
    {
      var query = BuildQueryable(filterModel, userId, useMasterDb);

      var totalCount = query.LongCount();
      if (totalCount < 1)
      {
        return new PagingResult<BellNotificationListViewModel>(0, pageable.PageIndex, pageable.PageSize);
      }

      var orderBy = QueryableUtils.GetOrderByFunction<BellNotificationEntity>(pageable.GetSortables(nameof(BellNotificationEntity.CreatedDate)));

      var data = orderBy(query).Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new BellNotificationListViewModel(x)).ToList();

      BuildResultData(data, userId, useMasterDb);

      return new PagingResult<BellNotificationListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    public BellNotificationCreateModel SaveNotifyObjectChange(BellNotificationInputModel model)
    {
      var sendToUserIds = new List<Guid>();

      if (model.BellNotificationType == (byte)BellNotificationType.Site)
      {
        if (model.ObjectTypeId == ObjectTypeId.Post)
        {
          var post = _replicaDbContext.Posts.FirstOrDefault(x => x.Guid == model.ObjectId && x.Type == (byte)PostType.Post);
          if (post == null)
            throw new NotFoundException(ErrorMessages.Post_NotFound);
          sendToUserIds.Add(post.CreatedBy);
        }

        if (model.ObjectTypeId == ObjectTypeId.Poll)
        {
          var post = _replicaDbContext.Posts.FirstOrDefault(x => x.Guid == model.ObjectId && x.Type == (byte)PostType.Poll);
          if (post == null)
            throw new NotFoundException(ErrorMessages.Poll_NotFound);
          sendToUserIds.Add(post.CreatedBy);
        }
      }

      if (model.BellNotificationType == (byte)BellNotificationType.CMS)
      {
        if (model.SchoolIds.Any())
        {
          var userIds = _replicaDbContext.Users.Where(x => model.SchoolIds.Contains(x.SchoolId)).Select(x => x.UserId).ToList();
          if (userIds.Any())
            sendToUserIds.AddRange(userIds);
        }
      }

      if (!sendToUserIds.Any())
        throw new BadRequestException(ErrorMessages.BellNotification_NotFoundUserToSend);

      BellNotificationEntity bellNotification = new BellNotificationEntity();

      // Create in Site and CMS
      if (model.UserAction == UserAction.Create)
      {
        bellNotification = new BellNotificationEntity
        {
          Title = model.Title,
          Description = model.Description,
          UrlLink = model.UrlLink,
          CreatedDate = model.CreatedDate.GetValueOrDefault(),
          CreatedBy = model.CreatedUser,
          ObjectId = model.ObjectId,
          ObjectTypeId = model.ObjectTypeId,
          Type = model.BellNotificationType,
          Status = model.BellNotificationStatus,
          SchoolIds = JsonUtils.ToJson(model.SchoolIds),
          ScheduledDate = model.ScheduledDate,
        };

        _masterDbContext.BellNotifications.Add(bellNotification);
        _masterDbContext.SaveChanges();
      }

      // Update only on CMS
      if (model.UserAction == UserAction.Update)
      {
        bellNotification = _masterDbContext.BellNotifications.FirstOrDefault(x => x.Guid == model.Id);
        bellNotification.Title = model.Title;
        bellNotification.UrlLink = model.UrlLink;
        bellNotification.UpdatedDate = model.UpdatedDate;
        bellNotification.UpdatedBy = model.UpdatedUser.GetValueOrDefault();
        bellNotification.Type = model.BellNotificationType;
        bellNotification.Status = model.BellNotificationStatus;
        bellNotification.SchoolIds = JsonUtils.ToJson(model.SchoolIds);
        bellNotification.ScheduledDate = model.ScheduledDate;

        _masterDbContext.BellNotifications.Update(bellNotification);
        _masterDbContext.SaveChanges();

        var deleteBellNotiticationUsers = _masterDbContext.BellNotificationUsers.Where(x => x.BellNotificationId == model.Id).ToList();
        _masterDbContext.BellNotificationUsers.RemoveRange(deleteBellNotiticationUsers);
        _masterDbContext.SaveChanges();
      }

      var bellNotificationUsers = sendToUserIds.Select(userId =>
     new BellNotificationUserEntity
     {
       BellNotificationId = bellNotification.Guid,
       CreatedUser = userId
     })
   .ToList();

      _masterDbContext.BellNotificationUsers.AddRange(bellNotificationUsers);
      _masterDbContext.SaveChanges();

      return new BellNotificationCreateModel
      {
        Id = bellNotification.Guid,
        Title = bellNotification.Title,
        Description = bellNotification.Description,
        ObjectTypeId = bellNotification.ObjectTypeId,
        CreatedUser = model.CreatedUser,
        UserIds = sendToUserIds
      };
    }

    public void MarkAsRead(Guid bellNotificationUserId)
    {
      var bellNotificationUser = _masterDbContext.BellNotificationUsers.FirstOrDefault(x => x.Guid == bellNotificationUserId);
      if (bellNotificationUser == null)
        throw new NotFoundException(ErrorMessages.BellNotificationUser_NotFound);
      bellNotificationUser.IsRead = true;
      bellNotificationUser.ReadDate = DateTime.UtcNow;
      _masterDbContext.BellNotificationUsers.Update(bellNotificationUser);
      _masterDbContext.SaveChanges();
    }

    public void MarkAllAsRead(Guid userId)
    {
      var user = _replicaDbContext.Users.FirstOrDefault(x => x.UserId == userId);
      if (user == null)
        throw new NotFoundException(ErrorMessages.User_NotFound);

      var unReadBellNotificationUsers = _masterDbContext.BellNotificationUsers.Where(x => x.CreatedUser == userId && !x.IsRead).ToList();
      if (!unReadBellNotificationUsers.Any()) return;

      unReadBellNotificationUsers.ForEach(x =>
      {
        x.IsRead = true;
        x.ReadDate = DateTime.UtcNow;
      });

      _masterDbContext.BellNotificationUsers.UpdateRange(unReadBellNotificationUsers);
      _masterDbContext.SaveChanges();
    }

    public void UpdateLastViewTime(Guid userId)
    {
      var user = _replicaDbContext.Users.FirstOrDefault(x => x.UserId == userId);
      if (user == null)
        throw new NotFoundException(ErrorMessages.User_NotFound);

      var bellNotificationLastView = _masterDbContext.BellNotificationLastViews.FirstOrDefault(x => x.CreatedUser == userId);
      if (bellNotificationLastView == null)
      {
        bellNotificationLastView = new BellNotificationLastViewEntity { CreatedUser = userId, LastViewDateTime = DateTime.UtcNow };
        _masterDbContext.BellNotificationLastViews.Add(bellNotificationLastView);
      }
      else
      {
        bellNotificationLastView.LastViewDateTime = DateTime.UtcNow;
        _masterDbContext.BellNotificationLastViews.Update(bellNotificationLastView);
      }

      _masterDbContext.SaveChanges();
    }

    public bool CheckNewNotificationFromLastView(Guid userId)
    {
      var user = _replicaDbContext.Users.FirstOrDefault(x => x.UserId == userId);
      if (user == null)
        throw new NotFoundException(ErrorMessages.User_NotFound);

      return _replicaDbContext.BellNotificationLastViews.Where(x => x.CreatedUser == userId)
        .Join(_replicaDbContext.BellNotificationUsers,
        bellLastView => bellLastView.CreatedUser,
        bellUser => bellUser.CreatedUser,
        (bellLastView, bellUser) => new { bellLastView, bellUser })
        .Join(_replicaDbContext.BellNotifications.Where(x => x.CreatedBy != userId && x.Status != (byte)BellNotificationStatus.Scheduled),
        bellLastViewUser => bellLastViewUser.bellUser.BellNotificationId,
        bell => bell.Guid,
        (bellLastViewUser, bell) => new { bellLastViewUser, bell })
        .Any(x => (x.bell.CreatedDate > x.bellLastViewUser.bellLastView.LastViewDateTime) || (x.bell.Type == (byte)BellNotificationType.CMS && x.bell.UpdatedDate != null && x.bell.UpdatedDate > x.bellLastViewUser.bellLastView.LastViewDateTime));
    }

    public void SendBellAndPushMobileNotificationByGroupSchool(List<Guid> schoolIds)
    {
      if (!schoolIds.Any()) return;

      var connectionIds = new List<string>();
      foreach (var schoolId in schoolIds.Distinct())
      {
        connectionIds.AddRange(_webSocketService.GetConnectionIdsBySchoolIdAsync(schoolId).Result);
      }

      if (connectionIds.Any())
      {
        NotificationInputModel notificationInput = new NotificationInputModel()
        {
          ConnectionIds = connectionIds.Distinct().ToList(),
          Action = EnumExtensions.GetName<WebsocketAction>((byte)WebsocketAction.NewNotification),
          Payload = "New Notification"
        };
        _webSocketService.SendNotification(notificationInput).GetAwaiter();
      }
    }

    public void SendBellAndPushMobileNotificationByGroupUser(List<Guid> userIds)
    {
      if (!userIds.Any()) return;

      var connectionIds = new List<string>();
      foreach (var userId in userIds.Distinct())
      {
        connectionIds.AddRange(_webSocketService.GetConnectionIdsByUserIdAsync(userId).Result);
      }

      if(connectionIds.Any())
      {
        NotificationInputModel notificationInput = new NotificationInputModel()
        {
          ConnectionIds = connectionIds.Distinct().ToList(),
          Action = EnumExtensions.GetName<WebsocketAction>((byte)WebsocketAction.NewNotification),
          Payload = "New Notification"
        };
        _webSocketService.SendNotification(notificationInput).GetAwaiter();
      }
    }

    #region Private methods
    private void BuildResultData(List<BellNotificationListViewModel> data, Guid userId, bool useMasterDb)
    {
      SetBellNotificationUser(data, userId, useMasterDb);
      SetNickname(data, useMasterDb);
      SetContent(data, useMasterDb);
    }

    private void SetBellNotificationUser(List<BellNotificationListViewModel> data, Guid userId, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var bellNotificationIds = data.Select(x => x.Id).ToList();
      var mapData = dbContext.BellNotificationUsers.Where(x => bellNotificationIds.Contains(x.BellNotificationId) && x.CreatedUser == userId).
        Join(dbContext.Users,
        bellNotificationUser => bellNotificationUser.CreatedUser,
        user => user.UserId,
        (bellNotificationUser, user) => new
        {
          BellNotificationId = bellNotificationUser.BellNotificationId,
          BellNotificationUserId = bellNotificationUser.Guid,
          IsRead = bellNotificationUser.IsRead,
        }).ToDictionary(x => x.BellNotificationId, x => new
        {
          BellNotificationUserId = x.BellNotificationUserId,
          IsRead = x.IsRead,
        });

      data.ForEach(x =>
      {
        var item = mapData.GetValueOrDefault(x.Id);
        if (item == null) return;
        x.BellNotificationUserId = item.BellNotificationUserId;
        x.IsRead = item.IsRead;
      });
    }

    private void SetNickname(List<BellNotificationListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var siteBellNotifications = data.Where(x => x.BellNotificationType == (byte)BellNotificationType.Site).ToList();
      var bellNotificationIds = siteBellNotifications.Select(x => x.Id).ToList();
      var mapData = dbContext.BellNotifications.Where(x => bellNotificationIds.Contains(x.Guid))
        .Join(dbContext.Users,
        bellNotification => bellNotification.CreatedBy,
        user => user.UserId,
        (bellNotification, user) => new
        {
          BellNotificationId = bellNotification.Guid,
          NickName = user.NickName
        }).ToDictionary(x => x.BellNotificationId, x => x.NickName);

      siteBellNotifications.ForEach(x =>
      {
        var nickName = mapData.GetValueOrDefault(x.Id);
        if (nickName == null) return;
        x.NickName = nickName;
      });
    }

    private void SetContent(List<BellNotificationListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var postPollIds = data.Where(x => x.BellNotificationType == (byte)BellNotificationType.Site && (x.ObjectTypeId == ObjectTypeId.Post || x.ObjectTypeId == ObjectTypeId.Poll)).Select(x => x.ObjectId).ToList();

      var mapData = dbContext.Posts.Where(x => postPollIds.Contains(x.Guid)).ToDictionary(x => x.Guid, x => x.Title);

      data.ForEach(x =>
      {
        var title = mapData.GetValueOrDefault(x.ObjectId.Value);
        if (title == null) return;
        x.Content = title;
      });
    }

    private IQueryable<BellNotificationEntity> BuildQueryable(BellNotificationFilterModel filterModel, Guid userId, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.BellNotifications.AsNoTracking().Where(x => x.CreatedBy != userId && x.Status != (byte)BellNotificationStatus.Scheduled)
        .Join(dbContext.BellNotificationUsers.AsNoTracking().Where(x => x.CreatedUser == userId),
        bellNotification => bellNotification.Guid,
        bellNotificationUser => bellNotificationUser.BellNotificationId,
        (bellNotification, bellNotificationUser) => bellNotification);

      query = BellNotificationQueryBuilder<BellNotificationEntity>.BuildWhereClause(filterModel, query, dbContext);
      return query;
    }
    #endregion
  }
}

﻿using Kempus.Models.Public.Poll;
using Kempus.Models.Public.Vote;

namespace Kempus.Services.Public.Vote
{
  public interface IVoteService
  {
    PollListViewModel Create(VoteInputModel model);
    bool CheckVoted(Guid creatorId, Guid pollId);
    void SetVote(List<IHasVote> data, Guid userId, bool useMasterDb);
    void SetVote(IHasVote data, Guid userId, bool useMasterDb);
  }
}

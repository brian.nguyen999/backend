﻿using Kempus.Core.Errors;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Poll;
using Kempus.Models.Public.Vote;
using Kempus.Services.Public.Poll;
using Kempus.Services.Public.Statistics;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.Public.Vote
{
  public class VoteService : IVoteService
  {
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly MasterDbContext _masterDbContext;
    private readonly IStatisticsService _statisticsService;
    private readonly Lazy<IPollService> _pollService;

    public VoteService(
      ReplicaDbContext replicaDbContext,
      MasterDbContext masterDbContext,
      IStatisticsService statisticsService,
      Lazy<IPollService> pollService)
    {
      _replicaDbContext = replicaDbContext;
      _masterDbContext = masterDbContext;
      _statisticsService = statisticsService;
      _pollService = pollService;
    }

    public PollListViewModel Create(VoteInputModel model)
    {
      ValidateBeforeCreateOrUpdate(model);

      var transaction = _masterDbContext.Database.BeginTransaction();
      try
      {
        if (!CheckVoted(model.CreatorId, model.PollId))
        {
          var poll = _replicaDbContext.Polls.FirstOrDefault(x => x.PostId == model.PollId);
          var VoteUser = new VoteUserEntity
          {
            CreatedUser = model.CreatorId,
            PollId = poll.Guid
          };
          _masterDbContext.VoteUsers.Add(VoteUser);
          _masterDbContext.SaveChanges();

          var votes = new List<VoteEntity>();
          foreach (var item in model.PollOptionIds)
          {
            votes.Add(new VoteEntity
            {
              VoteUserId = VoteUser.Guid,
              PollOptionId = item
            });
          }
          _masterDbContext.Votes.AddRange(votes);
          _masterDbContext.SaveChanges();

          _statisticsService.AddVote(model.CreatorId, poll.Guid);

          transaction.Commit();
        }

        var pollListViewModels = new List<PollListViewModel>();

        var pollListViewModel = _masterDbContext.Posts.Where(x => x.Guid == model.PollId).Select(x => new PollListViewModel(x)).FirstOrDefault();
        if (pollListViewModel != null)
          pollListViewModels.Add(pollListViewModel);

        _pollService.Value.BuildResultData(pollListViewModels, model.CreatorId, true);

        return pollListViewModels.FirstOrDefault();
      }
      catch
      {
        transaction.Rollback();
        throw;
      }
    }

    public bool CheckVoted(Guid creatorId, Guid pollId)
    {
      var poll = _replicaDbContext.Polls.FirstOrDefault(x => x.PostId == pollId);
      return _replicaDbContext.VoteUsers.Any(x => x.CreatedUser == creatorId && x.PollId == poll.Guid);
    }

    private void ValidateBeforeCreateOrUpdate(VoteInputModel model)
    {
      var user = _replicaDbContext.Users.FirstOrDefault(x => x.UserId == model.CreatorId);
      if (user == null)
        throw new NotFoundException(ErrorMessages.User_NotFound);

      var poll = _replicaDbContext.Polls.FirstOrDefault(x => x.PostId == model.PollId);
      if (poll == null)
        throw new NotFoundException(ErrorMessages.Poll_NotFound);

      var pollType = poll.Type;
      if (pollType == (int)PollType.SCQ && model.PollOptionIds.Count > 1)
        throw new BadRequestException(ErrorMessages.Poll_SingleChoiceCannotHaveMultipleOptions);
    }

    public void SetVote(List<IHasVote> data, Guid userId, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var objectIds = data.Select(x => x.PollId);
      var voteList = dbContext.VoteUsers
        .Where(x => objectIds.Contains(x.PollId) && x.CreatedUser == userId)
        .Select(x => x.PollId)
        .ToList();
      if (voteList.Any())
      {
        data.ForEach(x =>
        {
          x.IsVoted = voteList.Any(q => q == x.PollId);
        });
      }
    }

    public void SetVote(IHasVote data, Guid userId, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var objectId = data.PollId;
      var vote = dbContext.VoteUsers
        .FirstOrDefault(x => x.PollId == data.PollId && x.CreatedUser == userId);
      if (vote != null)
      {
        data.IsVoted = true;
      }
    }
  }
}

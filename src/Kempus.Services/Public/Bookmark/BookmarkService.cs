﻿using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Bookmark;
using Kempus.Models.Public.Poll;
using Kempus.Models.Public.Post;
using Kempus.Services.Public.Poll;
using Kempus.Services.Public.Post;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.Public.Bookmark
{
  public class BookmarkService : IBookmarkService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly Lazy<IPostService> _postService;
    private readonly Lazy<IPollService> _pollService;


    public BookmarkService(
      MasterDbContext masterDbContext,
      ReplicaDbContext replicaDbContext,
      Lazy<IPostService> postService,
      Lazy<IPollService> pollService
      )
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
      _postService = postService;
      _pollService = pollService;
    }

    public PagingResult<BookmarkListViewModel> Search(BookmarkFilterModel filterModel, Pageable requestModel, bool useMasterDb)
    {
      var query = BuildQueryable(filterModel, useMasterDb);

      var totalCount = query.LongCount();
      if (totalCount < 1)
        return new PagingResult<BookmarkListViewModel>(0, requestModel.PageIndex, requestModel.PageSize);

      var orderBy = QueryableUtils.GetOrderByFunction<BookmarkEntity>(requestModel.GetSortables(nameof(BookmarkEntity.CreatedDate)));

      var data = orderBy(query).Skip(requestModel.Offset).Take(requestModel.PageSize).Select(x => new BookmarkListViewModel(x)).ToList();

      BuildResultData(data, useMasterDb, filterModel.UserId);

      return new PagingResult<BookmarkListViewModel>(data, totalCount, requestModel.PageIndex, requestModel.PageSize);
    }

    public Guid CreateIfNotExist(BookmarkInputModel input)
    {
      ValidateBeforeInsert(input);

      var bookmark = _replicaDbContext.Bookmarks.FirstOrDefault(x => x.ObjectId == input.ObjectId &&
      x.ObjectType == (byte)input.ObjectType && x.UserId == input.UserId);

      if (bookmark == null)
      {
        bookmark = new BookmarkEntity
        {
          Guid = Guid.NewGuid(),
          UserId = input.UserId,
          ObjectId = input.ObjectId,
          ObjectType = (byte)input.ObjectType,
          CreatedDate = DateTime.UtcNow
        };

        _masterDbContext.Add(bookmark);
        _masterDbContext.SaveChanges();
      }

      return bookmark.Guid;
    }

    public void Delete(Guid objectId, BookmarkObjectType objectType, Guid userId)
    {
      var bookmark = _replicaDbContext.Bookmarks.FirstOrDefault(x =>
         x.ObjectId == objectId && x.ObjectType == (byte)objectType && x.UserId == userId);
      if (bookmark != null)
      {
        _masterDbContext.Bookmarks.Remove(bookmark);
        _masterDbContext.SaveChanges();
      }
    }

    public void SetBookmarks(List<IHasBookmark> data, Guid userId, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var objectIds = data.Select(x => x.PostId);
      var bookmarkList = dbContext.Bookmarks
        .Where(x => objectIds.Contains(x.ObjectId) && x.UserId == userId)
        .Select(x => x.ObjectId)
        .ToList();
      if (bookmarkList.Any())
      {
        data.ForEach(x =>
        {
          x.IsBookmarked = bookmarkList.Any(q => q == x.PostId);
        });
      }
    }

    public void SetBookmark(IHasBookmark data, Guid userId, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var objectId = data.PostId;
      var bookmark = dbContext.Bookmarks
        .FirstOrDefault(x => objectId == x.ObjectId && x.UserId == userId);
      if (bookmark != null)
      {
        data.IsBookmarked = true;
      }
    }

    #region Private methods

    private IQueryable<BookmarkEntity> BuildQueryable(BookmarkFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.Bookmarks.AsNoTracking();
      query = BookmarkQueryBuilder<BookmarkEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    private void BuildResultData(List<BookmarkListViewModel> data, bool useMasterDb, Guid? userId)
    {
      SetPost(data, useMasterDb, userId);
      SetPoll(data, useMasterDb, userId);
    }

    private void SetPost(List<BookmarkListViewModel> data, bool useMasterDb, Guid? userId)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var postIds = data.Where(x => x.ObjectType == (byte)BookmarkObjectType.Post).Select(x => x.ObjectId).ToList();
      var mapData = _postService.Value.Search(new PostFilterModel { PostIds = postIds }, new Pageable(), useMasterDb, userId).Data.ToDictionary(x => x.Id, x => x);
      data.ForEach(x =>
      {
        var item = mapData.GetValueOrDefault(x.ObjectId);
        if (item == null) return;
        x.ObjectModel = item;
      });
    }

    private void SetPoll(List<BookmarkListViewModel> data, bool useMasterDb, Guid? userId)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var postIds = data.Where(x => x.ObjectType == (byte)BookmarkObjectType.Poll).Select(x => x.ObjectId).ToList();
      var mapData = _pollService.Value.Search(new PollFilterModel { PostIds = postIds }, new Pageable(), useMasterDb, userId).Data.ToDictionary(x => x.Id, x => x);
      data.ForEach(x =>
      {
        var item = mapData.GetValueOrDefault(x.ObjectId);
        if (item == null) return;
        x.ObjectModel = item;
      });
    }

    private void ValidateBeforeInsert(BookmarkInputModel model)
    {
      var post = _replicaDbContext.Posts.FirstOrDefault(x => x.Guid == model.ObjectId && x.Type == (byte)model.ObjectType);
      if (post == null)
      {
        throw model.ObjectType == BookmarkObjectType.Post
          ? new NotFoundException(ErrorMessages.Post_NotFound)
          : new NotFoundException(ErrorMessages.Poll_NotFound);
      }
    }

    #endregion

  }
}

﻿using Kempus.Core.Enums;
using Kempus.Core.Models;
using Kempus.Models.Public.Bookmark;

namespace Kempus.Services.Public.Bookmark
{
  public interface IBookmarkService
  {
    PagingResult<BookmarkListViewModel> Search(BookmarkFilterModel filterModel, Pageable requestModel, bool useMasterDb);
    Guid CreateIfNotExist(BookmarkInputModel input);
    void Delete(Guid objectId, CoreEnums.BookmarkObjectType objectType, Guid userId);
    void SetBookmarks(List<IHasBookmark> data, Guid userId, bool useMasterDb);
    void SetBookmark(IHasBookmark data, Guid userId, bool useMasterDb);
  }
}

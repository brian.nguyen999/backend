﻿using Kempus.Core.Enums;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.BellNotification;
using Kempus.Models.Public.Comment;
using Kempus.Models.Public.PostComments;
using Kempus.Models.Public.PostPollActivity;
using Kempus.Services.Public.BellNotification;
using Kempus.Services.Public.PostPollActivity;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;
using static Kempus.Core.Constants.CoreConstants;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.Public.Comment
{
  public class PostCommentService : IPostCommentService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly IPostPollActivityService _postPollActivityService;
    private readonly IBellNotificationService _bellNotificationService;

    public PostCommentService(
      MasterDbContext masterDbContext,
      ReplicaDbContext replicaDbContext,
      IPostPollActivityService postPollActivityService,
      IBellNotificationService bellNotificationService)
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
      _postPollActivityService = postPollActivityService;
      _bellNotificationService = bellNotificationService;
    }

    public Guid Create(PostCommentInputModel model, Guid userId, Guid schoolId)
    {
      ValidateBeforeInsert(model);

      var entity = new PostCommentEntity
      {
        PostId = model.PostId,
        ParentId = model.ParentId,
        Content = model.Content,
        Type = model.Type,
        CreatedBy = userId
      };

      using var transaction = _masterDbContext.Database.BeginTransaction();
      try
      {
        _postPollActivityService.Create(new PostPollActivityInputModel
        {
          ObjectId = model.PostId,
          ObjectType = PostType.Post,
          UserId = userId,
          ActionId = CoreEnums.PostPollActivity.Comment,
          SchoolId = schoolId,
          CreatedDate = DateTime.UtcNow
        });

        _masterDbContext.Add(entity);
        _masterDbContext.SaveChanges();

        var bellNotificationInputModel = new BellNotificationInputModel
        {
          CreatedDate = DateTime.UtcNow,
          ObjectId = entity.PostId,
          CreatedUser = userId,
          UserAction = UserAction.Create,
          BellNotificationType = (byte)BellNotificationType.Site
        };

        if (model.Type == (byte)PostType.Post)
        {
          bellNotificationInputModel.Title = "commented on this post";
          bellNotificationInputModel.ObjectTypeId = ObjectTypeId.Post;
        }
        else
        {
          bellNotificationInputModel.Title = "commented on this poll";
          bellNotificationInputModel.ObjectTypeId = ObjectTypeId.Poll;
        }

        _bellNotificationService.SaveNotifyObjectChange(bellNotificationInputModel);
        transaction.Commit();

        var postCreatorId = _replicaDbContext.Posts.Where(x => x.Guid == entity.PostId).Select(x => x.CreatedBy).FirstOrDefault();
        if(postCreatorId != userId)
        {
          _bellNotificationService.SendBellAndPushMobileNotificationByGroupUser(new List<Guid>() { postCreatorId });
        }

        return entity.Guid;
      }
      catch (Exception)
      {
        transaction.Rollback();
        throw;
      }
    }

    public PagingResult<PostCommentListViewModel> Search(PostCommentFilterModel filter, Pageable pageable, bool useMasterDb)
    {
      var query = BuildQueryable(filter, useMasterDb);

      var totalCount = query.LongCount();
      if (totalCount < 1)
      {
        return new PagingResult<PostCommentListViewModel>(0, pageable.PageIndex, pageable.PageSize);
      }

      var orderBy =
        QueryableUtils.GetOrderByFunction<PostCommentEntity>(
          pageable.GetSortables(nameof(PostCommentEntity.CreatedDate)));

      var data = orderBy(query).Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new PostCommentListViewModel(x))
        .ToList();

      BuildResultData(data, useMasterDb);

      return new PagingResult<PostCommentListViewModel>(data, totalCount, pageable.PageIndex,
        pageable.PageSize);
    }

    #region Private methods

    private void BuildResultData(List<PostCommentListViewModel> data, bool useMasterDb)
    {
      SetUsers(data, useMasterDb);
      SetNumberOfReplies(data, useMasterDb);
      SetCommentTrees(data, useMasterDb);
    }

    private IQueryable<PostCommentEntity> BuildQueryable(PostCommentFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.PostComments.AsNoTracking();
      query = PostCommentQueryBuilder<PostCommentEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    private void SetUsers(List<PostCommentListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var userIds = data.Select(x => x.CreatedBy).ToList();
      var mapData = dbContext.Users.Where(x => userIds.Contains(x.UserId))
        .ToDictionary(x => x.UserId, x => x.NickName);

      data.ForEach(x =>
      {
        var name = mapData.GetValueOrDefault(x.CreatedBy);
        if (string.IsNullOrWhiteSpace(name)) return;

        x.NickName = name;
      });
    }

    private void SetNumberOfReplies(List<PostCommentListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var parentIds = data.Select(x => x.Id).ToList();
      var mapData = dbContext.PostComments
        .Where(x => parentIds.Contains(x.ParentId.Value))
        .GroupBy(x => x.ParentId)
        .Select(x => new
        {
          Id = x.Key,
          NumberOfReplies = x.Count()
        })
        .ToDictionary(x => x.Id, x => x.NumberOfReplies);

      data.ForEach(x =>
      {
        x.NumberOfReplies = mapData.GetValueOrDefault(x.Id);
      });
    }

    public void SetNumberOfComments(List<IHasNumberOfComments> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var postIds = data.Select(x => x.Id).ToList();
      var mapData = dbContext.PostComments
        .Where(x => postIds.Contains(x.PostId))
        .GroupBy(x => x.PostId)
        .Select(x => new
        {
          Id = x.Key,
          NumberOfComments = x.Count()
        })
        .ToDictionary(x => x.Id, x => x.NumberOfComments);

      data.ForEach(x =>
      {
        x.NumberOfComment = mapData.GetValueOrDefault(x.Id);
      });
    }

    public void SetNumberOfComments(IHasNumberOfComments data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var postId = data.Id;
      var numberOfComment = dbContext.PostComments
        .LongCount(x => x.PostId == postId);
      data.NumberOfComment = numberOfComment;
    }

    private void SetCommentTrees(List<PostCommentListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var parentIds = data.Select(x => x.Id).ToList();
      IQueryable<PostCommentEntity> finalQuery = null;
      foreach (var parentId in parentIds)
      {
        var query = dbContext.PostComments
          .Where(x => x.ParentId == parentId)
          .OrderByDescending(x => x.CreatedDate)
          .Take(10)
          .Select(x => x);
        finalQuery = finalQuery == null
          ? query
          : finalQuery.Concat(query);
      }

      var mapData = finalQuery.Select(x => new PostCommentListViewModel(x)).ToList();
      SetUsers(mapData, useMasterDb);
      data.ForEach(x =>
      {
        x.Replies = mapData.Where(q => q.ParentId == x.Id).ToList();
      });
    }

    private void ValidateBeforeInsert(PostCommentInputModel model)
    {
      var post = _replicaDbContext.Posts.FirstOrDefault(x => x.Guid == model.PostId);
      if (post == null)
      {
        if (model.Type == (byte)PostType.Post)
        {
          throw new NotFoundException(ErrorMessages.Post_NotFound);
        }
        else
        {
          throw new NotFoundException(ErrorMessages.Poll_NotFound);
        }
      }

      if (model.ParentId != null)
      {
        var postComment = _replicaDbContext.PostComments.FirstOrDefault(x => x.Guid == model.ParentId);
        if (postComment == null)
          throw new NotFoundException(ErrorMessages.Comment_NotFound);

        if (postComment.PostId != post.Guid)
        {
          if (model.Type == (byte)PostType.Post)
          {
            throw new BadRequestException(ErrorMessages.Comment_NotMatchWithParentInSamePost);
          }
          else
          {
            throw new BadRequestException(ErrorMessages.Comment_NotMatchWithParentInSamePoll);
          }
        }
      }

      if (string.IsNullOrWhiteSpace(model.Content))
        throw new BadRequestException(ErrorMessages.Comment_Required);
    }

    #endregion
  }
}
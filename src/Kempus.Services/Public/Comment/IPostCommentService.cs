﻿using Kempus.Core.Models;
using Kempus.Models.Public.Comment;
using Kempus.Models.Public.PostComments;

namespace Kempus.Services.Public.Comment
{
  public interface IPostCommentService
  {
    Guid Create(PostCommentInputModel model, Guid userId, Guid schoolId);
    PagingResult<PostCommentListViewModel> Search(PostCommentFilterModel filter, Pageable pageable, bool useMasterDb);
    void SetNumberOfComments(List<IHasNumberOfComments> data, bool useMasterDb);
    void SetNumberOfComments(IHasNumberOfComments data, bool useMasterDb);
  }
}
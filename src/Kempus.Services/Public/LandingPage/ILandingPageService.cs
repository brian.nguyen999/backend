﻿using Kempus.Core.Models;
using Kempus.Models.Public.LandingPage;
using Kempus.Models.Public.School;

namespace Kempus.Services.Public.LandingPage
{
  public interface ILandingPageService
  {
    PagingResult<LandingPageSchoolModel> Search(SchoolFilterModel filterModel, Pageable pageable, bool useMasterDb);
    SchoolCountModel GetSchoolCount();
  }
}
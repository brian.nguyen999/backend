using Kempus.Core.Constants;
using Kempus.Core.Models;
using Kempus.Core.Models.Configuration;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.LandingPage;
using Kempus.Models.Public.School;
using Kempus.Services.Common.Redis;
using Kempus.Services.Public.School;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace Kempus.Services.Public.LandingPage
{
  public class LandingPageService : ILandingPageService
  {
    private readonly ILogger _logger;
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly MasterDbContext _masterDbContext;
    private readonly ISchoolService _schoolService;
    private readonly IRedisService _redisService;
    private readonly IConnectionMultiplexer _redis;
    private readonly RedisConfig _redisConfig;

    public LandingPageService(
      ILogger<LandingPageService> logger,
      ReplicaDbContext replicaDbContext,
      MasterDbContext masterDbContext,
      ISchoolService schoolService,
      IRedisService redisService,
      IConnectionMultiplexer redis,
      AwsConfig awsConfig
      )
    {
      _logger = logger;
      _replicaDbContext = replicaDbContext;
      _masterDbContext = masterDbContext;
      _schoolService = schoolService;
      _redisService = redisService;
      _redis = redis;
      _redisConfig = awsConfig.Redis;
    }

    public PagingResult<LandingPageSchoolModel> Search(SchoolFilterModel filterModel, Pageable pageable, bool useMasterDb)
    {
      var cacheKey = BuildCacheKey(filterModel, pageable);
      if (string.IsNullOrEmpty(filterModel.Keyword))
      {
        var cacheObject = GetCache(cacheKey);
        if (cacheObject != null) return cacheObject;
      }

      var query = BuildQueryable(filterModel, useMasterDb);
      var totalCount = query.LongCount();
      if (totalCount < 1)
        return new PagingResult<LandingPageSchoolModel>(0, pageable.PageIndex, pageable.PageSize);
      query = BuildOrderClause(query, pageable, useMasterDb);
      var data = query.Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new LandingPageSchoolModel(x)).ToList();
      BuildResultData(data, useMasterDb);
      var result = new PagingResult<LandingPageSchoolModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);

      if (string.IsNullOrEmpty(filterModel.Keyword) && data.Any())
        SetCache(cacheKey, result);
      return result;
    }

    public SchoolCountModel GetSchoolCount()
    {
      return _schoolService.GetTotalSchoolCount();
    }

    private IQueryable<SchoolEntity> BuildQueryable(SchoolFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.Schools.AsNoTracking();
      query = SchoolQueryBuilder<SchoolEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }
    private void BuildResultData(List<LandingPageSchoolModel> data, bool useMasterDb)
    {
      SetTotalUsers(data, useMasterDb);
    }

    private void SetTotalUsers(List<LandingPageSchoolModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var schoolIds = data.Select(x => x.Guid).ToList();
      var mapData = dbContext.SchoolLifeTimeAnalytics
        .Where(x => schoolIds.Contains(x.SchoolId))
        .ToDictionary(x => x.SchoolId, x => new
        {
          x.Users,
          x.NoOfWaitList
        });
      data.ForEach(x =>
      {
        var item = mapData.GetValueOrDefault(x.Guid);
        if (item == null) return;
        x.TotalUsers = x.IsActivated ? item.Users : item.NoOfWaitList;
      });
    }

    private IQueryable<SchoolEntity> BuildOrderClause(IQueryable<SchoolEntity> query, Pageable pageable, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      query = pageable.SortedField?.ToLower() switch
      {
        _ => query.Join(dbContext.SchoolLifeTimeAnalytics,
          school => school.Guid,
          schoolAnalytic => schoolAnalytic.SchoolId,
          (school, schoolAnalytic) => new
          {
            School = school,
            TotalUsers = school.IsActivated ? schoolAnalytic.Users : schoolAnalytic.NoOfWaitList
          })
          .OrderByDescending(x => x.TotalUsers)
          .ThenBy(x => x.School.Name)
          .Select(x => x.School)
      };

      return query;
    }

    private string BuildCacheKey(SchoolFilterModel schoolFilterModel, Pageable pageable)
    {
      return
        $"{RedisCacheKey.School_SchoolList}-{schoolFilterModel.GetCacheKey()}-{pageable.PageIndex}-{pageable.PageSize}";
    }

    private void SetCache(string cacheKey, PagingResult<LandingPageSchoolModel> cacheObject)
    {
      _redisService.AddCacheAsync(cacheKey, JsonConvert.SerializeObject(cacheObject), DateTime.UtcNow.AddHours(24));
    }

    private PagingResult<LandingPageSchoolModel>? GetCache(string cacheKey)
    {
      var cacheResult = _redisService.GetValueByKeyAsync(cacheKey).Result;
      return !string.IsNullOrEmpty(cacheResult)
        ? JsonConvert.DeserializeObject<PagingResult<LandingPageSchoolModel>>(cacheResult)
        : null;
    }
  }
}
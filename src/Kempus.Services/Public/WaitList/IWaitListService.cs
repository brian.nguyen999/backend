﻿using Kempus.Entities;

namespace Kempus.Services.Public.WaitList
{
  public interface IWaitListService
  {
    Guid InsertIfNotExist(string email, Guid schoolId);
    long CountTotalInWaitList(Guid schoolId);
    WaitListEntity GetWaitListInfoByEmail(string email);
    bool Delete(string email);
  }
}

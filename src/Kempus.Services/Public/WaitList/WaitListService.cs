﻿using Kempus.Entities;
using Kempus.EntityFramework;

namespace Kempus.Services.Public.WaitList
{
  public class WaitListService : IWaitListService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;

    public WaitListService(
      MasterDbContext masterDbContext,
      ReplicaDbContext replicaDbContext
      )
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
    }

    public Guid InsertIfNotExist(string email, Guid schoolId)
    {
      var waitList = _replicaDbContext.WaitList.FirstOrDefault(x => x.Email == email &&
                                                                    x.SchoolId == schoolId);
      if (waitList == null)
      {
        waitList = new WaitListEntity
        {
          Guid = Guid.NewGuid(),
          Email = email,
          SchoolId = schoolId
        };

        var transaction = _masterDbContext.Database.BeginTransaction();
        try
        {
          _masterDbContext.WaitList.Add(waitList);
          _masterDbContext.SaveChanges();
          transaction.Commit();
        }
        catch (Exception)
        {
          transaction.Rollback();
          throw;
        }
      }

      return waitList.Guid;
    }

    public long CountTotalInWaitList(Guid schoolId)
    {
      return _replicaDbContext.WaitList.LongCount(x => x.SchoolId == schoolId);
    }

    public WaitListEntity GetWaitListInfoByEmail(string email)
    {
      if (string.IsNullOrEmpty(email)) return null;
      return _replicaDbContext.WaitList.Where(x => x.Email.ToLower() == email.ToLower()).FirstOrDefault();
    }

    public bool Delete(string email)
    {
      var currentEntity = GetWaitListInfoByEmail(email);
      if (currentEntity == null)
      {
        return false;
      }

      _masterDbContext.WaitList.Remove(currentEntity);
      int result = _masterDbContext.SaveChanges();
      
      return result > 0;
    }
  }
}

﻿using Kempus.Core.Models;
using Kempus.Models.Public.CourseReview;
using Kempus.Models.Public.CourseReviewActivity;
using Kempus.Models.Public.PostPollActivitySummary;
using Kempus.Models.Public.Trending.cs;

namespace Kempus.Services.Public.Trending
{
  public interface ITrendingService
  {
    PagingResult<CourseReviewListViewModel> GetTrendingCourseReview(CourseReviewActivitySummaryFilterModel filter,
      Pageable pageable, bool useMasterDb, Guid userId);
    PagingResult<PostPollTrendingListView> GetTrendingPostPoll(PostPollActivitySummaryFilterModel filter,
      Pageable pageable, bool useMasterDb);
  }
}

﻿using Kempus.Core.Models;
using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.CourseReview;
using Kempus.Models.Public.CourseReviewActivity;
using Kempus.Models.Public.PostPollActivitySummary;
using Kempus.Models.Public.Trending.cs;
using Kempus.Services.Public.CourseReview;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Kempus.Services.Public.Trending
{
  public class TrendingService : ITrendingService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly ICourseReviewService _courseReviewService;

    public TrendingService(
      ReplicaDbContext replicaDbContext,
      MasterDbContext masterDbContext,
      ICourseReviewService courseReviewService
      )
    {
      _replicaDbContext = replicaDbContext;
      _masterDbContext = masterDbContext;
      _courseReviewService = courseReviewService;
    }


    public PagingResult<CourseReviewListViewModel> GetTrendingCourseReview(CourseReviewActivitySummaryFilterModel filter, Pageable pageable,
      bool useMasterDb, Guid userId)
    {
      var query = BuildQueryable(filter, useMasterDb);
      var orderBy =
        QueryableUtils.GetOrderByFunction<CourseReviewActivitySummaryEntity>(pageable.GetSortables(nameof(CourseReviewActivitySummaryEntity.TotalActivitiesIn6Month)));

      var data = orderBy(query).Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => x).ToList();

      var courseReviewFilterModel = new CourseReviewFilterModel()
      {
        CourseReviewIds = data.Select(x => x.CourseReviewId).ToList()
      };

      return _courseReviewService.Search(courseReviewFilterModel, pageable, useMasterDb: false, useFullTextSearch: false, userId);
    }

    public PagingResult<PostPollTrendingListView> GetTrendingPostPoll(PostPollActivitySummaryFilterModel filter, Pageable pageable,
      bool useMasterDb)
    {
      var query = BuildQueryable(filter, useMasterDb)
        .GroupBy(x => new { x.ObjectId, x.ObjectType })
        .Select(x => new
        {
          ObjectId = x.Key.ObjectId,
          ObjectType = x.Key.ObjectType,
          CountValue = pageable.SortedField == "MostLiked"
            ? x.Max(x => x.TotalLikeIn7days)
            : x.Max(x => x.TotalCommentIn7days)
        })
        .OrderByDescending(x => x.CountValue)
        .Select(x => new PostPollTrendingListView
        {
          ObjectId = x.ObjectId,
          ObjectType = x.ObjectType
        });

      var totalCount = query.LongCount();
      if (totalCount < 1)
        return new PagingResult<PostPollTrendingListView>(0, pageable.PageIndex, pageable.PageSize);

      var data = query.Skip(pageable.Offset).Take(pageable.PageSize).ToList();
      BuildResultData(data, useMasterDb);

      return new PagingResult<PostPollTrendingListView>(data, totalCount, pageable.PageIndex,
        pageable.PageSize);
    }

    #region Private methods

    private IQueryable<CourseReviewActivitySummaryEntity> BuildQueryable(CourseReviewActivitySummaryFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.CourseReviewActivitySummaries.AsNoTracking();
      query = CourseReviewActivitySummaryQueryBuilder<CourseReviewActivitySummaryEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    private IQueryable<PostPollActivitySummaryEntity> BuildQueryable(PostPollActivitySummaryFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.PostPollActivitySummaries.AsNoTracking();
      query = PostPollActivitySummaryQueryBuilder<PostPollActivitySummaryEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    private IQueryable<PostPollActivitySummaryEntity> BuildOrderClause(IQueryable<PostPollActivitySummaryEntity> query, Pageable pageable)
    {
      query = pageable.SortedField?.ToLower() switch
      {
        "mostliked" => query.OrderByDescending(x => x.TotalLikeIn7days),
        "mostcomment" => query.OrderByDescending(x => x.TotalCommentIn7days),
        _ => query.OrderByDescending(x => x.TotalLikeIn7days)
      };

      return query;
    }

    private void BuildResultData(List<PostPollTrendingListView> data, bool useMasterDb)
    {
      SetPostPollName(data, useMasterDb);
    }

    private void SetPostPollName(List<PostPollTrendingListView> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var postIds = data.Select(x => x.ObjectId).ToList();
      var mapData = dbContext.Posts.Where(x => postIds.Contains(x.Guid)).ToDictionary(x => x.Guid, x => x.Title);
      data.ForEach(x =>
      {
        var item = mapData.GetValueOrDefault(x.ObjectId);
        if (item == null) return;
        x.Title = item;
      });
    }

    #endregion

  }
}

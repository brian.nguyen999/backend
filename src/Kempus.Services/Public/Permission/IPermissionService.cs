﻿using Kempus.Models.Public.Permission;

namespace Kempus.Services.Public.Permission
{
  public interface IPermissionService
  {
    List<PermissionDetailModel> GetAll(int? moduleApplicationId);
  }
}
﻿using Kempus.Core;
using Kempus.EntityFramework;
using Kempus.Models.Public.Permission;

namespace Kempus.Services.Public.Permission
{
  public class PermissionService : IPermissionService
  {
    private readonly MasterDbContext _masterContext;
    private readonly ReplicaDbContext _replicaContext;
    private readonly SessionStore _sessionStore;

    public PermissionService(
      MasterDbContext masterContext,
      ReplicaDbContext replicaContext,
      SessionStore sessionStore
    )
    {
      _masterContext = masterContext;
      _replicaContext = replicaContext;
      _sessionStore = sessionStore;
    }

    public List<PermissionDetailModel> GetAll(int? moduleApplicationId)
    {
      var query = _replicaContext.Permission.AsQueryable();
      if (moduleApplicationId != null)
      {
        query = query.Where(x => x.ModuleApplicationId == moduleApplicationId);
      }

      return query.Select(x =>
          new PermissionDetailModel
          {
            Id = x.Id,
            Name = x.Name,
            ModuleApplicationId = x.ModuleApplicationId,
            Order = x.Order,
            ParentId = x.ParentId,
          })
        .ToList();
    }
  }
}
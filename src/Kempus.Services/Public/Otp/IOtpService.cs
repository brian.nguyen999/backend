﻿using Kempus.Entities;
using Kempus.Models.Public.User.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Public.Otp
{
  public interface IOtpService
  {
    OtpEntity Create(OtpRequestDto requestDto);
    Task<OtpEntity> GetByIdAndTypeAsync(OtpRequestDto requestDto);
    Task<bool> CheckOtpValidAsync(OtpRequestDto requestDto);
    Task<bool> CheckObjectExistAsync(OtpRequestDto requestDto);
    bool CheckOtpIsExpired(DateTime createDate);
    Task<bool> DeleteOtpAsync(OtpRequestDto requestDto);
    void DeleteExpiredOtp();
  }
}
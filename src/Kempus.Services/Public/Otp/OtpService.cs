﻿using Kempus.Core.Errors;
using Kempus.Core.Models.Configuration;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.User.Request;
using Kempus.Services.Common;
using Kempus.Services.Public.Notification;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Kempus.Services.Public.Otp
{
  public class OtpService : IOtpService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ILogger _logger;
    private readonly IConfiguration _configuration;
    private readonly UserConfig _userConfig;
    private readonly IEmailNotificationService _notificationService;


    public OtpService(
      MasterDbContext masterDbContext,
      ILogger<OtpService> logger,
      IConfiguration configuration,
      UserConfig userConfig,
      IEmailNotificationService notificationService
    )
    {
      _masterDbContext = masterDbContext;
      _logger = logger;
      _userConfig = userConfig;
      _configuration = configuration;
      _notificationService = notificationService;
    }

    public OtpEntity Create(OtpRequestDto requestDto)
    {
      var currentEntity = GetByIdAndTypeAsync(requestDto).Result;
      if (currentEntity != null)
      {
        currentEntity.IsDeleted = true;
        _masterDbContext.Update(currentEntity);
      }

      OtpEntity newEntity = new OtpEntity()
      {
        OtpCode = Utilities.StringHelper.RandomNumber(100000, 999999),
        ObjectId = requestDto.ObjectId,
        ObjectType = requestDto.ObjectType,
        ExpirationTime = DateTime.UtcNow.AddMinutes(_userConfig.OtpValidityInMinutes)
      };

      int result = 0;
      using var transaction = _masterDbContext.Database.BeginTransaction();
      try
      {
        _masterDbContext.Otp.Add(newEntity);
        result = _masterDbContext.SaveChanges();
        transaction.Commit();
      }
      catch (Exception)
      {
        transaction.Rollback();
        throw;
      }

      if (result > 0 && requestDto.IsSendNotification)
      {
        _notificationService.SendOtpAsync(newEntity.OtpCode, requestDto.ObjectId).GetAwaiter().GetResult();
      }

      return newEntity;
    }

    public async Task<OtpEntity> GetByIdAndTypeAsync(OtpRequestDto requestDto)
    {
      return await _masterDbContext.Otp.FirstOrDefaultAsync(x =>
        x.ObjectId == requestDto.ObjectId && x.ObjectType == requestDto.ObjectType);
    }

    public async Task<bool> CheckOtpValidAsync(OtpRequestDto requestDto)
    {
      var currentEnttiy = await GetByIdAndTypeAsync(requestDto);

      if (currentEnttiy == null || currentEnttiy.OtpCode != requestDto.OtpCode)
      {
        throw new BadRequestException(ErrorMessages.User_OtpInvalid);
      }
      else
      {
        if (CheckOtpIsExpired(currentEnttiy.CreatedDate))
        {
          throw new BadRequestException(ErrorMessages.User_OtpInvalid);
        }
      }


      return true;
    }

    public async Task<bool> CheckObjectExistAsync(OtpRequestDto requestDto)
    {
      var currentEntity = await GetByIdAndTypeAsync(requestDto);
      if (currentEntity == null)
      {
        return false;
      }

      if (CheckOtpIsExpired(currentEntity.CreatedDate))
      {
        return false;
      }

      return true;
    }

    public bool CheckOtpIsExpired(DateTime createDate)
    {
      return createDate.AddMinutes(_userConfig.OtpValidityInMinutes) < DateTime.UtcNow;
    }

    public async Task<bool> DeleteOtpAsync(OtpRequestDto requestDto)
    {
      using (var transaction = await _masterDbContext.Database.BeginTransactionAsync())
      {
        try
        {
          var currentEntity = await GetByIdAndTypeAsync(requestDto);
          if (currentEntity == null)
          {
            return false;
          }

          _masterDbContext.Otp.Remove(currentEntity);
          var result = await _masterDbContext.SaveChangesAsync();
          await transaction.CommitAsync();

          return result > 0;
        }
        catch (Exception ex)
        {
          await transaction.RollbackAsync();
          _logger.LogError(ex.Message);
          throw new BadRequestException(ex.Message);
        }
      }
    }

    public void DeleteExpiredOtp()
    {
      var expiredOtpTime = _configuration.GetValue<int>("UserConfig:OtpValidityInMinutes");
      var expiredOtps = _masterDbContext.Otp.ToList()
        .Where(x => DateTime.UtcNow.Subtract(x.CreatedDate).TotalSeconds > (expiredOtpTime * 60)).ToList();
      if (expiredOtps.Any())
      {
        _masterDbContext.Otp.RemoveRange(expiredOtps);
      }

      _masterDbContext.SaveChanges();
    }
  }
}
﻿using Kempus.Models.Public.PostPollActivity;

namespace Kempus.Services.Public.PostPollActivity
{
  public interface IPostPollActivityService
  {
    void Create(PostPollActivityInputModel model);
    void ClearLast7DaysActivities();
  }
}

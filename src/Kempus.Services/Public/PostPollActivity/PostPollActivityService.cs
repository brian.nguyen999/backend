﻿using EFCore.BulkExtensions;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.PostPollActivity;

namespace Kempus.Services.Public.PostPollActivity
{
  public class PostPollActivityService : IPostPollActivityService
  {
    private readonly MasterDbContext _masterDbContext;

    public PostPollActivityService(
      MasterDbContext masterDbContext
      )
    {
      _masterDbContext = masterDbContext;
    }

    public void Create(PostPollActivityInputModel model)
    {

      var entity = new PostPollActivityEntity()
      {
        Id = Guid.NewGuid(),
        ObjectId = model.ObjectId,
        ObjectType = (byte)model.ObjectType,
        ActionId = (byte)model.ActionId,
        CreatedDate = DateTime.UtcNow,
        SchoolId = model.SchoolId
      };
      _masterDbContext.PostPollActivities.Add(entity);

      var entityActivityIn7days = new PostPoll7daysActivityEntity
      {
        Id = Guid.NewGuid(),
        ObjectId = model.ObjectId,
        ObjectType = (byte)model.ObjectType,
        ActionId = (byte)model.ActionId,
        CreatedDate = DateTime.UtcNow,
        SchoolId = model.SchoolId
      };
      _masterDbContext.PostPoll7daysActivities.Add(entityActivityIn7days);
      _masterDbContext.SaveChanges();
    }

    public void ClearLast7DaysActivities()
    {
      var last7days = DateTime.UtcNow.AddDays(-7);
      var activitiesIn7days = _masterDbContext.PostPoll7daysActivities.Where(x => x.CreatedDate < last7days).ToList();
      _masterDbContext.PostPoll7daysActivities.RemoveRange(activitiesIn7days);
      _masterDbContext.SaveChanges();
    }
  }
}

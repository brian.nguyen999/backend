﻿using Kempus.Models.Public.CourseReviewActivity;

namespace Kempus.Services.Public.CourseReviewActivity
{
  public interface ICourseReviewActivityService
  {
    void Create(CourseReviewActivityInputModel model);
    void ClearAllActivity();
  }
}

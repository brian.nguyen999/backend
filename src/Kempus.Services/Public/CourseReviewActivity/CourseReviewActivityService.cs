﻿using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.CourseReviewActivity;
using Microsoft.EntityFrameworkCore;

namespace Kempus.Services.Public.CourseReviewActivity
{
  public class CourseReviewActivity : ICourseReviewActivityService
  {
    private readonly MasterDbContext _masterDbContext;

    public CourseReviewActivity(
      MasterDbContext masterDbContext
      )
    {
      _masterDbContext = masterDbContext;
    }

    public void Create(CourseReviewActivityInputModel model)
    {
      var entity = new CourseReview6MonthActivityEntity
      {
        CourseReviewId = model.CourseReviewId,
        UserId = model.UserId,
        ActionId = (byte)model.ActionId,
        CreatedDate = DateTime.UtcNow,
        SchoolId = model.SchoolId
      };

      _masterDbContext.CourseReview6MonthActivities.Add(entity);
      _masterDbContext.SaveChanges();
    }

    public void ClearAllActivity()
    {
      try
      {
        var currentDateTime = DateTime.UtcNow;
        var pageSize = 100;
        var totalCount = _masterDbContext.CourseReview6MonthActivities
          .LongCount(x => x.CreatedDate <= currentDateTime.AddMonths(-6));
        if (totalCount < 1) return;
        var totalPage = (int)totalCount / pageSize;
        for (int pageIndex = 0; pageIndex <= totalPage; pageIndex++)
        {
          var offset = (int)(pageIndex * pageSize);
          var listActivities = _masterDbContext.CourseReview6MonthActivities
            .Where(x => x.CreatedDate <= currentDateTime.AddMonths(-6))
            .Skip(offset).Take(pageSize)
            .ToList();
          if (listActivities.Any())
          {
            _masterDbContext.CourseReview6MonthActivities.RemoveRange(listActivities);
            _masterDbContext.SaveChanges();
          }
        }
      }
      catch (Exception)
      {
        throw;
      }
    }
  }
}

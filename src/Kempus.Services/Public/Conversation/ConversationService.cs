using Kempus.Core.Enums;
using Kempus.Core.Errors;
using Kempus.Core.Extensions;
using Kempus.Core.Models;
using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Integration.Ably;
using Kempus.Models.Common.WebSocket;
using Kempus.Models.Public.Conversation;
using Kempus.Services.Common.WebSocket;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.Public.Conversation
{
  public class ConversationService : IConversationService
  {
    private readonly ILogger<ConversationService> _logger;
    private readonly ReplicaDbContext _replicaContext;
    private readonly MasterDbContext _masterDbContext;
    private readonly IAblyService _ablyService;
    private readonly IConfiguration _configuration;
    private readonly IWebSocketService _webSocketService;

    public ConversationService(
      ILogger<ConversationService> logger,
      ReplicaDbContext replicaContext,
      MasterDbContext masterDbContext,
      IAblyService ablyService,
      IConfiguration configuration,
      IWebSocketService webSocketService)
    {
      _logger = logger;
      _replicaContext = replicaContext;
      _masterDbContext = masterDbContext;
      _ablyService = ablyService;
      _configuration = configuration;
      _webSocketService = webSocketService;
    }

    public PagingResult<ConversationListViewModel> SearchConversations(ConversationFilterModel filter, Pageable pageable, Guid currentUserId, bool useMasterDb)
    {
      var query = BuildQueryable(filter, useMasterDb);
      var totalCount = query.LongCount();
      if (totalCount < 1)
        return new PagingResult<ConversationListViewModel>(0, pageable.PageIndex, pageable.PageSize);
      var orderBy = QueryableUtils.GetOrderByFunction<ConversationMemberEntity>(pageable.GetSortables(nameof(ConversationMemberEntity.LatestActivityDateTime)));
      var data = orderBy(query).Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new ConversationListViewModel(x)).ToList();
      BuildResultData(data, currentUserId, useMasterDb);
      return new PagingResult<ConversationListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    public Guid Create(ConversationInputModel model)
    {
      if (model.FromUserId == model.ToUserId)
        throw new BadRequestException(ErrorMessages.Conversation_ErrorSameUserId);

      var conversationId = GetDirectMessageConversation(model.FromUserId, model.ToUserId, model.SchoolId);
      if (conversationId == null)
      {
        conversationId = CreateNewConversation(model.FromUserId, model.ToUserId, model.SchoolId);
      }
      else
      {
        // If user deleted conversation => re-active conversation
        // If user currently have pending request => active conversation for both user
        var conversationMemberEntity = _masterDbContext.ConversationMembers
          .FirstOrDefault(x => x.ConversationId == conversationId &&
                               x.MemberId == model.FromUserId &&
                               (x.Status == (byte)CoreEnums.ConversationMemberStatus.Delete ||
                               x.Status == (byte)CoreEnums.ConversationMemberStatus.Pending));
        if (conversationMemberEntity != null)
        {
          conversationMemberEntity.Status = (byte)CoreEnums.ConversationMemberStatus.Accept;
          _masterDbContext.ConversationMembers.Update(conversationMemberEntity);
          _masterDbContext.SaveChanges();

          PushAcceptConversationNotification(conversationMemberEntity.ConversationId, model.FromUserId, false);
        }
      }

      return conversationId.Value;
    }

    public ConversationDetailModel GetDetail(Guid conversationId, Guid currentUserId, Guid schoolId)
    {
      var conversation = GetConversation(conversationId, currentUserId, schoolId);
      if (conversation == null)
        throw new BadRequestException(ErrorMessages.Conversation_NotFound);

      var member = _masterDbContext.ConversationMembers
        .FirstOrDefault(x => x.ConversationId == conversation.Guid &&
                             x.MemberId == currentUserId &&
                             x.IsHasNewMessage);
      if (member != null)
      {
        member.IsHasNewMessage = false;
        _masterDbContext.Update(member);
        _masterDbContext.SaveChanges();
      }

      var result = new ConversationDetailModel(conversation);
      BuildResultData(result, currentUserId, false);
      return result;
    }

    public PagingResult<ConversationMessageListViewModel> SearchMessages(ConversationMessageFilterModel filter, Pageable pageable, Guid currentUserId, bool useMasterDb)
    {
      var conversation = GetConversation(filter.ConversationId, currentUserId, filter.SchoolId);
      var conversationMember = GetConversationMember(filter.ConversationId, currentUserId);
      if (conversation == null || conversationMember == null)
        throw new NotFoundException(ErrorMessages.Conversation_NotFound);

      filter.ConversationDeletionTime = conversationMember.DeletionTime;

      var query = BuildQueryable(filter, useMasterDb);
      var totalCount = query.LongCount();
      if (totalCount < 1)
        return new PagingResult<ConversationMessageListViewModel>(0, pageable.PageIndex, pageable.PageSize);

      var orderBy = QueryableUtils.GetOrderByFunction<MessageEntity>(pageable.GetSortables(nameof(MessageEntity.CreatedDate)));
      var data = orderBy(query).Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new ConversationMessageListViewModel(x)).ToList();
      return new PagingResult<ConversationMessageListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize); ;
    }

    public int GetTotalPending(Guid userId)
    {
      return _replicaContext.ConversationMembers
        .Count(x => x.MemberId == userId &&
                    x.Status == (byte)CoreEnums.ConversationMemberStatus.Pending);
    }

    public int GetTotalUnread(Guid userId)
    {
      var totalCount = _replicaContext.ConversationMembers
        .Count(x => x.MemberId == userId && x.IsHasNewMessage &&
               (x.Status == (byte)CoreEnums.ConversationMemberStatus.Accept ||
                x.Status == (byte)CoreEnums.ConversationMemberStatus.Pending));
      return totalCount;
    }

    public ConversationMessageListViewModel SendMessage(ConversationMessageInputModel input)
    {
      var currentTime = DateTime.UtcNow;

      var conversation = GetConversation(input.ConversationId, input.FromUserId, input.SchoolId);
      if (conversation == null)
        throw new BadRequestException(ErrorMessages.Conversation_NotFound);

      var conversationMembers = _masterDbContext.ConversationMembers.Where(x => x.ConversationId == input.ConversationId).ToList();
      if (conversationMembers == null)
        throw new BadRequestException(ErrorMessages.Conversation_NotFound);

      var fromMember = conversationMembers.FirstOrDefault(x => x.MemberId == input.FromUserId);
      var toMembers = conversationMembers.Where(x => x.MemberId != input.FromUserId).ToList();
      if (conversation.Type == (byte)ConversationType.DirectMessage && toMembers.Any(x => x.Status == (byte)ConversationMemberStatus.Blocked))
        throw new BadRequestException(ErrorMessages.Conversation_ErrorBlocked);

      if (conversationMembers.Any(x => x.Status == (byte)CoreEnums.ConversationMemberStatus.Pending))
        ValidatePendingMessage(input.ConversationId, input.FromUserId);

      using var transaction = _masterDbContext.Database.BeginTransaction();
      try
      {
        var message = new MessageEntity
        {
          Guid = Guid.NewGuid(),
          ConversationId = input.ConversationId,
          Content = input.Message,
          CreatedDate = currentTime,
          CreatedUser = input.FromUserId,
          SchoolId = input.SchoolId
        };
        _masterDbContext.Messages.Add(message);
        _masterDbContext.SaveChanges();
        _ablyService.PublishMessageAsync(input.ConversationId, input.FromUserId, new ConversationMessageModel(message));

        if (fromMember != null)
        {
          fromMember.IsHasNewMessage = false;
          fromMember.Status = UpdateConversationMemberStatusWhenSendNewMessage(fromMember);
          fromMember.LatestActivityDateTime = DateTime.UtcNow;
          _masterDbContext.ConversationMembers.Update(fromMember);
        }

        if (toMembers.Any())
        {
          toMembers.ForEach(x =>
          {
            x.IsHasNewMessage = true;
            x.Status = UpdateConversationMemberStatusWhenSendNewMessage(x);
            x.LatestActivityDateTime = DateTime.UtcNow;
          });

          _masterDbContext.ConversationMembers.UpdateRange(toMembers);
        }

        _masterDbContext.SaveChanges();
        transaction.Commit();

        PushNewMessageToChatList(conversation.Guid, input.FromUserId, userMasterDb: true);
        PushNewMessageToChatNotification(toMembers.Select(x => x.MemberId).ToList());

        return new ConversationMessageListViewModel(message) { };
      }
      catch
      {
        transaction.Rollback();
        throw;
      }
    }

    public bool Delete(Guid conversationId, Guid userId)
    {
      var currentConversationMember = _masterDbContext.ConversationMembers
        .FirstOrDefault(x => x.ConversationId == conversationId && x.MemberId == userId);
      if (currentConversationMember == null)
      {
        throw new BadRequestException(ErrorMessages.Conversation_NotFound);
      }

      currentConversationMember.DeletionTime = DateTime.UtcNow;
      currentConversationMember.PreviousStatus = currentConversationMember.Status;
      currentConversationMember.Status = (byte)ConversationMemberStatus.Delete;

      _masterDbContext.ConversationMembers.Update(currentConversationMember);
      var result = _masterDbContext.SaveChanges() > 0;
      if (result)
      {
        SendWebsocketConversationDeleted(userId, conversationId);
      }

      return result;
    }

    public PagingResult<ConversationBlockedListViewModel> SearchConversationBlocked(ConversationBlockFilterModel filter, Pageable pageable, bool useMasterDb)
    {
      var query = BuildQueryable(filter, useMasterDb);
      var totalCount = query.LongCount();
      if (totalCount < 1)
        return new PagingResult<ConversationBlockedListViewModel>(0, pageable.PageIndex, pageable.PageSize);
      var orderBy = QueryableUtils.GetOrderByFunction<BlockEntity>(pageable.GetSortables(nameof(BlockEntity.CreatedDate)));
      var data = orderBy(query).Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new ConversationBlockedListViewModel(x)).ToList();
      BuildResultData(data, useMasterDb);
      return new PagingResult<ConversationBlockedListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    public bool Block(ConversationBlockInputModel input)
    {
      if (_masterDbContext.Blocks.Any(x => x.ConversationId == input.ConversationId && x.Blocker == input.BlockerUserId))
      {
        // Send Websocket Blocked event to delete chats in other open tabs
        SendWebsocketConversationBlocked(input.BlockerUserId, input.ConversationId);
        return true;
      }

      var conversationMembers = _masterDbContext.ConversationMembers
        .Where(x => x.ConversationId == input.ConversationId).ToList();
      if (conversationMembers == null)
      {
        throw new BadRequestException(ErrorMessages.Conversation_NotFound);
      }

      var currentDate = DateTime.UtcNow;
      var blockerConversationMember = conversationMembers.FirstOrDefault(x => x.MemberId == input.BlockerUserId);
      if (blockerConversationMember == null)
        return false;
      blockerConversationMember.DeletionTime = input.IsDeleteChat ? currentDate : blockerConversationMember.DeletionTime;
      blockerConversationMember.PreviousStatus = blockerConversationMember.Status;
      blockerConversationMember.Status = (byte)ConversationMemberStatus.Blocked;

      // Add Data block
      var newBlockEntity = new BlockEntity()
      {
        ConversationId = input.ConversationId,
        Blocker = input.BlockerUserId,
        BlockedUser = conversationMembers.Where(x => x.MemberId != input.BlockerUserId).Select(x => x.MemberId).FirstOrDefault(),
        CreatedBy = input.BlockerUserId,
        CreatedDate = currentDate
      };

      bool result = false;
      using var transaction = _masterDbContext.Database.BeginTransaction();
      try
      {
        _masterDbContext.ConversationMembers.Update(blockerConversationMember);
        _masterDbContext.Blocks.Add(newBlockEntity);

        result = _masterDbContext.SaveChanges() > 0;

        transaction.Commit();
      }
      catch (Exception ex)
      {
        transaction.Rollback();
        throw new BadRequestException(ex.Message);
      }

      if (result)
      {
        // Send Websocket Blocked event to delete chats in other open tabs
        SendWebsocketConversationBlocked(newBlockEntity.Blocker, newBlockEntity.ConversationId);
      }

      return result;
    }

    public bool Unblock(Guid conversationId, Guid userId)
    {
      var currentConversationMember = _masterDbContext.ConversationMembers.FirstOrDefault(x => x.ConversationId == conversationId && x.MemberId == userId);
      if (currentConversationMember == null)
      {
        throw new BadRequestException(ErrorMessages.Conversation_NotFound);
      }

      // Unblock Conversation
      currentConversationMember.Status = currentConversationMember.PreviousStatus.HasValue ?
        currentConversationMember.PreviousStatus.Value
        : (byte)ConversationMemberStatus.Accept;
      currentConversationMember.UpdatedDate = DateTime.UtcNow;
      currentConversationMember.UpdatedBy = userId;

      // Delete block entity
      var blockEntity = _masterDbContext.Blocks.FirstOrDefault(x => x.ConversationId == conversationId && x.Blocker == userId);
      if (blockEntity == null)
        return false;

      blockEntity.IsDeleted = true;
      blockEntity.DeletedDate = DateTime.UtcNow;
      blockEntity.DeletedBy = userId;

      bool result = false;
      using var transaction = _masterDbContext.Database.BeginTransaction();
      try
      {
        _masterDbContext.ConversationMembers.UpdateRange(currentConversationMember);
        _masterDbContext.Blocks.Update(blockEntity);

        result = _masterDbContext.SaveChanges() > 0;

        transaction.Commit();
      }
      catch (Exception ex)
      {
        transaction.Rollback();
        throw new BadRequestException(ex.Message);
      }

      return result;
    }

    public bool AcceptPendingConversation(Guid conversationId, Guid userId)
    {
      var pendingRequest = _masterDbContext.ConversationMembers.
        FirstOrDefault(x => x.ConversationId == conversationId && x.MemberId == userId &&
                            x.Status == (byte)CoreEnums.ConversationMemberStatus.Pending);
      if (pendingRequest == null)
      {
        throw new BadRequestException(ErrorMessages.Conversation_RequestNotFound);
      }

      pendingRequest.PreviousStatus = pendingRequest.Status;
      pendingRequest.Status = (byte)ConversationMemberStatus.Accept;
      _masterDbContext.ConversationMembers.Update(pendingRequest);
      _masterDbContext.SaveChanges();

      PushAcceptConversationNotification(pendingRequest.ConversationId, userId, false);
      return true;
    }

    public int GetTotalMessages(Guid conversationId, Guid userId)
    {
      return _replicaContext.Messages.Count(x => x.ConversationId == conversationId && x.CreatedUser == userId);
    }

    #region Private methods

    private void BuildResultData(ConversationDetailModel detailModel, Guid currentUserId, bool useMasterDb)
    {
      SetConversationMembers(detailModel, useMasterDb);
      SetConversationName(detailModel, currentUserId);
      SetConversationStatus(detailModel);
    }

    private void BuildResultData(List<ConversationBlockedListViewModel> data, bool useMasterDb)
    {
      SetNickNameBlocked(data, useMasterDb);
    }

    private void BuildResultData(List<ConversationListViewModel> data, Guid currentUserId, bool useMasterDb)
    {
      SetLatestMessages(data, useMasterDb);
      SetConversationMembers(data, currentUserId, useMasterDb);
      SetConversationStatus(data, currentUserId);
      SetNewMessageStatus(data, currentUserId);
    }

    private void SetLatestMessages(List<ConversationListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaContext;
      var conversationIds = data.Select(x => x.Id).ToList();
      var mapData = dbContext.Messages
        .Where(x => conversationIds.Contains(x.ConversationId))
        .GroupBy(x => x.ConversationId)
        .Select(x => new
        {
          ConversationId = x.Key,
          LatestMessage = x.OrderByDescending(q => q.CreatedDate).FirstOrDefault()
        })
        .ToList();

      data.ForEach(x =>
      {
        var item = mapData.FirstOrDefault(y => x.Id == y.ConversationId);
        x.LatestMessage = item != null
          && item.LatestMessage != null
          && item.LatestMessage.CreatedDate >= x.DeletionDate.GetValueOrDefault()
            ? new ConversationMessageModel(item.LatestMessage) : null;
      });
    }

    private void SetConversationMembers(ConversationDetailModel detailModel, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaContext;
      var mapData = dbContext.ConversationMembers.Where(x => x.ConversationId == detailModel.Id)
        .Join(dbContext.Users,
          conversationMember => conversationMember.MemberId,
          user => user.UserId,
          (conversationMember, user) => new ConversationMemberModel
          {
            Id = conversationMember.MemberId,
            ConversationId = conversationMember.ConversationId,
            IsHasNewMessage = conversationMember.IsHasNewMessage,
            NickName = user.NickName,
            Status = conversationMember.Status
          }).ToList();
      detailModel.Members = mapData;
    }

    private void SetConversationName(ConversationDetailModel detailModel, Guid currentUserId)
    {
      var nicknames = detailModel.Members.Where(x => x.Id != currentUserId).Select(x => x.NickName ?? "").ToList();
      detailModel.Name = string.IsNullOrEmpty(detailModel.Name) && detailModel.Members.Any()
        ? string.Join(',', nicknames)
        : detailModel.Name;
    }

    private void SetConversationStatus(ConversationDetailModel detailModel)
    {
      detailModel.Status = detailModel.Members.Any(x => x.Status == (byte)CoreEnums.ConversationMemberStatus.Pending)
        ? (byte)CoreEnums.ConversationMemberStatus.Pending
        : (byte)CoreEnums.ConversationMemberStatus.Accept;
    }

    private void SetConversationStatus(List<ConversationListViewModel> data, Guid currentUserId)
    {
      data.ForEach(item =>
      {
        if (item.Members.Any())
        {
          item.Status = item.Members.FirstOrDefault(x => x.Id == currentUserId)?.Status ??
                        (byte)CoreEnums.ConversationMemberStatus.Accept;
        }
      });
    }

    private void SetNewMessageStatus(List<ConversationListViewModel> data, Guid currentUserId)
    {
      data.ForEach(item =>
      {
        SetNewMessageStatus(item, currentUserId);
      });
    }

    private void SetNewMessageStatus(ConversationListViewModel data, Guid currentUserId)
    {
      data.IsHasNewMessage = data.Members
        .FirstOrDefault(x => x.ConversationId == data.Id && x.Id == currentUserId)?.IsHasNewMessage
                             ?? false;
    }

    private void SetConversationMembers(List<ConversationListViewModel> data, Guid currentUserId, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaContext;
      var conversationIds = data.Select(x => x.Id).ToList();
      var mapData = dbContext.ConversationMembers
        .Where(x => conversationIds.Contains(x.ConversationId))
        .Join(dbContext.Users,
          conversationMember => conversationMember.MemberId,
          user => user.UserId,
          (conversationMember, user) => new ConversationMemberModel()
          {
            Id = conversationMember.MemberId,
            ConversationId = conversationMember.ConversationId,
            IsHasNewMessage = conversationMember.IsHasNewMessage,
            NickName = user.NickName,
            Status = conversationMember.Status
          }).ToList();
      data.ForEach(x =>
      {
        var members = mapData.Where(y => y.ConversationId == x.Id).ToList();
        if (members.Any())
        {
          x.Name = string.Join(",", members.Where(x => x.Id != currentUserId).Select(x => x.NickName).ToList());
          x.Members = members;
        }
      });
    }

    private void SetNickNameBlocked(List<ConversationBlockedListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaContext;
      var listBlockedId = data.Select(x => x.BlockedUserId).ToList();
      var mapData = dbContext.Users.Where(x => listBlockedId.Contains(x.UserId))
        .ToDictionary(x => x.UserId, x => x.NickName);

      data.ForEach(x =>
      {
        x.NickName = mapData.GetValueOrDefault(x.BlockedUserId);
      });
    }

    private IQueryable<MessageEntity> BuildQueryable(ConversationMessageFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaContext;
      var query = dbContext.Messages.AsNoTracking();
      query = ConversationMessageQueryBuilder<MessageEntity>.BuildWhereClause(model, query);
      return query;
    }

    private IQueryable<ConversationMemberEntity> BuildQueryable(ConversationFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaContext;
      var query = dbContext.ConversationMembers.AsNoTracking();
      query = ConversationMemberQueryBuilder<ConversationMemberEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    private IQueryable<BlockEntity> BuildQueryable(ConversationBlockFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaContext;
      var query = dbContext.Blocks.AsNoTracking();
      query = BlockQueryBuilder<BlockEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    private Guid? GetDirectMessageConversation(Guid fromUserId, Guid toUserId, Guid schoolId)
    {
      // Get conversation and make sure current user have permission to view
      var conversationMember = _replicaContext.ConversationMembers
        .Where(x => x.MemberId == toUserId || x.MemberId == fromUserId)
        .GroupBy(x => x.ConversationId)
        .Select(x => new
        {
          ConversationId = x.Key,
          ConversationMemberCount = x.Count()
        })
        .FirstOrDefault(x => x.ConversationMemberCount >= 2);

      return conversationMember?.ConversationId;
    }

    private ConversationEntity? GetConversation(Guid conversationId, Guid currentUserId, Guid schoolId)
    {
      // Get conversation and make sure current user have permission to view
      return _replicaContext.Conversations
        .Where(x => x.Guid == conversationId && x.SchoolId == schoolId)
          .Join(_replicaContext.ConversationMembers,
          conversation => conversation.Guid,
          conversationMembers => conversationMembers.ConversationId,
          (conversation, conversationMembers) => new { conversation, conversationMembers })
        .Where(x => x.conversationMembers.MemberId == currentUserId)
        .Select(x => x.conversation)
        .FirstOrDefault();
    }

    private ConversationMemberEntity? GetConversationMember(Guid conversationId, Guid currentUserId)
    {
      // Get conversation and make sure current user have permission to view
      return _replicaContext.ConversationMembers
        .FirstOrDefault(x => x.ConversationId == conversationId && x.MemberId == currentUserId);
    }

    private Guid CreateNewConversation(Guid fromUserId, Guid toUserId, Guid schoolId)
    {
      var currentTime = DateTime.UtcNow;
      var conversation = new ConversationEntity
      {
        Guid = Guid.NewGuid(),
        Name = null,
        CreatedDate = currentTime,
        CreatedUser = fromUserId,
        Type = (byte)CoreEnums.ConversationType.DirectMessage,
        SchoolId = schoolId,
      };

      var conversationMembers = new List<ConversationMemberEntity>()
      {
        new ConversationMemberEntity
        {
          Guid = Guid.NewGuid(),
          ConversationId = conversation.Guid,
          MemberId = fromUserId,
          Status = (byte)CoreEnums.ConversationMemberStatus.Accept,
          IsHasNewMessage = false,
          CreatedDate = currentTime,
          SchoolId = schoolId,
          LatestActivityDateTime = DateTime.UtcNow
        },
        new ConversationMemberEntity
        {
          Guid = Guid.NewGuid(),
          ConversationId = conversation.Guid,
          MemberId = toUserId,
          Status = (byte)CoreEnums.ConversationMemberStatus.Pending,
          IsHasNewMessage = false,
          CreatedDate = currentTime,
          SchoolId = schoolId,
          LatestActivityDateTime = DateTime.UtcNow
        },
      };

      using var transaction = _masterDbContext.Database.BeginTransaction();
      try
      {
        _masterDbContext.Conversations.Add(conversation);
        _masterDbContext.SaveChanges();

        _masterDbContext.ConversationMembers.AddRange(conversationMembers);
        _masterDbContext.SaveChanges();

        transaction.Commit();
        return conversation.Guid;
      }
      catch (Exception e)
      {
        _logger.LogError(e.ToString());
        transaction.Rollback();
        throw;
      }
    }

    private void ValidatePendingMessage(Guid conversationId, Guid currentUserId)
    {
      var numberOfMessage = int.Parse(_configuration["ChatConfig:NumberOfPendingMessage"]);
      var totalMessages =
        _replicaContext.Messages.Count(x => x.ConversationId == conversationId && x.CreatedUser == currentUserId);
      if (totalMessages >= numberOfMessage)
        throw new BadRequestException(ErrorMessages.Conversation_ReachNumberOfPendingMessage);
    }

    private void PushNewMessageToChatList(Guid conversationId, Guid currentLoginUserId, bool userMasterDb)
    {
      var conversation = _masterDbContext.Conversations.FirstOrDefault(x => x.Guid == conversationId);
      if (conversation == null) return;
      var model = new ConversationListViewModel(conversation);
      BuildResultData(new List<ConversationListViewModel> { model }, currentLoginUserId, userMasterDb);
      foreach (var userId in model.Members.Select(x => x.Id).Distinct())
      {
        List<string> connectionIds = _webSocketService.GetConnectionIdsByUserIdAsync(userId).Result;
        if (connectionIds.Any())
        {
          SetNewMessageStatus(model, userId);
          var notificationInput = new NotificationInputModel
          {
            ConnectionIds = connectionIds.Distinct().ToList(),
            Action = EnumExtensions.GetName<WebsocketAction>((byte)WebsocketAction.NewMessageToChatList),
            Payload = JsonUtils.ToJson(model)
          };
          _webSocketService.SendNotification(notificationInput).GetAwaiter();
        }
      }
    }

    private void PushNewMessageToChatNotification(List<Guid> userIds)
    {
      if (!userIds.Any()) return;

      var connectionIds = new List<string>();
      foreach (var userId in userIds.Distinct())
      {
        connectionIds.AddRange(_webSocketService.GetConnectionIdsByUserIdAsync(userId).Result);
        if (connectionIds.Any())
        {
          var notificationInput = new NotificationInputModel
          {
            ConnectionIds = connectionIds.Distinct().ToList(),
            Action = EnumExtensions.GetName<WebsocketAction>((byte)WebsocketAction.NewMessageToChatNotification),
            Payload = "New Message To Chat Notification"
          };
          _webSocketService.SendNotification(notificationInput).GetAwaiter();
        }
      }
    }

    private void SendWebsocketConversationBlocked(Guid blockedUserId, Guid conversationId)
    {
      var connectionIds = _webSocketService.GetConnectionIdsByUserIdAsync(blockedUserId).Result;
      if (connectionIds.Any())
      {
        NotificationInputModel notificationInput = new NotificationInputModel()
        {
          ConnectionIds = connectionIds.Distinct().ToList(),
          Action = EnumExtensions.GetName<WebsocketAction>((byte)WebsocketAction.ConversationBlocked),
          Payload = JsonUtils.ToJson(new { ConversationId = conversationId })
        };
        _webSocketService.SendNotification(notificationInput).GetAwaiter();
      }
    }

    private void SendWebsocketConversationDeleted(Guid userId, Guid conversationId)
    {
      var connectionIds = _webSocketService.GetConnectionIdsByUserIdAsync(userId).Result;
      if (connectionIds.Any())
      {
        NotificationInputModel notificationInput = new NotificationInputModel()
        {
          ConnectionIds = connectionIds.Distinct().ToList(),
          Action = EnumExtensions.GetName<WebsocketAction>((byte)WebsocketAction.ConversationDeleted),
          Payload = JsonUtils.ToJson(new { ConversationId = conversationId })
        };
        _webSocketService.SendNotification(notificationInput).GetAwaiter();
      }
    }

    private void PushAcceptConversationNotification(Guid conversationId, Guid currentLoginUser, bool userMasterDb)
    {
      var conversation = _masterDbContext.Conversations.FirstOrDefault(x => x.Guid == conversationId);
      if (conversation == null) return;

      var model = new ConversationListViewModel(conversation);
      BuildResultData(new List<ConversationListViewModel> { model }, currentLoginUser, userMasterDb);
      foreach (var userId in model.Members.Select(x => x.Id).Distinct())
      {
        var connectionIds = new List<string>();
        connectionIds.AddRange(_webSocketService.GetConnectionIdsByUserIdAsync(userId).Result);
        if (connectionIds.Any())
        {
          var notificationInput = new NotificationInputModel
          {
            ConnectionIds = connectionIds.Distinct().ToList(),
            Action = EnumExtensions.GetName<WebsocketAction>((byte)WebsocketAction.ConversationAccepted),
            Payload = JsonUtils.ToJson(model)
          };
          _webSocketService.SendNotification(notificationInput).GetAwaiter();
        }
      }
    }

    private byte UpdateConversationMemberStatusWhenSendNewMessage(ConversationMemberEntity entity)
    {
      if (entity.Status == (byte)ConversationMemberStatus.Delete)
        entity.Status = entity.PreviousStatus.HasValue ? entity.PreviousStatus.Value : (byte)ConversationMemberStatus.Accept;

      return entity.Status;
    }

    #endregion
  }
}

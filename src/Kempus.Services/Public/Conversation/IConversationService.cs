using Kempus.Core.Models;
using Kempus.Models.Public.Conversation;

namespace Kempus.Services.Public.Conversation
{
  public interface IConversationService
  {
    Guid Create(ConversationInputModel model);

    ConversationDetailModel GetDetail(Guid conversationId, Guid currentUserId, Guid schoolId);

    PagingResult<ConversationListViewModel> SearchConversations(ConversationFilterModel filter, Pageable pageable,
      Guid currentUserId, bool useMasterDb);

    ConversationMessageListViewModel SendMessage(ConversationMessageInputModel input);

    PagingResult<ConversationMessageListViewModel> SearchMessages(ConversationMessageFilterModel filter,
      Pageable pageable, Guid currentUserId, bool useMasterDb);

    int GetTotalPending(Guid userId);

    int GetTotalUnread(Guid userId);

    bool Delete(Guid conversationId, Guid userId);

    PagingResult<ConversationBlockedListViewModel> SearchConversationBlocked(ConversationBlockFilterModel filter, Pageable pageable, bool useMasterDb);

    bool Block(ConversationBlockInputModel input);

    bool Unblock(Guid conversationId, Guid userId);

    bool AcceptPendingConversation(Guid conversationId, Guid userId);

    int GetTotalMessages(Guid conversationId, Guid userId);
  }
}

﻿using Kempus.Models.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Public.SignalR
{
  public interface ISignalRService
  {
    Task SendAll(string hubName, string methodName, object data);
    Task SendAllDataList(string hubName, string methodName, List<object> dataList);
    Task SendGroup(string hubName, string groupName, string methodName, object data);
    Task SendGroupDataList(string hubName, string groupName, string methodName, List<object> dataList);
    Task SendUsers(string hubName, List<int> userIds, string methodName, object data);
    Task SendUsers(string hubName, string methodName, List<SignalrSendUserModel> dataUsers);
    Task SendUser(string hubName, int userId, string methodName, object data);
  }
}
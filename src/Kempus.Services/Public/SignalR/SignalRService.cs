﻿using Kempus.Core.Constants;
using Kempus.Core.Models.Configuration;
using Kempus.Core.Utils;
using Kempus.Models.SignalR;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Text;

namespace Kempus.Services.Public.SignalR
{
  public class SignalRService : ISignalRService
  {
    private const string ControllerEndpoint = "/api/SignalR/SignalRHandler";
    private readonly ILogger _logger;
    private readonly HttpClient _httpClient;

    public SignalRService(IConfiguration configuration, ILogger<SignalRService> logger, PrivateAppInfo privateAppInfo)
    {
      _logger = logger;
      _httpClient = new HttpClient
      {
        BaseAddress = new Uri(configuration["DomainWeb:SignalR"]),
        Timeout = TimeSpan.FromSeconds(90)
      };
      _httpClient.DefaultRequestHeaders.Add(CoreConstants.Headers.KempusInternalApiKey,
        privateAppInfo.KempusInternalApiKey);
    }

    public Task SendAll(string hubName, string methodName, object data)
    {
      return SendAllDataList(hubName, methodName, new List<object> { data });
    }

    public async Task SendAllDataList(string hubName, string methodName, List<object> dataList)
    {
      if (dataList == null || !dataList.Any()) return;
      var endpoint = ControllerEndpoint + "/all";
      var parameters = new Dictionary<string, string>();
      parameters.Add("hubName", hubName);
      parameters.Add("methodName", methodName);
      using var request = new HttpRequestMessage(HttpMethod.Post, QueryHelpers.AddQueryString(endpoint, parameters))
      {
        Content = new StringContent(JsonUtils.ToJson(dataList), Encoding.UTF8,
          CoreConstants.HttpRequestMediaType.Json)
      };
      using var response = await _httpClient.SendAsync(request);
      if (!response.IsSuccessStatusCode)
      {
        _logger.LogError("Fail to call SignalR server. " + request.RequestUri + " " +
                         response.StatusCode + response.Content.ReadAsStringAsync().Result);
      }
    }

    public Task SendGroup(string hubName, string groupName, string methodName, object data)
    {
      return SendGroupDataList(hubName, groupName, methodName, new List<object> { data });
    }

    public async Task SendGroupDataList(string hubName, string groupName, string methodName, List<object> dataList)
    {
      if (dataList == null || !dataList.Any()) return;
      var endpoint = ControllerEndpoint + "/group";
      var parameters = new Dictionary<string, string>();
      parameters.Add("hubName", hubName);
      parameters.Add("groupName", groupName);
      parameters.Add("methodName", methodName);
      using var request = new HttpRequestMessage(HttpMethod.Post, QueryHelpers.AddQueryString(endpoint, parameters))
      {
        Content = new StringContent(JsonUtils.ToJson(dataList), Encoding.UTF8,
          CoreConstants.HttpRequestMediaType.Json)
      };
      using var response = await _httpClient.SendAsync(request);
      if (!response.IsSuccessStatusCode)
      {
        _logger.LogError("Fail to call SignalR server. " + request.RequestUri + " " +
                         response.StatusCode + response.Content.ReadAsStringAsync().Result);
      }
    }

    public async Task SendUsers(string hubName, List<int> userIds, string methodName, object data)
    {
      var endpoint = ControllerEndpoint + "/users";
      var parameters = new Dictionary<string, string>();
      parameters.Add("hubName", hubName);
      parameters.Add("methodName", methodName);
      using var request = new HttpRequestMessage(HttpMethod.Post, QueryHelpers.AddQueryString(endpoint, parameters))
      {
        Content = new StringContent(JsonUtils.ToJson(new SignalrSendUsersModel
          {
            Data = data,
            UserIds = userIds
          }), Encoding.UTF8,
          CoreConstants.HttpRequestMediaType.Json)
      };
      using var response = await _httpClient.SendAsync(request);
      if (!response.IsSuccessStatusCode)
      {
        _logger.LogError("Fail to call SignalR server. " + request.RequestUri + " " +
                         response.StatusCode + response.Content.ReadAsStringAsync().Result);
      }
    }

    public async Task SendUsers(string hubName, string methodName, List<SignalrSendUserModel> dataUsers)
    {
      var endpoint = ControllerEndpoint + "/users-different-data";
      var parameters = new Dictionary<string, string>();
      parameters.Add("hubName", hubName);
      parameters.Add("methodName", methodName);
      using var request = new HttpRequestMessage(HttpMethod.Post, QueryHelpers.AddQueryString(endpoint, parameters))
      {
        Content = new StringContent(JsonUtils.ToJson(dataUsers), Encoding.UTF8,
          CoreConstants.HttpRequestMediaType.Json)
      };
      using var response = await _httpClient.SendAsync(request);
      if (!response.IsSuccessStatusCode)
      {
        _logger.LogError("Fail to call SignalR server. " + request.RequestUri + " " +
                         response.StatusCode + response.Content.ReadAsStringAsync().Result);
      }
    }

    public Task SendUser(string hubName, int userId, string methodName, object data)
    {
      return SendUsers(hubName, new List<int> { userId }, methodName, data);
    }
  }
}
﻿using Kempus.Core.Enums;
using Kempus.Entities;
using Kempus.EntityFramework;

namespace Kempus.Services.Public.CourseReviewActivitySummary
{
  public class CourseReviewActivitySummaryService : ICourseReviewActivitySummaryService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;

    public CourseReviewActivitySummaryService(
      MasterDbContext masterDbContext,
      ReplicaDbContext replicaDbContext)
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
    }

    public void CalculateCourseReviewActivity(Guid schoolId)
    {
      var totalActivities = _replicaDbContext.CourseReview6MonthActivities
        .Where(x => x.SchoolId == schoolId)
        .GroupBy(x => x.CourseReviewId)
        .Select(x => new
        {
          CourseReviewId = x.Key,
          Total24HActivity = x.LongCount()
        })
        .ToDictionary(x => x.CourseReviewId, x => x.Total24HActivity);
      if (!totalActivities.Any()) return;

      var latestLikeOn = _replicaDbContext.CourseReview6MonthActivities
        .Where(x => x.SchoolId == schoolId &&
                    x.ActionId == (byte)CoreEnums.CourseReviewActivity.Like)
        .GroupBy(x => x.CourseReviewId)
        .Select(x => new
        {
          CourseReviewId = x.Key,
          LatestLikeOn = x.Max(x => x.CreatedDate)
        })
        .ToDictionary(x => x.CourseReviewId, x => x.LatestLikeOn);

      var latestViewOn = _replicaDbContext.CourseReview6MonthActivities
        .Where(x => x.SchoolId == schoolId &&
                    x.ActionId == (byte)CoreEnums.CourseReviewActivity.View)
        .GroupBy(x => x.CourseReviewId)
        .Select(x => new
        {
          CourseReviewId = x.Key,
          latestViewOn = x.Max(x => x.CreatedDate)
        })
        .ToDictionary(x => x.CourseReviewId, x => x.latestViewOn);

      var courseReviewIds = totalActivities.Select(x => x.Key).ToList();
      var updateList = _replicaDbContext.CourseReviewActivitySummaries
        .Where(x => courseReviewIds.Contains(x.CourseReviewId)).ToList();
      var existingSummaryIds = updateList.Select(x => x.CourseReviewId).ToList();
      if (updateList.Any())
      {
        foreach (var summary in updateList)
        {
          var totalActivitiesIn6Month = totalActivities.GetValueOrDefault(summary.CourseReviewId);
          var latestLikeDateTime = latestLikeOn.GetValueOrDefault(summary.CourseReviewId);
          var latestViewDateTime = latestViewOn.GetValueOrDefault(summary.CourseReviewId);
          summary.TotalActivitiesIn6Month = totalActivitiesIn6Month != 0 ? totalActivitiesIn6Month : summary.TotalActivitiesIn6Month;
          summary.LatestLikeOn = latestLikeDateTime != default ? latestLikeDateTime : summary.LatestLikeOn;
          summary.LatestViewOn = latestViewDateTime != default ? latestViewDateTime : summary.LatestViewOn;
        }
        _masterDbContext.UpdateRange(updateList);
      }

      List<CourseReviewActivitySummaryEntity> insertList = new();
      var insertCourseReviewIds = courseReviewIds.Where(x => !existingSummaryIds.Contains(x)).ToList();
      if (insertCourseReviewIds.Any())
      {
        foreach (var insertCourseReviewId in insertCourseReviewIds)
        {
          var summaryEntity = new CourseReviewActivitySummaryEntity();
          summaryEntity.CourseReviewId = insertCourseReviewId;
          summaryEntity.TotalActivitiesIn6Month = totalActivities.GetValueOrDefault(insertCourseReviewId);
          summaryEntity.LatestLikeOn = latestLikeOn.GetValueOrDefault(insertCourseReviewId);
          summaryEntity.LatestViewOn = latestViewOn.GetValueOrDefault(insertCourseReviewId);
          summaryEntity.SchoolId = schoolId;
          summaryEntity.CreatedDate = DateTime.Now;
          insertList.Add(summaryEntity);
        }

        _masterDbContext.AddRange(insertList);
      }

      _masterDbContext.SaveChanges();
    }
  }
}

﻿using Kempus.Models.Public.CourseReviewActivity;

namespace Kempus.Services.Public.CourseReviewActivitySummary
{
  public interface ICourseReviewActivitySummaryService
  {
    void CalculateCourseReviewActivity(Guid schoolId);
  }
}

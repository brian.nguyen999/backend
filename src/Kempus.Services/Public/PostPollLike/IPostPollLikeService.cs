﻿using Kempus.Models.Public.PostPollLike;

namespace Kempus.Services.Public.PostPollLike
{
  public interface IPostPollLikeService
  {
    Guid CreateIfNotExits(PostPollLikeInputModel model);
    void Delete(PostPollLikeInputModel model);
    void SetLike(List<IHasLike> data, Guid userId, bool useMasterDb);
    void SetLike(IHasLike data, Guid userId, bool useMasterDb);
    void SetNumberOfLikes(IHasNumberOfLikes data, bool useMasterDb);
    void SetNumberOfLikes(List<IHasNumberOfLikes> data, bool useMasterDb);
  }
}

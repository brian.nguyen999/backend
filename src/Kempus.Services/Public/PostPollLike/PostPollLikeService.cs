﻿using Kempus.Core.Enums;
using Kempus.Core.Errors;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.PostPollActivity;
using Kempus.Models.Public.PostPollLike;
using Kempus.Services.Public.PostPollActivity;

namespace Kempus.Services.Public.PostPollLike
{
  public class PostPollLikeService : IPostPollLikeService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly IPostPollActivityService _postPollActivityService;

    public PostPollLikeService(
      MasterDbContext masterDbContext,
      ReplicaDbContext replicaDbContext,
      IPostPollActivityService postPollActivityService
      )
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
      _postPollActivityService = postPollActivityService;
    }

    public Guid CreateIfNotExits(PostPollLikeInputModel model)
    {
      Guid postId;
      if (model.ObjectType == CoreEnums.PostType.Post)
      {
        var postEntity = _replicaDbContext.Posts.FirstOrDefault(x =>
          x.Guid == model.ObjectId && x.Type == (byte)model.ObjectType);
        if (postEntity == null)
          throw new NotFoundException(ErrorMessages.Post_NotFound);
        postId = postEntity.Guid;
      }
      else
      {
        var pollEntity = _replicaDbContext.Polls.FirstOrDefault(x =>
          x.PostId == model.ObjectId);
        if (pollEntity == null)
          throw new NotFoundException(ErrorMessages.Poll_NotFound);
        postId = pollEntity.PostId;
      }

      var postPollLikeEntity = _replicaDbContext.PostPollLikes
        .FirstOrDefault(x => x.ObjectId == postId &&
                             x.ObjectType == (byte)model.ObjectType &&
                             x.UserId == model.UserId);
      if (postPollLikeEntity == null)
      {
        using var transaction = _masterDbContext.Database.BeginTransaction();
        try
        {
          postPollLikeEntity = new PostPollLikeEntity
          {
            ObjectId = model.ObjectId,
            ObjectType = (byte)model.ObjectType,
            UserId = model.UserId,
            CreatedDate = DateTime.UtcNow
          };

          _masterDbContext.PostPollLikes.Add(postPollLikeEntity);
          _masterDbContext.SaveChanges();
          _postPollActivityService.Create(new PostPollActivityInputModel
          {
            ObjectId = postId,
            ObjectType = model.ObjectType,
            UserId = model.UserId,
            ActionId = CoreEnums.PostPollActivity.Like,
            SchoolId = model.SchoolId,
            CreatedDate = DateTime.UtcNow
          });
          transaction.Commit();
        }
        catch (Exception)
        {
          transaction.Rollback();
          throw;
        }
      }

      return postPollLikeEntity.Guid;
    }

    public void Delete(PostPollLikeInputModel model)
    {
      Guid postId;
      if (model.ObjectType == CoreEnums.PostType.Post)
      {
        var postEntity = _replicaDbContext.Posts.FirstOrDefault(x =>
          x.Guid == model.ObjectId && x.Type == (byte)model.ObjectType);
        if (postEntity == null)
          throw new NotFoundException(ErrorMessages.Post_NotFound);
        postId = postEntity.Guid;
      }
      else
      {
        var pollEntity = _replicaDbContext.Polls.FirstOrDefault(x =>
          x.PostId == model.ObjectId);
        if (pollEntity == null)
          throw new NotFoundException(ErrorMessages.Poll_NotFound);
        postId = pollEntity.PostId;
      }

      var postPollLikeEntity = _replicaDbContext.PostPollLikes
        .FirstOrDefault(x => x.ObjectId == postId &&
                             x.ObjectType == (byte)model.ObjectType &&
                             x.UserId == model.UserId);
      if (postPollLikeEntity != null)
      {
        _masterDbContext.PostPollLikes.Remove(postPollLikeEntity);
        _masterDbContext.SaveChanges();
      }
    }

    public void SetLike(List<IHasLike> data, Guid userId, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var objectIds = data.Select(x => x.Id);
      var bookmarkList = dbContext.PostPollLikes
        .Where(x => objectIds.Contains(x.ObjectId) && x.UserId == userId)
        .Select(x => x.ObjectId)
        .ToList();
      if (bookmarkList.Any())
      {
        data.ForEach(x =>
        {
          x.IsLiked = bookmarkList.Any(q => q == x.Id);
        });
      }
    }

    public void SetLike(IHasLike data, Guid userId, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var objectId = data.Id;
      var bookmark = dbContext.PostPollLikes
        .FirstOrDefault(x => objectId == x.ObjectId && x.UserId == userId);
      if (bookmark != null)
      {
        data.IsLiked = true;
      }
    }

    public void SetNumberOfLikes(List<IHasNumberOfLikes> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var postIds = data.Select(x => x.Id).ToList();
      var mapData = dbContext.PostPollLikes
        .Where(x => postIds.Contains(x.ObjectId))
        .GroupBy(x => x.ObjectId)
        .Select(x => new
        {
          Id = x.Key,
          NumberOfLikes = x.Count()
        })
        .ToDictionary(x => x.Id, x => x.NumberOfLikes);

      data.ForEach(x =>
      {
        x.NumberOfLike = mapData.GetValueOrDefault(x.Id);
      });
    }

    public void SetNumberOfLikes(IHasNumberOfLikes data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var postId = data.Id;
      var numberOfLikes = dbContext.PostPollLikes.Count(x => x.ObjectId == postId);
      data.NumberOfLike = numberOfLikes;
    }
  }
}

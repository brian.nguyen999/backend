﻿using Kempus.Core.Models;
using Kempus.Entities;
using Kempus.Models.Public.Authentication;
using Kempus.Models.Public.User;
using Kempus.Models.Public.User.Request;
using Kempus.Models.Public.User.Response;

namespace Kempus.Services.Public.User
{
  public interface IUserService
  {
    #region Site
    Task<bool> InsertAsync(UserInputModel input);
    void ValidateUserInput(UserInputModel input);
    Task<bool> ResetPasswordByEmailAsync(ResetPasswordRequestDto dto);
    Task ForgotPasswordAsync(ForgotPasswordRequestDto dto);
    Task<string> CreateReferralCodeAsync();
    Task<bool> CheckReferralCodeExistAsync(string referralCode);
    Task<bool> SigninAsync(LoginInputModel request);
    Task<Guid> HandleSigninSuccessAsync(string userName);
    Task<int> HandleSigninFailedAsync(string userName);
    Task<ApplicationUser> GetUserInfoByUsernameAsync(string userName);
    Task<string> CreateNickNameAsync();
    UserProfileResponseDto GetUserProfile(Guid userId);
    Task<ApplicationUser> GetUserInfoByReferralCodeAsync(string referralCode);
    UserReferralDetailResponseDto GetUserReferralDetail(Guid userId);
    Task<string> UpdateNicknameAsync(Guid userId);
    Task<ApplicationUser> GetUserByIdAsync(Guid userId);
    Task UpdateAsync(Guid userId, UpdateProfileRequestDto input);
    Task ResetSitePasswordAsync(Guid userId);
    #endregion

    #region Admin
    Task<Guid> CreateAdminAsync(AdminInputModel input);
    PagingResult<AdminListViewModel> SearchAdmin(AdminFilterModel filter, Pageable pageable, bool useMasterDb);
    AdminDetailModel GetAdminDetail(Guid id);
    Task<bool> UpdateAdminAsync(AdminInputModel input);
    Task ResetPasswordAsync(Guid userId);
    PagingResult<UserListViewModel> SearchUser(UserFilterModel filter, Pageable pageable, bool useMasterDb);
    UserDetailModel GetUserDetail(Guid id);
    Guid UpdateUser(CmsUserInputModel input);
    Guid CreateUser(CmsUserInputModel input);

    #endregion

    #region Common
    Task<bool> CheckEmailExistAsync(string email);
    Task<bool> ChangePasswordAsync(ApplicationUser userEntity, string password);
    Task<bool> CheckUsernameExistAsync(string userName);
    Task<bool> CheckNickNameExistAsync(string nickName);
    #endregion
  }
}
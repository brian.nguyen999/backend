using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Core.Extensions;
using Kempus.Core.Models;
using Kempus.Core.Models.Configuration;
using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Authentication;
using Kempus.Models.Public.User;
using Kempus.Models.Public.User.Request;
using Kempus.Models.Public.User.Response;
using Kempus.Models.Public.UserReferralStatistic;
using Kempus.Services.Common;
using Kempus.Services.Common.KempTransaction;
using Kempus.Services.Public.Notification;
using Kempus.Services.Public.Otp;
using Kempus.Services.Public.School;
using Kempus.Services.Public.SchoolLifeTimeAnalytic;
using Kempus.Services.Public.UserReferralStatistic;
using Kempus.Services.Public.WaitList;
using Kempus.Services.QueryBuilder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Data;
using System.Net.Mail;
using static Kempus.Core.Enums.CoreEnums;
using DomainConfig = Kempus.Core.Models.Configuration.DomainConfig;

namespace Kempus.Services.Public.User
{

  public class UserService : IUserService
  {
    private readonly UserConfig _userConfig;
    private readonly IEmailNotificationService _emailNotificationService;
    private readonly IOtpService _otpService;
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly SignInManager<ApplicationUser> _signInManager;
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly ILogger _logger;
    private readonly DomainConfig _domainConfig;
    private readonly ISchoolService _schoolService;
    private readonly IWaitListService _waitListService;
    private readonly ISchoolLifeTimeAnalyticService _schoolLifeTimeAnalyticService;
    private readonly IKempTransactionCommonService _kempTransactionService;
    private readonly IUserReferralStatisticService _userReferralStatisticService;

    public UserService(
      IEmailNotificationService notificationService,
      IOtpService otpService,
      UserManager<ApplicationUser> userManager,
      SignInManager<ApplicationUser> signInManager,
      MasterDbContext masterDbContext,
      ILogger<UserService> logger,
      UserConfig userConfig,
      ReplicaDbContext replicaDbContext,
      DomainConfig domainConfig,
      ISchoolService schoolService,
      IWaitListService waitListService,
      ISchoolLifeTimeAnalyticService schoolLifeTimeAnalyticService,
      IKempTransactionCommonService kempTransactionService,
      IUserReferralStatisticService userReferralStatisticService)
    {
      _emailNotificationService = notificationService;
      _otpService = otpService;
      _userManager = userManager;
      _signInManager = signInManager;
      _masterDbContext = masterDbContext;
      _logger = logger;
      _userConfig = userConfig;
      _replicaDbContext = replicaDbContext;
      _domainConfig = domainConfig;
      _schoolService = schoolService;
      _waitListService = waitListService;
      _schoolLifeTimeAnalyticService = schoolLifeTimeAnalyticService;
      _kempTransactionService = kempTransactionService;
      _userReferralStatisticService = userReferralStatisticService;
    }

    #region Site
    public async Task<bool> InsertAsync(UserInputModel input)
    {
      ValidateUserInput(input);

      MailAddress address = new MailAddress(input.Email);
      string host = address.Host;
      var currentSchool = _schoolService.GetSchoolByDomain(host).Result;

      var referralCode = StringHelper.GenerateReferralCodeFromEmail(input.Email);
      var currentDate = DateTime.UtcNow;
      Guid? inviterId;
      var inviter = await GetUserInfoByReferralCodeAsync(input.ReferralCode);
      if (inviter != null)
      {
        inviterId = inviter.UserId;
      }
      else
      {
        var inviterEmail = StringHelper.GenerateEmailFromReferralCode(input.ReferralCode);
        inviterId = _replicaDbContext.WaitList.Where(x => x.Email == inviterEmail).Select(x => x.Guid).FirstOrDefault();
      }

      Guid newUserId = Guid.NewGuid();
      var userInWaitList = _replicaDbContext.WaitList.Where(x => x.Email == input.Email).FirstOrDefault();
      if (userInWaitList != null)
      {
        newUserId = userInWaitList.Guid;
      }

      var newUser = new ApplicationUser()
      {
        UserId = newUserId,
        Email = input.Email,
        EmailConfirmed = true,
        UserName = input.UserName,
        ReferralCode = referralCode,
        Status = (byte)UserStatus.Active,
        CreatedDate = currentDate,
        DegreeId = input.DegreeId,
        LastChangedDegree = input.DegreeId.HasValue ? currentDate : null,
        MajorId = input.MajorId,
        LastChangedMajor = input.MajorId.HasValue ? currentDate : null,
        ClassYear = input.ClassYear,
        LastChangeClassYear = input.ClassYear.HasValue ? currentDate : null,
        NickName = input.NickName,
        LastChangeNickname = currentDate,
        SchoolId = currentSchool.Guid,
        UserType = input.UserType,
        InviterReferralCode = input.ReferralCode,
        InviterGuid = inviterId,
      };

      IdentityResult result;
      using var transaction = await _masterDbContext.Database.BeginTransactionAsync();
      try
      {
        result = await _userManager.CreateAsync(newUser, input.Password);
        if (!result.Succeeded)
        {
          throw new BadRequestException(string.Join(", ", result.Errors.Select(e => e.Description)));
        }

        await transaction.CommitAsync();
      }
      catch (Exception ex)
      {
        transaction.Rollback();
        throw new BadRequestException(ex.Message);
      }

      if (result.Succeeded)
      {
        try
        {
          if (inviterId.HasValue)
          {
            // Create NewUserReferredSignedUp transaction. Add 3 Kemp
            _kempTransactionService.CreateTransaction(new Models.Common.KempTransactionInputModel()
            {
              UserIds = new List<Guid>() { inviterId.Value },
              TransactionType = (byte)KempTransactionType.NewUserReferredSignedUp,
              ReferenceGuid = inviterId.Value,
              Amount = KempTransactionValue.NewUserReferredSignedUp,
              Action = (byte)KempTransactionAction.Add,
              CreatorId = newUser.UserId,
              SchoolId = newUser.SchoolId
            });

            // Update Referral Statistic for inviter
            _userReferralStatisticService.Refresh(new UserReferralStatisticRefreshModel()
            {
              CreatedBy = newUser.UserId,
              InviterId = inviterId.Value,
              InviterReferralCode = input.ReferralCode
            });
          }

          // Delete old OTP
          await _otpService.DeleteOtpAsync(input.OtpDto);

          // Delete Email in Waitlist if exist
          _waitListService.Delete(input.Email);

          // Update Number of User for current school
          _schoolLifeTimeAnalyticService.RefreshNumber(currentSchool.Guid);

          // Create new Wallet
          _kempTransactionService.CreateWallet(newUser.UserId, newUser.UserId);

          // Create SignupSuccessfully transaction. Add 3 Kemp
          _kempTransactionService.CreateTransaction(new Models.Common.KempTransactionInputModel()
          {
            UserIds = new List<Guid>() { newUser.UserId },
            TransactionType = (byte)KempTransactionType.SignupSuccessfully,
            ReferenceGuid = newUser.UserId,
            Amount = KempTransactionValue.SignupSuccessfully,
            Action = (byte)KempTransactionAction.Add,
            CreatorId = newUser.UserId,
            SchoolId = newUser.SchoolId
          });
        }
        catch (Exception ex)
        {
          throw new BadRequestException(ex.InnerException != null ? ex.InnerException.Message : ex.Message);
        }
      }

      return result.Succeeded;
    }

    public async Task UpdateAsync(Guid userId, UpdateProfileRequestDto input)
    {
      var userEntity = await _masterDbContext.Users.FirstOrDefaultAsync(x => x.UserId == userId);
      if (userEntity == null)
        throw new NotFoundException(ErrorMessages.User_NotFound);

      if (input.ClassYear != null)
      {
        var lastChangeClassYear = userEntity.LastChangeClassYear;
        if (lastChangeClassYear.HasValue && lastChangeClassYear.Value.AddMinutes(_userConfig.NickNameLockdownMinutes) > DateTime.UtcNow)
        {
          var countdownTimeSpan = (lastChangeClassYear.Value.AddMinutes(_userConfig.NickNameLockdownMinutes) - DateTime.UtcNow);
          var countdownDays = countdownTimeSpan.TotalDays;
          if (countdownDays < 1)
          {
            var countdownHours = countdownTimeSpan.TotalHours;
            throw new BadRequestException(string.Format(ErrorMessages.User_ClassYearIsLockedForHours, Convert.ToInt32(countdownHours)));
          }
          else
          {
            throw new BadRequestException(string.Format(ErrorMessages.User_ClassYearIsLockedForDays, Convert.ToInt32(countdownDays)));
          }
        }

        userEntity.ClassYear = input.ClassYear;
        userEntity.LastChangeClassYear = DateTime.UtcNow;
      }

      if (input.IsHiddenClassYear != null)
      {
        userEntity.IsHiddenClassYear = input.IsHiddenClassYear;
      }

      if (input.DegreeId != null)
      {
        var lastChangedDegree = userEntity.LastChangedDegree;
        if (lastChangedDegree.HasValue && lastChangedDegree.Value.AddMinutes(_userConfig.NickNameLockdownMinutes) > DateTime.UtcNow)
        {
          var countdownTimeSpan = (lastChangedDegree.Value.AddMinutes(_userConfig.NickNameLockdownMinutes) - DateTime.UtcNow);
          var countdownDays = countdownTimeSpan.TotalDays;
          if (countdownDays < 1)
          {
            var countdownHours = countdownTimeSpan.TotalHours;
            throw new BadRequestException(string.Format(ErrorMessages.User_DegreeIsLockedForHours, Convert.ToInt32(countdownHours)));
          }
          else
          {
            throw new BadRequestException(string.Format(ErrorMessages.User_DegreeIsLockedForDays, Convert.ToInt32(countdownDays)));
          }
        }

        userEntity.DegreeId = input.DegreeId;
        userEntity.LastChangedDegree = DateTime.UtcNow;
      }

      if (input.IsHiddenDegree != null)
      {
        userEntity.IsHiddenDegree = input.IsHiddenDegree.Value;
      }

      if (input.MajorId != null)
      {
        var lastChangedMajor = userEntity.LastChangedMajor;
        if (lastChangedMajor.HasValue && lastChangedMajor.Value.AddMinutes(_userConfig.NickNameLockdownMinutes) > DateTime.UtcNow)
        {
          var countdownTimeSpan = (lastChangedMajor.Value.AddMinutes(_userConfig.NickNameLockdownMinutes) - DateTime.UtcNow);
          var countdownDays = countdownTimeSpan.TotalDays;
          if (countdownDays < 1)
          {
            var countdownHours = countdownTimeSpan.TotalHours;
            throw new BadRequestException(string.Format(ErrorMessages.User_MajorIsLockedForHours, Convert.ToInt32(countdownHours)));
          }
          else
          {
            throw new BadRequestException(string.Format(ErrorMessages.User_MajorIsLockedForDays, Convert.ToInt32(countdownDays)));
          }
        }

        userEntity.MajorId = input.MajorId;
        userEntity.LastChangedMajor = DateTime.UtcNow;
      }

      if (input.IsHiddenMajor != null)
      {
        userEntity.IsHiddenMajor = input.IsHiddenMajor.Value;
      }

      _masterDbContext.Users.Update(userEntity);
      _masterDbContext.SaveChanges();
    }

    public void ValidateUserInput(UserInputModel input)
    {
      // Check School
      MailAddress address = new MailAddress(input.Email);
      string host = address.Host;
      var currentSchool = _schoolService.GetSchoolByDomain(host).Result;
      if (currentSchool == null)
      {
        throw new BadRequestException(ErrorMessages.User_DomainIsNotSupported);
      }

      // Validate ReferralCode
      ValidateInviter(input.ReferralCode, host, currentSchool.IsRequiredReferralCode);

      // Validate Email
      var newObjectOtp = new OtpRequestDto()
      {
        ObjectType = (byte)OtpType.Email,
        ObjectId = input.Email
      };

      if (CheckEmailExistAsync(input.Email).Result ||
        (!input.IsRegister && _otpService.CheckObjectExistAsync(newObjectOtp).Result))
      {
        throw new BadRequestException(ErrorMessages.User_EmailHasTaken);
      }

      // Validate username and nickname
      if (input.IsRegister)
      {
        if (CheckUsernameExistAsync(input.UserName).Result)
        {
          throw new BadRequestException(ErrorMessages.User_UsernameHasTaken);
        }

        if (CheckNickNameExistAsync(input.NickName).Result)
        {
          throw new BadRequestException(ErrorMessages.User_NickNameHasTaken);
        }
      }
    }

    public async Task<ApplicationUser> GetUserInfoByUsernameAsync(string userName)
    {
      return await _masterDbContext.Users.FirstOrDefaultAsync(x => x.Email == userName || x.UserName == userName);
    }

    public async Task<bool> ChangePasswordAsync(ApplicationUser userEntity, string password)
    {
      var token = await _userManager.GeneratePasswordResetTokenAsync(userEntity);

      var result = await _userManager.ResetPasswordAsync(userEntity, token, password);

      return result.Succeeded;
    }

    public async Task<bool> ResetPasswordByEmailAsync(ResetPasswordRequestDto dto)
    {
      // Validate user
      var user = await _masterDbContext.Users.FirstOrDefaultAsync(x => x.Email == dto.Email && dto.UserTypes.Contains(x.UserType));
      if (user == null)
        throw new NotFoundException(ErrorMessages.User_NotFound);

      // Validate otp
      var objectOtp = new OtpRequestDto()
      {
        ObjectType = (byte)OtpType.Email,
        ObjectId = dto.Email,
        OtpCode = dto.OtpCode
      };
      await _otpService.CheckOtpValidAsync(objectOtp);

      // Reset new password
      var token = await _userManager.GeneratePasswordResetTokenAsync(user);
      var result = await _userManager.ResetPasswordAsync(user, token, dto.Password);
      if (!result.Succeeded)
      {
        throw new BadRequestException(string.Join(", ", result.Errors.Select(e => e.Description)));
      }
      else
      {
        await _otpService.DeleteOtpAsync(objectOtp);
      }

      return result.Succeeded;
    }

    public async Task<string> CreateReferralCodeAsync()
    {
      int retryTimes = 3;
      do
      {
        var generatedReferralCode = Utilities.StringHelper.RandomString();
        var isUsedReferralCode = await CheckReferralCodeExistAsync(generatedReferralCode);
        if (isUsedReferralCode == false)
          return generatedReferralCode;
        retryTimes--;
      } while (retryTimes > 0);

      throw new BadRequestException(ErrorMessages.User_ReferralCode_FailedToGenerate);
    }

    public async Task<string> CreateNickNameAsync()
    {
      int retryTimes = 3;
      do
      {
        var generatedNickname = Utilities.StringHelper.RandomString(6);
        var isUsedNickname = await CheckNickNameExistAsync(generatedNickname);
        if (isUsedNickname == false)
          return generatedNickname;
        retryTimes--;
      } while (retryTimes > 0);

      throw new BadRequestException(ErrorMessages.User_NickName_FailedToGenerate);
    }

    public async Task<bool> SigninAsync(LoginInputModel request)
    {
      var currentUser = await GetUserInfoByUsernameAsync(request.Username);
      if (currentUser == null)
      {
        throw new BadRequestException(ErrorMessages.User_NotFound);
      }

      if (currentUser.Status == (byte)UserStatus.Inactive)
      {
        throw new BadRequestException(ErrorMessages.User_NotFound);
      }

      var result = await _signInManager.PasswordSignInAsync(currentUser, request.Password, false, false);

      return result.Succeeded;
    }

    public async Task<int> HandleSigninFailedAsync(string userName)
    {
      using (var transaction = await _masterDbContext.Database.BeginTransactionAsync())
      {
        try
        {
          var currentUser = await GetUserInfoByUsernameAsync(userName);
          if (currentUser == null)
          {
            return 0;
          }

          var currentDate = DateTime.UtcNow;
          if (currentUser.AccessFailedCount < 4)
          {
            currentUser.AccessFailedCount++;
          }
          else
          {
            currentUser.LockoutEnabled = true;
            currentUser.LockoutEnd = currentDate.AddMinutes(5);
          }

          currentUser.UpdatedDate = currentDate;
          await _userManager.UpdateAsync(currentUser);
          await transaction.CommitAsync();
          return currentUser.AccessFailedCount;
        }
        catch (Exception ex)
        {
          transaction.Rollback();
          _logger.LogError(ex.Message);
          throw new BadRequestException(ex.Message);
        }
      }
    }

    public async Task<Guid> HandleSigninSuccessAsync(string userName)
    {
      using (var transaction = await _masterDbContext.Database.BeginTransactionAsync())
      {
        try
        {
          var currentUser = await GetUserInfoByUsernameAsync(userName);
          var currentDate = DateTime.UtcNow;

          currentUser.AccessFailedCount = 0;
          currentUser.LastAccessTime = currentDate;
          currentUser.UpdatedDate = currentDate;

          await _userManager.UpdateAsync(currentUser);
          await transaction.CommitAsync();
          return currentUser.UserId;
        }
        catch (Exception ex)
        {
          transaction.Rollback();
          _logger.LogError(ex.Message);
          throw new BadRequestException(ex.Message);
        }
      }
    }

    public async Task ForgotPasswordAsync(ForgotPasswordRequestDto dto)
    {
      // Validate user
      var user = await _masterDbContext.Users.FirstOrDefaultAsync(x => x.Email == dto.Email && dto.UserTypes.Contains(x.UserType));
      if (user == null)
        throw new NotFoundException(ErrorMessages.User_NotFound);

      var newObjectOtp = new OtpRequestDto()
      {
        ObjectType = (byte)OtpType.Email,
        ObjectId = dto.Email
      };

      if (await _otpService.CheckObjectExistAsync(newObjectOtp))
        throw new BadRequestException(ErrorMessages.User_OtpStillValid);
      else
        _otpService.Create(newObjectOtp);
    }

    public UserProfileResponseDto GetUserProfile(Guid userId)
    {
      try
      {
        var currentUser = _replicaDbContext.Users.Where(x => x.UserId == userId).FirstOrDefault();
        if (currentUser == null)
        {
          throw new BadRequestException(ErrorMessages.User_NotFound);
        }

        var currentStatistic = _replicaDbContext.UserReferralStatistics.FirstOrDefault(x => x.UserId == userId);
        if (currentStatistic == null)
        {
          _userReferralStatisticService.Refresh(new UserReferralStatisticRefreshModel()
          {
            CreatedBy = userId,
            InviterReferralCode = currentUser.ReferralCode,
            InviterId = userId
          });

          currentStatistic = _replicaDbContext.UserReferralStatistics.FirstOrDefault(x => x.UserId == userId);
        }

        var result = new UserProfileResponseDto()
        {
          Id = currentUser.UserId,
          Email = currentUser.Email,
          InviteFriendInfo = new InviteFriend()
          {
            InviteLeft = currentUser.UserType == (byte)UserType.Ambassador
                                  ? "Unlimited"
                                  : (_userConfig.MaxInviteFriend - currentStatistic.NumberFriendsInvited).ToString(),
            SuccessfulSignup = currentStatistic.NumberFriendsInvited,
            WroteFirstReview = currentStatistic.NumberFirstReviewsWrote
          },
          NickName = currentUser.NickName,
          LastChangeNickname = currentUser.LastChangeNickname,
          UserName = currentUser.UserName,
          ReferralCode = currentUser.ReferralCode,
          UserType = EnumExtensions.GetEnumDescription((UserType)currentUser.UserType),
          ClassYear = new OthersInfo()
          {
            Guid = Guid.NewGuid(),
            Status = (currentUser.IsHiddenClassYear.HasValue && currentUser.IsHiddenClassYear.Value == false) ? HiddenStatus.Public : HiddenStatus.Hidden,
            Name = currentUser.ClassYear?.ToString(),
            LastChangeDateTime = currentUser.LastChangeClassYear
          }
        };

        result.SchoolName = _replicaDbContext.Schools.Where(x => x.Guid == currentUser.SchoolId).Select(x => x.Name).FirstOrDefault();
        result.Kemp = _replicaDbContext.KempWallets.Where(x => x.UserId == currentUser.UserId).Select(x => x.Amount).FirstOrDefault();
        result.DegreeInfo = _replicaDbContext.Degrees.Where(x => x.Guid == currentUser.DegreeId)
          .Select(x => new OthersInfo()
          {
            Guid = x.Guid,
            Status = currentUser.IsHiddenDegree ? HiddenStatus.Hidden : HiddenStatus.Public,
            Name = x.Name,
            LastChangeDateTime = currentUser.LastChangedDegree
          })
          .FirstOrDefault() ?? new OthersInfo();
        result.MajorInfo = _replicaDbContext.Majors.Where(x => x.Guid == currentUser.MajorId)
          .Select(x => new OthersInfo()
          {
            Guid = x.Guid,
            Status = currentUser.IsHiddenMajor ? HiddenStatus.Hidden : HiddenStatus.Public,
            Name = x.Name,
            LastChangeDateTime = currentUser.LastChangedMajor
          })
          .FirstOrDefault() ?? new OthersInfo();


        return result;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message);
        throw new BadRequestException(ex.Message);
      }
    }

    public UserReferralDetailResponseDto GetUserReferralDetail(Guid userId)
    {
      var currentUser = _replicaDbContext.Users.Where(x => x.UserId == userId).FirstOrDefault();
      if (currentUser == null)
      {
        throw new BadRequestException(ErrorMessages.User_NotFound);
      }

      UserReferralDetailResponseDto result = _replicaDbContext.UserReferralStatistics.Where(x => x.UserId == userId)
        .Select(x => new UserReferralDetailResponseDto()
        {
          InviteLeft = currentUser.UserType == (byte)UserType.Ambassador
                                ? "Unlimited"
                                : (_userConfig.MaxInviteFriend - x.NumberFriendsInvited).ToString(),
          SuccessfulSignup = x.NumberFriendsInvited,
          WroteFirstReview = x.NumberFirstReviewsWrote,
          UserType = EnumExtensions.GetEnumDescription((UserType)currentUser.UserType)
        })
        .FirstOrDefault();

      return result;
    }

    public async Task<ApplicationUser> GetUserInfoByReferralCodeAsync(string referralCode)
    {
      return await _replicaDbContext.Users.FirstOrDefaultAsync(x => x.ReferralCode == referralCode);
    }

    public async Task<string> UpdateNicknameAsync(Guid userId)
    {
      using (var transaction = await _masterDbContext.Database.BeginTransactionAsync())
      {
        try
        {
          var newNickname = await CreateNickNameAsync();

          var currentUser = await GetUserByIdAsync(userId);
          if (currentUser == null)
          {
            throw new BadRequestException(ErrorMessages.User_NotFound);
          }

          var lastDateChangedNickname = currentUser.LastChangeNickname;
          if (lastDateChangedNickname.HasValue && lastDateChangedNickname.Value.AddMinutes(_userConfig.NickNameLockdownMinutes) > DateTime.UtcNow)
          {
            var countdownTimeSpan = (lastDateChangedNickname.Value.AddMinutes(_userConfig.NickNameLockdownMinutes) - DateTime.UtcNow);
            var countdownDays = countdownTimeSpan.TotalDays;
            if (countdownDays < 1)
            {
              var countdownHours = countdownTimeSpan.TotalHours;
              throw new BadRequestException(string.Format(ErrorMessages.User_NickNameIsLockedForHours, Convert.ToInt32(countdownHours)));
            }
            else
            {
              throw new BadRequestException(string.Format(ErrorMessages.User_NickNameIsLockedForDays, Convert.ToInt32(countdownDays)));
            }
          }

          currentUser.NickName = newNickname;
          currentUser.LastChangeNickname = DateTime.UtcNow;

          var result = await _userManager.UpdateAsync(currentUser);
          if (!result.Succeeded)
          {
            throw new BadRequestException(string.Join(", ", result.Errors.Select(e => e.Description)));
          }

          await transaction.CommitAsync();
          return newNickname;
        }
        catch (Exception ex)
        {
          transaction.Rollback();
          _logger.LogError(ex.Message);
          throw new BadRequestException(ex.Message);
        }
      }
    }

    public async Task<ApplicationUser> GetUserByIdAsync(Guid userId)
    {
      try
      {
        return await _replicaDbContext.Users.Where(x => x.UserId == userId).FirstOrDefaultAsync();
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message);
        throw new BadRequestException(ex.Message);
      }
    }
    #endregion

    #region Admin
    public async Task<Guid> CreateAdminAsync(AdminInputModel input)
    {
      //TODO Check permission of Creator

      // Validate input
      if (await CheckEmailExistAsync(input.Email))
      {
        throw new BadRequestException(ErrorMessages.User_EmailHasTaken);
      }

      var roleEntity = _replicaDbContext.Role.Where(x => x.Guid == input.RoleId).FirstOrDefault();
      if (roleEntity == null)
      {
        throw new BadRequestException(ErrorMessages.Role_Notfound);
      }

      var newAdmin = new ApplicationUser()
      {
        UserId = Guid.NewGuid(),
        UserName = input.Email,
        FirstName = input.Name,
        Email = input.Email,
        Status = input.Status,
        CreatedBy = input.CreatedBy,
        CreatedDate = DateTime.UtcNow,
        UserType = (byte)UserType.Admin,
      };

      using var transaction = _masterDbContext.Database.BeginTransaction();
      try
      {
        var result = await _userManager.CreateAsync(newAdmin);
        if (!result.Succeeded)
        {
          throw new BadRequestException(string.Join(", ", result.Errors.Select(e => e.Description)));
        }

        var newUserRole = new UserRoleEntity()
        {
          RoleId = roleEntity.Id,
          UserId = newAdmin.UserId
        };

        _masterDbContext.UserRole.Add(newUserRole);
        _masterDbContext.SaveChanges();

        transaction.Commit();
      }
      catch (Exception)
      {
        transaction.Rollback();
        throw;
      }

      //Send mail create password
      await SendMailResetPassword(newAdmin.Email);

      return newAdmin.UserId;
    }

    public PagingResult<AdminListViewModel> SearchAdmin(AdminFilterModel filter, Pageable pageable, bool useMasterDb)
    {
      var query = BuildAdminQueryable(filter, useMasterDb);
      var totalCount = query.LongCount();
      if (totalCount < 1)
      {
        return new PagingResult<AdminListViewModel>(0, pageable.PageIndex, pageable.PageSize);
      }

      var orderBy = QueryableUtils.GetOrderByFunction<ApplicationUser>(pageable.GetSortables(nameof(ApplicationUser.CreatedDate)));
      var data = orderBy(query).Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new AdminListViewModel(x)).ToList();
      BuildAdminResultData(data, useMasterDb);
      return new PagingResult<AdminListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    public AdminDetailModel GetAdminDetail(Guid id)
    {
      var query = BuildAdminQueryable(new AdminFilterModel() { Id = id }, false);
      var totalCount = query.LongCount();
      if (totalCount < 1)
      {
        throw new BadRequestException(ErrorMessages.Admin_NotFound);
      }

      var data = query.Select(x => new AdminDetailModel(x)).FirstOrDefault();

      BuildAdminDetailResultData(data, false);

      return data;
    }

    public async Task<bool> UpdateAdminAsync(AdminInputModel input)
    {
      var currentUser = await GetUserByIdAsync(input.Id);
      if (currentUser == null)
      {
        throw new BadRequestException(ErrorMessages.Admin_NotFound);
      }

      // Validate input
      if (await _replicaDbContext.Users.AnyAsync(x => x.Email == input.Email && x.UserId != input.Id))
      {
        throw new BadRequestException(ErrorMessages.User_EmailHasTaken);
      }

      var roleEntity = _replicaDbContext.Role.Where(x => x.Guid == input.RoleId).FirstOrDefault();
      if (roleEntity == null)
      {
        throw new BadRequestException(ErrorMessages.Role_Notfound);
      }

      currentUser.FirstName = input.Name;
      currentUser.Email = input.Email;
      currentUser.UpdatedBy = input.UpdatedBy;
      currentUser.UpdatedDate = DateTime.UtcNow;
      currentUser.Status = input.Status;

      using var transaction = _masterDbContext.Database.BeginTransaction();
      try
      {
        var result = await _userManager.UpdateAsync(currentUser);
        if (!result.Succeeded)
        {
          throw new BadRequestException(string.Join(", ", result.Errors.Select(e => e.Description)));
        }

        // Delete old UserRole
        var currentUserRole = _replicaDbContext.UserRole.Where(x => x.UserId == currentUser.UserId).FirstOrDefault();
        if (currentUserRole != null)
        {
          _masterDbContext.UserRole.Remove(currentUserRole);
        }

        // Create new UserRole
        var newUserRole = new UserRoleEntity()
        {
          RoleId = roleEntity.Id,
          UserId = currentUser.UserId
        };

        _masterDbContext.UserRole.Add(newUserRole);
        _masterDbContext.SaveChanges();

        transaction.Commit();
        return result.Succeeded;
      }
      catch (Exception)
      {
        transaction.Rollback();
        throw;
      }
    }

    public PagingResult<UserListViewModel> SearchUser(UserFilterModel filter, Pageable pageable, bool useMasterDb)
    {
      var query = BuildUserQueryable(filter, useMasterDb);
      var totalCount = query.LongCount();
      if (totalCount < 1)
      {
        return new PagingResult<UserListViewModel>(0, pageable.PageIndex, pageable.PageSize);
      }

      var orderBy = QueryableUtils.GetOrderByFunction<ApplicationUser>(pageable.GetSortables(nameof(ApplicationUser.CreatedDate)));
      var data = orderBy(query).Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new UserListViewModel(x)).ToList();
      BuildUserResultData(data, useMasterDb);
      return new PagingResult<UserListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    public UserDetailModel GetUserDetail(Guid id)
    {
      var query = BuildUserQueryable(new UserFilterModel() { Id = id }, false);
      var totalCount = query.LongCount();
      if (totalCount < 1)
      {
        throw new BadRequestException(ErrorMessages.User_NotFound);
      }

      var data = query.Select(x => new UserDetailModel(x)).FirstOrDefault();
      BuildUserDetailResultData(data, false);

      return data;
    }

    public Guid UpdateUser(CmsUserInputModel input)
    {
      var currentUser = GetUserByIdAsync(input.Id.GetValueOrDefault()).Result;
      if (currentUser == null)
      {
        throw new BadRequestException(ErrorMessages.User_NotFound);
      }

      if (input.UserType == (byte)UserType.Admin)
      {
        throw new BadRequestException(ErrorMessages.User_PermissionDenied);
      }

      using var transaction = _masterDbContext.Database.BeginTransaction();
      try
      {
        currentUser.AdminMemory = input.Memory;
        if (input.Status.HasValue)
        {
          currentUser.Status = input.Status.Value;
        }
        if (input.UserType.HasValue)
        {
          currentUser.UserType = input.UserType.Value;
        }

        currentUser.UpdatedBy = input.UpdatedBy.GetValueOrDefault();
        currentUser.UpdatedDate = DateTime.UtcNow;

        _masterDbContext.Users.Update(currentUser);
        _masterDbContext.SaveChanges();

        transaction.Commit();
        return currentUser.UserId;
      }
      catch (Exception)
      {
        transaction.Rollback();
        throw;
      }
    }

    public Guid CreateUser(CmsUserInputModel input)
    {
      if (CheckUsernameExistAsync(input.Username).Result)
      {
        throw new BadRequestException(ErrorMessages.User_UsernameHasTaken);
      }

      if (CheckEmailExistAsync(input.Email).Result)
      {
        throw new BadRequestException(ErrorMessages.User_EmailHasTaken);
      }

      if (input.UserType == (byte)UserType.Admin)
      {
        throw new BadRequestException(ErrorMessages.User_PermissionDenied);
      }

      var referralCode = StringHelper.GenerateReferralCodeFromEmail(input.Email);
      var currentDate = DateTime.UtcNow;
      ApplicationUser newUser = new ApplicationUser()
      {
        UserId = Guid.NewGuid(),
        NickName = CreateNickNameAsync().Result,
        LastChangeNickname = currentDate,
        UserName = input.Username,
        Email = input.Email,
        EmailConfirmed = true,
        SchoolId = input.SchoolId.GetValueOrDefault(),
        ClassYear = input.ClassYear,
        LastChangeClassYear = currentDate,
        DegreeId = input.DegreeId,
        LastChangedDegree = currentDate,
        MajorId = input.MajorId,
        LastChangedMajor = currentDate,
        UserType = input.UserType.GetValueOrDefault(),
        Status = input.Status.GetValueOrDefault(),
        ReferralCode = referralCode,
        CreatedBy = input.CreatedBy,
        CreatedDate = currentDate
      };

      IdentityResult result;
      using var transaction = _masterDbContext.Database.BeginTransaction();
      try
      {
        result = _userManager.CreateAsync(newUser, input.Password).Result;
        if (!result.Succeeded)
        {
          throw new BadRequestException(string.Join(", ", result.Errors.Select(e => e.Description)));
        }

        transaction.Commit();
      }
      catch (Exception)
      {
        transaction.Rollback();
        throw;
      }

      if (result.Succeeded)
      {
        try
        {
          // Delete email in waitlist if Exist
          _waitListService.Delete(input.Email);

          // Update Number Of Use in school
          _schoolLifeTimeAnalyticService.RefreshNumber(input.SchoolId.GetValueOrDefault());

          // Create new Wallet
          _kempTransactionService.CreateWallet(newUser.UserId, newUser.UserId);

          // Create SignupSuccessfully transaction. Add 3 Kemp
          _kempTransactionService.CreateTransaction(new Models.Common.KempTransactionInputModel()
          {
            UserIds = new List<Guid>() { newUser.UserId },
            TransactionType = (byte)KempTransactionType.SignupSuccessfully,
            ReferenceGuid = newUser.UserId,
            Amount = KempTransactionValue.SignupSuccessfully,
            Action = (byte)KempTransactionAction.Add,
            CreatorId = newUser.UserId,
            SchoolId = newUser.SchoolId
          });
        }
        catch (Exception ex)
        {
          throw new BadRequestException(ex.InnerException?.Message);
        }
      }

      return newUser.UserId;
    }
    public async Task ResetPasswordAsync(Guid userId)
    {
      var currentUser = _replicaDbContext.Users.Where(x => x.UserId == userId).FirstOrDefault();
      if (currentUser != null)
      {
        await SendMailResetPassword(currentUser.Email);
      }
    }

    public async Task ResetSitePasswordAsync(Guid userId)
    {
      var currentUser = _replicaDbContext.Users.Where(x => x.UserId == userId).FirstOrDefault();
      if (currentUser != null)
      {
        await SendSiteMailResetPasswordAsync(currentUser.Email);
      }
    }

    #endregion

    #region Common
    public async Task<bool> CheckEmailExistAsync(string email)
    {
      return (await _replicaDbContext.Users.FirstOrDefaultAsync(x => x.Email == email)) != null;
    }

    public async Task<bool> CheckUsernameExistAsync(string userName)
    {
      return (await _userManager.FindByNameAsync(userName)) != null;
    }

    public async Task<bool> CheckReferralCodeExistAsync(string referralCode)
    {
      return await _replicaDbContext.Users.AnyAsync(x => x.ReferralCode == referralCode);
    }

    public async Task<bool> CheckNickNameExistAsync(string nickName)
    {
      return await _replicaDbContext.Users.AnyAsync(x => !string.IsNullOrEmpty(x.NickName) && x.NickName.Contains(nickName));
    }
    #endregion

    #region Private Methods
    private void BuildAdminResultData(List<AdminListViewModel> data, bool useMasterDb)
    {
      SetRole(data, useMasterDb);
    }

    private void BuildAdminDetailResultData(AdminDetailModel data, bool useMasterDb)
    {
      SetRole(data, useMasterDb);
      SetPermissions(data, useMasterDb);
    }

    private void BuildUserDetailResultData(UserDetailModel data, bool useMasterDb)
    {
      SetUserDetail(data, useMasterDb);
      SetUserActivity(data, useMasterDb);
    }

    private void SetRole(List<AdminListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var userIds = data.Select(x => x.Guid).ToList();
      var mapData = dbContext.Users.Where(x => userIds.Contains(x.UserId))
        .Join(dbContext.UserRole,
          user => user.UserId,
          userRole => userRole.UserId,
          (user, userRole) => userRole
          )
        .Join(dbContext.Role,
          userUserRole => userUserRole.RoleId,
          role => role.Id,
          (userUserRole, role) => new { UserId = userUserRole.UserId, Role = role.Name }
        )
        .ToDictionary(x => x.UserId, x => x.Role);

      data.ForEach(x =>
      {
        var item = mapData.GetValueOrDefault(x.Guid);
        if (string.IsNullOrWhiteSpace(item)) return;

        x.Role = item;
      });
    }

    private void SetUserDetail(UserDetailModel data, bool useMasterDb)
    {
      if (data != null)
      {
        data.UserDetails.School = _replicaDbContext.Schools.Where(x => x.Guid == data.UserDetails.SchoolId).Select(x => x.Name).FirstOrDefault();
        data.UserDetails.Degree = _replicaDbContext.Degrees.Where(x => x.Guid == data.UserDetails.DegreeId).Select(x => x.Name).FirstOrDefault();
        data.UserDetails.Major = _replicaDbContext.Majors.Where(x => x.Guid == data.UserDetails.MajorId).Select(x => x.Name).FirstOrDefault();
      }
    }

    private void SetUserActivity(UserDetailModel data, bool useMasterDb)
    {
      // TODO 1.0.1 Update User Statistic
      if (data != null)
      {
        data.Activities.InvitesLeft = data.UserDetails.Group == (byte)UserType.Ambassador
                                  ? "Unlimited"
                                  : (_userConfig.MaxInviteFriend - data.Activities.SuccessfulReferral).ToString();

      }
    }

    private void SetRole(AdminDetailModel data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var mapData = dbContext.Users.Where(x => x.UserId == data.Id)
        .Join(dbContext.UserRole,
          user => user.UserId,
          userRole => userRole.UserId,
          (user, userRole) => userRole
          )
        .Join(dbContext.Role,
          userUserRole => userUserRole.RoleId,
          role => role.Id,
          (userUserRole, role) => new { userUserRole, role }
        )
        .Select(x => x.role).FirstOrDefault();

      if (mapData != null)
      {
        data.RoleId = mapData.Guid;
        data.RoleName = mapData.Name;
      }
    }

    private void SetPermissions(AdminDetailModel data, bool useMasterDb)
    {
      if (data.RoleId == null) return;
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var permissions = dbContext.RolePermission
        .Join(dbContext.Role,
          rolePermission => rolePermission.RoleId,
          role => role.Id,
          (rolePermission, role) => new { rolePermission, role })
        .Where(x => x.role.Guid == data.RoleId)
        .Select(x => x.rolePermission.PermissionId)
        .ToList();
      data.Permissions = permissions;
    }

    private IQueryable<ApplicationUser> BuildAdminQueryable(AdminFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.Users.AsNoTracking();
      query = AdminQueryBuilder<ApplicationUser>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    private void ValidateInviter(string referralCode, string host, bool isSchoolRequiredReferralCode)
    {
      if (isSchoolRequiredReferralCode)
      {
        if (string.IsNullOrEmpty(referralCode))
        {
          throw new BadRequestException(ErrorMessages.User_ReferralCodeRequried);
        }
      }

      // Validate referral code when user enters regardless of school request or not
      if (!string.IsNullOrEmpty(referralCode))
      {
        long numberFriendsInvited = 0;
        var inviter = GetUserInfoByReferralCodeAsync(referralCode).Result;
        if (inviter == null)
        {
          var inviterInWaitlist = _waitListService.GetWaitListInfoByEmail(StringHelper.GenerateEmailFromReferralCode(referralCode));
          if (inviterInWaitlist == null)
            throw new BadRequestException(ErrorMessages.User_ReferralCodeInvalid);

          numberFriendsInvited = _replicaDbContext.UserReferralStatistics.Where(x => x.UserId == inviterInWaitlist.Guid).Select(x => x.NumberFriendsInvited).FirstOrDefault();
          if (numberFriendsInvited >= _userConfig.MaxInviteFriend)
          {
            throw new BadRequestException(ErrorMessages.User_ReferralCodeExceedNumberOfInvite);
          }
        }
        else
        {
          numberFriendsInvited = _replicaDbContext.UserReferralStatistics.Where(x => x.UserId == inviter.UserId).Select(x => x.NumberFriendsInvited).FirstOrDefault();
          if (inviter.UserType == (byte)UserType.User
            && numberFriendsInvited >= _userConfig.MaxInviteFriend)
          {
            throw new BadRequestException(ErrorMessages.User_ReferralCodeExceedNumberOfInvite);
          }

          // Check domain school of inviter
          MailAddress inviterEmail = new MailAddress(inviter.Email);
          string inviterDomain = inviterEmail.Host;
          if (inviterDomain != host)
          {
            throw new BadRequestException(ErrorMessages.User_SchoolDomainNotMatchWithRefferer);
          }
        }
      }
    }

    private IQueryable<ApplicationUser> BuildUserQueryable(UserFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.Users.AsNoTracking();
      query = UserQueryBuidler<ApplicationUser>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    private void BuildUserResultData(List<UserListViewModel> data, bool useMasterDb)
    {
      SetKemp(data, useMasterDb);
      SetViolation(data, useMasterDb);
    }

    private void SetKemp(List<UserListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var userIds = data.Select(x => x.Guid).ToList();
      var mapData = dbContext.KempWallets
        .Where(x => userIds.Contains(x.UserId))
        .ToDictionary(x => x.UserId, x => x.Amount);
      data.ForEach(x =>
      {
        var item = mapData.GetValueOrDefault(x.Guid);
        if (item == null) return;
        x.Kemp = item;
      });
    }

    // TODO
    private void SetViolation(List<UserListViewModel> data, bool useMasterDb)
    {
      //var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      //var userIds = data.Select(x => x.Guid).ToList();
    }

    private async Task SendMailResetPassword(string email)
    {
      var newObjectOtp = new OtpRequestDto()
      {
        ObjectType = (byte)OtpType.Email,
        ObjectId = email,
        IsSendNotification = false,
      };
      var otp = _otpService.Create(newObjectOtp);
      if (!string.IsNullOrEmpty(otp.OtpCode))
      {
        var linkResetToken = $"{_domainConfig.CMS}/{_domainConfig.Pages.ForgotPassword}?email={email}";
        await _emailNotificationService.SendCmsResetPasswordAsync(otp.OtpCode, linkResetToken, email);
      }
    }

    private async Task SendSiteMailResetPasswordAsync(string email)
    {
      var newObjectOtp = new OtpRequestDto()
      {
        ObjectType = (byte)OtpType.Email,
        ObjectId = email,
        IsSendNotification = false,
      };
      var otp = _otpService.Create(newObjectOtp);
      if (!string.IsNullOrEmpty(otp.OtpCode))
      {
        var linkResetToken = $"{_domainConfig.Site}/{_domainConfig.Pages.ForgotPassword}?email={email}";
        await _emailNotificationService.SendSiteResetPasswordAsync(otp.OtpCode, linkResetToken, email);
      }
    }

    #endregion
  }
}
﻿using Kempus.Core.Models;
using Kempus.Models.Admin.Kemp;
using Kempus.Models.Public.KempTransaction;

namespace Kempus.Services.Public.KempTransaction
{
  public interface IKempTransactionService
  {
    PagingResult<KemTransactionListViewModel> Search(KempFilterModel filter, Pageable pageable, bool useMasterDb);
  }
}

﻿using Kempus.Core.Extensions;
using Kempus.Core.Models;
using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Admin.Kemp;
using Kempus.Models.Public.KempTransaction;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.Public.KempTransaction
{
  public class KempTransactionService : IKempTransactionService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;

    public KempTransactionService(
      MasterDbContext masterDbContext,
      ReplicaDbContext replicaDbContext
      )
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
    }

    public PagingResult<KemTransactionListViewModel> Search(KempFilterModel filter, Pageable pageable, bool useMasterDb)
    {
      var query = BuildQueryable(filter, useMasterDb);
      var totalCount = query.LongCount();
      if (totalCount < 1)
      {
        return new PagingResult<KemTransactionListViewModel>(0, pageable.PageIndex, pageable.PageSize);
      }
      var orderBy =
        QueryableUtils.GetOrderByFunction<KempTransactionEntity>(pageable.GetSortables(nameof(KempTransactionEntity.CreatedDate)));

      var data = orderBy(query).Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new KemTransactionListViewModel(x)).ToList();

      BuildResultData(data, useMasterDb);

      return new PagingResult<KemTransactionListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    #region Private methods

    private IQueryable<KempTransactionEntity> BuildQueryable(KempFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.KempTransactions.AsNoTracking();
      query = KempTransactionQueryBuilder<KempTransactionEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    private void BuildResultData(List<KemTransactionListViewModel> data, bool useMasterDb)
    {
      SetGroupTitleDescription(data);
    }

    private void SetGroupTitleDescription(List<KemTransactionListViewModel> data)
    {
      foreach (var item in data)
      {
        item.Title = !item.IsAdminCreated
          ? GetTransactionTitle(item.TransactionType)
          : KempTransactionTitle.Admin.GetEnumDescription();
        item.Description = GetTransactionDescription(item);
      }
    }

    private string GetTransactionDescription(KemTransactionListViewModel item)
    {
      return item.IsAdminCreated && item.TransactionType != KempTransactionType.SignupSuccessfully
        ? item.Amount < 0
          ? KempTransactionDescription.PenaltyFromKempus.GetEnumDescription()
          : KempTransactionDescription.BonusFromKempus.GetEnumDescription()
        : EnumExtensions.GetEnumDescription((KempTransactionDescription)(byte)item.TransactionType);
    }

    private string GetTransactionTitle(KempTransactionType transactionType)
    {
      string title = string.Empty;
      switch (transactionType)
      {
        case KempTransactionType.WriteCourseReview:
        case KempTransactionType.UnlockCourseReview:
          {
            title = KempTransactionTitle.CourseReview.GetEnumDescription();
            break;
          }
        case KempTransactionType.ConsecutiveLoginFor3Days:
          {
            title = KempTransactionTitle.Activity.GetEnumDescription();
            break;
          }
        case KempTransactionType.SignupSuccessfully:
          {
            title = KempTransactionTitle.Bonus.GetEnumDescription();
            break;
          }
        case KempTransactionType.NewUserReferredSignedUp:
          {
            title = KempTransactionTitle.Reward.GetEnumDescription();
            break;
          }
        case KempTransactionType.SuccessfullyFlagReport:
        case KempTransactionType.FlagReportUser:
        case KempTransactionType.FlagReportReview:
        case KempTransactionType.FlagReportPost:
        case KempTransactionType.FlagReportPoll:
        case KempTransactionType.FlagReportComment:
        case KempTransactionType.FlaggedByOtherUser:
          {
            title = KempTransactionTitle.Flag.GetEnumDescription();
            break;
          }
        default:
          throw new ArgumentOutOfRangeException();
      }

      return title;
    }

    #endregion
  }
}

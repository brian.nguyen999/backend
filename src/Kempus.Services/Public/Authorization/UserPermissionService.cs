﻿using Kempus.Models.Authorization.Request;
using Kempus.Models.Authorization.Response;
using Kempus.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Public.Authorization
{
  public class UserPermissionService : IUserPermissionService
  {
    private readonly MasterDbContext _masterContext;
    private readonly ReplicaDbContext _replicaContext;

    public UserPermissionService(MasterDbContext masterContext, ReplicaDbContext replicaContext)
    {
      _masterContext = masterContext;
      _replicaContext = replicaContext;
    }

    private List<UserPermissionModuleModel> GetUserPermissionModuleModels(List<Guid> userIds, bool useMasterDb)
    {
      var result = UserPermissionModels(userIds, useMasterDb);

      var permissionIds = new HashSet<int>(result.Select(x => x.PermissionId)).ToList();
      var permissionModuleMap = _replicaContext.Permission.Where(x => permissionIds.Contains(x.Id))
        .Select(x => new { permissionId = x.Id, x.ModuleApplicationId }).ToList()
        .ToDictionary(x => x.permissionId, x => x.ModuleApplicationId);
      foreach (var item in result)
      {
        item.ModuleApplicationId = permissionModuleMap.GetValueOrDefault(item.PermissionId);
      }

      return result;
    }

    public async Task<ObjectHasPermissionListModel> GetObjectHasPermissionListModel(Guid userId)
    {
      var model = new ObjectHasPermissionListModel { UserId = userId };
      SetPermissionIds(new List<IHasPermissionList> { model }, true);
      return model;
    }

    private List<UserPermissionModuleModel> UserPermissionModels(List<Guid> userIds, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterContext : _replicaContext;
      var result = dbContext.UserRole.Where(x => userIds.Contains(x.UserId))
        .Join(dbContext.Role.Where(x => x.IsActive), roleUser => roleUser.RoleId, role => role.Id,
          (userRole, role) => new { UserId = userRole.UserId, roleId = role.Id })
        .Join(dbContext.RolePermission, userRole => userRole.roleId, rolePermission => rolePermission.RoleId,
          (userRole, rolePermission) => new UserPermissionModuleModel
          {
            UserId = userRole.UserId,
            PermissionId = rolePermission.PermissionId
          })
        .Distinct()
        .ToList();
      return result;
    }

    public void SetPermissionIds(List<IHasPermissionList> list, bool useMasterDb)
    {
      var userIds = list.Select(x => x.UserId).ToList();
      var models = GetUserPermissionModuleModels(userIds, useMasterDb);
      var userPermissionLookup = models.ToLookup(x => x.UserId, x => x.PermissionId);
      var userModuleLookup = models.ToLookup(x => x.UserId, x => x.ModuleApplicationId);
      foreach (var item in list)
      {
        item.PermissionIds = userPermissionLookup.Where(x => x.Key == item.UserId).SelectMany(g => g).ToList();
        item.ModuleApplicationIds =
          new HashSet<int>(userModuleLookup.Where(x => x.Key == item.UserId).SelectMany(g => g)).ToList();
      }
    }
  }
}
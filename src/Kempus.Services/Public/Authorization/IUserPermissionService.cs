﻿using Kempus.Models.Authorization.Request;
using Kempus.Models.Authorization.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Public.Authorization
{
  public interface IUserPermissionService
  {
    void SetPermissionIds(List<IHasPermissionList> list, bool useMasterDb);
    Task<ObjectHasPermissionListModel> GetObjectHasPermissionListModel(Guid userId);
  }
}
﻿namespace Kempus.Services.Public.Statistics
{
  public interface IStatisticsService
  {
    void AddVote(Guid userId, Guid pollId);
  }
}

﻿using Kempus.Entities;
using Kempus.EntityFramework;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.Public.Statistics
{
  public class StatisticsService : IStatisticsService
  {
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly MasterDbContext _masterDbContext;
    public StatisticsService(
      ReplicaDbContext replicaDbContext,
      MasterDbContext masterDbContext)
    {
      _replicaDbContext = replicaDbContext;
      _masterDbContext = masterDbContext;
    }

    public void AddVote(Guid userId, Guid pollId)
    {
      var statistics = _replicaDbContext.Statistics.FirstOrDefault(x => x.ObjectId == pollId);
      if (statistics != null)
      {
        statistics.NumberOfVote++;
        _masterDbContext.Statistics.Update(statistics);
      }
      else
      {
        statistics = new StatisticsEntity
        {
          NumberOfVote = 1,
          ObjectId = pollId,
          ObjectType = (byte)StatisticsObjectType.Poll,
          CreatedUser = userId
        };
        _masterDbContext.Statistics.Add(statistics);
      }
      _masterDbContext.SaveChanges();
    }
  }
}

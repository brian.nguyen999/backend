﻿using Kempus.Core.Models;
using Kempus.Models.Admin.DepartmentProgram;
using Kempus.Models.Public.DepartmentProgram;

namespace Kempus.Services.Public.DepartmentProgram
{
  public interface IDepartmentProgramService
  {
    PagingResult<DepartmentProgramListViewModel> CmsSearch(DepartmentProgramFilterModel filter, Pageable pageable, bool useMasterDb);
    DepartmentProgramDetailCmsModel GetDetailCms(Guid id);
    void SetDepartments(List<IHasDepartment> data, bool useMasterDb);
    void SetDepartment(IHasDepartment data, bool useMasterDb);
  }
}

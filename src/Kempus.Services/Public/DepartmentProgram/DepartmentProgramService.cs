﻿using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Admin.DepartmentProgram;
using Kempus.Models.Public.Course;
using Kempus.Models.Public.DepartmentProgram;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Public.DepartmentProgram
{
  public class DepartmentProgramService : IDepartmentProgramService
  {
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly MasterDbContext _masterDbContext;

    public DepartmentProgramService(
      ReplicaDbContext replicaDbContext,
      MasterDbContext masterDbContext)
    {
      _replicaDbContext = replicaDbContext;
      _masterDbContext = masterDbContext;
    }

    #region CMS
    public PagingResult<DepartmentProgramListViewModel> CmsSearch(DepartmentProgramFilterModel filter, Pageable pageable, bool useMasterDb)
    {
      if (!filter.SchoolId.HasValue)
        return new PagingResult<DepartmentProgramListViewModel>(0, pageable.PageIndex, pageable.PageSize);

      var query = BuildQueryable(filter, useMasterDb);

      var totalCount = query.LongCount();
      if (totalCount < 1)
        return new PagingResult<DepartmentProgramListViewModel>(0, pageable.PageIndex, pageable.PageSize);

      var data = query.Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new DepartmentProgramListViewModel(x)).ToList();

      return new PagingResult<DepartmentProgramListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    public DepartmentProgramDetailCmsModel GetDetailCms(Guid id)
    {
      var entity = _replicaDbContext.DepartmentPrograms.FirstOrDefault(x => x.Guid == id);
      if (entity == null)
        throw new BadRequestException(ErrorMessages.DepartmentProgram_NotFound);

      return new DepartmentProgramDetailCmsModel(entity);
    }

    public void SetDepartments(List<IHasDepartment> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var departmentIds = data.Where(x => x.DepartmentId != null).Select(x => x.DepartmentId.Value).ToList();
      var mapData = dbContext.DepartmentPrograms
        .Where(x => departmentIds.Contains(x.Guid))
        .ToDictionary(x => x.Guid, x => x.Name);

      data.ForEach(x =>
      {
        if (x.DepartmentId != null)
        {
          var item = mapData.GetValueOrDefault(x.DepartmentId.Value);
          x.DepartmentName = item;
        }
      });
    }

    public void SetDepartment(IHasDepartment data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var departmentId = data.DepartmentId;
      var mapData = dbContext.DepartmentPrograms
        .FirstOrDefault(x => x.Guid == departmentId);
      data.DepartmentName = mapData?.Name;
    }

    #endregion

    #region Private Methods
    private IQueryable<DepartmentProgramEntity> BuildQueryable(DepartmentProgramFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.DepartmentPrograms.AsNoTracking();
      query = DepartmentProgramQueryBuilder<DepartmentProgramEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }
    #endregion
  }
}

﻿using Kempus.Core.Enums;
using Kempus.Core.Errors;
using Kempus.EntityFramework;
using Kempus.Models.Public.CourseReviewActivity;
using Kempus.Models.Public.CourseReviewLike;
using Kempus.Services.Public.CourseReviewActivity;

namespace Kempus.Services.Public.CourseReviewLike
{
  public class CourseReviewLikeService : ICourseReviewLikeService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly ICourseReviewActivityService _courseReviewActivityService;

    public CourseReviewLikeService(
      MasterDbContext masterDbContext,
      ReplicaDbContext replicaDbContext,
      ICourseReviewActivityService courseReviewActivityService)
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
      _courseReviewActivityService = courseReviewActivityService;
    }

    public Guid CreateIfNotExits(CourseReviewLikeInputModel model)
    {
      var courseReview = _replicaDbContext.CourseReviews
        .FirstOrDefault(x => x.Guid == model.CourseReviewId);
      if (courseReview == null)
        throw new NotFoundException(ErrorMessages.CourseReview_NotFound);

      var courseReviewEntity = _replicaDbContext.CourseReviewLikes
        .FirstOrDefault(x => x.CourseReviewId == model.CourseReviewId &&
                             x.UserId == model.CreatorId &&
                             x.SchoolId == model.SchoolId);
      if (courseReviewEntity == null)
      {
        using var transaction = _masterDbContext.Database.BeginTransaction();
        try
        {
          courseReviewEntity = new Entities.CourseReviewLikeEntity()
          {
            CourseReviewId = model.CourseReviewId,
            UserId = model.CreatorId,
            SchoolId = model.SchoolId,
            CreatedDate = DateTime.UtcNow
          };

          _masterDbContext.CourseReviewLikes.Add(courseReviewEntity);
          _masterDbContext.SaveChanges();
          _courseReviewActivityService.Create(new CourseReviewActivityInputModel
          {
            CourseReviewId = courseReviewEntity.CourseReviewId,
            UserId = model.CreatorId,
            ActionId = CoreEnums.CourseReviewActivity.Like,
            SchoolId = model.SchoolId
          });

          transaction.Commit();
        }
        catch (Exception)
        {
          transaction.Rollback();
          throw;
        }
      }

      return courseReviewEntity.CourseReviewId;
    }

    public void Delete(CourseReviewLikeInputModel model)
    {
      var courseReview = _replicaDbContext.CourseReviews
        .FirstOrDefault(x => x.Guid == model.CourseReviewId);
      if (courseReview == null)
        throw new NotFoundException(ErrorMessages.CourseReview_NotFound);

      var courseReviewEntity = _replicaDbContext.CourseReviewLikes
        .FirstOrDefault(x => x.CourseReviewId == model.CourseReviewId &&
                             x.UserId == model.CreatorId &&
                             x.SchoolId == model.SchoolId);
      if (courseReviewEntity != null)
      {
        _masterDbContext.CourseReviewLikes.Remove(courseReviewEntity);
        _masterDbContext.SaveChanges();
      }
    }

    public void SetCourseReviewLike(List<IHasCourseReviewLike> data, Guid currentUserId, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var courseReviewIds = data.Select(x => x.Id).ToList();
      var mapData = dbContext.CourseReviewLikes
        .Where(x => courseReviewIds.Contains(x.CourseReviewId) && x.UserId == currentUserId)
        .Select(x => x.CourseReviewId)
        .ToList();
      data.ForEach(x => x.IsLiked = mapData.Contains(x.Id));
    }

    public void SetNumberOfLikes(List<IHasNumberOfLikes> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var courseReviewIds = data.Select(x => x.Id).ToList();
      var mapData = dbContext.CourseReviewLikes
        .Where(x => courseReviewIds.Contains(x.CourseReviewId))
        .GroupBy(x => x.CourseReviewId)
        .Select(x => new
        {
          Id = x.Key,
          NumberOfLikes = x.Count()
        })
        .ToDictionary(x => x.Id, x => x.NumberOfLikes);

      data.ForEach(x =>
      {
        x.NumberOfLike = mapData.GetValueOrDefault(x.Id);
      });
    }

    public void SetNumberOfLikes(IHasNumberOfLikes data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var courseReviewId = data.Id;
      var numberOfLikes = dbContext.CourseReviewLikes.Count(x => x.CourseReviewId == courseReviewId);
      data.NumberOfLike = numberOfLikes;
    }
  }
}

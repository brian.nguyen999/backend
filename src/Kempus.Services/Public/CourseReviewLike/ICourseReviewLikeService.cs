﻿using Kempus.Models.Public.CourseReviewLike;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Public.CourseReviewLike
{
  public interface ICourseReviewLikeService
  {
    Guid CreateIfNotExits(CourseReviewLikeInputModel model);
    void Delete(CourseReviewLikeInputModel model);
    void SetCourseReviewLike(List<IHasCourseReviewLike> data, Guid currentUserId, bool useMasterDb);
    void SetNumberOfLikes(List<IHasNumberOfLikes> data, bool useMasterDb);
    void SetNumberOfLikes(IHasNumberOfLikes data, bool useMasterDb);
  }
}

﻿using Kempus.Core.Models;
using Kempus.Models.Public.Banner;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Public.Banner
{
  public interface IBannerService
  {
    PagingResult<BannerListViewModel> Search(BannerFilterModel filter, Pageable pageable, bool useMasterDb);
  }
}

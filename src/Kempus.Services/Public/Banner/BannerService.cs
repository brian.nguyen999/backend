﻿using Kempus.Core.Models;
using Kempus.Core.Models.Configuration;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Banner;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Public.Banner
{
  public class BannerService : IBannerService
  {
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly MasterDbContext _masterDbContext;
    private S3Config _s3Config;

    public BannerService(
      ReplicaDbContext replicaDbContext,
      MasterDbContext masterDbContext,
       AwsConfig awsConfig
    )
    {
      _replicaDbContext = replicaDbContext;
      _masterDbContext = masterDbContext;
      _s3Config = awsConfig.S3;
    }

    public PagingResult<BannerListViewModel> Search(BannerFilterModel filter, Pageable pageable, bool useMasterDb)
    {
      var query = BuildQueryable(filter, useMasterDb);
      var totalCount = query.LongCount();
      if (totalCount < 1)
      {
        return new PagingResult<BannerListViewModel>(0, pageable.PageIndex, pageable.PageSize);
      }

      var data = query.Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new BannerListViewModel(x)).ToList();

      BuildResultData(data, useMasterDb);

      return new PagingResult<BannerListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    #region Private Methods
    private IQueryable<BannerEntity> BuildQueryable(BannerFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.Banners.AsNoTracking();
      query = BannerQueryBuilder<BannerEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    private void BuildResultData(List<BannerListViewModel> data, bool useMasterDb)
    {
      SetImageUrl(data);
    }

    private void SetImageUrl(List<BannerListViewModel> data)
    {
      data.ForEach(x =>
      {
        x.WebImage = $"{_s3Config.PublicDomain}/{x.WebImage}";
        x.MobileImage = $"{_s3Config.PublicDomain}/{x.MobileImage}";
      });
    }
    #endregion
  }
}

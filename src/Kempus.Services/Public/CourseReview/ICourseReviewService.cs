﻿using Kempus.Core.Models;
using Kempus.Models.Public.CourseReview;
using Kempus.Models.Public.CourseReviewActivity;

namespace Kempus.Services.Public.CourseReview
{
  public interface ICourseReviewService
  {
    Guid Create(CourseReviewInputModel input, Guid? userId, Guid? schoolId);

    PagingResult<CourseReviewListViewModel> Search(CourseReviewFilterModel filter, Pageable pageable,
      bool useMasterDb, bool useFullTextSearch, Guid userId);

    PagingResult<CourseReviewListViewModel> GetListMostRecent(CourseReviewActivitySummaryFilterModel filter,
      Pageable pageable,
      bool useMasterDb, bool useFullTextSearch, Guid userId);
    CourseReviewDetailModel GetDetail(Guid id);
  }
}
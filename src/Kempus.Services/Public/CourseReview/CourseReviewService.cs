﻿using Kempus.Core.Constants;
using Kempus.Core.Enums;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.FullTextSearch.Course;
using Kempus.Models.FullTextSearch.CourseReview;
using Kempus.Models.Public.Course;
using Kempus.Models.Public.CourseReview;
using Kempus.Models.Public.CourseReviewActivity;
using Kempus.Models.Public.CourseReviewLike;
using Kempus.Models.Public.DepartmentProgram;
using Kempus.Models.Public.Instructor;
using Kempus.Services.Common.KempTransaction;
using Kempus.Services.Common.Queue;
using Kempus.Services.FullTextSearch.Course;
using Kempus.Services.FullTextSearch.CourseReview;
using Kempus.Services.Public.Course;
using Kempus.Services.Public.CourseReviewLike;
using Kempus.Services.Public.CourseSummary;
using Kempus.Services.Public.DepartmentProgram;
using Kempus.Services.Public.User;
using Kempus.Services.Public.UserReferralStatistic;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.Public.CourseReview
{
  public class CourseReviewService : ICourseReviewService
  {
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly MasterDbContext _masterDbContext;
    private readonly ICourseService _courseService;
    private readonly IUserService _userService;
    private readonly ICourseSummaryService _courseSummaryService;
    private readonly ICourseFtsService _courseFtsService;
    private readonly ICourseReviewLikeService _courseReviewLikeService;
    private readonly IKempTransactionCommonService _kempTransactionService;
    private readonly IUserReferralStatisticService _userReferralStatisticService;
    private readonly IDepartmentProgramService _departmentProgramService;
    private readonly IOpenSearchEventPublisher _openSearchEventPublisher;
    private readonly ICourseReviewFtsService _courseReviewFtsService;
    public CourseReviewService(
      ReplicaDbContext replicaDbContext,
      MasterDbContext masterDbContext,
      ICourseService courseService,
      IUserService userService,
      ICourseSummaryService courseSummaryService,
      ICourseFtsService courseFtsService,
      ICourseReviewLikeService courseReviewLikeService,
      IKempTransactionCommonService kempTransactionService,
      IUserReferralStatisticService userReferralStatisticService,
      IDepartmentProgramService departmentProgramService,
      IOpenSearchEventPublisher openSearchEventPublisher,
      ICourseReviewFtsService courseReviewFtsService
      )
    {
      _replicaDbContext = replicaDbContext;
      _masterDbContext = masterDbContext;
      _courseService = courseService;
      _userService = userService;
      _courseSummaryService = courseSummaryService;
      _courseFtsService = courseFtsService;
      _courseReviewLikeService = courseReviewLikeService;
      _kempTransactionService = kempTransactionService;
      _userReferralStatisticService = userReferralStatisticService;
      _departmentProgramService = departmentProgramService;
      _openSearchEventPublisher = openSearchEventPublisher;
      _courseReviewFtsService = courseReviewFtsService;
    }

    public Guid Create(CourseReviewInputModel input, Guid? userId, Guid? schoolId)
    {
      var course = _courseService.GetDetail(input.CourseId);
      if (course == null)
        throw new NotFoundException(ErrorMessages.Course_NotFound);

      var courseReview = new CourseReviewEntity
      {
        CreatedDate = DateTime.UtcNow,
        CreatedBy = userId.GetValueOrDefault(),
        IsRequiredTextBook = input.IsRequiredTextBook,
        DifficultRating = input.DifficultRating,
        HoursSpend = input.HoursSpend,
        NumberOfExams = input.NumberOfExams,
        IsRequiredGroupProject = input.IsRequiredGroupProject,
        GradingCriteriaRating = input.GradingCriteriaRating,
        UserGrade = (byte)Enum.Parse<CoreEnums.UserGrading>(input.UserGrade),
        UserAchievementRating = input.UserAchievementRating,
        Characteristics = JsonConvert.SerializeObject(input.Characteristics),
        RecommendRating = input.RecommendRating,
        Comment = input.Comment,
        CourseId = input.CourseId,
        SchoolId = schoolId,
        UserId = userId
      };

      var result = 0;
      var transaction = _masterDbContext.Database.BeginTransaction();
      try
      {
        _masterDbContext.CourseReviews.Add(courseReview);
        result = _masterDbContext.SaveChanges();
        _courseSummaryService.SetCourseSummary(course.Id);
        transaction.Commit();
      }
      catch (Exception ex)
      {
        transaction.Rollback();
        throw;
      }

      // KWA-300 Update NumberFirstReview for Inviter
      if (result > 0)
      {
        // Update Referral Statistic for inviter
        _userReferralStatisticService.RefreshByFriendId(userId.GetValueOrDefault());

        // Create WriteCourseReview transaction. Add 3 Kemp
        _kempTransactionService.CreateTransaction(new Models.Common.KempTransactionInputModel()
        {
          UserIds = new List<Guid>() { userId.GetValueOrDefault() },
          TransactionType = (byte)KempTransactionType.WriteCourseReview,
          ReferenceGuid = courseReview.Guid,
          Amount = KempTransactionValue.WriteCourseReview,
          Action = (byte)KempTransactionAction.Add,
          CreatorId = userId.GetValueOrDefault(),
          SchoolId = schoolId.GetValueOrDefault()
        });
      }

      _openSearchEventPublisher.PublishMessageAsync(CoreConstants.OpenSearchEventType.CourseReview.Update,
        new List<Guid>() { courseReview.Guid });
      return courseReview.Guid;
    }

    public PagingResult<CourseReviewListViewModel> Search(CourseReviewFilterModel filter, Pageable pageable,
      bool useMasterDb, bool useFullTextSearch, Guid userId)
    {
      if (!useFullTextSearch || string.IsNullOrEmpty(filter.Keyword))
        return Search(filter, pageable, useMasterDb, userId);
      var ftsFilterModel = new CourseReviewFtsFilterModel()
      {
        Keyword = filter?.Keyword?.Trim(),
        SchoolId = filter.SchoolId.GetValueOrDefault(),
        Status = filter.Status.GetValueOrDefault()
      };

      return Search(ftsFilterModel, pageable, useMasterDb, userId);
    }

    public CourseReviewDetailModel GetDetail(Guid id)
    {
      var course = _replicaDbContext.Courses.FirstOrDefault(x => x.Guid == id);

      if (course == null)
        throw new NotFoundException(ErrorMessages.Course_NotFound);

      var model = new CourseReviewDetailModel
      {
        CourseName = course.Name,
        DepartmentId = course.DepartmentId
      };

      _departmentProgramService.SetDepartment(model, useMasterDb: false);
      SetInstructorNames(model, course.Guid);
      SetSummaryData(model, course.Guid);

      return model;
    }

    public PagingResult<CourseReviewListViewModel> GetListMostRecent(CourseReviewActivitySummaryFilterModel filter, Pageable pageable,
      bool useMasterDb, bool useFullTextSearch, Guid userId)
    {
      var query = BuildQueryable(filter, useMasterDb);
      var orderBy =
        QueryableUtils.GetOrderByFunction<CourseReviewActivitySummaryEntity>(pageable.GetSortables(nameof(CourseReviewActivitySummaryEntity.TotalActivitiesIn6Month)));

      var data = orderBy(query).Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => x).ToList();

      var courseReviewFilterModel = new CourseReviewFilterModel()
      {
        CourseReviewIds = data.Select(x => x.CourseReviewId).ToList()
      };

      return Search(courseReviewFilterModel, pageable, false, true, userId); ;
    }

    #region Private methods
    private void SetInstructorNames(CourseReviewDetailModel model, Guid courseId)
    {
      model.InstructorNames = _replicaDbContext.CourseInstructor
        .Where(x => x.CourseId == courseId)
        .Join(_replicaDbContext.Instructors,
        courseInstructor => courseInstructor.InstructorId,
        instructor => instructor.Guid,
        (courseInstructor, instructor) => instructor.Name)
        .ToList();
    }

    private void SetSummaryData(CourseReviewDetailModel model, Guid courseId)
    {
      var courseSummary = _replicaDbContext.CourseSummaries.FirstOrDefault(x => x.CourseId == courseId);
      if (courseSummary != null)
      {
        model.TotalReviews = courseSummary.TotalReviews;
        model.IsRequiredTextbook = courseSummary.IsTextbookRequired;
        model.NumberOfExams = courseSummary.NumberOfExams;
        model.RecommendedRate = courseSummary.RecommendedRate;
        model.GradingCriteriaRate = courseSummary.GradingCriteriaRate;
        model.CourseDifficultyRate = courseSummary.CourseDifficultyRate;
        model.IsRequiredGroupProjects = courseSummary.IsGroupProjects;
        model.HoursPerWeekRate = courseSummary.HoursPerWeekRate;
        model.GoalsAchivedRate = courseSummary.GoalsAchivedRate;
        model.StudentGradePercents = JsonUtils.Convert<List<GradePercent>>(courseSummary.StudentGradePercents);
        model.TopThreeCharacteristicInstructors = JsonUtils.Convert<List<string>>(courseSummary.TopThreeCharacteristicInstructors);
      }
    }

    private IQueryable<CourseReviewEntity> BuildQueryable(CourseReviewFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.CourseReviews.AsNoTracking();
      query = CourseReviewQueryBuilder<CourseReviewEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    private IQueryable<CourseReviewActivitySummaryEntity> BuildQueryable(CourseReviewActivitySummaryFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.CourseReviewActivitySummaries.AsNoTracking();
      query = CourseReviewActivitySummaryQueryBuilder<CourseReviewActivitySummaryEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    private void BuildResultData(List<CourseReviewListViewModel> data, bool useMasterDb, Guid userId)
    {
      SetCourses(data, useMasterDb);
      SetInstructors(data, useMasterDb);
      SetUsers(data, useMasterDb);
      _departmentProgramService.SetDepartments(data.Select(x => x.Course).ToList<IHasDepartment>(), useMasterDb);
      _courseReviewLikeService.SetCourseReviewLike(data.ToList<IHasCourseReviewLike>(), userId, useMasterDb);
      _courseReviewLikeService.SetNumberOfLikes(data.ToList<IHasNumberOfLikes>(), useMasterDb);
    }

    private void SetCourses(List<CourseReviewListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var courseIds = data.Select(x => x.CourseId).ToList();
      var mapData = dbContext.Courses.Where(x => courseIds.Contains(x.Guid))
        .ToDictionary(x => x.Guid, x => x);

      data.ForEach(x =>
      {
        var item = mapData.GetValueOrDefault(x.CourseId.Value);
        if (item != null)
        {
          x.Course = new CourseDetailModel
          {
            Id = item.Guid,
            Name = item.Name,
            DepartmentId = item.DepartmentId
          };
        }
      });
    }

    private void SetInstructors(List<CourseReviewListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var courseIds = data.Select(x => x.CourseId).ToList();
      var mapData = dbContext.CourseInstructor
        .Where(x => courseIds.Contains(x.CourseId))
        .Join(dbContext.Instructors,
          courseInstructor => courseInstructor.InstructorId,
          instructor => instructor.Guid,
          (courseInstructor, instructor) => new
          {
            CourseId = courseInstructor.CourseId,
            Instructor = new InstructorDetailModel
            {
              Id = instructor.Guid,
              Name = instructor.Name,
            }
          })
        .ToList()
        .ToLookup(x => x.CourseId, x => x.Instructor);


      data.ForEach(x =>
      {
        x.Course.Instructors = mapData
          .Where(q => q.Key == x.CourseId)
          .SelectMany(o => o)
          .ToList();
      });
    }

    private void SetUsers(List<CourseReviewListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var userIds = data.Select(x => x.CreatedBy).ToList();
      var mapData = dbContext.Users.Where(x => userIds.Contains(x.UserId))
        .ToDictionary(x => x.UserId, x => x.NickName);

      data.ForEach(x =>
      {
        var name = mapData.GetValueOrDefault(x.CreatedBy);
        if (string.IsNullOrWhiteSpace(name)) return;

        x.UserName = name;
      });
    }

    public PagingResult<CourseReviewListViewModel> Search(CourseReviewFtsFilterModel filter, Pageable pageable, bool useMasterDb, Guid userId)
    {
      var ftsResult = _courseReviewFtsService.SearchDocumentAsync(filter, pageable).Result;
      if (ftsResult.TotalCount < 1 || ftsResult.TotalPages < pageable.PageIndex)
        return new PagingResult<CourseReviewListViewModel>(ftsResult.TotalCount, ftsResult.PageIndex, ftsResult.PageSize);
      var courseReviewIds = ftsResult.Data.Select(x => x.Id).ToList();
      var courseReviewFilterModel = new CourseReviewFilterModel()
      {
        CourseReviewIds = courseReviewIds,
        SchoolId = filter.SchoolId
      };

      var query = BuildQueryable(courseReviewFilterModel, useMasterDb);
      var orderBy = QueryableUtils.GetOrderByFunction<CourseReviewEntity>(
        pageable.GetSortables(nameof(CourseReviewEntity.CreatedDate)));
      var data = orderBy(query).Select(x => new CourseReviewListViewModel(x)).ToList();
      BuildResultData(data, useMasterDb, userId);

      if (data.Any())
        _openSearchEventPublisher.PublishMessageAsync(CoreConstants.OpenSearchEventType.Course.Update,
          data.Select(x => x.Id).Distinct().ToList());
      return new PagingResult<CourseReviewListViewModel>(data, ftsResult.TotalCount, ftsResult.PageIndex, ftsResult.PageSize);
    }

    public PagingResult<CourseReviewListViewModel> Search(CourseReviewFilterModel filter, Pageable pageable, bool useMasterDb, Guid userId)
    {
      var query = BuildQueryable(filter, useMasterDb);
      var totalCount = query.LongCount();
      if (totalCount < 1)
        return new PagingResult<CourseReviewListViewModel>(0, pageable.PageIndex, pageable.PageSize);
      var orderBy =
        QueryableUtils.GetOrderByFunction<CourseReviewEntity>(pageable.GetSortables(nameof(CourseReviewEntity.CreatedDate)));
      var data = orderBy(query).Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new CourseReviewListViewModel(x)).ToList();
      BuildResultData(data, useMasterDb, userId);
      if (data.Any())
        _openSearchEventPublisher.PublishMessageAsync(CoreConstants.OpenSearchEventType.CourseReview.Update,
          data.Select(x => x.Id).Distinct().ToList());
      return new PagingResult<CourseReviewListViewModel>(data, totalCount, pageable.PageIndex,
        pageable.PageSize);
    }

    #endregion
  }
}
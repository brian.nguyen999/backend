﻿using Kempus.Core.Models;
using Kempus.Entities;
using Kempus.Models.Public.School;

namespace Kempus.Services.Public.School
{
  public interface ISchoolService
  {
    PagingResult<SchoolListViewModel> Search(SchoolFilterModel filterModel, Pageable pageable, bool useMasterDb);
    Task<SchoolEntity> GetSchoolByDomain(string domain);
    SchoolDetailModel GetDetail(Guid id);
    Task<string> GetImportTemplateLink();
    Guid Create(SchoolInputModel model);
    void Update(SchoolInputModel model);
    Task<bool> UploadAsync(SchoolUploadInputModel input);
    Task IndexCourseBySchoolIdAsync(Guid schoolId, DateTime? created = null);
    SchoolCountModel GetTotalSchoolCount();
  }
}
using CsvHelper;
using CsvHelper.Configuration;
using Kempus.Core;
using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Core.Models.Configuration;
using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Common;
using Kempus.Models.Public.School;
using Kempus.Services.Common.Queue;
using Kempus.Services.Common.Redis;
using Kempus.Services.Common.Upload;
using Kempus.Services.Public.Notification;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using StackExchange.Redis;
using System.Globalization;
using static Kempus.Core.Constants.CoreConstants;
using static Kempus.Core.Enums.CoreEnums;
using static Kempus.Core.Utils.FileUtilities;

namespace Kempus.Services.Public.School
{
  public class SchoolService : ISchoolService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly SessionStore _sessionStore;
    private readonly IEmailNotificationService _emailNotificationService;
    private readonly DomainConfig _domainConfig;
    private readonly AwsS3IntegrationHelper _awsS3IntegrationHelper;
    private readonly S3Config _s3Config;
    private readonly RedisConfig _redisConfig;
    private readonly IOpenSearchEventPublisher _openSearchEventPublisher;
    private readonly IServiceProvider _provider;
    private readonly IRedisService _redisService;
    private readonly IConnectionMultiplexer _redis;


    public SchoolService(
      MasterDbContext masterDbContext,
      ReplicaDbContext replicaDbContext,
      SessionStore sessionStore,
      IEmailNotificationService emailNotificationService,
      DomainConfig domainConfig,
      AwsS3IntegrationHelper awsS3IntegrationHelper,
      AwsConfig awsConfig,
      IOpenSearchEventPublisher openSearchEventPublisher,
      IServiceProvider provider,
      IRedisService redisService,
      IConnectionMultiplexer redis
      )
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
      _sessionStore = sessionStore;
      _emailNotificationService = emailNotificationService;
      _domainConfig = domainConfig;
      _awsS3IntegrationHelper = awsS3IntegrationHelper;
      _s3Config = awsConfig.S3;
      _openSearchEventPublisher = openSearchEventPublisher;
      _provider = provider;
      _redisService = redisService;
      _redis = redis;
      _redisConfig = awsConfig.Redis;
    }

    public PagingResult<SchoolListViewModel> Search(SchoolFilterModel filterModel, Pageable pageable, bool useMasterDb)
    {
      var query = BuildQueryable(filterModel, useMasterDb);

      var totalCount = query.LongCount();
      if (totalCount < 1)
        return new PagingResult<SchoolListViewModel>(0, pageable.PageIndex, pageable.PageSize);

      var orderBy = QueryableUtils.GetOrderByFunction<SchoolEntity>(pageable.GetSortables(nameof(PostEntity.CreatedDate)));

      var data = orderBy(query).Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new SchoolListViewModel(x)).ToList();

      BuildResultData(data, useMasterDb);

      return new PagingResult<SchoolListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    public async Task<SchoolEntity> GetSchoolByDomain(string domain)
    {
      return await _masterDbContext.Schools.FirstOrDefaultAsync(x =>
        !string.IsNullOrEmpty(domain) && x.EmailDomain.Contains(domain));
    }

    public SchoolDetailModel GetDetail(Guid id)
    {
      var filterModel = new SchoolFilterModel { Id = id };
      var query = BuildQueryable(filterModel, false);
      var school = query.FirstOrDefault();

      if (school == null)
        throw new NotFoundException(ErrorMessages.School_NotFound);

      var result = new SchoolDetailModel
      {
        Id = school.Guid,
        SchoolName = school.Name,
        PrimaryColor = school.PrimaryColor,
        SecondaryColor = school.SecondaryColor,
        SchoolEmailFormat = school.EmailDomain.Split(SchoolEmailFormat.Delimeter).ToList(),
        IsActivated = school.IsActivated,
        IsRequiredReferralCode = school.IsRequiredReferralCode,
        IsShowHomePage = school.IsShowHomePage

      };

      var schoolLifeTimeAnalytics = _replicaDbContext.SchoolLifeTimeAnalytics.FirstOrDefault(x => x.SchoolId == result.Id);
      if (schoolLifeTimeAnalytics != null)
        result.NumberOfStudent = schoolLifeTimeAnalytics.Users;

      return result;
    }

    public Guid Create(SchoolInputModel model)
    {
      model.SchoolEmailFormat = model.SchoolEmailFormat.Distinct().ToList();

      ValidateBeforeCreateOrUpdate(model);
      var entity = new SchoolEntity
      {
        Name = model.SchoolName,
        PrimaryColor = model.PrimaryColor,
        SecondaryColor = model.SecondaryColor,
        EmailDomain = string.Join(SchoolEmailFormat.Delimeter, model.SchoolEmailFormat),
        IsActivated = model.Status,
        IsRequiredReferralCode = model.IsRequiredReferralCode,
        CreatedBy = _sessionStore.UserId ?? Guid.Empty,
        LastUpdateStatusDateTime = DateTime.UtcNow,
        IsShowHomePage = model.IsShowHomePage,
      };

      var newSchoolAnalytic = new SchoolLifeTimeAnalyticEntity()
      {
        SchoolId = entity.Guid,
      };

      _masterDbContext.SchoolLifeTimeAnalytics.Add(newSchoolAnalytic);
      _masterDbContext.Schools.Add(entity);
      _masterDbContext.SaveChanges();
      RefreshSchoolCountCache();
      ClearSchoolListCache();
      return entity.Guid;
    }

    public void Update(SchoolInputModel model)
    {
      model.SchoolEmailFormat = model.SchoolEmailFormat.Distinct().ToList();

      ValidateBeforeCreateOrUpdate(model);

      var existSchool = _masterDbContext.Schools.FirstOrDefault(x => x.Guid == model.Id);
      var isActiveSchool = !existSchool.IsActivated && model.Status;

      existSchool.Name = model.SchoolName;
      existSchool.PrimaryColor = model.PrimaryColor;
      existSchool.SecondaryColor = model.SecondaryColor;
      existSchool.EmailDomain = string.Join(SchoolEmailFormat.Delimeter, model.SchoolEmailFormat);
      existSchool.LastUpdateStatusDateTime = model.Status != existSchool.IsActivated ? DateTime.UtcNow : existSchool.LastUpdateStatusDateTime;
      existSchool.IsActivated = model.Status;
      existSchool.IsRequiredReferralCode = model.IsRequiredReferralCode;
      existSchool.UpdatedBy = _sessionStore.UserId ?? Guid.Empty;
      existSchool.UpdatedDate = DateTime.UtcNow;
      existSchool.IsShowHomePage = model.IsShowHomePage;

      _masterDbContext.Update(existSchool);
      var result = _masterDbContext.SaveChanges();
      RefreshSchoolCountCache();
      ClearSchoolListCache();

      // TODO Add to Background Job
      // KWA-144 Auto send sign up notification email to all emails in waitlist
      if (result > 0 && isActiveSchool)
      {
        var listEmail = _replicaDbContext.WaitList
          .Where(x => x.SchoolId == existSchool.Guid)
          .Select(x => new EmailAddress() { Email = x.Email })
          .ToList();
        if (listEmail.Any())
        {
          var linkSignUp = $"{_domainConfig.Site}/{_domainConfig.Pages.SignUp}";
          _emailNotificationService.SendCmsActiveSchoolNotificationAsync(existSchool.Name, linkSignUp, listEmail).Wait();
        }
      }
    }

    public SchoolCountModel GetTotalSchoolCount()
    {
      var cacheResult = _redisService.GetValueByKeyAsync(RedisCacheKey.School_TotalSchoolCount).Result;
      return !string.IsNullOrEmpty(cacheResult)
        ? JsonConvert.DeserializeObject<SchoolCountModel>(cacheResult)
        : RefreshSchoolCountCache();
    }

    public SchoolCountModel RefreshSchoolCountCache()
    {
      var cacheObject = GetSchoolCount();
      _redisService.AddCacheAsync(RedisCacheKey.School_TotalSchoolCount,
        JsonConvert.SerializeObject(cacheObject), DateTime.UtcNow.AddHours(24));
      return cacheObject;
    }

    private SchoolCountModel GetSchoolCount()
    {
      try
      {
        var result =
          _masterDbContext.Schools
            .GroupBy(x => x.IsActivated)
            .Select(grp => new
            {
              IsActivated = grp.Key,
              TotalSchools = grp.Count()
            }).ToList();
        return new SchoolCountModel
        {
          TotalOpenedSchools = result.FirstOrDefault(x => x.IsActivated)?.TotalSchools ?? 0,
          TotalWaitedSchools = result.FirstOrDefault(x => !x.IsActivated)?.TotalSchools ?? 0,
        };
      }
      catch (Exception)
      {
        return new SchoolCountModel();
      }
    }

    public void ClearSchoolListCache()
    {
      var server = _redis.GetServer(_redisConfig.Endpoint, _redisConfig.Port);
      var queueKeys = server.Keys(pattern: $"*{RedisCacheKey.School_SchoolList}*");
      if (queueKeys.Any())
      {
        foreach (var cacheKey in queueKeys)
        {
          _redisService.RemoveCacheAsync(cacheKey).Wait();
        }
      }
    }

    #region Admin

    public async Task<string> GetImportTemplateLink()
    {
      var files = await _awsS3IntegrationHelper.GetFilesFromFolder(SchoolImportTemplate.S3Folder);
      var file = files.FirstOrDefault();

      return file ?? await _awsS3IntegrationHelper.UploadFileToS3Async(Path.Combine(Directory.GetCurrentDirectory(), $"{SchoolImportTemplate.LocalFolder}/{SchoolImportTemplate.FileName}"), $"{SchoolImportTemplate.S3Folder}/{SchoolImportTemplate.FileName}");
    }

    public async Task<bool> UploadAsync(SchoolUploadInputModel input)
    {
      // 1.Validate
      ValidateBeforeUpload(input);

      // 2.Upload file to s3 and save info to DB to track history
      var currentTime = DateTime.UtcNow;
      string s3UploadPath = $"file-uploads/{currentTime.Year}/{currentTime.Month}/{currentTime.Day}/";
      var uploadResponse = await _awsS3IntegrationHelper.UploadFileAsync(input.File, _s3Config.Bucket, s3UploadPath, input.File.FileName, false);

      FileUploadTrackingEntity trackingEntity = new FileUploadTrackingEntity()
      {
        SchoolId = input.SchoolId,
        FileName = input.File.FileName,
        Type = input.File.ContentType,
        FileSize = input.File.Length,
        FileUrl = uploadResponse.S3Url,
        CreatedBy = input.CreatorId,
        CreatedDate = currentTime
      };

      // 3.Read data from CSV
      var courseRecords = new List<ImportCourseModel>();
      var config = new CsvConfiguration(CultureInfo.InvariantCulture)
      {
        Delimiter = "|",
        BadDataFound = arg =>
        {
          throw new BadRequestException(String.Format(ErrorMessages.School_Upload_InvalidValue_HtmlTag, arg.Context.Parser.RawRow));
        },
        MissingFieldFound = null
      };

      using (var reader = new StreamReader(input.File.OpenReadStream()))
      using (var csv = new CsvReader(reader, config))
      {

        ValidateCsvFormat(csv);

        try
        {
          courseRecords = csv.GetRecords<ImportCourseModel>().ToList();
        }
        catch (CsvHelperException ex)
        {
          throw new BadRequestException(String.Format(ErrorMessages.School_Upload_InvalidValue, ex.Context.Parser.RawRow));
        }

      }

      if (courseRecords == null || courseRecords.Count == 0)
        return true;

      courseRecords.ForEach(x =>
      {
        int rowNumber = courseRecords.IndexOf(x) + 2;
        if (string.IsNullOrEmpty(x.CourseFullName) || string.IsNullOrEmpty(x.Instructor) || string.IsNullOrEmpty(x.DepartmentProgram)
        || string.IsNullOrWhiteSpace(x.CourseFullName) || string.IsNullOrWhiteSpace(x.Instructor) || string.IsNullOrWhiteSpace(x.DepartmentProgram))
        {
          throw new BadRequestException(String.Format(ErrorMessages.School_Upload_EmptyValue, rowNumber));
        }

        if (StringHelper.ContainsHTMLElements(x.CourseFullName) || StringHelper.ContainsHTMLElements(x.Instructor) || StringHelper.ContainsHTMLElements(x.DepartmentProgram))
        {
          throw new BadRequestException(String.Format(ErrorMessages.School_Upload_InvalidValue_HtmlTag, rowNumber));
        }
      });

      var courseConvertedRecords = courseRecords.Select((x, index) => new ImportCourseConvertedModel(x, index)).ToList();

      // 4.Check Instructor Exits
      var lstInstructorNameUpload = courseConvertedRecords.SelectMany(x => x.Instructor).Distinct().ToList();

      var lstExistInstructor = _masterDbContext.Instructors.Where(x => lstInstructorNameUpload.Contains(x.Name) && x.SchoolId == input.SchoolId).ToList();
      var lstNewInstructor = lstInstructorNameUpload.Where(x => !lstExistInstructor.Select(x => x.Name).Contains(x))
        .Select(x => new InstructorEntity()
        {
          Name = x,
          SchoolId = input.SchoolId,
          CreatedBy = input.CreatorId,
          CreatedDate = currentTime,
          IsActivated = true,
          GeneratedTypeId = (byte)GeneratedType.ImportedFromCSV
        }).ToList();
      var instructorEntityMap = lstExistInstructor.Union(lstNewInstructor)
        .GroupBy(
          x => x.Name,
          x => x.Guid,
          (key, g) => new { Name = key, Guids = g.ToList() })
        .ToDictionary(x => x.Name, x => x.Guids.FirstOrDefault());

      // 5.Check DepartmentProgram Exist
      var lstDepartmentUpload = courseConvertedRecords.Select(x => x.DepartmentProgram).Distinct().ToList();

      var lstExistDepartment = _masterDbContext.DepartmentPrograms.Where(x => lstDepartmentUpload.Contains(x.Name) && x.SchoolId == input.SchoolId).ToList();
      var lstNewDepartment = lstDepartmentUpload.Where(x => !lstExistDepartment.Select(x => x.Name).Contains(x))
        .Select(x => new DepartmentProgramEntity()
        {
          Name = x,
          SchoolId = input.SchoolId,
          CreatedBy = input.CreatorId,
          CreatedDate = currentTime,
          Status = (byte)DepartmentProgramStatus.Active,
          GeneratedTypeId = (byte)GeneratedType.ImportedFromCSV
        }).ToList();
      var departmentEntityMap = lstExistDepartment.Union(lstNewDepartment)
        .GroupBy(
          x => x.Name,
          x => x.Guid,
          (key, g) => new { Name = key, Guids = g.ToList() })
        .ToDictionary(x => x.Name, x => x.Guids.FirstOrDefault());

      // 6.Check Course Exist
      var csvRowIndexList = courseConvertedRecords.Select(x => x.CsvRowIndex).ToList();
      var listIdImported = csvRowIndexList.Select(x => BuildIdImported(input.SchoolId, x)).ToList();
      var lstExistCourseEntity = _masterDbContext.Courses
        .Where(x => !string.IsNullOrEmpty(x.IdImported) && listIdImported.Contains(x.IdImported))
        .ToList();
      var lstNewCourse = courseConvertedRecords
        .Where(x => !lstExistCourseEntity.Select(x => x.CsvRowIndex).Contains(x.CsvRowIndex))
        .ToList();

      // 7.Prepare data to Insert
      List<CourseEntity> lstNewCourseEntity = new List<CourseEntity>();
      List<CourseInstructorEntity> lstNewCourseInstructorEntity = new List<CourseInstructorEntity>();
      List<CourseInstructorEntity> lstEistCourseInstructorEntity = _masterDbContext.CourseInstructor
        .Where(x => lstExistCourseEntity.Select(y => y.Guid).Contains(x.CourseId))
        .ToList();

      foreach (var courseRecord in lstNewCourse)
      {
        var departmentId = departmentEntityMap.GetValueOrDefault(courseRecord.DepartmentProgram);

        CourseEntity newCourseEntity = new CourseEntity()
        {
          Name = courseRecord.CourseFullName,
          CreatedBy = input.CreatorId,
          CreatedDate = currentTime,
          SchoolId = input.SchoolId,
          DepartmentId = departmentId,
          FileUploadTrackingId = trackingEntity.Guid,
          CsvRowIndex = courseRecord.CsvRowIndex,
          IsActivated = true,
          IdImported = BuildIdImported(input.SchoolId, courseRecord.CsvRowIndex)
        };
        lstNewCourseEntity.Add(newCourseEntity);

        foreach (var instructor in courseRecord.Instructor)
        {
          var instructorId = instructorEntityMap.GetValueOrDefault(instructor);
          CourseInstructorEntity newCourseInstructorEntity = new CourseInstructorEntity()
          {
            CourseId = newCourseEntity.Guid,
            InstructorId = instructorId
          };

          lstNewCourseInstructorEntity.Add(newCourseInstructorEntity);
        }
      }

      // 8.Prepare data to Update
      var objectMap = courseConvertedRecords.ToDictionary(x => BuildIdImported(input.SchoolId, x.CsvRowIndex), x => x);
      foreach (var courseEntity in lstExistCourseEntity)
      {
        var updateObject = objectMap.GetValueOrDefault(BuildIdImported(input.SchoolId, courseEntity.CsvRowIndex));
        if (updateObject == null) continue;

        var departmentId = departmentEntityMap.GetValueOrDefault(updateObject.DepartmentProgram);

        courseEntity.Name = updateObject.CourseFullName;
        courseEntity.DepartmentId = departmentId;
        courseEntity.UpdatedBy = input.CreatorId;
        courseEntity.UpdatedDate = currentTime;
        courseEntity.FileUploadTrackingId = trackingEntity.Guid;

        foreach (var instructor in updateObject.Instructor)
        {
          var instructorId = instructorEntityMap.GetValueOrDefault(instructor);
          CourseInstructorEntity newCourseInstructorEntity = new CourseInstructorEntity()
          {
            CourseId = courseEntity.Guid,
            InstructorId = instructorId
          };

          lstNewCourseInstructorEntity.Add(newCourseInstructorEntity);
        }
      }

      // 9.Import data to DB
      using var transaction = _masterDbContext.Database.BeginTransaction();
      try
      {
        // Batch insert
        List<CourseEntity> lstCourseEntityUploaded = lstNewCourseEntity.Union(lstExistCourseEntity).ToList();
        List<Guid> lstCourseEntityIdExist = lstExistCourseEntity.Select(x => x.Guid).ToList();
        List<Guid> lstCourseEntityIdNew = lstNewCourseEntity.Select(x => x.Guid).ToList();
        List<Guid> lstInstructorIdInserted = new List<Guid>();
        List<Guid> lstDepartmentIdInserted = new List<Guid>();
        List<Guid> lstCourseInstructorIdInserted = new List<Guid>();

        int skip = 0, take = 500;
        var subObjectList = lstCourseEntityUploaded.Skip(skip).Take(take).ToList();
        do
        {
          using var scope = _provider.CreateScope();
          var dbContext = scope.ServiceProvider.GetService<MasterDbContext>();

          var updateSubCourses = subObjectList.Where(x => lstCourseEntityIdExist.Contains(x.Guid)).ToList();
          var insertSubCourses = subObjectList.Where(x => lstCourseEntityIdNew.Contains(x.Guid)).ToList();

          var deleteSubCourseInstructors = lstEistCourseInstructorEntity.Where(ci => updateSubCourses.Any(c => c.Guid == ci.CourseId)).ToList();
          var insertSubCourseInstructors = lstNewCourseInstructorEntity.Where(ci => subObjectList.Any(c => c.Guid == ci.CourseId) && !lstCourseInstructorIdInserted.Contains(ci.Guid)).ToList();
          lstCourseInstructorIdInserted.AddRange(insertSubCourseInstructors.Select(x => x.Guid));

          var relationshipInstructorIds = insertSubCourseInstructors.Select(x => x.InstructorId).ToList();
          var insertSubInstructors = lstNewInstructor.Where(x => relationshipInstructorIds.Contains(x.Guid) && !lstInstructorIdInserted.Contains(x.Guid)).ToList();
          lstInstructorIdInserted.AddRange(insertSubInstructors.Select(x => x.Guid));

          var insertSubDepartmentIds = subObjectList.Select(x => x.DepartmentId).ToList();
          var insertSubDepartments = lstNewDepartment.Where(x => insertSubDepartmentIds.Contains(x.Guid) && !lstDepartmentIdInserted.Contains(x.Guid)).ToList();
          lstDepartmentIdInserted.AddRange(insertSubDepartments.Select(x => x.Guid));

          dbContext.CourseInstructor.RemoveRange(deleteSubCourseInstructors);

          dbContext.Instructors.AddRange(insertSubInstructors);
          dbContext.DepartmentPrograms.AddRange(insertSubDepartments);
          dbContext.Courses.AddRange(insertSubCourses);
          dbContext.CourseInstructor.AddRange(insertSubCourseInstructors);

          dbContext.Courses.UpdateRange(updateSubCourses);
          dbContext.SaveChanges();

          skip += take;
          subObjectList = lstCourseEntityUploaded.Skip(skip).Take(take).ToList();
        } while (subObjectList.Any());

        // 9.4 Insert import tracking history
        _masterDbContext.FileUploadTrackings.Add(trackingEntity);
        _masterDbContext.SaveChanges();
        transaction.Commit();
      }
      catch (Exception ex)
      {
        transaction.Rollback();
        throw new BadRequestException(ex.Message);
      }

      await IndexCourseBySchoolIdAsync(input.SchoolId, currentTime);

      return true;
    }

    public async Task IndexCourseBySchoolIdAsync(Guid schoolId, DateTime? createdDate = null)
    {
      var query = _replicaDbContext.Courses.Where(x => x.SchoolId == schoolId);
      if (createdDate.HasValue)
      {
        query = query.Where(x => x.CreatedDate >= createdDate || x.UpdatedDate >= createdDate);
      }

      var totalCourses = query.Count();
      if (totalCourses < 1) return;

      var pageSize = 1000;
      var totalPages = Math.Round((decimal)totalCourses / pageSize, 0);
      for (int pageIndex = 0; pageIndex <= totalPages; pageIndex++)
      {
        var offset = pageSize * pageIndex;
        var courseIds = query.Select(x => x.Guid).Skip(offset).Take(pageSize).ToList();
        await _openSearchEventPublisher.PublishMessageAsync(CoreConstants.OpenSearchEventType.Course.Update, courseIds.Distinct().ToList());
      }
    }
    #endregion

    #region Private methods
    private string BuildIdImported(Guid schoolId, long CsvIndex)
    {
      return $"{schoolId}_{CsvIndex}";
    }
    private void ValidateBeforeCreateOrUpdate(SchoolInputModel model)
    {
      bool isExistSchoolEmailFormat = false;
      List<string> emailFormatExist = new List<string>();
      List<string> emailFormatInValid = new List<string>();

      if (model.Id != null)
      {
        var existSchool = _masterDbContext.Schools.FirstOrDefault(x => x.Guid == model.Id);
        if (existSchool == null)
          throw new NotFoundException(ErrorMessages.School_NotFound);

        if (_masterDbContext.Schools.Any(x => x.Guid != existSchool.Guid && x.Name.ToLower() == model.SchoolName.ToLower()))
          throw new NotFoundException(ErrorMessages.School_ExistedName);

        model.SchoolEmailFormat.ForEach(x =>
        {
          if (x.Contains(SchoolEmailFormat.Delimeter))
          {
            emailFormatInValid.Add(x);
          }
          isExistSchoolEmailFormat = _masterDbContext.Schools.Any(y => y.Guid != existSchool.Guid && y.EmailDomain.Contains(x));
          if (isExistSchoolEmailFormat)
          {
            emailFormatExist.Add(x);
          }
        });
      }
      else
      {
        if (_masterDbContext.Schools.Any(x => x.Name.ToLower() == model.SchoolName.ToLower()))
          throw new NotFoundException(ErrorMessages.School_ExistedName);

        model.SchoolEmailFormat.ForEach(x =>
        {
          if (x.Contains(SchoolEmailFormat.Delimeter))
          {
            emailFormatInValid.Add(x);
          }
          isExistSchoolEmailFormat = _masterDbContext.Schools.Any(y => y.EmailDomain.Contains(x));
          if (isExistSchoolEmailFormat)
          {
            emailFormatExist.Add(x);
          }
        });
      }

      if (emailFormatInValid.Any())
        throw new BadRequestException(String.Format(ErrorMessages.School_InvalidEmailFormat, string.Join(SchoolEmailFormat.Delimeter, emailFormatInValid)));

      if (emailFormatExist.Any())
        throw new BadRequestException(String.Format(ErrorMessages.School_ExistedEmailFormat, string.Join(SchoolEmailFormat.Delimeter, emailFormatExist)));
    }

    private void ValidateCsvFormat(CsvReader csv)
    {
      csv.Read();
      csv.ReadHeader();

      List<string> headers = csv.HeaderRecord?.ToList();
      if (headers.Count != 3)
      {
        throw new BadRequestException($"Please use | as separator");
      }
      if (!headers.Exists(x => x == nameof(ImportCourseModel.CourseFullName)) ||
          !headers.Exists(x => x == nameof(ImportCourseModel.Instructor)) ||
          !headers.Exists(x => x == nameof(ImportCourseModel.DepartmentProgram))
      )
      {
        throw new BadRequestException($"Header needs to follow the format: {nameof(ImportCourseModel.CourseFullName)} | {nameof(ImportCourseModel.Instructor)} | {nameof(ImportCourseModel.DepartmentProgram)}.");
      }
    }

    private void ValidateBeforeUpload(SchoolUploadInputModel input)
    {
      if (!FileHelper.IsCsvFile(input.File.ContentType))
      {
        throw new BadRequestException(String.Format(ErrorMessages.School_Upload_UnsupportedFile, input.File.ContentType));
      }

      if (input.File.FileName.Length > 250)
      {
        throw new BadRequestException(String.Format(ErrorMessages.School_Upload_FileNameTooLong, 250));
      }

      if (input.File.Length > _s3Config.MaximumFileSize)
      {
        throw new BadRequestException(ErrorMessages.School_Upload_FileSizeTooBig);
      }

      if (!_replicaDbContext.Schools.Where(x => x.Guid == input.SchoolId).Any())
      {
        throw new BadRequestException(ErrorMessages.School_NotFound);
      }
    }

    private void BuildResultData(List<SchoolListViewModel> data, bool useMasterDb)
    {
      SetNumberOfUserAndNumberSignUp(data, useMasterDb);
      SetTotalKempsBalances(data, useMasterDb);
    }

    private void SetNumberOfUserAndNumberSignUp(List<SchoolListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var schoolIds = data.Select(x => x.Guid).ToList();
      var mapData = dbContext.SchoolLifeTimeAnalytics.Where(x => schoolIds.Contains(x.SchoolId)).ToDictionary(x => x.SchoolId, x => new
      {
        x.Users,
        x.NoOfWaitList
      });

      data.ForEach(x =>
      {
        var item = mapData.GetValueOrDefault(x.Guid);
        if (item == null) return;

        x.NumberOfUser = item.Users;
        x.NumberOfSignUp = item.NoOfWaitList;
      });
    }

    private void SetTotalKempsBalances(List<SchoolListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var schoolIds = data.Select(x => x.Guid).ToList();
      var mapData = dbContext.Users.Where(x => schoolIds.Contains(x.SchoolId))
        .Join(dbContext.KempWallets,
          user => user.UserId,
          wallet => wallet.UserId,
          (user, wallet) => new { user, wallet })
        .GroupBy(x => x.user.SchoolId)
        .Select(x => new
        {
          SchoolId = x.Key,
          TotalBalance = x.Sum(x => x.wallet.Amount)
        })
        .ToDictionary(x => x.SchoolId, x => x.TotalBalance);

      data.ForEach(x =>
      {
        var item = mapData.GetValueOrDefault(x.Guid);
        if (item == null) return;
        x.TotalKemBalance = item;
      });
    }

    private IQueryable<SchoolEntity> BuildQueryable(SchoolFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.Schools.AsNoTracking();
      query = SchoolQueryBuilder<SchoolEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    #endregion
  }
}
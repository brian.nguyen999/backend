﻿using Kempus.Entities;
using Kempus.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Public.EmailTemplate
{
  public class EmailTemplateService : IEmailTemplateService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ILogger _logger;

    public EmailTemplateService(
      MasterDbContext masterDbContext,
      ILogger<EmailTemplateService> logger)
    {
      _masterDbContext = masterDbContext;
      _logger = logger;
    }

    public async Task<EmailTemplateEntity> GetByType(string type)
    {
      return await _masterDbContext.EmailTemplates.FirstOrDefaultAsync(x => x.Type == type);
    }
  }
}
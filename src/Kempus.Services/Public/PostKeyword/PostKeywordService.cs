﻿using Kempus.Core.Errors;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Services.Public.Keyword;
using Microsoft.EntityFrameworkCore;

namespace Kempus.Services.Public.PostKeyword
{
  public class PostKeywordService : IPostKeywordService
  {
    private readonly MasterDbContext _masterContext;
    private readonly IKeywordService _keywordService;

    public PostKeywordService(
      MasterDbContext masterContext,
      IKeywordService keywordService
    )
    {
      _masterContext = masterContext;
      _keywordService = keywordService;
    }

    public void Insert(Guid postId, List<string> keywords)
    {
      var keywordIds = _keywordService.InsertAndGetIds(keywords);
      List<PostKeywordEntity> entities = new();
      foreach (var item in keywordIds)
      {
        entities.Add(new PostKeywordEntity
        {
          PostId = postId,
          KeywordId = item
        });
      }

      _masterContext.AddRange(entities);
      _masterContext.SaveChanges();
    }

    public void Update(Guid postId, List<string> keywords)
    {
      var keywordIds = _keywordService.InsertAndGetIds(keywords);
      var post = _masterContext.Posts.FirstOrDefault(x => x.Guid == postId);
      if (post == null)
        throw new NotFoundException(ErrorMessages.Post_NotFound);

      var existPostKeywords = _masterContext.PostKeywords.Where(x => x.PostId == postId).ToList();

      // Delete old postkeywords
      var deletePostKeywords = existPostKeywords.Where(x => !keywordIds.Contains(x.KeywordId)).ToList();

      if (deletePostKeywords.Any())
      {
        _masterContext.PostKeywords.RemoveRange(deletePostKeywords);
      }

      // Add new postkeywords
      var newKeywordIds = keywordIds.Except(existPostKeywords.Select(x => x.KeywordId)).ToList();

      if (newKeywordIds.Any())
      {
        List<PostKeywordEntity> entities = new();
        foreach (var item in keywordIds)
        {
          entities.Add(new PostKeywordEntity
          {
            PostId = postId,
            KeywordId = item
          });
        }

        _masterContext.AddRange(entities);
        _masterContext.SaveChanges();
      }
    }
  }
}
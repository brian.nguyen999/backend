﻿namespace Kempus.Services.Public.PostKeyword
{
  public interface IPostKeywordService
  {
    void Insert(Guid postId, List<string> keywords);
    void Update(Guid postId, List<string> keywords);
  }
}
﻿using Kempus.Models.Common;

namespace Kempus.Services.Public.Notification
{
  public interface IEmailNotificationService
  {
    Task SendOtpAsync(string otp, string emailTo);
    Task SendCmsResetPasswordAsync(string otp, string linkReset, string emailTo);
    Task SendCmsActiveSchoolNotificationAsync(string schoolName, string linkSignUp, List<EmailAddress> emailsTo);
    Task SendSiteResetPasswordAsync(string otp, string linkReset, string emailTo);
  }
}
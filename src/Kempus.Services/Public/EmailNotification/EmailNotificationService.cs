﻿using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Models.Common;
using Kempus.Services.Common.Email;
using Kempus.Services.Public.EmailTemplate;
using Microsoft.Extensions.Logging;

namespace Kempus.Services.Public.Notification
{
  public class EmailNotificationService : IEmailNotificationService
  {
    private readonly IEmailTemplateService _emailTemplateService;
    private readonly IEmailService _emailService;
    private readonly ILogger _logger;

    public EmailNotificationService(
      IEmailTemplateService emailTemplateService,
      IEmailService emailService,
      ILogger<EmailNotificationService> logger)
    {
      _emailTemplateService = emailTemplateService;
      _emailService = emailService;
      _logger = logger;
    }

    public async Task SendOtpAsync(string otp, string emailTo)
    {
      try
      {
        var emailTemplate = await _emailTemplateService.GetByType(CoreConstants.EmailTemplate.SEND_OTP);
        if (emailTemplate == null) return;

        var sendEmailArg = new SendEmailArg()
        {
          ToAddresses = new List<EmailAddress>() { new EmailAddress() { Email = emailTo } },
          Subject = emailTemplate.Title,
          Body = String.Format(emailTemplate.Content, otp),
        };

        await _emailService.SendMailAsync(sendEmailArg);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message);
        throw new BadRequestException(ex.Message);
      }
    }

    public async Task SendCmsResetPasswordAsync(string otp, string linkReset, string emailTo)
    {
      try
      {
        var emailTemplate = await _emailTemplateService.GetByType(CoreConstants.EmailTemplate.CMS_CONFIRM_RESET_PASSWORD);
        if (emailTemplate == null) return;

        var sendEmailArg = new SendEmailArg()
        {
          ToAddresses = new List<EmailAddress>() { new EmailAddress() { Email = emailTo } },
          Subject = emailTemplate.Title,
          Body = String.Format(emailTemplate.Content, otp, linkReset),
        };

        await _emailService.SendMailAsync(sendEmailArg);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message);
        throw new BadRequestException(ex.Message);
      }
    }

    public async Task SendSiteResetPasswordAsync(string otp, string linkReset, string emailTo)
    {
      try
      {
        var emailTemplate = await _emailTemplateService.GetByType(CoreConstants.EmailTemplate.SITE_CONFIRM_RESET_PASSWORD);
        if (emailTemplate == null) return;

        var sendEmailArg = new SendEmailArg()
        {
          ToAddresses = new List<EmailAddress>() { new EmailAddress() { Email = emailTo } },
          Subject = emailTemplate.Title,
          Body = string.Format(emailTemplate.Content, otp, linkReset),
        };

        await _emailService.SendMailAsync(sendEmailArg);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message);
        throw new BadRequestException(ex.Message);
      }
    }

    public async Task SendCmsActiveSchoolNotificationAsync(string schoolName, string linkSignUp, List<EmailAddress> emailsTo)
    {
      try
      {
        var emailTemplate = await _emailTemplateService.GetByType(CoreConstants.EmailTemplate.CMS_SEND_MAIL_WAITLIST);
        if (emailTemplate == null) return;
        if (emailsTo == null) return;

        var sendEmailArg = new SendEmailArg()
        {
          ToAddresses = emailsTo,
          Subject = emailTemplate.Title,
          Body = String.Format(emailTemplate.Content, schoolName, linkSignUp),
        };

        await _emailService.SendMailAsync(sendEmailArg);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message);
        throw new BadRequestException(ex.Message);
      }
    }
  }
}
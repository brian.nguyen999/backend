﻿using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Admin.Instructor;
using Kempus.Models.Public.Instructor;
using Kempus.Models.Public.School;
using Kempus.Services.QueryBuilder;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.EntityFrameworkCore;

namespace Kempus.Services.Public.Instructor
{
  public class InstructorService : IInstructorService
  {
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly MasterDbContext _masterDbContext;

    public InstructorService(
      ReplicaDbContext replicaDbContext,
      MasterDbContext masterDbContext
      )
    {
      _replicaDbContext = replicaDbContext;
      _masterDbContext = masterDbContext;
    }

    public void SetInstructors(IHasInstructorList data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var mapData = dbContext.CourseInstructor
        .Where(x => x.CourseId == data.Id)
        .Join(dbContext.Instructors,
          courseInstructor => courseInstructor.InstructorId,
          instructor => instructor.Guid,
          (courseInstructor, instructor) => new
          {
            CourseId = courseInstructor.CourseId,
            Instructor = new InstructorDetailModel
            {
              Id = instructor.Guid,
              Name = instructor.Name,
            }
          })
        .ToList()
        .ToLookup(x => x.CourseId, x => x.Instructor);

      data.Instructors = mapData
        .Where(q => q.Key == data.Id)
        .SelectMany(o => o)
        .ToList();
    }

    public PagingResult<InstructorListViewModel> Search(InstructorFilterModel filterModel, Pageable pageable, bool useMasterDb)
    {
      if (!filterModel.SchoolId.HasValue)
        return new PagingResult<InstructorListViewModel>(0, pageable.PageIndex, pageable.PageSize);

      var query = BuildQueryable(filterModel, useMasterDb);

      var totalCount = query.LongCount();
      if (totalCount < 1)
        return new PagingResult<InstructorListViewModel>(0, pageable.PageIndex, pageable.PageSize);

      var orderBy = QueryableUtils.GetOrderByFunction<InstructorEntity>(pageable.GetSortables());

      var data = orderBy(query).Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new InstructorListViewModel(x)).ToList();

      BuildResultData(data, useMasterDb);

      return new PagingResult<InstructorListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    public InstructorDetailCmsModel GetDetail(Guid id)
    {
      var query = BuildQueryable(new InstructorFilterModel() { Id = id }, false);
      var totalCount = query.LongCount();
      if (totalCount < 1)
      {
        throw new BadRequestException(ErrorMessages.Instructor_NotFound);
      }

      var data = query.Select(x => new InstructorDetailCmsModel(x)).FirstOrDefault();
      data.CourseNames = _replicaDbContext.CourseInstructor.Where(x => x.InstructorId == data.Id)
        .Join(_replicaDbContext.Courses,
          courseInstructor => courseInstructor.CourseId,
          course => course.Guid,
          (courseInstructor, course) => new { courseInstructor, course })
        .Select(x => x.course.Name).ToList();

      return data;
    }

    #region Private Methods
    private IQueryable<InstructorEntity> BuildQueryable(InstructorFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.Instructors.AsNoTracking();
      query = InstructorQueryBuilder<InstructorEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    private void BuildResultData(List<InstructorListViewModel> data, bool useMasterDb)
    {
      SetNumberOfCourseTaught(data, useMasterDb);
    }

    private void SetNumberOfCourseTaught(List<InstructorListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var instructorIds = data.Select(x => x.Guid).ToList();
      var mapData = dbContext.Instructors.Where(x => instructorIds.Contains(x.Guid))
        .Join(dbContext.CourseInstructor,
          instructor => instructor.Guid,
          courseInstructor => courseInstructor.InstructorId,
        (instructor, courseInstructor) => new { Guid = instructor.Guid, courseInstructor })
        .GroupBy(x => x.Guid,
          (key, data) => new
          {
            Guid = key,
            NumberOfCourseTaught = data.LongCount()
          })
        .ToDictionary(x => x.Guid, x => x.NumberOfCourseTaught);

      data.ForEach(x =>
      {
        x.NumberOfCourseTaught = mapData.GetValueOrDefault(x.Guid);
      });
    }

    #endregion
  }
}
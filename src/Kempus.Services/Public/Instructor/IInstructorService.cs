﻿using Kempus.Core.Models;
using Kempus.Models.Admin.Instructor;
using Kempus.Models.Public.Instructor;

namespace Kempus.Services.Public.Instructor
{
  public interface IInstructorService
  {
    void SetInstructors(IHasInstructorList data, bool useMasterDb);

    PagingResult<InstructorListViewModel> Search(InstructorFilterModel filterModel, Pageable pageable, bool useMasterDb);
    InstructorDetailCmsModel GetDetail(Guid id);
  }
}
﻿using Kempus.Core;
using Kempus.Entities;
using Kempus.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Public.RolePermission
{
  public class RolePermissionService : IRolePermissionService
  {
    private readonly MasterDbContext _masterContext;
    private readonly ReplicaDbContext _replicaContext;
    private readonly SessionStore _sessionStore;

    public RolePermissionService(
      MasterDbContext masterContext,
      ReplicaDbContext replicaContext,
      SessionStore sessionStore
    )
    {
      _masterContext = masterContext;
      _replicaContext = replicaContext;
      _sessionStore = sessionStore;
    }

    public void Insert(long roleId, List<int> permissionIds)
    {
      var validPermissionIds =
        _replicaContext.Permission.Where(x => permissionIds.Contains(x.Id)).Select(x => x.Id).ToList();
      var rolePermission = new List<RolePermissionEntity>();
      foreach (var permissionId in validPermissionIds)
      {
        rolePermission.Add(new RolePermissionEntity
        {
          RoleId = roleId,
          PermissionId = permissionId
        });
      }

      _masterContext.RolePermission.AddRange(rolePermission);
    }

    public void Update(long roleId, List<int> permissionIds)
    {
      List<RolePermissionEntity> existingCustomerTypeDetails =
        _masterContext.RolePermission.Where(x => x.RoleId == roleId).ToList();
      List<RolePermissionEntity> deleteCustomerTypeDetails =
        existingCustomerTypeDetails.FindAll(x => !permissionIds.Contains(x.PermissionId)).ToList();
      _masterContext.RolePermission.RemoveRange(deleteCustomerTypeDetails);

      List<int> existingTypeIds = existingCustomerTypeDetails.Select(x => x.PermissionId).ToList();
      List<int> insertTypeIds = permissionIds.FindAll(x => !existingTypeIds.Contains(x)).ToList();
      Insert(roleId, insertTypeIds);
    }
  }
}
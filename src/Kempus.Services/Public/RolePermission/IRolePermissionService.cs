﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Public.RolePermission
{
  public interface IRolePermissionService
  {
    void Insert(long roleId, List<int> permissionIds);
    void Update(long roleId, List<int> permissionIds);
  }
}
﻿using Kempus.Core.Constants;
using Kempus.Core.Enums;
using Kempus.Core.Errors;
using Kempus.Models.Authentication.Response;
using Kempus.Core.Models.Configuration;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Services.Public.Authentication.JWT;
using Kempus.Services.Public.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Kempus.Core.Utils;
using Kempus.Services.Common.HttpsContext;
using Kempus.Models.Public.Authentication.Request;
using Kempus.Services.Public.User;
using static Kempus.Core.Enums.CoreEnums;
using Microsoft.EntityFrameworkCore;
using Kempus.Services.Public.UserLoginHistory;
using Kempus.Services.Common.KempTransaction;

namespace Kempus.Services.Public.Authentication
{
  public class AuthenticationService : IAuthenticationService
  {
    private const string Dash = "_";
    private readonly JwtConfiguration _jwtConfiguration;
    private readonly MasterDbContext _masterContext;
    private readonly IJwtService _jwtService;
    private readonly IUserPermissionService _userPermissionService;
    private readonly ILogger _logger;
    private readonly IHttpContextService _httpContextService;
    private readonly IUserService _userService;
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly IKempTransactionCommonService _kempTransactionService;

    public AuthenticationService(
      MasterDbContext masterContext,
      IJwtService jwtService,
      JwtConfiguration jwtConfiguration,
      IUserPermissionService userPermissionService,
      ILogger<AuthenticationService> logger,
      IHttpContextService httpContextService,
      IUserService userService,
      ReplicaDbContext replicaDbContext,
      IKempTransactionCommonService kempTransactionService)
    {
      _masterContext = masterContext;
      _jwtService = jwtService;
      _jwtConfiguration = jwtConfiguration;
      _userPermissionService = userPermissionService;
      _logger = logger;
      _httpContextService = httpContextService;
      _userService = userService;
      _replicaDbContext = replicaDbContext;
      _kempTransactionService = kempTransactionService;
    }

    public async Task<AuthenticationResult> GetAuthenticationResultAsync(LoginRequestDto request, List<byte> userType)
    {
      var currentUser = await _userService.GetUserInfoByUsernameAsync(request.Username);
      if (currentUser == null)
      {
        throw new BadRequestException(ErrorMessages.User_NotFound);
      }

      if (!userType.Contains(currentUser.UserType))
      {
        throw new UnAuthorizeException(ErrorMessages.User_RequestDenied);
      }

      return await GetAuthenticationResult(currentUser, null);
    }

    public async Task<AuthenticationResult> RefreshAuthenticationResultAsync(string encryptedRefreshToken)
    {
      var rawRefreshToken = ValidateRefreshToken(encryptedRefreshToken);
      var userId = GetUserIdFromRawRefreshToken(rawRefreshToken);
      var userRefreshToken = await _replicaDbContext.UserRefreshToken.FirstOrDefaultAsync(x =>
        x.UserId == userId &&
        !string.IsNullOrEmpty(x.RefreshToken) && x.RefreshToken.Equals(rawRefreshToken) &&
        x.ExpirationTime > DateTime.UtcNow
      );

      if (userRefreshToken == null) throw new BadRequestException("RefreshToken invalid");

      var currentUser =
        await _replicaDbContext.Users.FirstOrDefaultAsync(x => x.UserId == userId && x.Status == (byte)UserStatus.Active);
      if (currentUser == null)
      {
        throw new BadRequestException(ErrorMessages.User_NotFound);
      }

      return await GetAuthenticationResult(currentUser, userRefreshToken);
    }

    public async Task LogoutAsync(Guid currentUserId, string encodedRefreshToken)
    {
      try
      {
        var rawRefreshToken = ValidateRefreshToken(encodedRefreshToken);
        var userIdFromRefreshToken = GetUserIdFromRawRefreshToken(rawRefreshToken);
        if (currentUserId != userIdFromRefreshToken) return;
        _masterContext.RemoveRange(_masterContext.UserRefreshToken
          .Where(x => x.UserId == currentUserId && !string.IsNullOrEmpty(x.RefreshToken) &&
                      x.RefreshToken.Equals(rawRefreshToken)));
        _masterContext.SaveChanges();
      }
      catch (Exception e)
      {
        _logger.LogInformation("UserId " + currentUserId + " logout failed. " + e.Message);
      }
    }

    private async Task<AuthenticationResult> GetAuthenticationResult(ApplicationUser userEntity,
      UserRefreshTokenEntity userRefreshToken)
    {
      if(userEntity.UserType != (byte)UserType.Admin)
      {
        var currentSchool = _replicaDbContext.Schools.FirstOrDefault(x => x.Guid == userEntity.SchoolId);
        if (currentSchool == null)
        {
          throw new BadRequestException(ErrorMessages.School_NotFound);
        }

        if (!currentSchool.IsActivated)
        {
          throw new BadRequestException(ErrorMessages.User_SchoolInActive);
        }
      }

      var currentDateTime = DateTime.UtcNow;
      var isNewSession = userRefreshToken == null;
      var result = new AuthenticationResult();
      var permissionModel = await _userPermissionService.GetObjectHasPermissionListModel(userEntity.UserId);
      var permissions = permissionModel.PermissionIds;
      var moduleApplicationIds = permissionModel.ModuleApplicationIds;

      // Claims
      var claims = new List<Claim>
      {
        new Claim(CoreConstants.Claims.UserId, userEntity.UserId.ToString()),
        new Claim(CoreConstants.Claims.UserType, userEntity.UserType.ToString()),
        new Claim(CoreConstants.Claims.PermissionIds, JsonUtils.ToJson(permissions)),
        new Claim(CoreConstants.Claims.ModuleApplicationIds, JsonUtils.ToJson(moduleApplicationIds))
      };
      if (userEntity.UserType == (byte)UserType.User || userEntity.UserType == (byte)UserType.Ambassador)
      {
        claims.Add(new Claim(CoreConstants.Claims.SchoolId, userEntity.SchoolId.ToString()));
        if (!string.IsNullOrEmpty(userEntity.ReferralCode))
        {
          claims.Add(new Claim(CoreConstants.Claims.ReferralCode, userEntity.ReferralCode));
        }
      }


      // AccessToken & RefreshToken
      result.AccessToken = _jwtService.GenerateToken(claims);
      var rawRefreshToken = userEntity.UserId + Dash + Guid.NewGuid();
      userRefreshToken ??= new UserRefreshTokenEntity
      {
        UserId = userEntity.UserId
      };
      userRefreshToken.RefreshToken = rawRefreshToken;
      userRefreshToken.ExpirationTime = currentDateTime.AddMinutes(double.Parse(_jwtConfiguration.RefreshTokenValidityInMinutes));

      var currentUserIP = _httpContextService.GetCurrentUserIp();
      userEntity.LastAccessTime = currentDateTime;
      userEntity.LastLoginIP = currentUserIP;
      userRefreshToken.UserIp = currentUserIP;

      bool canInsertTransaction = false;
      using (var transaction = await _masterContext.Database.BeginTransactionAsync())
      {
        try
        {
          var newLoginHistory = new UserLoginHistoryEntity() { UserId = userEntity.UserId, IP = currentUserIP, CreatedDate = currentDateTime };
          var lastLogin = _replicaDbContext.UserLoginHistory
            .Where(x => x.UserId == userEntity.UserId)
            .OrderByDescending(x => x.CreatedDate).FirstOrDefault();
          var lastConsecutiveTransaction = _replicaDbContext.KempTransactions
            .Where(x => x.UserId == userEntity.UserId && x.Type == (byte)KempTransactionType.ConsecutiveLoginFor3Days)
            .OrderByDescending(x => x.CreatedDate).FirstOrDefault();
          if (lastLogin == null)
          {
            userEntity.ConsecutiveLogin++;
          }
          else
          {
            if(lastLogin.CreatedDate.Date < currentDateTime.Date)
            {
              if((currentDateTime.Date - lastLogin.CreatedDate.Date).TotalDays == 1)
              {
                userEntity.ConsecutiveLogin++;
              }
              else
              {
                userEntity.ConsecutiveLogin = 1;
              }
            }
          }

          canInsertTransaction = userEntity.ConsecutiveLogin % 3 == 0
              && (lastConsecutiveTransaction == null || lastConsecutiveTransaction.CreatedDate.Date != newLoginHistory.CreatedDate.Date)
              && userEntity.UserType != (byte)UserType.Admin;

          _masterContext.UserLoginHistory.Add(newLoginHistory);

          _masterContext.Users.Update(userEntity);
          if (isNewSession)
          {
            _masterContext.UserRefreshToken.Add(userRefreshToken);
          }
          else _masterContext.UserRefreshToken.Update(userRefreshToken);

          _masterContext.SaveChanges();
          transaction.Commit();
        }
        catch (Exception ex)
        {
          transaction.Rollback();
          _logger.LogError(ex.Message);
          throw new BadRequestException(ex.Message);
        }
      }

      if (canInsertTransaction)
      {
        // Create ConsecutiveLoginFor3Days transaction. Add 1 Kemp
        _kempTransactionService.CreateTransaction(new Models.Common.KempTransactionInputModel()
        {
          UserIds = new List<Guid>() { userEntity.UserId },
          TransactionType = (byte)KempTransactionType.ConsecutiveLoginFor3Days,
          ReferenceGuid = userEntity.UserId,
          Amount = KempTransactionValue.ConsecutiveLoginFor3Days,
          Action = (byte)KempTransactionAction.Add,
          CreatorId = userEntity.UserId,
          SchoolId = userEntity.SchoolId
        });
      }

      result.RefreshToken = Base64UrlEncoder.Encode(rawRefreshToken);
      return result;
    }

    private string ValidateRefreshToken(string encodedRefreshToken)
    {
      string rawRefreshToken;
      try
      {
        rawRefreshToken = Base64UrlEncoder.Decode(encodedRefreshToken);
        if (string.IsNullOrWhiteSpace(rawRefreshToken)) throw new Exception("Invalid.");
        var userId = GetUserIdFromRawRefreshToken(rawRefreshToken);
        Guid.Parse(rawRefreshToken.Substring(userId.ToString().Length + Dash.Length));
      }
      catch (Exception)
      {
        throw new BadRequestException("RefreshToken invalid");
      }

      return rawRefreshToken;
    }

    private Guid GetUserIdFromRawRefreshToken(string rawRefreshToken)
    {
      return Guid.Parse(rawRefreshToken.Substring(0, rawRefreshToken.IndexOf(Dash, StringComparison.Ordinal)));
    }
  }
}
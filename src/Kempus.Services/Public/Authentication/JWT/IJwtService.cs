﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Public.Authentication.JWT
{
  public interface IJwtService
  {
    string GenerateToken(List<Claim> claims);
  }
}
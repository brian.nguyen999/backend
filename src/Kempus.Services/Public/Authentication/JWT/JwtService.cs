﻿using Kempus.Core.Models.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Public.Authentication.JWT
{
  public class JwtService : IJwtService
  {
    private readonly JwtConfiguration _jwtConfiguration;

    public JwtService(JwtConfiguration jwtConfiguration)
    {
      _jwtConfiguration = jwtConfiguration;
    }

    public string GenerateToken(List<Claim> claims)
    {
      var tokenHandler = new JwtSecurityTokenHandler();
      var tokenDescriptor = new SecurityTokenDescriptor
      {
        Subject = new ClaimsIdentity(claims),
        Expires = DateTime.UtcNow.AddMinutes(double.Parse(_jwtConfiguration.AccessTokenValidityInMinutes)),
        SigningCredentials =
          new SigningCredentials(
            new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_jwtConfiguration.Secret)),
            SecurityAlgorithms.HmacSha256Signature)
      };

      return tokenHandler.WriteToken(tokenHandler.CreateToken(tokenDescriptor));
    }
  }
}
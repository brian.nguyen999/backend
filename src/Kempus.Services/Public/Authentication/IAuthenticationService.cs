﻿using Kempus.Models.Authentication.Response;
using Kempus.Models.Public.Authentication.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Public.Authentication
{
  public interface IAuthenticationService
  {
    Task<AuthenticationResult> GetAuthenticationResultAsync(LoginRequestDto request, List<byte> userType);
    Task<AuthenticationResult> RefreshAuthenticationResultAsync(string encryptedRefreshToken);
    Task LogoutAsync(Guid currentUserId, string encodedRefreshToken);
  }
}
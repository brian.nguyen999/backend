﻿using Kempus.Entities;
using Kempus.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Public.UserLoginHistory
{
  internal class UserLoginHistoryService : IUserLoginHistoryService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;

    public UserLoginHistoryService(MasterDbContext masterDbContext, ReplicaDbContext replicaDbContext)
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
    }

    public bool Create(UserLoginHistoryEntity entity)
    {
      _masterDbContext.UserLoginHistory.Add(entity);
      return _masterDbContext.SaveChanges() > 0;
    }
  }
}

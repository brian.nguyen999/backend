﻿using Kempus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Public.UserLoginHistory
{
  public interface IUserLoginHistoryService
  {
    bool Create(UserLoginHistoryEntity entity);
  }
}

﻿namespace Kempus.Services.Public.Keyword
{
  public interface IKeywordService
  {
    List<Guid> InsertAndGetIds(List<string> keywords);
  }
}
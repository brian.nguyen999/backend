﻿using Kempus.Core;
using Kempus.Core.Models;
using Kempus.Entities;
using Kempus.EntityFramework;

namespace Kempus.Services.Public.Keyword
{
  public class KeywordService : IKeywordService
  {
    private readonly MasterDbContext _masterContext;
    private readonly SessionStore _sessionStore;

    public KeywordService(MasterDbContext masterContext,
      SessionStore sessionStore)
    {
      _masterContext = masterContext;
      _sessionStore = sessionStore;
    }

    public List<Guid> InsertAndGetIds(List<string> keywords)
    {
      List<Guid> result = new();
      var existKeywords = _masterContext.Keywords.Where(x => keywords.Contains(x.Name)).ToList();
      var newKeywords = keywords.Except(existKeywords.Select(x => x.Name).ToList()).ToList();

      // Insert new keywords
      List<Guid> newIds = new();
      foreach (var keyword in newKeywords)
      {
        var item = new KeywordEntity
        {
          Name = keyword,
          NormalizedName = string.Empty,
          SchoolId = _sessionStore.SchoolId
        };
        _masterContext.Keywords.Add(item);
        _masterContext.SaveChanges();

        newIds.Add(item.Guid);
      }

      result.AddRange(existKeywords.Select(x => x.Guid).ToList());
      result.AddRange(newIds);

      return result;
    }
  }
}
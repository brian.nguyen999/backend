﻿using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Topic;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;

namespace Kempus.Services.Public.Topic
{
  public class TopicService : ITopicService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;

    public TopicService(
      MasterDbContext masterContext,
      ReplicaDbContext replicaDbContext
    )
    {
      _masterDbContext = masterContext;
      _replicaDbContext = replicaDbContext;
    }

    public PagingResult<TopicListViewModel> Search(TopicFilterModel model, Pageable pageable, bool useMasterDb)
    {
      var query = BuildQueryable(model, useMasterDb);

      var totalCount = query.LongCount();
      if (totalCount < 1)
      {
        return new PagingResult<TopicListViewModel>(0, pageable.PageIndex, pageable.PageSize);
      }

      var orderBy = QueryableUtils.GetOrderByFunction<TopicEntity>(pageable.GetSortables(nameof(TopicEntity.CreatedDate)));

      var data = orderBy(query).Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new TopicListViewModel(x)).ToList();

      BuildResultData(data, useMasterDb);

      return new PagingResult<TopicListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    public Guid Create(TopicInputModel model)
    {
      ValidateBeforeCreateOrEdit(model);

      var entity = new TopicEntity
      {
        SchoolId = model.SchoolId,
        CreatedBy = model.CreatorId,
        Name = model.Name,
        IsActivated = model.IsActivated
      };

      _masterDbContext.Topics.Add(entity);
      _masterDbContext.SaveChanges();

      return entity.Guid;
    }

    #region

    private void ValidateBeforeCreateOrEdit(TopicInputModel model)
    {
      var school = _replicaDbContext.Schools.FirstOrDefault(x => x.Guid == model.SchoolId);
      if (school == null)
        throw new NotFoundException(ErrorMessages.School_NotFound);

      var topicName = _replicaDbContext.Topics.FirstOrDefault(x => x.Name.ToLower() == model.Name.ToLower());
      if (topicName != null)
        throw new BadRequestException(ErrorMessages.Topic_NameExisted);
    }

    private void BuildResultData(List<TopicListViewModel> data, bool useMasterDb)
    {
      SetNumberOfPost(data, useMasterDb);
    }

    private void SetNumberOfPost(List<TopicListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var topicIds = data.Select(x => x.Id).ToList();
      var mapData = dbContext.Posts.Where(x => topicIds.Contains(x.TopicId.Value)).GroupBy(x => x.TopicId,
        (key, data) => new { TopicId = key, NumberOfPost = data.Count() }).ToDictionary(x => x.TopicId, x => x.NumberOfPost);

      data.ForEach(x =>
      {
        x.NumberOfPost = mapData.GetValueOrDefault(x.Id);
      });
    }

    private IQueryable<TopicEntity> BuildQueryable(TopicFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.Topics.AsNoTracking();
      query = TopicQueryBuilder<TopicEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    #endregion
  }
}
﻿using Kempus.Core.Models;
using Kempus.Models.Public.Topic;

namespace Kempus.Services.Public.Topic
{
  public interface ITopicService
  {
    PagingResult<TopicListViewModel> Search(TopicFilterModel model, Pageable pageable, bool useMasterDb);
    Guid Create(TopicInputModel model);
  }
}
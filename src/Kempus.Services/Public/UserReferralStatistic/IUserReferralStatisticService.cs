﻿using Kempus.Models.Public.UserReferralStatistic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Public.UserReferralStatistic
{
  public interface IUserReferralStatisticService
  {
    void Refresh(UserReferralStatisticRefreshModel model);
    void RefreshByFriendId(Guid friendId);
  }
}

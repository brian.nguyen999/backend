﻿using Kempus.Core.Errors;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.UserReferralStatistic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Public.UserReferralStatistic
{
  public class UserReferralStatisticService : IUserReferralStatisticService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;

    public UserReferralStatisticService(
      MasterDbContext masterDbContext, 
      ReplicaDbContext replicaDbContext)
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
    }

    public void Refresh(UserReferralStatisticRefreshModel model)
    {
      long numberFriendsInvited = 0;
      long numberFirstReviewsWrote = 0;
      var listUserInvited = _replicaDbContext.Users.Where(x => x.InviterReferralCode == model.InviterReferralCode).Select(x => x.UserId).ToList();
      if(listUserInvited != null)
      {
        numberFriendsInvited = listUserInvited.LongCount();
        numberFirstReviewsWrote = _replicaDbContext.CourseReviews.Where(x => listUserInvited.Contains(x.CreatedBy))
          .GroupBy(x => x.CreatedBy).LongCount();
      }
      
      var currentEntity = _replicaDbContext.UserReferralStatistics.FirstOrDefault(x => x.UserId == model.InviterId);
      if (currentEntity == null)
      {
        var newEntity = new UserReferralStatisticEntity
        {
          UserId = model.InviterId,
          NumberFriendsInvited = numberFriendsInvited,
          NumberFirstReviewsWrote = numberFirstReviewsWrote,
          CreatedBy = model.CreatedBy,
        };
        _masterDbContext.UserReferralStatistics.Add(newEntity);
      }
      else
      {
        currentEntity.UpdatedBy = model.CreatedBy;
        currentEntity.NumberFriendsInvited = numberFriendsInvited;
        currentEntity.NumberFirstReviewsWrote = numberFirstReviewsWrote;
        currentEntity.UpdatedDate = DateTime.UtcNow;

        _masterDbContext.UserReferralStatistics.Update(currentEntity);
      }
      _masterDbContext.SaveChanges();
    }

    public void RefreshByFriendId(Guid friendId)
    {
      var friend = _replicaDbContext.Users.Where(x => x.UserId == friendId).FirstOrDefault();
      if (friend != null)
      {
        var inviter = _replicaDbContext.Users.Where(x => x.UserId == friend.InviterGuid).FirstOrDefault();
        if (inviter != null)
        {
          Refresh(new UserReferralStatisticRefreshModel()
          {
            CreatedBy = friendId,
            InviterId = inviter.UserId,
            InviterReferralCode = inviter.ReferralCode
          });
        }
      }
    }
  }
}

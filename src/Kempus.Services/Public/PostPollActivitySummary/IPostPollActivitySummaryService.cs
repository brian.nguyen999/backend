﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Public.PostPollActivitySummary
{
  public interface IPostPollActivitySummaryService
  {
    void CalculatePostPollActivity(Guid schoolId);
  }
}

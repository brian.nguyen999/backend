﻿using Kempus.Core.Constants;
using Kempus.Core.Enums;
using Kempus.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace Kempus.Services.Public.PostPollActivitySummary
{
  public class PostPollActivitySummaryService : IPostPollActivitySummaryService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;

    public PostPollActivitySummaryService(
      MasterDbContext masterDbContext,
      ReplicaDbContext replicaDbContext
    )
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
    }

    public void CalculatePostPollActivity(Guid schoolId)
    {
      var totalPostPoll = _replicaDbContext.Posts.Count(x => x.SchoolId == schoolId);
      if (totalPostPoll < 1) return;

      var pageSize = 500;
      var totalPages = Math.Round((decimal)totalPostPoll / pageSize, 0);
      var last7days = DateTime.UtcNow.AddDays(-7);

      for (int pageIndex = 0; pageIndex <= totalPages; pageIndex++)
      {
        var offset = pageSize * pageIndex;
        var objectEntities = _replicaDbContext.Posts
          .Where(x => x.SchoolId == schoolId)
          .Skip(offset)
          .Take(pageSize)
          .ToList();
        var objectIds = objectEntities.Select(x => x.Guid).ToList();
        var totalLikeActivities = _replicaDbContext.PostPoll7daysActivities
           .Where(x => objectIds.Contains(x.ObjectId)
                       && x.ActionId == (byte)CoreEnums.PostPollActivity.Like
                       && x.CreatedDate >= last7days)
           .GroupBy(x => new { x.ObjectId, x.ObjectType })
           .Select(x => new
           {
             ObjectId = x.Key.ObjectId,
             ObjectType = x.Key.ObjectType,
             TotalLikeIn7days = x.LongCount(q => q.ObjectId == x.Key.ObjectId)
           })
           .ToList();
        var totalCommentActivities = _replicaDbContext.PostPoll7daysActivities
          .Where(x => objectIds.Contains(x.ObjectId)
                      && x.ActionId == (byte)CoreEnums.PostPollActivity.Comment
                      && x.CreatedDate >= last7days)
          .GroupBy(x => new { x.ObjectId, x.ObjectType })
          .Select(x => new
          {
            ObjectId = x.Key.ObjectId,
            ObjectType = x.Key.ObjectType,
            TotalCommentIn7days = x.LongCount(q => q.ObjectId == x.Key.ObjectId)
          })
          .ToList();

        // Remove if need to show, the day which does have any activity
        if (!totalLikeActivities.Any() && !totalCommentActivities.Any()) continue; 

        List<Entities.PostPollActivitySummaryEntity> insertList = new();
        if (objectIds.Any())
        {
          foreach (var objectEntity in objectEntities)
          {
            var likeActivityData = totalLikeActivities.FirstOrDefault(x => x.ObjectId == objectEntity.Guid);
            var commentActivityData = totalCommentActivities.FirstOrDefault(x => x.ObjectId == objectEntity.Guid);
            if (likeActivityData == null && commentActivityData == null) continue;

            insertList.Add(new Entities.PostPollActivitySummaryEntity
            {
              ObjectId = objectEntity.Guid,
              ObjectType = objectEntity.Type,
              TotalLikeIn7days = likeActivityData?.TotalLikeIn7days ?? 0,
              TotalCommentIn7days = commentActivityData?.TotalCommentIn7days ?? 0,
              SchoolId = schoolId,
              CreatedDate = DateTime.UtcNow
            });
          }

          _masterDbContext.AddRange(insertList);
          _masterDbContext.SaveChanges();
        }
      }
    }
  }
}
﻿using Kempus.Models.Public.PollOption;
using Kempus.Models.Public.Post;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Public.PollOptions
{
  public interface IPollOptionService
  {
    void Insert(PollOptionInputModel input);
    void Update(PollOptionUpdateInputModel model);
  }
}
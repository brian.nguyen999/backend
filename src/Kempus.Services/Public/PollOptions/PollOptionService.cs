﻿using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.PollOption;
using Microsoft.EntityFrameworkCore;

namespace Kempus.Services.Public.PollOptions
{
  public class PollOptionService : IPollOptionService
  {
    private readonly MasterDbContext _masterContext;

    public PollOptionService(
      MasterDbContext masterContext
    )
    {
      _masterContext = masterContext;
    }

    public void Insert(PollOptionInputModel input)
    {
      var pollOptions = new List<PollOptionEntity>();
      foreach (var option in input.Options)
      {
        pollOptions.Add(new PollOptionEntity
        {
          PollId = input.PollId,
          Content = option
        });
      }

      _masterContext.PollOptions.AddRange(pollOptions);
      _masterContext.SaveChanges();
    }

    public void Update(PollOptionUpdateInputModel model)
    {
      var pollOptions = _masterContext.PollOptions
        .Where(x => x.PollId == model.PollId)
        .ToList();

      // Sync new options
      var newOptions = model.Options.Where(x => x.Id == null).ToList();
      if (newOptions.Any())
      {
        var newOptionEntities = newOptions.Select(x => new PollOptionEntity
        {
          PollId = model.PollId,
          Content = x.Content
        });
        _masterContext.PollOptions.AddRange(newOptionEntities);
      }

      // Delete options
      var deletedOptions = pollOptions
        .Where(x => !model.Options.Select(x => x.Id).ToList().Contains(x.Guid))
        .ToList();
      if (deletedOptions.Any())
      {
        _masterContext.PollOptions.RemoveRange(deletedOptions);
      }

      // Update options
      var updatedOptions = pollOptions
        .Where(x => model.Options.Select(x => x.Id).ToList().Contains(x.Guid))
        .ToList();
      if (updatedOptions.Any())
      {
        foreach (var option in updatedOptions)
        {
          var newContent = model.Options
            .FirstOrDefault(x => x.Id != null && x.Id.Value == option.Guid);
          if (newContent != null)
          {
            option.Content = newContent.Content;
          }
        }

        _masterContext.PollOptions.UpdateRange(updatedOptions);
      }

      _masterContext.SaveChanges();
    }
  }
}
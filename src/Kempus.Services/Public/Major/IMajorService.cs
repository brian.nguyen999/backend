﻿using Kempus.Core.Models;
using Kempus.Models.Public.Major;

namespace Kempus.Services.Public.Major
{
  public interface IMajorService
  {
    PagingResult<MajorListViewModel> Search(MajorFilterModel filter, Pageable pageable,
      bool useMasterDb);
  }
}
﻿using Kempus.Core.Models;
using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Course;
using Kempus.Models.Public.Major;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;

namespace Kempus.Services.Public.Major
{
  public class MajorService : IMajorService
  {
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly MasterDbContext _masterDbContext;

    public MajorService(
      ReplicaDbContext replicaDbContext, 
      MasterDbContext masterDbContext
      )
    {
      _replicaDbContext = replicaDbContext;
      _masterDbContext = masterDbContext;
    }

    public PagingResult<MajorListViewModel> Search(MajorFilterModel filter, Pageable pageable,
      bool useMasterDb)
    {
      var query = BuildQueryable(filter, useMasterDb);

      var totalCount = query.LongCount();
      if (totalCount < 1)
        return new PagingResult<MajorListViewModel>(0, pageable.PageIndex, pageable.PageSize);

      var orderBy = QueryableUtils.GetOrderByFunction<MajorEntity>(pageable.GetSortables(nameof(MajorEntity.Name)));

      var data = orderBy(query).Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new MajorListViewModel(x)).ToList();


      return new PagingResult<MajorListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    #region Private methods

    private IQueryable<MajorEntity> BuildQueryable(MajorFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.Majors.AsNoTracking();
      query = MajorQueryBuilder<MajorEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    #endregion
  }
}
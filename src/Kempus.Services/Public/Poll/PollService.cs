using Kempus.Core.Constants;
using Kempus.Core.Enums;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Bookmark;
using Kempus.Models.Public.Poll;
using Kempus.Models.Public.Poll.Response;
using Kempus.Models.Public.PollOption;
using Kempus.Models.Public.Post;
using Kempus.Models.Public.PostComments;
using Kempus.Models.Public.PostPollLike;
using Kempus.Models.Public.Tracking;
using Kempus.Models.Public.Vote;
using Kempus.Services.Public.Bookmark;
using Kempus.Services.Public.Comment;
using Kempus.Services.Public.PollOptions;
using Kempus.Services.Public.Post;
using Kempus.Services.Public.PostPollLike;
using Kempus.Services.Public.UserTracking;
using Kempus.Services.Public.Vote;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using static Kempus.Core.Enums.CoreEnums;
using PollOptionModel = Kempus.Models.Public.PollOption.PollOptionDetailModel;

namespace Kempus.Services.Public.Poll
{
  public class PollService : IPollService
  {
    private readonly IConfiguration _configuration;
    private readonly ILogger _logger;
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly IPostService _postService;
    private readonly IPollOptionService _pollOptionService;
    private readonly IBookmarkService _bookmarkService;
    private readonly IVoteService _voteService;
    private readonly IPostPollLikeService _postPollLikeService;
    private readonly IPostCommentService _postCommentService;
    private readonly IUserTrackingService _userTrackingService;

    public PollService(
      IConfiguration configuration,
      MasterDbContext masterContext,
      ReplicaDbContext replicaDbContext,
      IPostService postService,
      IPollOptionService pollOptionService,
      ILogger<PollService> logger,
      IBookmarkService bookmarkService,
      IVoteService voteService,
      IPostPollLikeService postPollLikeService,
      IPostCommentService postCommentService,
      IUserTrackingService userTrackingService
      )
    {
      _configuration = configuration;
      _masterDbContext = masterContext;
      _replicaDbContext = replicaDbContext;
      _postService = postService;
      _pollOptionService = pollOptionService;
      _logger = logger;
      _bookmarkService = bookmarkService;
      _voteService = voteService;
      _postPollLikeService = postPollLikeService;
      _postCommentService = postCommentService;
      _userTrackingService = userTrackingService;
    }

    public PagingResult<PollListViewModel> Search(PollFilterModel filter, Pageable pageable, bool useMasterDb, Guid? currentUserId = null)
    {
      var query = BuildQueryable(filter, useMasterDb);

      var totalCount = query.LongCount();
      if (totalCount < 1)
        return new PagingResult<PollListViewModel>(0, pageable.PageIndex, pageable.PageSize);

      query = BuildOrderClause(query, pageable, useMasterDb);
      var data = query.Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new PollListViewModel(x)).ToList();

      BuildResultData(data, currentUserId, useMasterDb);

      return new PagingResult<PollListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    public Guid Create(PollInputModel model)
    {
      try
      {
        ValidateBeforeInsert(model);

        // Insert post
        var postId = _postService.Create(new PostInputModel
        {
          Title = model.Title,
          Content = model.Content,
          Keywords = model.Keywords,
          Type = (byte)CoreEnums.PostType.Poll,
          IsPinned = model.IsPinned
        });
        _masterDbContext.SaveChanges();

        // Insert poll
        var poll = new PollEntity()
        {
          Type = (int)Enum.Parse<CoreEnums.PollType>(model.Type),
          PostId = postId,
        };

        _masterDbContext.Polls.Add(poll);
        _masterDbContext.SaveChanges();

        // Insert poll options
        _pollOptionService.Insert(new PollOptionInputModel
        {
          PollId = poll.Guid,
          Options = model.Options
        });
        _masterDbContext.SaveChanges();
        return postId;
      }
      catch (Exception)
      {
        throw;
      }
    }

    public void Update(PollUpdateInputModel model)
    {
      try
      {
        ValidateBeforeUpdate(model);

        // Update post
        var post = _postService.Update(new PostInputModel
        {
          Id = model.Id,
          Title = model.Title,
          Content = model.Content,
          Keywords = model.Keywords,
          Type = (byte)CoreEnums.PostType.Poll,
          UpdatedBy = model.UpdatedBy,
          IsPinned = model.IsPinned
        });

        // Update poll
        var poll = _replicaDbContext.Polls.FirstOrDefault(x => x.PostId == post.Id);
        if (poll.Type != (int)Enum.Parse<CoreEnums.PollType>(model.Type))
        {
          poll.Type = (int)Enum.Parse<CoreEnums.PollType>(model.Type);
          _masterDbContext.Polls.Update(poll);
        }

        // Update poll options
        _pollOptionService.Update(new PollOptionUpdateInputModel
        {
          PollId = poll.Guid,
          Options = model.Options
        });

        _masterDbContext.SaveChanges();
      }
      catch (Exception)
      {
        throw;
      }
    }

    public PollDetailModel GetDetail(Guid id, Guid? currentUserId = null)
    {
      var post = _masterDbContext.Posts
        .FirstOrDefault(x => x.Guid == id && x.Type == (byte)PostType.Poll && x.IsPublished);
      if (post == null)
      {
        throw new NotFoundException(ErrorMessages.Poll_NotFound);
      }

      var poll = _replicaDbContext.Polls.FirstOrDefault(x => x.PostId == post.Guid);
      if (poll == null)
      {
        throw new NotFoundException(ErrorMessages.Poll_NotFound);
      }

      var pollOptions = _replicaDbContext.PollOptions.Where(x => x.PollId == poll.Guid).ToList();
      var result = new PollDetailModel
      {
        Id = poll.PostId,
        Title = post.Title,
        Keywords = post.Keywords != null
          ? JsonConvert.DeserializeObject<List<string>>(post.Keywords)
          : null,
        Content = post.Content,
        Type = Enum.GetName(typeof(PollType), poll.Type),
        Options = pollOptions.Select(x => new PollOptionModel
        {
          Id = x.Guid,
          Content = x.Content
        }).ToList(),
        IsPinned = post.IsPinned,
        PollId = poll.Guid,
        PostId = poll.PostId
      };

      // Check editable
      var time = DateTime.UtcNow - post.CreatedDate;
      var limitMinutes = Convert.ToInt32(_configuration.GetSection("LimitedTimeForEditPostPollInMinutes").Value);
      if (time.TotalMinutes > limitMinutes)
      {
        result.IsEditable = false;
      }
      else
      {
        result.IsEditable = true;
        result.RemainSeconds = limitMinutes * 60 - Convert.ToInt32(time.TotalSeconds);
      }

      BuiltResultData(result, currentUserId, false);

      return result;
    }

    public void ValidateBeforeInsert(PollInputModel model)
    {
      if (!model.Options.Any() || model.Options.Count() > 5)
      {
        throw new BadRequestException(ErrorMessages.Poll_OptionInvalid);
      }

      if (!model.Keywords.Any() || model.Keywords.Count() > 3)
      {
        throw new BadRequestException(ErrorMessages.Poll_KeywordsInvalid);
      }
    }

    public void ValidateBeforeUpdate(PollUpdateInputModel model)
    {
      var post = _masterDbContext.Posts.FirstOrDefault(x => x.Guid == model.Id);
      if (post == null)
        throw new NotFoundException(ErrorMessages.Poll_NotFound);

      var limitMinutes = Convert.ToInt32(_configuration.GetSection("LimitedTimeForEditPostPollInMinutes").Value);
      if ((DateTime.UtcNow - post.CreatedDate).TotalMinutes > limitMinutes)
        throw new BadRequestException(string.Format(ErrorMessages.Poll_UnableEdit, limitMinutes));

      if (!model.UpdatedBy.HasValue || model.UpdatedBy.Value != post.CreatedBy)
        throw new BadRequestException(ErrorMessages.Poll_UnableEditOtherPeoplePoll);

      if (!model.Options.Any() || model.Options.Count() > 5)
      {
        throw new BadRequestException(ErrorMessages.Poll_OptionInvalid);
      }

      if (!model.Keywords.Any() || model.Keywords.Count() > 3)
      {
        throw new BadRequestException(ErrorMessages.Poll_KeywordsInvalid);
      }
    }

    public PollViewResponseDto GetViewDetail(Guid id, Guid? currentUserId = null)
    {
      try
      {
        var post = _masterDbContext.Posts
          .FirstOrDefault(x => x.Guid == id && x.Type == (byte)PostType.Poll && x.IsPublished == true);
        if (post == null)
        {
          throw new NotFoundException(ErrorMessages.Poll_NotFound);
        }

        var poll = _replicaDbContext.Polls.FirstOrDefault(x => x.PostId == post.Guid);
        if (poll == null)
        {
          throw new NotFoundException(ErrorMessages.Poll_NotFound);
        }

        var pollOptions = _replicaDbContext.PollOptions.Where(x => x.PollId == poll.Guid).ToList();
        var createdBy = _replicaDbContext.Users.FirstOrDefault(x => x.UserId == post.CreatedBy);

        var result = new PollViewResponseDto
        {
          Id = poll.PostId,
          Title = post.Title,
          Type = Enum.GetName(typeof(PollType), poll.Type),
          Content = post.Content,
          Keywords = JsonUtils.Convert<List<string>>(post.Keywords),
          CreatedDate = post.CreatedDate,
          CreatedBy = createdBy != null ? createdBy.NickName : String.Empty,
          CreatedUserId = createdBy?.UserId,
          Options = pollOptions.Select(x => new PollOptionModel
          {
            Id = x.Guid,
            Content = x.Content
          }).ToList(),
          IsPinned = post.IsPinned,
          PollId = poll.Guid,
          PostId = poll.PostId,
          IsEdited = post.UpdatedDate != null
        };

        BuiltResultData(result, currentUserId, false);
        return result;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message);
        throw new BadRequestException(ex.Message);
      }
    }

    #region Private methods

    private List<Guid> GetCheckedOptions(List<Guid> pollOptionIds, Guid userId)
    {
      return _replicaDbContext.Votes.Where(x => pollOptionIds.Contains(x.PollOptionId))
            .Join(_replicaDbContext.VoteUsers.Where(x => x.CreatedUser == userId),
            vote => vote.VoteUserId,
            voteUser => voteUser.Guid,
            (vote, voteUser) => vote.PollOptionId
            ).ToList();
    }

    private IQueryable<PostEntity> BuildQueryable(PollFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.Posts.AsNoTracking().Where(x => x.Type == (byte)PostType.Poll && x.IsPublished);
      query = PollQueryBuilder<PostEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    public void BuildResultData(List<PollListViewModel> data, Guid? userId, bool useMasterDb)
    {
      SetPolls(data, useMasterDb);
      SetUsers(data, useMasterDb);
      SetPollOptions(data, useMasterDb);
      SetNumberOfVote(data, useMasterDb);
      _postPollLikeService.SetNumberOfLikes(data.ToList<IHasNumberOfLikes>(), useMasterDb);
      _postCommentService.SetNumberOfComments(data.ToList<IHasNumberOfComments>(), useMasterDb);
      _userTrackingService.SetNumberOfViews(data.ToList<IHasNumberOfViews>(), CoreConstants.UserTracking.ObjectType.Poll, false);
      SetPollOptionStatistic(data.SelectMany(x => x.PollOptions).ToList(), useMasterDb);
      if (userId != null)
      {
        _bookmarkService.SetBookmarks(data.ToList<IHasBookmark>(), userId.Value, useMasterDb);
        _voteService.SetVote(data.ToList<IHasVote>(), userId.Value, useMasterDb);
        _postPollLikeService.SetLike(data.ToList<IHasLike>(), userId.Value, useMasterDb);
        SetCheckedPollOptions(data, userId.Value, useMasterDb);
      }
    }

    private void BuiltResultData(PollViewResponseDto data, Guid? currentUserId, bool useMasterDb)
    {
      SetNumberOfVote(data, useMasterDb);
      _postPollLikeService.SetNumberOfLikes(data, useMasterDb);
      _postCommentService.SetNumberOfComments(data, useMasterDb);
      _userTrackingService.SetNumberOfViews(data, CoreConstants.UserTracking.ObjectType.Poll, useMasterDb);
      SetPollOptionStatistic(data.Options.ToList(), useMasterDb);
      if (currentUserId != null)
      {
        _bookmarkService.SetBookmark(data, currentUserId.Value, false);
        _voteService.SetVote(data, currentUserId.Value, false);
        _postPollLikeService.SetLike(data, currentUserId.Value, false);

        var checkPollOptionIds = GetCheckedOptions(data.Options.Select(x => x.Id.Value).ToList(), currentUserId.Value);
        data.Options.ForEach(x =>
        {
          x.IsChecked = checkPollOptionIds.Any(y => y == x.Id);
        });
      }
    }

    private void BuiltResultData(PollDetailModel data, Guid? currentUserId, bool useMasterDb)
    {
      SetNumberOfVote(data, useMasterDb);
      _postPollLikeService.SetNumberOfLikes(data, useMasterDb);
      _postCommentService.SetNumberOfComments(data, false);
      _userTrackingService.SetNumberOfViews(data, CoreConstants.UserTracking.ObjectType.Poll, useMasterDb);
      if (currentUserId != null)
      {
        _bookmarkService.SetBookmark(data, currentUserId.Value, false);
        _voteService.SetVote(data, currentUserId.Value, false);
        _postPollLikeService.SetLike(data, currentUserId.Value, false);

        var checkPollOptionIds = GetCheckedOptions(data.Options.Select(x => x.Id.Value).ToList(), currentUserId.Value);
        data.Options.ForEach(x =>
        {
          x.IsChecked = checkPollOptionIds.Any(y => y == x.Id);
        });
      }
    }

    private void SetNumberOfVote(List<PollListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var pollIds = data.Select(x => x.PollId).ToList();
      var mapData = _replicaDbContext.Statistics.Where(x => x.ObjectType == (byte)StatisticsObjectType.Poll && pollIds.Contains(x.ObjectId)).ToDictionary(x => x.ObjectId, x => x.NumberOfVote);

      data.ForEach(x =>
      {
        x.NumberOfVote = mapData.GetValueOrDefault(x.PollId);
      });
    }

    private void SetNumberOfVote(PollDetailModel data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var pollId = data.PollId;
      var mapData = _replicaDbContext.Statistics
        .FirstOrDefault(x => x.ObjectType == (byte)StatisticsObjectType.Poll && pollId == x.ObjectId);
      data.NumberOfVote = mapData?.NumberOfVote ?? 0;
    }

    private void SetNumberOfVote(PollViewResponseDto data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var pollId = data.PollId;
      var mapData = _replicaDbContext.Statistics
        .FirstOrDefault(x => x.ObjectType == (byte)StatisticsObjectType.Poll && pollId == x.ObjectId);
      data.NumberOfVote = mapData?.NumberOfVote ?? 0;
    }

    private void SetPolls(List<PollListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var postIds = data.Select(x => x.PostId).ToList();
      var mapData = dbContext.Polls.Where(x => postIds.Contains(x.PostId))
        .ToDictionary(x => x.PostId, x => new { x.Guid, x.Type });

      data.ForEach(x =>
      {
        var item = mapData.GetValueOrDefault(x.PostId);
        if (item != null)
        {
          x.Type = Enum.GetName(typeof(PollType), item.Type);
          x.PollId = item.Guid;
        }
      });
    }

    private void SetPollOptions(List<PollListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;

      var postIds = data.Select(x => x.PostId).ToList();

      var mapData = dbContext.Polls.Where(x => postIds.Contains(x.PostId)).Join(dbContext.PollOptions,
        poll => poll.Guid,
        pollOption => pollOption.PollId,
        (poll, pollOption) => new
        {
          Id = poll.PostId,
          PollOptions = new PollOptionModel
          {
            Id = pollOption.Guid,
            PollId = poll.PostId,
            Content = pollOption.Content
          },
        })
        .ToList()
        .ToLookup(x => x.Id, x => x.PollOptions);
      data.ForEach(x => { x.PollOptions = mapData.Where(y => y.Key == x.PostId).SelectMany(o => o).ToList(); });
    }

    private void SetCheckedPollOptions(List<PollListViewModel> data, Guid userId, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;

      List<Guid> pollOptionIds = new();
      foreach (var item in data)
      {
        pollOptionIds.AddRange(item.PollOptions.Select(x => x.Id.Value).ToList());
      }

      var checkedPollOptionIds = GetCheckedOptions(pollOptionIds, userId);

      data.ForEach(x =>
      {
        x.PollOptions.ForEach(y =>
        {
          y.IsChecked = checkedPollOptionIds.Any(k => k == y.Id);
        });
      });
    }

    private void SetUsers(List<PollListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var userIds = data.Select(x => x.CreatedBy).ToList();
      var mapData = dbContext.Users.Where(x => userIds.Contains(x.UserId))
        .ToDictionary(x => x.UserId, x => x.NickName);

      data.ForEach(x =>
      {
        var name = mapData.GetValueOrDefault(x.CreatedBy);
        if (string.IsNullOrWhiteSpace(name)) return;

        x.UserName = name;
      });
    }

    private IQueryable<PostEntity> BuildOrderClause(IQueryable<PostEntity> query, Pageable pageable, bool useMasterDb = false)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      if (pageable.SortedField?.ToLower() == "mostliked")
      {
        query = query
          .GroupJoin(dbContext.PostPollLikes.Where(x => x.ObjectType == (byte)PostType.Poll),
            post => post.Guid,
            like => like.ObjectId,
            (post, like) => new { post, like }
          )
          .SelectMany(x => x.like.DefaultIfEmpty(),
            (post, like) => new { post.post, like })
          .GroupBy(x => x.post.Guid)
          .Select(x => new
          {
            Id = x.Key,
            NumberOfLikes = x.Count(x => x.like != null)
          })
          .Join(dbContext.Posts.Where(x => x.Type == (byte)PostType.Poll),
            postGrp => postGrp.Id,
            post => post.Guid,
            (postGrp, post) => new { post, postGrp })
          .OrderByDescending(x => x.post.IsPinned)
          .ThenByDescending(x => x.post.PinnedDate)
          .ThenByDescending(x => x.postGrp.NumberOfLikes)
          .ThenByDescending(x => x.post.CreatedDate)
          .Select(x => x.post);
      }
      else if (pageable.SortedField?.ToLower() == "mostviewed")
      {
        query = query
          .GroupJoin(dbContext.ViewTrackings.Where(x => x.ObjectType == CoreConstants.UserTracking.ObjectType.Poll),
            post => post.Guid,
            view => view.ObjectId,
            (post, view) => new { post, view }
          )
          .SelectMany(x => x.view.DefaultIfEmpty(),
            (post, view) => new { post.post, view })
          .GroupBy(x => x.post.Guid)
          .Select(x => new
          {
            Id = x.Key,
            NumberOfViews = x.Count(x => x.view != null)
          })
          .Join(dbContext.Posts.Where(x => x.Type == (byte)PostType.Poll),
            postGrp => postGrp.Id,
            post => post.Guid,
            (postGrp, post) => new { post, postGrp }
          )
          .OrderByDescending(x => x.post.IsPinned)
          .ThenByDescending(x => x.post.PinnedDate)
          .ThenByDescending(x => x.postGrp.NumberOfViews)
          .ThenByDescending(x => x.post.CreatedDate)
          .Select(x => x.post);
      }
      else
      {
        return query.OrderByDescending(x => x.IsPinned)
          .ThenByDescending(x => x.PinnedDate)
          .ThenByDescending(x => x.CreatedDate);
      }

      return query;
    }

    private void SetPollOptionStatistic(List<PollOptionDetailModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var optionIds = data.Select(x => x.Id).ToList();
      var pollIds = data.GroupBy(x => x.PollId).Select(x => x.Key).ToList();
      var mapData = dbContext.Votes.Where(x => optionIds.Contains(x.PollOptionId))
        .GroupBy(x => x.PollOptionId)
        .Select(x => new
        {
          PollOptionId = x.Key,
          NumberOfVote = x.LongCount()
        })
        .ToDictionary(x => x.PollOptionId, x => x.NumberOfVote);
      if (!mapData.Any()) return;

      foreach (var pollId in pollIds)
      {
        var pollOptions = data.Where(x => x.PollId == pollId).ToList();
        var pollOptionIds = pollOptions.Select(x => x.Id).ToList();
        if (!pollOptions.Any()) continue;
        var totalVotes = mapData.Where(x => pollOptionIds.Contains(x.Key)).Select(x => x.Value).Sum();
        foreach (var pollOption in pollOptions)
        {
          pollOption.NumberOfVote = mapData.GetValueOrDefault(pollOption.Id.Value);
          pollOption.PercentageOfVote = (pollOption.NumberOfVote * 1.0 / totalVotes) * 100;
        }
      }
    }

    #endregion
  }
}
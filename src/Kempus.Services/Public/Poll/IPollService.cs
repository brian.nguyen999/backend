﻿using Kempus.Core.Models;
using Kempus.Models.Public.Poll;
using Kempus.Models.Public.Poll.Response;

namespace Kempus.Services.Public.Poll
{
  public interface IPollService
  {
    PagingResult<PollListViewModel> Search(PollFilterModel filter, Pageable pageable, bool useMasterDb,
      Guid? currentUserId = null);
    Guid Create(PollInputModel model);
    void Update(PollUpdateInputModel model);
    PollDetailModel GetDetail(Guid id, Guid? currentUserId);
    PollViewResponseDto GetViewDetail(Guid id, Guid? currentUserId);
    public void BuildResultData(List<PollListViewModel> data, Guid? userId, bool useMasterDb);
  }
}
using Kempus.Core;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Role;
using Kempus.Services.Public.RolePermission;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;

namespace Kempus.Services.Admin.Role
{
  public class RoleService : IRoleService
  {
    private readonly MasterDbContext _masterContext;
    private readonly ReplicaDbContext _replicaContext;
    private readonly SessionStore _sessionStore;
    private readonly IRolePermissionService _rolePermissionService;

    public RoleService(
      MasterDbContext masterContext,
      ReplicaDbContext replicaContext,
      SessionStore sessionStore,
      IRolePermissionService rolePermissionService)
    {
      _masterContext = masterContext;
      _replicaContext = replicaContext;
      _sessionStore = sessionStore;
      _rolePermissionService = rolePermissionService;
    }

    public PagingResult<RoleListViewModel> Search(RoleFilterModel filter, Pageable pageable, bool useMasterDb)
    {
      var query = BuildQueryable(filter, useMasterDb);
      var totalCount = query.LongCount();
      if (totalCount < 1)
      {
        return new PagingResult<RoleListViewModel>(0, pageable.PageIndex, pageable.PageSize);
      }

      var orderBy = QueryableUtils.GetOrderByFunction<RoleEntity>(pageable.GetSortables(nameof(RoleEntity.CreatedDate)));
      var data = orderBy(query).Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new RoleListViewModel(x)).ToList();
      BuildResultData(data, useMasterDb);
      return new PagingResult<RoleListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    public RoleDetailModel GetDetail(Guid id)
    {
      var role = _replicaContext
        .Role.Where(x => x.Guid == id)
        .Join(_replicaContext.RolePermission,
          role => role.Id,
          rolePermission => rolePermission.RoleId,
          (role, rolePermission) => new
          {
            Id = role.Guid,
            Name = role.Name,
            IsActive = role.IsActive,
            PermissionId = rolePermission.PermissionId
          }).ToList().GroupBy(x => new { x.Id, x.Name, x.IsActive }, x => x.PermissionId,
          (key, g) => new { RoleData = key, PermissionIds = g.ToList() })
        .FirstOrDefault();

      if (role == null) throw new NotFoundException(ErrorMessages.Role_Notfound);

      return new RoleDetailModel
      {
        Id = role.RoleData.Id,
        Name = role.RoleData.Name,
        IsActive = role.RoleData.IsActive,
        PermissionIds = role.PermissionIds
      };
    }

    public Guid Create(RoleInputModel model)
    {
      var isExitedRole = _masterContext.Role.Any(x => x.Name == model.Name);
      if (isExitedRole)
        throw new BadRequestException(ErrorMessages.Role_Existed);

      var role = new RoleEntity
      {
        Name = model.Name,
        IsActive = model.IsActive,
        CreatedBy = _sessionStore.UserId ?? Guid.Empty,
        CreatedDate = DateTime.UtcNow
      };

      using var transaction = _masterContext.Database.BeginTransaction();
      try
      {
        _masterContext.Role.Add(role);
        _masterContext.SaveChanges();

        _rolePermissionService.Insert(role.Id, model.PermissionIds);
        _masterContext.SaveChanges();
        transaction.Commit();
      }
      catch (Exception)
      {
        transaction.Rollback();
        throw;
      }

      return role.Guid;
    }

    public void Update(RoleUpdateInputModel model)
    {
      var existingRole =
        _replicaContext.Role.FirstOrDefault(x => x.Name == model.Name);
      if (existingRole != null && existingRole.Guid != model.Id)
      {
        throw new BadRequestException(ErrorMessages.Role_Existed);
      }

      var entity = existingRole ?? (_replicaContext.Role.FirstOrDefault(x => x.Guid == model.Id));
      if (entity == null) throw new BadRequestException(ErrorMessages.Role_Notfound);
      entity.Name = model.Name;
      entity.IsActive = model.IsActive;
      entity.UpdatedBy = _sessionStore.UserId ?? Guid.Empty;
      entity.UpdatedDate = DateTime.Now;

      using var transaction = _masterContext.Database.BeginTransaction();
      try
      {
        _masterContext.Role.Update(entity);
        _masterContext.SaveChanges();

        _rolePermissionService.Update(entity.Id, model.PermissionIds);
        _masterContext.SaveChanges();
        transaction.Commit();
      }
      catch (Exception)
      {
        transaction.Rollback();
        throw;
      }
    }

    #region private methods

    private void BuildResultData(List<RoleListViewModel> data, bool useMasterDb)
    {
      SetPermission(data, useMasterDb);
    }

    private void SetPermission(List<RoleListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterContext : _replicaContext;
      var roleIds = data.Select(x => x.Id).ToList();
      var mapData = dbContext.Role.Where(x => roleIds.Contains(x.Id))
        .Join(dbContext.RolePermission,
          role => role.Id,
          rolePermission => rolePermission.RoleId,
          (role, rolePermission) => new
          {
            Guid = role.Guid,
            PermissionIds = rolePermission.PermissionId
          }).ToList().ToLookup(x => x.Guid, x => x.PermissionIds);

      data.ForEach(x => { x.PermissionIds = mapData.Where(y => y.Key == x.Guid).SelectMany(o => o).ToList(); });
    }

    private IQueryable<RoleEntity> BuildQueryable(RoleFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterContext : _replicaContext;
      var query = dbContext.Role.AsNoTracking();
      query = RoleQueryBuilder<RoleEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    #endregion
  }
}
﻿using Kempus.Core.Models;
using Kempus.Models.Public.Role;

namespace Kempus.Services.Admin.Role
{
  public interface IRoleService
  {
    PagingResult<RoleListViewModel> Search(RoleFilterModel filter, Pageable pageable, bool useMasterDb);
    RoleDetailModel GetDetail(Guid id);
    Guid Create(RoleInputModel model);
    void Update(RoleUpdateInputModel model);
  }
}
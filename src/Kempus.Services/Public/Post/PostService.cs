using Kempus.Core;
using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Bookmark;
using Kempus.Models.Public.Post;
using Kempus.Models.Public.Post.Response;
using Kempus.Models.Public.PostComments;
using Kempus.Models.Public.PostPollLike;
using Kempus.Models.Public.Topic;
using Kempus.Models.Public.Tracking;
using Kempus.Services.Public.Bookmark;
using Kempus.Services.Public.Comment;
using Kempus.Services.Public.PostKeyword;
using Kempus.Services.Public.PostPollLike;
using Kempus.Services.Public.Topic;
using Kempus.Services.Public.UserTracking;
using Kempus.Services.QueryBuilder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OpenSearch.Client;
using static Kempus.Core.Constants.CoreConstants;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.Public.Post
{
  public class PostService : IPostService
  {
    private IConfiguration _configuration;
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly SessionStore _sessionStore;
    private readonly IPostKeywordService _postKeywordService;
    private readonly ITopicService _topicService;
    private readonly IBookmarkService _bookmarkService;
    private readonly IPostPollLikeService _postPollLikeService;
    private readonly IPostCommentService _postCommentService;
    private readonly IUserTrackingService _userTrackingService;

    public PostService(
      IConfiguration configuration,
      MasterDbContext masterContext,
      ReplicaDbContext replicaDbContext,
      SessionStore sessionStore,
      IPostKeywordService postKeywordService,
      ITopicService topicService,
      IBookmarkService bookmarkService,
      IPostPollLikeService postPollLikeService,
      IPostCommentService postCommentService,
      IUserTrackingService userTrackingService
      )
    {
      _configuration = configuration;
      _masterDbContext = masterContext;
      _replicaDbContext = replicaDbContext;
      _postKeywordService = postKeywordService;
      _sessionStore = sessionStore;
      _topicService = topicService;
      _bookmarkService = bookmarkService;
      _postPollLikeService = postPollLikeService;
      _postCommentService = postCommentService;
      _userTrackingService = userTrackingService;
    }

    public PagingResult<PostListViewModel> Search(PostFilterModel filter, Pageable pageable, bool useMasterDb, Guid? currentUserId = null)
    {
      var query = BuildQueryable(filter, useMasterDb);

      var totalCount = query.LongCount();
      if (totalCount < 1)
        return new PagingResult<PostListViewModel>(0, pageable.PageIndex, pageable.PageSize);

      query = BuildOrderClause(query, pageable, useMasterDb);
      var data = query.Skip(pageable.Offset).Take(pageable.PageSize)
        .Select(x => new PostListViewModel(x)).ToList();

      BuildResultData(data, currentUserId, useMasterDb);

      return new PagingResult<PostListViewModel>(data, totalCount, pageable.PageIndex, pageable.PageSize);
    }

    public PostDetailModel GetDetail(Guid id, Guid? currentUserId = null)
    {
      var post = _replicaDbContext.Posts.FirstOrDefault(x => x.Guid == id && x.IsPublished == true);
      if (post == null)
        throw new NotFoundException(ErrorMessages.Post_NotFound);
      var result = new PostDetailModel
      {
        Id = post.Guid,
        PostId = post.Guid,
        Title = post.Title,
        Content = post.Content,
        Keywords = JsonUtils.Convert<List<string>>(post.Keywords),
        TopicId = post.TopicId
      };

      // Check editable
      var time = DateTime.UtcNow - post.CreatedDate;
      var limitMinutes = Convert.ToInt32(_configuration.GetSection("LimitedTimeForEditPostPollInMinutes").Value);
      if (time.TotalMinutes > limitMinutes)
      {
        result.IsEditable = false;
      }
      else
      {
        result.IsEditable = true;
        result.RemainSeconds = limitMinutes * 60 - Convert.ToInt32(time.TotalSeconds);
      }

      _postPollLikeService.SetNumberOfLikes(result, false);
      _postCommentService.SetNumberOfComments(result, false);
      _userTrackingService.SetNumberOfViews(result, CoreConstants.UserTracking.ObjectType.Post, false);
      if (currentUserId != null)
      {
        _bookmarkService.SetBookmark(result, currentUserId.Value, false);
        _postPollLikeService.SetLike(result, currentUserId.Value, false);
      }

      return result;
    }

    public Guid Create(PostInputModel model)
    {
      using var transaction = _masterDbContext.Database.BeginTransaction();
      try
      {
        ValidateBeforeInsertOrUpdate(model);
        var entity = new PostEntity
        {
          Title = model.Title,
          Keywords = model.Keywords != null && model.Keywords.Any()
            ? JsonConvert.SerializeObject(model.Keywords)
            : JsonData.EmptyArray,
          TopicId = model.TopicId,
          Content = model.Content,
          Type = model.Type != null ? (byte)model.Type : (byte)PostType.Post,
          SchoolId = _sessionStore.SchoolId,
          CreatedBy = _sessionStore.UserId ?? Guid.Empty
        };

        _masterDbContext.Posts.Add(entity);
        _masterDbContext.SaveChanges();

        if (model.Keywords != null && model.Keywords.Any())
        {
          _postKeywordService.Insert(entity.Guid, model.Keywords);
        }

        _masterDbContext.SaveChanges();
        transaction.Commit();

        return entity.Guid;
      }
      catch (Exception)
      {
        transaction.Rollback();
        throw;
      }
    }

    public PostResponseDto Update(PostInputModel model)
    {
      using var transaction = _masterDbContext.Database.BeginTransaction();
      try
      {
        ValidateBeforeInsertOrUpdate(model);
        var post = _masterDbContext.Posts.FirstOrDefault(x => x.Guid == model.Id);
        post.Title = model.Title;
        post.Keywords = model.Keywords != null && model.Keywords.Any()
          ? JsonConvert.SerializeObject(model.Keywords)
          : JsonData.EmptyArray;
        post.TopicId = model.TopicId;
        post.Content = model.Content;
        post.Type = model.Type != null ? (byte)model.Type : (byte)PostType.Post;
        post.SchoolId = _sessionStore.SchoolId;
        post.UpdatedBy = model.UpdatedBy.HasValue ? model.UpdatedBy.Value : Guid.Empty;
        post.UpdatedDate = DateTime.UtcNow;
        var result = _masterDbContext.Posts.Update(post);
        _masterDbContext.SaveChanges();

        if (model.Keywords != null && model.Keywords.Any())
        {
          _postKeywordService.Update(post.Guid, model.Keywords);
        }

        transaction.Commit();

        return MapData(post);
      }
      catch (Exception)
      {
        transaction.Rollback();
        throw;
      }
    }

    public void ValidateBeforeInsertOrUpdate(PostInputModel model)
    {
      if (model.Id != null)
      {
        var post = _replicaDbContext.Posts.FirstOrDefault(x => x.Guid == model.Id);
        if (post == null)
          throw new NotFoundException(ErrorMessages.Post_NotFound);

        var limitMinutes = Convert.ToInt32(_configuration.GetSection("LimitedTimeForEditPostPollInMinutes").Value);
        if ((DateTime.UtcNow - post.CreatedDate).TotalMinutes > limitMinutes)
          throw new BadRequestException(string.Format(ErrorMessages.Post_UnableEdit, limitMinutes));

        if (!model.UpdatedBy.HasValue || model.UpdatedBy.Value != post.CreatedBy)
          throw new BadRequestException(ErrorMessages.Post_UnableEditOtherPeoplePost);
      }

      if (model.TopicId != null)
      {
        var result = _topicService.Search(new TopicFilterModel { TopicId = model.TopicId }, new Pageable(), true);
        if (result.TotalCount == 0)
          throw new NotFoundException(ErrorMessages.Topic_NotFound);
      }

      if (!model.Keywords.Any() || model.Keywords.Count() > 3)
      {
        throw new BadRequestException(ErrorMessages.Post_KeywordsInvalid);
      }
    }

    public PostViewResponseDto GetViewDetail(Guid id, Guid? currentUserId = null)
    {
      var result = (
          from post in _masterDbContext.Posts.AsNoTracking()
          join users in _masterDbContext.Users.AsNoTracking() on post.CreatedBy equals users.UserId into post_user_group
          from post_user in post_user_group.DefaultIfEmpty()
          join topics in _masterDbContext.Topics.AsNoTracking() on post.TopicId equals topics.Guid into post_topic_group
          from post_topic in post_topic_group.DefaultIfEmpty()
          where post.Guid == id && post.IsPublished
          select new PostViewResponseDto
          {
            Id = post.Guid,
            PostId = post.Guid,
            Title = post.Title,
            Content = post.Content,
            Keywords = JsonUtils.Convert<List<string>>(post.Keywords),
            Topic = post_topic != null ? post_topic.Name : "All Topics",
            CreatedDate = post.CreatedDate,
            CreatedBy = post_user != null ? post_user.NickName : string.Empty,
            CreatedUserId = post_user.UserId,
            IsEdited = post.UpdatedDate != null
          })
        .FirstOrDefault();

      if (result == null)
        throw new NotFoundException(ErrorMessages.Post_NotFound);

      _postPollLikeService.SetNumberOfLikes(result, false);
      _postCommentService.SetNumberOfComments(result, false);
      _userTrackingService.SetNumberOfViews(result, CoreConstants.UserTracking.ObjectType.Post, false);
      if (currentUserId != null)
      {
        _bookmarkService.SetBookmark(result, currentUserId.Value, false);
        _postPollLikeService.SetLike(result, currentUserId.Value, false);
      }

      return result;
    }

    #region Private methods

    private void BuildResultData(List<PostListViewModel> data, Guid? userId, bool useMasterDb)
    {
      SetTopic(data, useMasterDb);
      SetUser(data, useMasterDb);
      _postPollLikeService.SetNumberOfLikes(data.ToList<IHasNumberOfLikes>(), useMasterDb);
      _postCommentService.SetNumberOfComments(data.ToList<IHasNumberOfComments>(), useMasterDb);
      _userTrackingService.SetNumberOfViews(data.ToList<IHasNumberOfViews>(), CoreConstants.UserTracking.ObjectType.Post, false);
      if (userId != null)
      {
        _bookmarkService.SetBookmarks(data.ToList<IHasBookmark>(), userId.Value, useMasterDb);
        _postPollLikeService.SetLike(data.ToList<IHasLike>(), userId.Value, useMasterDb);
      }
    }

    private IQueryable<PostEntity> BuildQueryable(PostFilterModel model, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var query = dbContext.Posts.AsNoTracking().Where(x => x.Type == (byte)PostType.Post && x.IsPublished);
      query = PostQueryBuilder<PostEntity>.BuildWhereClause(model, query, dbContext);
      return query;
    }

    private void SetTopic(List<PostListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var postIds = data.Select(x => x.Id).ToList();
      var mapData = dbContext.Posts
        .Join(dbContext.Topics,
        post => post.TopicId,
        topic => topic.Guid,
        (post, topic) => new { post, topic })
        .Where(x => postIds.Contains(x.post.Guid))
        .ToDictionary(x => x.post.Guid, x => x.topic?.Name);

      data.ForEach(x =>
      {
        var item = mapData.GetValueOrDefault(x.Id);
        if (string.IsNullOrWhiteSpace(item)) return;

        x.Topic = item;
      });
    }

    private void SetUser(List<PostListViewModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var userIds = data.Select(x => x.CreatedBy).ToList();
      var mapData = dbContext.Users.Where(x => userIds.Contains(x.UserId))
        .ToDictionary(x => x.UserId, x => x.NickName);

      data.ForEach(x =>
      {
        var name = mapData.GetValueOrDefault(x.CreatedBy);
        if (string.IsNullOrWhiteSpace(name)) return;

        x.UserName = name;
      });
    }

    private PostResponseDto MapData(PostEntity entity)
    {
      return new PostResponseDto
      {
        Id = entity.Guid,
        Title = entity.Title,
        Content = entity.Content,
        Keywords = entity.Keywords,
        Type = ((PostType)entity.Type).ToString(),
        SchoolId = entity.SchoolId,
        TopicId = entity.TopicId
      };
    }

    private IQueryable<PostEntity> BuildOrderClause(IQueryable<PostEntity> query, Pageable pageable, bool useMasterDb = false)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      if (pageable.SortedField?.ToLower() == "mostliked")
      {
        query = query
          .GroupJoin(dbContext.PostPollLikes.Where(x => x.ObjectType == (byte)PostType.Post),
            post => post.Guid,
            like => like.ObjectId,
            (post, like) => new { post, like }
          )
          .SelectMany(x => x.like.DefaultIfEmpty(),
            (post, like) => new { post.post, like })
          .GroupBy(x => x.post.Guid)
          .Select(x => new
          {
            Id = x.Key,
            NumberOfLikes = x.Count(x => x.like != null)
          })
          .Join(dbContext.Posts.Where(x => x.Type == (byte)PostType.Post),
            postGrp => postGrp.Id,
            post => post.Guid,
            (postGrp, post) => new { post, postGrp })
          .OrderByDescending(x => x.post.IsPinned)
          .ThenByDescending(x => x.post.PinnedDate)
          .ThenByDescending(x => x.postGrp.NumberOfLikes)
          .ThenByDescending(x => x.post.CreatedDate)
          .Select(x => x.post);
      }
      else if (pageable.SortedField?.ToLower() == "mostviewed")
      {
        query = query
          .GroupJoin(dbContext.ViewTrackings.Where(x => x.ObjectType == CoreConstants.UserTracking.ObjectType.Post),
            post => post.Guid,
            view => view.ObjectId,
            (post, view) => new { post, view }
          )
          .SelectMany(x => x.view.DefaultIfEmpty(),
            (post, view) => new { post.post, view })
          .GroupBy(x => x.post.Guid)
          .Select(x => new
          {
            Id = x.Key,
            NumberOfViews = x.Count(x => x.view != null)
          })
          .Join(dbContext.Posts.Where(x => x.Type == (byte)PostType.Post),
            postGrp => postGrp.Id,
            post => post.Guid,
            (postGrp, post) => new { post, postGrp }
          )
          .OrderByDescending(x => x.post.IsPinned)
          .ThenByDescending(x => x.post.PinnedDate)
          .ThenByDescending(x => x.postGrp.NumberOfViews)
          .ThenByDescending(x => x.post.CreatedDate)
          .Select(x => x.post);
      }
      else
      {
        return query.OrderByDescending(x => x.IsPinned)
          .ThenByDescending(x => x.PinnedDate)
          .ThenByDescending(x => x.CreatedDate);
      }

      return query;
    }

    #endregion
  }
}
﻿using Kempus.Core.Models;
using Kempus.Models.Public.Post;
using Kempus.Models.Public.Post.Response;

namespace Kempus.Services.Public.Post
{
  public interface IPostService
  {
    PagingResult<PostListViewModel> Search(PostFilterModel filter, Pageable pageable, bool useMasterDb, Guid? currentUserId = null);
    PostDetailModel GetDetail(Guid id, Guid? currentUserId = null);
    Guid Create(PostInputModel dto);
    PostResponseDto Update(PostInputModel model);
    PostViewResponseDto GetViewDetail(Guid id, Guid? currentUserId = null);
  }
}
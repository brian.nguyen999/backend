﻿using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using StackExchange.Redis;
using System.Text;

namespace Kempus.Services.Common.Redis
{
  public class RedisService : IRedisService
  {
    private readonly IDistributedCache _cache;
    private readonly IConfiguration _configuration;

    public RedisService(
      IDistributedCache cache,
      IConfiguration configuration)
    {
      _cache = cache;
      _configuration = configuration;
    }

    public async Task<bool> AddCacheAsync(string cacheKey, string cacheValue, DateTime dateExpired)
    {
      if (string.IsNullOrEmpty(cacheKey) || string.IsNullOrEmpty(cacheValue) || dateExpired <= DateTime.UtcNow)
      {
        return false;
      }

      // Serializing the data
      var dataToCache = Encoding.UTF8.GetBytes(cacheValue);

      // Setting up the cache options
      DistributedCacheEntryOptions options = new DistributedCacheEntryOptions()
        .SetAbsoluteExpiration(dateExpired);

      // Add the data into the cache
      await _cache.SetAsync(cacheKey, dataToCache, options);

      return true;
    }

    public async Task<string> GetValueByKeyAsync(string cacheKey)
    {
      if (string.IsNullOrEmpty(cacheKey))
      {
        return String.Empty;
      }

      var cacheValueAsByte = await _cache.GetAsync(cacheKey);
      if (cacheValueAsByte != null)
      {
        return Encoding.UTF8.GetString(cacheValueAsByte);
      }

      return String.Empty;
    }

    public async Task RemoveCacheAsync(string cacheKey)
    {
      await _cache.RemoveAsync(cacheKey);
    }

    public async Task<bool> CheckKeyExist(string cacheKey)
    {
      var cacheValue = await _cache.GetAsync(cacheKey);
      if (cacheValue != null)
      {
        return true;
      }

      return false;
    }

  }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Common.Redis
{
  public interface IRedisService
  {
    Task<bool> AddCacheAsync(string cacheKey, string cacheValue, DateTime dateExpired);
    Task<string> GetValueByKeyAsync(string cacheKey);
    Task RemoveCacheAsync(string cacheKey);
    Task<bool> CheckKeyExist(string cacheKey);
  }
}
﻿using Kempus.Models.Common.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Common.WebSocket
{
  public interface IWebSocketService
  {
    Task<bool> AddConnectionAsync(ConnectionInputModel input);
    Task SendNotification(NotificationInputModel input);
    Task RemoveConnectionAsync(ConnectionInputModel model);
    Task<List<string>> GetConnectionIdsBySchoolIdAsync(Guid schoolId);
    Task<List<string>> GetConnectionIdsByUserIdAsync(Guid userId);
    Task SendMessage(IList<string> connectionIds, string action, object payload);
  }
}

﻿using Amazon.ApiGatewayManagementApi;
using Amazon.ApiGatewayManagementApi.Model;
using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Core.Models.Configuration;
using Kempus.Core.Utils;
using Kempus.EntityFramework;
using Kempus.Integration.Aws.ApiGateway;
using Kempus.Models.Common.WebSocket;
using Kempus.Services.Common.Redis;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;
using static Microsoft.Extensions.Logging.EventSource.LoggingEventSource;
using static System.Collections.Specialized.BitVector32;

namespace Kempus.Services.Common.WebSocket
{
  public class WebSocketService : IWebSocketService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly ILogger _logger;
    private readonly IAmazonApiGatewayService _amazonApiGatewayService;
    private readonly IRedisService _redisService;
    private readonly IConnectionMultiplexer _redis;
    private readonly RedisConfig _redisConfig;

    public WebSocketService(
      MasterDbContext masterDbContext,
      ReplicaDbContext replicaDbContext,
      ILogger<WebSocketService> logger,
      IAmazonApiGatewayService amazonApiGatewayService,
      IRedisService redisService,
      AwsConfig awsConfig,
      IConnectionMultiplexer redis)
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
      _logger = logger;
      _amazonApiGatewayService = amazonApiGatewayService;
      _redisService = redisService;
      _redisConfig = awsConfig.Redis;
      _redis = redis;
    }

    public async Task<bool> AddConnectionAsync(ConnectionInputModel input)
    {
      try
      {
        _logger.LogInformation($"Add Connection Id: {input.ConnectionId}");
        var cacheKey = BuildCacheKey(input.SchoolId.GetValueOrDefault(), input.UserId.GetValueOrDefault(), input.ConnectionId);
        var cacheValue = JsonUtils.ToJson(input);

        if (await _redisService.CheckKeyExist(cacheKey))
        {
          await RenewCache(input.ConnectionId);
        }
        else
        {
          await _redisService.AddCacheAsync(cacheKey, cacheValue, DateTime.UtcNow.AddMinutes(10));
        }

        return true;
      }
      catch(Exception ex)
      {
        _logger.LogError(ex.Message);
        throw new BadRequestException(ex.Message);
      }
    }

    public async Task RemoveConnectionAsync(ConnectionInputModel input)
    {
      _logger.LogInformation($"Remove Connection Id: {input.ConnectionId}");
      var server = _redis.GetServer(_redisConfig.Endpoint, _redisConfig.Port);
      var queueKeys = server.Keys(pattern: $"*{input.ConnectionId}*");
      if(queueKeys.Any())
      {
        foreach(var cacheKey in queueKeys)
        {
          await _redisService.RemoveCacheAsync((string)cacheKey);
        }
      }
    }

    public async Task<List<string>> GetConnectionIdsBySchoolIdAsync(Guid schoolId)
    {
      var server = _redis.GetServer(_redisConfig.Endpoint, _redisConfig.Port);
      var queueKeys = server.Keys(pattern: $"*{schoolId}*");
      var listConnectionIds = new List<string> ();
      if(queueKeys.Any())
      {
        foreach (var key in queueKeys)
        {
          var splitString = ((string)key).Split(RedisCacheKeyDelimeter.Delimeter);
          listConnectionIds.Add(splitString[2]);
        }
      }
      return listConnectionIds;
    }

    public async Task<List<string>> GetConnectionIdsByUserIdAsync(Guid userId)
    {
      var server = _redis.GetServer(_redisConfig.Endpoint, _redisConfig.Port);
      var queueKeys = server.Keys(pattern: $"*{userId}*");
      var listConnectionIds = new List<string>();
      if (queueKeys.Any())
      {
        foreach (var key in queueKeys)
        {
          var splitString = ((string)key).Split(RedisCacheKeyDelimeter.Delimeter);
          listConnectionIds.Add(splitString[2]);
        }
      }
      return listConnectionIds;
    }

    public async Task SendNotification(NotificationInputModel input)
    {
      foreach(var connectionId in input.ConnectionIds)
      {
        await _amazonApiGatewayService.PostToConnection(connectionId, input.Action, input.Payload);
        await RenewCache(connectionId);
      }
    }

    private async Task RenewCache(string connectionId)
    {
      var server = _redis.GetServer(_redisConfig.Endpoint, _redisConfig.Port);
      var queueKeys = server.Keys(pattern: $"*{connectionId}*");
      if (queueKeys.Any())
      {
        foreach (var key in queueKeys)
        {
          var value = await _redisService.GetValueByKeyAsync((string)key);
          await _redisService.RemoveCacheAsync((string)key);
          await _redisService.AddCacheAsync((string)key, value, DateTime.UtcNow.AddMinutes(10));
        }
      }
    }

    public async Task SendMessage(IList<string> connectionIds, string action, object payload)
    {
      foreach (var connectionId in connectionIds)
      {
        await _amazonApiGatewayService.PostToConnection(connectionId, action, payload);
        await RenewCache(connectionId);
      }
    }

    private string BuildCacheKey(Guid schoolId, Guid userId, string connectionId)
    {
      return $"{schoolId}{RedisCacheKeyDelimeter.Delimeter}{userId}{RedisCacheKeyDelimeter.Delimeter}{connectionId}";
    }
  }
}

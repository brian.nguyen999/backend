﻿using Kempus.Integration.Aws.S3;
using Kempus.Models.Common;
using Microsoft.AspNetCore.Http;

namespace Kempus.Services.Common.Upload
{
  public class AwsS3IntegrationHelper
  {
    private readonly IS3Client _s3Client;

    public AwsS3IntegrationHelper(
        IS3Client s3Client
    )
    {
      _s3Client = s3Client;
    }

    public async Task<UploadFileResponse> UploadFileAsync(IFormFile fileData, string bucket, string objectKey,
        string fileName, bool isPublic)
    {
      try
      {
        if (fileName == null)
          fileName = DateTime.UtcNow.ToString("yyyy-MM-ddTHHmmssfffff") +
                     Path.GetExtension(fileData.FileName);

        using var stream = new MemoryStream();
        await fileData.CopyToAsync(stream);
        objectKey = objectKey + fileName;
        var s3Response =
            await _s3Client.PutObjectToS3(stream, bucket, objectKey, fileData.ContentType, isPublic);

        if (s3Response is { HttpStatusCode: System.Net.HttpStatusCode.OK })
        {
          var fileS3Url = _s3Client.GetAbsoluteLink(bucket, objectKey);
          return new UploadFileResponse()
          {
            ContentType = fileData.ContentType,
            FileName = fileName,
            Size = fileData.Length,
            S3Url = fileS3Url
          };
        }

        return null;
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    public async Task<UploadFileResponse> UploadFileAsync(string filePath, string contentType, long contentLength,
        string bucket, string objectKey, string fileName)
    {
      try
      {
        var uploadFileResult = new UploadFileResponse()
        {
          ContentType = contentType,
          FileName = fileName,
          Size = contentLength
        };

        objectKey = objectKey + fileName;
        var s3Response =
            await _s3Client.PutObjectToS3(filePath, objectKey, contentType);

        if (s3Response != null && s3Response.HttpStatusCode == System.Net.HttpStatusCode.OK)
        {
          uploadFileResult.S3Url = _s3Client.GetAbsoluteLink(bucket, objectKey);
        }

        return uploadFileResult;
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    public async Task<string> UploadFileToS3Async(string filePath, string objectKey)
    {
      return await _s3Client.UploadFileToS3Async(filePath, objectKey);
    }

    public async Task<long> GetSpaceUsageAsync(string bucket, string objectKey)
    {
      return await _s3Client.GetObjectSize(bucket, objectKey);
    }

    public async Task<List<string>> GetFilesFromFolder(string folderPath)
    {
      return await _s3Client.GetFilesFromFolder(folderPath);
    }

    public async Task DeleteObjectAsync(string bucket, string objectKey)
    {
      await _s3Client.DeleteObjects(bucket, objectKey);
    }

    public async Task DeleteObjectsAsync(string bucket, IEnumerable<string> objectKey)
    {
      await _s3Client.DeleteObjects(bucket, objectKey);
    }

    public async Task DeleteFileByLinks(params string[] fileLink)
    {
      await _s3Client.DeleteObjects(fileLink);
    }
  }
}

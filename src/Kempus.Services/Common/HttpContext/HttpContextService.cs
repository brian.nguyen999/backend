﻿using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Core.Utils;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Common.HttpsContext
{
  public class HttpContextService : IHttpContextService
  {
    private readonly IHttpContextAccessor _httpContextAccessor;
    private Guid? _currentUserId;
    private Guid? _schoolId;
    private string? _currentUserIP;
    private HashSet<int> _permissionIds;
    private HashSet<int> _moduleApplicationIds;

    public HttpContextService(IHttpContextAccessor httpContextAccessor)
    {
      _httpContextAccessor = httpContextAccessor;
    }

    public Guid GetCurrentUserId()
    {
      if (_currentUserId != null) return _currentUserId.Value;
      try
      {
        _currentUserId =
          Guid.Parse(_httpContextAccessor.HttpContext.User.Claims
            .FirstOrDefault(x => x.Type == CoreConstants.Claims.UserId)?.Value ?? string.Empty);
        return _currentUserId.Value;
      }
      catch (Exception)
      {
        throw new UnAuthorizeException("Token invalid, please re-login");
      }
    }

    public Guid GetCurrentSchoolId()
    {
      if (_schoolId != null) return _schoolId.Value;
      try
      {
        _schoolId =
          Guid.Parse(_httpContextAccessor.HttpContext.User.Claims
            .FirstOrDefault(x => x.Type == CoreConstants.Claims.SchoolId)?.Value ?? string.Empty);
        return _schoolId.Value;
      }
      catch (Exception)
      {
        throw new UnAuthorizeException("Token invalid, please re-login");
      }
    }

    public string? GetCurrentUserIp()
    {
      if (_currentUserIP != null) return _currentUserIP;
      _currentUserIP = _httpContextAccessor.HttpContext?.Connection.RemoteIpAddress?.ToString();
      return _currentUserIP;
    }

    public bool HasPermission(int permissionId)
    {
      return HasPermissions(new int[] { permissionId });
    }

    public bool HasPermissions(params int[] permissionIds)
    {
      if (_permissionIds == null) GetPermissions();
      return permissionIds.Any(permissionId => _permissionIds.Contains(permissionId));
    }

    public HashSet<int> GetPermissions()
    {
      if (_permissionIds != null) return _permissionIds;
      var permissionClaim = _httpContextAccessor.HttpContext.User.Claims
        .FirstOrDefault(x => x.Type == CoreConstants.Claims.PermissionIds);
      if (permissionClaim != null) _permissionIds = JsonUtils.Convert<HashSet<int>>(permissionClaim.Value);
      _permissionIds ??= new HashSet<int>();
      return _permissionIds;
    }
  }
}
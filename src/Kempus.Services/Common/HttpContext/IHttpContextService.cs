﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Common.HttpsContext
{
  public interface IHttpContextService
  {
    Guid GetCurrentUserId();
    Guid GetCurrentSchoolId();
    string? GetCurrentUserIp();
    bool HasPermission(int permissionId);
    bool HasPermissions(params int[] permissionId);
    HashSet<int> GetPermissions();
  }
}
﻿using Kempus.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Common.Email
{
  public interface IEmailService
  {
    Task SendMailAsync(SendEmailArg emailInput);
  }
}
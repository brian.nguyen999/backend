﻿using Kempus.Integration.Aws.SES;
using Kempus.Models.Common;
using Microsoft.Extensions.Logging;

namespace Kempus.Services.Common.Email
{
  public class EmailService : IEmailService
  {
    private readonly ISESService _sesService;
    private readonly ILogger _logger;

    public EmailService(
      ISESService sesService,
      ILogger<EmailService> logger
    )
    {
      _sesService = sesService;
      _logger = logger;
    }

    public async Task SendMailAsync(SendEmailArg emailInput)
    {
      try
      {
        _logger.LogInformation("Execute send email");

        if (emailInput.ToAddresses == null || emailInput.ToAddresses.Count == 0)
        {
          _logger.LogInformation("Skipped send email due to no recipient in argument");
          return;
        }

        foreach (var toAddress in emailInput.ToAddresses)
        {
          await _sesService.SendMailAsync(toAddress.Email, toAddress.Name, emailInput.Subject, emailInput.Body);
        }

        _logger.LogInformation("End send email");
      }
      catch (Exception ex)
      {
        _logger.LogError("Error when send email job run: " + ex.Message);
      }
    }
  }
}
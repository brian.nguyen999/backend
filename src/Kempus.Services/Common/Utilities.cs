﻿using System.Text;
using System.Security.Cryptography;

namespace Kempus.Services.Common
{
  public class Utilities
  {
    public static class StringHelper
    {
      public static string RandomNumber(int min, int max)
      {
        Random random = new Random();
        return random.Next(min, max).ToString();
      }

      public static string RandomString(int length = 5)
      {
        string valid = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder sb = new StringBuilder();
        //using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
        //{
        //    byte[] uintBuffer = new byte[sizeof(uint)];

        //    while (length-- > 0)
        //    {
        //        rng.GetBytes(uintBuffer);
        //        uint num = BitConverter.ToUInt32(uintBuffer, 0);
        //        sb.Append(valid[(int)(num % (uint)valid.Length)]);
        //    }
        //}
        using (var random = RandomNumberGenerator.Create())
        {
          byte[] uintBuffer = new byte[sizeof(uint)];
          while (length-- > 0)
          {
            random.GetNonZeroBytes(uintBuffer);
            uint num = BitConverter.ToUInt32(uintBuffer, 0);
            sb.Append(valid[(int)(num % (uint)valid.Length)]);
          }
        }

        return sb.ToString();
      }
    }
  }
}
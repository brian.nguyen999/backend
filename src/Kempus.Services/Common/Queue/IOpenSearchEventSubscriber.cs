﻿using Amazon.SQS.Model;

namespace Kempus.Services.Common.Queue
{
  public interface IOpenSearchEventSubscriber
  {
    Task<ReceiveMessageResponse> GetMessageAsync(int numberOfMessage, int waitTime);
    Task DeleteMessageAsync(Message message);
  }
}

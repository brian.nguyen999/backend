﻿using Amazon.SQS.Model;
using Kempus.Core.Models.Configuration;
using Kempus.Integration.Aws.SQS;

namespace Kempus.Services.Common.Queue
{
  public class OpenSearchEventSubscriber : IOpenSearchEventSubscriber
  {
    private readonly SqsConfig _config;
    private readonly ISqsService _sqsService;
    public OpenSearchEventSubscriber(
      AwsConfig awsConfig,
      ISqsService sqsService
      )
    {
      _config = awsConfig.Sqs;
      _sqsService = sqsService;
    }

    public async Task<ReceiveMessageResponse> GetMessageAsync(int numberOfMessage, int waitTime)
    {
      return await _sqsService.GetMessagesAsync(_config.Queues.OpenSearchDataSyncQueueUrl, numberOfMessage, waitTime);
    }

    public async Task DeleteMessageAsync(Message message)
    {
      await _sqsService.DeleteMessageAsync(_config.Queues.OpenSearchDataSyncQueueUrl, message);
    }
  }
}

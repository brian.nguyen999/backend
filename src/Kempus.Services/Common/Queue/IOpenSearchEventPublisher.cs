﻿namespace Kempus.Services.Common.Queue
{
  public interface IOpenSearchEventPublisher
  {
    Task PublishMessageAsync(string eventType, List<Guid> objectIds);
  }
}

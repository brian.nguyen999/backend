﻿using Kempus.Core.Models.Configuration;
using Kempus.Integration.Aws.SQS;
using Kempus.Models.Common;

namespace Kempus.Services.Common.Queue
{
  public class OpenSearchEventPublisher : IOpenSearchEventPublisher
  {
    private readonly SqsConfig _config;
    private readonly ISqsService _sqsService;
    public OpenSearchEventPublisher(
      AwsConfig awsConfig,
      ISqsService sqsService
      )
    {
      _sqsService = sqsService;
      _config = awsConfig.Sqs;
    }

    public async Task PublishMessageAsync(string eventType, List<Guid> objectIds)
    {
      var eventMessage = new OpenSearchEventMessage
      {
        EventTime = DateTime.UtcNow,
        EventType = eventType,
        ObjectIds = objectIds
      };

      await _sqsService.SendMessageAsync(_config.Queues.OpenSearchDataSyncQueueUrl, eventMessage);
    }
  }
}
 
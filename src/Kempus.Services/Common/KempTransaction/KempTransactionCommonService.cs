﻿using Kempus.Core.Enums;
using Kempus.Core.Errors;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Common;

namespace Kempus.Services.Common.KempTransaction
{
  public class KempTransactionCommonService : IKempTransactionCommonService
  {
    private readonly MasterDbContext _masterDbContext;
    private readonly ReplicaDbContext _replicaDbContext;

    public KempTransactionCommonService(
      MasterDbContext masterDbContext,
      ReplicaDbContext replicaDbContext
    )
    {
      _masterDbContext = masterDbContext;
      _replicaDbContext = replicaDbContext;
    }

    public bool CreateTransaction(KempTransactionInputModel input)
    {
      var currentDate = DateTime.UtcNow;
      if (input.IsSelectAllUser)
      {
        input.UserIds = _replicaDbContext.Users
          .Where(x => x.SchoolId == input.SchoolId && x.Status == (byte)CoreEnums.UserStatus.Active)
          .Select(x => x.UserId)
          .ToList();
      }

      var lstNewTransactionEntity = input.UserIds.Select(x => new KempTransactionEntity()
      {
        Type = input.TransactionType,
        ReferenceGuid = input.ReferenceGuid,
        UserId = x,
        Amount = input.Amount,
        Action = input.Action,
        CreatedBy = input.CreatorId,
        CreatedDate = currentDate,
        IsAdminCreated = input.IsAdminCreated,
        SchoolId = input.SchoolId
      }).ToList();

      var lstWalletEntity = _replicaDbContext.KempWallets
        .Where(x => input.UserIds.Contains(x.UserId))
        .ToList();

      lstWalletEntity.ForEach(x =>
      {
        x.Amount = x.Amount + input.Amount < 0 ? 0 : x.Amount + input.Amount;
        x.UpdatedBy = input.CreatorId;
        x.UpdatedDate = currentDate;
      });

      int result;
      using var transaction = _masterDbContext.Database.BeginTransaction();
      try
      {
        _masterDbContext.ChangeTracker.Clear();
        _masterDbContext.KempWallets.UpdateRange(lstWalletEntity);
        _masterDbContext.KempTransactions.AddRange(lstNewTransactionEntity);
        result = _masterDbContext.SaveChanges();
        transaction.Commit();
      }
      catch (Exception)
      {
        transaction.Rollback();
        throw;
      }

      return result > 0;
    }

    public Guid CreateWallet(Guid userId, Guid creatorId)
    {
      if (_replicaDbContext.KempWallets.Any(x => x.UserId == userId))
        return userId;

      var amount = _replicaDbContext.KempTransactions.Where(x => x.UserId == userId).Sum(x => x.Amount);

      var newWalletEntity = new KempWalletEntity()
      {
        UserId = userId,
        Amount = amount,
        CreatedBy = creatorId,
        CreatedDate = DateTime.UtcNow
      };

      _masterDbContext.KempWallets.Add(newWalletEntity);
      _masterDbContext.SaveChanges();

      return newWalletEntity.Guid;
    }
  }
}

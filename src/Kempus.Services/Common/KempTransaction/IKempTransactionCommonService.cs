﻿using Kempus.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.Common.KempTransaction
{
  public interface IKempTransactionCommonService
  {
    bool CreateTransaction(KempTransactionInputModel input);
    Guid CreateWallet(Guid UserId, Guid creatorId);
  }
}

﻿using Kempus.Core.Models;
using Kempus.Models.FullTextSearch.CourseReview;

namespace Kempus.Services.FullTextSearch.CourseReview
{
  public interface ICourseReviewFtsService
  {
    Task IndexDocumentAsync(List<CourseReviewFtsModel> input);
    Task DeleteDocumentAsync(Guid id);
    Task<PagingResult<CourseReviewFtsModel>> SearchDocumentAsync(CourseReviewFtsFilterModel filter, Pageable pageable);
    List<CourseReviewFtsModel> BuildDataModel(List<Guid> ids);
    Task DeleteIndex(string indexName);
  }
}

﻿using Kempus.Models.FullTextSearch.CourseReview;
using Kempus.Models.FullTextSearch.Instructor;
using OpenSearch.Client;

namespace Kempus.Services.FullTextSearch.CourseReview
{
  public static class Mapping
  {
    public static CreateIndexDescriptor CourseReviewFtsModelMapping(this CreateIndexDescriptor descriptor)
    {
      return descriptor
        .Map<CourseReviewFtsModel>(m => m
          // Flatten object mapping
          .Properties(p => p
            .Keyword(t => t.Name(n => n.Id))
            .Keyword(t => t.Name(n => n.CourseId))
            .Text(t => t.Name(n => n.CourseName))
            .Date(t => t.Name(n => n.CreatedDate))
            .Keyword(t => t.Name(n => n.SchoolId))
            .Keyword(t => t.Name(n => n.DepartmentId))
            .Text(t => t.Name(n => n.DepartmentName))
            .Keyword(t => t.Name(n => n.Status))
            // Nested object mapping
            .Object<InstructorFtsModel>(o => o
              .Name(n => n.Instructors)
              .Properties(sp => sp
                .Keyword(z => z.Name(e => e.Id))
                .Text(z => z.Name(e => e.Name)))
            )
          )
        );
    }
  }
}

﻿using Kempus.Core.Constants;
using Kempus.Core.Models;
using Kempus.EntityFramework;
using Kempus.Integration.Aws.OpenSearch;
using Kempus.Models.FullTextSearch.CourseReview;
using Kempus.Models.FullTextSearch.Instructor;
using OpenSearch.Client;

namespace Kempus.Services.FullTextSearch.CourseReview
{
  public class CourseReviewFtsService : ICourseReviewFtsService
  {
    private readonly IFullTextSearchService<CourseReviewFtsModel> _openSearchService;
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly MasterDbContext _masterDbContext;

    public CourseReviewFtsService(
      IFullTextSearchService<CourseReviewFtsModel> openSearchService,
      ReplicaDbContext replicaDbContext,
      MasterDbContext masterDbContext
    )
    {
      _openSearchService = openSearchService;
      _replicaDbContext = replicaDbContext;
      _masterDbContext = masterDbContext;
    }


    public async Task IndexDocumentAsync(List<CourseReviewFtsModel> input)
    {
      await EnsureIndexCreatedAsync();
      await _openSearchService.IndexDocumentAsync(input, CoreConstants.OpenSearchIndex.CourseReviews);
    }

    public async Task<PagingResult<CourseReviewFtsModel>> SearchDocumentAsync(CourseReviewFtsFilterModel filter, Pageable pageable)
    {
      var filterQuery = BuildFilterConditions(filter);
      var totalCount = await _openSearchService.CountDocumentAsync(BuildCountQuery(filterQuery));
      if (totalCount < 1)
      {
        return new PagingResult<CourseReviewFtsModel>(0, pageable.PageIndex, pageable.PageSize);
      }

      var result = await _openSearchService.SearchDocumentAsync(BuildSearchQuery(filterQuery, pageable));
      return new PagingResult<CourseReviewFtsModel>(result, totalCount, pageable.PageIndex, pageable.PageSize); ;
    }

    public async Task DeleteDocumentAsync(Guid id)
    {
      await _openSearchService.DeleteDocumentAsync(id, x => x.Index(CoreConstants.OpenSearchIndex.CourseReviews));
    }

    public List<CourseReviewFtsModel> BuildDataModel(List<Guid> ids)
    {
      var courses = _replicaDbContext.CourseReviews
        .Select(x => new CourseReviewFtsModel
        {
          Id = x.Guid,
          CourseId = x.CourseId,
          SchoolId = x.SchoolId.Value,
          CreatedDate = x.CreatedDate,
          Status = x.Status
        })
        .Where(x => ids.Contains(x.Id))
        .ToList();
      if (!courses.Any()) return new List<CourseReviewFtsModel>();

      SetCourses(courses, false);
      SetInstructors(courses, false);
      SetDepartments(courses, false);
      return courses;
    }

    public async Task DeleteIndex(string indexName)
    {
      await _openSearchService.DeleteIndexAsync(indexName);
    }

    #region Private methods

    private async Task EnsureIndexCreatedAsync()
    {
      await _openSearchService.EnsureIndexCreatedAsync(
          CoreConstants.OpenSearchIndex.CourseReviews,
          indice => indice.CourseReviewFtsModelMapping());
    }

    private Func<SearchDescriptor<CourseReviewFtsModel>, ISearchRequest> BuildSearchQuery
      (Func<QueryContainerDescriptor<CourseReviewFtsModel>, QueryContainer> filter, Pageable pageable)
    {
      Func<SearchDescriptor<CourseReviewFtsModel>, ISearchRequest> query = s =>
        s.Index(CoreConstants.OpenSearchIndex.CourseReviews)
          .Query(filter)
          .Sort(BuildSortQuery(pageable))
          .Skip(pageable.Offset)
          .Take(pageable.PageSize);
      return query;
    }

    private Func<SortDescriptor<CourseReviewFtsModel>, IPromise<IList<ISort>>> BuildSortQuery(Pageable pageable)
    {
      Func<SortDescriptor<CourseReviewFtsModel>, IPromise<IList<ISort>>> sort = pageable.SortedField switch
      {
        "name" => pageable.IsDescending ? x => x.Descending("name") : x => x.Ascending("name"),
        _ => x => x.Descending("_score")
      };
      return sort;
    }

    private Func<CountDescriptor<CourseReviewFtsModel>, ICountRequest> BuildCountQuery
      (Func<QueryContainerDescriptor<CourseReviewFtsModel>, QueryContainer> filter)
    {
      Func<CountDescriptor<CourseReviewFtsModel>, ICountRequest> query = s =>
        s.Index(CoreConstants.OpenSearchIndex.CourseReviews)
          .Query(filter);
      return query;
    }

    private Func<QueryContainerDescriptor<CourseReviewFtsModel>, QueryContainer> BuildFilterConditions(CourseReviewFtsFilterModel input)
    {
      Func<QueryContainerDescriptor<CourseReviewFtsModel>, QueryContainer> filter = null;
      filter = s => s.Bool(p => p.Must(m =>
        m.Match(q =>
          q.Field(fld => fld.SchoolId).Query(input.SchoolId.ToString())) &&
        m.Match(q =>
          q.Field(fld => fld.Status).Query(input.Status.ToString())) &&
        m.QueryString(m => m
          .Fields(x => x
            .Field(fld => fld.CourseName)
            .Field(fld => fld.DepartmentName)
            .Field(fld => fld.Instructors.First().Name)
          )
          .Query($"*{input.Keyword}*")
        )));

      return filter;
    }

    private void SetInstructors(List<CourseReviewFtsModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var courseIds = data.Select(x => x.CourseId).ToList();
      var mapData = dbContext.CourseInstructor
        .Where(x => courseIds.Contains(x.CourseId))
        .Join(dbContext.Instructors,
          courseInstructor => courseInstructor.InstructorId,
          instructor => instructor.Guid,
          (courseInstructor, instructor) => new
          {
            CourseId = courseInstructor.CourseId,
            Instructor = new InstructorFtsModel()
            {
              Id = instructor.Guid,
              Name = instructor.Name,
            }
          })
        .ToList()
        .ToLookup(x => x.CourseId, x => x.Instructor);

      if (!mapData.Any()) return;

      foreach (var item in data)
      {
        item.Instructors = mapData
          .Where(q => q.Key == item.Id)
          .SelectMany(o => o)
          .ToList();
      }
    }

    private void SetDepartments(List<CourseReviewFtsModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var departmentProgramIds = data.Select(x => x.DepartmentId).ToList();
      var mapData = dbContext.DepartmentPrograms
        .Where(x => departmentProgramIds.Contains(x.Guid))
        .ToDictionary(x => x.Guid, x => x.Name);

      if (!mapData.Any()) return;

      foreach (var item in data)
      {
        item.DepartmentName = item.DepartmentId != null
          ? mapData.GetValueOrDefault(item.DepartmentId.Value)
          : null;
      }
    }

    private void SetCourses(List<CourseReviewFtsModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var courseIds = data.Select(x => x.CourseId).ToList();
      var mapData = dbContext.Courses
        .Where(x => courseIds.Contains(x.Guid))
        .ToList();

      if (!mapData.Any()) return;

      foreach (var item in data)
      {
        var course = mapData.FirstOrDefault(x => x.Guid == item.CourseId);
        if (course != null)
        {
          item.CourseName = course.Name;
          item.DepartmentId = course.DepartmentId;
        }
      }
    }

    #endregion
  }
}

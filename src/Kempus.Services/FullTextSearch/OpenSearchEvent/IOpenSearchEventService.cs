﻿using Kempus.Models.Common;

namespace Kempus.Services.FullTextSearch.OpenSearchEvent
{
  public interface IOpenSearchEventService
  {
    Task ProcessEvent(OpenSearchEventMessage eventMessage);
  }
}

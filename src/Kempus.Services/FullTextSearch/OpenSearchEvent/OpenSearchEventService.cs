﻿using Kempus.Core.Constants;
using Kempus.Models.Common;
using Kempus.Services.FullTextSearch.Course;
using Kempus.Services.FullTextSearch.CourseReview;
using Microsoft.Extensions.Logging;

namespace Kempus.Services.FullTextSearch.OpenSearchEvent
{
  public class OpenSearchEventService : IOpenSearchEventService
  {
    private readonly ICourseFtsService _courseFtsService;
    private readonly ICourseReviewFtsService _courseReviewFtsService;
    private readonly ILogger<OpenSearchEventService> _logger;

    public OpenSearchEventService(
      ILogger<OpenSearchEventService> logger,
      ICourseFtsService courseFtsService,
      ICourseReviewFtsService courseReviewFtsService)
    {
      _courseFtsService = courseFtsService;
      _logger = logger;
      _courseReviewFtsService = courseReviewFtsService;
    }

    public async Task ProcessEvent(OpenSearchEventMessage eventMessage)
    {
      switch (eventMessage.EventType)
      {
        case CoreConstants.OpenSearchEventType.Course.Update:
        case CoreConstants.OpenSearchEventType.Course.Create:
          {
            await HandleCourseCreatedOrUpdated(eventMessage);
            break;
          }
        case CoreConstants.OpenSearchEventType.CourseReview.Update:
        case CoreConstants.OpenSearchEventType.CourseReview.Create:
          {
            await HandleCourseReviewCreatedOrUpdated(eventMessage);
            break;
          }
        case CoreConstants.OpenSearchEventType.Course.Delete:
          {
            await HandleCourseDeleteData(eventMessage);
            break;
          }
      }
    }

    #region Course

    private async Task HandleCourseCreatedOrUpdated(OpenSearchEventMessage eventMessage)
    {
      _logger.LogInformation("Start Handle Create/Update Course");
      if (!eventMessage.ObjectIds.Any())
      {
        _logger.LogInformation("Skip sync Course to OpenSearch due to no objectIds");
      }

      var totalObjects = eventMessage.ObjectIds.Count();
      var pageSize = 100;
      var totalPages = Math.Round((decimal)totalObjects / pageSize, 0);
      for (int pageIndex = 0; pageIndex <= totalPages; pageIndex++)
      {
        _logger.LogInformation("Build data model of page ({0}/{1})", pageIndex, totalPages);
        var offset = pageSize * pageIndex;
        var objectIds = eventMessage.ObjectIds.Skip(offset).Take(pageSize).ToList();
        var courseFtsData = _courseFtsService.BuildDataModel(objectIds);
        _logger.LogInformation("Total models to sync = {0}", courseFtsData.Count);
        if (courseFtsData.Any())
        {
          await _courseFtsService.IndexDocumentAsync(courseFtsData);
        }
      }

      _logger.LogInformation("End Handle Create/Update Course");
    }

    private async Task HandleCourseReviewCreatedOrUpdated(OpenSearchEventMessage eventMessage)
    {
      _logger.LogInformation("Start Handle Create/Update CourseReview");
      if (!eventMessage.ObjectIds.Any())
      {
        _logger.LogInformation("Skip sync Course to OpenSearch due to no objectIds");
      }

      var totalObjects = eventMessage.ObjectIds.Count();
      var pageSize = 100;
      var totalPages = Math.Round((decimal)totalObjects / pageSize, 0);
      for (int pageIndex = 0; pageIndex <= totalPages; pageIndex++)
      {
        _logger.LogInformation("Build data model of page ({0}/{1})", pageIndex, totalPages);
        var offset = pageSize * pageIndex;
        var objectIds = eventMessage.ObjectIds.Skip(offset).Take(pageSize).ToList();
        var courseFtsData = _courseReviewFtsService.BuildDataModel(objectIds);
        _logger.LogInformation("Total models to sync = {0}", courseFtsData.Count);
        if (courseFtsData.Any())
        {
          await _courseReviewFtsService.IndexDocumentAsync(courseFtsData);
        }
      }

      _logger.LogInformation("End Handle Create/Update CourseReview");
    }

    private async Task HandleCourseDeleteData(OpenSearchEventMessage eventMessage)
    {
      if (eventMessage.ObjectIds.Any())
      {
        foreach (var objectId in eventMessage.ObjectIds)
        {
          await _courseFtsService.DeleteDocumentAsync(objectId);
        }
      }
    }

    #endregion
  }
}

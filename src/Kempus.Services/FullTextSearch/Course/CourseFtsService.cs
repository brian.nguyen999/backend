﻿using Kempus.Core.Constants;
using Kempus.Core.Models;
using Kempus.Core.Models.Configuration;
using Kempus.EntityFramework;
using Kempus.Integration.Aws.OpenSearch;
using Kempus.Integration.Aws.SQS;
using Kempus.Models.FullTextSearch.Course;
using Kempus.Models.FullTextSearch.Instructor;
using OpenSearch.Client;

namespace Kempus.Services.FullTextSearch.Course
{
  public class CourseFtsService : ICourseFtsService
  {
    private readonly IFullTextSearchService<CourseFtsModel> _openSearchService;
    private readonly ISqsService _sqsService;
    private readonly SqsConfig _sqsConfig;
    private readonly ReplicaDbContext _replicaDbContext;
    private readonly MasterDbContext _masterDbContext;

    public CourseFtsService(
        IFullTextSearchService<CourseFtsModel> openSearchService,
        ReplicaDbContext replicaDbContext,
        MasterDbContext masterDbContext,
        ISqsService sqsService,
        AwsConfig awsConfig
        )
    {
      _openSearchService = openSearchService;
      _replicaDbContext = replicaDbContext;
      _masterDbContext = masterDbContext;
      _sqsService = sqsService;
      _sqsConfig = awsConfig.Sqs;
    }

    public async Task IndexDocumentAsync(List<CourseFtsModel> input)
    {
      await EnsureIndexCreatedAsync();
      await _openSearchService.IndexDocumentAsync(input, CoreConstants.OpenSearchIndex.Courses);
    }

    public async Task<PagingResult<CourseFtsModel>> SearchDocumentAsync(CourseFtsFilterModel filter, Pageable pageable)
    {
      var filterQuery = BuildFilterConditions(filter);
      var totalCount = await _openSearchService.CountDocumentAsync(BuildCountQuery(filterQuery));
      if (totalCount < 1)
      {
        return new PagingResult<CourseFtsModel>(0, pageable.PageIndex, pageable.PageSize);
      }

      var result = await _openSearchService.SearchDocumentAsync(BuildSearchQuery(filterQuery, pageable));
      return new PagingResult<CourseFtsModel>(result, totalCount, pageable.PageIndex, pageable.PageSize); ;
    }

    public async Task DeleteDocumentAsync(Guid id)
    {
      await _openSearchService.DeleteDocumentAsync(id, x => x.Index(CoreConstants.OpenSearchIndex.Courses));
    }

    public List<CourseFtsModel> BuildDataModel(List<Guid> ids)
    {
      var courses = _replicaDbContext.Courses
        .Select(x => new CourseFtsModel
        {
          Id = x.Guid,
          Name = x.Name,
          SchoolId = x.SchoolId.Value,
          CreatedDate = x.CreatedDate,
          DepartmentId = x.DepartmentId,
        })
        .Where(x => ids.Contains(x.Id))
        .ToList();
      if (!courses.Any()) return new List<CourseFtsModel>();

      SetInstructors(courses, false);
      SetDepartments(courses, false);
      return courses;
    }

    public async Task DeleteIndex(string indexName)
    {
      await _openSearchService.DeleteIndexAsync(indexName);
    }

    #region Private methods

    private async Task EnsureIndexCreatedAsync()
    {
      await _openSearchService.EnsureIndexCreatedAsync(
          CoreConstants.OpenSearchIndex.Courses,
          indice => indice.CourseFtsModelMapping());
    }

    private Func<SearchDescriptor<CourseFtsModel>, ISearchRequest> BuildSearchQuery
      (Func<QueryContainerDescriptor<CourseFtsModel>, QueryContainer> filter, Pageable pageable)
    {
      Func<SearchDescriptor<CourseFtsModel>, ISearchRequest> query = s =>
        s.Index(CoreConstants.OpenSearchIndex.Courses)
          .Query(filter)
          .Sort(BuildSortQuery(pageable))
          .Skip(pageable.Offset)
          .Take(pageable.PageSize);
      return query;
    }

    private Func<SortDescriptor<CourseFtsModel>, IPromise<IList<ISort>>> BuildSortQuery(Pageable pageable)
    {
      Func<SortDescriptor<CourseFtsModel>, IPromise<IList<ISort>>> sort = pageable.SortedField switch
      {
        "name" => pageable.IsDescending ? x => x.Descending("name") : x => x.Ascending("name"),
        _ => x => x.Descending("_score")
      };
      return sort;
    }

    private Func<CountDescriptor<CourseFtsModel>, ICountRequest> BuildCountQuery
      (Func<QueryContainerDescriptor<CourseFtsModel>, QueryContainer> filter)
    {
      Func<CountDescriptor<CourseFtsModel>, ICountRequest> query = s =>
        s.Index(CoreConstants.OpenSearchIndex.Courses)
          .Query(filter);
      return query;
    }

    private Func<QueryContainerDescriptor<CourseFtsModel>, QueryContainer> BuildFilterConditions(CourseFtsFilterModel input)
    {
      Func<QueryContainerDescriptor<CourseFtsModel>, QueryContainer> filter = null;
      filter = s => s.Bool(p => p.Must(m =>
        m.Match(q => q.Field(fld => fld.SchoolId).Query(input.SchoolId.ToString())) &&
        m.QueryString(m => m
          .Fields(x => x
            .Field(fld => fld.Name)
            .Field(fld => fld.Instructors.First().Name)
          )
          .Query($"*{input.Keyword}*")
        )));

      return filter;
    }

    private void SetInstructors(List<CourseFtsModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var courseIds = data.Select(x => x.Id).ToList();
      var mapData = dbContext.CourseInstructor
        .Where(x => courseIds.Contains(x.CourseId))
        .Join(dbContext.Instructors,
          courseInstructor => courseInstructor.InstructorId,
          instructor => instructor.Guid,
          (courseInstructor, instructor) => new
          {
            CourseId = courseInstructor.CourseId,
            Instructor = new InstructorFtsModel()
            {
              Id = instructor.Guid,
              Name = instructor.Name,
            }
          })
        .ToList()
        .ToLookup(x => x.CourseId, x => x.Instructor);

      if (!mapData.Any()) return;

      foreach (var item in data)
      {
        item.Instructors = mapData
          .Where(q => q.Key == item.Id)
          .SelectMany(o => o)
          .ToList();
      }
    }

    private void SetDepartments(List<CourseFtsModel> data, bool useMasterDb)
    {
      var dbContext = useMasterDb ? (ApplicationDbContext)_masterDbContext : _replicaDbContext;
      var departmentProgramIds = data.Select(x => x.DepartmentId).ToList();
      var mapData = dbContext.DepartmentPrograms
        .Where(x => departmentProgramIds.Contains(x.Guid))
        .ToDictionary(x => x.Guid, x => x.Name);

      if (!mapData.Any()) return;

      foreach (var item in data)
      {
        item.DepartmentName = item.DepartmentId != null
          ? mapData.GetValueOrDefault(item.DepartmentId.Value)
          : null;
      }
    }

    #endregion

  }
}

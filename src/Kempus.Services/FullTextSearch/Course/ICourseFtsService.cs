﻿using Kempus.Core.Models;
using Kempus.Models.FullTextSearch.Course;

namespace Kempus.Services.FullTextSearch.Course;

public interface ICourseFtsService
{
  Task IndexDocumentAsync(List<CourseFtsModel> input);
  Task DeleteDocumentAsync(Guid id);
  Task<PagingResult<CourseFtsModel>> SearchDocumentAsync(CourseFtsFilterModel filter, Pageable pageable);
  List<CourseFtsModel> BuildDataModel(List<Guid> ids);
  Task DeleteIndex(string indexName);
}
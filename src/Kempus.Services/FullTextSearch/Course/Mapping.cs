﻿using Kempus.Models.FullTextSearch.Course;
using Kempus.Models.FullTextSearch.Instructor;
using OpenSearch.Client;

namespace Kempus.Services.FullTextSearch.Course
{
  public static class Mapping
  {
    public static CreateIndexDescriptor CourseFtsModelMapping(this CreateIndexDescriptor descriptor)
    {
      return descriptor
        .Map<CourseFtsModel>(m => m
          // Flatten object mapping
          .Properties(p => p
            .Keyword(t => t.Name(n => n.Id))
            .Text(t => t.Name(n => n.Name))
            .Date(t => t.Name(n => n.CreatedDate))
            .Keyword(t => t.Name(n => n.SchoolId))
            .Keyword(t => t.Name(n => n.DepartmentId))
            .Text(t => t.Name(n => n.DepartmentName))
            // Nested object mapping
            .Object<InstructorFtsModel>(o => o
              .Name(n => n.Instructors)
              .Properties(sp => sp
                .Keyword(z => z.Name(e => e.Id))
                .Text(z => z.Name(e => e.Name)))
            )
          )
        );
    }
  }
}

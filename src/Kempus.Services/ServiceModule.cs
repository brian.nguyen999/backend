using Kempus.Core;
using Kempus.Core.Models.Configuration;
using Kempus.Services.Admin.Banner;
using Kempus.Services.Admin.BellNotification;
using Kempus.Services.Admin.Course;
using Kempus.Services.Admin.DepartmentProgram;
using Kempus.Services.Admin.Flag;
using Kempus.Services.Admin.Instructor;
using Kempus.Services.Admin.Kemp;
using Kempus.Services.Admin.Major;
using Kempus.Services.Admin.Poll;
using Kempus.Services.Admin.PollOption;
using Kempus.Services.Admin.Post;
using Kempus.Services.Admin.Review;
using Kempus.Services.Admin.Role;
using Kempus.Services.Admin.Topic;
using Kempus.Services.Common.Email;
using Kempus.Services.Common.HttpsContext;
using Kempus.Services.Common.KempTransaction;
using Kempus.Services.Common.Queue;
using Kempus.Services.Common.Redis;
using Kempus.Services.Common.Upload;
using Kempus.Services.Common.WebSocket;
using Kempus.Services.FullTextSearch.Course;
using Kempus.Services.FullTextSearch.CourseReview;
using Kempus.Services.FullTextSearch.OpenSearchEvent;
using Kempus.Services.Public.Authentication;
using Kempus.Services.Public.Authentication.JWT;
using Kempus.Services.Public.Authorization;
using Kempus.Services.Public.Banner;
using Kempus.Services.Public.BellNotification;
using Kempus.Services.Public.Bookmark;
using Kempus.Services.Public.Comment;
using Kempus.Services.Public.Conversation;
using Kempus.Services.Public.Course;
using Kempus.Services.Public.CourseReview;
using Kempus.Services.Public.CourseReviewActivity;
using Kempus.Services.Public.CourseReviewActivitySummary;
using Kempus.Services.Public.CourseReviewLike;
using Kempus.Services.Public.CourseSummary;
using Kempus.Services.Public.Degree;
using Kempus.Services.Public.DepartmentProgram;
using Kempus.Services.Public.EmailTemplate;
using Kempus.Services.Public.Flag;
using Kempus.Services.Public.Instructor;
using Kempus.Services.Public.KempTransaction;
using Kempus.Services.Public.Keyword;
using Kempus.Services.Public.LandingPage;
using Kempus.Services.Public.Major;
using Kempus.Services.Public.Notification;
using Kempus.Services.Public.Otp;
using Kempus.Services.Public.Permission;
using Kempus.Services.Public.Poll;
using Kempus.Services.Public.PollOptions;
using Kempus.Services.Public.Post;
using Kempus.Services.Public.PostKeyword;
using Kempus.Services.Public.PostPollActivity;
using Kempus.Services.Public.PostPollActivitySummary;
using Kempus.Services.Public.PostPollLike;
using Kempus.Services.Public.RolePermission;
using Kempus.Services.Public.School;
using Kempus.Services.Public.SchoolLifeTimeAnalytic;
using Kempus.Services.Public.SignalR;
using Kempus.Services.Public.Statistics;
using Kempus.Services.Public.Topic;
using Kempus.Services.Public.Trending;
using Kempus.Services.Public.User;
using Kempus.Services.Public.UserLoginHistory;
using Kempus.Services.Public.UserReferralStatistic;
using Kempus.Services.Public.UserTracking;
using Kempus.Services.Public.Vote;
using Kempus.Services.Public.WaitList;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Kempus.Services
{
  public static class ServiceModule
  {
    public static void ConfigureServices(this IServiceCollection services, IConfiguration configuration)
    {
      // UserConfig
      var userConfig = configuration.GetSection(nameof(UserConfig)).Get<UserConfig>();
      services.AddSingleton(userConfig);

      // AdminConfig
      var adminConfig = configuration.GetSection(nameof(AdminConfig)).Get<AdminConfig>();
      services.AddSingleton(adminConfig);

      // DomainConfig
      var domainConfig = configuration.GetSection(nameof(DomainConfig)).Get<DomainConfig>();
      services.AddSingleton(domainConfig);

      // AblyConfig
      var ablyConfig = configuration.GetSection(nameof(AblyConfig)).Get<AblyConfig>();
      services.AddSingleton(ablyConfig);

      // Common
      services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
      services.AddScoped<IHttpContextService, HttpContextService>();
      services.AddSingleton<ISignalRService, SignalRService>();

      // Authentication
      services.AddScoped<IAuthenticationService, AuthenticationService>();
      services.AddSingleton<IJwtService, JwtService>();
      services.AddScoped<IOtpService, OtpService>();

      // Authorization
      //services.AddScoped<IPermissionService, PermissionService>();
      //services.AddScoped<IRoleService, RoleService>();
      //services.AddScoped<IRolePermissionService, RolePermissionService>();
      //services.AddScoped<IUserRoleService, UserRoleService>();
      services.AddScoped<IUserPermissionService, UserPermissionService>();
      services.AddScoped<IUserService, UserService>();
      services.AddScoped<ILandingPageService, LandingPageService>();

      // Redis
      services.AddScoped<IRedisService, RedisService>();

      // Post
      services.AddScoped<IPostService, PostService>();
      services.AddScoped<IKeywordService, KeywordService>();
      services.AddScoped<IPostKeywordService, PostKeywordService>();

      // Poll
      services.AddScoped<IPollService, PollService>();
      services.AddScoped<IPollOptionService, PollOptionService>();

      // Topic
      services.AddScoped<ITopicService, TopicService>();

      // Permission
      services.AddScoped<IPermissionService, PermissionService>();

      // Role
      services.AddScoped<IRoleService, RoleService>();

      // Role Permission
      services.AddScoped<IRolePermissionService, RolePermissionService>();

      // Email Template
      services.AddScoped<IEmailTemplateService, EmailTemplateService>();

      // Email
      services.AddScoped<IEmailService, EmailService>();

      // Notification
      services.AddScoped<IEmailNotificationService, EmailNotificationService>();

      // Course
      services.AddScoped<ICourseService, CourseService>();

      // Course Review
      services.AddScoped<ICourseReviewService, CourseReviewService>();

      // School
      services.AddScoped<ISchoolService, SchoolService>();

      // PostComment
      services.AddScoped<IPostCommentService, PostCommentService>();

      // Wait List
      services.AddScoped<IWaitListService, WaitListService>();

      // Flag
      services.AddScoped<IFlagService, FlagService>();

      // Course Summary
      services.AddScoped<ICourseSummaryService, CourseSummaryService>();

      // Bookmark
      services.AddScoped<IBookmarkService, BookmarkService>();

      // Course Summary
      services.AddScoped<ICourseSummaryService, CourseSummaryService>();

      // Course Full Text Search
      services.AddScoped<ICourseFtsService, CourseFtsService>();

      // Instructor
      services.AddScoped<IInstructorService, InstructorService>();

      // Open Search Event
      services.AddScoped<IOpenSearchEventService, OpenSearchEventService>();
      services.AddScoped<IOpenSearchEventSubscriber, OpenSearchEventSubscriber>();
      services.AddScoped<IOpenSearchEventPublisher, OpenSearchEventPublisher>();

      // Service config
      services.AddTransient<SessionStore, SessionStore>();

      // SchoolLifeTimeAnalyticService
      services.AddScoped<ISchoolLifeTimeAnalyticService, SchoolLifeTimeAnalyticService>();

      // SchoolLifeTimeAnalyticService
      services.AddScoped<ISchoolLifeTimeAnalyticService, SchoolLifeTimeAnalyticService>();

      // CourseReviewLikeService
      services.AddScoped<ICourseReviewLikeService, CourseReviewLikeService>();

      // CourseReviewActivityService
      services.AddScoped<ICourseReviewActivitySummaryService, CourseReviewActivitySummaryService>();
      services.AddScoped<ICourseReviewActivityService, CourseReviewActivity>();

      // AwsS3IntegrationHelper
      services.AddTransient<AwsS3IntegrationHelper, AwsS3IntegrationHelper>();

      // PostPollLikeService
      services.AddScoped<IPostPollLikeService, PostPollLikeService>();

      // Vote
      services.AddScoped<IVoteService, VoteService>();

      // Statistics
      services.AddScoped<IStatisticsService, StatisticsService>();

      // Department Program
      services.AddScoped<IDepartmentProgramService, DepartmentProgramService>();

      // PostPollActivity
      services.AddScoped<IPostPollActivityService, PostPollActivityService>();
      services.AddScoped<IPostPollActivitySummaryService, PostPollActivitySummaryService>();

      // Trending
      services.AddScoped<ITrendingService, TrendingService>();

      // PostAdminService
      services.AddScoped<IPostAdminService, PostAdminService>();

      // Department Program
      services.AddScoped<IDepartmentProgramService, DepartmentProgramService>();

      // TopicAdminService
      services.AddScoped<ITopicAdminService, TopicAdminService>();

      // ReviewAdminService
      services.AddScoped<IReviewAdminService, ReviewAdminService>();

      // PollAdminService
      services.AddScoped<IPollAdminService, PollAdminService>();

      // PollOptionAdminService
      services.AddScoped<IPollOptionAdminService, PollOptionAdminService>();

      // Banner Service
      services.AddScoped<IBannerAdminService, BannerAdminService>();
      services.AddScoped<IBannerService, BannerService>();

      // MajorAdminService
      services.AddScoped<IMajorAdminService, MajorAdminService>();

      // KempTransactionService
      services.AddScoped<IKempTransactionCommonService, KempTransactionCommonService>();

      // TrackingService
      services.AddScoped<IUserTrackingService, UserTrackingService>();

      // MajorService
      services.AddScoped<IMajorService, MajorService>();

      // DegreeService
      services.AddScoped<IDegreeService, DegreeService>();

      // BellNotification
      services.AddScoped<IBellNotificationService, BellNotificationService>();

      // FlagAdminService
      services.AddScoped<IFlagAdminService, FlagAdminService>();

      // UserReferralStatisticService
      services.AddScoped<IUserReferralStatisticService, UserReferralStatisticService>();

      // KempAdminService
      services.AddScoped<IKempAdminService, KempAdminService>();

      // UserLoginHistoryService
      services.AddScoped<IUserLoginHistoryService, UserLoginHistoryService>();

      // BellNotificationAdminService
      services.AddScoped<IBellNotificationAdminService, BellNotificationAdminService>();

      // WebSocket
      services.AddScoped<IWebSocketService, WebSocketService>();

      // KempTransactionService
      services.AddScoped<IKempTransactionService, KempTransactionService>();

      // DepartmentProgramAdminService
      services.AddScoped<IDepartmentProgramAdminService, DepartmentProgramAdminService>();

      // InstructorAdminService
      services.AddScoped<IInstructorAdminService, InstructorAdminService>();

      // CourseAdminService
      services.AddScoped<ICourseAdminService, CourseAdminService>();

      // CourseInstructorAdminService
      services.AddScoped<ICourseInstructorAdminService, CourseInstructorAdminService>();

      // CourseReviewFtsService
      services.AddScoped<ICourseReviewFtsService, CourseReviewFtsService>();
      // ConversationService
      services.AddScoped<IConversationService, ConversationService>();
    }
  }
}
﻿using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Admin.BellNotification;
using Kempus.Models.Public.BellNotification;

namespace Kempus.Services.QueryBuilder
{
  public static class BellNotificationQueryBuilder<T> where T : BellNotificationEntity
  {
    public static IQueryable<T> BuildWhereClause(BellNotificationFilterModel model, IQueryable<T> query, ApplicationDbContext dbContext)
    {
      if (model.ObjectTypeId != null)
      {
        query = query.Where(x => x.ObjectTypeId == model.ObjectTypeId);
      }

      if (model.BeforeTime != null)
      {
        query = query.Where(x => x.CreatedDate < model.BeforeTime);
      }

      return query;
    }

    public static IQueryable<T> BuildWhereClause(BellNotificationFilterCmsModel model, IQueryable<T> query, ApplicationDbContext dbContext)
    {
      if (model.Ids.Any())
      {
        query = query.Where(x => model.Ids.Contains(x.Guid));
      }

      if (model.Keyword != null)
      {
        query = query.Where(x => x.Title.ToLower().Contains(model.Keyword.ToLower()));
      }

      if (model.SchoolId != null)
      {
        query = query.Join(dbContext.BellNotificationUsers,
          bell => bell.Guid,
          bellUser => bellUser.BellNotificationId,
          (bell, bellUser) => new
          {
            bell,
            bellUser
          }).Join(dbContext.Users.Where(x => x.SchoolId == model.SchoolId),
          bellBellUser => bellBellUser.bellUser.CreatedUser,
          user => user.UserId,
          (bellBellUser, user) => bellBellUser.bell).Distinct();
      }

      return query;
    }
  }
}

﻿using Kempus.Core.Extensions;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Admin.Kemp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.QueryBuilder
{
  public class KempTransactionQueryBuilder<T> where T : KempTransactionEntity
  {
    public static IQueryable<T> BuildWhereClause(KempFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      if (model.Id != null)
      {
        query = query.Where(x => x.Guid == model.Id);
      }

      if (model.SchoolId != null)
      {
        query = query.Where(x => x.SchoolId == model.SchoolId);
      }

      if (model.Type != null)
      {
        query = query.Where(x => x.Type == model.Type);
      }

      if (model.UserId != null)
      {
        query = query.Where(x => x.UserId == model.UserId);
      }

      if (!string.IsNullOrWhiteSpace(model.Keyword))
      {
        query = query.Join(dbContext.Users,
          transaction => transaction.UserId,
          user => user.UserId,
          (transaction, user) => new { transaction, user })
          .Where(x => x.user.UserName.Contains(model.Keyword)
            //|| EnumExtensions.GetEnumDescription((KempTransactionType)x.transaction.Type).Contains(model.Keyword)
            || x.transaction.ReferenceGuid.ToString().Contains(model.Keyword)
            || x.transaction.Amount.ToString().Contains(model.Keyword)
           //|| x.transaction.CreatedDate.ToString("MM/dd/yyyy hh:mm").Contains(model.Keyword)
           )
          .Select(x => x.transaction);
      }

      return query.Distinct();
    }
  }
}

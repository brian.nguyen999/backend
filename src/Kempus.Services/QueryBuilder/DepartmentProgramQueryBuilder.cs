﻿using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.DepartmentProgram;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Services.QueryBuilder
{
  public class DepartmentProgramQueryBuilder<T> where T : DepartmentProgramEntity
  {
    public static IQueryable<T> BuildWhereClause(DepartmentProgramFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      if (model.Id != null)
      {
        query = query.Where(x => x.Guid == model.Id);
      }

      if (model.Status != null)
      {
        query = query.Where(x => x.Status == model.Status);
      }

      if (model.SchoolId != null)
      {
        query = query.Where(x => x.SchoolId == model.SchoolId);
      }

      if (!string.IsNullOrEmpty(model.Keyword))
      {
        query = query.Where(x => x.Name.ToLower().Contains(model.Keyword.ToLower()));
      }

      return query;
    }
  }
}

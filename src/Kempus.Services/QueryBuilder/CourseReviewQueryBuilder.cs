﻿using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Admin.Review;
using Kempus.Models.Public.CourseReview;
using Microsoft.EntityFrameworkCore;

namespace Kempus.Services.QueryBuilder
{
  public class CourseReviewQueryBuilder<T> where T : CourseReviewEntity
  {
    public static IQueryable<T> BuildWhereClause(CourseReviewFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      if (model.CourseId != null)
      {
        query = query.Where(x => x.CourseId == model.CourseId);
      }

      if (model.Status != null)
      {
        query = query.Where(x => x.Status == model.Status);
      }

      if (model.UserId != null)
      {
        query = query.Where(x => x.UserId == model.UserId);
      }

      if (model.SchoolId != null)
      {
        query = query.Where(x => x.SchoolId == model.SchoolId);
      }

      if (model.CourseIds != null && model.CourseIds.Any())
      {
        query = query.Where(x => model.CourseIds.Contains(x.CourseId));
      }
      
      if (model.CourseReviewIds != null && model.CourseReviewIds.Any())
      {
        query = query.Where(x => model.CourseReviewIds.Contains(x.Guid));
      }

      return query.Distinct();
    }

    public static IQueryable<T> BuildWhereClause(ReviewFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      if (model.Id != null)
      {
        query = query.Where(x => x.Guid == model.Id);
      }

      if (model.SchoolId != null)
      {
        query = query.Where(x => x.SchoolId == model.SchoolId);
      }

      if (model.Status != null)
      {
        query = query.Where(x => x.Status == model.Status);
      }

      if (!string.IsNullOrWhiteSpace(model.Keyword))
      {
        query = query.Join(dbContext.Courses,
          courseReview => courseReview.CourseId,
          course => course.Guid,
          (courseReview, course) => new { courseReview, course })
          .Where(x => !string.IsNullOrEmpty(x.course.Name) && x.course.Name.ToLower().Contains(model.Keyword.ToLower()))
          .Select(x => x.courseReview);
      }

      return query.Distinct();
    }
  }
}
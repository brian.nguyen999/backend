﻿using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Course;

namespace Kempus.Services.QueryBuilder
{
  public static class CourseQueryBuilder<T> where T : CourseEntity
  {
    public static IQueryable<T> BuildWhereClause(CourseFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      if (model.CourseIds != null && model.CourseIds.Any())
      {
        query = query.Where(x => model.CourseIds.Contains(x.Guid));
      }

      if (model.SchoolId != null)
      {
        query = query.Where(x => x.SchoolId == model.SchoolId);
      }

      if (!string.IsNullOrEmpty(model.Keyword))
      {
        query = query.Where(x => x.Name.ToLower().Contains(model.Keyword.ToLower()));
      }

      return query;
    }

    public static IQueryable<T> BuildWhereClause(CmsCourseFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      if (model.Id != null)
      {
        query = query.Where(x => x.Guid == model.Id);
      }

      if (model.Status != null)
      {
        query = query.Where(x => x.IsActivated == model.Status);
      }

      if (model.SchoolId != null)
      {
        query = query.Where(x => x.SchoolId == model.SchoolId);
      }

      if (!string.IsNullOrEmpty(model.Keyword))
      {
        query = query.Where(x => x.Name.ToLower().Contains(model.Keyword.ToLower()));
      }

      return query;
    }
  }
}

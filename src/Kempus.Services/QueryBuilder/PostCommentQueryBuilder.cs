﻿using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.PostComments;

namespace Kempus.Services.QueryBuilder
{
  public static class PostCommentQueryBuilder<T> where T : PostCommentEntity
  {
    public static IQueryable<T> BuildWhereClause(PostCommentFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      if (model.PostId != null)
      {
        query = query.Where(x => x.PostId == model.PostId);
      }

      if (model.ParentId != null)
      {
        query = query.Where(x => x.ParentId == model.ParentId);
      }
      else
      {
        query = query.Where(x => x.ParentId == null);
      }

      return query;
    }
  }
}
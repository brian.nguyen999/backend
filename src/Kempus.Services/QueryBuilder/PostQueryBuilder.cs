﻿using Kempus.Entities;
using Kempus.EntityFramework;
using System.Text.RegularExpressions;

namespace Kempus.Services.QueryBuilder
{
  public static class PostQueryBuilder<T> where T : PostEntity
  {
    public static IQueryable<T> BuildWhereClause(Models.Public.Post.PostFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      if (model.IsPublished != null)
      {
        query = query.Where(x => x.IsPublished == model.IsPublished);
      }

      if (model.SchoolId != null)
      {
        query = query.Where(x => x.SchoolId == model.SchoolId);
      }

      if (model.PostIds.Any())
      {
        query = query.Where(x => model.PostIds.Contains(x.Guid));
      }

      if (model.UserId != null)
      {
        query = query.Where(x => x.CreatedBy == model.UserId);
      }

      if (model.TopicId != null)
      {
        query = query.Where(x => x.TopicId == model.TopicId);
      }

      if (!string.IsNullOrWhiteSpace(model.Keyword))
      {
        var lowerKeyword = model.Keyword.ToLower();
        List<string> keywords = Regex.Split(lowerKeyword.Trim(), @"\W|_").ToList();
        query = query.Join(dbContext.PostKeywords,
          post => post.Guid,
          postKeyword => postKeyword.PostId,
          (post, postKeyword) => new { post, postKeyword })
          .Join(dbContext.Keywords,
          postPostKeyword => postPostKeyword.postKeyword.KeywordId,
          keyword => keyword.Guid,
          (postPostKeyword, keyword) => new { postPostKeyword, keyword }).
          Where(x => x.postPostKeyword.post.Title.ToLower().Contains(lowerKeyword)
          || keywords.Contains(x.keyword.Name.ToLower()))
          .Select(x => x.postPostKeyword.post).Distinct();
      }

      return query;
    }

    public static IQueryable<T> BuildWhereClause(Models.Admin.Post.PostFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      if (model.SchoolId != null)
      {
        query = query.Where(x => x.SchoolId == model.SchoolId);
      }

      if (!string.IsNullOrEmpty(model.Keyword))
      {
        query = query.Where(x => x.Title.Contains(model.Keyword));
      }

      if (model.Status != null)
      {
        query = query.Where(x => x.IsPublished == model.Status);
      }

      return query;
    }
  }
}
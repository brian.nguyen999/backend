﻿using Kempus.Entities;
using Kempus.EntityFramework;

namespace Kempus.Services.QueryBuilder
{
  public class MajorQueryBuilder<T> where T : MajorEntity
  {
    public static IQueryable<T> BuildWhereClause(Models.Public.Major.MajorFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      if (!string.IsNullOrWhiteSpace(model.Keyword))
      {
        var lowerKeyword = model.Keyword.ToLower();
        query = query.Where(x => x.Name.ToLower().Contains(lowerKeyword));
      }

      if (model.IsActivated != null)
      {
        query = query.Where(x => x.IsActivated == model.IsActivated);
      }

      return query;
    }

    public static IQueryable<T> BuildWhereClause(Models.Admin.Major.MajorFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      if (!string.IsNullOrWhiteSpace(model.Keyword))
      {
        var lowerKeyword = model.Keyword.ToLower();
        query = query.Where(x => x.Name.ToLower().Contains(lowerKeyword));
      }

      return query;
    }
  }
}

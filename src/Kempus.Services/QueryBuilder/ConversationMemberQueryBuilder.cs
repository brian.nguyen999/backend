﻿using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Conversation;

namespace Kempus.Services.QueryBuilder
{
  public class ConversationMemberQueryBuilder<T> where T : ConversationMemberEntity
  {
    public static IQueryable<T> BuildWhereClause(ConversationFilterModel model, IQueryable<T> query, ApplicationDbContext dbContext)
    {
      if (model.Status != null)
      {
        query = query.Where(x => model.Status.Contains(x.Status));
      }

      if (model.SchoolId != null)
      {
        query = query.Where(x => x.SchoolId == model.SchoolId);
      }

      if (model.UserId != null)
      {
        query = query.Where(x => x.MemberId == model.UserId);
      }

      return query.Distinct();
    }
  }
}

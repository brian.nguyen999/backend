﻿using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.CourseReviewActivity;

namespace Kempus.Services.QueryBuilder
{
  public class CourseReviewActivitySummaryQueryBuilder<T> where T : CourseReviewActivitySummaryEntity
  {
    public static IQueryable<T> BuildWhereClause(CourseReviewActivitySummaryFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      if (model.SchoolId != null)
      {
        query = query.Where(x => x.SchoolId == model.SchoolId);
      }

      if (model.Status != null)
      {
        query = query
          .Join(dbContext.CourseReviews,
            activity => activity.CourseReviewId,
            courseReview => courseReview.Guid,
            (activity, courseReview) => new { activity, courseReview })
          .Where(x => x.courseReview.Status == model.Status)
          .Select(x => x.activity);
      }

      return query;
    }
  }
}
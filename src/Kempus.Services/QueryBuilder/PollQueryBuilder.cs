﻿using Kempus.Entities;
using Kempus.EntityFramework;
using System.Text.RegularExpressions;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.QueryBuilder
{
  public static class PollQueryBuilder<T> where T : PostEntity
  {
    public static IQueryable<T> BuildWhereClause(Models.Public.Poll.PollFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      query = query.Where(x => x.IsPublished);

      if (model.SchoolId != null)
      {
        query = query.Where(x => x.SchoolId == model.SchoolId);
      }

      if (model.PostIds.Any())
      {
        query = query.Where(x => model.PostIds.Contains(x.Guid));
      }

      if (model.UserId != null)
      {
        query = query.Where(x => x.CreatedBy == model.UserId);
      }

      if (!string.IsNullOrWhiteSpace(model.PollStatus))
      {
        var previousSevenDays = DateTime.UtcNow.AddDays(-7);
        Enum.TryParse(model.PollStatus, out PollStatus myStatus);

        if (myStatus == PollStatus.OnGoing)
        {
          query = query.Where(x => x.CreatedDate >= previousSevenDays);
        }
        else
        {
          query = query.Where(x => x.CreatedDate < previousSevenDays);
        }
      }

      if (!string.IsNullOrWhiteSpace(model.Keyword))
      {
        var lowerKeyword = model.Keyword.ToLower();
        List<string> keywords = Regex.Split(lowerKeyword.Trim(), @"\W|_").ToList();
        query = query.Join(dbContext.PostKeywords,
          post => post.Guid,
          postKeyword => postKeyword.PostId,
          (post, postKeyword) => new { post, postKeyword })
          .Join(dbContext.Keywords,
          postPostKeyword => postPostKeyword.postKeyword.KeywordId,
          keyword => keyword.Guid,
          (postPostKeyword, keyword) => new { postPostKeyword, keyword }).
          Where(x => x.postPostKeyword.post.Title.ToLower().Contains(lowerKeyword)
          || keywords.Contains(x.keyword.Name.ToLower()))
          .Select(x => x.postPostKeyword.post).Distinct();
      }

      return query;
    }

    public static IQueryable<T> BuildWhereClause(Models.Admin.Poll.PollFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      if (model.PostIds.Any())
      {
        query = query.Where(x => model.PostIds.Contains(x.Guid));
      }

      if (model.UserId != null)
      {
        query = query.Where(x => x.CreatedBy == model.UserId);
      }

      if (model.SchoolId != null)
      {
        query = query.Where(x => x.SchoolId == model.SchoolId);
      }

      if (model.PublishStatus != null)
      {
        query = query.Where(x => x.IsPublished == model.PublishStatus);
      }

      if (!string.IsNullOrWhiteSpace(model.PollStatus))
      {
        var previousSevenDays = DateTime.UtcNow.AddDays(-7);
        Enum.TryParse(model.PollStatus, out PollStatus myStatus);

        if (myStatus == PollStatus.OnGoing)
        {
          query = query.Where(x => x.CreatedDate >= previousSevenDays);
        }
        else
        {
          query = query.Where(x => x.CreatedDate < previousSevenDays);
        }
      }

      if (!string.IsNullOrWhiteSpace(model.Keyword))
      {
        var lowerKeyword = model.Keyword.ToLower();
        List<string> keywords = Regex.Split(lowerKeyword.Trim(), @"\W|_").ToList();
        query = query.Join(dbContext.PostKeywords,
            post => post.Guid,
            postKeyword => postKeyword.PostId,
            (post, postKeyword) => new { post, postKeyword })
          .Join(dbContext.Keywords,
            postPostKeyword => postPostKeyword.postKeyword.KeywordId,
            keyword => keyword.Guid,
            (postPostKeyword, keyword) => new { postPostKeyword, keyword }).
          Where(x => x.postPostKeyword.post.Title.ToLower().Contains(lowerKeyword)
                     || keywords.Contains(x.keyword.Name.ToLower()))
          .Select(x => x.postPostKeyword.post).Distinct();
      }

      return query;
    }
  }
}
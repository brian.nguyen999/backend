﻿using Kempus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kempus.EntityFramework;
using Kempus.Models.Public.Degree;

namespace Kempus.Services.QueryBuilder
{
  public class DegreeQueryBuilder<T> where T : DegreeEntity
  {
    public static IQueryable<T> BuildWhereClause(DegreeFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      if (!string.IsNullOrWhiteSpace(model.Keyword))
      {
        var lowerKeyword = model.Keyword.ToLower();
        query = query.Where(x => x.Name.ToLower().Contains(lowerKeyword));
      }

      if (model.IsActivated != null)
      {
        query = query.Where(x => x.IsActivated == model.IsActivated);
      }

      return query;
    }
  }
}

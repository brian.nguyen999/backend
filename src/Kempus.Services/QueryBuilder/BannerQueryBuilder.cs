﻿using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Admin.Banner;
using Kempus.Models.Public.Banner;

namespace Kempus.Services.QueryBuilder
{
  public class BannerQueryBuilder<T> where T : BannerEntity
  {
    public static IQueryable<T> BuildWhereClause(BannerCmsFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      if (model.Id != null)
      {
        query = query.Where(x => x.Guid == model.Id);
      }

      if (model.SchoolId != null)
      {
        query = query.Where(x => x.SchoolId == model.SchoolId);
      }

      if (model.Status != null)
      {
        query = query.Where(x => x.Status == model.Status);
      }

      if (!string.IsNullOrEmpty(model.Keyword))
      {
        query = query.Where(x =>!string.IsNullOrEmpty(x.RedirectUrl) && x.RedirectUrl.ToLower().Contains(model.Keyword.ToLower()));
      }

      return query.Distinct();
    }

    public static IQueryable<T> BuildWhereClause(BannerFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      if (model.SchoolId != null)
      {
        query = query.Where(x => x.SchoolId == model.SchoolId);
      }

      if (model.Status != null)
      {
        query = query.Where(x => x.Status == model.Status);
      }

      return query.Distinct();
    }
  }
}

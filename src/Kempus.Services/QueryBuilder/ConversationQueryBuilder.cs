﻿using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Conversation;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.QueryBuilder
{
  public class ConversationQueryBuilder<T> where T : ConversationEntity
  {
    public static IQueryable<T> BuildWhereClause(ConversationFilterModel model, IQueryable<T> query, ApplicationDbContext dbContext)
    {
      if (model.SchoolId != null)
      {
        query = query.Where(x => x.SchoolId == model.SchoolId);
      }

      if (model.UserId != null)
      {
        query = query
          .Join(dbContext.ConversationMembers.Where(x => x.Status != (byte)ConversationMemberStatus.Delete),
            conversation => conversation.Guid,
            conversationMembers => conversationMembers.ConversationId,
            (conversation, conversationMembers) => new { conversation, conversationMembers })
          .Where(x => x.conversationMembers.MemberId == model.UserId)
          .Select(x => x.conversation);
      }

      if (!string.IsNullOrEmpty(model.Keyword))
      {

      }

      return query.Distinct();
    }
  }
}

﻿using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.QueryBuilder
{
  public class UserQueryBuidler<T> where T : ApplicationUser
  {
    public static IQueryable<T> BuildWhereClause(UserFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      query = query.Where(x => x.UserType == (byte)UserType.User || x.UserType == (byte)UserType.Ambassador);

      if (model.Id != null)
      {
        query = query.Where(x => x.UserId == model.Id);
      }

      if (model.Status != null)
      {
        query = query.Where(x => x.Status == model.Status);
      }

      if (model.SchoolId != null)
      {
        query = query.Where(x => x.SchoolId == model.SchoolId);
      }

      if (model.Group != null && model.Group != (byte)UserType.Admin)
      {
        query = query.Where(x => x.UserType == model.Group);
      }

      if (!string.IsNullOrWhiteSpace(model.Keyword))
      {
        query = query.Where(x => x.UserName.ToLower().Contains(model.Keyword.ToLower()));
      }

      return query;
    }
  }
}

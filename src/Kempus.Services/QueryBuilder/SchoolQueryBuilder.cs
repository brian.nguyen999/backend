﻿using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.School;

namespace Kempus.Services.QueryBuilder
{
  public static class SchoolQueryBuilder<T> where T : SchoolEntity
  {
    public static IQueryable<T> BuildWhereClause(SchoolFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      if (model.Id != null)
      {
        query = query.Where(x => x.Guid == model.Id);
      }

      if (model.IsShowHomePage != null)
      {
        query = query.Where(x => x.IsShowHomePage == model.IsShowHomePage);
      }

      if (model.IsActivated != null)
      {
        query = query.Where(x => x.IsActivated == model.IsActivated);
      }

      if (model.Status != null)
      {
        query = query.Where(x => x.IsActivated == model.Status);
      }

      if (!string.IsNullOrWhiteSpace(model.Keyword))
      {
        query = query.Where(x => x.Name.Contains(model.Keyword));
      }

      if (model.IsHaveUser != null && model.IsHaveUser == true)
      {
        query = query.Join(dbContext.Users,
          school => school.Guid,
          user => user.SchoolId,
          (school, user) => school).Distinct();
      }

      return query;
    }
  }
}

﻿using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Bookmark;

namespace Kempus.Services.QueryBuilder
{
  public static class BookmarkQueryBuilder<T> where T : BookmarkEntity
  {
    public static IQueryable<T> BuildWhereClause(BookmarkFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      if (model.UserId != null)
      {
        query = query.Where(x => x.UserId == model.UserId);
      }

      return query;
    }
  }
}

﻿using Kempus.Entities;
using Kempus.EntityFramework;

namespace Kempus.Services.QueryBuilder
{
  public static class TopicQueryBuilder<T> where T : TopicEntity
  {
    public static IQueryable<T> BuildWhereClause(Models.Public.Topic.TopicFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      if (model.TopicId != null)
      {
        query = query.Where(x => x.Guid == model.TopicId);
      }

      if (model.SchoolId != null)
      {
        query = query.Where(x => x.SchoolId == model.SchoolId);
      }

      if (model.Status != null)
      {
        query = query.Where(x => x.IsActivated == model.Status);
      }

      if (!string.IsNullOrWhiteSpace(model.Keyword))
      {
        var lowerKeyword = model.Keyword.ToLower();
        query = query.Where(x => x.Name.ToLower().Contains(lowerKeyword));
      }

      return query;
    }

    public static IQueryable<T> BuildWhereClause(Models.Admin.Topic.TopicFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      if (model.TopicId != null)
      {
        query = query.Where(x => x.Guid == model.TopicId);
      }

      if (model.SchoolId != null)
      {
        query = query.Where(x => x.SchoolId == model.SchoolId);
      }

      if (!string.IsNullOrWhiteSpace(model.Keyword))
      {
        var lowerKeyword = model.Keyword.ToLower();
        query = query.Where(x => x.Name.ToLower().Contains(lowerKeyword));
      }

      return query;
    }
  }
}
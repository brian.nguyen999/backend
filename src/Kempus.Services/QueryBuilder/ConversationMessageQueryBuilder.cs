﻿using Kempus.Entities;
using Kempus.Models.Public.Conversation;

namespace Kempus.Services.QueryBuilder
{
  public class ConversationMessageQueryBuilder<T> where T : MessageEntity
  {
    public static IQueryable<T> BuildWhereClause(ConversationMessageFilterModel model, IQueryable<T> query)
    {
      query = query.Where(x => x.SchoolId == model.SchoolId);
      query = query.Where(x => x.ConversationId == model.ConversationId);

      if (model.ConversationDeletionTime != null)
      {
        query = query.Where(x => x.CreatedDate > model.ConversationDeletionTime);
      }

      if (model.DateTime != null)
      {
        query = query.Where(x => x.CreatedDate <= model.DateTime);
      }

      if (model.Keyword != null)
      {
        query = query.Where(x => x.Content != null && x.Content.Contains(model.Keyword));
      }

      return query.Distinct();
    }
  }
}

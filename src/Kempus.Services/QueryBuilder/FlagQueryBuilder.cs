﻿using Kempus.Core.Extensions;
using Kempus.Core.Models;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Admin.Flag;
using Kempus.Models.Public.School;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.QueryBuilder
{
  public static class FlagQueryBuilder<T> where T : FlagEntity
  {
    public static IQueryable<T> BuildWhereClause(FlagFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      if (model.Id != null)
      {
        query = query.Where(x => x.Guid == model.Id);
      }

      if (model.SchoolId != null)
      {
        query = query.Where(x => x.SchoolId == model.SchoolId);
      }

      if (model.Status != null)
      {
        query = query.Where(x => x.Status == model.Status);
      }

      if (!string.IsNullOrWhiteSpace(model.Keyword))
      {
        FlagType flagtype;
        if (Enum.TryParse(model.Keyword, true, out flagtype))
        {
          query = query.Where(x => x.ObjectType == (byte)flagtype);
        }
      }

      return query;
    }
  }
}

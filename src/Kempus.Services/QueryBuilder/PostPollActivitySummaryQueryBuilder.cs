﻿using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.PostPollActivitySummary;

namespace Kempus.Services.QueryBuilder
{
  public class PostPollActivitySummaryQueryBuilder<T> where T : PostPollActivitySummaryEntity
  {
    public static IQueryable<T> BuildWhereClause(PostPollActivitySummaryFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      if (model.SchoolId != null)
      {
        query = query.Where(x => x.SchoolId == model.SchoolId);
      }

      if (model.ExcludedIds != null && model.ExcludedIds.Any())
      {
        query = query.Where(x => !model.ExcludedIds.Contains(x.ObjectId));
      }

      if (model.IsPublished != null)
      {
        query = query
          .Join(dbContext.Posts,
            postSummary => postSummary.ObjectId,
            post => post.Guid,
            (postSummary, post) => new { postSummary, post })
          .Where(x => x.post.IsPublished == model.IsPublished.Value)
          .Select(x => x.postSummary);
      }

      return query.Distinct();
    }
  }
}
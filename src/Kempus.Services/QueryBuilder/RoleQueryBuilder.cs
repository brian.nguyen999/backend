using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Role;

namespace Kempus.Services.QueryBuilder
{
  public static class RoleQueryBuilder<T> where T : RoleEntity
  {
    public static IQueryable<T> BuildWhereClause(RoleFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      if (model.RoleId != null)
      {
        query = query.Where(x => x.Guid == model.RoleId);
      }

      if (model.IsActive != null)
      {
        query = query.Where(x => x.IsActive == model.IsActive);
      }

      if (!string.IsNullOrWhiteSpace(model.Keyword))
      {
        query = query.Where(x => x.Name.ToLower().Contains(model.Keyword.ToLower()));
      }

      return query;
    }
  }
}
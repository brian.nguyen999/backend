﻿using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Role;
using Kempus.Models.Public.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.QueryBuilder
{
  public static class AdminQueryBuilder<T> where T : ApplicationUser
  {
    public static IQueryable<T> BuildWhereClause(AdminFilterModel model, IQueryable<T> query,
      ApplicationDbContext dbContext)
    {
      query = query.Where(x => x.UserType == (byte)UserType.Admin);

      if (model.Id != null)
      {
        query = query.Where(x => x.UserId == model.Id);
      }

      if (model.Status != null)
      {
        query = query.Where(x => x.Status == model.Status);
      }

      if(model.RoleIds != null && model.RoleIds.Any())
      {
        query = query.Join(dbContext.UserRole,
          user => user.UserId,
          userRole => userRole.UserId,
          (user, userRole) => new { user, userRole })
          .Join(dbContext.Role.Where(x => model.RoleIds.Contains(x.Guid)),
          userUserRole => userUserRole.userRole.RoleId,
          role => role.Id,
          (userUserRole, role) => new { userUserRole, role })
          .Select(x => x.userUserRole.user).Distinct();
      }

      if (!string.IsNullOrWhiteSpace(model.Keyword))
      {
        query = query.Where(x => x.UserName.ToLower().Contains(model.Keyword.ToLower())
          || x.Email.ToLower().Contains(model.Keyword.ToLower())
        );
      }

      return query;
    }
  }
}

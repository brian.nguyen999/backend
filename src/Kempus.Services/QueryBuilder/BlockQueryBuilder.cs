﻿using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Conversation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.QueryBuilder
{
  public class BlockQueryBuilder<T> where T : BlockEntity
  {
    public static IQueryable<T> BuildWhereClause(ConversationBlockFilterModel model, IQueryable<T> query, ApplicationDbContext dbContext)
    {
      query = query.Where(x => x.Blocker == model.UserId && !x.IsDeleted);

      if (!string.IsNullOrEmpty(model.Keyword))
      {
        query = query
          .Join(dbContext.Users.Where(x => !string.IsNullOrEmpty(x.NickName) && x.NickName.Contains(model.Keyword)),
            conversationBlock => conversationBlock.BlockedUser,
            user => user.UserId,
            (conversationBlock, user) => new { conversationBlock, user })
          .Select(x => x.conversationBlock);
      }

      return query.Distinct();
    }
  }
}

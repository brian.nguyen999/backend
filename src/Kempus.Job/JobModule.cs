﻿using Kempus.Core.Extensions;
using Kempus.Job.Jobs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Quartz;

namespace Kempus.Job
{
  public static class JobModule
  {
    public static void ConfigureServices(this IServiceCollection services, IConfiguration configuration)
    {
      // Quartz config
      services.AddQuartz(q =>
      {
        q.UseMicrosoftDependencyInjectionJobFactory();
        q.AddJobAndTrigger<DeleteOtpJob>(configuration);
        q.AddJobAndTrigger<OpenSearchEventWorkerJob>(configuration);
        q.AddJobAndTrigger<ClearCourseReviewActivityJob>(configuration);
        q.AddJobAndTrigger<ClearPostPollActivityJob>(configuration);
        q.AddJobAndTrigger<CalculateCourseReviewActivityJob>(configuration);
        q.AddJobAndTrigger<CalculatePollPostActivityJob>(configuration);
        q.AddJobAndTrigger<SendScheduledNotificationJob>(configuration);
      });

      services.AddQuartzHostedService(q => q.WaitForJobsToComplete = true);
    }
  }
}
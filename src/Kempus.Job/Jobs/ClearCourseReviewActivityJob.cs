﻿using Kempus.Services.Public.CourseReviewActivity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;

namespace Kempus.Job.Jobs
{
  [DisallowConcurrentExecution]
  public class ClearCourseReviewActivityJob : IJob
  {
    private readonly ILogger<ClearCourseReviewActivityJob> _logger;
    private readonly IServiceProvider _serviceProvider;

    public ClearCourseReviewActivityJob(
      ILogger<ClearCourseReviewActivityJob> logger,
      IServiceProvider serviceProvider
    )
    {
      _logger = logger;
      _serviceProvider = serviceProvider;
    }

    public async Task Execute(IJobExecutionContext context)
    {
      _logger.LogInformation("Start ClearCourseReviewActivityJob");
      try
      {
        await HandleClearCourseReviewActivity();
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message);
      }
      _logger.LogInformation("End ClearCourseReviewActivityJob");
    }

    private async Task HandleClearCourseReviewActivity()
    {
      try
      {
        using (IServiceScope scope = _serviceProvider.CreateScope())
        {
          ICourseReviewActivityService _courseReviewActivity =
            scope.ServiceProvider.GetRequiredService<ICourseReviewActivityService>();
          _courseReviewActivity.ClearAllActivity();
        }
      }
      catch (Exception ex)
      {
        _logger.LogError($"Error when clear course review activity", ex);
      }
    }
  }
}

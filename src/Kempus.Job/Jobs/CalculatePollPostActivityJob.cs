﻿using Kempus.Core.Models;
using Kempus.Models.Public.School;
using Kempus.Services.Public.PostPollActivitySummary;
using Kempus.Services.Public.School;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;

namespace Kempus.Job.Jobs
{
  [DisallowConcurrentExecution]
  public class CalculatePollPostActivityJob : IJob
  {
    private readonly ILogger<CalculatePollPostActivityJob> _logger;
    private readonly IServiceProvider _serviceProvider;

    public CalculatePollPostActivityJob(
      ILogger<CalculatePollPostActivityJob> logger,
      IServiceProvider serviceProvider
    )
    {
      _logger = logger;
      _serviceProvider = serviceProvider;
    }
    public async Task Execute(IJobExecutionContext context)
    {
      _logger.LogInformation("Start CalculatePollPostActivityJob");
      try
      {
        await HandleCalculatePostPollActivity();
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message);
      }
      _logger.LogInformation("End CalculatePollPostActivityJob");
    }

    private async Task HandleCalculatePostPollActivity()
    {
      _logger.LogInformation("Start HandleCalculatePostPollActivity");
      try
      {
        using (IServiceScope scope = _serviceProvider.CreateScope())
        {
          IPostPollActivitySummaryService _postPollActivitySummaryService =
            scope.ServiceProvider.GetRequiredService<IPostPollActivitySummaryService>();
          ISchoolService _schoolService =
            scope.ServiceProvider.GetRequiredService<ISchoolService>();
          var activeSchools = _schoolService.Search(new SchoolFilterModel() { Status = true }, new Pageable() { PageSize = 999 }, false);
          if (!activeSchools.Data.Any()) return;
          foreach (var school in activeSchools.Data)
          {
            try
            {
              _logger.LogInformation($"Start CalculatePostPollActivity for SchoolId = {school.Guid}");
              _postPollActivitySummaryService.CalculatePostPollActivity(school.Guid);
              _logger.LogInformation($"End CalculatePostPollActivity for SchoolId = {school.Guid}");
            }
            catch (Exception ex)
            {
              _logger.LogError($"Error HandleCalculatePostPollActivity for SchoolId = {school.Guid}", ex);
            }

          }
        }
      }
      catch (Exception ex)
      {
        _logger.LogError($"Error HandleCalculatePostPollActivity", ex);
      }

      _logger.LogInformation("Start HandleCalculatePostPollActivity");
    }
  }
}

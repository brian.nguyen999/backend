﻿using Kempus.Services.Public.CourseReviewActivity;
using Kempus.Services.Public.PostPollActivity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;

namespace Kempus.Job.Jobs
{
  public class ClearPostPollActivityJob : IJob
  {
    private readonly ILogger<ClearPostPollActivityJob> _logger;
    private readonly IServiceProvider _serviceProvider;

    public ClearPostPollActivityJob(
      ILogger<ClearPostPollActivityJob> logger,
      IServiceProvider serviceProvider
    )
    {
      _logger = logger;
      _serviceProvider = serviceProvider;
    }

    public async Task Execute(IJobExecutionContext context)
    {
      _logger.LogInformation("Start ClearPostPollActivityJob");
      try
      {
        await HandleClearPostPollActivity();
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message);
      }
      _logger.LogInformation("End ClearPostPollActivityJob");
    }

    private async Task HandleClearPostPollActivity()
    {
      try
      {
        using (IServiceScope scope = _serviceProvider.CreateScope())
        {
          IPostPollActivityService _postPollActivityService =
            scope.ServiceProvider.GetRequiredService<IPostPollActivityService>();
          _postPollActivityService.ClearLast7DaysActivities();
        }
      }
      catch (Exception ex)
      {
        _logger.LogError($"Error when ClearPostPollActivityJob", ex);
      }
    }
  }
}
﻿using Amazon.SQS.Model;
using Kempus.Models.Common;
using Kempus.Services.Common.Queue;
using Kempus.Services.FullTextSearch.OpenSearchEvent;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Quartz;

namespace Kempus.Job.Jobs
{
  [DisallowConcurrentExecution]
  public class OpenSearchEventWorkerJob : IJob
  {
    private readonly ILogger<OpenSearchEventWorkerJob> _logger;
    private readonly IServiceProvider _serviceProvider;
    private readonly IOpenSearchEventSubscriber _openSearchEventSubscriber;

    public OpenSearchEventWorkerJob(
      ILogger<OpenSearchEventWorkerJob> logger,
      IServiceProvider serviceProvider,
      IOpenSearchEventSubscriber openSearchEventSubscriber)
    {
      _logger = logger;
      _serviceProvider = serviceProvider;
      _openSearchEventSubscriber = openSearchEventSubscriber;
    }

    public async Task Execute(IJobExecutionContext context)
    {
      _logger.LogInformation("OpenSearch Sqs Event Worker is running.");
      var numberOfMessage = 1;
      var waitTimeInSeconds = 0;
      var messages = new ReceiveMessageResponse();
      try
      {
        while (true)
        {
          await Task.Delay(1000);
          _logger.LogInformation("Start polling message");
          messages = await _openSearchEventSubscriber.GetMessageAsync(numberOfMessage, waitTimeInSeconds);
          if (messages.Messages.Count == 0)
          {
            _logger.LogInformation("Skip processing due to no messages found");
            continue;
          }

          _logger.LogInformation("Start process {0} messages", messages.Messages.Count);
          foreach (var message in messages.Messages)
          {
            if (await ProcessMessage(message))
            {
              _logger.LogInformation("Delete message process {0}", message.MessageId);
              await _openSearchEventSubscriber.DeleteMessageAsync(message);
            }
          }
          _logger.LogInformation("End process messages");
        }
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message);
      }
    }

    private async Task<bool> ProcessMessage(Message message)
    {
      _logger.LogInformation($"Message body of {message.MessageId}:");
      try
      {
        var eventMessage = JsonConvert.DeserializeObject<OpenSearchEventMessage>(message.Body);
        using (IServiceScope scope = _serviceProvider.CreateScope())
        {
          IOpenSearchEventService _openSearchEventHandler =
            scope.ServiceProvider.GetRequiredService<IOpenSearchEventService>();
          await _openSearchEventHandler.ProcessEvent(eventMessage);
        }

        return true;
      }
      catch (Exception ex)
      {
        _logger.LogError($"Error when process message {message.MessageId}", ex);
      }

      return false;
    }
  }
}

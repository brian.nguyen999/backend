﻿using Kempus.Services.Admin.BellNotification;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;

namespace Kempus.Job.Jobs
{
  [DisallowConcurrentExecution]
  public class SendScheduledNotificationJob : IJob
  {
    private readonly ILogger<CalculateCourseReviewActivityJob> _logger;
    private readonly IServiceProvider _serviceProvider;

    public SendScheduledNotificationJob(
      ILogger<CalculateCourseReviewActivityJob> logger,
      IServiceProvider serviceProvider
    )
    {
      _logger = logger;
      _serviceProvider = serviceProvider;
    }

    public async Task Execute(IJobExecutionContext context)
    {
      _logger.LogInformation("Start SendScheduledNotificationJob");
      try
      {
        using (IServiceScope scope = _serviceProvider.CreateScope())
        {
          IBellNotificationAdminService bellNotificationAdminService =
            scope.ServiceProvider.GetRequiredService<IBellNotificationAdminService>();
          bellNotificationAdminService.SendScheduledNotification();
        }
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message);
      }
      _logger.LogInformation("End SendScheduledNotificationJob");
    }
  }
}

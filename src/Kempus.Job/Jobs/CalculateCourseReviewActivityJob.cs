﻿using Kempus.Core.Models;
using Kempus.Models.Public.School;
using Kempus.Services.Public.CourseReviewActivitySummary;
using Kempus.Services.Public.School;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;

namespace Kempus.Job.Jobs
{
  [DisallowConcurrentExecution]
  public class CalculateCourseReviewActivityJob : IJob
  {
    private readonly ILogger<CalculateCourseReviewActivityJob> _logger;
    private readonly IServiceProvider _serviceProvider;

    public CalculateCourseReviewActivityJob(
      ILogger<CalculateCourseReviewActivityJob> logger,
      IServiceProvider serviceProvider
    )
    {
      _logger = logger;
      _serviceProvider = serviceProvider;
    }
    public async Task Execute(IJobExecutionContext context)
    {
      _logger.LogInformation("Start CalculateCourseReviewActivityJob");
      try
      {
        await HandleCalculateCourseReviewActivity();
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message);
      }
      _logger.LogInformation("End CalculateCourseReviewActivityJob");
    }

    private async Task HandleCalculateCourseReviewActivity()
    {
      _logger.LogInformation("Start HandleCalculateCourseReviewActivity");
      try
      {
        using (IServiceScope scope = _serviceProvider.CreateScope())
        {
          ICourseReviewActivitySummaryService _courseReviewActivitySummaryService =
            scope.ServiceProvider.GetRequiredService<ICourseReviewActivitySummaryService>();
          ISchoolService _schoolService =
            scope.ServiceProvider.GetRequiredService<ISchoolService>();
          var activeSchools = _schoolService.Search(new SchoolFilterModel() { Status = true }, new Pageable() { PageSize = 999 }, false);
          if (!activeSchools.Data.Any()) return;
          foreach (var school in activeSchools.Data)
          {
            try
            {
              _logger.LogInformation($"Start HandleCalculateCourseReviewActivity for SchoolId = {school.Guid}");
              _courseReviewActivitySummaryService.CalculateCourseReviewActivity(school.Guid);
              _logger.LogInformation($"End HandleCalculateCourseReviewActivity for SchoolId = {school.Guid}");
            }
            catch (Exception ex)
            {
              _logger.LogError($"Error HandleCalculateCourseReviewActivity for SchoolId = {school.Guid}", ex);
            }

          }
        }
      }
      catch (Exception ex)
      {
        _logger.LogError($"Error HandleCalculateCourseReviewActivity", ex);
      }

      _logger.LogInformation("Start HandleCalculateCourseReviewActivity");
    }
  }
}

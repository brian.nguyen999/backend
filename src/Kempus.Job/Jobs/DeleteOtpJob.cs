﻿using Kempus.Services.Public.Otp;
using Microsoft.Extensions.Logging;
using Quartz;

namespace Kempus.Job.Jobs
{
  [DisallowConcurrentExecution]
  public class DeleteOtpJob : IJob
  {
    private readonly ILogger<DeleteOtpJob> _logger;
    private readonly IOtpService _otpService;

    public DeleteOtpJob(
      ILogger<DeleteOtpJob> logger,
      IOtpService otpService
    )
    {
      _logger = logger;
      _otpService = otpService;
    }

    public async Task Execute(IJobExecutionContext context)
    {
      _logger.LogInformation("Begin execute #DeleteOtpJob job.");
      try
      {
        _logger.LogInformation("Job trigger at: " + DateTime.UtcNow.ToString());

        _otpService.DeleteExpiredOtp();
      }
      catch (Exception ex)
      {
        _logger.LogError("Error when execute #DeleteOtpJob:" + ex.ToString());
      }

      _logger.LogInformation("End execute #DeleteOtpJob job.");
    }
  }
}
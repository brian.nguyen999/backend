using System.ComponentModel;

namespace Kempus.Core.Enums
{
  public static class CoreEnums
  {
    public enum UserStatus : byte
    {
      [Description("Active")] Active,
      [Description("Deactivated")] Inactive
    }

    public enum PostType : byte
    {
      Post = 1,
      Poll = 2
    }

    public enum SchoolStatus
    {
      InActive = 0,
      Active = 1,
    }

    public enum PollType : byte
    {
      MCQ,
      SCQ
    }

    public enum OtpType : byte
    {
      [Description("Email")] Email,
      [Description("Phone number")] PhoneNumber
    }

    public enum PollStatus : byte
    {
      OnGoing,
      Completed
    }

    public enum UserType : byte
    {
      [Description("User")] User = 0,
      [Description("Admin")] Admin = 1,
      [Description("Ambassador")] Ambassador = 2
    }

    public enum UserGrading : byte
    {
      A = 1,
      B = 2,
      C = 3,
      D = 4,
      F = 5,
      Pass = 6
    }

    public enum FlagType : byte
    {
      User = 0,
      Review = 1,
      Post = 2,
      Poll = 3,
      Comment = 4,
    }

    public enum FlagStatus : byte
    {
      Pending = 0,
      Actioned = 1,
    }

    public enum BookmarkObjectType : byte
    {
      Post = 1,
      Poll = 2
    }

    public enum StatisticsObjectType : byte
    {
      Post = 1,
      Poll = 2,
      CourseReview = 3
    }

    public enum CourseReviewActivity : byte
    {
      Like = 1,
      View = 2
    }

    public enum PostPollActivity : byte
    {
      Like = 1,
      View = 2,
      Comment = 3
    }

    public enum DepartmentProgramStatus : byte
    {
      [Description("Active")] Active,
      [Description("Deactivated")] Inactive
    }

    public enum CourseReviewStatus : byte
    {
      [Description("Published")] Published = 0,
      [Description("Unpublished")] Unpublished = 1
    }

    public enum BannerStatus : byte
    {
      [Description("Active")] Active = 0,
      [Description("Deactivated")] Deactivated = 1
    }

    public enum BannerUrlType : byte
    {
      [Description("Within Kempus")] WithinKempus = 0,
      [Description("Extenal")] Extenal = 1
    }

    public enum KempTransactionType : byte
    {
      [Description("Writing a review")] WriteCourseReview = 0,
      [Description("Successfully flagging")] SuccessfullyFlagReport = 1,
      [Description("Consecutive login for 3 days")] ConsecutiveLoginFor3Days = 2,
      [Description("Signup successfully")] SignupSuccessfully = 3,
      [Description("New User referred & signed up")] NewUserReferredSignedUp = 4,
      [Description("Unlock review")] UnlockCourseReview = 5,
      [Description("Flag & report a User")] FlagReportUser = 6,
      [Description("Flag & report a Review")] FlagReportReview = 7,
      [Description("Flag & report a Post")] FlagReportPost = 8,
      [Description("Flag & report a Poll")] FlagReportPoll = 9,
      [Description("Flag & report a Comment")] FlagReportComment = 10,
      [Description("Flagged by other user")] FlaggedByOtherUser = 11,
    }

    public enum KempTransactionTitle : byte
    {
      [Description("Flag")] Flag = 0,
      [Description("Activity")] Activity = 1,
      [Description("Bonus")] Bonus = 2,
      [Description("Reward")] Reward = 3,
      [Description("Course Review")] CourseReview = 4,
      [Description("Admin")] Admin = 5
    }

    public enum KempTransactionDescription : byte
    {
      [Description("Written a Course Review ")] WriteCourseReview = 0,
      [Description("Flag successful")] SuccessfullyFlagReport = 1,
      [Description("Consecutive login for 3 days")] ConsecutiveLoginFor3Days = 2,
      [Description("Welcome Gift")] SignupSuccessfully = 3,
      [Description("One of your friends has signed up for Kempus")] NewUserReferredSignedUp = 4,
      [Description("Unlocked an in-depth course review")] UnlockCourseReview = 5,
      [Description("Flag submitted")] FlagReportUser = 6,
      [Description("Flag submitted")] FlagReportReview = 7,
      [Description("Flag submitted")] FlagReportPost = 8,
      [Description("Flag submitted")] FlagReportPoll = 9,
      [Description("Flag submitted")] FlagReportComment = 10,
      [Description("Violating community guidelines")] FlaggedByOtherUser = 11,
      [Description("Bonus from Kempus")] BonusFromKempus = 12, // Refactor later
      [Description("Penalty from Kempus")] PenaltyFromKempus = 13, // Refactor later
    }

    public enum KempTransactionAction : byte
    {
      Add = 0,
      Deduct = 1
    }

    public enum BellNotificationStatus : byte
    {
      Sent = 0,
      Scheduled = 1
    }

    public enum BellNotificationType : byte
    {
      Site = 0,
      CMS = 1
    }

    public enum WebsocketAction : byte
    {
      [Description("New Notification")] NewNotification = 0,
      [Description("New Message To Chat List")] NewMessageToChatList = 1,
      [Description("New Message To Chat Notification")] NewMessageToChatNotification = 2,
      [Description("Conversation Blocked")] ConversationBlocked = 3,
      [Description("Conversation Deleted")] ConversationDeleted = 4,
      [Description("Conversation Accepted")] ConversationAccepted = 5,
    }

    public enum GeneratedType : byte
    {
      ImportedFromCSV = 0,
      ManualInCMS = 1
    }

    public enum ConversationMemberStatus : byte
    {
      Pending = 0,
      Accept = 1,
      Delete = 2,
      Blocked = 3,
    }

    public enum ConversationType : byte
    {
      DirectMessage = 0,
      Group = 1
    }

    public enum ConversationStatus : byte
    {
      Pending = 0,
      OnGoing = 1,
    }
  }
}
﻿using Kempus.Core.Constants;
using Microsoft.AspNetCore.Http;

namespace Kempus.Core
{
  public class SessionStore
  {
    private readonly IHttpContextAccessor _httpContextAccessor;
    private Guid? _schoolId;
    private Guid? _userId;

    public SessionStore(
      IHttpContextAccessor httpContextAccessor
    )
    {
      _httpContextAccessor = httpContextAccessor;
    }

    public SessionStore()
    {
    }

    public Guid? UserId
    {
      get
      {
        if (_userId == null
            && Guid.TryParse(GetClaimValue(CoreConstants.Claims.UserId), out Guid userId))
        {
          _userId = userId;
        }

        return _userId;
      }
    }

    public Guid? SchoolId
    {
      get
      {
        if (_schoolId == null
            && Guid.TryParse(GetClaimValue(CoreConstants.Claims.SchoolId), out Guid schoolId))
        {
          _schoolId = schoolId;
        }

        return _schoolId;
      }
    }

    public void SetTenant(Guid? schoolId)
    {
      _schoolId = schoolId;
    }

    public void SetUserId(Guid? userId)
    {
      _userId = userId;
    }

    private string GetClaimValue(string type)
    {
      return _httpContextAccessor?.HttpContext?.User?.Claims?.FirstOrDefault(c => c.Type == type)?.Value;
    }
  }
}
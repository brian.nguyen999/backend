﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Core.Constants
{
  public class KempTransactionValue
  {
    public const int WriteCourseReview = 3;
    public const int SuccessfullyFlagReport = 3;
    public const int ConsecutiveLoginFor3Days = 1;
    public const int SignupSuccessfully = 3;
    public const int NewUserReferredSignedUp = 3;
    public const int UnlockCourseReview = -1;
    public const int FlagReportUser = -1;
    public const int FlagReportReview = -1;
    public const int FlagReportPost = -1;
    public const int FlagReportPoll = -1;
    public const int FlagReportComment = -1;
    public const int FlaggedByOtherUser = -3;
  }
}

namespace Kempus.Core.Constants
{
  public static class CoreConstants
  {
    public static class SchoolImportTemplate
    {

      public const string FileName = "Import-School-Template.csv";
      public const string S3Folder = "schools/Template";
      public const string LocalFolder = "App_Data/TemplateFiles";
    }

    public static class SwaggerGroupApi
    {
      public const string AdminApi = "AdminApi";
      public const string PublicApi = "PublicApi";
    }

    public static class HttpRequestMediaType
    {
      public const string Json = "application/json";
    }

    public static class Claims
    {
      public const string UserId = "UserId";
      public const string SchoolId = "SchoolId";
      public const string UserType = "UserType";
      public const string ReferralCode = "ReferralCode";
      public const string PermissionIds = "PermissionIds";
      public const string ModuleApplicationIds = "ModuleApplicationIds";
    }

    public static class Headers
    {
      public const string KempusInternalApiKey = "KempusInternalApiKey";
      public const string KempusExternalApiKey = "KempusExternalApiKey";
    }

    public static class RegexConstant
    {
      public const string PasswordRegularExpression =
        "^(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,16}$";
    }

    public static class JsonData
    {
      public const string EmptyArray = "[]";
    }

    public static class ParentModuleApplicationId
    {
      public const int Accounts = 1;
      public const int Forum = 2;
    }

    public static class ModuleApplicationId
    {
      public const int Accounts = 1;
      public const int Forum = 2;
      public const int User = 3;
      public const int SendNotification = 4;
      public const int Admin = 5;
      public const int AdminSettings = 6;
      public const int Posts = 7;
      public const int Polls = 8;
      public const int CourseReviews = 9;
      public const int Transaction = 10;
      public const int Flags = 11;
      public const int SchoolData = 12;
      public const int BannerContent = 13;
    }

    public static class ObjectTypeId
    {
      public static byte Post = 1;
      public static byte Poll = 2;
      public static byte CourseReview = 3;
    }
    public static class UserAction
    {
      public const int Create = 0;
      public const int Update = 1;
      public const int Delete = 2;
    }


    public static class PermissionName
    {
      public const string View = "Able to view data";
      public const string CreateEdit = "Able to create or edit data";
    }

    public static class PolicyName
    {
      public const string AdminPolicy = "AdminPolicy";
      public const string UserPolicy = "UserPolicy";
    }

    public static class EmailTemplate
    {
      public const string SEND_OTP = "SEND_OTP";
      public const string CONFIRM_RESET_PASSWORD = "CONFIRM_RESET_PASSWORD";
      public const string CMS_CONFIRM_RESET_PASSWORD = "CMS_CONFIRM_RESET_PASSWORD";
      public const string CMS_SEND_MAIL_WAITLIST = "CMS_SEND_MAIL_WAITLIST";
      public const string SITE_CONFIRM_RESET_PASSWORD = "SITE_CONFIRM_RESET_PASSWORD";
    }

    public static class SeedingDefaultData
    {
      public const string SchoolId = "a8c5f021-4f6c-11ed-ba4c-02c3445b6529";
      public const string AdminId = "36a4cb3a-72b4-49e2-a302-f360ffd805f7";
      public const string InstructorId = "a11ecfea-7096-482a-bf3d-913d08cdb31f";
      public const string InstructorId2 = "308797fc-0dab-42d5-8799-376d58711850";
      public const string InstructorId3 = "d49019c8-7e0e-43a5-9fb7-50a55f765898";
      public const string CourseId = "dfed759e-5a6a-4e8d-9496-9d480c9d6d3f";
      public const string CourseId2 = "1198640e-80c3-45bb-bec6-1f936e39a6bb";
      public const string CourseId3 = "64ac6285-12d3-403c-8c63-4cb31aa4843c";
      public const string CourseInstructorId = "4e88c8fa-a603-4415-9186-0d8de0f7d0ea";
    }

    public static List<string> InstructorCharacteristics = new()
        {
            "GIVES_GOOD_FEEDBACK",
            "INSPIRATIONAL",
            "HILARIOUS",
            "CARING",
            "RESPECTED",
            "AMAZING_LECTURES"
        };

    public static class OpenSearchIndex
    {
      public const string Courses = "courses";
      public const string CourseReviews = "course-reviews";
    }

    public static class OpenSearchEventType
    {
      public static class Course
      {
        public const string Create = "course.created";
        public const string Update = "course.updated";
        public const string Delete = "course.deleted";
      }

      public static class CourseReview
      {
        public const string Create = "coursereview.created";
        public const string Update = "coursereview.updated";
        public const string Delete = "coursereview.deleted";
      }
    }

    public static class UserTracking
    {
      public static class Platform
      {
        public const string Web = "Web";
        public const string Mobile = "Mobile";

      }

      public static class ActionName
      {
        public const string WritePost = "WRITE_POST";
        public const string CreatePoll = "CREATE_POLL";
        public const string WriteCourseReview = "WRITE_COURSE_REVIEW";
        public const string LikePost = "LIKE_POST";
        public const string LikePoll = "LIKE_POLL";
        public const string LikeCourseReview = "LIKE_COURSE_REVIEW";
        public const string LoginSuccess = "LOGIN_SUCCESS";
        public const string LoginFailed = "LOGIN_FAILED";
        public const string Logout = "LOGOUT";
        public const string Bookmark = "BOOKMARK";
        public const string Report = "REPORT";
        public const string Comment = "COMMENT";
        public const string LockedByAdmin = "LOCKED_BY_ADMIN";
      }

      public static class ObjectType
      {
        public const string Post = "Post";
        public const string Poll = "Poll";
        public const string CourseReview = "CourseReview";
      }
    }
  }

  public static class HiddenStatus
  {
    public const string Public = "Public";
    public const string Hidden = "Hidden";
  }

  public static class RedisCacheKeyDelimeter
  {
    public const string Delimeter = "__";
  }

  public static class SchoolEmailFormat
  {
    public const char Delimeter = ',';
  }

  public static class RedisCacheKey
  {
    public const string School_TotalSchoolCount = "school-total-count";
    public const string School_SchoolList = "school-list";
  }

  public static class ChatEventName
  {
    public const string NEW_MESSAGE = "NEW_MESSAGE";
  }
}
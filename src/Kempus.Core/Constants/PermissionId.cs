﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Core.Constants
{
  public static class PermissionId
  {
    // Account
    public const int AccountView = 1;
    public const int AccountCreateEdit = 2;

    // Forum
    public const int ForumView = 3;
    public const int ForumCreateEdit = 4;

    // User
    public const int UserView = 5;
    public const int UserCreateEdit = 6;

    // Send Notification
    public const int SendNotificationView = 7;
    public const int SendNotificationCreateEdit = 8;

    // Admin
    public const int AdminView = 9;
    public const int AdminCreateEdit = 10;

    // Admin Settings
    public const int AdminSettingsView = 11;
    public const int AdminSettingsCreateEdit = 12;

    // Posts
    public const int PostsView = 13;
    public const int PostsCreateEdit = 14;

    // Polls
    public const int PollsView = 15;
    public const int PollsCreateEdit = 16;

    // Course Reviews
    public const int CourseReviewsView = 17;
    public const int CourseReviewsCreateEdit = 18;

    // Transaction
    public const int TransactionView = 19;
    public const int TransactionCreateEdit = 20;

    // Flags
    public const int FlagsView = 21;
    public const int FlagsCreateEdit = 22;

    // School Data
    public const int SchoolDataView = 23;
    public const int SchoolDataCreateEdit = 24;

    // Banner Content
    public const int BannerContentView = 25;
    public const int BannerContentCreateEdit = 26;
  }
}
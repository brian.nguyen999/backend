﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Core.Models.ModelContracts
{
  public interface IFullAuditedEntity<TUserKey> : ICreationAudited<TUserKey>, IDeletionAudited<TUserKey>,
    IModificationAudited<TUserKey>
  {
  }
}
﻿namespace Kempus.Core.Models.ModelContracts
{
  public interface IHasActive
  {
    public bool IsActivated { get; set; }
  }
}
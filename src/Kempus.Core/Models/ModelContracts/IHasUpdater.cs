﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Core.Models.ModelContracts
{
  public interface IHasUpdater<TUserKey>
  {
    public TUserKey? UpdatedBy { get; set; }
  }
}
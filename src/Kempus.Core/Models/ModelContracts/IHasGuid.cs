﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Core.Models.ModelContracts
{
  public interface IHasGuid
  {
    public Guid Guid { get; set; }
  }
}
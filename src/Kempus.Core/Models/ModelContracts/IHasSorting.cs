﻿namespace Kempus.Core.Models.ModelContracts
{
  public interface IHasSorting
  {
    public string SortedField { get; set; }
    public bool IsDescending { get; set; }
  }
}
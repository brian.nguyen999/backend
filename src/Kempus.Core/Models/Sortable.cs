﻿namespace Kempus.Core.Models
{
  public class Sortable
  {
    public string FieldName { get; set; }
    public bool IsDescending { get; set; }

    public Sortable(string fieldName, bool isDescending)
    {
      FieldName = fieldName;
      IsDescending = isDescending;
    }

    public Sortable()
    {
    }
  }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kempus.Core.Models.ModelContracts;

namespace Kempus.Core.Models
{
  public abstract class FullAuditedEntity<TPrimaryKey, TUserKey> : FullAudited<TUserKey>, IHasGuid
  {
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Key, Column(Order = 0)]
    public TPrimaryKey Id { get; set; }

    [Column(Order = 1)] public Guid Guid { get; set; }
  }
}
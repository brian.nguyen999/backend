﻿using Kempus.Core.Models.ModelContracts;
using System.ComponentModel.DataAnnotations;

namespace Kempus.Core.Models
{
  public class Entity<TPrimaryKey> : IHasGuid
  {
    [Key] public TPrimaryKey Id { get; set; }

    public Guid Guid { get; set; } = Guid.NewGuid();
  }
}
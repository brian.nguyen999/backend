﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Core.Models.Configuration
{
  public class UserConfig
  {
    public long NickNameLockdownMinutes { get; set; }
    public int MaxInviteFriend { get; set; }
    public int OtpValidityInMinutes { get; set; }
  }
}
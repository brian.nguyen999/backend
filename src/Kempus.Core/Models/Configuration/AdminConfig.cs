﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Core.Models.Configuration
{
  public class AdminConfig
  {
    public int MaxBannerPerSchool { get; set; }
  }
}

﻿using Kempus.Core.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Core.Models.Configuration
{
  public class PrivateAppInfo
  {
    public string KempusInternalApiKey { get; set; }
    public string KempusExternalApiKey { get; set; }

    public string ApiKey(string headerName)
    {
      if (CoreConstants.Headers.KempusInternalApiKey == headerName) return KempusInternalApiKey;
      if (CoreConstants.Headers.KempusExternalApiKey == headerName) return KempusExternalApiKey;
      return null;
    }
  }
}
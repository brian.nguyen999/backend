﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Core.Models.Configuration
{
  public class AwsConfig
  {
    public SESConfig SES { get; set; }

    public SqsConfig Sqs { get; set; }

    public S3Config S3 { get; set; }

    public RedisConfig Redis { get; set; }

    public ApiGatewayConfig ApiGateway { get; set; }
  }

  public class SESConfig
  {
    public string MailFrom { get; set; }
    public string MailName { get; set; }
    public string SMTPUsername { get; set; }
    public string SMTPPassword { get; set; }
    public int Port { get; set; }
    public string Host { get; set; }
    public string SendTo { get; set; }
  }

  public class SqsConfig
  {
    public string AccessKey { get; set; }

    public string SecretKey { get; set; }

    public string Region { get; set; }

    public Queue Queues { get; set; }

    public class Queue
    {
      public string OpenSearchDataSyncQueueUrl { get; set; }
    }
  }

  public class S3Config
  {
    public string AccessKeyId { get; set; }
    public string SecretAccessKey { get; set; }
    public string Region { get; set; }
    public string Bucket { get; set; }
    public string PublicBucket { get; set; }
    public string PublicDomain { get; set; }
    public long MaximumFileSize { get; set; }
  }

  public class RedisConfig
  {
    public string Endpoint { get; set; }
    public int Port { get; set; }
    public string Password { get; set; }
  }
  public class ApiGatewayConfig
  {
    public string AccessKeyId { get; set; }
    public string SecretAccessKey { get; set; }
    public string WebSocketEndpoint { get; set; }
  }
}
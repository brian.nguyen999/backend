﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Core.Models.Configuration
{
  public class DomainConfig
  {
    public string CMS { get; set; }
    public string Site { get; set; }
    public Pages Pages { get; set; }
  }

  public class Pages
  {
    public string ResetPassword { get; set; }
    public string ForgotPassword { get; set; }
    public string SignUp { get; set; }
  }
}

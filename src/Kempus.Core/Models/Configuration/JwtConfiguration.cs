﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Core.Models.Configuration
{
  public class JwtConfiguration
  {
    public string Secret { get; set; }
    public string AccessTokenValidityInMinutes { get; set; }
    public string RefreshTokenValidityInMinutes { get; set; }
  }
}
﻿using Kempus.Core.Models.ModelContracts;
using System.ComponentModel.DataAnnotations;

namespace Kempus.Core.Models
{
  public class Pageable : IHasPaging, IHasSorting
  {
    [Range(1, Int32.MaxValue)]
    public int PageSize { get; set; } = 10;

    [Range(1, Int32.MaxValue)]
    public int PageIndex { get; set; } = 1;

    public int Offset
    {
      get { return (int)((PageIndex - 1) * PageSize); }
    }

    public string? SortedField { get; set; }
    public bool IsDescending { get; set; }

    public static Pageable GetPageable(int? pageSize, int? pageIndex, string? sortedField, bool isDescending)
    {
      var model = new Pageable
      {
        IsDescending = isDescending,
      };
      model.PageSize = pageSize ?? 10;
      model.PageIndex = pageIndex ?? 1;
      if (!string.IsNullOrEmpty(sortedField)) model.SortedField = sortedField;

      return model;
    }

    public List<Sortable> GetSortables(string defaultFieldName)
    {
      var sortables = new List<Sortable>();

      if (!string.IsNullOrWhiteSpace(SortedField))
      {
        sortables.Add(new Sortable
        {
          FieldName = SortedField,
          IsDescending = IsDescending
        });

        return sortables;
      }

      if (!string.IsNullOrWhiteSpace(defaultFieldName))
      {
        sortables.Add(new Sortable
        {
          FieldName = defaultFieldName,
          IsDescending = true
        });
      }

      return sortables;
    }

    public List<Sortable> GetSortables()
    {
      var sortables = new List<Sortable>();

      if (!string.IsNullOrWhiteSpace(SortedField))
      {
        sortables.Add(new Sortable
        {
          FieldName = SortedField,
          IsDescending = IsDescending
        });
      }

      return sortables;
    }
  }
}
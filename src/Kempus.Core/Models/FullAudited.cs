﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kempus.Core.Models.ModelContracts;

namespace Kempus.Core.Models
{
  public abstract class FullAudited<TKey> : Entity<long>, IFullAuditedEntity<TKey>
  {
    public virtual DateTime CreatedDate { get; set; } = DateTime.UtcNow;
    public virtual TKey CreatedBy { get; set; }
    public virtual bool IsDeleted { get; set; }
    public virtual DateTime? DeletedDate { get; set; }
    public virtual TKey? DeletedBy { get; set; }
    public virtual DateTime? UpdatedDate { get; set; }
    public virtual TKey? UpdatedBy { get; set; }
  }
}
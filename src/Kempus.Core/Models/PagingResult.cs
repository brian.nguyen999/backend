﻿namespace Kempus.Core.Models
{
  public class PagingResult<T>
  {
    public int PageIndex { get; set; }
    public int PageSize { get; set; }
    public long TotalPages { get; set; }
    public long TotalCount { get; set; }
    public List<T> Data { get; set; }

    public PagingResult()
    {

    }

    public PagingResult(List<T> data, long totalCount, int pageIndex, int pageSize)
    {
      PageIndex = pageIndex;
      TotalPages = (int)Math.Ceiling(totalCount / (double)pageSize);
      Data = data;
      PageSize = pageSize;
      TotalCount = totalCount;
    }

    public PagingResult(long totalCount, int pageIndex, int pageSize)
    {
      PageIndex = pageIndex;
      TotalPages = (int)Math.Ceiling(totalCount / (double)pageSize);
      Data = new List<T>();
      PageSize = pageSize;
      TotalCount = totalCount;
    }
  }
}
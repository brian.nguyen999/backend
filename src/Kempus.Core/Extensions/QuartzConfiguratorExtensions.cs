﻿using Microsoft.Extensions.Configuration;
using Quartz;

namespace Kempus.Core.Extensions
{
  public static class QuartzConfiguratorExtensions
  {
    public static void AddJobAndTrigger<T>(
      this IServiceCollectionQuartzConfigurator quartz,
      IConfiguration config)
      where T : IJob
    {
      // Use the name of the IJob as the appsettings.json key
      string jobName = typeof(T).Name;

      // Try and load the schedule from configuration
      var configKey = $"Quartz:{jobName}";
      var cronSchedule = config[$"{configKey}:Cron"];
      var isEnable = Convert.ToBoolean(config[$"{configKey}:IsEnabled"]);

      // Some minor validation
      if (!isEnable || string.IsNullOrEmpty(cronSchedule))
        return;

      // register the job as before
      var jobKey = new JobKey(jobName);
      quartz.AddJob<T>(opts => opts.WithIdentity(jobKey));

      quartz.AddTrigger(opts => opts
        .ForJob(jobKey)
        .WithIdentity(jobName + "-trigger")
        .WithCronSchedule(cronSchedule)); // use the schedule from configuration
    }
  }
}
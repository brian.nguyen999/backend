﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Core.Utils
{
  public class FileUtilities
  {
    public class FileHelper
    {
      public static bool IsCsvFile(string contentType)
      {
        switch (contentType.ToLower())
        {
          case "text/csv":
            return true;
          default:
            return false;
        }
      }

      public static bool IsImageFile(string contentType)
      {
        switch (contentType?.ToLower())
        {
          case "image/apng":
          case "image/heic":
          case "image/gif":
          case "image/jpeg":
          case "image/png":
            return true;
          default:
            return false;
        }
      }
    }
  }
}

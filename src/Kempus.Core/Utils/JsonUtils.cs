﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using ErrorEventArgs = Newtonsoft.Json.Serialization.ErrorEventArgs;

namespace Kempus.Core.Utils
{
  public static class JsonUtils
  {
    public static T Convert<T>(string json)
    {
      List<string> errors = new();

      return JsonConvert.DeserializeObject<T>(json, new JsonSerializerSettings
      {
        Error = delegate(object sender, ErrorEventArgs args)
        {
          errors.Add(args.ErrorContext.Error.Message);
          args.ErrorContext.Handled = true;
        }
      });
    }

    public static string ToJson(object data)
    {
      return JsonConvert.SerializeObject(data, new JsonSerializerSettings
      {
        ContractResolver = new CamelCasePropertyNamesContractResolver()
      });
    }
  }
}
﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Core.Utils
{
  public static class StringHelper
  {
    public static string? GenerateReferralCodeFromEmail(string email)
    {
      return email?.Replace("@", "_");
    }

    public static string? GenerateEmailFromReferralCode(string referralCode)
    {
      if (string.IsNullOrWhiteSpace(referralCode) || !referralCode.Contains('_')) return referralCode;
      int Place = referralCode.LastIndexOf('_');
      string result = referralCode.Remove(Place, 1).Insert(Place, "@");
      return result;
    }

    public static List<string> GetInstructorEntityFromFileUpload(string rawName)
    {
      List<string> result = new List<string>();

      if (string.IsNullOrEmpty(rawName)) return result;
      rawName = rawName.Trim();

      var listInstructor = rawName.Split(';');
      if (listInstructor != null)
      {
        foreach (var instructorName in listInstructor)
        {
          var name = instructorName.Trim().Split(',');
          if (name != null && name.Length >= 2)
          {
            result.Add($"{name[1].Trim()} {name[0].Trim()}");
          }
          else
          {
            result.Add(instructorName.Trim());
          }
        }
      }

      return result;
    }

    public static bool ContainsHTMLElements(string text)
    {
      HtmlDocument doc = new HtmlDocument();
      doc.LoadHtml(text);
      return !HtmlIsJustText(doc.DocumentNode);
    }

    private static bool HtmlIsJustText(HtmlNode rootNode)
    {
      return rootNode.Descendants().All(n => n.NodeType == HtmlNodeType.Text);
    }
  }
}
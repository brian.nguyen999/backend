﻿namespace Kempus.Core.Errors
{
  public class NotAcceptableException : Exception
  {
    public NotAcceptableException(string message) : base(message)
    {
    }
  }
}
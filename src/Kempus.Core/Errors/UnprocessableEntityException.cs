﻿using AutoWrapper.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Core.Errors
{
  public class UnprocessableEntityException : ApiException
  {
    public UnprocessableEntityException(IEnumerable<ValidationError> errors) : base(errors)
    {
    }
  }
}
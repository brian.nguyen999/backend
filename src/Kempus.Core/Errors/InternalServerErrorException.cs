﻿namespace Kempus.Core.Errors
{
  public class InternalServerErrorException : Exception
  {
    public InternalServerErrorException(string message) : base(message)
    {
    }
  }
}
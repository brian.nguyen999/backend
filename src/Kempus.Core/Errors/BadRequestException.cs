﻿using AutoWrapper.Wrappers;

namespace Kempus.Core.Errors
{
  /// <summary>
  ///  This is custom UserFriendlyException to have custom HTTP Status Code, Error code and Error code message
  /// </summary>
  public class BadRequestException : ApiException
  {
    public BadRequestException(string message) : base(message)
    {
    }
  }
}
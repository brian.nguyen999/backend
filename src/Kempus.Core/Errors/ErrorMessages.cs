namespace Kempus.Core.Errors
{
  public static class ErrorMessages
  {
    public const string Invalid_Request = "Your request is invalid.";
    public const string NoPermission = "You don't have permission to access this page.";
    // User
    public const string User_EmailHasTaken = "Email has already been taken.";
    public const string User_EmailInvalid = "Email is not valid.";
    public const string User_UsernameHasTaken = "Username has already been taken.";
    public const string User_NotFound = "User does not exist or deactivate.";
    public const string User_NickNameHasTaken = "Nick name has already been taken.";
    public const string User_OtpInvalid = "OTP code is not valid or expired.";
    public const string User_OtpStillValid = "The old OTP code is still valid.";

    public const string User_IsNotVerifyEmail =
      "Your email address has not been verified. Verify your email address.";

    public const string User_FailedLogin = "Incorrect username or password.";
    public const string User_SchoolInActive = "This school is currently inactive. Please come back later.";

    public const string User_ReferralCode_FailedToGenerate = "Failed to generate Referral Code, please try again.";
    public const string User_NickName_FailedToGenerate = "Failed to generate Nick name, please try again.";
    public const string User_TokenExpired = "Token is expired.";
    public const string User_RequestDenied = "Request denied. Unauthorized access.";
    public const string User_DomainIsNotSupported = "This domain is not a supported school domain.";
    public const string User_ReferralCodeInvalid = "Referral code is not valid.";
    public const string User_ReferralCodeRequried = "Referral code is required.";

    public const string User_ReferralCodeExceedNumberOfInvite =
      "Referral code has exceeded the allowed number of invitations.";

    public const string User_SchoolDomainNotMatchWithRefferer =
      "This referral code does not match with this School.";

    public const string User_NickNameIsLockedForDays = "Nickname is locked for {0} days.";
    public const string User_DegreeIsLockedForDays = "Degree is locked for {0} days.";
    public const string User_MajorIsLockedForDays = "Major is locked for {0} days.";
    public const string User_ClassYearIsLockedForDays = "ClassYear is locked for {0} days.";
    public const string User_NickNameIsLockedForHours = "Nickname is locked for {0} hours.";
    public const string User_DegreeIsLockedForHours = "Degree is locked for {0} hours.";
    public const string User_MajorIsLockedForHours = "Major is locked for {0} hours.";
    public const string User_ClassYearIsLockedForHours = "ClassYear is locked for {0} hours.";
    public const string User_PermissionDenied = "Edit permission denied.";

    // Validation message
    public const string Register_Username_Length = "Between 4-16 characters.";
    public const string Register_Password_Length = "Between 6-16 characters.";
    public const string Register_Password_RegularExpression = "At least 1 special characters, and one number.";

    // Topic
    public const string Topic_NotFound = "Topic does not exist or has been deleted.";

    // Post
    public const string Post_NotFound = "Post does not exist or has been deleted.";
    public const string Post_KeywordsInvalid = "Post keywords must have at least 1 keyword and maximum 3 keywords.";
    public const string Post_UnableEdit = "Cannot edit post after pub`ished more than {0} minutes.";
    public const string Post_UnableEditOtherPeoplePost = "Can not edit other people's post.";
    public const string Post_DescriptionMaxLength = "Description can only be 10,000 characters long.";
    public const string Post_TitleMaxLenght = "Title can only be 300 characters long.";

    // Poll
    public const string Poll_NotFound = "Poll does not exists or has been deleted.";
    public const string Poll_OptionInvalid = "Poll option must have at least 1 option and maximum 5 options.";
    public const string Poll_KeywordsInvalid = "Poll keywords must have at least 1 keyword and maximum 3 keywords.";
    public const string Poll_UnableEdit = "Cannot edit poll after published more than {0} minutes.";
    public const string Poll_UnableEditOtherPeoplePoll = "Can not edit other people's poll.";
    public const string Poll_DescriptionMaxLength = "Description can only be 10,000 characters long.";
    public const string Poll_SingleChoiceCannotHaveMultipleOptions = "Poll with single choice cannot have multiple options.";
    public const string Poll_TitleMaxLenght = "Title can only be 300 characters long.";

    // Otp
    public const string Otp_ObjectInvalid = "{0} is not valid.";

    // Role
    public const string Role_Existed = "This role is already exist.";
    public const string Role_Notfound = "Role does not exists or has been deleted.";

    // Course
    public const string Course_NotFound = "Course does not exists or has been deleted.";

    // Course Review
    public const string CourseReview_NotFound = "Course review does not exists or has been deleted.";
    public const string CourseReview_DifficultRatingInvalidRange = "DifficultRating must between 1 and 5.";
    public const string CourseReview_HoursSpendInvalidRange = "HoursSpend must be positive number.";
    public const string CourseReview_NumberOfExamsInvalidRange = "NumberOfExams must between 1 and 3.";
    public const string CourseReview_UserGradeInvalidRange = "UserGrade must be A/B/C/D/F/Pass.";
    public const string CourseReview_UserAchievementRatingInvalidRange = "UserAchievementRating must between 1 and 5.";
    public const string CourseReview_RecommendRatingInvalidRange = "RecommendRating must between 1 and 5.";
    public const string CourseReview_GradingCriteriaRatingInvalidRange = "GradingCriteriaRating must between 1 and 5.";

    public const string CourseReview_CommenInvalidLenght =
      "The field Comment must be a string with a minimum length of 100.";

    public const string CourseReview_CharacteristicsInvaliValue = "Characteristics contains invalid value.";

    // Post Comment
    public const string Comment_NotFound = "Comment does not exists or has been deleted.";
    public const string Comment_Required = "Please input a comment.";
    public const string Comment_NotMatchWithParentInSamePost = "Comment and parent is not in the same Post.";
    public const string Comment_NotMatchWithParentInSamePoll = "Comment and parent is not in the same Poll.";

    // School
    public const string School_NotFound = "School does not exists or has been deleted.";
    public const string School_ExistedEmailFormat = "Email format is already exist: {0}";
    public const string School_InActive = "This school is currrently in Waitlist. Please try later.";
    public const string School_Upload_UnsupportedFile = "Unsupported file {0}.";
    public const string School_Upload_FileNameTooLong = "File name cannot be more than {0} characters.";
    public const string School_Upload_FileSizeTooBig = "Cannot upload files larger than 10MB.";
    public const string School_Upload_InvalidFileFormat = "Invalid file format.";
    public const string School_Upload_EmptyValue = "Empty cells at row {0} are not allowed.";
    public const string School_Upload_InvalidValue_HtmlTag = "Html tag at row {0} are not allowed.";
    public const string School_Upload_InvalidValue = "Cell at row {0} has invalid value.";
    public const string School_ExistedName = "School name is already exists.";
    public const string School_InvalidEmailFormat = "Email format is invalid: {0}";

    // Admin
    public const string Admin_NotFound = "User does not exist.";

    // Topic
    public const string Topic_NameExisted = "Topic name already exists.";

    // Banner
    public const string Banner_NotFound = "Banner does not exists or has been deleted.";
    public const string Banner_Upload_UnsupportedFile = "Unsupported file.";
    public const string Banner_Upload_FileNameTooLong = "File name cannot be more than {0} characters.";
    public const string Banner_Upload_FileSizeTooBig = "Cannot upload files larger than 10MB.";
    public const string Banner_Upload_MaxQuatity = "Limit 10 banner per school.";

    // Major
    public const string Major_NameExisted = "Major name already exists.";
    public const string Major_NotFound = "Major does not exist.";

    // Bell Notification
    public const string BellNotificationUser_NotFound = "Notification of user does not exist or has been deleted.";
    public const string BellNotification_NotFound = "Notification does not exist or has been deleted.";
    public const string BellNotification_ScheduledDate_ShouldBeLater_CurrentDate = "Scheduled date should be later than current date.";
    public const string BellNotification_ScheduledDate_CannotNull = "Please input Scheduled date.";
    public const string BellNotification_NotFoundUserToSend = "Not found any users to send notification.";

    // Instructor Review
    public const string Instructor_NotFound = "Instructor does not exists or has been deleted.";

    // DepartmentProgram Review
    public const string DepartmentProgram_NotFound = "Department/Program does not exists or has been deleted.";

    // Flag
    public const string Flag_NotFound = "Flag does not exist.";
    public const string Flag_ActionedFlag = "Can not edit actioned flag.";
    public const string Flag_NotEnoughKemp = "Not enough Kemp to flag.";

    // Conversation
    public const string Conversation_ErrorSameUserId = "You cannot create conversation with yourself.";
    public const string Conversation_NotFound = "Conversation does not exist or has been deleted.";
    public const string Conversation_ReachNumberOfPendingMessage = "You have reached a limit for sending message.";
    public const string Conversation_ErrorBlocked = "Unable to interact.";
    public const string Conversation_RequestNotFound = "Request does not exist or has been deleted.";

    // Conversation Member
    public const string ConversationMember_NotFound = "Conversation member does not exist or has been deleted.";
  }
}
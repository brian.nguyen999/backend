﻿using Microsoft.EntityFrameworkCore;

namespace Kempus.EntityFramework
{
  public class ReplicaDbContext : ApplicationDbContext
  {
    public ReplicaDbContext(
      DbContextOptions<ReplicaDbContext> options
    ) : base(options)
    {
    }

    public ReplicaDbContext()
    {
    }
  }
}
﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Kempus.EntityFramework.Migrations
{
    public partial class Add_SchoolId_For_Chat_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "SchoolId",
                table: "Messages",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                collation: "ascii_general_ci");

            migrationBuilder.AddColumn<Guid>(
                name: "SchoolId",
                table: "Conversations",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                collation: "ascii_general_ci");

            migrationBuilder.AddColumn<Guid>(
                name: "SchoolId",
                table: "ConversationMembers",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                collation: "ascii_general_ci");

            migrationBuilder.CreateIndex(
                name: "IX_Messages_SchoolId",
                table: "Messages",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Conversations_SchoolId",
                table: "Conversations",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_ConversationMembers_SchoolId",
                table: "ConversationMembers",
                column: "SchoolId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Messages_SchoolId",
                table: "Messages");

            migrationBuilder.DropIndex(
                name: "IX_Conversations_SchoolId",
                table: "Conversations");

            migrationBuilder.DropIndex(
                name: "IX_ConversationMembers_SchoolId",
                table: "ConversationMembers");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Messages");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Conversations");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "ConversationMembers");
        }
    }
}

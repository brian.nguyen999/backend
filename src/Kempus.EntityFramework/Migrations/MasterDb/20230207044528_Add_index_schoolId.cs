﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Kempus.EntityFramework.Migrations
{
    public partial class Add_index_schoolId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_SchoolLifeTimeAnalytics_SchoolId",
                table: "SchoolLifeTimeAnalytics",
                column: "SchoolId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_SchoolLifeTimeAnalytics_SchoolId",
                table: "SchoolLifeTimeAnalytics");
        }
    }
}

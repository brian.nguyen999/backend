﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Kempus.EntityFramework.Migrations
{
    public partial class Alter_Blocktable_AddSoftDelete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Blocks_IsDeleted",
                table: "Blocks",
                column: "IsDeleted");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Blocks_IsDeleted",
                table: "Blocks");
        }
    }
}

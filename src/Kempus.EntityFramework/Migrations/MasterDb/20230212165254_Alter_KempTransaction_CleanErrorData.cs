﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Kempus.EntityFramework.Migrations
{
    public partial class Alter_KempTransaction_CleanErrorData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DROP TEMPORARY TABLE IF EXISTS tmpTableTransactionGuid;
                                  DROP TEMPORARY TABLE IF EXISTS tmpTableUserId;
                                  DROP TEMPORARY TABLE IF EXISTS tmpTableTransactionCreatedDate;

                                  CREATE TEMPORARY TABLE IF NOT EXISTS tmpTableTransactionGuid AS (
                                  SELECT Guid, UserId, date(CreatedDate), count(*) c
                                  FROM KempTransactions
                                  WHERE Type = 2
                                  GROUP BY date(CreatedDate), UserId
                                  HAVING c > 1);

                                  CREATE TEMPORARY TABLE IF NOT EXISTS tmpTableUserId AS (
                                  SELECT Guid, UserId, date(CreatedDate), count(*) c
                                  FROM KempTransactions
                                  WHERE Type = 2
                                  GROUP BY date(CreatedDate), UserId
                                  HAVING c > 1);

                                  CREATE TEMPORARY TABLE IF NOT EXISTS tmpTableTransactionCreatedDate AS (
                                  SELECT Guid, UserId, date(CreatedDate) as CreatedDate, count(*) c
                                  FROM KempTransactions
                                  WHERE Type = 2
                                  GROUP BY date(CreatedDate), UserId
                                  HAVING c > 1);

                                  SET SQL_SAFE_UPDATES = 0;

                                  DELETE FROM KempTransactions 
                                  WHERE Guid NOT IN (SELECT Guid FROM tmpTableTransactionGuid) 
	                                  AND Type = 2 
                                      AND UserId IN (SELECT UserId FROM tmpTableUserId)
                                      AND date(CreatedDate) IN (SELECT CreatedDate FROM tmpTableTransactionCreatedDate);

                                  UPDATE KempWallets as kw SET Amount = (SELECT SUM(kt.Amount) FROM KempTransactions as kt WHERE kt.UserId = kw.UserId);

                                  SET SQL_SAFE_UPDATES = 1;
                                  DROP TEMPORARY TABLE IF EXISTS tmpTableTransactionGuid;
                                  DROP TEMPORARY TABLE IF EXISTS tmpTableUserId;
                                  DROP TEMPORARY TABLE IF EXISTS tmpTableTransactionCreatedDate;");
    }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

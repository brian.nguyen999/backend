﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Kempus.EntityFramework.Migrations
{
  public partial class Add_IsShowHomePageFlag : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.AddColumn<bool>(
          name: "IsShowHomePage",
          table: "Schools",
          type: "tinyint(1)",
          nullable: false,
          defaultValue: true);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropColumn(
          name: "IsShowHomePage",
          table: "Schools");
    }
  }
}

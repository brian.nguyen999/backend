﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Kempus.EntityFramework.Migrations
{
    public partial class Set_Defualt_IsShowHomePageFlag_False : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
          migrationBuilder.AlterColumn<bool>(
            name: "IsShowHomePage",
            table: "Schools",
            type: "tinyint(1)",
            nullable: false,
            defaultValue: false);

          migrationBuilder.Sql("UPDATE Schools SET IsShowHomePage = 0;");
          migrationBuilder.Sql("UPDATE Schools SET IsShowHomePage = 1 WHERE IsActivated = 1;");
    }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

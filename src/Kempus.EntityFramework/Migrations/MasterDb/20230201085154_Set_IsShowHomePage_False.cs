﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Kempus.EntityFramework.Migrations
{
  public partial class Set_IsShowHomePage_False : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql("SET SQL_SAFE_UPDATES = 0; UPDATE Schools SET IsShowHomePage = 0 WHERE IsShowHomePage = 1;");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {

    }
  }
}

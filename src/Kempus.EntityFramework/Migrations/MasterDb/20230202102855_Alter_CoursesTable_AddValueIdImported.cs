﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Kempus.EntityFramework.Migrations
{
    public partial class Alter_CoursesTable_AddValueIdImported : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DROP TEMPORARY TABLE IF EXISTS tmpTable;
                CREATE TEMPORARY TABLE IF NOT EXISTS tmpTable AS (
                SELECT CsvRowIndex, Guid, SchoolId, FileUploadTrackingId,
                       COUNT(CsvRowIndex) AS c
                FROM Courses
                GROUP BY CsvRowIndex, SchoolId
                HAVING c > 1
                ORDER BY CsvRowIndex DESC);
                SET SQL_SAFE_UPDATES = 0;
                DELETE FROM Courses WHERE Guid IN (SELECT Guid FROM tmpTable);
                UPDATE Courses SET IdImported = CONCAT(SchoolId, ""_ "", CsvRowIndex) WHERE FileUploadTrackingId IS NOT NULL;
                SET SQL_SAFE_UPDATES = 1;
                DROP TEMPORARY TABLE IF EXISTS tmpTable;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

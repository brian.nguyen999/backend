﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Kempus.EntityFramework.Migrations
{
    public partial class Add_EmailTemplate_SiteResetPass : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "EmailTemplates",
                columns: new[] { "Id", "Content", "Guid", "Title", "Type" },
                values: new object[] { 5L, "Your OTP is {0}.\r\n                      Please use that OTP to reset the password by following the link below:\r\n                      {1}", new Guid("c01173c4-0ae9-4337-bfcc-07bc555059b8"), "Kempus - Reset Password", "SITE_CONFIRM_RESET_PASSWORD" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "EmailTemplates",
                keyColumn: "Id",
                keyValue: 5L);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Kempus.EntityFramework.Migrations
{
    public partial class AddLatestActivityDateTime : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "LatestActivityDateTime",
                table: "ConversationMembers",
                type: "datetime(6)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ConversationMembers_LatestActivityDateTime",
                table: "ConversationMembers",
                column: "LatestActivityDateTime");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ConversationMembers_LatestActivityDateTime",
                table: "ConversationMembers");

            migrationBuilder.DropColumn(
                name: "LatestActivityDateTime",
                table: "ConversationMembers");
        }
    }
}

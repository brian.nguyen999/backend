﻿using Kempus.Entities;
using Microsoft.EntityFrameworkCore;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.EntityFramework.Migrations.Seed
{
  public class DefaultSuperAdminAccount
  {
    public static void Seed(ModelBuilder builder)
    {
      // Add Super Admin account
      ApplicationUser newSuperAdmin = new ApplicationUser()
      {
        Id = 1,
        UserId = new Guid("36a4cb3a-72b4-49e2-a302-f360ffd805f7"),
        Status = (byte)UserStatus.Active,
        UserName = "SuperAdmin1",
        Email = "super_admin_1@kempus.com",
        EmailConfirmed = true,
        PasswordHash = "AQAAAAEAACcQAAAAEPRv9S2d+SnlsppkveTMxxb3OVOJ3Djyf82B6x+Yw5Ibei7+9wuC4ibhBFYWCPBk7w==",
        UserType = (byte)UserType.Admin,
        NickName = "SPAM01",
        CreatedDate = new DateTime(2022, 11, 9, 0, 0, 0),
        SecurityStamp = "3aee43b8-5b68-4464-be16-843b2ec26637",
        ConcurrencyStamp = "5912dfc9-e57b-4ec8-836d-09217905f60c"
      };
      builder.Entity<ApplicationUser>().HasData(new List<ApplicationUser>()
      {
        newSuperAdmin,
      });

      // Add Super Admin Role
      RoleEntity newSuperAdminRole = new RoleEntity()
      {
        Id = 1,
        Guid = new Guid("984e0e97-0667-4332-b237-7ad88bc1367e"),
        Name = "Super Admin",
        IsActive = true,
        CreatedDate = new DateTime(2022, 11, 9, 0, 0, 0),
        CreatedBy = newSuperAdmin.UserId,
        ConcurrencyStamp = "60cc76fc-7464-4857-bc0d-a11e36d9e3f3"
      };
      builder.Entity<RoleEntity>().HasData(new List<RoleEntity>()
      {
        newSuperAdminRole,
      });

      // Add Super Admin Role - Full Permission
      builder.Entity<RolePermissionEntity>().HasData(new List<RolePermissionEntity>()
      {
        new RolePermissionEntity()
        {
          Id = 1,
          RoleId = 1,
          PermissionId = 1,
        },
        new RolePermissionEntity()
        {
          Id = 2,
          RoleId = 1,
          PermissionId = 2,
        },
        new RolePermissionEntity()
        {
          Id = 3,
          RoleId = 1,
          PermissionId = 3,
        },
        new RolePermissionEntity()
        {
          Id = 4,
          RoleId = 1,
          PermissionId = 4,
        },
        new RolePermissionEntity()
        {
          Id = 5,
          RoleId = 1,
          PermissionId = 5,
        },
        new RolePermissionEntity()
        {
          Id = 6,
          RoleId = 1,
          PermissionId = 6,
        },
        new RolePermissionEntity()
        {
          Id = 7,
          RoleId = 1,
          PermissionId = 7,
        },
        new RolePermissionEntity()
        {
          Id = 8,
          RoleId = 1,
          PermissionId = 8,
        },
        new RolePermissionEntity()
        {
          Id = 9,
          RoleId = 1,
          PermissionId = 9,
        },
        new RolePermissionEntity()
        {
          Id = 10,
          RoleId = 1,
          PermissionId = 10,
        },
        new RolePermissionEntity()
        {
          Id = 11,
          RoleId = 1,
          PermissionId = 11,
        },
        new RolePermissionEntity()
        {
          Id = 12,
          RoleId = 1,
          PermissionId = 12,
        },
        new RolePermissionEntity()
        {
          Id = 13,
          RoleId = 1,
          PermissionId = 13,
        },
        new RolePermissionEntity()
        {
          Id = 14,
          RoleId = 1,
          PermissionId = 14,
        },
        new RolePermissionEntity()
        {
          Id = 15,
          RoleId = 1,
          PermissionId = 15,
        },
        new RolePermissionEntity()
        {
          Id = 16,
          RoleId = 1,
          PermissionId = 16,
        },
        new RolePermissionEntity()
        {
          Id = 17,
          RoleId = 1,
          PermissionId = 17,
        },
        new RolePermissionEntity()
        {
          Id = 18,
          RoleId = 1,
          PermissionId = 18,
        },
        new RolePermissionEntity()
        {
          Id = 19,
          RoleId = 1,
          PermissionId = 19,
        },
        new RolePermissionEntity()
        {
          Id = 20,
          RoleId = 1,
          PermissionId = 20,
        },
        new RolePermissionEntity()
        {
          Id = 21,
          RoleId = 1,
          PermissionId = 21,
        },
        new RolePermissionEntity()
        {
          Id = 22,
          RoleId = 1,
          PermissionId = 22,
        },
        new RolePermissionEntity()
        {
          Id = 23,
          RoleId = 1,
          PermissionId = 23,
        },
        new RolePermissionEntity()
        {
          Id = 24,
          RoleId = 1,
          PermissionId = 24,
        },
        new RolePermissionEntity()
        {
          Id = 25,
          RoleId = 1,
          PermissionId = 25,
        },
        new RolePermissionEntity()
        {
          Id = 26,
          RoleId = 1,
          PermissionId = 26,
        },
      });

      // Add Super Admin Role for Super Admin account
      builder.Entity<UserRoleEntity>().HasData(new List<UserRoleEntity>()
      {
        new UserRoleEntity()
        {
          Id = 1,
          RoleId = newSuperAdminRole.Id,
          UserId = newSuperAdmin.UserId
        }
      });
    }
  }
}
﻿using Kempus.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Kempus.Core.Constants.CoreConstants;

namespace Kempus.EntityFramework.Migrations.Seed
{
  public class DefaultDegree
  {
    public static void Seed(EntityTypeBuilder<DegreeEntity> builder)
    {
      builder.HasData(new List<DegreeEntity>()
      {
        new DegreeEntity
        {
          Id = 1,
          Guid = new Guid("f4c49fe1-ad78-4ad8-9929-f6ce31f1fdc1"),
          CreatedDate = DateTime.Parse("2022-01-01"),
          CreatedBy = Guid.Parse(SeedingDefaultData.AdminId),
          IsDeleted = false,
          Name = "Undergraduate",
          IsActivated = true,
          SchoolId = Guid.Parse(SeedingDefaultData.SchoolId)
        },
        new DegreeEntity
        {
          Id = 2,
          Guid = new Guid("32d258b3-0c71-4a9b-9d20-03f410540522"),
          CreatedDate = DateTime.Parse("2022-01-01"),
          CreatedBy = Guid.Parse(SeedingDefaultData.AdminId),
          IsDeleted = false,
          Name = "Graduate",
          IsActivated = true,
          SchoolId = Guid.Parse(SeedingDefaultData.SchoolId)
        },
      });
    }
  }
}
﻿using Kempus.Core.Constants;
using Kempus.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using static Kempus.Core.Constants.CoreConstants;

namespace Kempus.EntityFramework.Migrations.Seed
{
  public static class DefaultPermission
  {
    public static void Seed(EntityTypeBuilder<PermissionEntity> builder)
    {
      // Accounts
      builder.HasData(new List<PermissionEntity>()
      {
        // Account
        new PermissionEntity
        {
          Id = PermissionId.AccountView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.Accounts,
          Order = 0,
          ParentId = ParentModuleApplicationId.Accounts
        },
        new PermissionEntity
        {
          Id = PermissionId.AccountCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.Accounts,
          Order = 1,
          ParentId = ParentModuleApplicationId.Accounts,
        },

        // User
        new PermissionEntity
        {
          Id = PermissionId.UserView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.User,
          Order = 2,
          ParentId = ParentModuleApplicationId.Accounts
        },
        new PermissionEntity
        {
          Id = PermissionId.UserCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.User,
          Order = 3,
          ParentId = ParentModuleApplicationId.Accounts,
        },

        // SendNotification
        new PermissionEntity
        {
          Id = PermissionId.SendNotificationView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.SendNotification,
          Order = 4,
          ParentId = ParentModuleApplicationId.Accounts
        },
        new PermissionEntity
        {
          Id = PermissionId.SendNotificationCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.SendNotification,
          Order = 5,
          ParentId = ParentModuleApplicationId.Accounts,
        },

        // Admin
        new PermissionEntity
        {
          Id = PermissionId.AdminView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.Admin,
          Order = 6,
          ParentId = ParentModuleApplicationId.Accounts
        },
        new PermissionEntity
        {
          Id = PermissionId.AdminCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.Admin,
          Order = 7,
          ParentId = ParentModuleApplicationId.Accounts,
        },

        // Admin Setting
        new PermissionEntity
        {
          Id = PermissionId.AdminSettingsView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.AdminSettings,
          Order = 8,
          ParentId = ParentModuleApplicationId.Accounts
        },
        new PermissionEntity
        {
          Id = PermissionId.AdminSettingsCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.AdminSettings,
          Order = 9,
          ParentId = ParentModuleApplicationId.Accounts,
        },
      });

      // Forum
      builder.HasData(new List<PermissionEntity>()
      {
        // Forum
        new PermissionEntity
        {
          Id = PermissionId.ForumView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.Forum,
          Order = 10,
          ParentId = ParentModuleApplicationId.Forum
        },
        new PermissionEntity
        {
          Id = PermissionId.ForumCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.Forum,
          Order = 11,
          ParentId = ParentModuleApplicationId.Forum,
        },

        // Posts
        new PermissionEntity
        {
          Id = PermissionId.PostsView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.Posts,
          Order = 12,
          ParentId = ParentModuleApplicationId.Forum
        },
        new PermissionEntity
        {
          Id = PermissionId.PostsCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.Posts,
          Order = 13,
          ParentId = ParentModuleApplicationId.Forum,
        },

        // Polls
        new PermissionEntity
        {
          Id = PermissionId.PollsView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.Polls,
          Order = 14,
          ParentId = ParentModuleApplicationId.Forum
        },
        new PermissionEntity
        {
          Id = PermissionId.PollsCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.Polls,
          Order = 15,
          ParentId = ParentModuleApplicationId.Forum,
        },

        // Course review
        new PermissionEntity
        {
          Id = PermissionId.CourseReviewsView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.CourseReviews,
          Order = 16,
          ParentId = ParentModuleApplicationId.Forum
        },
        new PermissionEntity
        {
          Id = PermissionId.CourseReviewsCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.CourseReviews,
          Order = 17,
          ParentId = ParentModuleApplicationId.Forum,
        },

        // Transaction
        new PermissionEntity
        {
          Id = PermissionId.TransactionView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.Transaction,
          Order = 18,
          ParentId = ParentModuleApplicationId.Forum
        },
        new PermissionEntity
        {
          Id = PermissionId.TransactionCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.Transaction,
          Order = 19,
          ParentId = ParentModuleApplicationId.Forum,
        },

        // Flags
        new PermissionEntity
        {
          Id = PermissionId.FlagsView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.Flags,
          Order = 20,
          ParentId = ParentModuleApplicationId.Forum
        },
        new PermissionEntity
        {
          Id = PermissionId.FlagsCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.Flags,
          Order = 21,
          ParentId = ParentModuleApplicationId.Forum,
        },

        // School Data
        new PermissionEntity
        {
          Id = PermissionId.SchoolDataView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.SchoolData,
          Order = 22,
          ParentId = ParentModuleApplicationId.Forum
        },
        new PermissionEntity
        {
          Id = PermissionId.SchoolDataCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.SchoolData,
          Order = 23,
          ParentId = ParentModuleApplicationId.Forum,
        },

        // Banner content
        new PermissionEntity
        {
          Id = PermissionId.BannerContentView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.BannerContent,
          Order = 24,
          ParentId = ParentModuleApplicationId.Forum
        },
        new PermissionEntity
        {
          Id = PermissionId.BannerContentCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.BannerContent,
          Order = 25,
          ParentId = ParentModuleApplicationId.Forum,
        }
      });
    }
  }
}
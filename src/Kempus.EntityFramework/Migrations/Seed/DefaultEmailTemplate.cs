﻿using Kempus.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Kempus.EntityFramework.Migrations.Seed
{
  public class DefaultEmailTemplate
  {
    public static void Seed(EntityTypeBuilder<EmailTemplateEntity> builder)
    {
      builder.HasData(new List<EmailTemplateEntity>()
      {
        new EmailTemplateEntity()
        {
          Id = 1,
          Title = "Kempus OTP",
          Content = "Your OTP is {0}",
          Type = "SEND_OTP",
          Guid = new Guid("50a4cb3a-72b4-49e2-a302-f360ffd805f7")
        },
        new EmailTemplateEntity()
        {
          Id = 2,
          Title = "Kempus - Confirm Reset Password",
          Content = "Link reset password: {0}",
          Type = "CONFIRM_RESET_PASSWORD",
          Guid = new Guid("43d8051d-57fb-4f37-aec9-54e26f48f850")
        },
        new EmailTemplateEntity()
        {
          Id = 3,
          Title = "Kempus - Reset Password",
          Content = @"Your OTP is {0}.
                      Please use that OTP to reset the password by following the link below:
                      {1}",
          Type = "CMS_CONFIRM_RESET_PASSWORD",
          Guid = new Guid("68d8051d-57fb-4f37-aec9-54e26f48f890")
        },
        new EmailTemplateEntity()
        {
          Id = 4,
          Title = "Kempus - Sign Up",
          Content = @"{0} opened on Kempus.
                      You can register a new account using the link below:
                      {1}",
          Type = "CMS_SEND_MAIL_WAITLIST",
          Guid = new Guid("90d8051d-57fb-4f37-aec9-54e26f48f111")
        },
        new EmailTemplateEntity()
        {
          Id = 5,
          Title = "Kempus - Reset Password",
          Content = @"Your OTP is {0}.
                      Please use that OTP to reset the password by following the link below:
                      {1}",
          Type = "SITE_CONFIRM_RESET_PASSWORD",
          Guid = new Guid("c01173c4-0ae9-4337-bfcc-07bc555059b8")
        }
      });
    }
  }
}
﻿using Microsoft.EntityFrameworkCore;

namespace Kempus.EntityFramework
{
  public class MasterDbContext : ApplicationDbContext
  {
    public MasterDbContext(
      DbContextOptions<MasterDbContext> options
    ) : base(options)
    {
    }

    public MasterDbContext()
    {
    }
  }
}
﻿using Kempus.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Kempus.EntityFramework
{
  public static class EntityFrameworkModule
  {
    public static void ConfigureServices(this IServiceCollection services, IConfiguration configuration, ILogger logger)
    {
      // Master db
      var masterDBConnectionString = configuration.GetConnectionString("Master");
      services
        .AddDbContext<MasterDbContext>(
          options => options.UseMySql(masterDBConnectionString,
            serverVersion: ServerVersion.AutoDetect(masterDBConnectionString))
          .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking)
          );
      logger.LogInformation($"masterDBConnectionString:  {masterDBConnectionString}");

      // Replica db
      var replicaDBConnectionString = configuration.GetConnectionString("Replica");
      services
        .AddDbContext<ReplicaDbContext>(
          options => options.UseMySql(replicaDBConnectionString,
            serverVersion: ServerVersion.AutoDetect(replicaDBConnectionString))
          .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking)
          );
      logger.LogInformation($"replicaDBConnectionString:  {replicaDBConnectionString}");

      // Config identity
      services.AddIdentity<ApplicationUser, RoleEntity>()
        .AddEntityFrameworkStores<MasterDbContext>()
        .AddEntityFrameworkStores<ReplicaDbContext>()
        .AddDefaultTokenProviders();
      services.Configure<IdentityOptions>(options =>
      {
        // Password settings.
        options.Password.RequireDigit = true;
        options.Password.RequireLowercase = true;
        options.Password.RequireNonAlphanumeric = true;
        options.Password.RequireUppercase = false;
        options.Password.RequiredLength = 6;
        options.Password.RequiredUniqueChars = 1;

        // Lockout settings.
        options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
        options.Lockout.MaxFailedAccessAttempts = 5;
        options.Lockout.AllowedForNewUsers = true;

        // User settings.
        options.User.AllowedUserNameCharacters =
          "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
        options.User.RequireUniqueEmail = false;
      });
    }

    public static void UpdateDatabase(IApplicationBuilder app, Type dbContext, ILogger logger,
      IConfiguration configuration)
    {
      if (!Convert.ToBoolean(configuration["DatabaseMigration:Enable"]))
        return;

      logger.LogInformation("---Start migration database---" + DateTime.UtcNow);
      using (var serviceScope = app.ApplicationServices
               .GetRequiredService<IServiceScopeFactory>()
               .CreateScope())
      {
        if (dbContext == typeof(MasterDbContext))
        {
          using (var masterDbContext = serviceScope.ServiceProvider.GetService<MasterDbContext>())
          {
            masterDbContext.Database.SetCommandTimeout(86400);
            masterDbContext.Database.Migrate();
            logger.LogInformation("Finish execute master migration files: " + DateTime.UtcNow);
          }
        }

        if (dbContext == typeof(ReplicaDbContext))
        {
          // Disable migration at ReplicaDbContext to prevent table exists error
          // Enable when there is a separate ReplicaDB
          return;
          using (var replicaDbContext = serviceScope.ServiceProvider.GetService<ReplicaDbContext>())
          {
            replicaDbContext.Database.SetCommandTimeout(86400);
            replicaDbContext.Database.Migrate();
            logger.LogInformation("Finish execute replica migration files: " + DateTime.UtcNow);
          }
        }

        logger.LogInformation("Finish execute migration methods: " + DateTime.UtcNow);
      }

      logger.LogInformation("---Finish migration database---");
    }
  }
}
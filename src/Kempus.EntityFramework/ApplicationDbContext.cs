using Kempus.Core.Extensions;
using Kempus.Core.Models.ModelContracts;
using Kempus.Entities;
using Kempus.EntityFramework.Migrations.Seed;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Kempus.EntityFramework
{
  public class ApplicationDbContext : IdentityDbContext<ApplicationUser, RoleEntity, long>
  {
    protected ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
      : base(options)
    {
    }

    protected ApplicationDbContext(DbContextOptions options)
      : base(options)
    {
    }

    public ApplicationDbContext()
    {
    }

    public virtual DbSet<PostEntity> Posts { get; set; }
    public virtual DbSet<SchoolEntity> Schools { get; set; }
    public virtual DbSet<TopicEntity?> Topics { get; set; }
    public virtual DbSet<KeywordEntity> Keywords { get; set; }
    public virtual DbSet<PostKeywordEntity> PostKeywords { get; set; }

    // Authentication
    public virtual DbSet<UserRefreshTokenEntity> UserRefreshToken { get; set; }

    // Authorization
    public virtual DbSet<RoleEntity> Role { get; set; }
    public virtual DbSet<PermissionEntity> Permission { get; set; }
    public virtual DbSet<RolePermissionEntity> RolePermission { get; set; }
    public virtual DbSet<UserRoleEntity> UserRole { get; set; }
    public virtual DbSet<OtpEntity> Otp { get; set; }
    public virtual DbSet<SchoolLifeTimeAnalyticEntity> SchoolLifeTimeAnalytics { get; set; }
    public virtual DbSet<PollEntity> Polls { get; set; }
    public virtual DbSet<PollOptionEntity> PollOptions { get; set; }
    public virtual DbSet<EmailTemplateEntity> EmailTemplates { get; set; }
    public virtual DbSet<PostCommentEntity> PostComments { get; set; }

    public virtual DbSet<CourseEntity> Courses { get; set; }
    public virtual DbSet<CourseInstructorEntity> CourseInstructor { get; set; }
    public virtual DbSet<CourseReviewEntity> CourseReviews { get; set; }
    public virtual DbSet<CourseSummaryEntity> CourseSummaries { get; set; }
    public virtual DbSet<DegreeEntity> Degrees { get; set; }
    public virtual DbSet<InstructorEntity> Instructors { get; set; }
    public virtual DbSet<MajorEntity> Majors { get; set; }
    public virtual DbSet<WaitListEntity> WaitList { get; set; }
    public virtual DbSet<FlagEntity> Flags { get; set; }
    public virtual DbSet<BookmarkEntity> Bookmarks { get; set; }
    public virtual DbSet<CourseReviewLikeEntity> CourseReviewLikes { get; set; }
    public virtual DbSet<CourseReviewActivitySummaryEntity> CourseReviewActivitySummaries { get; set; }
    public virtual DbSet<CourseReview6MonthActivityEntity> CourseReview6MonthActivities { get; set; }
    public virtual DbSet<FileUploadTrackingEntity> FileUploadTrackings { get; set; }
    public virtual DbSet<VoteEntity> Votes { get; set; }
    public virtual DbSet<StatisticsEntity> Statistics { get; set; }
    public virtual DbSet<DepartmentProgramEntity> DepartmentPrograms { get; set; }
    public virtual DbSet<PostPollLikeEntity> PostPollLikes { get; set; }
    public virtual DbSet<VoteUserEntity> VoteUsers { get; set; }
    public virtual DbSet<PostPoll7daysActivityEntity> PostPoll7daysActivities { get; set; }
    public virtual DbSet<PostPollActivitySummaryEntity> PostPollActivitySummaries { get; set; }
    public virtual DbSet<BellNotificationEntity> BellNotifications { get; set; }
    public virtual DbSet<BellNotificationUserEntity> BellNotificationUsers { get; set; }
    public virtual DbSet<BellNotificationLastViewEntity> BellNotificationLastViews { get; set; }
    public virtual DbSet<KempWalletEntity> KempWallets { get; set; }
    public virtual DbSet<KempTransactionEntity> KempTransactions { get; set; }
    public virtual DbSet<BannerEntity> Banners { get; set; }
    public virtual DbSet<UserTrackingEntity> UserTrackings { get; set; }
    public virtual DbSet<ViewTrackingEntity> ViewTrackings { get; set; }
    public virtual DbSet<UserReferralStatisticEntity> UserReferralStatistics { get; set; }
    public virtual DbSet<UserLoginHistoryEntity> UserLoginHistory { get; set; }
    public virtual DbSet<PostPollActivityEntity> PostPollActivities { get; set; }
    public virtual DbSet<ConversationEntity> Conversations { get; set; }
    public virtual DbSet<ConversationMemberEntity> ConversationMembers { get; set; }
    public virtual DbSet<MessageEntity> Messages { get; set; }
    public virtual DbSet<BlockEntity> Blocks { get; set; }

    protected override void OnModelCreating(ModelBuilder builder)
    {
      base.OnModelCreating(builder);

      foreach (var entityType in builder.Model.GetEntityTypes())
      {
        if (typeof(ISoftDelete).IsAssignableFrom(entityType.ClrType))
          entityType.AddSoftDeleteQueryFilter();
      }

      // Authentication
      builder.Entity<UserRefreshTokenEntity>().HasIndex(x => x.UserId);
      builder.Entity<UserRefreshTokenEntity>().HasIndex(x => x.RefreshToken);
      builder.Entity<UserRefreshTokenEntity>().HasIndex(x => x.ExpirationTime);

      // Authorization
      builder.Entity<RoleEntity>().HasIndex(x => x.Name).IsUnique();
      builder.Entity<PermissionEntity>().HasIndex(x => x.ModuleApplicationId);
      builder.Entity<PermissionEntity>().HasIndex(x => x.Order);
      builder.Entity<RolePermissionEntity>().HasIndex(x => x.RoleId);
      builder.Entity<RolePermissionEntity>().HasIndex(x => x.PermissionId);
      builder.Entity<UserRoleEntity>().HasIndex(x => x.RoleId);
      builder.Entity<UserRoleEntity>().HasIndex(x => x.UserId);

      // SchoolLifeTimeAnalyticEntity
      builder.Entity<SchoolLifeTimeAnalyticEntity>().HasKey(x => x.Guid);
      builder.Entity<SchoolLifeTimeAnalyticEntity>().HasIndex(x => x.SchoolId);

      // Otp
      builder.Entity<OtpEntity>().HasIndex(x => x.Guid);

      builder.Entity<SchoolLifeTimeAnalyticEntity>().HasKey(x => x.Guid);


      // Course
      builder.Entity<CourseEntity>().HasIndex(x => x.Guid);
      builder.Entity<CourseEntity>().HasIndex(x => x.IdImported).IsUnique(true);

      // Degree
      builder.Entity<DegreeEntity>().HasIndex(x => x.Guid);

      // Instructor
      builder.Entity<InstructorEntity>().HasIndex(x => x.Guid);

      // CourseInstructor
      builder.Entity<CourseInstructorEntity>().HasIndex(x => new { x.Guid, x.InstructorId, x.CourseId });

      // CourseReview
      builder.Entity<CourseReviewEntity>().HasIndex(x => x.Guid);

      // CourseSummary
      builder.Entity<CourseSummaryEntity>().HasIndex(x => new { x.Guid, x.CourseId });

      // WaitList
      builder.Entity<WaitListEntity>().HasKey(x => x.Guid);
      builder.Entity<WaitListEntity>().HasIndex(x => new { x.Email, x.SchoolId });

      // Bookmark
      builder.Entity<BookmarkEntity>().HasKey(x => x.Guid);
      builder.Entity<BookmarkEntity>().HasIndex(x => new { x.UserId, x.ObjectId, x.ObjectType });

      // CourseReviewLike
      builder.Entity<CourseReviewLikeEntity>().HasIndex(x => new { x.Guid, x.CourseReviewId, x.SchoolId, x.UserId });

      // CourseReviewActivity
      builder.Entity<CourseReviewActivitySummaryEntity>().HasKey(x => x.CourseReviewId);
      builder.Entity<CourseReviewActivitySummaryEntity>().HasIndex(x => new { x.CourseReviewId, x.SchoolId });
      builder.Entity<CourseReview6MonthActivityEntity>().HasIndex(x => new { x.CourseReviewId, x.SchoolId, x.ActionId });

      // Vote
      builder.Entity<VoteEntity>().HasIndex(x => new { x.Guid, x.PollOptionId, x.VoteUserId });

      // Statistics
      builder.Entity<StatisticsEntity>().HasIndex(x => new { x.Guid, x.ObjectId, x.ObjectType, x.CreatedUser });

      // PostPollLike
      builder.Entity<PostPollLikeEntity>().HasIndex(x => new { x.Guid, x.ObjectId, x.ObjectType, x.UserId });

      // VoteUser
      builder.Entity<VoteUserEntity>().HasIndex(x => new { x.Guid, x.CreatedUser, x.PollId });

      // PostPollActivity
      builder.Entity<PostPoll7daysActivityEntity>().HasIndex(x => new { x.ObjectId, x.ObjectType, x.ActionId, x.SchoolId });

      // BellNotification 
      builder.Entity<BellNotificationEntity>().HasIndex(x => new { x.Guid, x.ObjectTypeId, x.ObjectId });

      // BellNotificationUser 
      builder.Entity<BellNotificationUserEntity>().HasIndex(x => new { x.Guid, x.BellNotificationId, x.CreatedUser });

      // BellNotificationLastView
      builder.Entity<BellNotificationLastViewEntity>().HasIndex(x => new { x.CreatedUser });

      // UserReferralStatistic
      builder.Entity<UserReferralStatisticEntity>().HasIndex(x => new { x.UserId });

      // UserLoginHistoryEntity
      builder.Entity<UserLoginHistoryEntity>().HasIndex(x => new { x.UserId });

      // Conversation
      builder.Entity<ConversationEntity>().HasIndex(x => x.Guid);
      builder.Entity<ConversationEntity>().HasIndex(x => x.CreatedUser);
      builder.Entity<ConversationEntity>().HasIndex(x => x.CreatedDate);
      builder.Entity<ConversationEntity>().HasIndex(x => x.SchoolId);

      // ConversationMember
      builder.Entity<ConversationMemberEntity>().HasKey(x => x.Guid);
      builder.Entity<ConversationMemberEntity>().HasIndex(x => x.ConversationId);
      builder.Entity<ConversationMemberEntity>().HasIndex(x => x.MemberId);
      builder.Entity<ConversationMemberEntity>().HasIndex(x => x.SchoolId);
      builder.Entity<ConversationMemberEntity>().HasIndex(x => x.LatestActivityDateTime);

      // Message
      builder.Entity<MessageEntity>().HasKey(x => x.Guid);
      builder.Entity<MessageEntity>().HasIndex(x => x.ConversationId);
      builder.Entity<MessageEntity>().HasIndex(x => x.CreatedUser);
      builder.Entity<MessageEntity>().HasIndex(x => x.CreatedDate);
      builder.Entity<MessageEntity>().HasIndex(x => x.SchoolId);

      // Block
      builder.Entity<BlockEntity>().HasKey(x => x.Guid);
      builder.Entity<BlockEntity>().HasIndex(x => x.ConversationId);
      builder.Entity<BlockEntity>().HasIndex(x => x.BlockedUser);
      builder.Entity<BlockEntity>().HasIndex(x => x.Blocker);

      // Seeding data
      DefaultPermission.Seed(builder.Entity<PermissionEntity>());
      DefaultSuperAdminAccount.Seed(builder);
      DefaultEmailTemplate.Seed(builder.Entity<EmailTemplateEntity>());

      DefaultDegree.Seed(builder.Entity<DegreeEntity>());
    }
  }
}
﻿using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Poll;
using Kempus.Services.Public.Bookmark;
using Kempus.Services.Public.Comment;
using Kempus.Services.Public.Poll;
using Kempus.Services.Public.PollOptions;
using Kempus.Services.Public.Post;
using Kempus.Services.Public.PostPollLike;
using Kempus.Services.Public.UserTracking;
using Kempus.Services.Public.Vote;
using Kempus.Services.Test.DataSeeding;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.EntityFrameworkCore;
using Xunit;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.Test.Public.Poll
{
  public class PollServiceTests
  {
    private readonly PollService _pollService;
    private readonly Mock<MasterDbContext> _masterDbContext = new();
    private readonly Mock<ReplicaDbContext> _replicaDbContext = new();
    private readonly Mock<IPostService> _postService = new();
    private readonly Mock<IPollOptionService> _pollServiceOption = new();
    private readonly Mock<IConfigurationSection> _configurationSection = new();
    private readonly Mock<IBookmarkService> _bookmarkService = new();
    private readonly Mock<IVoteService> _voteService = new();
    private readonly Mock<IPostPollLikeService> _postPollLkeService = new();
    private readonly Mock<IConfiguration> _configuration = new();
    private readonly Mock<ILogger<PollService>> _logger = new Mock<ILogger<PollService>>();
    private readonly Mock<IUserTrackingService> _userTrackingService = new();
    private readonly Mock<IPostCommentService> _postCommentService = new();

    public PollServiceTests()
    {
      _pollService = new PollService(_configuration.Object, _masterDbContext.Object,
        _replicaDbContext.Object, _postService.Object, _pollServiceOption.Object, _logger.Object,
        _bookmarkService.Object, _voteService.Object, _postPollLkeService.Object,
      _postCommentService.Object, _userTrackingService.Object
        );

      var databaseFacadeMockReplica = new Mock<DatabaseFacade>(_replicaDbContext.Object);
      _replicaDbContext.SetupGet(x => x.Database).Returns(databaseFacadeMockReplica.Object);
      _replicaDbContext.Setup(x => x.Database.BeginTransaction())
        .Returns(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);

      var databaseFacadeMockMaster = new Mock<DatabaseFacade>(_masterDbContext.Object);
      _masterDbContext.SetupGet(x => x.Database).Returns(databaseFacadeMockMaster.Object);
      _masterDbContext.Setup(x => x.Database.BeginTransaction())
        .Returns(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);
    }

    [Theory]
    [MemberData(nameof(TestBaseConstants.PollSearchData), MemberType = typeof(TestBaseConstants))]
    public async Task SearchPoll_ReturnData(Guid? postId, string? keyword, string? pollStatus, int expectedRows)
    {
      // Arrange
      _replicaDbContext.SetupGet(x => x.Users).ReturnsDbSet(MockDataHelper.GetApplicationUserMockData());
      _replicaDbContext.SetupGet(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
      _replicaDbContext.SetupGet(x => x.Topics).ReturnsDbSet(MockDataHelper.GetTopicMockData());
      _replicaDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _replicaDbContext.SetupGet(x => x.Keywords).ReturnsDbSet(GetKeywordMockData());
      _replicaDbContext.SetupGet(x => x.PostKeywords).ReturnsDbSet(GetPostKeywordMockData());
      _replicaDbContext.SetupGet(x => x.Polls).ReturnsDbSet(MockDataHelper.GetPollMockData());
      _replicaDbContext.SetupGet(x => x.PollOptions).ReturnsDbSet(MockDataHelper.GetPollOptionMockData());
      _replicaDbContext.SetupGet(x => x.VoteUsers).ReturnsDbSet(GetVoteUserMockData());
      _replicaDbContext.SetupGet(x => x.Votes).ReturnsDbSet(GetVoteMockData());
      _replicaDbContext.SetupGet(x => x.Statistics).ReturnsDbSet(GetStatisticsMockData());

      var model = new PollFilterModel
      {
        PostIds = postId != null ? new List<Guid> { postId.Value } : new List<Guid>(),
        Keyword = keyword,
        PollStatus = pollStatus
      };

      // Act
      var result = _pollService.Search(model, new Pageable(), false);

      // Assert
      Assert.NotNull(result);
      Assert.Equal(result.TotalCount, expectedRows);
    }

    [Fact]
    public async Task InsertPollMCQ_ReturnPostId()
    {
      // Arrange
      _masterDbContext.SetupGet(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
      _masterDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _masterDbContext.SetupGet(x => x.Topics).ReturnsDbSet(MockDataHelper.GetTopicMockData());
      _masterDbContext.SetupGet(x => x.Polls).ReturnsDbSet(MockDataHelper.GetEmptyPollMockData());
      _masterDbContext.SetupGet(x => x.PollOptions).ReturnsDbSet(MockDataHelper.GetEmptyPollOptionMockData());

      // Act
      var pollData = new PollInputModel
      {
        Title = "New poll",
        Keywords = new List<string>()
        {
          "Keyword 1",
          "Keyword 2",
          "Keyword 3",
        },
        Content = "Poll content",
        Type = "SCQ",
        Options = new List<string>()
        {
          "Option 1",
          "Option 2",
          "Option 3",
        }
      };
      var postId = _pollService.Create(pollData);

      // Assert
      Assert.NotNull(postId);
    }

    [Fact]
    public async Task InsertPollSCQ_ReturnPostId()
    {
      // Arrange
      _masterDbContext.SetupGet(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
      _masterDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _masterDbContext.SetupGet(x => x.Topics).ReturnsDbSet(MockDataHelper.GetTopicMockData());
      _masterDbContext.SetupGet(x => x.Polls).ReturnsDbSet(MockDataHelper.GetEmptyPollMockData());
      _masterDbContext.SetupGet(x => x.PollOptions).ReturnsDbSet(MockDataHelper.GetEmptyPollOptionMockData());

      // Act
      var pollData = new PollInputModel
      {
        Title = "New poll",
        Keywords = new List<string>()
        {
          "Keyword 1",
          "Keyword 2",
          "Keyword 3",
        },
        Content = "Poll content",
        Type = "SCQ",
        Options = new List<string>()
        {
          "Option 1",
          "Option 2",
          "Option 3",
        }
      };
      var postId = _pollService.Create(pollData);

      // Assert
      Assert.NotNull(postId);
    }

    [Fact]
    public async Task InsertPoll_MissingKeyword_ReturnException()
    {
      // Arrange
      _masterDbContext.SetupGet(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
      _masterDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _masterDbContext.SetupGet(x => x.Topics).ReturnsDbSet(MockDataHelper.GetTopicMockData());
      _masterDbContext.SetupGet(x => x.Polls).ReturnsDbSet(MockDataHelper.GetEmptyPollMockData());
      _masterDbContext.SetupGet(x => x.PollOptions).ReturnsDbSet(MockDataHelper.GetEmptyPollOptionMockData());

      // Act
      var pollData = new PollInputModel
      {
        Title = "New poll",
        Keywords = new List<string>(),
        Content = "Poll content",
        Type = "MCQ",
        Options = new List<string>()
        {
          "Option 1",
          "Option 2",
          "Option 3",
          "Option 4",
          "Option 5",
        }
      };

      var ex = Assert.Throws<BadRequestException>(() => _pollService.Create(pollData));

      // Assert
      Assert.Equal(ErrorMessages.Poll_KeywordsInvalid, ex.Message);
    }

    [Fact]
    public async Task InsertPoll_GreaterThan3Keywords_ReturnException()
    {
      // Arrange
      _masterDbContext.SetupGet(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
      _masterDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _masterDbContext.SetupGet(x => x.Topics).ReturnsDbSet(MockDataHelper.GetTopicMockData());
      _masterDbContext.SetupGet(x => x.Polls).ReturnsDbSet(MockDataHelper.GetEmptyPollMockData());
      _masterDbContext.SetupGet(x => x.PollOptions).ReturnsDbSet(MockDataHelper.GetEmptyPollOptionMockData());

      // Act
      var pollData = new PollInputModel
      {
        Title = "New poll",
        Keywords = new List<string>()
        {
          "Keyword 1",
          "Keyword 2",
          "Keyword 3",
          "Keyword 4",
        },
        Content = "Poll content",
        Type = "MCQ",
        Options = new List<string>()
        {
          "Option 1",
          "Option 2",
          "Option 3",
          "Option 4",
          "Option 5",
        }
      };

      var ex = Assert.Throws<BadRequestException>(() => _pollService.Create(pollData));

      // Assert
      Assert.Equal(ErrorMessages.Poll_KeywordsInvalid, ex.Message);
    }

    [Fact]
    public async Task InsertPoll_MissingOption_ReturnException()
    {
      // Arrange
      _masterDbContext.SetupGet(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
      _masterDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _masterDbContext.SetupGet(x => x.Topics).ReturnsDbSet(MockDataHelper.GetTopicMockData());
      _masterDbContext.SetupGet(x => x.Polls).ReturnsDbSet(MockDataHelper.GetEmptyPollMockData());
      _masterDbContext.SetupGet(x => x.PollOptions).ReturnsDbSet(MockDataHelper.GetEmptyPollOptionMockData());

      // Act
      var pollData = new PollInputModel
      {
        Title = "New poll",
        Keywords = new List<string>()
        {
          "Keyword 1",
          "Keyword 2",
          "Keyword 3",
        },
        Content = "Poll content",
        Type = "MCQ",
        Options = new List<string>()
        {
        }
      };

      var ex = Assert.Throws<BadRequestException>(() => _pollService.Create(pollData));

      // Assert
      Assert.Equal(ErrorMessages.Poll_OptionInvalid, ex.Message);
    }

    [Fact]
    public async Task InsertPoll_GreaterThan5Options_ReturnException()
    {
      // Arrange
      _masterDbContext.SetupGet(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
      _masterDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _masterDbContext.SetupGet(x => x.Topics).ReturnsDbSet(MockDataHelper.GetTopicMockData());
      _masterDbContext.SetupGet(x => x.Polls).ReturnsDbSet(MockDataHelper.GetEmptyPollMockData());
      _masterDbContext.SetupGet(x => x.PollOptions).ReturnsDbSet(MockDataHelper.GetEmptyPollOptionMockData());

      // Act
      var pollData = new PollInputModel
      {
        Title = "New poll",
        Keywords = new List<string>()
        {
          "Keyword 1",
          "Keyword 2",
          "Keyword 3",
        },
        Content = "Poll content",
        Type = "MCQ",
        Options = new List<string>()
        {
          "Option 1",
          "Option 2",
          "Option 3",
          "Option 4",
          "Option 5",
          "Option 6",
        }
      };

      var ex = Assert.Throws<BadRequestException>(() => _pollService.Create(pollData));

      // Assert
      Assert.Equal(ErrorMessages.Poll_OptionInvalid, ex.Message);
    }


    [Fact]
    public async Task ValidateBeforeInsert_MissingKeyword_ReturnException()
    {
      // Arrange
      _masterDbContext.SetupGet(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
      _masterDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _masterDbContext.SetupGet(x => x.Topics).ReturnsDbSet(MockDataHelper.GetTopicMockData());
      _masterDbContext.SetupGet(x => x.Polls).ReturnsDbSet(MockDataHelper.GetEmptyPollMockData());
      _masterDbContext.SetupGet(x => x.PollOptions).ReturnsDbSet(MockDataHelper.GetEmptyPollOptionMockData());

      // Act
      var pollData = new PollInputModel
      {
        Title = "New poll",
        Keywords = new List<string>(),
        Content = "Poll content",
        Type = "MCQ",
        Options = new List<string>()
        {
          "Option 1",
          "Option 2",
          "Option 3",
          "Option 4",
          "Option 5",
        }
      };

      var ex = Assert.Throws<BadRequestException>(() => _pollService.Create(pollData));

      // Assert
      Assert.Equal(ErrorMessages.Poll_KeywordsInvalid, ex.Message);
    }

    [Fact]
    public async Task ValidateBeforeInsert_GreaterThan3Keywords_ReturnException()
    {
      // Arrange
      _masterDbContext.SetupGet(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
      _masterDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _masterDbContext.SetupGet(x => x.Topics).ReturnsDbSet(MockDataHelper.GetTopicMockData());
      _masterDbContext.SetupGet(x => x.Polls).ReturnsDbSet(MockDataHelper.GetEmptyPollMockData());
      _masterDbContext.SetupGet(x => x.PollOptions).ReturnsDbSet(MockDataHelper.GetEmptyPollOptionMockData());

      // Act
      var pollData = new PollInputModel
      {
        Title = "New poll",
        Keywords = new List<string>()
        {
          "Keyword 1",
          "Keyword 2",
          "Keyword 3",
          "Keyword 4",
        },
        Content = "Poll content",
        Type = "MCQ",
        Options = new List<string>()
        {
          "Option 1",
          "Option 2",
          "Option 3",
          "Option 4",
          "Option 5",
        }
      };

      var ex = Assert.Throws<BadRequestException>(() => _pollService.ValidateBeforeInsert(pollData));

      // Assert
      Assert.Equal(ErrorMessages.Poll_KeywordsInvalid, ex.Message);
    }

    [Fact]
    public async Task ValidateBeforeInsert_MissingOption_ReturnException()
    {
      // Arrange
      _masterDbContext.SetupGet(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
      _masterDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _masterDbContext.SetupGet(x => x.Topics).ReturnsDbSet(MockDataHelper.GetTopicMockData());
      _masterDbContext.SetupGet(x => x.Polls).ReturnsDbSet(MockDataHelper.GetEmptyPollMockData());
      _masterDbContext.SetupGet(x => x.PollOptions).ReturnsDbSet(MockDataHelper.GetEmptyPollOptionMockData());

      // Act
      var pollData = new PollInputModel
      {
        Title = "New poll",
        Keywords = new List<string>()
        {
          "Keyword 1",
          "Keyword 2",
          "Keyword 3",
        },
        Content = "Poll content",
        Type = "MCQ",
        Options = new List<string>()
        {
        }
      };

      var ex = Assert.Throws<BadRequestException>(() => _pollService.ValidateBeforeInsert(pollData));

      // Assert
      Assert.Equal(ErrorMessages.Poll_OptionInvalid, ex.Message);
    }

    [Fact]
    public async Task ValidateBeforeInsert_GreaterThan5Options_ReturnException()
    {
      // Arrange
      _masterDbContext.SetupGet(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
      _masterDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _masterDbContext.SetupGet(x => x.Topics).ReturnsDbSet(MockDataHelper.GetTopicMockData());
      _masterDbContext.SetupGet(x => x.Polls).ReturnsDbSet(MockDataHelper.GetEmptyPollMockData());
      _masterDbContext.SetupGet(x => x.PollOptions).ReturnsDbSet(MockDataHelper.GetEmptyPollOptionMockData());

      // Act
      var pollData = new PollInputModel
      {
        Title = "New poll",
        Keywords = new List<string>()
        {
          "Keyword 1",
          "Keyword 2",
          "Keyword 3",
        },
        Content = "Poll content",
        Type = "MCQ",
        Options = new List<string>()
        {
          "Option 1",
          "Option 2",
          "Option 3",
          "Option 4",
          "Option 5",
          "Option 6",
        }
      };

      var ex = Assert.Throws<BadRequestException>(() => _pollService.ValidateBeforeInsert(pollData));

      // Assert
      Assert.Equal(ErrorMessages.Poll_OptionInvalid, ex.Message);
    }

    public List<KeywordEntity> GetKeywordMockData()
    {
      return new List<KeywordEntity>
      {
        new KeywordEntity
        {
          Id = 1,
          Name = "keyword1",
          SchoolId = Guid.Parse("a8c5f021-4f6c-11ed-ba4c-02c3445b6529"),
          Guid = Guid.Parse("f736d465-3a2e-4b9a-a953-62013f923a9a")
        }
      };
    }

    public List<PostKeywordEntity> GetPostKeywordMockData()
    {
      return new List<PostKeywordEntity>
      {
        new PostKeywordEntity
        {
          Id = 1,
          PostId = TestBaseConstants.PostId3,
          KeywordId = Guid.Parse("f736d465-3a2e-4b9a-a953-62013f923a9a"),
          Guid = Guid.Parse("01124e4e-ea1f-42e0-9190-18125864b75c")
        }
      };
    }

    private List<VoteUserEntity> GetVoteUserMockData()
    {
      return new List<VoteUserEntity>
      {
        new VoteUserEntity
        {
          Id = 1,
          PollId = TestBaseConstants.PollId,
          CreatedUser = TestBaseConstants.UserId,
          Guid = TestBaseConstants.VoteUserId
        }
      };
    }

    public List<VoteEntity> GetVoteMockData()
    {
      return new List<VoteEntity>
      {
        new VoteEntity
        {
          Id = 1,
          Guid = Guid.NewGuid(),
          PollOptionId = new Guid("b28fdeb1-a17b-467b-8d15-397451cb98cd"),
          VoteUserId = TestBaseConstants.VoteUserId
        }
      };
    }

    public List<StatisticsEntity> GetStatisticsMockData()
    {
      return new List<StatisticsEntity>
      {
        new StatisticsEntity
        {
          Id = 1,
          Guid = Guid.NewGuid(),
          ObjectId = TestBaseConstants.PollId,
          ObjectType = (byte)StatisticsObjectType.Poll,
          NumberOfVote = 1,
          CreatedUser = TestBaseConstants.UserId
        }
      };
    }
  }
}
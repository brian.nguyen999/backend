﻿using Kempus.Core;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.EntityFramework;
using Kempus.Models.Public.Role;
using Kempus.Services.Admin.Role;
using Kempus.Services.Public.RolePermission;
using Kempus.Services.Test.DataSeeding;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Moq;
using Moq.EntityFrameworkCore;
using Xunit;

namespace Kempus.Services.Test.Public.Role
{
  public class RoleServiceTests
  {
    private readonly RoleService _roleService;
    private readonly Mock<MasterDbContext> _masterDbContext = new();
    private readonly Mock<ReplicaDbContext> _replicaDbContext = new();
    private readonly Mock<SessionStore> _sessionStore = new();
    private readonly Mock<IRolePermissionService> _rolePermissionService = new();

    public RoleServiceTests()
    {
      _roleService = new RoleService(_masterDbContext.Object, _replicaDbContext.Object, _sessionStore.Object,
        _rolePermissionService.Object);
      var databaseFacadeMockReplica = new Mock<DatabaseFacade>(_replicaDbContext.Object);
      _replicaDbContext.SetupGet(x => x.Database).Returns(databaseFacadeMockReplica.Object);
      _replicaDbContext.Setup(x => x.Database.BeginTransaction())
        .Returns(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);

      var databaseFacadeMockMaster = new Mock<DatabaseFacade>(_masterDbContext.Object);
      _masterDbContext.SetupGet(x => x.Database).Returns(databaseFacadeMockMaster.Object);
      _masterDbContext.Setup(x => x.Database.BeginTransaction())
        .Returns(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);
    }

    //TODO UPDATE unit test after remove foreign key
    //[Theory]
    //[MemberData(nameof(TestBaseConstants.RoleSearchData), MemberType = typeof(TestBaseConstants))]
    //public async Task SearchRole_ReturnData(Guid? roleId, string? keyword, int expectedRows)
    //{
    //  // Arrange
    //  _masterDbContext.Setup(x => x.Role).ReturnsDbSet(MockDataHelper.GetRoleMockData());
    //  _masterDbContext.Setup(x => x.RolePermission).ReturnsDbSet(MockDataHelper.GetRolePermissionMockData());

    //  var model = new RoleFilterModel
    //  {
    //    RoleId = roleId,
    //    Keyword = keyword
    //  };

    //  var pageable = new Pageable
    //  {
    //    PageSize = 10,
    //    PageIndex = 1,
    //  };

    //  // Act
    //  var result = _roleService.Search(model, pageable, true);

    //  // Assert
    //  Assert.NotNull(result);
    //  Assert.Equal(result.TotalCount, expectedRows);
    //}

    //[Fact]
    //public void GetDetailRole_InvalidRoleId_ReturnNotFoundException()
    //{
    //  // Arrange
    //  _replicaDbContext.Setup(x => x.Role).ReturnsDbSet(MockDataHelper.GetRoleMockData());
    //  _replicaDbContext.Setup(x => x.RolePermission).ReturnsDbSet(MockDataHelper.GetRolePermissionMockData());

    //  // Act
    //  var ex = Assert.Throws<NotFoundException>(() => _roleService.GetDetail(Guid.NewGuid()));

    //  // Assert
    //  Assert.Equal(ErrorMessages.Role_Notfound, ex.Message);
    //}

    //[Fact]
    //public void GetDetailRole_ReturnRoleData()
    //{
    //  // Arrange
    //  _replicaDbContext.Setup(x => x.Role).ReturnsDbSet(MockDataHelper.GetRoleMockData());
    //  _replicaDbContext.Setup(x => x.RolePermission).ReturnsDbSet(MockDataHelper.GetRolePermissionMockData());

    //  // Act
    //  var result = _roleService.GetDetail(TestBaseConstants.RoleId);

    //  // Assert
    //  Assert.NotNull(result);
    //  Assert.Equal(result.Id, TestBaseConstants.RoleId);
    //}

    //[Fact]
    //public async Task CreateRole_ReturnRoleId()
    //{
    //  // Arrange
    //  _masterDbContext.SetupGet(x => x.Permission).ReturnsDbSet(MockDataHelper.GetPermissionMockData());
    //  _masterDbContext.SetupGet(x => x.RolePermission).ReturnsDbSet(MockDataHelper.GetRolePermissionMockData());
    //  _masterDbContext.SetupGet(x => x.Role).ReturnsDbSet(MockDataHelper.GetRoleMockData());

    //  // Act
    //  var roleData = new RoleInputModel
    //  {
    //    Name = "Superadmin",
    //    IsActive = true,
    //    PermissionIds = new List<int>()
    //    {
    //      1, 2, 10, 11
    //    }
    //  };
    //  var roleId = _roleService.Create(roleData);

    //  // Assert
    //  Assert.NotNull(roleId);
    //}

    //[Fact]
    //public async Task CreateRole_DuplicateName_ReturnException()
    //{
    //  // Arrange
    //  _masterDbContext.SetupGet(x => x.Permission).ReturnsDbSet(MockDataHelper.GetPermissionMockData());
    //  _masterDbContext.SetupGet(x => x.RolePermission).ReturnsDbSet(MockDataHelper.GetRolePermissionMockData());
    //  _masterDbContext.SetupGet(x => x.Role).ReturnsDbSet(MockDataHelper.GetRoleMockData());

    //  // Act
    //  var roleData = new RoleInputModel
    //  {
    //    Name = "Admin",
    //    IsActive = true,
    //    PermissionIds = new List<int>()
    //  };

    //  // Assert
    //  var ex = Assert.Throws<BadRequestException>(() => _roleService.Create(roleData));

    //  // Assert
    //  Assert.Equal(ErrorMessages.Role_Existed, ex.Message);
    //}


    //[Fact]
    //public async Task UpdateRole_DuplicateName_ReturnException()
    //{
    //  // Arrange
    //  _masterDbContext.SetupGet(x => x.Permission).ReturnsDbSet(MockDataHelper.GetPermissionMockData());
    //  _masterDbContext.SetupGet(x => x.RolePermission).ReturnsDbSet(MockDataHelper.GetRolePermissionMockData());
    //  _masterDbContext.SetupGet(x => x.Role).ReturnsDbSet(MockDataHelper.GetRoleMockData());
    //  _replicaDbContext.SetupGet(x => x.Permission).ReturnsDbSet(MockDataHelper.GetPermissionMockData());
    //  _replicaDbContext.SetupGet(x => x.RolePermission).ReturnsDbSet(MockDataHelper.GetRolePermissionMockData());
    //  _replicaDbContext.SetupGet(x => x.Role).ReturnsDbSet(MockDataHelper.GetRoleMockData());

    //  // Act
    //  var roleData = new RoleUpdateInputModel
    //  {
    //    Id = TestBaseConstants.RoleId,
    //    Name = "Editor",
    //    PermissionIds = new List<int>()
    //  };

    //  // Assert
    //  var ex = Assert.Throws<BadRequestException>(() => _roleService.Update(roleData));

    //  // Assert
    //  Assert.Equal(ErrorMessages.Role_Existed, ex.Message);
    //}

    //[Fact]
    //public async Task UpdateRole_WrongId_ReturnException()
    //{
    //  // Arrange
    //  _masterDbContext.SetupGet(x => x.Permission).ReturnsDbSet(MockDataHelper.GetPermissionMockData());
    //  _masterDbContext.SetupGet(x => x.RolePermission).ReturnsDbSet(MockDataHelper.GetRolePermissionMockData());
    //  _masterDbContext.SetupGet(x => x.Role).ReturnsDbSet(MockDataHelper.GetRoleMockData());
    //  _replicaDbContext.SetupGet(x => x.Permission).ReturnsDbSet(MockDataHelper.GetPermissionMockData());
    //  _replicaDbContext.SetupGet(x => x.RolePermission).ReturnsDbSet(MockDataHelper.GetRolePermissionMockData());
    //  _replicaDbContext.SetupGet(x => x.Role).ReturnsDbSet(MockDataHelper.GetRoleMockData());

    //  // Act
    //  var roleData = new RoleUpdateInputModel
    //  {
    //    Id = Guid.Parse("c954b62b-56b8-41eb-a89a-cc53e0a7a186"),
    //    Name = "Super Editor",
    //    PermissionIds = new List<int>()
    //  };

    //  // Assert
    //  var ex = Assert.Throws<BadRequestException>(() => _roleService.Update(roleData));

    //  // Assert
    //  Assert.Equal(ErrorMessages.Role_Notfound, ex.Message);
    //}
  }
}
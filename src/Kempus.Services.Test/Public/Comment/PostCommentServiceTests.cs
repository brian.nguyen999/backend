﻿using Kempus.Core.Errors;
using Kempus.EntityFramework;
using Kempus.Models.Public.Comment;
using Kempus.Services.Public.BellNotification;
using Kempus.Services.Public.Comment;
using Kempus.Services.Public.PostPollActivity;
using Kempus.Services.Test.DataSeeding;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Moq;
using Moq.EntityFrameworkCore;
using Xunit;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.Test.Public.Comment
{
  public class PostCommentServiceTests
  {
    private readonly PostCommentService _postCommentService;
    private readonly Mock<MasterDbContext> _masterDbContext = new();
    private readonly Mock<ReplicaDbContext> _replicaDbContext = new();
    private readonly Mock<IPostPollActivityService> _postPollActivityService = new();
    private readonly Mock<IBellNotificationService> _bellNotificationService = new();

    public PostCommentServiceTests()
    {
      _postCommentService = new PostCommentService(_masterDbContext.Object, _replicaDbContext.Object, _postPollActivityService.Object, _bellNotificationService.Object);

      var databaseFacadeMockReplica = new Mock<DatabaseFacade>(_replicaDbContext.Object);
      _replicaDbContext.SetupGet(x => x.Database).Returns(databaseFacadeMockReplica.Object);
      _replicaDbContext.Setup(x => x.Database.BeginTransaction())
        .Returns(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);

      var databaseFacadeMockMaster = new Mock<DatabaseFacade>(_masterDbContext.Object);
      _masterDbContext.SetupGet(x => x.Database).Returns(databaseFacadeMockMaster.Object);
      _masterDbContext.Setup(x => x.Database.BeginTransaction())
        .Returns(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);
    }

    [Fact]
    public async Task CreatePostComment_ReturnPostCommentId()
    {
      // Arrange
      _replicaDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _replicaDbContext.SetupGet(x => x.Users).ReturnsDbSet(MockDataHelper.GetApplicationUserMockData());
      _masterDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());

      var model = new PostCommentInputModel
      {
        PostId = TestBaseConstants.PostId1,
        ParentId = null,
        Content = "Comment 1"
      };

      // Act
      var postCommentId = _postCommentService.Create(model, TestBaseConstants.UserId, TestBaseConstants.SchoolId);

      // Assert
      Assert.True(postCommentId != default);
    }

    [Fact]
    public async Task CreatePostComment_WrongPost_ReturnException()
    {
      // Arrange
      _replicaDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _masterDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());

      var model = new PostCommentInputModel
      {
        PostId = Guid.NewGuid(),
        ParentId = null,
        Content = "Comment 1",
        Type = (byte)PostType.Post
      };

      // Act
      var ex = Assert.Throws<NotFoundException>(() => _postCommentService.Create(model, TestBaseConstants.UserId, TestBaseConstants.SchoolId));

      // Assert
      Assert.Equal(ErrorMessages.Post_NotFound, ex.Message);
    }

    [Fact]
    public async Task CreatePostComment_WrongParentId_ReturnException()
    {
      // Arrange
      _masterDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _masterDbContext.SetupGet(x => x.PostComments).ReturnsDbSet(MockDataHelper.GetPostCommentMockData());
      _replicaDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _replicaDbContext.SetupGet(x => x.PostComments).ReturnsDbSet(MockDataHelper.GetPostCommentMockData());

      var model = new PostCommentInputModel
      {
        PostId = TestBaseConstants.PostId1,
        ParentId = Guid.NewGuid(),
        Content = "Comment 1"
      };

      // Act
      var ex = Assert.Throws<NotFoundException>(() => _postCommentService.Create(model, TestBaseConstants.UserId, TestBaseConstants.SchoolId));

      // Assert
      Assert.Equal(ErrorMessages.Comment_NotFound, ex.Message);
    }

    [Fact]
    public async Task CreatePostComment_NotMatchWithParentInSamePost_ReturnException()
    {
      // Arrange
      _masterDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _masterDbContext.SetupGet(x => x.PostComments).ReturnsDbSet(MockDataHelper.GetPostCommentMockData());
      _replicaDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _replicaDbContext.SetupGet(x => x.PostComments).ReturnsDbSet(MockDataHelper.GetPostCommentMockData());

      var model = new PostCommentInputModel
      {
        PostId = TestBaseConstants.PostId2,
        ParentId = TestBaseConstants.PostCommentId,
        Content = "Comment 2",
        Type = (byte)PostType.Post
      };

      // Act
      var ex = Assert.Throws<BadRequestException>(() => _postCommentService.Create(model, TestBaseConstants.UserId, TestBaseConstants.SchoolId));

      // Assert
      Assert.Equal(ErrorMessages.Comment_NotMatchWithParentInSamePost, ex.Message);
    }

    [Fact]
    public async Task CreatePostComment_EmptyContent_ReturnException()
    {
      // Arrange
      _masterDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _masterDbContext.SetupGet(x => x.PostComments).ReturnsDbSet(MockDataHelper.GetPostCommentMockData());
      _replicaDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _replicaDbContext.SetupGet(x => x.PostComments).ReturnsDbSet(MockDataHelper.GetPostCommentMockData());

      var model = new PostCommentInputModel
      {
        PostId = TestBaseConstants.PostId1,
        ParentId = null,
        Content = string.Empty
      };

      // Act
      var ex = Assert.Throws<BadRequestException>(() => _postCommentService.Create(model, TestBaseConstants.UserId, TestBaseConstants.SchoolId));

      // Assert
      Assert.Equal(ErrorMessages.Comment_Required, ex.Message);
    }
  }
}
﻿using Kempus.Core.Enums;
using Kempus.Core.Errors;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Bookmark;
using Kempus.Services.Public.Bookmark;
using Kempus.Services.Public.Poll;
using Kempus.Services.Public.Post;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Moq;
using Moq.EntityFrameworkCore;
using Xunit;

namespace Kempus.Services.Test.Public.Bookmark
{
  public class BookmarkServiceTests
  {
    private readonly BookmarkService _bookmarkService;
    private readonly Mock<MasterDbContext> _masterContext = new();
    private readonly Mock<ReplicaDbContext> _replicaContext = new();
    private readonly Mock<Lazy<IPostService>> _postService = new();
    private readonly Mock<Lazy<IPollService>> _pollService = new();

    public BookmarkServiceTests()
    {
      var databaseFacadeMock = new Mock<DatabaseFacade>(_masterContext.Object);
      _masterContext.SetupGet(x => x.Database).Returns(databaseFacadeMock.Object);
      _masterContext.Setup(x => x.Database.BeginTransaction())
        .Returns(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);

      var databaseFacadeMockReplica = new Mock<DatabaseFacade>(_replicaContext.Object);
      _replicaContext.SetupGet(x => x.Database).Returns(databaseFacadeMockReplica.Object);
      _replicaContext.Setup(x => x.Database.BeginTransaction())
        .Returns(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);

      _bookmarkService = new BookmarkService(_masterContext.Object, _replicaContext.Object, _postService.Object, _pollService.Object);
    }

    [Fact]
    public async Task CreateBookMark_ReturnBookmarkId()
    {
      // Arrange
      _masterContext.SetupGet(x => x.Bookmarks).ReturnsDbSet(new List<BookmarkEntity> { });
      _replicaContext.SetupGet(x => x.Bookmarks).ReturnsDbSet(new List<BookmarkEntity> { });
      _replicaContext.SetupGet(x => x.Posts).ReturnsDbSet(new List<PostEntity>
      {
        new PostEntity
        {
          Id = 1,
          Guid = Guid.Parse("39dd4dfd-54cc-49c9-9075-140402090126"),
          Title = TestBaseConstants.PostTitle,
          Content = "Post 1",
          Keywords = "['Keyword1','Keyword2']",
          Type = (byte)CoreEnums.PostType.Post,
          IsPublished = true,
          SchoolId = Guid.Parse("bf853bcd-04d3-4b5f-b1cc-848e7994853f"),
          TopicId = Guid.Parse("39de4dfd-54cc-49c9-9075-140402090126"),
          CreatedBy = Guid.Parse("39dd4dfe-54cc-49c9-9075-140402090126"),
          CreatedDate = DateTime.UtcNow
        },
      });

      // Act
      var inputModel = new BookmarkInputModel
      {
        UserId = Guid.NewGuid(),
        ObjectId = Guid.Parse("39dd4dfd-54cc-49c9-9075-140402090126"),
        ObjectType = CoreEnums.BookmarkObjectType.Post
      };
      var bookmarkId = _bookmarkService.CreateIfNotExist(inputModel);

      // Assert
      Assert.NotEqual(bookmarkId, default(Guid));
    }

    [Fact]
    public async Task CreateDuplicateBookMark_ReturnSameBookmarkId()
    {
      // Arrange
      _masterContext.SetupGet(x => x.Bookmarks).ReturnsDbSet(new List<BookmarkEntity> { });
      _replicaContext.SetupGet(x => x.Bookmarks).ReturnsDbSet(new List<BookmarkEntity> { });
      _replicaContext.SetupGet(x => x.Posts).ReturnsDbSet(new List<PostEntity>
      {
        new PostEntity
        {
          Id = 1,
          Guid = Guid.Parse("39dd4dfd-54cc-49c9-9075-140402090126"),
          Title = TestBaseConstants.PostTitle,
          Content = "Post 1",
          Keywords = "['Keyword1','Keyword2']",
          Type = (byte)CoreEnums.PostType.Post,
          IsPublished = true,
          SchoolId = Guid.Parse("bf853bcd-04d3-4b5f-b1cc-848e7994853f"),
          TopicId = Guid.Parse("39de4dfd-54cc-49c9-9075-140402090126"),
          CreatedBy = Guid.Parse("39dd4dfe-54cc-49c9-9075-140402090126"),
          CreatedDate = DateTime.UtcNow
        },
      });

      // Act
      var inputModel = new BookmarkInputModel
      {
        UserId = Guid.NewGuid(),
        ObjectId = Guid.Parse("39dd4dfd-54cc-49c9-9075-140402090126"),
        ObjectType = CoreEnums.BookmarkObjectType.Post
      };
      var bookmarkId = _bookmarkService.CreateIfNotExist(inputModel);
      var bookmarkId2 = _bookmarkService.CreateIfNotExist(inputModel);

      // Assert
      Assert.NotEqual(bookmarkId, bookmarkId2);
    }

    [Fact]
    public async Task CreateBookMark_WrongPostId_ReturnException()
    {
      // Arrange
      _masterContext.SetupGet(x => x.Bookmarks).ReturnsDbSet(new List<BookmarkEntity> { });
      _replicaContext.SetupGet(x => x.Bookmarks).ReturnsDbSet(new List<BookmarkEntity> { });
      _replicaContext.SetupGet(x => x.Posts).ReturnsDbSet(new List<PostEntity>
      {
        new PostEntity
        {
          Id = 1,
          Guid = Guid.Parse("39dd4dfd-54cc-49c9-9075-140402090126"),
          Title = TestBaseConstants.PostTitle,
          Content = "Post 1",
          Keywords = "['Keyword1','Keyword2']",
          Type = (byte)CoreEnums.PostType.Post,
          IsPublished = true,
          SchoolId = Guid.Parse("bf853bcd-04d3-4b5f-b1cc-848e7994853f"),
          TopicId = Guid.Parse("39de4dfd-54cc-49c9-9075-140402090126"),
          CreatedBy = Guid.Parse("39dd4dfe-54cc-49c9-9075-140402090126"),
          CreatedDate = DateTime.UtcNow
        },
      });

      // Act
      var inputModel = new BookmarkInputModel
      {
        UserId = Guid.NewGuid(),
        ObjectId = Guid.Parse("29dd4dfd-54cc-49c9-9075-140402090126"),
        ObjectType = CoreEnums.BookmarkObjectType.Post
      };

      var ex = Assert.Throws<NotFoundException>(() => _bookmarkService.CreateIfNotExist(inputModel));

      // Assert
      Assert.Equal(ErrorMessages.Post_NotFound, ex.Message);
    }

    [Fact]
    public async Task CreateBookMark_WrongPollId_ReturnException()
    {
      // Arrange
      _masterContext.SetupGet(x => x.Bookmarks).ReturnsDbSet(new List<BookmarkEntity> { });
      _replicaContext.SetupGet(x => x.Bookmarks).ReturnsDbSet(new List<BookmarkEntity> { });
      _replicaContext.SetupGet(x => x.Posts).ReturnsDbSet(new List<PostEntity>
      {
        new PostEntity
        {
          Id = 1,
          Guid = Guid.Parse("39dd4dfd-54cc-49c9-9075-140402090126"),
          Title = TestBaseConstants.PostTitle,
          Content = "Post 1",
          Keywords = "['Keyword1','Keyword2']",
          Type = (byte)CoreEnums.PostType.Post,
          IsPublished = true,
          SchoolId = Guid.Parse("bf853bcd-04d3-4b5f-b1cc-848e7994853f"),
          TopicId = Guid.Parse("39de4dfd-54cc-49c9-9075-140402090126"),
          CreatedBy = Guid.Parse("39dd4dfe-54cc-49c9-9075-140402090126"),
          CreatedDate = DateTime.UtcNow
        },
      });

      // Act
      var inputModel = new BookmarkInputModel
      {
        UserId = Guid.NewGuid(),
        ObjectId = Guid.Parse("29dd4dfd-54cc-49c9-9075-140402090126"),
        ObjectType = CoreEnums.BookmarkObjectType.Poll
      };

      var ex = Assert.Throws<NotFoundException>(() => _bookmarkService.CreateIfNotExist(inputModel));

      // Assert
      Assert.Equal(ErrorMessages.Poll_NotFound, ex.Message);
    }
  }
}

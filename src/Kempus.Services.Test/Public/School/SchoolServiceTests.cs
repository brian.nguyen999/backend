﻿using Kempus.Core;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Core.Models.Configuration;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.School;
using Kempus.Services.Common.Queue;
using Kempus.Services.Common.Redis;
using Kempus.Services.Common.Upload;
using Kempus.Services.Public.Notification;
using Kempus.Services.Public.School;
using Kempus.Services.Test.DataSeeding;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Moq;
using Moq.EntityFrameworkCore;
using StackExchange.Redis;
using Xunit;

namespace Kempus.Services.Test.Public.School
{
  public class SchoolServiceTests
  {
    private readonly SchoolService _schoolService;
    private readonly SessionStore _sessionStore;
    private readonly Mock<MasterDbContext> _masterDbContext = new();
    private readonly Mock<ReplicaDbContext> _replicaDbContext = new();
    private readonly Mock<IHttpContextAccessor> _httpContextAccessorMock = new();
    private readonly Mock<IEmailNotificationService> _emailNotificationService = new();
    private readonly Mock<DomainConfig> _domainConfig = new();
    private readonly Mock<AwsS3IntegrationHelper> _awsS3IntegrationHelper = new();
    private Mock<S3Config> _s3Config = new();
    private Mock<AwsConfig> _awsConfig = new();
    private readonly Mock<IOpenSearchEventPublisher> _openSearchEventPublisher = new();
    private readonly Mock<IServiceProvider> _provider = new();
    private readonly Mock<IRedisService> _redisService = new();
    private readonly Mock<IConnectionMultiplexer> _redis = new();

    public SchoolServiceTests()
    {
      _sessionStore = new SessionStore(_httpContextAccessorMock.Object);
      _schoolService = new SchoolService(
        _masterDbContext.Object,
        _replicaDbContext.Object,
        _sessionStore,
        _emailNotificationService.Object,
        _domainConfig.Object,
        _awsS3IntegrationHelper.Object,
        _awsConfig.Object,
        _openSearchEventPublisher.Object,
        _provider.Object,
        _redisService.Object,
        _redis.Object
        );

      var databaseFacadeMockMaster = new Mock<DatabaseFacade>(_masterDbContext.Object);
      _masterDbContext.SetupGet(x => x.Database).Returns(databaseFacadeMockMaster.Object);
      _masterDbContext.Setup(x => x.Database.BeginTransactionAsync(default))
        .ReturnsAsync(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);

      var databaseFacadeMockReplica = new Mock<DatabaseFacade>(_replicaDbContext.Object);
      _replicaDbContext.SetupGet(x => x.Database).Returns(databaseFacadeMockReplica.Object);
      _replicaDbContext.Setup(x => x.Database.BeginTransactionAsync(default))
        .ReturnsAsync(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);
    }

    // TODO Fix Unit test , bug AwsS3IntegrationHelperProxy
    //[Theory]
    //[MemberData(nameof(TestBaseConstants.SchoolSearchData), MemberType = typeof(TestBaseConstants))]
    //public async Task SearchPost_ReturnData(bool? status, int expectedRows)
    //{
    //  // Arrange
    //  _masterDbContext.SetupGet(x => x.Users).ReturnsDbSet(MockDataHelper.GetApplicationUserMockData());
    //  _masterDbContext.SetupGet(x => x.Schools).ReturnsDbSet(GetSchoolMockData());
    //  _masterDbContext.SetupGet(x => x.SchoolLifeTimeAnalytics).ReturnsDbSet(GetSchoolLifeTimeAnalyticMockData());

    //  var model = new SchoolFilterModel
    //  {
    //    Status = status
    //  };

    //  // Act
    //  var result = _schoolService.Search(model, new Pageable(), true);

    //  // Assert
    //  Assert.NotNull(result);
    //  Assert.Equal(result.TotalCount, expectedRows);
    //}

    //[Fact]
    //public void GetDetailSchool_ValidId_ReturnData()
    //{
    //  // Arrange
    //  _replicaDbContext.Setup(x => x.Schools).ReturnsDbSet(GetSchoolMockData());
    //  _replicaDbContext.Setup(x => x.SchoolLifeTimeAnalytics).ReturnsDbSet(GetSchoolLifeTimeAnalyticMockData());
    //  var schoolId = Guid.Parse("da359532-a003-4851-85a8-e755e46201eb");

    //  // Act
    //  var result = _schoolService.GetDetail(schoolId);

    //  // Assert
    //  Assert.NotNull(result);
    //  Assert.Equal(result.Id, schoolId);
    //}

    //[Fact]
    //public void GetDetailSchool_WrongSchoolId_ReturnException()
    //{
    //  // Arrange
    //  _replicaDbContext.Setup(x => x.Schools).ReturnsDbSet(GetSchoolMockData());
    //  var schoolId = Guid.NewGuid();

    //  // Act
    //  var result = Assert.Throws<NotFoundException>(() => _schoolService.GetDetail(schoolId));

    //  // Assert
    //  Assert.Equal(ErrorMessages.School_NotFound, result.Message);
    //}

    //[Fact]
    //public async Task CreateSchool_ValidData_ReturnSchoolIdAsync()
    //{
    //  // Arrange
    //  _masterDbContext.SetupGet(x => x.Schools).ReturnsDbSet(GetSchoolMockData());
    //  _masterDbContext.SetupGet(x => x.SchoolLifeTimeAnalytics).ReturnsDbSet(new List<SchoolLifeTimeAnalyticEntity>());

    //  var data = new SchoolInputModel
    //  {
    //    PrimaryColor = "#003262",
    //    SecondaryColor = "#C4820E",
    //    SchoolName = "School 1",
    //    SchoolEmailFormat = "school1.edu",
    //    Status = true,
    //    IsRequiredReferralCode = true
    //  };

    //  // Act
    //  var result = _schoolService.Create(data);

    //  // Assert
    //  Assert.NotNull(result);
    //}

    //[Fact]
    //public async Task CreateSchool_ExistedEmailFormat_ReturnExceptionAsync()
    //{
    //  // Arrange
    //  _masterDbContext.SetupGet(x => x.Schools).ReturnsDbSet(GetSchoolMockData());

    //  var data = new SchoolInputModel
    //  {
    //    PrimaryColor = "#003262",
    //    SecondaryColor = "#C4820E",
    //    SchoolName = "School 1",
    //    SchoolEmailFormat = "alaska.edu",
    //    Status = true,
    //    IsRequiredReferralCode = true
    //  };

    //  // Act
    //  var result = Assert.Throws<BadRequestException>(() => _schoolService.Create(data));

    //  // Assert
    //  Assert.Equal(ErrorMessages.School_ExistedEmailFormat, result.Message);
    //}

    //[Fact]
    //public async Task UpdateSchool_WrongSchoolId_ReturnException()
    //{
    //  // Arrange
    //  _masterDbContext.SetupGet(x => x.Schools).ReturnsDbSet(GetSchoolMockData());

    //  // Act
    //  var model = new SchoolInputModel
    //  {
    //    Id = Guid.NewGuid(),
    //    SchoolName = "University of Alaska",
    //    Status = true,
    //    PrimaryColor = "#787078",
    //    SecondaryColor = "#787078",
    //    SchoolEmailFormat = "alaska.edu",
    //    IsRequiredReferralCode = true,
    //  };

    //  var exception = Assert.Throws<NotFoundException>(() => _schoolService.Update(model));

    //  // Assert
    //  Assert.Equal(ErrorMessages.School_NotFound, exception.Message);
    //}

    //[Fact]
    //public async Task UpdateSchool_ExistEmailFormat_ReturnException()
    //{
    //  // Arrange
    //  _masterDbContext.SetupGet(x => x.Schools).ReturnsDbSet(GetSchoolMockData());

    //  // Act
    //  var model = new SchoolInputModel
    //  {
    //    Id = Guid.Parse("da359532-a003-4851-85a8-e755e46201eb"),
    //    SchoolName = "University of Alaska",
    //    Status = true,
    //    PrimaryColor = "#787078",
    //    SecondaryColor = "#787078",
    //    SchoolEmailFormat = "askacc.edu",
    //    IsRequiredReferralCode = true,
    //  };

    //  var exception = Assert.Throws<BadRequestException>(() => _schoolService.Update(model));

    //  // Assert
    //  Assert.Equal(ErrorMessages.School_ExistedEmailFormat, exception.Message);
    //}

    public List<SchoolEntity> GetSchoolMockData()
    {
      return new List<SchoolEntity>
      {
        new SchoolEntity
        {
          Id = 1,
          Guid = Guid.Parse("da359532-a003-4851-85a8-e755e46201eb"),
          Name = "University of Alaska",
          IsActivated = true,
          PrimaryColor = "#787078",
          SecondaryColor = "#787078",
          EmailDomain = "alaska.edu",
          CreatedDate = DateTime.UtcNow,
          IsDeleted = false,
        },
        new SchoolEntity
        {
          Id = 2,
          Guid = Guid.Parse("bf853bcd-04d3-4b5f-b1cc-848e7994853f"),
          Name = "Alaska Christian College",
          IsActivated = false,
          PrimaryColor = "#787078",
          SecondaryColor = "#787078",
          EmailDomain = "askacc.edu",
          CreatedDate = DateTime.UtcNow,
          IsDeleted = false,
        },
        new SchoolEntity
        {
          Id = 3,
          Guid = Guid.Parse("0088164f-71fd-4c90-9071-ee75f9a23174"),
          Name = "Alaska Christian College B",
          IsActivated = false,
          PrimaryColor = "#787078",
          SecondaryColor = "#787078",
          EmailDomain = "askacb.edu",
          CreatedDate = DateTime.UtcNow,
          IsDeleted = false,
        }
      };
    }

    public List<SchoolLifeTimeAnalyticEntity> GetSchoolLifeTimeAnalyticMockData()
    {
      return new List<SchoolLifeTimeAnalyticEntity>
      {
        new SchoolLifeTimeAnalyticEntity
        {
          Id = 1,
          Guid = Guid.Parse("c0bbb64c-de2a-4a31-964c-5bb087af6d46"),
          SchoolId = Guid.Parse("da359532-a003-4851-85a8-e755e46201eb"),
          Users = 2,
          NoOfWaitList = 2,
          CreatedDate = DateTime.UtcNow
        }
      };
    }
  }
}
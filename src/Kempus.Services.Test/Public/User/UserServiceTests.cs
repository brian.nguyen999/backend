﻿using Kempus.Core.Errors;
using Kempus.Core.Models.Configuration;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Authentication;
using Kempus.Models.Public.Authentication.Request;
using Kempus.Models.Public.User;
using Kempus.Models.Public.User.Request;
using Kempus.Services.Common;
using Kempus.Services.Common.KempTransaction;
using Kempus.Services.Public.Notification;
using Kempus.Services.Public.Otp;
using Kempus.Services.Public.School;
using Kempus.Services.Public.SchoolLifeTimeAnalytic;
using Kempus.Services.Public.User;
using Kempus.Services.Public.UserReferralStatistic;
using Kempus.Services.Public.WaitList;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.EntityFrameworkCore;
using Xunit;

namespace Kempus.Services.Test.Public.User
{
  public class UserServiceTests
  {
    private readonly UserService _userService;
    private readonly Mock<ILogger<UserService>> _logger = new Mock<ILogger<UserService>>();
    private readonly Mock<MasterDbContext> _masterContext;
    private readonly Mock<ReplicaDbContext> _replicaContext;
    private readonly Mock<UserManager<ApplicationUser>> _userManager;
    private readonly Mock<SignInManager<ApplicationUser>> _signInManager;
    private readonly Mock<IEmailNotificationService> _emailNotificationService = new();
    private readonly Mock<UserConfig> _userConfig = new Mock<UserConfig>();
    private readonly Mock<DomainConfig> _domainConfig = new Mock<DomainConfig>();
    private readonly Mock<IOtpService> _otpService = new();
    private readonly Mock<ISchoolService> _schoolService = new();
    private readonly Mock<IWaitListService> _waitListService = new();
    private readonly Mock<ISchoolLifeTimeAnalyticService> _schoolLifeTimeAnalyticService = new();
    private readonly Mock<IKempTransactionCommonService> _kempTransactionService = new();
    private readonly Mock<IUserReferralStatisticService> _userReferralStatisticService = new();

    public Mock<SignInManager<ApplicationUser>> SignInManager => _signInManager;

    public UserServiceTests()
    {
      var contextAccessor = new Mock<IHttpContextAccessor>();
      var userPrincipalFactory = new Mock<IUserClaimsPrincipalFactory<ApplicationUser>>();
      var userStoreMock = new Mock<IUserStore<ApplicationUser>>();

      _masterContext = new Mock<MasterDbContext>();
      _replicaContext = new Mock<ReplicaDbContext>();

      _userManager = new Mock<UserManager<ApplicationUser>>(
        /* IUserStore<TUser> store */Mock.Of<IUserStore<ApplicationUser>>(),
        /* IOptions<IdentityOptions> optionsAccessor */null,
        /* IPasswordHasher<TUser> passwordHasher */new PasswordHasher<ApplicationUser>(),
        /* IEnumerable<IUserValidator<TUser>> userValidators */null,
        /* IEnumerable<IPasswordValidator<TUser>> passwordValidators */null,
        /* ILookupNormalizer keyNormalizer */null,
        /* IdentityErrorDescriber errors */null,
        /* IServiceProvider services */null,
        /* ILogger<UserManager<TUser>> logger */null);
      _signInManager = new Mock<SignInManager<ApplicationUser>>(
        _userManager.Object,
        /* IHttpContextAccessor contextAccessor */Mock.Of<IHttpContextAccessor>(),
        /* IUserClaimsPrincipalFactory<TUser> claimsFactory */Mock.Of<IUserClaimsPrincipalFactory<ApplicationUser>>(),
        /* IOptions<IdentityOptions> optionsAccessor */null,
        /* ILogger<SignInManager<TUser>> logger */null,
        /* IAuthenticationSchemeProvider schemes */null,
        /* IUserConfirmation<TUser> confirmation */null);

      _userService = new UserService(
        _emailNotificationService.Object,
        _otpService.Object,
        _userManager.Object,
        _signInManager.Object,
        _masterContext.Object,
        _logger.Object,
        _userConfig.Object,
        _replicaContext.Object,
        _domainConfig.Object,
        _schoolService.Object,
        _waitListService.Object,
        _schoolLifeTimeAnalyticService.Object,
        _kempTransactionService.Object,
        _userReferralStatisticService.Object);

      var databaseFacadeMock = new Mock<DatabaseFacade>(_masterContext.Object);
      _masterContext.SetupGet(x => x.Database).Returns(databaseFacadeMock.Object);
      _masterContext.Setup(x => x.Database.BeginTransactionAsync(default))
        .ReturnsAsync(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);

      _masterContext.Setup(x => x.Users).ReturnsDbSet(TestBaseConstants.ApplicationUsersDataTest);
      _replicaContext.Setup(x => x.Users).ReturnsDbSet(TestBaseConstants.ApplicationUsersDataTest);
    }

    // TODO Update unit test after refactor
    //[Fact]
    //public async Task InsertAsync_Should_ReturnTrue()
    //{
    //  // Arrange
    //  _userManager.Setup(x => x.CreateAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>()))
    //    .ReturnsAsync(IdentityResult.Success)
    //    .Verifiable();

    //  // Act
    //  var newEmail = "userservice_test3@kempus.us";
    //  var newUser = new UserInputModel()
    //  {
    //    Email = newEmail,
    //    DegreeId = Guid.Parse("4109eade-3cbe-4756-9234-9a693d6b0031"),
    //    MajorId = Guid.Parse("9d7dc70b-cf38-41dd-9a49-9519f7a6c6b9"),
    //    ClassYear = 2022,
    //    NickName = "kempus_user3",
    //    UserName = "userservice_test3",
    //    Password = "Kempus123#",
    //    ConfirmPassword = "Kempus123#",
    //    OtpCode = "745454",
    //    ReferralCode = TestBaseConstants.ApplicationUsersDataTest[0].ReferralCode
    //  };
    //  var result = await _userService.InsertAsync(newUser);
    //  var newUsereEntity = await _userService.GetUserInfoByUsernameAsync(newUser.Email);

    //  // Assert
    //  Assert.IsType<bool>(result);
    //  Assert.Equal(newEmail, newUsereEntity.Email);
    //}

    [Fact]
    public async Task CheckEmailExistAsync_Should_ReturnTrue()
    {
      // Arrange
      _userManager.Setup(x => x.FindByEmailAsync(It.IsAny<string>()))
        .ReturnsAsync(new ApplicationUser())
        .Verifiable();

      // Act
      var currentUser = TestBaseConstants.ApplicationUsersDataTest[0];
      var result = await _userService.CheckEmailExistAsync(currentUser.Email);

      // Assert
      Assert.True(result);
    }

    [Fact]
    public async Task CheckUsernameExistAsync_Should_ReturnTrue()
    {
      // Arrange
      _userManager.Setup(x => x.FindByNameAsync(It.IsAny<string>()))
        .ReturnsAsync(new ApplicationUser())
        .Verifiable();

      // Act
      var currentUser = TestBaseConstants.ApplicationUsersDataTest[0];
      var result = await _userService.CheckUsernameExistAsync(currentUser.UserName);

      // Assert
      Assert.True(result);
    }

    [Fact]
    public async Task GetUserInfoByUsernameAsync_Should_ReturnObject()
    {
      // Arrange

      // Act
      var result =
        await _userService.GetUserInfoByUsernameAsync(TestBaseConstants.ApplicationUsersDataTest[0].UserName);

      // Assert
      Assert.NotNull(result);
      Assert.Equal(TestBaseConstants.ApplicationUsersDataTest[0].Email, result.Email);
    }

    [Fact]
    public async Task ChangePasswordAsync_Should_ReturnTrue()
    {
      // Arrange
      _userManager.Setup(x => x.GeneratePasswordResetTokenAsync(It.IsAny<ApplicationUser>()))
        .ReturnsAsync(It.IsAny<string>)
        .Verifiable();
      _userManager.Setup(x => x.ResetPasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>()))
        .ReturnsAsync(IdentityResult.Success)
        .Verifiable();

      // Act
      var currentUser = TestBaseConstants.ApplicationUsersDataTest[0];
      var result = await _userService.ChangePasswordAsync(currentUser, "NewKempus123#");

      // Assert
      Assert.True(result);
    }

    [Fact]
    public async Task CreateReferralCodeAsync_Should_ReturnStringNotNull()
    {
      // Arrange

      // Act
      var result = await _userService.CreateReferralCodeAsync();

      // Assert
      Assert.NotNull(result);
    }

    [Fact]
    public async Task CreateNickNameAsync_Should_ReturnStringNotNull()
    {
      // Arrange

      // Act
      var result = await _userService.CreateNickNameAsync();

      // Assert
      Assert.NotNull(result);
    }

    [Fact]
    public async Task CheckReferralCodeExistAsync_Should_ReturnsTrue()
    {
      // Arrange

      // Act
      var result =
        await _userService.CheckReferralCodeExistAsync(TestBaseConstants.ApplicationUsersDataTest[0].ReferralCode);

      // Assert
      Assert.True(result);
    }

    [Fact]
    public async Task CheckReferralCodeExistAsync_Should_ReturnsFalse()
    {
      // Arrange

      // Act
      var result = await _userService.CheckReferralCodeExistAsync("Referral1");

      // Assert
      Assert.False(result);
    }

    [Fact]
    public async Task SigninAsync_Should_ReturnSuccess()
    {
      // Arrange
      _signInManager.Setup(x => x.PasswordSignInAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), false, false))
        .ReturnsAsync(SignInResult.Success)
        .Verifiable();

      // Act
      var request = new LoginInputModel()
      {
        Username = TestBaseConstants.ApplicationUsersDataTest[0].UserName,
        Password = "Kempus123#"
      };
      var result = await _userService.SigninAsync(request);

      // Assert
      Assert.True(result);
    }

    [Fact]
    public async Task SigninAsync_Should_ReturnFalse()
    {
      // Arrange
      _signInManager.Setup(x => x.PasswordSignInAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), false, false))
        .ReturnsAsync(SignInResult.Failed)
        .Verifiable();

      // Act
      var request = new LoginInputModel()
      {
        Username = TestBaseConstants.ApplicationUsersDataTest[0].UserName,
        Password = "WrongPass123#"
      };
      var result = await _userService.SigninAsync(request);

      // Assert
      Assert.False(result);
    }

    [Fact]
    public async Task HandleSigninFailedAsync_Should_ReturnIntGreaterThanZero()
    {
      // Arrange
      _userManager.Setup(x => x.UpdateAsync(It.IsAny<ApplicationUser>()))
        .ReturnsAsync(It.IsAny<IdentityResult>)
        .Verifiable();
      var databaseFacadeMock = new Mock<DatabaseFacade>(_masterContext.Object);
      _masterContext.SetupGet(x => x.Database).Returns(databaseFacadeMock.Object);
      _masterContext.Setup(x => x.Database.BeginTransactionAsync(default))
        .ReturnsAsync(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);

      // Act
      var result = await _userService.HandleSigninFailedAsync(TestBaseConstants.ApplicationUsersDataTest[0].UserName);

      // Assert
      Assert.True(result > 0);
    }

    [Fact]
    public async Task HandleSigninSuccessAsync_Should_ReturnGuidNotNull()
    {
      // Arrange
      _userManager.Setup(x => x.UpdateAsync(It.IsAny<ApplicationUser>()))
        .ReturnsAsync(It.IsAny<IdentityResult>)
        .Verifiable();

      // Act
      var result = await _userService.HandleSigninSuccessAsync(TestBaseConstants.ApplicationUsersDataTest[0].UserName);

      // Assert
      Assert.Equal(TestBaseConstants.ApplicationUsersDataTest[0].UserId, result);
    }

    [Fact]
    public async Task CheckNickNameExistAsync_Should_ReturnsTrue()
    {
      // Arrange

      // Act
      var result = await _userService.CheckNickNameExistAsync(TestBaseConstants.ApplicationUsersDataTest[0].NickName);

      // Assert
      Assert.True(result);
    }

    [Fact]
    public async Task CheckNickNameExistAsync_Should_ReturnsFalse()
    {
      // Arrange

      // Act
      var result = await _userService.CheckNickNameExistAsync("WrongNickName1");

      // Assert
      Assert.False(result);
    }

    //[Fact]
    //public async Task ForgotPassword_InputEmailHasTakenEmail_ReturnException()
    //{
    //  // Arrange
    //  var dto = new ForgotPasswordRequestDto { Email = "userservice_test1@kempus.us" };
    //  _otpService.Setup(x => x.CheckObjectExistAsync(It.IsAny<OtpRequestDto>()))
    //    .ReturnsAsync(true)
    //    .Verifiable();

    //  // Act
    //  var result = await Assert.ThrowsAsync<BadRequestException>(() => _userService.ForgotPasswordAsync(dto));

    //  // Assert
    //  Assert.Equal(ErrorMessages.User_OtpStillValid, result.Message);
    //}

    [Fact]
    public async Task ResetPasswordByEmail_InputInValidEmail_ReturnException()
    {
      // Arrange
      var email = "userservice_test11@kempus.us";
      var dto = new ResetPasswordRequestDto
      {
        Password = "vinova123#",
        ConfirmPassword = "vinova123#",
        Email = email,
        OtpCode = Utilities.StringHelper.RandomNumber(100000, 999999)
      };

      // Act
      var result = await Assert.ThrowsAsync<NotFoundException>(() => _userService.ResetPasswordByEmailAsync(dto));

      // Assert
      Assert.Equal(ErrorMessages.User_NotFound, result.Message);
    }

    //[Fact]
    //public void GetUserProfile_InputValidUserId_ReturnObjectNotNull()
    //{
    //  // Arrange
    //  _replicaContext.Setup(x => x.Schools).ReturnsDbSet(new List<SchoolEntity>() { new SchoolEntity { Guid = TestBaseConstants.SchoolId, Name = "Unit Test School"} });
    //  _replicaContext.Setup(x => x.KempWallets).ReturnsDbSet(new List<KempWalletEntity>());
    //  _replicaContext.Setup(x => x.Degrees).ReturnsDbSet(new List<DegreeEntity>());
    //  _replicaContext.Setup(x => x.Majors).ReturnsDbSet(new List<MajorEntity>());

    //  // Act
    //  var result = _userService.GetUserProfile(TestBaseConstants.ApplicationUsersDataTest[0].UserId);

    //  // Assert
    //  Assert.NotNull(result);
    //}

    //[Fact]
    //public async Task GetUserReferralDetail_InputValidUserId_ReturnObjectNotNull()
    //{
    //  // Arrange

    //  // Act
    //  var result = _userService.GetUserReferralDetail(TestBaseConstants.ApplicationUsersDataTest[0].UserId);

    //  // Assert
    //  Assert.NotNull(result);
    //}

    [Fact]
    public async Task GetUserInfoByReferralCodeAsync_InputValidReferralCode_ReturnUserNotNull()
    {
      // Arrange

      // Act
      var result = await _userService.GetUserInfoByReferralCodeAsync(TestBaseConstants.ApplicationUsersDataTest[0].ReferralCode);

      // Assert
      Assert.NotNull(result);
    }

    [Fact]
    public async Task UpdateNicknameAsync_InputValidUserIdAndNickname_ReturnTrue()
    {
      // Arrange
      _userConfig.SetReturnsDefault(new UserConfig() { NickNameLockdownMinutes = 43200 });
      _userManager.Setup(x => x.UpdateAsync(It.IsAny<ApplicationUser>()))
        .ReturnsAsync(IdentityResult.Success)
        .Verifiable();

      // Act
      var result = await _userService.UpdateNicknameAsync(TestBaseConstants.ApplicationUsersDataTest[0].UserId);

      // Assert
      Assert.NotNull(result);
    }

    [Fact]
    public async Task GetUserByIdAsync_InputValidUserId_ReturnUserNotNull()
    {
      // Arrange

      // Act
      var result = await _userService.GetUserByIdAsync(TestBaseConstants.ApplicationUsersDataTest[0].UserId);

      // Assert
      Assert.NotNull(result);
    }
  }
}
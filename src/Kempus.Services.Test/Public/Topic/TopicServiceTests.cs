﻿using Kempus.Core.Enums;
using Kempus.Core.Models;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Topic;
using Kempus.Services.Public.Topic;
using Kempus.Services.Test.DataSeeding;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Moq;
using Moq.EntityFrameworkCore;
using Xunit;

namespace Kempus.Services.Test.Public.Topic
{
  public class TopicServiceTests
  {
    private readonly TopicService _topicService;
    private readonly Mock<MasterDbContext> _masterDbContext = new();
    private readonly Mock<ReplicaDbContext> _replicaDbContext = new();

    public TopicServiceTests()
    {
      _topicService = new TopicService(_masterDbContext.Object, _replicaDbContext.Object);

      var databaseFacadeMockReplica = new Mock<DatabaseFacade>(_replicaDbContext.Object);
      _replicaDbContext.SetupGet(x => x.Database).Returns(databaseFacadeMockReplica.Object);
      _replicaDbContext.Setup(x => x.Database.BeginTransaction())
        .Returns(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);

      var databaseFacadeMockMaster = new Mock<DatabaseFacade>(_masterDbContext.Object);
      _masterDbContext.SetupGet(x => x.Database).Returns(databaseFacadeMockMaster.Object);
      _masterDbContext.Setup(x => x.Database.BeginTransaction())
        .Returns(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);
    }

    [Theory]
    [MemberData(nameof(TestBaseConstants.TopicSearchData), MemberType = typeof(TestBaseConstants))]
    public async Task SearchTopic_ReturnData(Guid? topicId, string? keyword, int expectedRows)
    {
      // Arrange
      _masterDbContext.Setup(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
      _masterDbContext.Setup(x => x.Topics).ReturnsDbSet(MockDataHelper.GetTopicMockData());
      _masterDbContext.Setup(x => x.Posts).ReturnsDbSet(GetPostMockData());

      var filter = new TopicFilterModel
      {
        TopicId = topicId,
        Keyword = keyword,
      };

      var paging = new Pageable
      {
        PageSize = 10,
        PageIndex = 1
      };

      // Act
      var result = _topicService.Search(filter, paging, true);

      // Assert
      Assert.NotNull(result);
      Assert.Equal(result.TotalCount, expectedRows);
    }

    public List<PostEntity> GetPostMockData()
    {
      return new List<PostEntity>
      {
        new PostEntity
        {
          Id = 1,
          Guid = TestBaseConstants.PostId1,
          Title = TestBaseConstants.PostTitle,
          Content = "Post 1",
          Keywords = "['Keyword1','Keyword2']",
          Type = (byte)CoreEnums.PostType.Post,
          IsPublished = true,
          SchoolId = Guid.Parse("bf853bcd-04d3-4b5f-b1cc-848e7994853f"),
          TopicId = TestBaseConstants.TopicId,
          CreatedBy = TestBaseConstants.UserId,
          CreatedDate = DateTime.UtcNow
        }
      };
    }
  }
}
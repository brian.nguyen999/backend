﻿using Kempus.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kempus.Entities;
using Kempus.Services.Public.WaitList;
using Xunit;
using Kempus.Services.Test.DataSeeding;
using Moq.EntityFrameworkCore;

namespace Kempus.Services.Test.Public.WaitList
{
  public class WaitListServiceTests
  {
    private readonly WaitListService _waitListService;
    private readonly Mock<MasterDbContext> _masterContext = new();
    private readonly Mock<ReplicaDbContext> _replicaContext = new();

    public WaitListServiceTests()
    {
      var databaseFacadeMock = new Mock<DatabaseFacade>(_masterContext.Object);
      _masterContext.SetupGet(x => x.Database).Returns(databaseFacadeMock.Object);
      _masterContext.Setup(x => x.Database.BeginTransaction())
        .Returns(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);

      var databaseFacadeMockReplica = new Mock<DatabaseFacade>(_replicaContext.Object);
      _replicaContext.SetupGet(x => x.Database).Returns(databaseFacadeMockReplica.Object);
      _replicaContext.Setup(x => x.Database.BeginTransaction())
        .Returns(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);

      _waitListService = new WaitListService(_masterContext.Object, _replicaContext.Object);
    }

    [Fact]
    public async Task InsertWaitList_ReturnWaitListId()
    {
      // Arrange
      _masterContext.SetupGet(x => x.WaitList).ReturnsDbSet(new List<WaitListEntity> { });
      _replicaContext.SetupGet(x => x.WaitList).ReturnsDbSet(new List<WaitListEntity> { });

      // Act
      var email = "test@mailinator.com";
      var schoolId = Guid.Parse("a9224ae3-4f6c-11ed-ba4c-02c3445b1234");
      var waitListId = _waitListService.InsertIfNotExist(email, schoolId);
      // Assert
      Assert.NotNull(waitListId);
    }

    [Fact]
    public async Task CountTotalWaitList_ReturnTotalWaitList()
    {
      // Arrange
      _replicaContext.SetupGet(x => x.WaitList).ReturnsDbSet(new List<WaitListEntity>
      {
        new WaitListEntity
        {
          Guid = Guid.NewGuid(),
          Email = "test@mailinator.com",
          SchoolId = Guid.Parse("a9224ae3-4f6c-11ed-ba4c-02c3445b1234")
        },
        new WaitListEntity
        {
          Guid = Guid.NewGuid(),
          Email = "test2@mailinator.com",
          SchoolId = Guid.Parse("a9224ae3-4f6c-11ed-ba4c-02c3445b1234")
        }
      });

      // Act
      var schoolId = Guid.Parse("a9224ae3-4f6c-11ed-ba4c-02c3445b1234");
      var totalWaitList = _waitListService.CountTotalInWaitList(schoolId);

      // Assert
      Assert.Equal(2, totalWaitList);
    }

    [Fact]
    public async Task GetWaitListInfoByEmail_ReturnObjectNotNull()
    {
      // Arrange
      _replicaContext.SetupGet(x => x.WaitList).ReturnsDbSet(new List<WaitListEntity>
      {
        new WaitListEntity
        {
          Guid = Guid.NewGuid(),
          Email = "test@mailinator.com",
          SchoolId = Guid.Parse("a9224ae3-4f6c-11ed-ba4c-02c3445b1234")
        },
        new WaitListEntity
        {
          Guid = Guid.NewGuid(),
          Email = "test2@mailinator.com",
          SchoolId = Guid.Parse("a9224ae3-4f6c-11ed-ba4c-02c3445b1234")
        }
      });

      // Act
      var result = _waitListService.GetWaitListInfoByEmail("test@mailinator.com");

      // Assert
      Assert.NotNull(result);
    }

    [Fact]
    public async Task Delete_ReturnTrue()
    {
      // Arrange
      _replicaContext.SetupGet(x => x.WaitList).ReturnsDbSet(new List<WaitListEntity>
      {
        new WaitListEntity
        {
          Guid = Guid.NewGuid(),
          Email = "test@mailinator.com",
          SchoolId = Guid.Parse("a9224ae3-4f6c-11ed-ba4c-02c3445b1234")
        },
        new WaitListEntity
        {
          Guid = Guid.NewGuid(),
          Email = "test2@mailinator.com",
          SchoolId = Guid.Parse("a9224ae3-4f6c-11ed-ba4c-02c3445b1234")
        }
      });

      _masterContext.SetupGet(x => x.WaitList).ReturnsDbSet(new List<WaitListEntity>
      {
        new WaitListEntity
        {
          Guid = Guid.NewGuid(),
          Email = "test@mailinator.com",
          SchoolId = Guid.Parse("a9224ae3-4f6c-11ed-ba4c-02c3445b1234")
        },
        new WaitListEntity
        {
          Guid = Guid.NewGuid(),
          Email = "test2@mailinator.com",
          SchoolId = Guid.Parse("a9224ae3-4f6c-11ed-ba4c-02c3445b1234")
        }
      });

      _masterContext.Setup(x => x.SaveChanges())
        .Returns(1)
        .Verifiable();

      // Act
      var result = _waitListService.Delete("test@mailinator.com");

      // Assert
      Assert.True(result);
    }
  }
}

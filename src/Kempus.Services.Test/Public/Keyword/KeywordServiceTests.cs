﻿using Kempus.Core;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Services.Public.Keyword;
using Moq;
using Moq.EntityFrameworkCore;
using Xunit;

namespace Kempus.Services.Test.Public.Keyword
{
  public class KeywordServiceTests
  {
    private readonly KeywordService _keywordService;
    private readonly Mock<MasterDbContext> _masterDbContext = new();
    private readonly Mock<SessionStore> _sessionStore = new();

    public KeywordServiceTests()
    {
      _keywordService = new KeywordService(_masterDbContext.Object, _sessionStore.Object);
    }

    [Fact]
    public async Task InsertAndGetIds()
    {
      // Arrange
      _masterDbContext.Setup(x => x.Keywords).ReturnsDbSet(new List<KeywordEntity>());
      var keywords = new List<string>
      {
        "key1", "key2"
      };

      // Act
      var result = _keywordService.InsertAndGetIds(keywords);

      // Assert
      Assert.Equal(result.Count, keywords.Count);
    }
  }
}
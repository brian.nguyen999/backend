﻿using Kempus.Core.Models.Configuration;
using Kempus.EntityFramework;
using Kempus.Models.Public.User.Request;
using Kempus.Services.Public.Notification;
using Kempus.Services.Public.Otp;
using Kempus.Services.Test.DataSeeding;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.EntityFrameworkCore;
using Xunit;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.Test.Public.Otp
{
  public class OtpServiceTest
  {
    private readonly OtpService _otpService;
    private readonly Mock<ILogger<OtpService>> _logger;
    private readonly Mock<MasterDbContext> _masterContext;
    private readonly Mock<UserConfig> _userConfig = new Mock<UserConfig>();
    private readonly Mock<IConfiguration> _configuration = new();
    private readonly Mock<IEmailNotificationService> _notificationService = new();

    public OtpServiceTest()
    {
      _masterContext = new Mock<MasterDbContext>();
      _logger = new Mock<ILogger<OtpService>>();
      _userConfig.Object.OtpValidityInMinutes = 5;

      _otpService = new OtpService(
        _masterContext.Object, 
        _logger.Object, 
        _configuration.Object, 
        _userConfig.Object,
        _notificationService.Object
      );

      var databaseFacadeMock = new Mock<DatabaseFacade>(_masterContext.Object);
      _masterContext.SetupGet(x => x.Database).Returns(databaseFacadeMock.Object);
      _masterContext.Setup(x => x.Database.BeginTransactionAsync(default))
        .ReturnsAsync(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);
    }

    // TODO Update unit test after refactor
    //[Fact]
    //public async Task InsertAsync_Should_ReturnNotNull()
    //{
    //  // Arrange
    //  _masterContext.SetupGet(x => x.Otp).ReturnsDbSet(MockDataHelper.GetOtpMockData());
    //  _masterContext.Setup(c => c.SaveChangesAsync(default))
    //    .Returns(Task.FromResult(1))
    //    .Verifiable();

    //  // Act
    //  var newObjectOtp = new OtpRequestDto()
    //  {
    //    ObjectType = (byte)OtpType.Email,
    //    ObjectId = TestBaseConstants.ApplicationUsersDataTest[0].Email
    //  };
    //  var result = _otpService.Create(newObjectOtp);

    //  // Assert
    //  Assert.NotNull(result);
    //}

    [Fact]
    public async Task GetByIdAndTypeAsync_Should_ReturnNotNull()
    {
      // Arrange
      _masterContext.SetupGet(x => x.Otp).ReturnsDbSet(MockDataHelper.GetOtpMockData());

      // Act
      var objectOtp = new OtpRequestDto()
      {
        ObjectType = (byte)OtpType.Email,
        ObjectId = TestBaseConstants.ApplicationUsersDataTest[1].Email
      };
      var result = await _otpService.GetByIdAndTypeAsync(objectOtp);

      // Assert
      Assert.NotNull(result);
    }

    [Fact]
    public async Task CheckOtpValidAsync_Should_ReturnTrue()
    {
      // Arrange
      _masterContext.SetupGet(x => x.Otp).ReturnsDbSet(MockDataHelper.GetOtpMockData());

      // Act
      var objectOtp = new OtpRequestDto()
      {
        ObjectType = (byte)OtpType.Email,
        ObjectId = TestBaseConstants.ApplicationUsersDataTest[1].Email,
        OtpCode = "123456"
      };

      var result = await _otpService.CheckOtpValidAsync(objectOtp);

      // Assert
      Assert.True(result);
    }

    [Fact]
    public async Task CheckObjectExistAsync_Should_ReturnTrue()
    {
      // Arrange
      _masterContext.SetupGet(x => x.Otp).ReturnsDbSet(MockDataHelper.GetOtpMockData());

      // Act
      var objectOtp = new OtpRequestDto()
      {
        ObjectType = (byte)OtpType.Email,
        ObjectId = TestBaseConstants.ApplicationUsersDataTest[1].Email,
        OtpCode = "123456"
      };

      var result = await _otpService.CheckObjectExistAsync(objectOtp);

      // Assert
      Assert.True(result);
    }

    [Fact]
    public async Task CheckOtpIsExpired_Should_ReturnFalse()
    {
      // Arrange

      // Act
      var objectOtp = MockDataHelper.GetOtpMockData()[0];

      var result = _otpService.CheckOtpIsExpired(objectOtp.CreatedDate);

      // Assert
      Assert.False(result);
    }

    [Fact]
    public async Task DeleteOtpAsync_Should_ReturnTrue()
    {
      // Arrange
      _masterContext.SetupGet(x => x.Otp).ReturnsDbSet(MockDataHelper.GetOtpMockData());
      _masterContext.Setup(c => c.SaveChangesAsync(default))
        .Returns(Task.FromResult(1))
        .Verifiable();

      // Act
      var objectOtp = new OtpRequestDto()
      {
        ObjectType = (byte)OtpType.Email,
        ObjectId = TestBaseConstants.ApplicationUsersDataTest[1].Email,
      };

      var result = await _otpService.DeleteOtpAsync(objectOtp);

      // Assert
      Assert.True(result);
    }
  }
}
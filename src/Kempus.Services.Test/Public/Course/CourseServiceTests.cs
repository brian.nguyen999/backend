﻿using Kempus.EntityFramework;
using Kempus.Services.Common.Queue;
using Kempus.Services.FullTextSearch.Course;
using Kempus.Services.Public.Course;
using Kempus.Services.Public.DepartmentProgram;
using Kempus.Services.Test.DataSeeding;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Moq;
using Moq.EntityFrameworkCore;
using Xunit;

namespace Kempus.Services.Test.Public.Course
{
  public class CourseServiceTests
  {
    private readonly CourseService _courseService;
    private readonly Mock<ReplicaDbContext> _replicaContext = new();
    private readonly Mock<MasterDbContext> _masterDbContext = new();
    private readonly Mock<ICourseFtsService> _courseFtsService = new();
    private readonly Mock<IOpenSearchEventPublisher> _openSearchEventPublisher = new();
    private readonly Mock<IDepartmentProgramService> _departmentProgramService = new();

    public CourseServiceTests()
    {
      _courseService = new CourseService(_replicaContext.Object, _masterDbContext.Object,
        _courseFtsService.Object, _openSearchEventPublisher.Object, _departmentProgramService.Object);

      var databaseFacadeMockReplica = new Mock<DatabaseFacade>(_replicaContext.Object);
      _replicaContext.SetupGet(x => x.Database).Returns(databaseFacadeMockReplica.Object);
      _replicaContext.Setup(x => x.Database.BeginTransaction())
        .Returns(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);

      var databaseFacadeMockMaster = new Mock<DatabaseFacade>(_masterDbContext.Object);
      _masterDbContext.SetupGet(x => x.Database).Returns(databaseFacadeMockMaster.Object);
      _masterDbContext.Setup(x => x.Database.BeginTransaction())
        .Returns(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);
    }

    [Fact]
    public async Task GetDetail_InvalidCourseId_ReturnNull()
    {
      // Arrange
      _replicaContext.Setup(x => x.Courses).ReturnsDbSet(MockDataHelper.GetCourseMockData());

      // Act
      var course = _courseService.GetDetail(Guid.NewGuid());

      // Assert
      Assert.Equal(null, course);
    }

    [Fact]
    public async Task GetDetail_ReturnCourseData()
    {
      // Arrange
      _replicaContext.Setup(x => x.Courses).ReturnsDbSet(MockDataHelper.GetCourseMockData());
      _replicaContext.Setup(x => x.Instructors).ReturnsDbSet(MockDataHelper.GetInstructorMockData());
      _replicaContext.Setup(x => x.CourseInstructor).ReturnsDbSet(MockDataHelper.GetCourseInstructorMockData());

      // Act
      var courseId = Guid.Parse("dfed759e-5a6a-4e8d-9496-9d480c9d6d3f");
      var course = _courseService.GetDetail(courseId);

      // Assert
      Assert.Equal(courseId, course.Id);
    }
  }
}
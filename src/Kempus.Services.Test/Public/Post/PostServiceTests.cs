﻿using Kempus.Core;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Post;
using Kempus.Services.Public.Bookmark;
using Kempus.Services.Public.Comment;
using Kempus.Services.Public.Post;
using Kempus.Services.Public.PostKeyword;
using Kempus.Services.Public.PostPollLike;
using Kempus.Services.Public.Topic;
using Kempus.Services.Public.UserTracking;
using Kempus.Services.Test.DataSeeding;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Moq;
using Moq.EntityFrameworkCore;
using Xunit;

namespace Kempus.Services.Test.Public.Post
{
  public class PostServiceTests
  {
    private readonly PostService _postService;
    private readonly Mock<MasterDbContext> _masterDbContext = new();
    private readonly Mock<ReplicaDbContext> _replicaDbContext = new();
    private readonly Mock<IPostKeywordService> _postKeywordService = new();
    private readonly TopicService _topicService;
    private readonly Mock<IBookmarkService> _bookmarkService = new();
    private readonly Mock<IPostPollLikeService> _postPollLikeService = new();
    private readonly SessionStore _sessionStore = new();
    private readonly Mock<IHttpContextAccessor> _httpContextAccessorMock = new();
    private readonly Mock<IConfigurationSection> _configurationSection = new();
    private readonly Mock<IConfiguration> _configuration = new();
    private readonly Mock<IUserTrackingService> _userTrackingService = new();
    private readonly Mock<IPostCommentService> _postCommentService = new();

    public PostServiceTests()
    {
      _sessionStore = new SessionStore(_httpContextAccessorMock.Object);
      _topicService = new TopicService(_masterDbContext.Object, _replicaDbContext.Object);
      _postService = new PostService(_configuration.Object, _masterDbContext.Object, _replicaDbContext.Object,
        _sessionStore, _postKeywordService.Object, _topicService, _bookmarkService.Object,
        _postPollLikeService.Object, _postCommentService.Object, _userTrackingService.Object);

      var databaseFacadeMockReplica = new Mock<DatabaseFacade>(_replicaDbContext.Object);
      _replicaDbContext.SetupGet(x => x.Database).Returns(databaseFacadeMockReplica.Object);
      _replicaDbContext.Setup(x => x.Database.BeginTransaction())
        .Returns(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);

      var databaseFacadeMockMaster = new Mock<DatabaseFacade>(_masterDbContext.Object);
      _masterDbContext.SetupGet(x => x.Database).Returns(databaseFacadeMockMaster.Object);
      _masterDbContext.Setup(x => x.Database.BeginTransaction())
        .Returns(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);
    }

    [Theory]
    [MemberData(nameof(TestBaseConstants.PostSearchData), MemberType = typeof(TestBaseConstants))]
    public async Task SearchPost_ReturnData(Guid? postId, Guid? topicId, string? keyword, int expectedRows)
    {
      // Arrange
      _masterDbContext.SetupGet(x => x.Users).ReturnsDbSet(MockDataHelper.GetApplicationUserMockData());
      _masterDbContext.SetupGet(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
      _masterDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _masterDbContext.SetupGet(x => x.Keywords).ReturnsDbSet(GetKeywordMockData());
      _masterDbContext.SetupGet(x => x.PostKeywords).ReturnsDbSet(GetPostKeywordMockData());
      _masterDbContext.SetupGet(x => x.Topics).ReturnsDbSet(MockDataHelper.GetTopicMockData());

      var model = new PostFilterModel
      {
        PostIds = postId != null ? new List<Guid> { postId.Value } : new List<Guid>(),
        Keyword = keyword,
        TopicId = topicId
      };

      // Act
      var result = _postService.Search(model, new Pageable(), true);

      // Assert
      Assert.NotNull(result);
      Assert.Equal(result.TotalCount, expectedRows);
    }

    [Fact]
    public async Task GetDetailPost_ReturnPostData()
    {
      // Arrange
      _replicaDbContext.Setup(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
      _replicaDbContext.Setup(x => x.Topics).ReturnsDbSet(MockDataHelper.GetTopicMockData());
      _replicaDbContext.Setup(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());

      // Setup mock config
      _configurationSection.Setup(x => x.Value).Returns("60");
      _configuration.Setup(x => x.GetSection(It.Is<string>(k => k == "LimitedTimeForEditPostPollInMinutes")))
        .Returns(_configurationSection.Object);

      // Act
      var result = _postService.GetDetail(TestBaseConstants.PostId1);

      // Assert
      Assert.NotNull(result);
      Assert.Equal(result.Id, TestBaseConstants.PostId1);
    }

    [Fact]
    public async Task InsertPost_ReturnPostId()
    {
      // Arrange
      _masterDbContext.SetupGet(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
      _masterDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _masterDbContext.SetupGet(x => x.Topics).ReturnsDbSet(MockDataHelper.GetTopicMockData());
      _replicaDbContext.Setup(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
      _replicaDbContext.Setup(x => x.Topics).ReturnsDbSet(MockDataHelper.GetTopicMockData());
      _replicaDbContext.Setup(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());

      // Act
      var postData = new PostInputModel
      {
        Title = "New post 1",
        Keywords = new List<string>()
        {
          "Keyword 1",
          "Keyword 1"
        },
        Content = "Post Content"
      };
      var postId = _postService.Create(postData);

      // Assert
      Assert.NotNull(postId);
    }

    [Fact]
    public async Task InsertPost_WrongTopic_ReturnException()
    {
      // Arrange
      _masterDbContext.SetupGet(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
      _masterDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _masterDbContext.SetupGet(x => x.Topics).ReturnsDbSet(MockDataHelper.GetTopicMockData());

      // Act
      var postData = new PostInputModel
      {
        Title = "New post 1",
        Keywords = new List<string>()
        {
          "Keyword 1",
          "Keyword 1"
        },
        Content = "Post Content",
        TopicId = new Guid("8bbc18ea-aab1-4f19-ae2d-376337ae7ece")
      };

      var ex = Assert.Throws<NotFoundException>(() => _postService.Create(postData));

      // Assert
      Assert.Equal(ErrorMessages.Topic_NotFound, ex.Message);
    }

    [Fact]
    public async Task InsertPost_MissingKeyword_ReturnException()
    {
      // Arrange
      _masterDbContext.SetupGet(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
      _masterDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _masterDbContext.SetupGet(x => x.Topics).ReturnsDbSet(MockDataHelper.GetTopicMockData());

      // Act
      var postData = new PostInputModel
      {
        Title = "New post 1",
        Keywords = new List<string>(),
        Content = "Post Content",
      };

      var ex = Assert.Throws<BadRequestException>(() => _postService.Create(postData));

      // Assert
      Assert.Equal(ErrorMessages.Post_KeywordsInvalid, ex.Message);
    }

    [Fact]
    public async Task UpdatePost_WrongPostId_ReturnException()
    {
      // Arrange
      _masterDbContext.SetupGet(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
      _masterDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _masterDbContext.SetupGet(x => x.Topics).ReturnsDbSet(MockDataHelper.GetTopicMockData());
      _replicaDbContext.SetupGet(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
      _replicaDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _replicaDbContext.SetupGet(x => x.Topics).ReturnsDbSet(MockDataHelper.GetTopicMockData());
      // Act
      var postData = new PostInputModel
      {
        Id = Guid.NewGuid(),
        Title = "New post 1",
        Keywords = new List<string>()
        {
          "Keyword 1",
          "Keyword 1"
        },
        Content = "Post Content"
      };

      var ex = Assert.Throws<NotFoundException>(() => _postService.Create(postData));

      // Assert
      Assert.Equal(ErrorMessages.Post_NotFound, ex.Message);
    }

    [Fact]
    public async Task ValidateBeforeInsert_WrongTopic_ReturnException()
    {
      // Arrange
      _masterDbContext.SetupGet(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
      _masterDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _masterDbContext.SetupGet(x => x.Topics).ReturnsDbSet(MockDataHelper.GetTopicMockData());

      // Act
      var postData = new PostInputModel
      {
        Title = "New post 1",
        Keywords = new List<string>()
        {
          "Keyword 1",
          "Keyword 1"
        },
        Content = "Post Content",
        TopicId = Guid.NewGuid()
      };

      var ex = Assert.Throws<NotFoundException>(() => _postService.ValidateBeforeInsertOrUpdate(postData));

      // Assert
      Assert.Equal(ErrorMessages.Topic_NotFound, ex.Message);
    }

    [Fact]
    public async Task ValidateBeforeInsert_MissingKeyword_ReturnException()
    {
      // Arrange
      _masterDbContext.SetupGet(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
      _masterDbContext.SetupGet(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _masterDbContext.SetupGet(x => x.Topics).ReturnsDbSet(MockDataHelper.GetTopicMockData());

      // Act
      var postData = new PostInputModel
      {
        Title = "New post 1",
        Keywords = new List<string>(),
        Content = "Post Content",
      };

      var ex = Assert.Throws<BadRequestException>(() => _postService.ValidateBeforeInsertOrUpdate(postData));

      // Assert
      Assert.Equal(ErrorMessages.Post_KeywordsInvalid, ex.Message);
    }

    [Fact]
    public async Task UpdatePost_ReturnPostData()
    {
      // Arrange
      _masterDbContext.Setup(x => x.Users).ReturnsDbSet(MockDataHelper.GetApplicationUserMockData());
      _masterDbContext.Setup(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
      _masterDbContext.Setup(x => x.Topics).ReturnsDbSet(MockDataHelper.GetTopicMockData());
      _masterDbContext.Setup(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _masterDbContext.Setup(x => x.Polls).ReturnsDbSet(MockDataHelper.GetPollMockData());
      _masterDbContext.Setup(x => x.PollOptions).ReturnsDbSet(MockDataHelper.GetPollOptionMockData());
      _replicaDbContext.Setup(x => x.Users).ReturnsDbSet(MockDataHelper.GetApplicationUserMockData());
      _replicaDbContext.Setup(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
      _replicaDbContext.Setup(x => x.Topics).ReturnsDbSet(MockDataHelper.GetTopicMockData());
      _replicaDbContext.Setup(x => x.Posts).ReturnsDbSet(MockDataHelper.GetPostMockData());
      _replicaDbContext.Setup(x => x.Polls).ReturnsDbSet(MockDataHelper.GetPollMockData());
      _replicaDbContext.Setup(x => x.PollOptions).ReturnsDbSet(MockDataHelper.GetPollOptionMockData());

      // Setup mock config
      _configurationSection.Setup(x => x.Value).Returns("60");
      _configuration.Setup(x => x.GetSection(It.Is<string>(k => k == "LimitedTimeForEditPostPollInMinutes")))
        .Returns(_configurationSection.Object);

      var result = _postService.Search(new PostFilterModel { PostIds = new List<Guid> { TestBaseConstants.PostId2 } }, new Pageable(), true);
      var existPost = result.Data.FirstOrDefault();
      Assert.NotNull(existPost);
      Assert.Equal(existPost.Id, TestBaseConstants.PostId2);

      var postUpdateModel = new PostInputModel
      {
        Title = "post1 Update",
        Id = existPost.Id,
        Keywords = new List<string> { "string 1" },
        Content = existPost.Content,
        TopicId = existPost.TopicId,
        UpdatedBy = existPost.CreatedBy
      };

      // Act
      var updateResult = _postService.Update(postUpdateModel);

      // Assert
      Assert.NotNull(updateResult);
      Assert.Equal(updateResult.Title, postUpdateModel.Title);
    }

    public List<KeywordEntity> GetKeywordMockData()
    {
      return new List<KeywordEntity>
      {
        new KeywordEntity
        {
          Id = 1,
          Name = "keyword1",
          SchoolId = Guid.Parse("a8c5f021-4f6c-11ed-ba4c-02c3445b6529"),
          Guid = Guid.Parse("f736d465-3a2e-4b9a-a953-62013f923a9a")
        }
      };
    }

    public List<PostKeywordEntity> GetPostKeywordMockData()
    {
      return new List<PostKeywordEntity>
      {
        new PostKeywordEntity
        {
          Id = 1,
          PostId = TestBaseConstants.PostId1,
          KeywordId = Guid.Parse("f736d465-3a2e-4b9a-a953-62013f923a9a"),
          Guid = Guid.Parse("01124e4e-ea1f-42e0-9190-18125864b75c")
        }
      };
    }
  }
}
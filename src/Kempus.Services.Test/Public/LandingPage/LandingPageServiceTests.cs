﻿//using Kempus.EntityFramework;
//using Kempus.Services.Public.LandingPage;
//using Kempus.Services.Test.DataSeeding;
//using Microsoft.Extensions.Logging;
//using Moq;
//using Moq.EntityFrameworkCore;
//using Xunit;

//namespace Kempus.Services.Test.Public.LandingPage
//{
//  public class LandingPageServiceTests
//  {
//    private readonly LandingPageService _landingPageService;
//    private readonly Mock<ILogger<LandingPageService>> _logger = new();
//    private readonly Mock<MasterDbContext> _masterContext = new();
//    private readonly Mock<ReplicaDbContext> _replicaContext = new();

//    public LandingPageServiceTests()
//    {
//      _landingPageService = new LandingPageService(_logger.Object, _replicaContext.Object, _masterContext.Object);
//    }

//    [Fact]
//    public async Task GetSchoolData()
//    {
//      // Arrange
//      _replicaContext.Setup(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());

//      // Act
//      var size = 1;
//      var schoolData = _landingPageService.GetSchoolData(new GetLandingPageSchoolRequestDto
//      {
//        Size = size
//      });

//      // Assert
//      Assert.NotNull(schoolData);
//      Assert.NotNull(schoolData.OpenedSchools);
//      Assert.NotNull(schoolData.WaitedSchools);
//    }

//    //[Fact]
//    //public async Task GetSchoolData_Should_ReturnWithLimit()
//    //{
//    //  // Arrange
//    //  _replicaContext.SetupGet(x => x.Schools).ReturnsDbSet(MockDataHelper.GetSchoolMockData());
//    //  _replicaContext.SetupGet(x => x.SchoolLifeTimeAnalytics).ReturnsDbSet(MockDataHelper.GetSchoolAnalyticMockData());

//    //  // Act
//    //  var size = 1;
//    //  var schoolData = _landingPageService.GetSchoolData(new GetLandingPageSchoolRequestDto
//    //  {
//    //    Size = size
//    //  });

//    //  // Assert
//    //  Assert.NotNull(schoolData);
//    //  Assert.NotNull(schoolData.OpenedSchools);
//    //  Assert.NotNull(schoolData.WaitedSchools);
//    //  Assert.Equal(size, schoolData.OpenedSchools.Count);
//    //  Assert.Equal(size, schoolData.WaitedSchools.Count);
//    //}
//  }
//}
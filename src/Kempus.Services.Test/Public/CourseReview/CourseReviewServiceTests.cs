﻿using Kempus.Core.Errors;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Course;
using Kempus.Models.Public.CourseReview;
using Kempus.Services.Common.KempTransaction;
using Kempus.Services.Common.Queue;
using Kempus.Services.FullTextSearch.Course;
using Kempus.Services.FullTextSearch.CourseReview;
using Kempus.Services.Public.Course;
using Kempus.Services.Public.CourseReview;
using Kempus.Services.Public.CourseReviewLike;
using Kempus.Services.Public.CourseSummary;
using Kempus.Services.Public.DepartmentProgram;
using Kempus.Services.Public.User;
using Kempus.Services.Public.UserReferralStatistic;
using Kempus.Services.Test.DataSeeding;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Moq;
using Moq.EntityFrameworkCore;
using Xunit;
using static Kempus.Core.Constants.CoreConstants;

namespace Kempus.Services.Test.Public.CourseReview
{
  public class CourseReviewServiceTests
  {
    private readonly CourseReviewService _courseReviewService;
    private readonly Mock<MasterDbContext> _masterDbContext = new();
    private readonly Mock<ReplicaDbContext> _replicaDbContext = new();
    private readonly Mock<ICourseService> _courseService = new();
    private readonly Mock<IUserService> _userService = new();
    private readonly Mock<ICourseSummaryService> _courseSummaryService = new();
    private readonly Mock<ICourseFtsService> _courseFtsService = new();
    private readonly Mock<ICourseReviewLikeService> _courseReviewLikeService = new();
    private readonly Mock<IKempTransactionCommonService> _kempTransactionService = new();
    private readonly Mock<IUserReferralStatisticService> _userReferralStatisticService = new();
    private readonly Mock<IDepartmentProgramService> _departmentProgramService = new();
    private readonly Mock<IOpenSearchEventPublisher> _openSearchEventPublisher = new();
    private readonly Mock<ICourseReviewFtsService> _courseReviewFtsService = new();

    public CourseReviewServiceTests()
    {
      _courseReviewService = new CourseReviewService(
        _replicaDbContext.Object,
        _masterDbContext.Object,
        _courseService.Object,
        _userService.Object,
        _courseSummaryService.Object,
        _courseFtsService.Object,
        _courseReviewLikeService.Object,
        _kempTransactionService.Object,
        _userReferralStatisticService.Object,
        _departmentProgramService.Object,
        _openSearchEventPublisher.Object,
        _courseReviewFtsService.Object
      );

      var databaseFacadeMockReplica = new Mock<DatabaseFacade>(_replicaDbContext.Object);
      _replicaDbContext.SetupGet(x => x.Database).Returns(databaseFacadeMockReplica.Object);
      _replicaDbContext.Setup(x => x.Database.BeginTransaction())
        .Returns(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);

      var databaseFacadeMockMaster = new Mock<DatabaseFacade>(_masterDbContext.Object);
      _masterDbContext.SetupGet(x => x.Database).Returns(databaseFacadeMockMaster.Object);
      _masterDbContext.Setup(x => x.Database.BeginTransaction())
        .Returns(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);
    }

    [Fact]
    public async Task CreateCourseReview_WrongCourseId_ReturnException()
    {
      // Arrange
      _replicaDbContext.SetupGet(x => x.Courses).ReturnsDbSet(MockDataHelper.GetCourseMockData());
      _masterDbContext.SetupGet(x => x.CourseReviews).ReturnsDbSet(MockDataHelper.GetCourseReviewEmptyMockData());

      // Act
      var input = new CourseReviewInputModel
      {
        CourseId = Guid.NewGuid(),
        IsRequiredTextBook = false,
        DifficultRating = 1,
        HoursSpend = 1,
        NumberOfExams = 1,
        IsRequiredGroupProject = false,
        GradingCriteriaRating = 1,
        UserGrade = "A",
        UserAchievementRating = 1,
        Characteristics = new List<string>()
        {
          "GIVES_GOOD_FEEDBACK",
          "INSPIRATIONAL",
          "HILARIOUS",
          "CARING",
          "RESPECTED",
          "AMAZING_LECTURES"
        },
        RecommendRating = 1,
        Comment = "Good course"
      };

      // Assert
      var ex = Assert.Throws<NotFoundException>(() => _courseReviewService.Create(input, null, null));
      Assert.Equal(ErrorMessages.Course_NotFound, ex.Message);
    }

    [Fact]
    public async Task CreateCourseReview_ReturnId()
    {
      // Arrange
      _replicaDbContext.SetupGet(x => x.Courses).ReturnsDbSet(MockDataHelper.GetCourseMockData());
      _masterDbContext.SetupGet(x => x.CourseReviews).ReturnsDbSet(MockDataHelper.GetCourseReviewEmptyMockData());
      _courseService.Setup(x => x.GetDetail(It.IsAny<Guid>()))
        .Returns(new CourseDetailModel
        {
          Id = Guid.NewGuid(),
          Name = "Analytical Chemistry"
        })
        .Verifiable();
      // Act
      var input = new CourseReviewInputModel
      {
        CourseId = Guid.NewGuid(),
        IsRequiredTextBook = false,
        DifficultRating = 1,
        HoursSpend = 1,
        NumberOfExams = 1,
        IsRequiredGroupProject = false,
        GradingCriteriaRating = 1,
        UserGrade = "A",
        UserAchievementRating = 1,
        Characteristics = new List<string>()
        {
          "GIVES_GOOD_FEEDBACK",
          "INSPIRATIONAL",
          "HILARIOUS",
          "CARING",
          "RESPECTED",
          "AMAZING_LECTURES"
        },
        RecommendRating = 1,
        Comment = "Good course"
      };
      var result = _courseReviewService.Create(input, null, null);

      // Assert
      Assert.NotNull(result);
    }

    [Fact]
    public async Task GetDetailCourseReview_ValidId_ReturnData()
    {
      // Arrange
      _replicaDbContext.SetupGet(x => x.Courses).ReturnsDbSet(MockDataHelper.GetCourseMockData());
      _replicaDbContext.SetupGet(x => x.Instructors).ReturnsDbSet(MockDataHelper.GetInstructorMockData());
      _replicaDbContext.SetupGet(x => x.CourseReviews).ReturnsDbSet(MockDataHelper.GetCourseReviewMockData());
      _replicaDbContext.SetupGet(x => x.CourseInstructor).ReturnsDbSet(MockDataHelper.GetCourseInstructorMockData());
      _replicaDbContext.SetupGet(x => x.CourseSummaries).ReturnsDbSet(GetCourseSummaryMockData());

      // Act
      var result = _courseReviewService.GetDetail(new Guid(SeedingDefaultData.CourseId));

      // Assert
      Assert.NotNull(result);
    }

    [Fact]
    public async Task GetDetailCourseReview_InValidId_ReturnException()
    {
      // Arrange
      _replicaDbContext.SetupGet(x => x.Courses).ReturnsDbSet(MockDataHelper.GetCourseMockData());
      _replicaDbContext.SetupGet(x => x.Instructors).ReturnsDbSet(MockDataHelper.GetInstructorMockData());
      _replicaDbContext.SetupGet(x => x.CourseReviews).ReturnsDbSet(MockDataHelper.GetCourseReviewMockData());
      _replicaDbContext.SetupGet(x => x.CourseInstructor).ReturnsDbSet(MockDataHelper.GetCourseInstructorMockData());

      // Act
      var result = Assert.Throws<NotFoundException>(() =>
        _courseReviewService.GetDetail(new Guid("cc881d93-1065-4932-83ed-37969250afcf")));

      // Assert
      Assert.Equal(ErrorMessages.Course_NotFound, result.Message);
    }

    private List<CourseSummaryEntity> GetCourseSummaryMockData()
    {
      return new List<CourseSummaryEntity>()
      {
        new CourseSummaryEntity
        {
          Id = 1,
          CourseId = new Guid(SeedingDefaultData.CourseId),
          Guid = new Guid("38cd7ede-e23f-4e2f-8a00-9c9ce21a15cd"),
          TotalReviews = 1,
          IsTextbookRequired = false,
          NumberOfExams = 1,
          IsGroupProjects = true,
          RecommendedRate = 1,
          GradingCriteriaRate = 1,
          CourseDifficultyRate = 2,
          TopThreeCharacteristicInstructors = "['GIVES_GOOD_FEEDBACK','RESPECTED','HILARIOUS']",
          StudentGradePercents = "[{\"grade\":\"A\",\"percent\":14},{\"grade\":\"B\",\"percent\":0},{\"grade\":\"C\",\"percent\":86},{\"grade\":\"D\",\"percent\":0},{\"grade\":\"F\",\"percent\":0},{\"grade\":\"Pass\",\"percent\":0}]",
          HoursPerWeekRate = 1,
          GoalsAchivedRate = 1
        },
      };
    }
  }
}
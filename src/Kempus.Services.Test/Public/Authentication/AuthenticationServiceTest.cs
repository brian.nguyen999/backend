﻿using Kempus.Core.Models.Configuration;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Authorization.Response;
using Kempus.Models.Public.Authentication.Request;
using Kempus.Services.Common.HttpsContext;
using Kempus.Services.Common.KempTransaction;
using Kempus.Services.Public.Authentication;
using Kempus.Services.Public.Authentication.JWT;
using Kempus.Services.Public.Authorization;
using Kempus.Services.Public.User;
using Kempus.Services.Test.DataSeeding;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.Test.Public.Authentication
{
  public class AuthenticationServiceTest
  {
    private readonly AuthenticationService _authenticationService;
    private readonly Mock<ILogger<AuthenticationService>> _logger = new Mock<ILogger<AuthenticationService>>();
    private readonly Mock<MasterDbContext> _masterContext = new();
    private readonly Mock<IJwtService> _jwtService = new Mock<IJwtService>();
    private readonly Mock<JwtConfiguration> _jwtConfiguration = new Mock<JwtConfiguration>();
    private readonly Mock<IUserPermissionService> _userPermissionService = new Mock<IUserPermissionService>();
    private readonly Mock<IHttpContextService> _httpContextService = new Mock<IHttpContextService>();
    private readonly Mock<IUserService> _userService = new Mock<IUserService>();
    private readonly Mock<ReplicaDbContext> _replicaDbContext = new();
    private readonly Mock<IKempTransactionCommonService> _kempTransactionService = new();

    public AuthenticationServiceTest()
    {
      _masterContext = new Mock<MasterDbContext>();
      _authenticationService = new AuthenticationService(
        _masterContext.Object,
        _jwtService.Object,
        _jwtConfiguration.Object,
        _userPermissionService.Object,
        _logger.Object,
        _httpContextService.Object,
        _userService.Object,
        _replicaDbContext.Object,
        _kempTransactionService.Object
      );

      var databaseFacadeMock = new Mock<DatabaseFacade>(_masterContext.Object);
      _masterContext.SetupGet(x => x.Database).Returns(databaseFacadeMock.Object);
      _masterContext.Setup(x => x.Database.BeginTransactionAsync(default))
        .ReturnsAsync(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);
    }

    //[Fact]
    //public async Task GetAuthenticationResultAsync_Should_ReturnResultNotNull()
    //{
    //  // Arrange
    //  _userService.Setup(x => x.GetUserInfoByUsernameAsync(It.IsAny<string>()))
    //    .ReturnsAsync(TestBaseConstants.ApplicationUsersDataTest[0])
    //    .Verifiable();
    //  _userPermissionService.Setup(x => x.GetObjectHasPermissionListModel(It.IsAny<Guid>()))
    //    .ReturnsAsync(new ObjectHasPermissionListModel())
    //    .Verifiable();
    //  _jwtService.Setup(x => x.GenerateToken(It.IsAny<List<Claim>>()))
    //    .Returns(TestBaseConstants.AccessTokenTest)
    //    .Verifiable();
    //  _jwtConfiguration.Object.RefreshTokenValidityInMinutes = TestBaseConstants.RefreshTokenValidityInMinutesTest;

    //  _masterContext.Setup(x => x.Users).ReturnsDbSet(TestBaseConstants.ApplicationUsersDataTest);
    //  _masterContext.SetupGet(x => x.UserRefreshToken).ReturnsDbSet(MockDataHelper.GetUserRefreshTokenMockData());

    //  // Act
    //  var request = new LoginRequestDto()
    //  {
    //    Username = TestBaseConstants.ApplicationUsersDataTest[0].UserName,
    //    Password = "Kempus123#"
    //  };
    //  var userTypes = new List<byte>() { (byte)UserType.User, (byte)UserType.Ambassador };
    //  var result = await _authenticationService.GetAuthenticationResultAsync(request, userTypes);

    //  // Assert
    //  Assert.NotNull(result);
    //  Assert.NotNull(result?.AccessToken);
    //  Assert.NotNull(result?.RefreshToken);
    //}

    //[Fact]
    //public async Task RefreshAuthenticationResultAsync_Should_ReturnResultNotNull()
    //{
    //  // Arrange
    //  _masterContext.SetupGet(x => x.UserRefreshToken).ReturnsDbSet(MockDataHelper.GetUserRefreshTokenMockData());
    //  _masterContext.SetupGet(x => x.Users).ReturnsDbSet(TestBaseConstants.ApplicationUsersDataTest);

    //  _userService.Setup(x => x.GetUserInfoByUsernameAsync(It.IsAny<string>()))
    //    .ReturnsAsync(TestBaseConstants.ApplicationUsersDataTest[0])
    //    .Verifiable();
    //  _userPermissionService.Setup(x => x.GetObjectHasPermissionListModel(It.IsAny<Guid>()))
    //    .ReturnsAsync(new ObjectHasPermissionListModel())
    //    .Verifiable();
    //  _jwtService.Setup(x => x.GenerateToken(It.IsAny<List<Claim>>()))
    //    .Returns(TestBaseConstants.AccessTokenTest)
    //    .Verifiable();
    //  _jwtConfiguration.Object.RefreshTokenValidityInMinutes = TestBaseConstants.RefreshTokenValidityInMinutesTest;

    //  // Act
    //  var result = await _authenticationService.RefreshAuthenticationResultAsync(TestBaseConstants.RefreshTokenTest);

    //  // Assert
    //  Assert.NotNull(result);
    //  Assert.NotNull(result?.AccessToken);
    //  Assert.NotNull(result?.RefreshToken);
    //}

    [Fact]
    public async Task LogoutAsync_Should_DoneWithoutException()
    {
      try
      {
        // Arrange
        _masterContext.SetupGet(x => x.UserRefreshToken).ReturnsDbSet(MockDataHelper.GetUserRefreshTokenMockData());

        // Act
        await _authenticationService.LogoutAsync(TestBaseConstants.ApplicationUsersDataTest[0].UserId,
          TestBaseConstants.RefreshTokenTest);

        // Assert
        Assert.True(true);
      }
      catch
      {
        Assert.False(false);
      }
    }
  }
}
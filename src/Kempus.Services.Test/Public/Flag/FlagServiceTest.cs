﻿using Kempus.Core.Enums;
using Kempus.Entities;
using Kempus.EntityFramework;
using Kempus.Models.Public.Flag;
using Kempus.Services.Common.KempTransaction;
using Kempus.Services.Public.Flag;
using Kempus.Services.Test.DataSeeding;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.Test.Public.Flag
{
  public class FlagServiceTest
  {
    private readonly FlagService _flagService;
    private readonly Mock<ILogger<FlagService>> _logger = new Mock<ILogger<FlagService>>();
    private readonly Mock<MasterDbContext> _masterContext;
    private readonly Mock<ReplicaDbContext> _replicaContext;
    private readonly Mock<IKempTransactionCommonService> _kempTransactionService = new();

    public FlagServiceTest()
    {
      _masterContext = new Mock<MasterDbContext>();
      _replicaContext = new Mock<ReplicaDbContext>();

      _flagService = new FlagService(
        _masterContext.Object, 
        _replicaContext.Object, 
        _logger.Object,
        _kempTransactionService.Object
      );
    }

    //[Fact]
    //public async Task InsertAsync_Should_ReturnTrue()
    //{
    //  // Arrange
    //  _replicaContext.Setup(x => x.Posts).ReturnsDbSet(new List<PostEntity>
    //  {
    //    new PostEntity
    //    {
    //      Id = 1,
    //      Guid = TestBaseConstants.PostId1,
    //      Title = TestBaseConstants.PostTitle,
    //      Content = "Post 1",
    //      Keywords = "['Keyword1','Keyword2']",
    //      Type = (byte)CoreEnums.PostType.Post,
    //      IsPublished = true,
    //      SchoolId = Guid.Parse("bf853bcd-04d3-4b5f-b1cc-848e7994853f"),
    //      TopicId = TestBaseConstants.TopicId,
    //      CreatedBy = TestBaseConstants.UserId,
    //      CreatedDate = DateTime.UtcNow
    //    } 
    //  });
    //  _masterContext.Setup(x => x.Flags).ReturnsDbSet(new List<FlagEntity>()
    //  {
    //    new FlagEntity()
    //    {
    //      ObjectId = TestBaseConstants.PostId1,
    //      ObjectType = (byte)FlagType.Post,
    //      ContentUrl = "kempus.com/post-detail",
    //      Reason = "Trolling",
    //      Description = "Unit test insert report 1",
    //      SchoolId = Guid.Parse("bf853bcd-04d3-4b5f-b1cc-848e7994853f"),
    //      CreatedBy = TestBaseConstants.UserId,
    //      Status = (byte)FlagStatus.Pending,
    //    }
    //  });
      
    //  var databaseFacadeMock = new Mock<DatabaseFacade>(_masterContext.Object);
    //  _masterContext.SetupGet(x => x.Database).Returns(databaseFacadeMock.Object);
    //  _masterContext.Setup(x => x.Database.BeginTransaction())
    //    .Returns(new Mock<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction>().Object);

    //  _masterContext.Setup(x => x.SaveChanges())
    //    .Returns(1)
    //    .Verifiable();

    //  // Act
    //  var newReport = new FlagInputModel()
    //  {
    //    ObjectId = TestBaseConstants.PostId1,
    //    ObjectType = (byte)FlagType.Post,
    //    ContentUrl = "kempus.com/post-detail",
    //    Reason = "Trolling",
    //    Description = "Unit test insert report",
    //    SchoolId = Guid.Parse("bf853bcd-04d3-4b5f-b1cc-848e7994853f"),
    //    CreatedBy = TestBaseConstants.UserId
    //  };
    //  var result = _flagService.Create(newReport);

    //  // Assert
    //  Assert.NotNull(result);
    //}
  }
}

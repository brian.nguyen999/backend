﻿using Kempus.Entities;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.Test
{
  public static class TestBaseConstants
  {
    public static readonly Guid DegreeId = new Guid("4109eade-3cbe-4756-9234-9a693d6b0031");
    public static readonly Guid MajorId = new Guid("9d7dc70b-cf38-41dd-9a49-9519f7a6c6b9");

    public static List<ApplicationUser> ApplicationUsersDataTest = new List<ApplicationUser>()
    {
      new ApplicationUser()
      {
        UserId = Guid.Parse("c954b62b-56b8-41eb-a89a-cc53e0a7aa86"),
        Email = "userservice_test1@kempus.us",
        EmailConfirmed = true,
        UserName = "userservice_test1",
        ReferralCode = "KEMP1",
        Status = (byte)UserStatus.Active,
        CreatedDate = DateTime.UtcNow,
        DegreeId = DegreeId,
        LastChangedDegree = DateTime.UtcNow,
        MajorId = MajorId,
        LastChangedMajor = DateTime.UtcNow,
        ClassYear = 2022,
        LastChangeClassYear = DateTime.UtcNow,
        NickName = "kempus_user1",
        LastChangeNickname = DateTime.UtcNow,
        SchoolId = SchoolId,
      },
      new ApplicationUser()
      {
        UserId = Guid.Parse("8c3feced-df33-439e-8d3b-4bd57c206482"),
        Email = "userservice_test2@kempus.us",
        EmailConfirmed = true,
        UserName = "userservice_test2",
        ReferralCode = "KEMP2",
        Status = (byte)UserStatus.Active,
        CreatedDate = DateTime.UtcNow,
        DegreeId = DegreeId,
        LastChangedDegree = DateTime.UtcNow,
        MajorId = MajorId,
        LastChangedMajor = DateTime.UtcNow,
        ClassYear = 2022,
        LastChangeClassYear = DateTime.UtcNow,
        NickName = "kempus_user2",
        LastChangeNickname = DateTime.UtcNow,
        SchoolId = SchoolId,
      },
      new ApplicationUser()
      {
        UserId = Guid.Parse("2506b4ca-5819-4c5a-94ce-f345eee232e3"),
        Email = "userservice_test3@kempus.us",
        EmailConfirmed = true,
        UserName = "userservice_test3",
        ReferralCode = "KEMP3",
        Status = (byte)UserStatus.Active,
        CreatedDate = DateTime.UtcNow,
        DegreeId = DegreeId,
        LastChangedDegree = DateTime.UtcNow,
        MajorId = MajorId,
        LastChangedMajor = DateTime.UtcNow,
        ClassYear = 2022,
        LastChangeClassYear = DateTime.UtcNow,
        NickName = "kempus_user3",
        LastChangeNickname = DateTime.UtcNow,
        SchoolId = SchoolId,
      },
    };

    public static string AccessTokenTest =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOiJjOTU0YjYyYi01NmI4LTQxZWItYTg5YS1jYzUzZTBhN2FhODYiLCJTY2hvb2xJZCI6IjYyMzEwMmU2LTY1YTctNDlkYi1hNGQwLWNiNzQ3MDkxZGQzNiIsIm5iZiI6MTY2NjAyNjU4OSwiZXhwIjoxNjY2MDI3NDg4LCJpYXQiOjE2NjYwMjY1ODl9.VYE4tkgGubyMWTmSJeXSTudfEorN7uTHAPJj5MAXvns";

    public static string RefreshTokenTest =
      "Yzk1NGI2MmItNTZiOC00MWViLWE4OWEtY2M1M2UwYTdhYTg2X2VjY2FiYWE4LTAyZDYtNGEzOS05NWU2LTMwNGE0MTljYzRlMQ";

    public static string RefreshTokenValidityInMinutesTest = "129600";

    // Mock Ids
    public static readonly Guid UserId = new("ced515f6-0761-488f-9d1a-f5784bb192ac");
    public static readonly Guid RoleId = new("c954b62b-56b8-41eb-a89a-cc53e0a7aa86");
    public static readonly Guid SchoolId = new("3fa85f64-5717-4562-b3fc-2c963f66afa6");
    public static readonly Guid TopicId = new("5f5f42b1-84ef-462a-8eea-c09469295bd1");
    public static readonly Guid PostId1 = new("39dd4dfd-54cc-49c9-9075-140402090126");
    public static readonly Guid PostId2 = new("7187ce6d-0455-4ce6-b643-79b4cbb2ddc3");
    public static readonly Guid PostId3 = new("7ccd90ab-811c-4e56-b4e3-28caf4277e0c");
    public static readonly Guid PostId4 = new("fbc0ad4d-7e97-4821-a013-53a93c9edc5c");
    public static readonly Guid KeywordId1 = new("b5731ff9-2261-4686-b174-ce79aeff54b1");
    public static readonly Guid KeywordId2 = new("b5731ff9-2261-4686-b174-ce79aeff54b1");
    public static readonly Guid PollId = new("e6ef68b0-704d-4453-9b60-e3f7e3831e17");
    public static readonly Guid CourseId = new("dfed759e-5a6a-4e8d-9496-9d480c9d6d3f");
    public static readonly Guid PostCommentId = new("fcc3705f-53bd-4cd1-ad77-9e9ffe2e53b9");
    public static readonly Guid VoteUserId = new("4f4926dd-12df-4096-8cdf-16df2506dcf0");

    public static readonly string PostTitle = "Post 1";
    public static readonly string PollTitle = "Poll 1";
    public static readonly string TopicTitle = "Topic 1";

    // Member data
    public static IEnumerable<object[]> PostSearchData =>
      new List<object[]>
      {
        new object[] { null, null, null, 2 },
        new object[] { PostId1, null, null, 1 },
        new object[] { null, TopicId, null, 1 },
        new object[] { null, null, PostTitle, 1 },
        new object[] { Guid.NewGuid(), Guid.NewGuid(), null, 0 }
      };

    public static IEnumerable<object[]> PollSearchData =>
      new List<object[]>
      {
        new object[] { null, null, null, 2 },
        new object[] { PostId3, null, null, 1 },
        new object[] { null, PollTitle, null, 1 },
        new object[] { Guid.NewGuid(), null, null, 0 }
      };

    public static IEnumerable<object[]> TopicSearchData =>
      new List<object[]>
      {
        new object[] { null, null, 2 },
        new object[] { TopicId, null, 1 },
        new object[] { null, TopicTitle, 1 },
        new object[] { Guid.NewGuid(), null, 0 }
      };

    public static IEnumerable<object[]> RoleSearchData =>
      new List<object[]>
      {
        new object[] { null, null, 2 },
        new object[] { RoleId, null, 1 },
        new object[] { null, "Admin", 1 },
        new object[] { Guid.NewGuid(), null, 0 }
      };

    public static IEnumerable<object[]> SchoolSearchData =>
      new List<object[]>
      {
        new object[] { null, 3 },
        new object[] { true, 1 }
      };
  }
}
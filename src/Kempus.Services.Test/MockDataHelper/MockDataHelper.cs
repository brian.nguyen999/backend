﻿using Kempus.Core.Constants;
using Kempus.Core.Enums;
using Kempus.Entities;
using static Kempus.Core.Constants.CoreConstants;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Services.Test.DataSeeding
{
  public static class MockDataHelper
  {
    public static List<SchoolEntity> GetSchoolMockData()
    {
      return new List<SchoolEntity>
      {
        new SchoolEntity
        {
          Id = 1,
          Guid = Guid.Parse("da359532-a003-4851-85a8-e755e46201eb"),
          Name = "University of Alaska",
          IsActivated = true,
          PrimaryColor = "#787078",
          SecondaryColor = "#787078",
          EmailDomain = "alaska.edu",
          CreatedDate = DateTime.UtcNow,
          IsDeleted = false,
        },
        new SchoolEntity
        {
          Id = 2,
          Guid = Guid.Parse("bf853bcd-04d3-4b5f-b1cc-848e7994853f"),
          Name = "Alaska Christian College",
          IsActivated = false,
          PrimaryColor = "#787078",
          SecondaryColor = "#787078",
          EmailDomain = "askacc.edu",
          CreatedDate = DateTime.UtcNow,
          IsDeleted = false,
        },
        new SchoolEntity
        {
          Id = 3,
          Guid = Guid.Parse("0088164f-71fd-4c90-9071-ee75f9a23174"),
          Name = "Alaska Christian College B",
          IsActivated = false,
          PrimaryColor = "#787078",
          SecondaryColor = "#787078",
          EmailDomain = "askacb.edu",
          CreatedDate = DateTime.UtcNow,
          IsDeleted = false,
        }
      };
    }

    public static List<TopicEntity> GetTopicMockData()
    {
      return new List<TopicEntity>
      {
        new TopicEntity
        {
          Id = 1,
          Name = TestBaseConstants.TopicTitle,
          IsActivated = true,
          SchoolId = TestBaseConstants.SchoolId,
          IsDeleted = false,
          Guid = TestBaseConstants.TopicId
        },
        new TopicEntity
        {
          Id = 2,
          Name = "Topic 2",
          IsActivated = true,
          SchoolId = TestBaseConstants.SchoolId,
          IsDeleted = false,
          Guid = Guid.NewGuid()
        }
      };
    }

    public static List<PostEntity> GetPostMockData()
    {
      return new List<PostEntity>
      {
        new PostEntity
        {
          Id = 1,
          Guid = TestBaseConstants.PostId1,
          Title = TestBaseConstants.PostTitle,
          Content = "Post 1",
          Keywords = "['Keyword1','Keyword2']",
          Type = (byte)CoreEnums.PostType.Post,
          IsPublished = true,
          SchoolId = Guid.Parse("bf853bcd-04d3-4b5f-b1cc-848e7994853f"),
          TopicId = TestBaseConstants.TopicId,
          CreatedBy = TestBaseConstants.UserId,
          CreatedDate = DateTime.UtcNow
        },
        new PostEntity
        {
          Id = 2,
          Guid = TestBaseConstants.PostId2,
          Title = "Post 2",
          Content = "Post 2",
          Keywords = "['Keyword1','Keyword2']",
          Type = (byte)CoreEnums.PostType.Post,
          IsPublished = true,
          SchoolId = Guid.Parse("bf853bcd-04d3-4b5f-b1cc-848e7994853f"),
          CreatedBy = TestBaseConstants.UserId,
          CreatedDate = DateTime.UtcNow
        },
        new PostEntity
        {
          Id = 3,
          Guid = TestBaseConstants.PostId3,
          Title = TestBaseConstants.PollTitle,
          Content = "Poll",
          Keywords = "['Keyword1','Keyword2']",
          Type = (byte)CoreEnums.PostType.Poll,
          IsPublished = true,
          SchoolId = Guid.Parse("ae123bb2-3fe6-4771-b0a3-58d4b8b0fdbc"),
          CreatedBy = TestBaseConstants.UserId,
          CreatedDate = DateTime.UtcNow
        },
        new PostEntity
        {
          Id = 4,
          Guid = TestBaseConstants.PostId4,
          Title = "Post 4",
          Content = "Post 4",
          Keywords = "['Keyword1','Keyword2']",
          Type = (byte)CoreEnums.PostType.Poll,
          IsPublished = true,
          SchoolId = Guid.Parse("ae123bb2-3fe6-4771-b0a3-58d4b8b0fdbc"),
          CreatedBy = TestBaseConstants.UserId,
          CreatedDate = DateTime.UtcNow
        }
      };
    }

    public static List<PollEntity> GetPollMockData()
    {
      return new List<PollEntity>
      {
        new PollEntity
        {
          Id = 1,
          Guid = TestBaseConstants.PollId,
          PostId = TestBaseConstants.PostId3,
          Type = (byte)CoreEnums.PollType.SCQ,
          CreatedDate = DateTime.UtcNow,
          CreatedBy = Guid.Empty,
          IsDeleted = false
        },
        new PollEntity
        {
          Id = 2,
          Guid = new Guid("3af50eb1-5aed-4ac4-809e-2f5120d3978e"),
          PostId = TestBaseConstants.PostId4,
          Type = (byte)CoreEnums.PollType.SCQ,
          CreatedDate = DateTime.UtcNow,
          CreatedBy = Guid.Empty,
          IsDeleted = false
        }
      };
    }

    public static List<PollOptionEntity> GetPollOptionMockData()
    {
      return new List<PollOptionEntity>
      {
        new PollOptionEntity
        {
          Id = 1,
          Guid = new Guid("b28fdeb1-a17b-467b-8d15-397451cb98cd"),
          PollId = TestBaseConstants.PollId,
          Content = "Option 1",
          CreatedDate = DateTime.UtcNow,
          CreatedBy = Guid.Empty,
          IsDeleted = false
        },
        new PollOptionEntity
        {
          Id = 2,
          Guid = new Guid("2e8af3b0-414a-4b0b-8396-e9b65f27a94a"),
          PollId = TestBaseConstants.PollId,
          Content = "Option 2",
          CreatedDate = DateTime.UtcNow,
          CreatedBy = Guid.Empty,
          IsDeleted = false
        }
      };
    }

    public static List<PollEntity> GetEmptyPollMockData()
    {
      return new List<PollEntity>();
    }

    public static List<PollOptionEntity> GetEmptyPollOptionMockData()
    {
      return new List<PollOptionEntity>();
    }

    public static List<UserRefreshTokenEntity> GetUserRefreshTokenMockData()
    {
      return new List<UserRefreshTokenEntity>()
      {
        new UserRefreshTokenEntity()
        {
          Id = 1,
          UserId = Guid.Parse("c954b62b-56b8-41eb-a89a-cc53e0a7aa86"),
          RefreshToken = "c954b62b-56b8-41eb-a89a-cc53e0a7aa86_eccabaa8-02d6-4a39-95e6-304a419cc4e1",
          ExpirationTime = DateTime.UtcNow.AddMinutes(129600)
        }
      };
    }

    public static List<OtpEntity> GetOtpMockData()
    {
      return new List<OtpEntity>()
      {
        new OtpEntity
        {
          Id = 1,
          ObjectId = TestBaseConstants.ApplicationUsersDataTest[1].Email,
          ObjectType = (byte)OtpType.Email,
          OtpCode = "123456",
          CreatedDate = DateTime.UtcNow
        }
      };
    }

    public static List<PermissionEntity> GetPermissionMockData()
    {
      return new List<PermissionEntity>()
      {
        // Account
        new PermissionEntity
        {
          Id = PermissionId.AccountView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.Accounts,
          Order = 0,
          ParentId = ParentModuleApplicationId.Accounts
        },
        new PermissionEntity
        {
          Id = PermissionId.AccountCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.Accounts,
          Order = 1,
          ParentId = ParentModuleApplicationId.Accounts,
        },

        // User
        new PermissionEntity
        {
          Id = PermissionId.UserView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.User,
          Order = 2,
          ParentId = ParentModuleApplicationId.Accounts
        },
        new PermissionEntity
        {
          Id = PermissionId.UserCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.User,
          Order = 3,
          ParentId = ParentModuleApplicationId.Accounts,
        },

        // SendNotification
        new PermissionEntity
        {
          Id = PermissionId.SendNotificationView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.SendNotification,
          Order = 4,
          ParentId = ParentModuleApplicationId.Accounts
        },
        new PermissionEntity
        {
          Id = PermissionId.SendNotificationCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.SendNotification,
          Order = 5,
          ParentId = ParentModuleApplicationId.Accounts,
        },

        // Admin
        new PermissionEntity
        {
          Id = PermissionId.AdminView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.Admin,
          Order = 6,
          ParentId = ParentModuleApplicationId.Accounts
        },
        new PermissionEntity
        {
          Id = PermissionId.AdminCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.Admin,
          Order = 7,
          ParentId = ParentModuleApplicationId.Accounts,
        },

        // Admin Setting
        new PermissionEntity
        {
          Id = PermissionId.AdminSettingsView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.AdminSettings,
          Order = 8,
          ParentId = ParentModuleApplicationId.Accounts
        },
        new PermissionEntity
        {
          Id = PermissionId.AdminSettingsCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.AdminSettings,
          Order = 9,
          ParentId = ParentModuleApplicationId.Accounts,
        }, // Forum
        new PermissionEntity
        {
          Id = PermissionId.ForumView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.Forum,
          Order = 10,
          ParentId = ParentModuleApplicationId.Forum
        },
        new PermissionEntity
        {
          Id = PermissionId.ForumCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.Forum,
          Order = 11,
          ParentId = ParentModuleApplicationId.Forum,
        },

        // Posts
        new PermissionEntity
        {
          Id = PermissionId.PostsView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.Posts,
          Order = 12,
          ParentId = ParentModuleApplicationId.Forum
        },
        new PermissionEntity
        {
          Id = PermissionId.PostsCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.Posts,
          Order = 13,
          ParentId = ParentModuleApplicationId.Forum,
        },

        // Polls
        new PermissionEntity
        {
          Id = PermissionId.PollsView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.Polls,
          Order = 14,
          ParentId = ParentModuleApplicationId.Forum
        },
        new PermissionEntity
        {
          Id = PermissionId.PollsCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.Polls,
          Order = 15,
          ParentId = ParentModuleApplicationId.Forum,
        },

        // Course review
        new PermissionEntity
        {
          Id = PermissionId.CourseReviewsView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.CourseReviews,
          Order = 16,
          ParentId = ParentModuleApplicationId.Forum
        },
        new PermissionEntity
        {
          Id = PermissionId.CourseReviewsCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.CourseReviews,
          Order = 17,
          ParentId = ParentModuleApplicationId.Forum,
        },

        // Transaction
        new PermissionEntity
        {
          Id = PermissionId.TransactionView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.Transaction,
          Order = 18,
          ParentId = ParentModuleApplicationId.Forum
        },
        new PermissionEntity
        {
          Id = PermissionId.TransactionCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.Transaction,
          Order = 19,
          ParentId = ParentModuleApplicationId.Forum,
        },

        // Flags
        new PermissionEntity
        {
          Id = PermissionId.FlagsView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.Flags,
          Order = 20,
          ParentId = ParentModuleApplicationId.Forum
        },
        new PermissionEntity
        {
          Id = PermissionId.FlagsCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.Flags,
          Order = 21,
          ParentId = ParentModuleApplicationId.Forum,
        },

        // School Data
        new PermissionEntity
        {
          Id = PermissionId.SchoolDataView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.SchoolData,
          Order = 22,
          ParentId = ParentModuleApplicationId.Forum
        },
        new PermissionEntity
        {
          Id = PermissionId.SchoolDataCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.SchoolData,
          Order = 23,
          ParentId = ParentModuleApplicationId.Forum,
        },

        // Banner content
        new PermissionEntity
        {
          Id = PermissionId.BannerContentView,
          Name = PermissionName.View,
          ModuleApplicationId = ModuleApplicationId.BannerContent,
          Order = 24,
          ParentId = ParentModuleApplicationId.Forum
        },
        new PermissionEntity
        {
          Id = PermissionId.BannerContentCreateEdit,
          Name = PermissionName.CreateEdit,
          ModuleApplicationId = ModuleApplicationId.BannerContent,
          Order = 25,
          ParentId = ParentModuleApplicationId.Forum,
        }
      };
    }

    // TODO Update Unit test after remove foreign key
    //public static List<RoleEntity> GetRoleMockData()
    //{
    //  return new List<RoleEntity>
    //  {
    //    new RoleEntity
    //    {
    //      NormalizedName = "Admin",
    //      Id = 1,
    //      Guid = TestBaseConstants.RoleId,
    //      Name = "Admin",
    //      IsActive = true,
    //      RolePermissions = new List<RolePermissionEntity>()
    //      {
    //        new RolePermissionEntity
    //        {
    //          Id = 1,
    //          RoleId = 1,
    //          PermissionId = 1,
    //          Permission = new PermissionEntity
    //          {
    //            Id = PermissionId.AccountView,
    //            Name = PermissionName.View,
    //            ModuleApplicationId = ModuleApplicationId.Accounts,
    //            Order = 0,
    //            ParentId = ParentModuleApplicationId.Accounts
    //          },
    //          Role = new RoleEntity()
    //          {
    //            NormalizedName = "Admin",
    //            Id = 1,
    //            Guid = TestBaseConstants.RoleId,
    //            Name = "Admin",
    //            IsActive = true
    //          }
    //        }
    //      }
    //    },
    //    new RoleEntity
    //    {
    //      NormalizedName = "Editor",
    //      Id = 2,
    //      Guid = Guid.Parse("c954b62b-56b8-41eb-a89a-cc53e0a7aa82"),
    //      Name = "Editor",
    //      IsActive = true,
    //      RolePermissions = new List<RolePermissionEntity>()
    //    },
    //  };
    //}

    //public static List<RolePermissionEntity> GetRolePermissionMockData()
    //{
    //  return new List<RolePermissionEntity>()
    //  {
    //    new RolePermissionEntity
    //    {
    //      Id = 1,
    //      RoleId = 1,
    //      PermissionId = 1,
    //      Permission = new PermissionEntity
    //      {
    //        Id = PermissionId.AccountView,
    //        Name = PermissionName.View,
    //        ModuleApplicationId = ModuleApplicationId.Accounts,
    //        Order = 0,
    //        ParentId = ParentModuleApplicationId.Accounts
    //      },
    //      Role = new RoleEntity()
    //      {
    //        NormalizedName = "Admin",
    //        Id = 1,
    //        Guid = TestBaseConstants.RoleId,
    //        Name = "Admin",
    //        IsActive = true
    //      }
    //    }
    //  };
    //}

    public static List<ApplicationUser> GetApplicationUserMockData()
    {
      return new List<ApplicationUser>()
      {
        new ApplicationUser
        {
          Id = 999999,
          UserId = TestBaseConstants.UserId,
          Status = (byte)UserStatus.Active,
          UserName = "Danda_SuperAdmin_1",
          NormalizedUserName = "DANDA_SUPERADMIN_1",
          Email = "danda_superadmin_1@vinova.com.vn",
          NormalizedEmail = "DANDA_SUPERADMIN_1@VINOVA.COM.VN",
          EmailConfirmed = true,
          PasswordHash = "AQAAAAEAACcQAAAAEOWKV6wm3Zqo+5DiVo8dZGNdtcaEqAUcCcYJ5Yjl18OiPh6AGl3N8z+MmOvZ9dTNGQ==",
          SecurityStamp = "PGD6CSQ3SK3HSLUWDJ43MV6IZH5DMAS7",
          ConcurrencyStamp = "a54d530d-e8eb-4c5b-974f-29d5d8eaa4bc",
          UserType = (byte)UserType.Admin
        }
      };
    }

    public static List<CourseEntity> GetCourseMockData()
    {
      return new List<CourseEntity>()
      {
        new CourseEntity
        {
          Id = 1,
          Guid = new Guid(SeedingDefaultData.CourseId),
          CreatedDate = DateTime.Parse("2022-01-01"),
          CreatedBy = new Guid(SeedingDefaultData.AdminId),
          IsDeleted = false,
          Name = "Analytical Chemistry",
          IsActivated = true,
          SchoolId = new Guid(SeedingDefaultData.SchoolId),
        },
      };
    }

    public static List<InstructorEntity> GetInstructorMockData()
    {
      return new List<InstructorEntity>()
      {
        new InstructorEntity
        {
          Id = 1,
          Guid = new Guid(SeedingDefaultData.InstructorId),
          CreatedDate = DateTime.Parse("2022-01-01"),
          CreatedBy = Guid.Parse(SeedingDefaultData.AdminId),
          IsDeleted = false,
          Name = "Alex Tran",
          IsActivated = true,
          SchoolId = Guid.Parse(SeedingDefaultData.SchoolId)
        },
      };
    }

    public static List<CourseInstructorEntity> GetCourseInstructorMockData()
    {
      return new List<CourseInstructorEntity>()
      {
        new CourseInstructorEntity
        {
          Id = 1,
          Guid = new Guid("4e88c8fa-a603-4415-9186-0d8de0f7d0ea"),
          CreatedDate = DateTime.Parse("2022-01-01"),
          CreatedBy = new Guid(SeedingDefaultData.AdminId),
          IsDeleted = false,
          CourseId = new Guid(SeedingDefaultData.CourseId),
          InstructorId = new Guid(SeedingDefaultData.InstructorId),
        },
      };
    }

    public static List<SchoolLifeTimeAnalyticEntity> GetSchoolAnalyticMockData()
    {
      return new List<SchoolLifeTimeAnalyticEntity>()
      {
        new SchoolLifeTimeAnalyticEntity
        {
          Id = 1,
          Guid = new Guid("5666c076-9846-4a43-ac7e-85184e2f6303"),
          CreatedDate = new DateTime(2022, 11, 9, 0, 0, 0),
          CreatedBy = 1,
          IsDeleted = false,
          SchoolId = Guid.Parse("da359532-a003-4851-85a8-e755e46201eb"),
          Users = 10000,
        },
        new SchoolLifeTimeAnalyticEntity
        {
          Id = 2,
          Guid = new Guid("1babdbf6-b398-421b-bab2-f6f724ec6ec1"),
          CreatedDate = new DateTime(2022, 11, 9, 0, 0, 0),
          CreatedBy = 1,
          IsDeleted = false,
          SchoolId = Guid.Parse("bf853bcd-04d3-4b5f-b1cc-848e7994853f"),
          Users = 10000,
        },
      };
    }

    public static List<CourseReviewEntity> GetCourseReviewEmptyMockData()
    {
      return new List<CourseReviewEntity>()
      {
      };
    }

    public static List<CourseReviewEntity> GetCourseReviewMockData()
    {
      return new List<CourseReviewEntity>()
      {
        new CourseReviewEntity
        {
          Id = 1,
          Guid = new Guid("87dfd71c-f14d-4a21-9811-31cc4c399cd7"),
          CourseId = new Guid(SeedingDefaultData.CourseId),
          IsRequiredTextBook = false,
          DifficultRating = 1,
          HoursSpend = 1,
          NumberOfExams = 1,
          IsRequiredGroupProject = true,
          GradingCriteriaRating = 1,
          UserGrade = 1,
          UserAchievementRating = 1,
          RecommendRating = 1,
          CreatedDate = DateTime.Parse("2022-01-01"),
          CreatedBy = new Guid(SeedingDefaultData.AdminId),
          IsDeleted = false
        }
      };
    }

    public static List<PostCommentEntity> GetPostCommentMockData()
    {
      return new List<PostCommentEntity>
      {
        new PostCommentEntity
        {
          Id = 1,
          Guid = TestBaseConstants.PostCommentId,
          PostId = TestBaseConstants.PostId1,
          ParentId = null,
          Content = "Comment 1",
          Type = (byte)PostType.Post,
          CreatedBy = TestBaseConstants.UserId,
          CreatedDate = DateTime.UtcNow
        }
      };
    }

    public static List<FlagEntity> GetFlagMockData()
    {
      return new List<FlagEntity>()
      {
        new FlagEntity()
        {
          ObjectId = TestBaseConstants.PostId1,
          ObjectType = (byte)FlagType.Post,
          ContentUrl = "kempus.com/post-detail",
          Reason = "Trolling",
          Description = "Unit test insert report 1",
          SchoolId = Guid.Parse("bf853bcd-04d3-4b5f-b1cc-848e7994853f"),
          CreatedBy = TestBaseConstants.UserId,
          Status = (byte)FlagStatus.Pending,
        }
      };
    }
  }
}
﻿using Kempus.Entities;

namespace Kempus.Models.Public.LandingPage
{
  public class LandingPageSchoolModel
  {
    public long Id { get; set; }
    public Guid Guid { get; set; }
    public string Name { get; set; }
    public string PrimaryColor { get; set; }
    public string SecondaryColor { get; set; }
    public long TotalUsers { get; set; }
    public bool IsActivated { get; set; }

    public LandingPageSchoolModel()
    {
    }

    public LandingPageSchoolModel(SchoolEntity entity)
    {
      Id = entity.Id;
      Guid = entity.Guid;
      Name = entity.Name;
      PrimaryColor = entity.PrimaryColor;
      SecondaryColor = entity.SecondaryColor;
      IsActivated = entity.IsActivated;
    }
  }
}

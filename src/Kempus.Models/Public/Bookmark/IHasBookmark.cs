﻿namespace Kempus.Models.Public.Bookmark
{
  public interface IHasBookmark
  {
    public Guid PostId { get; set; }
    public bool IsBookmarked { get; set; }
  }
}

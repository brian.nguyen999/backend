﻿using Kempus.Core.Enums;

namespace Kempus.Models.Public.Bookmark
{
  public class BookmarkInputModel
  {
    public Guid UserId { get; set; }

    public Guid ObjectId { get; set; }

    public CoreEnums.BookmarkObjectType ObjectType { get; set; }
  }
}

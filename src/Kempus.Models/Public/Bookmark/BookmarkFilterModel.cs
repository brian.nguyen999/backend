﻿namespace Kempus.Models.Public.Bookmark
{
  public class BookmarkFilterModel
  {
    public Guid? UserId { get; set; }

    public Guid? SchoolId { get; set; }
  }
}

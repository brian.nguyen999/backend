﻿using Kempus.Entities;

namespace Kempus.Models.Public.Bookmark
{
  public class BookmarkListViewModel
  {
    public Guid Guid { get; set; }
    public Guid ObjectId { get; set; }
    public byte ObjectType { get; set; }
    public object ObjectModel { get; set; }
    public DateTime CreatedDate { get; set; }

    public BookmarkListViewModel(BookmarkEntity entity)
    {
      Guid = entity.Guid;
      ObjectId = entity.ObjectId;
      ObjectType = entity.ObjectType;
      CreatedDate = entity.CreatedDate;
    }
  }
}

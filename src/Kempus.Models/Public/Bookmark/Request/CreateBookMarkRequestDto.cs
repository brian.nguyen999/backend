﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.Bookmark.Request
{
  public class CreateBookMarkRequestDto
  {
    public Guid ObjectId { get; set; }

    public byte ObjectType { get; set; }
  }
}

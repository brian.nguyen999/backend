﻿using System.ComponentModel.DataAnnotations;

namespace Kempus.Models.Public.Bookmark.Request
{
  public class DeleteBookmarkRequestDto
  {
    [Required]
    public Guid ObjectId { get; set; }

    [Required]
    public byte ObjectType { get; set; }
  }
}

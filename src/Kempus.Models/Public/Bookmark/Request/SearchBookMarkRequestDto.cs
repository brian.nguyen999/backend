﻿namespace Kempus.Models.Public.Bookmark.Request
{
  public class SearchBookMarkRequestDto
  {
    public Guid? UserId { get; set; }
  }
}

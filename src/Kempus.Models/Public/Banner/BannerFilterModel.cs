﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.Banner
{
  public class BannerFilterModel
  {
    public Guid? SchoolId { get; set; }
    public byte? Status { get; set; }
  }
}

﻿using Kempus.Core.Extensions;
using Kempus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Models.Public.Banner
{
  public class BannerListViewModel
  {
    public Guid Id { get; set; }
    public string? WebImage { get; set; }
    public string? MobileImage { get; set; }
    public string? UrlLink { get; set; }
    public string? Type { get; set; }
    

    public BannerListViewModel(BannerEntity entity)
    {
      Id = entity.Guid;
      WebImage = entity.WebUrl;
      MobileImage = entity.MobileUrl;
      UrlLink = entity.RedirectUrl;
      Type = EnumExtensions.GetEnumDescription((BannerUrlType)entity.UrlType);
    }
  }
}

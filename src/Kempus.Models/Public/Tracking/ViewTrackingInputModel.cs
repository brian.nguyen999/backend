﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.Tracking
{
  public class ViewTrackingInputModel
  {
    public Guid? UserId { get; set; }

    public Guid? ObjectId { get; set; }

    public string? ObjectType { get; set; }

    public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
  }
}

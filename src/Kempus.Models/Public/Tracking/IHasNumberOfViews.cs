﻿namespace Kempus.Models.Public.Tracking
{
  public interface IHasNumberOfViews
  {
    public Guid Id { get; set; }

    public long NumberOfView { get; set; }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Authorization.Request
{
  public interface IHasPermissionList
  {
    public Guid UserId { get; }
    public List<int> PermissionIds { get; set; }
    public List<int> ModuleApplicationIds { get; set; }
  }
}
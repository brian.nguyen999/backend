﻿using Kempus.Models.Authorization.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Authorization.Response
{
  public class ObjectHasPermissionListModel : IHasPermissionList
  {
    public Guid UserId { get; set; }
    public List<int> PermissionIds { get; set; } = new List<int>();
    public List<int> ModuleApplicationIds { get; set; } = new List<int>();
  }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Authorization.Response
{
  public class UserPermissionModuleModel
  {
    public Guid UserId { get; set; }
    public int PermissionId { get; set; }
    public int ModuleApplicationId { get; set; }
  }
}
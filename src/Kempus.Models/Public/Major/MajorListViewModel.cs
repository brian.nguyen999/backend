﻿using Kempus.Entities;

namespace Kempus.Models.Public.Major
{
  public class MajorListViewModel
  {
    public Guid Id { get; set; }

    public string? Name { get; set; }

    public MajorListViewModel(MajorEntity entity)
    {
      this.Id = entity.Guid;
      this.Name = entity.Name;
    }
  }
}

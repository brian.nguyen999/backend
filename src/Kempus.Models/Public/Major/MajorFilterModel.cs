﻿namespace Kempus.Models.Public.Major
{
  public class MajorFilterModel
  {
    public string? Keyword { get; set; }

    public bool? IsActivated { get; set; }
  }
}

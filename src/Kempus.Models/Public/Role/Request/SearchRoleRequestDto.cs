﻿using Kempus.Core.Models;

namespace Kempus.Models.Public.Role.Request
{
  public class SearchRoleRequestDto : Pageable
  {
    public Guid? RoleId { get; set; }
    public string? Keyword { get; set; }
    public bool? IsActive { get; set; }
  }
}

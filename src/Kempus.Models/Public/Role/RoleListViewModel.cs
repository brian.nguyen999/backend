﻿using Kempus.Entities;

namespace Kempus.Models.Public.Role
{
  public class RoleListViewModel
  {
    public long Id { get; set; }
    public Guid Guid { get; set; }
    public string Name { get; set; }
    public DateTime CreatedDate { get; set; }
    public bool IsActive { get; set; }
    public List<int> PermissionIds { get; set; } = new List<int>();

    public RoleListViewModel(RoleEntity entity)
    {
      Id = entity.Id;
      Guid = entity.Guid;
      Name = entity.Name;
      CreatedDate = entity.CreatedDate;
      IsActive = entity.IsActive;
    }
  }
}
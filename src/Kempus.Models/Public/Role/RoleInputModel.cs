﻿using System.ComponentModel.DataAnnotations;

namespace Kempus.Models.Public.Role
{
  public class RoleInputModel
  {
    public long? Id { get; set; }

    [StringLength(200, MinimumLength = 1)] public string Name { get; set; }

    public bool IsActive { get; set; } = true;

    public List<int> PermissionIds { get; set; } = new List<int>();
  }
}
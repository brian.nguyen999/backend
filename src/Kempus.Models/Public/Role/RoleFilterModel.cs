﻿using Kempus.Core.Models;

namespace Kempus.Models.Public.Role
{
  public class RoleFilterModel
  {
    public Guid? RoleId { get; set; }
    public string? Keyword { get; set; }
    public bool? IsActive { get; set; }
  }
}
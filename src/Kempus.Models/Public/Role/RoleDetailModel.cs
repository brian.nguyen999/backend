﻿namespace Kempus.Models.Public.Role
{
  public class RoleDetailModel
  {
    public Guid Id { get; set; }

    public string Name { get; set; }

    public bool IsActive { get; set; }

    public List<int> PermissionIds { get; set; } = new List<int>();
  }
}
﻿using Kempus.Core.Enums;

namespace Kempus.Models.Public.CourseReviewActivity
{
  public class CourseReviewActivityInputModel
  {
    public Guid CourseReviewId { get; set; }

    public Guid UserId { get; set; }

    public Guid SchoolId { get; set; }

    public CoreEnums.CourseReviewActivity ActionId { get; set; }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Authentication.Response
{
  public class AuthenticationResult
  {
    public string AccessToken { get; set; }
    public string RefreshToken { get; set; }
  }
}
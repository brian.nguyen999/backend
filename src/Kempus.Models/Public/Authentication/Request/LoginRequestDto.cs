﻿using Kempus.Core.Errors;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.Authentication.Request
{
  public class LoginRequestDto
  {
    [Required] public string Username { get; set; }
    [Required] public string Password { get; set; }
  }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.Authentication.Request
{
  public class RefreshAuthenticationRequestDto
  {
    [MinLength(1)] public string RefreshToken { get; set; }
  }
}
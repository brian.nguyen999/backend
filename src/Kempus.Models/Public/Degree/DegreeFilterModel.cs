﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.Degree
{
  public class DegreeFilterModel
  {
    public string? Keyword { get; set; }

    public bool? IsActivated { get; set; }
  }
}

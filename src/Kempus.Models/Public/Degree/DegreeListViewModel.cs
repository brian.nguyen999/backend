﻿using Kempus.Entities;

namespace Kempus.Models.Public.Degree
{
  public class DegreeListViewModel
  {
    public Guid Id { get; set; }

    public string? Name { get; set; }

    public DegreeListViewModel(DegreeEntity entity)
    {
      this.Id = entity.Guid;
      this.Name = entity.Name;
    }
  }
}

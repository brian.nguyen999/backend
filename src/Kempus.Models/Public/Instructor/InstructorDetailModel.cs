﻿namespace Kempus.Models.Public.Instructor
{
  public class InstructorDetailModel
  {
    public Guid Id { get; set; }

    public string Name { get; set; }
  }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.Instructor
{
  public interface IHasInstructorList
  {
    public Guid Id { get; set; }

    public List<InstructorDetailModel> Instructors { get; set; }
  }
}

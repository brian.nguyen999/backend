﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.PollOption
{
  public class PollOptionInputModel
  {
    public Guid PollId { get; set; }

    public List<string> Options { get; set; } = new List<string>();
  }
}
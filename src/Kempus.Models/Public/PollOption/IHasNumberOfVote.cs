﻿namespace Kempus.Models.Public.PollOption
{
  public interface IHasNumberOfVote
  {
    public Guid Id { get; set; }

    public int NumberOfVote { get; set; }

    public int PercentageOfVote { get; set; }
  }
}

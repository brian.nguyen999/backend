﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Kempus.Models.Public.PollOption
{
  public class PollOptionDetailModel
  {
    public Guid? Id { get; set; }

    [JsonIgnore]
    public Guid PollId { get; set; }

    public string Content { get; set; }

    public bool IsChecked { get; set; }

    public long NumberOfVote { get; set; }

    public double PercentageOfVote { get; set; }
  }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.PollOption
{
  public class PollOptionUpdateInputModel
  {
    public Guid PollId { get; set; }

    public List<PollOptionDetailModel> Options { get; set; } = new List<PollOptionDetailModel>();
  }
}
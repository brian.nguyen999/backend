﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.Flag
{
  public class FlagInputRequestDto
  {
    [Required]
    public Guid ObjectId { get; set; }
    [Required]
    public byte ObjectType { get; set; }
    public string ContentUrl { get; set; }
    [Required]
    public string Reason { get; set; }
    [Required]
    public string Description { get; set; }
  }
}

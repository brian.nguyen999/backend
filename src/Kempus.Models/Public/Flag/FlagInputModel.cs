﻿namespace Kempus.Models.Public.Flag
{
  public class FlagInputModel
  {
    public Guid ObjectId { get; set; }
    public byte ObjectType { get; set; }
    public string? ContentUrl { get; set; }
    public string Reason { get; set; }
    public string Description { get; set; }
    public Guid SchoolId { get; set; }
    public Guid CreatedBy { get; set; }
  }
}

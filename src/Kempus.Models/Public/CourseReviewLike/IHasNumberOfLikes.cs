﻿namespace Kempus.Models.Public.CourseReviewLike
{
  public interface IHasNumberOfLikes
  {
    public Guid Id { get; set; }

    public long NumberOfLike { get; set; }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.CourseReviewLike
{
  public class CourseReviewLikeInputModel
  {
    public Guid CourseReviewId { get; set; }

    public Guid CreatorId { get; set; }

    public Guid SchoolId { get; set; }
  }
}

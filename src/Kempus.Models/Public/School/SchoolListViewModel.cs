﻿using Kempus.Core.Constants;
using Kempus.Entities;

namespace Kempus.Models.Public.School
{
  public class SchoolListViewModel
  {
    public long Id { get; set; }
    public Guid Guid { get; set; }
    public string SchoolName { get; set; }
    public string PrimaryColor { get; set; }
    public string SecondaryColor { get; set; }
    public List<string> SchoolEmail { get; set; }
    public int NumberOfUser { get; set; }
    public long NumberOfSignUp { get; set; }
    public long TotalKemBalance { get; set; }
    public bool Status { get; set; }
    public DateTime? ActivatedDate { get; set; }
    public bool IsShowHomePage { get; set; }

    public SchoolListViewModel(SchoolEntity entity)
    {
      Id = entity.Id;
      Guid = entity.Guid;
      SchoolName = entity.Name;
      PrimaryColor = entity.PrimaryColor;
      SecondaryColor = entity.SecondaryColor;
      SchoolEmail = entity.EmailDomain.Split(SchoolEmailFormat.Delimeter).ToList();
      Status = entity.IsActivated;
      ActivatedDate = entity.LastUpdateStatusDateTime;
      IsShowHomePage = entity.IsShowHomePage;
    }
  }
}

﻿namespace Kempus.Models.Public.School
{
  public class SchoolCountModel
  {
    public int TotalOpenedSchools { get; set; }
    public int TotalWaitedSchools { get; set; }
  }
}

﻿using Kempus.Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.School
{
  public class ImportCourseConvertedModel
  {
    public string CourseFullName { get; set; }
    public List<string> Instructor { get; set; }
    public string DepartmentProgram { get; set; }
    public long CsvRowIndex { get; set; }

    public ImportCourseConvertedModel(ImportCourseModel importValue, long index)
    {
      CourseFullName = importValue.CourseFullName.Trim();
      DepartmentProgram = importValue.DepartmentProgram.Trim();
      Instructor = StringHelper.GetInstructorEntityFromFileUpload(importValue.Instructor);
      CsvRowIndex = index;
    }
  }
}

﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.School
{
  public class SchoolUploadInputModel
  {
    public IFormFile File { get; set; }
    public Guid SchoolId { get; set; }
    public Guid CreatorId { get; set; }
  }
}

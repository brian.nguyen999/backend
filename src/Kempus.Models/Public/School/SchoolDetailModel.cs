﻿namespace Kempus.Models.Public.School
{
  public class SchoolDetailModel
  {
    public Guid Id { get; set; }
    public string SchoolName { get; set; }
    public string PrimaryColor { get; set; }
    public string SecondaryColor { get; set; }
    public bool IsActivated { get; set; }
    public bool IsRequiredReferralCode { get; set; }
    public List<string> SchoolEmailFormat { get; set; }
    public int NumberOfStudent { get; set; }
    public bool IsShowHomePage { get; set; }
  }
}

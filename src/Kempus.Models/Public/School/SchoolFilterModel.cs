﻿using System;

namespace Kempus.Models.Public.School
{
  public class SchoolFilterModel
  {
    public Guid? Id { get; set; }
    public bool? Status { get; set; }
    public bool? IsHaveUser { get; set; }
    public string? Keyword { get; set; }
    public bool? IsShowHomePage { get; set; }
    public bool? IsActivated { get; set; }

    public string GetCacheKey()
    {
      return $"{Id}-{Status}-{IsHaveUser}-{IsShowHomePage}-{IsActivated}";
    }
  }
}

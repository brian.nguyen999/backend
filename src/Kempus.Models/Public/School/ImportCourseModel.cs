﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.School
{
  public class ImportCourseModel
  {
    public string CourseFullName { get; set; }
    public string Instructor { get; set; }
    public string DepartmentProgram { get; set; }
  }

  public class ImportCourseModelMap : ClassMap<ImportCourseModel>
  {
    public ImportCourseModelMap()
    {
      Map(m => m.CourseFullName).Validate(x => x.Field == nameof(ImportCourseModel.CourseFullName));
      Map(m => m.Instructor).Validate(x => x.Field == nameof(ImportCourseModel.Instructor));
      Map(m => m.DepartmentProgram).Validate(x => x.Field == nameof(ImportCourseModel.DepartmentProgram));
    }
  }
}

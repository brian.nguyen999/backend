﻿using System.ComponentModel.DataAnnotations;

namespace Kempus.Models.Public.School
{
  public class SchoolInputModel
  {
    public Guid? Id { get; set; }
    public string PrimaryColor { get; set; }
    public string SecondaryColor { get; set; }
    [Required]
    public string SchoolName { get; set; }
    [Required]
    public List<string> SchoolEmailFormat { get; set; } = new List<string>();
    public bool Status { get; set; }
    public bool IsRequiredReferralCode { get; set; }
    public bool IsShowHomePage { get; set; }

  }
}
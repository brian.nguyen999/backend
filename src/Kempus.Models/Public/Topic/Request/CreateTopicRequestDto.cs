﻿namespace Kempus.Models.Public.Topic.Request
{
  public class CreateTopicRequestDto
  {
    public Guid SchoolId { get; set; }
    public string Name { get; set; }
    public bool IsActivated { get; set; }
  }
}

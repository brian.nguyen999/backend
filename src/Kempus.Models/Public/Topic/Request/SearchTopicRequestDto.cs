﻿using Kempus.Core.Models;

namespace Kempus.Models.Public.Topic.Request
{
  public class SearchTopicRequestDto : Pageable
  {
    public Guid? TopicId { get; set; }
    public string? Keyword { get; set; }
  }
}

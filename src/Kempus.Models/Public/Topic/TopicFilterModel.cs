﻿using Kempus.Core.Models;

namespace Kempus.Models.Public.Topic
{
  public class TopicFilterModel
  {
    public Guid? SchoolId { get; set; }
    public Guid? TopicId { get; set; }
    public string? Keyword { get; set; }
    public bool? Status { get; set; }
  }
}
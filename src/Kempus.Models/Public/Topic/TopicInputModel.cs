﻿namespace Kempus.Models.Public.Topic
{
  public class TopicInputModel
  {
    public Guid SchoolId { get; set; }
    public string Name { get; set; }
    public bool IsActivated { get; set; }
    public Guid CreatorId { get; set; }
  }
}

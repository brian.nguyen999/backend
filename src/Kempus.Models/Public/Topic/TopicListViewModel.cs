﻿using Kempus.Entities;

namespace Kempus.Models.Public.Topic
{
  public class TopicListViewModel
  {
    public Guid Id { get; set; }
    public string Name { get; set; }
    public bool IsActivated { get; set; }
    public DateTime CreatedDate { get; set; }
    public int NumberOfPost { get; set; }

    public TopicListViewModel(TopicEntity entity)
    {
      Id = entity.Guid;
      Name = entity.Name;
      IsActivated = entity.IsActivated;
      CreatedDate = entity.CreatedDate;
    }
  }
}
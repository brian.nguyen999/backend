﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.PostPollActivitySummary
{
  public class PostPollActivitySummaryFilterModel
  {
    public Guid? SchoolId { get; set; }
    public bool? IsPublished { get; set; }
    public List<Guid>? ExcludedIds { get; set; }
  }
}
  
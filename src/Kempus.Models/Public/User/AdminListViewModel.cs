﻿using Kempus.Core.Extensions;
using Kempus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Models.Public.User
{
  public class AdminListViewModel
  {
    public long Id { get; set; }
    public Guid Guid { get; set; }
    public string Username { get; set; }
    public string Email { get; set; }
    public string Role { get; set; }
    public string Status { get; set; }
    public DateTime CreatedOn { get; set; }

    public AdminListViewModel(ApplicationUser entity)
    {
      Id = entity.Id;
      Guid = entity.UserId;
      Username = entity.FirstName;
      Email = entity.Email;
      Status = EnumExtensions.GetEnumDescription((UserStatus)entity.Status);
      CreatedOn = entity.CreatedDate;
    }
  }
}

﻿using Kempus.Models.Public.User.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.User
{
  public class UserInputModel
  {
    public string Email { get; set; }
    public Guid? DegreeId { get; set; }
    public Guid? MajorId { get; set; }
    public int? ClassYear { get; set; }
    public string NickName { get; set; }
    public string UserName { get; set; }
    public string Password { get; set; }
    public string ConfirmPassword { get; set; }
    public string OtpCode { get; set; }
    public string ReferralCode { get; set; }
    public byte UserType { get; set; }
    public Guid SchoolId { get; set; }
    public bool IsRegister { get; set; }

    public OtpRequestDto OtpDto { get; set; }
  }
}

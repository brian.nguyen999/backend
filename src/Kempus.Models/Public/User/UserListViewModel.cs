﻿using Kempus.Core.Extensions;
using Kempus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Models.Public.User
{
  public class UserListViewModel
  {
    public long Id { get; set; }
    public Guid Guid { get; set; }
    public string Nickname { get; set; }
    public string Username { get; set; }
    public string Group { get; set; }
    public string Status { get; set; }
    public string Violation { get; set; }
    public long Kemp { get; set; }
    public DateTime CreatedOn { get; set; }

    public UserListViewModel(ApplicationUser entity)
    {
      Id = entity.Id;
      Guid = entity.UserId;
      Username = entity.UserName;
      Nickname = entity.NickName;
      Group = EnumExtensions.GetEnumDescription((UserType)entity.UserType);
      Status = EnumExtensions.GetEnumDescription((UserStatus)entity.Status);
      CreatedOn = entity.CreatedDate;
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.User
{
  public class CmsUserInputModel
  {
    public Guid? Id { get; set; }
    public string? Memory { get; set; }
    public Guid? UpdatedBy { get; set; }
    public Guid? CreatedBy { get; set; }
    public byte? Status { get; set; }
    public string? Username { get; set; }
    public string? Email { get; set; }
    public string? Password { get; set; }
    public Guid? SchoolId { get; set; }
    public int? ClassYear { get; set; }
    public Guid? DegreeId { get; set; }
    public Guid? MajorId { get; set; }
    public byte? UserType { get; set; }
  }
}

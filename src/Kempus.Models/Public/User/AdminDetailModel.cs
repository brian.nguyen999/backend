﻿using Kempus.Core.Extensions;
using Kempus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Models.Public.User
{
  public class AdminDetailModel
  {
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Email { get; set; }
    public Guid? RoleId { get; set; }
    public string RoleName { get; set; }
    public byte Status { get; set; }
    public string StatusName { get; set; }
    public List<int> Permissions { get; set; }
    public AdminDetailModel(ApplicationUser entity)
    {
      Id = entity.UserId;
      Name = entity.FirstName;
      Email = entity.Email;
      Status = entity.Status;
      StatusName = EnumExtensions.GetEnumDescription((UserStatus)entity.Status);
    }
  }
}

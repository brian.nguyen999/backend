﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.User
{
  public class AdminInputModel
  {
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Email { get; set; }
    public Guid RoleId { get; set; }
    public byte Status { get; set; }
    public Guid CreatedBy { get; set; }
    public Guid UpdatedBy { get; set; }
  }
}

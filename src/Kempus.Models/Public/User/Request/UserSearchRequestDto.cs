﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.User.Request
{
  public class UserSearchRequestDto
  {
    public Guid? Id { get; set; }
    public string? Keyword { get; set; }
    public byte? Status { get; set; }
    public Guid? SchoolId { get; set; }
    public byte? Group { get; set; }
  }
}

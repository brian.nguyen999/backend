﻿using Kempus.Core.Errors;
using System.ComponentModel.DataAnnotations;
using static Kempus.Core.Constants.CoreConstants;

namespace Kempus.Models.Public.User.Request
{
  public class ResetPasswordRequestDto
  {
    [StringLength(16, ErrorMessage = ErrorMessages.Register_Password_Length, MinimumLength = 6)]
    [RegularExpression(RegexConstant.PasswordRegularExpression,
      ErrorMessage = ErrorMessages.Register_Password_RegularExpression)]
    public string Password { get; set; }

    [StringLength(16, ErrorMessage = ErrorMessages.Register_Password_Length, MinimumLength = 6)]
    [RegularExpression(RegexConstant.PasswordRegularExpression,
      ErrorMessage = ErrorMessages.Register_Password_RegularExpression)]
    [Compare(nameof(Password))]
    public string ConfirmPassword { get; set; }

    public string Email { get; set; }

    public string OtpCode { get; set; }

    public List<byte> UserTypes { get; set; } = new List<byte> ();
  }
}
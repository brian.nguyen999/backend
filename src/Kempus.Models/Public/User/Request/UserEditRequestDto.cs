﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.User.Request
{
  public class UserEditRequestDto
  {
    [Required]
    public Guid? Id { get; set; }
    public string? Memory { get; set; }
    public byte? Status { get; set; }
    public byte? Group { get; set; }
  }
}

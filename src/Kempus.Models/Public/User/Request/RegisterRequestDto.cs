﻿using Kempus.Core.Errors;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Kempus.Core.Constants.CoreConstants;

namespace Kempus.Models.Public.User.Request
{
  public class RegisterRequestDto
  {
    [Required] public string Email { get; set; }
    public Guid? DegreeId { get; set; }
    public Guid? MajorId { get; set; }
    public int? ClassYear { get; set; }
    [Required] public string NickName { get; set; }

    [Required]
    [StringLength(16, ErrorMessage = ErrorMessages.Register_Username_Length, MinimumLength = 4)]
    public string UserName { get; set; }

    [Required]
    [StringLength(16, ErrorMessage = ErrorMessages.Register_Password_Length, MinimumLength = 6)]
    [RegularExpression(RegexConstant.PasswordRegularExpression,
      ErrorMessage = ErrorMessages.Register_Password_RegularExpression)]
    [DataType(DataType.Password)]
    public string Password { get; set; }

    [Required]
    [DataType(DataType.Password)]
    [Compare(nameof(Password))]
    public string ConfirmPassword { get; set; }

    [Required] public string OtpCode { get; set; }
    public string ReferralCode { get; set; }
  }
}
﻿using System.ComponentModel.DataAnnotations;

namespace Kempus.Models.Public.User.Request
{
  public class ForgotPasswordRequestDto
  {
    [Required] [EmailAddress] public string Email { get; set; }
    public List<byte> UserTypes { get; set; } = new List<byte>();
  }
}
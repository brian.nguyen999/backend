﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.User.Request
{
  public class AdminSearchRequestDto
  {
    public Guid? Id { get; set; }
    public string? Keyword { get; set; }
    public byte? Status { get; set; }
    public List<Guid> RoleIds { get; set; } = new List<Guid>();
  }
}

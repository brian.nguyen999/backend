﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.User.Request
{
  public class UpdateProfileRequestDto
  {
    public Guid? MajorId { get; set; }

    public bool? IsHiddenMajor { get; set; }

    public Guid? DegreeId { get; set; }

    public bool? IsHiddenDegree { get; set; }

    public int? ClassYear { get; set; }

    public bool? IsHiddenClassYear { get; set; }
  }
}

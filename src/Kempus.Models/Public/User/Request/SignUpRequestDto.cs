﻿using System.ComponentModel.DataAnnotations;

namespace Kempus.Models.Public.User.Request
{
  public class SignupRequestDto
  {
    public string ReferralCode { get; set; }

    [Required]
    [EmailAddress]
    public string Email { get; set; }
  }
}
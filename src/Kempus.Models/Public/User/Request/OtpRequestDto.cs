﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.User.Request
{
  public class OtpRequestDto
  {
    public string OtpCode { get; set; }
    public string ObjectId { get; set; }
    public byte ObjectType { get; set; }
    public bool IsSendNotification { get; set; } = true;
  }
}
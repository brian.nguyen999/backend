﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.User.Request
{
  public class UserCreateRequestDto
  {
    [Required]
    public string? Username { get; set; }
    [Required]
    public string? Email { get; set; }
    [Required]
    public string? Password { get; set; }
    [Required]
    public Guid? SchoolId { get; set; }
    [Required]
    public int? ClassYear { get; set; }
    [Required]
    public Guid? DegreeId { get; set; }
    [Required]
    public Guid? MajorId { get; set; }
    [Required]
    public byte? Group { get; set; }
    [Required]
    public byte? Status { get; set; }
  }
}

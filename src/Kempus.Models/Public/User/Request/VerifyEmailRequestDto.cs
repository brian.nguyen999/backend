﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.User.Request
{
  public class VerifyEmailRequestDto
  {
    [Required] public string OtpCode { get; set; }
    [Required] public string Email { get; set; }
  }
}
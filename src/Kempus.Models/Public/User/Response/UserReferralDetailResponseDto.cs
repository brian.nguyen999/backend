﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.User.Response
{
  public class UserReferralDetailResponseDto
  {
    public string InviteLeft { get; set; }
    public long SuccessfulSignup { get; set; }
    public long WroteFirstReview { get; set; }
    public string UserType { get; set; }
  }
}
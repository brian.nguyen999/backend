﻿namespace Kempus.Models.Public.User.Response
{
  public class UserSignUpResponseDto
  {
    public bool IsInWaitList { get; set; }

    public string SchoolName { get; set; }

    public long TotalWaitList { get; set; }

    public string ReferralCode { get; set; }

    public bool IsVerified { get; set; }
  }
}

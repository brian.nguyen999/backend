﻿using Kempus.Core.Extensions;
using Kempus.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Models.Public.User.Response
{
  public class UserDetailModel
  {
    public UserDetail UserDetails { get; set; }

    public Activity Activities { get; set; }

    public UserDetailModel(ApplicationUser entity)
    {
      UserDetails = new UserDetail()
      {
        Id = entity.UserId,
        Nickname = entity.NickName,
        Username = entity.UserName,
        Status = entity.Status,
        Group = entity.UserType,
        ClassYear = entity.ClassYear.GetValueOrDefault(),
        Memo = entity.AdminMemory,
        DegreeId = entity.DegreeId,
        MajorId = entity.MajorId,
        SchoolId = entity.SchoolId,
      };

      Activities = new Activity()
      {
        LastLogin = entity.LastAccessTime,
        LastLoginIp = entity.LastLoginIP,
        //SuccessfulReferral = entity.NumberFriendsInvited,
      };
    }

  }

  public class UserDetail
{
    public Guid Id { get; set; }
    public string Nickname { get; set; }
    public string Username { get; set; }
    public string? School { get; set; }
    public int ClassYear { get; set; }
    public string? Degree { get; set; }
    public string? Major { get; set; }
    public byte Status { get; set; }
    public byte Group { get; set; }
    public string? Violation { get; set; }
    public DateTime? ViolationUntil { get; set; }
    public string? Memo { get; set; }
    [JsonIgnore]
    public Guid? DegreeId { get; set; }
    [JsonIgnore]
    public Guid? MajorId { get; set; }
    [JsonIgnore]
    public Guid? SchoolId { get; set; }
  }

  public class Activity
  {
    public DateTime? LastLogin { get; set; }
    public string? LastLoginIp { get; set; }
    public long Post { get; set; }
    public long Poll { get; set; }
    public long Review { get; set; }
    public long Comment { get; set; }
    public long Like { get; set; }
    public long Vote { get; set; }
    public long TotalFlag { get; set; }
    public long SuccessfulFlag { get; set; }
    public long FlaggedPost { get; set; }
    public long FlaggedDeletedPost { get; set; }
    public int SuccessfulReferral { get; set; }
    public string? InvitesLeft { get; set; }
    public long Kemp { get; set; }
  }
}

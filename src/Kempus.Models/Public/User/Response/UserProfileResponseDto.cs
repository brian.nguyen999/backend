﻿namespace Kempus.Models.Public.User.Response
{
  public class UserProfileResponseDto
  {
    public Guid Id { get; set; }
    public long Kemp { get; set; }
    public string? SchoolName { get; set; }

    public InviteFriend? InviteFriendInfo { get; set; }

    public string? NickName { get; set; }
    public DateTime? LastChangeNickname { get; set; }
    public string? UserName { get; set; }
    public string? ReferralCode { get; set; }

    public OthersInfo DegreeInfo { get; set; } = new OthersInfo();
    public OthersInfo MajorInfo { get; set; } = new OthersInfo();
    public OthersInfo ClassYear { get; set; } = new OthersInfo();
    public string UserType { get; set; }

    public string Email { get; set; }
  }

  public class InviteFriend
  {
    public string InviteLeft { get; set; }
    public long SuccessfulSignup { get; set; }
    public long WroteFirstReview { get; set; }
  }

  public class OthersInfo
  {
    public Guid? Guid { get; set; }
    public string Status { get; set; }
    public string? Name { get; set; }
    public DateTime? LastChangeDateTime { get; set; }
  }
}
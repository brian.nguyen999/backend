﻿using Kempus.Core.Extensions;
using Kempus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.Course
{
  public class CmsCourseListViewModel
  {
    public long Id { get; set; }
    public Guid Guid { get; set; }
    public string? CourseName { get; set; }
    public List<string> InstructorNames { get; set; }
    public string? DeptProgram { get; set; }
    public string? Degree { get; set; }
    public long NumberOfReviews { get; set; }
    public string? Status { get; set; }
    public DateTime? ModifiedDate { get; set; }

    public CmsCourseListViewModel(CourseEntity entity)
    {
      Id = entity.Id;
      Guid = entity.Guid;
      CourseName = entity.Name;
      Status = entity.IsActivated ? "Active" : "Deactivated";
      ModifiedDate = entity.UpdatedDate ?? entity.CreatedDate;
    }
  }
}

﻿namespace Kempus.Models.Public.Course
{
  public class CourseFilterModel
  {
    public string? Keyword { get; set; }

    public List<Guid>? CourseIds { get; set; }

    public Guid? SchoolId { get; set; }
  }
}

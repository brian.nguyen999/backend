﻿using Kempus.Models.Public.DepartmentProgram;
using Kempus.Models.Public.Instructor;

namespace Kempus.Models.Public.Course
{
  public class CourseDetailModel : IHasInstructorList, IHasDepartment
  {
    public Guid Id { get; set; }

    public string? Name { get; set; }

    public List<InstructorDetailModel> Instructors { get; set; }

    public Guid? DepartmentId { get; set; }

    public string DepartmentName { get; set; }
  }
}
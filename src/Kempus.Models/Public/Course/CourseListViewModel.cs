﻿using Kempus.Entities;
using Kempus.Models.Public.DepartmentProgram;

namespace Kempus.Models.Public.Course
{
  public class CourseListViewModel : IHasDepartment
  {
    public Guid Id { get; set; }
    public string? CourseName { get; set; }
    public List<string> InstructorNames { get; set; }
    public int TotalReviews { get; set; }
    public bool IsRequiredTextbook { get; set; }
    public byte NumberOfExams { get; set; }
    public bool IsRequiredGroupProjects { get; set; }
    public double RecommendedRate { get; set; }
    public double GradingCriteriaRate { get; set; }
    public double CourseDifficultyRate { get; set; }
    public Guid? DepartmentId { get; set; }
    public string DepartmentName { get; set; }

    public CourseListViewModel(CourseEntity courseEntity)
    {
      Id = courseEntity.Guid;
      CourseName = courseEntity.Name;
      DepartmentId = courseEntity.DepartmentId;
    }
  }
}

﻿namespace Kempus.Models.Public.Permission
{
  public class PermissionDetailModel
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public int ModuleApplicationId { get; set; }
    public int Order { get; set; }
    public int? ParentId { get; set; }
  }
}
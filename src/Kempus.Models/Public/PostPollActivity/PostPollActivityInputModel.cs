﻿using Kempus.Core.Enums;

namespace Kempus.Models.Public.PostPollActivity
{
  public class PostPollActivityInputModel
  {
    public Guid ObjectId { get; set; }

    public CoreEnums.PostType ObjectType { get; set; }

    public CoreEnums.PostPollActivity ActionId { get; set; }

    public Guid UserId { get; set; }

    public Guid SchoolId { get; set; }

    public DateTime CreatedDate { get; set; }
  }
}

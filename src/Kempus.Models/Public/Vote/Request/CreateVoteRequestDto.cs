﻿namespace Kempus.Models.Public.Vote.Request
{
  public class CreateVoteRequestDto
  {
    public Guid PollId { get; set; }
    public List<Guid> PollOptionIds { get; set; }
  }
}

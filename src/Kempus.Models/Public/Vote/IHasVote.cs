﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.Vote
{
  public interface IHasVote
  {
    public Guid Id { get; set; }

    public Guid PollId { get; set; }

    public bool IsVoted { get; set; }
  }
}

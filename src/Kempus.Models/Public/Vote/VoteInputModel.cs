﻿namespace Kempus.Models.Public.Vote
{
  public class VoteInputModel
  {
    public Guid PollId { get; set; }
    public Guid CreatorId { get; set; }
    public List<Guid> PollOptionIds { get; set; }
  }
}

﻿using Kempus.Core.Extensions;
using Kempus.Entities;
using Newtonsoft.Json;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Models.Public.KempTransaction
{
  public class KemTransactionListViewModel
  {
    public Guid Id { get; set; }

    public string Title { get; set; }

    [JsonIgnore]
    public KempTransactionType TransactionType { get; set; }

    public string Description { get; set; }

    public int Amount { get; set; }

    public DateTime CreatedDate { get; set; }

    [JsonIgnore]
    public bool IsAdminCreated { get; set; }

    public KemTransactionListViewModel(KempTransactionEntity entity)
    {
      this.Id = entity.Guid;
      this.TransactionType = (KempTransactionType)(entity.Type);
      this.Amount = entity.Amount;
      this.CreatedDate = entity.CreatedDate;
      this.IsAdminCreated = entity.IsAdminCreated;
    }
  }
}

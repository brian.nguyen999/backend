﻿using Kempus.Core.Errors;
using System.ComponentModel.DataAnnotations;

namespace Kempus.Models.Public.Conversation
{
  public class ConversationMessageInputModel
  {
    [StringLength(1000, ErrorMessage = ErrorMessages.Invalid_Request, MinimumLength = 1)]
    public string? Message { get; set; }

    public Guid ConversationId { get; set; }

    public Guid FromUserId { get; set; }

    public Guid SchoolId { get; set; }
  }
}

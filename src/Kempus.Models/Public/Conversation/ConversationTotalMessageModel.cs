﻿namespace Kempus.Models.Public.Conversation
{
  public class ConversationTotalMessageModel
  {
    public int TotalMessages { get; set; }

    public int NumberOfPendingMessage { get; set; }
  }
}

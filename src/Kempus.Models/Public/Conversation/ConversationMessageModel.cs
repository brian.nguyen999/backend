﻿using Kempus.Entities;
using Newtonsoft.Json;

namespace Kempus.Models.Public.Conversation
{
  public class ConversationMessageModel
  {
    [JsonProperty("id")]
    public Guid Id { get; set; }

    [JsonProperty("message")]
    public string? Message { get; set; }

    [JsonProperty("userId")]
    public Guid UserId { get; set; }

    [JsonProperty("createdDate")]
    public DateTime CreatedDate { get; set; }

    public ConversationMessageModel(MessageEntity entity)
    {
      Id = entity.Guid;
      Message = entity.Content;
      UserId = entity.CreatedUser;
      CreatedDate = entity.CreatedDate;
    }
  }
}

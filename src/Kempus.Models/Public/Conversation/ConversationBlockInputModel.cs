﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.Conversation
{
  public class ConversationBlockInputModel
  {
    public Guid ConversationId { get; set; }
    public bool IsDeleteChat { get; set; } = false;
    public Guid BlockerUserId { get; set; }
    public Guid SchoolId { get; set; }
  }
}

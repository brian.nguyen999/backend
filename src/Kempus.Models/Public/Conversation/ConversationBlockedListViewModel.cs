﻿using Kempus.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.Conversation
{
  public class ConversationBlockedListViewModel
  {
    public string? NickName { get; set; }
    public Guid? ConversationId { get; set; }
    [JsonIgnore]
    public Guid BlockedUserId { get; set; }

    public ConversationBlockedListViewModel(BlockEntity entity) 
    {
      ConversationId = entity.ConversationId;
      BlockedUserId = entity.BlockedUser;
    }
  }
}

﻿namespace Kempus.Models.Public.Conversation
{
  public class ConversationInputModel
  {
    public Guid FromUserId { get; set; }

    public Guid ToUserId { get; set; }

    public Guid SchoolId { get; set; }
  }
}

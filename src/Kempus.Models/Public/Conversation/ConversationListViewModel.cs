﻿using Kempus.Entities;

namespace Kempus.Models.Public.Conversation
{
  public class ConversationListViewModel
  {
    public Guid Id { get; set; }

    public string? Name { get; set; }

    public ConversationMessageModel? LatestMessage { get; set; }

    public bool IsHasNewMessage { get; set; }

    public byte Status { get; set; }

    public DateTime? DeletionDate { get; set; }

    public DateTime? CreatedDate { get; set; }

    public List<ConversationMemberModel> Members { get; set; } = new List<ConversationMemberModel>();

    public ConversationListViewModel(ConversationMemberEntity entity)
    {
      Id = entity.ConversationId;
      Status = entity.Status;
      DeletionDate = entity.DeletionTime;
      CreatedDate = entity.CreatedDate;
    }

    public ConversationListViewModel(ConversationEntity entity)
    {
      Id = entity.Guid;
      CreatedDate = entity.CreatedDate;
    }
  }
}

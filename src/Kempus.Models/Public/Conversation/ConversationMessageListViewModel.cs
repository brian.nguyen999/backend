﻿using Kempus.Entities;

namespace Kempus.Models.Public.Conversation
{
  public class ConversationMessageListViewModel
  {
    public Guid Id { get; set; }

    public string? Message { get; set; }

    public Guid UserId { get; set; }

    public DateTime CreatedDate { get; set; }

    public ConversationMessageListViewModel(MessageEntity entity)
    {
      Id = entity.Guid;
      Message = entity.Content;
      UserId = entity.CreatedUser;
      CreatedDate = entity.CreatedDate;
    }
  }
}

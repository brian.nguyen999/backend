﻿using Kempus.Core.Errors;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.Conversation
{
  public class CreateConversationMessageRequestModel
  {
    [StringLength(1000, ErrorMessage = ErrorMessages.Invalid_Request, MinimumLength = 1)]
    public string? Message { get; set; }
  }
}

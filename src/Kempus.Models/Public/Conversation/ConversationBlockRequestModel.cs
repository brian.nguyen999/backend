﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.Conversation
{
  public class ConversationBlockRequestModel
  {
    [Required]
    public Guid ConversationId { get; set; }
    public bool IsDeleteChat { get; set; } = false;
  }
}

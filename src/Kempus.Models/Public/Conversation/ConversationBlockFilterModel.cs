﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.Conversation
{
  public class ConversationBlockFilterModel
  {

    public Guid UserId { get; set; }

    public string? Keyword { get; set; }
  }
}

﻿using System.ComponentModel.DataAnnotations;

namespace Kempus.Models.Public.Conversation
{
  public class ConversationListRequestModel
  {
    [Required]
    public byte Status { get; set; }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.Conversation
{
  public class ConversationMemberModel
  {
    public Guid Id { get; set; }

    public Guid ConversationId { get; set; }

    public bool IsHasNewMessage { get; set; }

    public string? NickName { get; set; }

    public byte Status { get; set; }
  }
}

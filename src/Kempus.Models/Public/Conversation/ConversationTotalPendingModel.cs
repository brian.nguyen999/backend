﻿namespace Kempus.Models.Public.Conversation
{
  public class ConversationTotalPendingModel
  {
    public int TotalPending { get; set; }
  }
}

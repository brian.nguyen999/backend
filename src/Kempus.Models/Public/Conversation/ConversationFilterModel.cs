﻿namespace Kempus.Models.Public.Conversation
{
  public class ConversationFilterModel
  {
    public Guid? SchoolId { get; set; }

    public Guid? UserId { get; set; }

    public List<byte> Status { get; set; }
    public string? Keyword { get; set; }
  }
}

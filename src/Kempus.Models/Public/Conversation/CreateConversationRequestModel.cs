﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.Conversation
{
  public class CreateConversationRequestModel
  {
    public Guid ToUserId { get; set; }
  }
}

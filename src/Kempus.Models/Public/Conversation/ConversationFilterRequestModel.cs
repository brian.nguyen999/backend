﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.Conversation
{
  public class ConversationFilterRequestModel
  {
    public string? Keyword { get; set; }
  }
}

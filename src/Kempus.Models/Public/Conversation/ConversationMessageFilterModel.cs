﻿namespace Kempus.Models.Public.Conversation
{
  public class ConversationMessageFilterModel
  {
    public Guid ConversationId { get; set; }

    public string? Keyword { get; set; }

    public DateTime? DateTime { get; set; }

    public Guid SchoolId { get; set; }

    public DateTime? ConversationDeletionTime { get; set; }
  }
}

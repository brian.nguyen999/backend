﻿namespace Kempus.Models.Public.Conversation
{
  public class ConversationTotalUnreadMessageModel
  {
    public long TotalUnread { get; set; }
  }
}

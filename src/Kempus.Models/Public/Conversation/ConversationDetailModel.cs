﻿using Kempus.Entities;

namespace Kempus.Models.Public.Conversation
{
  public class ConversationDetailModel
  {
    public Guid Id { get; set; }

    public string? Name { get; set; }

    public List<ConversationMemberModel> Members { get; set; } = new List<ConversationMemberModel>();

    public byte Status { get; set; }

    public DateTime CreatedDate { get; set; }

    public ConversationDetailModel(ConversationEntity entity)
    {
      Id = entity.Guid;
      CreatedDate = entity.CreatedDate;
      Name = entity.Name;
    }
  }
}

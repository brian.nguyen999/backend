﻿namespace Kempus.Models.Public.Comment
{
  public class PostCommentInputModel
  {
    public Guid PostId { get; set; }
    public Guid? ParentId { get; set; }
    public string Content { get; set; }
    public byte Type { get; set; }
  }
}
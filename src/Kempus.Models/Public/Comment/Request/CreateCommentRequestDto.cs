﻿namespace Kempus.Models.Public.Comment.Request
{
  public class CreateCommentRequestDto
  {
    public Guid PostId { get; set; }
    public Guid? ParentId { get; set; }
    public string Content { get; set; }
    public byte Type { get; set; }
  }
}

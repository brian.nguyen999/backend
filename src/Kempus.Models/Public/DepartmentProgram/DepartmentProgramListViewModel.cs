﻿using Kempus.Core.Extensions;
using Kempus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Models.Public.DepartmentProgram
{
  public class DepartmentProgramListViewModel
  {
    public long Id { get; set; }
    public Guid? Guid { get; set; }
    public string? Name { get; set; }
    public DateTime? ModifiedDate { get; set; }
    public string Status { get; set; }

    public DepartmentProgramListViewModel(DepartmentProgramEntity entity)
    {
      Id = entity.Id;
      Guid = entity.Guid;
      Name = entity.Name;
      Status = EnumExtensions.GetEnumDescription((DepartmentProgramStatus)entity.Status);
      ModifiedDate = entity.UpdatedDate ?? entity.CreatedDate;
    }
  }
}

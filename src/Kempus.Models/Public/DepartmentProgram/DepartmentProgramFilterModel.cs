﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.DepartmentProgram
{
  public class DepartmentProgramFilterModel
  {
    public Guid? Id { get; set; }
    public byte? Status { get; set; }
    public Guid? SchoolId { get; set; }
    public string? Keyword { get; set; }
  }
}

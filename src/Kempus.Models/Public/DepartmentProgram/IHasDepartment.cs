﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.DepartmentProgram
{
  public interface IHasDepartment
  {
    public Guid? DepartmentId { get; set; }

    public string DepartmentName { get; set; }
  }
}

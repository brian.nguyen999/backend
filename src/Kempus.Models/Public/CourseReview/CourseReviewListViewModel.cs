﻿using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.Models.Public.Course;
using Kempus.Models.Public.CourseReviewLike;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Models.Public.CourseReview
{
  public class CourseReviewListViewModel : IHasCourseReviewLike, IHasNumberOfLikes
  {
    public Guid Id { get; set; }

    public bool IsRequiredTextBook { get; set; }

    public byte DifficultRating { get; set; }

    public double HoursSpend { get; set; }

    public byte NumberOfExams { get; set; }

    public bool IsRequiredGroupProject { get; set; }

    public byte GradingCriteriaRating { get; set; }

    public string UserGrade { get; set; }

    public byte UserAchievementRating { get; set; }

    public Guid? CourseId { get; set; }

    public Guid CreatedBy { get; set; }

    public string UserName { get; set; }

    public DateTime CreatedDate { get; set; }

    public byte RecommendRating { get; set; }

    public long NumberOfLike { get; set; }

    public List<string> Characteristics { get; set; }

    public bool? IsLocked { get; set; }

    public string? Description { get; set; }

    public bool IsLiked { get; set; }

    public CourseDetailModel Course { get; set; }

    public CourseReviewListViewModel(CourseReviewEntity entity)
    {
      Id = entity.Guid;
      IsRequiredTextBook = entity.IsRequiredTextBook;
      DifficultRating = entity.DifficultRating;
      HoursSpend = entity.HoursSpend;
      NumberOfExams = entity.NumberOfExams;
      IsRequiredGroupProject = entity.IsRequiredGroupProject;
      GradingCriteriaRating = entity.GradingCriteriaRating;
      UserGrade = Enum.GetName(typeof(UserGrading), entity.UserGrade);
      UserAchievementRating = entity.UserAchievementRating;
      CourseId = entity.CourseId;
      CreatedBy = entity.CreatedBy;
      CreatedDate = entity.CreatedDate;
      RecommendRating = entity.RecommendRating;
      Description = entity.Comment;
      Characteristics = JsonUtils.Convert<List<string>>(entity.Characteristics);
    }
  }
}
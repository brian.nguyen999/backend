﻿using Kempus.Models.Public.DepartmentProgram;

namespace Kempus.Models.Public.CourseReview
{
  public class CourseReviewDetailModel : IHasDepartment
  {
    public string? CourseName { get; set; }
    public List<string> InstructorNames { get; set; }
    public int TotalReviews { get; set; }
    public bool IsRequiredTextbook { get; set; }
    public byte NumberOfExams { get; set; }
    public bool IsRequiredGroupProjects { get; set; }
    public double RecommendedRate { get; set; }
    public double GradingCriteriaRate { get; set; }
    public double CourseDifficultyRate { get; set; }
    public List<GradePercent> StudentGradePercents { get; set; }
    public double HoursPerWeekRate { get; set; }
    public double GoalsAchivedRate { get; set; }
    public List<string> TopThreeCharacteristicInstructors { get; set; } = new();
    public Guid? DepartmentId { get; set; }
    public string DepartmentName { get; set; }
  }

  public class GradePercent
  {
    public string Grade { get; set; }
    public int Percent { get; set; }
  }
}
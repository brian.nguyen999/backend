﻿using Kempus.Core.Models;

namespace Kempus.Models.Public.CourseReview
{
  public class CourseReviewFilterModel
  {
    public string? Keyword { get; set; }
    public Guid? CourseId { get; set; }
    public Guid? UserId { get; set; }
    public Guid? SchoolId { get; set; }
    public List<Guid?> CourseIds { get; set; }
    public List<Guid>? CourseReviewIds { get; set; }
    public byte? Status { get; set; }
  }
}
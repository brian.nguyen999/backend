﻿using Kempus.Core.Models;

namespace Kempus.Models.Public.CourseReview.Request
{
  public class SearchCourseReviewRequestDto : Pageable
  {
    public string? Keyword { get; set; }
    public Guid? CourseId { get; set; }
  }
}

﻿using System.ComponentModel.DataAnnotations;
using Kempus.Core.Constants;
using Kempus.Core.Errors;

namespace Kempus.Models.Public.CourseReview
{
  public class CourseReviewInputModel : IValidatableObject
  {
    [Required] public Guid CourseId { get; set; }

    /// <summary>
    /// Question 1: Is the course text book required for this course?
    /// </summary>
    public bool IsRequiredTextBook { get; set; }

    /// <summary>
    /// Question 2: How difficult will you rate this course?
    /// </summary>
    [Range(1, 5, ErrorMessage = ErrorMessages.CourseReview_DifficultRatingInvalidRange)]
    public byte DifficultRating { get; set; }

    /// <summary>
    /// Question 3: On average, how many hours per week do you spend on this course (excluding attending classes)
    /// </summary>
    [Range(0, 999999, ErrorMessage = ErrorMessages.CourseReview_HoursSpendInvalidRange)]
    public double HoursSpend { get; set; }

    /// <summary>
    /// Question 4: Number of exams (e.g. mid / final terms)
    /// </summary>
    [Range(1, 3, ErrorMessage = ErrorMessages.CourseReview_NumberOfExamsInvalidRange)]
    public byte NumberOfExams { get; set; }

    /// <summary>
    /// Question 5: Is group projects or presentation required for this course?
    /// </summary>
    public bool IsRequiredGroupProject { get; set; }

    /// <summary>
    /// Question 6: Is the grading criteria clear?
    /// </summary>
    [Range(1, 5, ErrorMessage = ErrorMessages.CourseReview_GradingCriteriaRatingInvalidRange)]
    public byte GradingCriteriaRating { get; set; }

    /// <summary>
    /// Question 7: What was your grade?
    /// </summary>
    public string UserGrade { get; set; }

    // Question 8: Did you achieve the goals as stated on the course syllabus?
    [Range(1, 5, ErrorMessage = ErrorMessages.CourseReview_UserAchievementRatingInvalidRange)]
    public byte UserAchievementRating { get; set; }

    /// <summary>
    /// Question 9: What are some characteristics of the instructor?
    /// </summary>
    public List<string> Characteristics { get; set; } = new List<string>();

    /// <summary>
    /// Question 10: Would you recommend this course to others?
    /// </summary>
    [Range(1, 5, ErrorMessage = ErrorMessages.CourseReview_RecommendRatingInvalidRange)]
    public byte RecommendRating { get; set; }

    /// <summary>
    /// Question 11: What do you want other students to know about this course?
    /// </summary>
    [MinLength(100, ErrorMessage = ErrorMessages.CourseReview_CommenInvalidLenght)]
    public string? Comment { get; set; }

    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
      if (Characteristics != null && Characteristics.Any())
      {
        var invalidValues = Characteristics
          .Where(x => !CoreConstants.InstructorCharacteristics.Contains(x))
          .ToList();
        if (invalidValues.Any())
        {
          yield return new ValidationResult(ErrorMessages.CourseReview_CharacteristicsInvaliValue);
        }
      }
    }
  }
}
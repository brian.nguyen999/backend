﻿using Kempus.Entities;

namespace Kempus.Models.Public.Trending.cs
{
  public class PostPollTrendingListView
  {
    public Guid ObjectId { get; set; }

    public byte ObjectType { get; set; }

    public string Title { get; set; }

    public PostPollTrendingListView() { }

    public PostPollTrendingListView(PostPollActivitySummaryEntity entity)
    {
      ObjectId = entity.ObjectId;
      ObjectType = entity.ObjectType;
    }
  }
}

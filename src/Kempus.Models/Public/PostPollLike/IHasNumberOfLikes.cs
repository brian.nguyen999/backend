﻿namespace Kempus.Models.Public.PostPollLike
{
  public interface IHasNumberOfLikes
  {
    public Guid Id { get; set; }

    public long NumberOfLike { get; set; }
  }
}

﻿using Kempus.Core.Enums;

namespace Kempus.Models.Public.PostPollLike
{
  public class PostPollLikeInputModel
  {
    public Guid ObjectId { get; set; }

    public CoreEnums.PostType ObjectType { get; set; }

    public Guid UserId { get; set; }

    public Guid SchoolId { get; set; }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.UserReferralStatistic
{
  public class UserReferralStatisticRefreshModel
  {
    public Guid InviterId { get; set; }
    public string InviterReferralCode { get; set; }
    public virtual Guid? CreatedBy { get; set; }
  }
}

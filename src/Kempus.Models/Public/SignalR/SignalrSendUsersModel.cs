﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.SignalR
{
  public class SignalrSendUsersModel
  {
    public List<int> UserIds { get; set; }
    public object Data { get; set; }
  }
}
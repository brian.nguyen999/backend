﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.SignalR
{
  public class RealTimeUpdateModel
  {
    public int Action { get; set; }
    public int ObjectTypeId { get; set; }
    public object ObjectId { get; set; }
    public object? Data { get; set; }

    public RealTimeUpdateModel()
    {
    }
  }
}
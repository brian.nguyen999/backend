﻿namespace Kempus.Models.Public.BellNotification
{
  public class BellNotificationFilterModel
  {
    public byte? ObjectTypeId { get; set; }
    public DateTime? BeforeTime { get; set; } // this field keep correct the paginator
  }
}

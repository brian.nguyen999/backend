﻿using Kempus.Entities;

namespace Kempus.Models.Public.BellNotification
{
  public class BellNotificationListViewModel
  {
    public Guid Id { get; set; }
    public string NickName { get; set; }
    public string Title { get; set; }
    public string Content { get; set; }
    public string UrlLink { get; set; }
    public string Category { get; set; } = "Activity"; // TODO: need update when confirmed logic
    public Guid? ObjectId { get; set; }
    public byte? ObjectTypeId { get; set; }
    public byte BellNotificationType { get; set; }
    public Guid BellNotificationUserId { get; set; }
    public bool IsRead { get; set; }
    public DateTime CreatedDate { get; set; }

    public BellNotificationListViewModel(BellNotificationEntity entity)
    {
      Id = entity.Guid;
      Title = entity.Title;
      BellNotificationType = entity.Type;
      UrlLink = entity.UrlLink;
      ObjectId = entity.ObjectId;
      ObjectTypeId = entity.ObjectTypeId;
      CreatedDate = entity.CreatedDate;
    }
  }
}

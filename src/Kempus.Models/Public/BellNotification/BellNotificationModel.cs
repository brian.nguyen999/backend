﻿namespace Kempus.Models.Public.BellNotification
{
  public class BellNotificationCreateModel
  {
    public Guid Id { get; set; }
    public string Title { get; set; }
    public string? Description { get; set; }
    public byte? ObjectTypeId { get; set; }
    public Guid? CreatedUser { get; set; }
    public List<Guid> UserIds { get; set; } = new List<Guid>();
  }
}

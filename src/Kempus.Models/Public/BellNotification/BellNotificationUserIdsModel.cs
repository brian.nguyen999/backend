﻿namespace Kempus.Models.Public.BellNotification
{
  public class BellNotificationUserIdsModel
  {
    public Guid Id { get; set; }
    public List<Guid> UserIds { get; set; } = new List<Guid>();
  }
}

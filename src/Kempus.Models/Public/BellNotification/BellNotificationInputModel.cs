﻿namespace Kempus.Models.Public.BellNotification
{
  public class BellNotificationInputModel
  {
    public Guid? Id { get; set; }
    public List<Guid> SchoolIds { get; set; } = new List<Guid>();
    public string Title { get; set; }
    public string Description { get; set; }
    public string? UrlLink { get; set; }
    public DateTime? CreatedDate { get; set; }
    public DateTime? UpdatedDate { get; set; }
    public byte ObjectTypeId { get; set; }
    public Guid ObjectId { get; set; }
    public Guid CreatedUser { get; set; }
    public Guid? UpdatedUser { get; set; }
    public int UserAction { get; set; }
    public byte? BellNotificationStatus { get; set; }
    public byte BellNotificationType { get; set; }
    public DateTime? ScheduledDate { get; set; }
  }
}

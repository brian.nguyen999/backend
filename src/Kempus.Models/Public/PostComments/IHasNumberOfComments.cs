﻿namespace Kempus.Models.Public.PostComments
{
  public interface IHasNumberOfComments
  {
    public Guid Id { get; set; }

    public long NumberOfComment { get; set; }
  }
}

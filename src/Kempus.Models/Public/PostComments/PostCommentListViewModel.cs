﻿using Kempus.Entities;

namespace Kempus.Models.Public.PostComments
{
  public class PostCommentListViewModel
  {
    public Guid Id { get; set; }

    public Guid PostId { get; set; }

    public string Content { get; set; }

    public Guid CreatedBy { get; set; }

    public string NickName { get; set; }

    public DateTime? CreatedDate { get; set; }

    public Guid? ParentId { get; set; }

    public long NumberOfReplies { get; set; }

    public List<PostCommentListViewModel> Replies { get; set; }

    public PostCommentListViewModel(PostCommentEntity entity)
    {
      Id = entity.Guid;
      PostId = entity.PostId;
      Content = entity.Content;
      CreatedDate = entity.CreatedDate;
      CreatedBy = entity.CreatedBy;
      ParentId = entity.ParentId;
    }
  }
}
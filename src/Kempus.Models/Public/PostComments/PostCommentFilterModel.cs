﻿using Kempus.Core.Models;

namespace Kempus.Models.Public.PostComments
{
  public class PostCommentFilterModel
  {
    public Guid? PostId { get; set; }

    public Guid? ParentId { get; set; }
  }
}

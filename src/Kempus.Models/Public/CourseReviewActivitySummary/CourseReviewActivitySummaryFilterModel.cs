﻿namespace Kempus.Models.Public.CourseReviewActivity
{
  public class CourseReviewActivitySummaryFilterModel
  {
    public Guid? SchoolId { get; set; }

    public byte? Status { get; set; }
  }
}

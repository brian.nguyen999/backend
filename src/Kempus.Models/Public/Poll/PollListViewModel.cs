using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.Models.Public.Bookmark;
using Kempus.Models.Public.PollOption;
using Kempus.Models.Public.PostComments;
using Kempus.Models.Public.PostPollLike;
using Kempus.Models.Public.Tracking;
using Kempus.Models.Public.Vote;

namespace Kempus.Models.Public.Poll
{
  public class PollListViewModel : IHasBookmark, IHasLike, IHasVote, IHasNumberOfLikes, IHasNumberOfComments, IHasNumberOfViews
  {
    public Guid Id { get; set; }

    public long NumberOfView { get; set; }

    public Guid PollId { get; set; }

    public bool IsLiked { get; set; }

    public bool IsBookmarked { get; set; }

    public bool IsVoted { get; set; }

    public bool IsEdited { get; set; }

    public Guid PostId { get; set; }

    public string Title { get; set; }

    public string Content { get; set; }

    public string Type { get; set; }

    public DateTime CreatedDate { get; set; }

    public List<string> Keywords { get; set; }

    public Guid CreatedBy { get; set; }

    public string UserName { get; set; }

    public long NumberOfVote { get; set; }

    public long NumberOfLike { get; set; }

    public long NumberOfComment { get; set; }

    public bool IsPinned { get; set; }

    public List<PollOptionDetailModel> PollOptions { get; set; }

    public PollListViewModel(PostEntity entity)
    {
      Id = entity.Guid;
      PostId = entity.Guid;
      Title = entity.Title;
      Content = entity.Content;
      CreatedDate = entity.CreatedDate;
      Keywords = JsonUtils.Convert<List<string>>(entity.Keywords);
      CreatedBy = entity.CreatedBy;
      IsPinned = entity.IsPinned;
      IsEdited = entity.UpdatedDate != null;
    }
  }
}
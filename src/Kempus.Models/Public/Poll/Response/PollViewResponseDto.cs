﻿using Kempus.Models.Public.Bookmark;
using Kempus.Models.Public.PollOption;
using Kempus.Models.Public.PostComments;
using Kempus.Models.Public.PostPollLike;
using Kempus.Models.Public.Tracking;
using Kempus.Models.Public.Vote;
using Newtonsoft.Json;

namespace Kempus.Models.Public.Poll.Response
{
  public class PollViewResponseDto : IHasBookmark, IHasVote, IHasLike, IHasNumberOfLikes, IHasNumberOfComments, IHasNumberOfViews
  {
    public Guid Id { get; set; }

    public Guid PollId { get; set; }

    public bool IsLiked { get; set; }

    [JsonIgnore]
    public Guid PostId { get; set; }

    public bool IsBookmarked { get; set; }

    public bool IsVoted { get; set; }

    public bool IsEdited { get; set; }

    public string Title { get; set; }

    public string Type { get; set; }

    public List<string> Keywords { get; set; }

    public string Content { get; set; }

    public List<PollOptionDetailModel> Options { get; set; } = new List<PollOptionDetailModel>();

    public DateTime? CreatedDate { get; set; }

    public string? CreatedBy { get; set; }

    public Guid? CreatedUserId { get; set; }

    public long NumberOfVote { get; set; }

    public long NumberOfLike { get; set; }

    public long NumberOfComment { get; set; }

    public long NumberOfView { get; set; }

    public bool IsPinned { get; set; }
  }
}
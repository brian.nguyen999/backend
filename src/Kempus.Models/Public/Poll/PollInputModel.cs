﻿using Kempus.Core.Errors;
using System.ComponentModel.DataAnnotations;

namespace Kempus.Models.Public.Poll
{
  public class PollInputModel
  {
    [StringLength(300, ErrorMessage = ErrorMessages.Poll_TitleMaxLenght)]
    public string Title { get; set; }

    public List<string> Keywords { get; set; }

    [StringLength(10000, ErrorMessage = ErrorMessages.Poll_DescriptionMaxLength)]
    public string Content { get; set; }

    public string Type { get; set; }

    public bool IsPinned { get; set; }

    public List<string> Options { get; set; } = new List<string>();
  }
}
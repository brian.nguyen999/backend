﻿namespace Kempus.Models.Public.Poll
{
  public class PollFilterModel
  {
    public List<Guid> PostIds { get; set; } = new List<Guid>();

    public string? Keyword { get; set; }

    public string? PollStatus { get; set; }

    public Guid? UserId { get; set; }

    public Guid? SchoolId { get; set; }
  }
}
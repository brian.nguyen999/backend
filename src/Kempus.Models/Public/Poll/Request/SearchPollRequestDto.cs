﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kempus.Core.Models;

namespace Kempus.Models.Public.Poll.Request
{
  public class SearchPollRequestDto : Pageable
  {
    public Guid? PostId { get; set; }

    public string? Keyword { get; set; }

    public string? PollStatus { get; set; }

    public Guid? UserId { get; set; }
  }
}

using Kempus.Models.Public.Bookmark;
using Kempus.Models.Public.PollOption;
using Kempus.Models.Public.PostComments;
using Kempus.Models.Public.PostPollLike;
using Kempus.Models.Public.Tracking;
using Kempus.Models.Public.Vote;
using Newtonsoft.Json;

namespace Kempus.Models.Public.Poll
{
  public class PollDetailModel : IHasBookmark, IHasLike, IHasVote, IHasNumberOfLikes, IHasNumberOfComments, IHasNumberOfViews
  {
    public Guid Id { get; set; }

    public Guid PollId { get; set; }

    public bool IsLiked { get; set; }

    public bool IsVoted { get; set; }

    [JsonIgnore]
    public Guid PostId { get; set; }

    public bool IsBookmarked { get; set; }

    public string Title { get; set; }

    public List<string> Keywords { get; set; }

    public string Content { get; set; }

    public string Type { get; set; }

    public bool IsEditable { get; set; }

    public int RemainSeconds { get; set; }

    public bool IsPinned { get; set; }

    public long NumberOfVote { get; set; }

    public long NumberOfLike { get; set; }

    public long NumberOfComment { get; set; }

    public long NumberOfView { get; set; }

    public List<PollOptionDetailModel> Options { get; set; } = new List<PollOptionDetailModel>();
  }
}
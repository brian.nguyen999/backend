﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.Status.Response
{
  public class AppSettingConfig
  {
    public string NickNameLockdownMinutes { get; set; }
  }
}
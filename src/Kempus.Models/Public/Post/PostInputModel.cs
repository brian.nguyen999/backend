﻿using System.ComponentModel.DataAnnotations;
using Kempus.Core.Errors;

namespace Kempus.Models.Public.Post
{
  public class PostInputModel
  {
    public Guid? Id { get; set; }

    [StringLength(300, ErrorMessage = ErrorMessages.Post_TitleMaxLenght)]
    public string Title { get; set; }

    [StringLength(10000, ErrorMessage = ErrorMessages.Post_DescriptionMaxLength)]
    public string Content { get; set; }

    public List<string> Keywords { get; set; } = new List<string>();

    public int? Type { get; set; }

    public Guid? TopicId { get; set; }

    public Guid? UpdatedBy { get; set; }

    public bool IsPinned { get; set; }
  }
}
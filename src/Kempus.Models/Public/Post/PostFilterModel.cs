﻿namespace Kempus.Models.Public.Post
{
  public class PostFilterModel
  {
    public List<Guid> PostIds { get; set; } = new List<Guid>();

    public string? Keyword { get; set; }

    public Guid? TopicId { get; set; }

    public Guid? UserId { get; set; }

    public Guid? SchoolId { get; set; }

    public bool? IsPublished { get; set; } = true;
  }
}
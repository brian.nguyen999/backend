﻿using Kempus.Core.Enums;
using Kempus.Core.Errors;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Public.Post
{
  public class PostUpdateInputModel
  {
    public Guid? Id { get; set; }

    [StringLength(300, ErrorMessage = ErrorMessages.Post_TitleMaxLenght)]
    public string Title { get; set; }

    [StringLength(10000, ErrorMessage = ErrorMessages.Post_DescriptionMaxLength)]
    public string Content { get; set; }

    public List<string> Keywords { get; set; } = new List<string>();

    public int Type { get; set; } = (byte)CoreEnums.PostType.Post;

    public bool IsPublished { get; set; } = true;

    public Guid? SchoolId { get; set; }

    public Guid? TopicId { get; set; }
  }
}
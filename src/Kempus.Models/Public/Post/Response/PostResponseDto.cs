﻿namespace Kempus.Models.Public.Post.Response
{
  public class PostResponseDto
  {
    public Guid Id { get; set; }
    public string Title { get; set; }
    public string Content { get; set; }
    public string Keywords { get; set; }
    public string Type { get; set; }
    public Guid? SchoolId { get; set; }
    public Guid? TopicId { get; set; }
  }
}
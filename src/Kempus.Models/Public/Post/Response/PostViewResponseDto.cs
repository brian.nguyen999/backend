using Kempus.Models.Public.Bookmark;
using Kempus.Models.Public.PostComments;
using Kempus.Models.Public.PostPollLike;
using Kempus.Models.Public.Tracking;
using System.Text.Json.Serialization;

namespace Kempus.Models.Public.Post.Response
{
  public class PostViewResponseDto : IHasBookmark, IHasLike, IHasNumberOfLikes, IHasNumberOfComments, IHasNumberOfViews
  {
    public Guid Id { get; set; }
    public long NumberOfView { get; set; }

    public long NumberOfComment { get; set; }

    public long NumberOfLike { get; set; }

    public bool IsLiked { get; set; }

    [JsonIgnore]
    public Guid PostId { get; set; }

    public bool IsBookmarked { get; set; }

    public bool IsEdited { get; set; }

    public string Title { get; set; }

    public string Content { get; set; }

    public List<string> Keywords { get; set; } = new List<string>();

    public string Topic { get; set; }

    public DateTime? CreatedDate { get; set; }

    public string? CreatedBy { get; set; }

    public Guid? CreatedUserId { get; set; }
  }
}
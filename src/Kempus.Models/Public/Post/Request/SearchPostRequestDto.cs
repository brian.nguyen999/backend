﻿using Kempus.Core.Models;

namespace Kempus.Models.Public.Post.Request
{
  public class SearchPostRequestDto : Pageable
  {
    public Guid? PostId { get; set; }

    public string? Keyword { get; set; }

    public Guid? TopicId { get; set; }

    public Guid? UserId { get; set; }
  }
}

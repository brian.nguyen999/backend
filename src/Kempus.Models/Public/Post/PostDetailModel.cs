﻿using Kempus.Models.Public.Bookmark;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using Kempus.Models.Public.PostComments;
using Kempus.Models.Public.PostPollLike;
using Kempus.Models.Public.Tracking;

namespace Kempus.Models.Public.Post
{
  public class PostDetailModel : IHasBookmark, IHasLike, IHasNumberOfLikes, IHasNumberOfComments, IHasNumberOfViews
  {
    public Guid Id { get; set; }
    public long NumberOfView { get; set; }

    public long NumberOfComment { get; set; }

    public long NumberOfLike { get; set; }

    public bool IsLiked { get; set; }

    [JsonIgnore]
    public Guid PostId { get; set; }

    public bool IsBookmarked { get; set; }

    public string Title { get; set; }

    [StringLength(500)]
    public string Content { get; set; }

    public List<string> Keywords { get; set; } = new List<string>();

    public Guid? TopicId { get; set; }

    public bool IsEditable { get; set; }

    public int RemainSeconds { get; set; }
  }
}
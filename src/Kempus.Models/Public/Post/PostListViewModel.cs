﻿using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.Models.Public.Bookmark;
using Kempus.Models.Public.PostComments;
using Kempus.Models.Public.PostPollLike;
using Kempus.Models.Public.Tracking;
using Newtonsoft.Json;

namespace Kempus.Models.Public.Post
{
  public class PostListViewModel : IHasBookmark, IHasLike, IHasNumberOfLikes, IHasNumberOfComments, IHasNumberOfViews
  {
    public Guid Id { get; set; }

    public bool IsLiked { get; set; }

    [JsonIgnore]
    public Guid PostId { get; set; }

    public Guid? TopicId { get; set; }

    public string Topic { get; set; }

    public string Title { get; set; }

    public string Content { get; set; }

    public DateTime CreatedDate { get; set; }

    public List<string> Keywords { get; set; }

    public Guid CreatedBy { get; set; }

    public string UserName { get; set; }

    public long NumberOfView { get; set; }

    public long NumberOfLike { get; set; }

    public long NumberOfComment { get; set; }

    public bool IsBookmarked { get; set; }
    public bool IsEdited { get; set; }

    public PostListViewModel(PostEntity entity)
    {
      Id = entity.Guid;
      PostId = entity.Guid;
      TopicId = entity.TopicId;
      Title = entity.Title;
      Content = entity.Content;
      CreatedDate = entity.CreatedDate;
      Keywords = JsonUtils.Convert<List<string>>(entity.Keywords);
      CreatedBy = entity.CreatedBy;
      IsEdited = entity.UpdatedDate != null;
    }
  }
}
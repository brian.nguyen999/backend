﻿using System.ComponentModel.DataAnnotations;

namespace Kempus.Models.Admin.Post
{
  public class PostDetailModel
  {
    public long Id { get; set; }

    public Guid Guid { get; set; }

    public string Title { get; set; }

    public string Content { get; set; }

    public List<string> Keywords { get; set; } = new List<string>();

    public Guid? TopicId { get; set; }

    public bool IsPublished { get; set; }
  }
}

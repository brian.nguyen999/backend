﻿using Kempus.Core.Utils;
using Kempus.Entities;

namespace Kempus.Models.Admin.Post
{
  public class PostListViewModel
  {
    public long Id { get; set; }

    public Guid Guid { get; set; }

    public Guid? TopicId { get; set; }

    public string Topic { get; set; }

    public string Title { get; set; }

    public DateTime CreatedDate { get; set; }

    public List<string> Keywords { get; set; }

    public Guid CreatedBy { get; set; }

    public string UserName { get; set; }

    public int NumberOfView { get; set; } = 100; // TODO: need update when confirm logic View post

    public int NumberOfLike { get; set; } = 190; // TODO: need update when confirm logic Like post

    public int NumberOfComment { get; set; } = 20; // TODO: need update when confirm logic Comment post

    public bool IsPublished { get; set; }

    public PostListViewModel(PostEntity entity)
    {
      Id = entity.Id;
      Guid = entity.Guid;
      TopicId = entity.TopicId;
      Title = entity.Title;
      CreatedDate = entity.CreatedDate;
      Keywords = JsonUtils.Convert<List<string>>(entity.Keywords);
      CreatedBy = entity.CreatedBy;
      IsPublished = entity.IsPublished;
    }
  }
}

﻿using Kempus.Core.Extensions;
using Kempus.Entities;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Models.Admin.BellNotification
{
  public class BellNotificationListViewCmsModel
  {
    public Guid Id { get; set; }
    public List<string> SchoolNames { get; set; }
    public string Title { get; set; }
    public DateTime CreatedDate { get; set; }
    public string Status { get; set; }
    public string? SchoolIds { get; set; }
    public BellNotificationListViewCmsModel(BellNotificationEntity entity)
    {
      Id = entity.Guid;
      Title = entity.Title;
      CreatedDate = entity.CreatedDate;
      Status = EnumExtensions.GetName<BellNotificationStatus>(entity.Status.GetValueOrDefault());
      SchoolIds = entity.SchoolIds;
    }
  }
}

﻿namespace Kempus.Models.Admin.BellNotification
{
  public class BellNotificationDetailCmsModel
  {
    public Guid Id { get; set; }
    public List<Guid> SchoolIds { get; set; } = new List<Guid>();
    public string Title { get; set; }
    public string? UrlLink { get; set; }
    public byte? BellNotificationStatus { get; set; }
    public DateTime? ScheduledDate { get; set; }
  }
}

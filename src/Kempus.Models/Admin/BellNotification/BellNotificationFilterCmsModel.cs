﻿namespace Kempus.Models.Admin.BellNotification
{
  public class BellNotificationFilterCmsModel
  {
    public List<Guid> Ids { get; set; } = new List<Guid>();
    public string? Keyword { get; set; }
    public Guid? SchoolId { get; set; }
  }
}

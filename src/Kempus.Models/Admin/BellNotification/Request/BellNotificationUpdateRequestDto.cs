﻿namespace Kempus.Models.Admin.BellNotification.Request
{
  public class BellNotificationUpdateRequestDto : BellNotificationCreateRequestDto
  {
    public Guid Id { get; set; }
  }
}

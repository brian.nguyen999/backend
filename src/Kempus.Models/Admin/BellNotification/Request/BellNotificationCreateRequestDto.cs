﻿using System.ComponentModel.DataAnnotations;

namespace Kempus.Models.Admin.BellNotification.Request
{
  public class BellNotificationCreateRequestDto
  {
    public List<Guid> SchoolIds { get; set; } = new List<Guid>();
    public string Title { get; set; }

    [Url(ErrorMessage ="Invalid Url")]
    public string? UrlLink { get; set; }
    public byte BellNotificationStatus { get; set; }
    public DateTime? ScheduledDate { get; set; }
  }
}

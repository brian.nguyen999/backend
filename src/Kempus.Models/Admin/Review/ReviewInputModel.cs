﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Admin.Review
{
  public class ReviewInputModel
  {
    public Guid? Id { get; set; }
    public byte? Status { get; set; }
    public Guid? UpdatedBy { get; set; }
  }
}

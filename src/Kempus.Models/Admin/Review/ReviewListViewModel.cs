﻿using Kempus.Core.Extensions;
using Kempus.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Models.Admin.Review
{
  public class ReviewListViewModel
  {
    public long Id { get; set; }
    public Guid Guid { get; set; }
    public string? CourseName { get; set; }
    public List<string> InstructorNames { get; set; }
    public string? ReviewedBy { get; set; }
    public Guid? ReviewerGuid { get; set; }
    // Number of Like
    public long Recommended { get; set; }
    public DateTime? CreatedOn { get; set; }
    public string StatusName { get; set; }

    [JsonIgnore]
    public byte? Status { get; set; }

    public ReviewListViewModel(CourseReviewEntity entity)
    {
      Id = entity.Id;
      Guid = entity.Guid;
      CreatedOn = entity.CreatedDate;
      Status = entity.Status;
      ReviewerGuid = entity.CreatedBy;
      Recommended = entity.RecommendRating;
      StatusName = EnumExtensions.GetName<CourseReviewStatus>(entity.Status);
    }
  }
}

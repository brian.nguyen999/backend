﻿using Kempus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Admin.Review
{
  public class ReviewDetailModel
  {
    public ReviewDetailModel()
    {
    }

    public Guid? Guid { get; set; }
    public string CourseName { get; set; }
    public List<string> InstructorNames { get; set; }
    public string WriteenBy { get; set; }
    public byte? Status { get; set; }
  }
}

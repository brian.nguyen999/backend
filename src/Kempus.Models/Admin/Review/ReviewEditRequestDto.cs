﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Admin.Review
{
  public class ReviewEditRequestDto
  {
    [Required]
    public Guid? Id { get; set; }
    public byte? Status { get; set; }
  }
}

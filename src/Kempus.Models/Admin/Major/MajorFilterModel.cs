﻿namespace Kempus.Models.Admin.Major
{
  public class MajorFilterModel
  {
    public string? Keyword { get; set; }

    public Guid? SchoolId { get; set; }
  }
}

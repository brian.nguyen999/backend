﻿using Kempus.Entities;

namespace Kempus.Models.Admin.Major
{
  public class MajorListViewModel
  {
    public long Id { get; set; }

    public Guid Guid { get; set; }

    public string? Name { get; set; }

    public bool IsActivated { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime? UpdatedDate { get; set; }

    public virtual Guid? SchoolId { get; set; }

    public MajorListViewModel(MajorEntity entity)
    {
      Id = entity.Id;
      Guid = entity.Guid;
      Name = entity.Name;
      IsActivated = entity.IsActivated;
      CreatedDate = entity.CreatedDate;
      UpdatedDate = entity.UpdatedDate;
      SchoolId = entity.SchoolId;
    }
  }
}

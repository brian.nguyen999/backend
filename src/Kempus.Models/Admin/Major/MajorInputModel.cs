﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Admin.Major
{
  public class MajorInputModel
  {
    public string? Name { get; set; }

    public bool IsActivated { get; set; }

    public virtual Guid? SchoolId { get; set; }

    public Guid? CreatedBy { get; set; }
  }
}

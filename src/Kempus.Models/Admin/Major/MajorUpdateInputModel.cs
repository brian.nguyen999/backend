﻿namespace Kempus.Models.Admin.Major
{
  public class MajorUpdateInputModel
  {
    public Guid Id { get; set; }

    public string? Name { get; set; }

    public bool? IsActivated { get; set; }

    public virtual Guid? SchoolId { get; set; }

    public Guid? UpdatedBy { get; set; }
  }
}

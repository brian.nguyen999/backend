﻿using Kempus.Core.Extensions;
using Kempus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Models.Admin.Kemp
{
  public class KempListViewModel
  {
    public Guid Id { get; set; }
    public string TransactionType { get; set; }
    public byte Type { get; set; }
    public object? TransactionReference { get; set; }
    public string? Student { get; set; }
    public Guid StudentGuid { get; set; }
    public int Amount { get; set; }
    public DateTime TransactionDate { get; set; }
    public bool IsAdminCreated { get; set; }
    public string AdminName { get; set; }

    [JsonIgnore]
    public Guid CreatorId { get; set; }

    public KempListViewModel(KempTransactionEntity entity)
    {
      Id = entity.Guid;
      TransactionType = EnumExtensions.GetEnumDescription((KempTransactionType)entity.Type);
      TransactionReference = entity.ReferenceGuid;
      StudentGuid = entity.UserId;
      Amount = entity.Amount;
      TransactionDate = entity.CreatedDate;
      IsAdminCreated = entity.IsAdminCreated;
      Type = entity.Type;
      CreatorId = entity.CreatedBy;
    }
  }
}

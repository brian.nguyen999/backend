﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Admin.Kemp
{
  public class KempTransactionCreateRequestDto
  {
    [Required]
    public byte Action { get; set; }
    [Required]
    [Range(1, 100)]
    public int Amount { get; set; }
    [Required]
    public byte TransactionType { get; set; }
    [Required]
    public Guid SchoolId { get; set; }
    [Required]
    public List<Guid> UserIds { get; set; }
    [Required]
    public bool IsSelectAllUser { get; set; } = false;
  }
}

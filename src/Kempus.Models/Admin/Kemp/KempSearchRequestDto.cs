﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Admin.Kemp
{
  public class KempSearchRequestDto
  {
    public Guid? Id { get; set; }
    public string? Keyword { get; set; }
    public byte? Type { get; set; }
    public Guid? SchoolId { get; set; }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Admin.Banner
{
  public class BannerDetailModel
  {
    public Guid Id { get; set; }
    public string? SchoolName { get; set; }
    public string? WebBannerFileName { get; set; }
    public string? WebBannerUrl { get; set; }
    public string? MobileBannerFileName { get; set; }
    public string? MobileBannerUrl { get; set; }
    public string? Url { get; set; }
    public byte? UrlType { get; set; }
    public byte? Status { get; set; }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Admin.Banner
{
  public class BannerCmsFilterModel
  {
    public Guid? Id { get; set; }
    public string? Keyword { get; set; }
    public byte? Status { get; set; }
    public Guid? SchoolId { get; set; }
  }
}

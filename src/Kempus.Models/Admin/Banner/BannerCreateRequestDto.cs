﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Admin.Banner
{
  public class BannerCreateRequestDto
  {
    [Required]
    public Guid SchoolId { get; set; }
    [Required]
    public IFormFile WebBanner { get; set; }
    [Required]
    public IFormFile MobileBanner { get; set; }
    [Required]
    public string Url { get; set; }
    [Required]
    public byte UrlType { get; set; }
    [Required]
    public byte Status { get; set; }
  }
}

﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Admin.Banner
{
  public class BannerEditRequestDto
  {
    [Required]
    public Guid Id { get; set; }
    public IFormFile? WebBanner { get; set; }
    public IFormFile? MobileBanner { get; set; }
    public string? Url { get; set; }
    public byte? UrlType { get; set; }
    public byte? Status { get; set; }
  }
}

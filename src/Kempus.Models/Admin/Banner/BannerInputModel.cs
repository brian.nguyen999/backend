﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Admin.Banner
{
  public class BannerInputModel
  {
    public Guid? Id { get; set; }
    public Guid SchoolId { get; set; }
    public IFormFile? WebBanner { get; set; }
    public IFormFile? MobileBanner { get; set; }
    public string? Url { get; set; }
    public byte? UrlType { get; set; }
    public byte? Status { get; set; }
    public Guid CreatedBy { get; set; }
    public Guid? UpdatedBy { get; set; }
  }
}

﻿using Kempus.Core.Extensions;
using Kempus.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Models.Admin.Banner
{
  public class BannerListViewModel
  {
    public Guid Id { get; set; }
    public string? WebImage { get; set; }
    public string? MobileImage { get; set; }
    public string? UrlLink { get; set; }
    public string? StatusName { get; set; }
    public string? Type { get; set; }
    public DateTime CreatedDate { get; set; }
    [JsonIgnore]
    public string SchoolName { get; set; }
    [JsonIgnore]
    public string WebBannerFileName { get; set; }
    [JsonIgnore]
    public string MobileBannerFileName { get; set; }
    [JsonIgnore]
    public byte? Status { get; set; }
    [JsonIgnore]
    public byte? UrlType { get; set; }

    public BannerListViewModel(BannerEntity entity)
    {
      Id = entity.Guid;
      WebImage = entity.WebUrl;
      MobileImage = entity.MobileUrl;
      UrlLink = entity.RedirectUrl;
      StatusName = EnumExtensions.GetEnumDescription((BannerStatus)entity.Status);
      Type = EnumExtensions.GetEnumDescription((BannerUrlType)entity.UrlType);
      CreatedDate = entity.CreatedDate;
      WebBannerFileName = entity.WebBannerFileName;
      MobileBannerFileName = entity.MobileBannerFileName;
      UrlType = entity.UrlType;
      Status = entity.Status;
    }
  }
}

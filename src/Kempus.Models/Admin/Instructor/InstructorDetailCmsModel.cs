﻿using Kempus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Admin.Instructor
{
  public class InstructorDetailCmsModel
  {
    public Guid Id { get; set; }
    public string Name { get; set; }
    public List<string> CourseNames { get; set; } = new List<string>();

    public InstructorDetailCmsModel(InstructorEntity entity)
    {
      Id = entity.Guid;
      Name = entity.Name;
    }
  }
}

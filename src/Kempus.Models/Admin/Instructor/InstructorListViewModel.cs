﻿using Kempus.Core.Extensions;
using Kempus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Models.Admin.Instructor
{
  public class InstructorListViewModel
  {
    public long Id { get; set; }
    public Guid Guid { get; set; }
    public string Name { get; set; }
    public long NumberOfCourseTaught { get; set; }
    public string Status { get; set; }
    public DateTime? ModifiedDate { get; set; }

    public InstructorListViewModel(InstructorEntity entity)
    {
      Id = entity.Id;
      Guid = entity.Guid;
      Name = entity.Name;
      Status = entity.IsActivated ? "Active" : "Deactivated";
      ModifiedDate = entity.UpdatedDate ?? entity.CreatedDate;
    }
  }
}

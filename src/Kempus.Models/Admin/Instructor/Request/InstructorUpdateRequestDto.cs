﻿namespace Kempus.Models.Admin.Instructor.Request
{
  public class InstructorUpdateRequestDto
  {
    public Guid Id { get; set; }
    public string Name { get; set; }
  }
}

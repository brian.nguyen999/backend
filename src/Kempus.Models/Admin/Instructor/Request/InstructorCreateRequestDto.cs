﻿namespace Kempus.Models.Admin.Instructor.Request
{
  public class InstructorCreateRequestDto
  {
    public Guid SchoolId { get; set; }
    public string Name { get; set; }
  }
}

﻿namespace Kempus.Models.Admin.Instructor
{
  public class InstructorInputCmsModel
  {
    public Guid? Id { get; set; }
    public Guid? CreatorId { get; set; }
    public Guid? UpdaterId { get; set; }
    public Guid? SchoolId { get; set; }
    public string Name { get; set; }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Admin.Instructor
{
  public class InstructorFilterModel
  {
    public Guid? Id { get; set; }
    public bool? Status { get; set; }
    public Guid? SchoolId { get; set; }
    public string? Keyword { get; set; }
  }
}

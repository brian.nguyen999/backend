﻿using Kempus.Core.Errors;
using System.ComponentModel.DataAnnotations;

namespace Kempus.Models.Admin.Poll
{
  public class PollInputModel
  {
    public Guid Id { get; set; }

    [StringLength(300, ErrorMessage = ErrorMessages.Poll_TitleMaxLenght)]
    public string Title { get; set; }

    public List<string> Keywords { get; set; }

    [StringLength(10000, ErrorMessage = ErrorMessages.Poll_DescriptionMaxLength)]
    public string Content { get; set; }

    public string Type { get; set; }

    public List<string> Options { get; set; } = new List<string>();

    public Guid? UpdatedBy { get; set; }

    public bool IsPinned { get; set; }

    public bool IsPublished { get; set; }
  }
}

﻿using Kempus.Models.Admin.PollOption;

namespace Kempus.Models.Admin.Poll
{
  public class PollDetailModel
  {
    public long Id { get; set; }

    public Guid Guid { get; set; }

    public string Title { get; set; }

    public List<string> Keywords { get; set; }

    public string Content { get; set; }

    public string Type { get; set; }

    public List<PollOptionDetailModel> Options { get; set; } = new List<PollOptionDetailModel>();

    public bool IsPinned { get; set; }

    public bool IsPublished { get; set; }
  }
}

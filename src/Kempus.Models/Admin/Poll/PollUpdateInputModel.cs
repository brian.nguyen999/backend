﻿using System.ComponentModel.DataAnnotations;
using Kempus.Core.Errors;
using Kempus.Models.Admin.PollOption;

namespace Kempus.Models.Admin.Poll
{
  public class PollUpdateInputModel
  {
    public Guid Id { get; set; }

    [StringLength(300, ErrorMessage = ErrorMessages.Poll_TitleMaxLenght)]
    public string Title { get; set; }

    public List<string> Keywords { get; set; }

    [StringLength(10000, ErrorMessage = ErrorMessages.Poll_DescriptionMaxLength)]
    public string Content { get; set; }

    public string? Type { get; set; }

    public List<PollOptionDetailModel> Options { get; set; } = new List<PollOptionDetailModel>();

    public Guid? UpdatedBy { get; set; }

    public bool IsPinned { get; set; }

    public bool IsPublished { get; set; }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Admin.Poll
{
  public class PollFilterModel
  {
    public List<Guid> PostIds { get; set; } = new List<Guid>();

    public string? Keyword { get; set; }

    public string? PollStatus { get; set; }

    public Guid? UserId { get; set; }

    public Guid? SchoolId { get; set; }
    public bool? PublishStatus { get; set; }
  }
}

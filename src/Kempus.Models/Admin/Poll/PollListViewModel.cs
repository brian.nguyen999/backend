﻿using Kempus.Core.Utils;
using Kempus.Entities;
using Kempus.Models.Admin.PollOption;

namespace Kempus.Models.Admin.Poll
{
  public class PollListViewModel
  {
    public long Id { get; set; }
    public Guid Guid { get; set; }
    public Guid PollId { get; set; }
    public string Title { get; set; }
    public string? Type { get; set; }
    public DateTime CreatedDate { get; set; }
    public List<string> Keywords { get; set; }
    public Guid CreatedBy { get; set; }
    public string UserName { get; set; }
    public long NumberOfVote { get; set; }
    public int NumberOfLike { get; set; }
    public int NumberOfComment { get; set; }
    public int NumberOfView { get; set; }
    public bool IsPublished { get; set; }

    public List<PollOptionModel> PollOptions { get; set; }

    public PollListViewModel(PostEntity entity)
    {
      Id = entity.Id;
      Guid = entity.Guid;
      Title = entity.Title;
      CreatedDate = entity.CreatedDate;
      Keywords = JsonUtils.Convert<List<string>>(entity.Keywords);
      CreatedBy = entity.CreatedBy;
      IsPublished = entity.IsPublished;
    }
  }
}

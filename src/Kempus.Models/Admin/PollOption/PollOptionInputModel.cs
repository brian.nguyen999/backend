﻿namespace Kempus.Models.Admin.PollOption
{
  public class PollOptionInputModel
  {
    public Guid PollId { get; set; }

    public List<string> Options { get; set; } = new List<string>();
  }
}

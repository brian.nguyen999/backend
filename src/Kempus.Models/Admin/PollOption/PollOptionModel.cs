﻿namespace Kempus.Models.Admin.PollOption
{
  public class PollOptionModel
  {
    public Guid? Id { get; set; }

    public string Content { get; set; }
  }
}

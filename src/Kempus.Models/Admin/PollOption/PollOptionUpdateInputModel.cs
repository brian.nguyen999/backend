﻿namespace Kempus.Models.Admin.PollOption
{
  public class PollOptionUpdateInputModel
  {
    public Guid PollId { get; set; }

    public List<PollOptionDetailModel> Options { get; set; } = new List<PollOptionDetailModel>();
  }
}

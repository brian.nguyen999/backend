﻿using Kempus.Core.Extensions;
using Kempus.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.Models.Admin.Flag
{
  public class FlagListViewModel
  {
    public Guid Id { get; set; }
    public string? FlagType { get; set; }
    public string? Content { get; set; }
    public string? UrlContent { get; set; }
    public string? School { get; set; }
    public string? FlaggedStudent { get; set; }
    public string? FlaggedBy { get; set; }
    public string? Status { get; set; }
    public DateTime? ReportedDate { get; set; }
    public Guid FlaggedStudentGuid { get; set; }
    public Guid FlaggedByGuid { get; set; }
    public Guid ObjectId { get; set; }
    public byte ObjectType { get; set; }

    public FlagListViewModel(FlagEntity entity)
    {
      Id = entity.Guid;
      FlagType = EnumExtensions.GetName<FlagType>(entity.ObjectType);
      Status = EnumExtensions.GetName<FlagStatus>(entity.Status);
      ReportedDate = entity.CreatedDate;
      FlaggedStudentGuid = entity.FlaggedUser;
      FlaggedByGuid = entity.CreatedBy;
      ObjectId = entity.ObjectId;
      ObjectType = entity.ObjectType;
    }
  }
}

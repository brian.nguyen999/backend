﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Admin.Flag
{
  public class FlagEditRequestDto
  {
    public Guid Id { get; set; }
    public byte Status { get; set; }
  }
}

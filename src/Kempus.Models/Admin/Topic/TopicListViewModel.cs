﻿using Kempus.Entities;

namespace Kempus.Models.Admin.Topic
{
  public class TopicListViewModel
  {
    public long Id { get; set; }
    public Guid Guid { get; set; }
    public string Name { get; set; }
    public bool IsActivated { get; set; }
    public DateTime CreatedDate { get; set; }
    public int NumberOfPost { get; set; }

    public TopicListViewModel(TopicEntity entity)
    {
      Id = entity.Id;
      Guid = entity.Guid;
      Name = entity.Name;
      IsActivated = entity.IsActivated;
      CreatedDate = entity.UpdatedDate ?? entity.CreatedDate;
    }
  }
}

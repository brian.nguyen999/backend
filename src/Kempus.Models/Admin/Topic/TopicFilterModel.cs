﻿namespace Kempus.Models.Admin.Topic
{
  public class TopicFilterModel
  {
    public Guid? SchoolId { get; set; }
    public Guid? TopicId { get; set; }
    public string? Keyword { get; set; }
  }
}

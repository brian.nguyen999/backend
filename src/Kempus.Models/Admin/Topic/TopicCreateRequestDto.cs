﻿namespace Kempus.Models.Admin.Topic
{
  public class TopicCreateRequestDto
  {
    public Guid SchoolId { get; set; }
    public string Name { get; set; }
    public bool IsActivated { get; set; }
  }
}

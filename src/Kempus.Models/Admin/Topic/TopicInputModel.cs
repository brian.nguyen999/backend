﻿namespace Kempus.Models.Admin.Topic
{
  public class TopicInputModel
  {
    public Guid? Id { get; set; }
    public Guid? SchoolId { get; set; }
    public Guid CreatorId { get; set; }
    public string? Name { get; set; }
    public bool IsActivated { get; set; }
  }
}

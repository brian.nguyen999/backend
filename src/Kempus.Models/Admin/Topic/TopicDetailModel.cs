﻿namespace Kempus.Models.Admin.Topic
{
  public class TopicDetailModel
  {
    public long Id { get; set; }
    public Guid Guid { get; set; }
    public string Name { get; set; }
    public bool IsActivated { get; set; }
  }
}

﻿namespace Kempus.Models.Admin.Topic
{
  public class TopicUpdateRequestDto
  {
    public Guid Id { get; set; }
    public bool IsActivated { get; set; }
  }
}

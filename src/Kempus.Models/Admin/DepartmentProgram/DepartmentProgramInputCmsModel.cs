﻿namespace Kempus.Models.Admin.DepartmentProgram
{
  public class DepartmentProgramInputCmsModel
  {
    public Guid? Id { get; set; }
    public Guid? SchoolId { get; set; }
    public Guid? CreatorId { get; set; }
    public Guid? UpdaterId { get; set; }
    public string Name { get; set; }
  }
}

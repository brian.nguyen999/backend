﻿namespace Kempus.Models.Admin.DepartmentProgram.Request
{
  public class DepartmentProgramUpdateRequestDto
  {
    public Guid Id { get; set; }
    public string Name { get; set; }
  }
}

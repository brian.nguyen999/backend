﻿namespace Kempus.Models.Admin.DepartmentProgram.Request
{
  public class DepartmentProgramCreateRequestDto
  {
    public Guid SchoolId { get; set; }
    public string Name { get; set; }
  }
}

﻿using Kempus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Admin.DepartmentProgram
{
  public class DepartmentProgramDetailCmsModel
  {
    public Guid Id { get; set; }
    public string Name { get; set; }
    public byte Status { get; set; }

    public DepartmentProgramDetailCmsModel(DepartmentProgramEntity entity)
    {
      Id = entity.Guid;
      Name = entity.Name;
      Status = entity.Status;
    }
  }
}

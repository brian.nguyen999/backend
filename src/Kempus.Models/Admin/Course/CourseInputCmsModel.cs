﻿namespace Kempus.Models.Admin.Course
{
  public class CourseInputCmsModel
  {
    public Guid? Id { get; set; }
    public string Name { get; set; }
    public Guid? SchoolId { get; set; }
    public Guid? CreatorId { get; set; }
    public Guid? UpdaterId { get; set; }
    public Guid DepartmentProgramId { get; set; }
    public List<Guid> InstructorIds { get; set; }
    public bool Status { get; set; }
  }
}

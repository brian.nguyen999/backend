﻿namespace Kempus.Models.Admin.Course.Request
{
  public class CourseUpdateRequestDto
  {
    public Guid Id { get; set; }
    public string Name { get; set; }
    public Guid DepartmentProgramId { get; set; }
    public List<Guid> InstructorIds { get; set; }
    public bool Status { get; set; }
  }
}

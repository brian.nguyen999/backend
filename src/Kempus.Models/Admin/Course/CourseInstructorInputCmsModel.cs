﻿namespace Kempus.Models.Admin.Course
{
  public class CourseInstructorInputCmsModel
  {
    public Guid CourseId { get; set; }
    public List<Guid> InstructorIds { get; set; }
    public CourseInstructorInputCmsModel(Guid courseId, List<Guid> instructorIds)
    {
      CourseId = courseId;
      InstructorIds = instructorIds;
    }
  }
}

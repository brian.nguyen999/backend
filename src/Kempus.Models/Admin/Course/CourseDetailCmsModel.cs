﻿using Kempus.Entities;
using Kempus.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Admin.Course
{
  public class CourseDetailCmsModel
  {
    public Guid Id { get; set; }
    public string Name { get; set; }
    public SelectModel Department { get; set; } = new SelectModel();
    public List<SelectModel> Instructors { get; set; } = new List<SelectModel>();
    public bool IsActivated { get; set; }

    public CourseDetailCmsModel(CourseEntity entity)
    {
      Id = entity.Guid;
      Name = entity.Name;
      IsActivated = entity.IsActivated;
      Department.Id = entity.DepartmentId.GetValueOrDefault();
    }
  }
}

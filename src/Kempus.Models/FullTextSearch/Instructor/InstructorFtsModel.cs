﻿namespace Kempus.Models.FullTextSearch.Instructor
{
  public class InstructorFtsModel
  {
    public Guid Id { get; set; }

    public string Name { get; set; }
  }
}

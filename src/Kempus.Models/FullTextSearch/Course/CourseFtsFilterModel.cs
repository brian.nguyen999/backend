﻿namespace Kempus.Models.FullTextSearch.Course
{
  public class CourseFtsFilterModel
  {
    public string? Keyword { get; set; }

    public Guid SchoolId { get; set; }
  }
}

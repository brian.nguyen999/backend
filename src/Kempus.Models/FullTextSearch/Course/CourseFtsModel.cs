﻿using Kempus.Models.FullTextSearch.Instructor;

namespace Kempus.Models.FullTextSearch.Course
{
  public class CourseFtsModel
  {
    public Guid Id { get; set; }

    public string Name { get; set; }

    public DateTime? CreatedDate { get; set; }

    public Guid SchoolId { get; set; }

    public List<InstructorFtsModel> Instructors { get; set; }

    public Guid? DepartmentId { get; set; }

    public string? DepartmentName { get; set; }
  }
}

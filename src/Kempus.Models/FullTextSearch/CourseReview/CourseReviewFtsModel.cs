﻿using Kempus.Models.FullTextSearch.Instructor;

namespace Kempus.Models.FullTextSearch.CourseReview
{
  public class CourseReviewFtsModel
  {
    public Guid Id { get; set; }

    public Guid? CourseId { get; set; }

    public string? CourseName { get; set; }

    public DateTime? CreatedDate { get; set; }

    public Guid SchoolId { get; set; }

    public List<InstructorFtsModel> Instructors { get; set; }

    public Guid? DepartmentId { get; set; }

    public string? DepartmentName { get; set; }

    public byte Status { get; set; }
  }
}

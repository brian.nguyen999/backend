﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.FullTextSearch.CourseReview
{
  public class CourseReviewFtsFilterModel
  {
    public string Keyword { get; set; }

    public Guid SchoolId { get; set; }

    public byte Status { get; set; }
  }
}

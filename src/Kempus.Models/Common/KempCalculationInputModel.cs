﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Common
{
  public class KempCalculationInputModel
  {
    public List<Guid> UserIds { get; set; } = new List<Guid>();
    public byte TransactionType { get; set; }
    public byte Action { get; set; }
    public Guid ReferenceGuid { get; set; }
    public int Amount { get; set; }
    public Guid CreatedBy { get; set; }
  }
}

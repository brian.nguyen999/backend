﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Common.WebSocket
{
  public class NotificationRequestDto
  {
    public string ConnectionId { get; set; }
    public string Action { get; set; }
    public string Payload { get; set; }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Common.WebSocket
{
  public class ConnectionRequestDto
  {
    public string RequestId { get; set; }
    public string Ip { get; set; }
    public string Caller { get; set; }
    public string User { get; set; }
    public DateTime RequestTime { get; set; }
    public string EventType { get; set; }
    public string RouteKey { get; set; }
    public int Status { get; set; }
    public string ConnectionId { get; set; }
    public string RequestAuthorization { get; set; }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Common.WebSocket
{
  public class WebSocketMessageModel
  {
    public WebSocketMessageModel(string action, object payload)
    {
      Action = action;
      Payload = payload;
    }

    public string Action { get; set; }
    public object Payload { get; set; }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Common.WebSocket
{
  public class NotificationInputModel
  {
    public List<string> ConnectionIds { get; set; }
    public string Action { get; set; }
    public string Payload { get; set; }
    public Guid? UserId { get; set; }
    public Guid? SchoolId { get; set; }
  }
}

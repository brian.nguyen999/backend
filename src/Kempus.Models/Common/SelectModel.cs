﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Common
{
  public class SelectModel
  {
    public Guid Id { get; set; }
    public string Text { get; set; }
  }
}

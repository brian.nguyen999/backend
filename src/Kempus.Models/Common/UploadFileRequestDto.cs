﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Common
{
  public class UploadFileRequestDto
  {
    [Required]
    public IFormFile File { get; set; }
    [Required]
    public Guid SchoolId { get; set; }
  }
}

﻿namespace Kempus.Models.Common
{
  public class OpenSearchEventMessage
  {
    public DateTime EventTime { get; set; } = DateTime.UtcNow;

    public string EventType { get; set; }

    public List<Guid> ObjectIds { get; set; }
  }
}

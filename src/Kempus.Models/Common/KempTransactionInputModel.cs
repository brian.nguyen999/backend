﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Common
{
  public class KempTransactionInputModel
  {
    public List<Guid> UserIds { get; set; }
    public byte TransactionType { get; set; }
    public Guid? ReferenceGuid { get; set; }
    public int Amount { get; set; }
    public byte Action { get; set; }
    public Guid CreatorId { get; set; }
    public bool IsAdminCreated { get; set; } = false;
    public Guid SchoolId { get; set; }
    public bool IsSelectAllUser { get; set; } = false;
  }
}

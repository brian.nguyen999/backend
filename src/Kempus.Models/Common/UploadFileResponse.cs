﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Common
{
  public class UploadFileResponse
  {
    public string FileName { get; set; }

    public string ContentType { get; set; }

    public string S3Url { get; set; }

    public long Size { get; set; }
  }
}

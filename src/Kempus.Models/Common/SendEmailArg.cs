﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Models.Common
{
  public class SendEmailArg
  {
    public List<EmailAddress> ToAddresses { get; set; } = new List<EmailAddress>();

    public string Subject { get; set; }

    public string Body { get; set; }
  }

  public class EmailAddress
  {
    public string Email { get; set; }

    public string Name { get; set; }
  }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Kempus.Core.Constants;
using Kempus.Core.Models;
using Microsoft.AspNetCore.Http;
using Moq;
using Xunit;

namespace Kempus.Core.Test
{
  public class SessionStoreTests
  {
    private readonly SessionStore _sessionStore;
    private readonly Mock<IHttpContextAccessor> _httpContextAccessorMock = new Mock<IHttpContextAccessor>();

    public SessionStoreTests()
    {
      _sessionStore = new SessionStore(_httpContextAccessorMock.Object);
    }

    [Fact]
    public async Task GetSessionValue()
    {
      // Arrange

      // Act
      var userId = _sessionStore.UserId;
      var schoolId = _sessionStore.SchoolId;

      // Assert
      Assert.Null(userId);
      Assert.Null(schoolId);
    }

    [Fact]
    public async Task CanSetTenantId()
    {
      // Arrange
      Guid? schoolId = Guid.NewGuid();

      // Act
      _sessionStore.SetTenant(schoolId);

      // Assert
      Assert.Equal(schoolId, _sessionStore.SchoolId);
    }

    [Fact]
    public async Task CanSetNullTenantId()
    {
      // Arrange
      Guid? schoolId = null;

      // Act
      _sessionStore.SetTenant(schoolId);

      // Assert
      Assert.Equal(schoolId, _sessionStore.SchoolId);
    }


    [Fact]
    public async Task CanSetUserId()
    {
      // Arrange
      Guid? userId = Guid.NewGuid();

      // Act
      _sessionStore.SetUserId(userId);

      // Assert
      Assert.Equal(userId, _sessionStore.UserId);
    }

    [Fact]
    public async Task CanSetNullUserId()
    {
      // Arrange
      Guid? userId = Guid.NewGuid();

      // Act
      _sessionStore.SetUserId(userId);

      // Assert
      Assert.Equal(userId, _sessionStore.UserId);
    }

    [Fact]
    public async Task CanClaimValueFromToken()
    {
      // Assert init value
      Assert.Null(_sessionStore.UserId);
      Assert.Null(_sessionStore.SchoolId);

      // Arrange
      Guid userId = Guid.NewGuid();
      Guid schoolId = Guid.NewGuid();
      var userClaims = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
      {
        new Claim(CoreConstants.Claims.SchoolId, schoolId.ToString()),
        new Claim(CoreConstants.Claims.UserId, userId.ToString()),
      }, "mock"));
      _httpContextAccessorMock.Setup(x => x.HttpContext)
        .Returns(new DefaultHttpContext()
        {
          User = userClaims
        });

      // Act
      var claimedUserId = _sessionStore.UserId;
      var claimSchoolId = _sessionStore.SchoolId;

      // Assert
      Assert.Equal(userId, claimedUserId);
      Assert.Equal(schoolId, claimSchoolId);
    }
  }
}
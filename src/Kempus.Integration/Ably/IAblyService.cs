﻿namespace Kempus.Integration.Ably
{
  public interface IAblyService
  {
    Task PublishMessageAsync(Guid channelId, Guid userId, object message);
  }
}

﻿using IO.Ably;
using IO.Ably.Encryption;
using Kempus.Core.Constants;
using Kempus.Core.Models.Configuration;

namespace Kempus.Integration.Ably
{
  public class AblyService : IAblyService
  {
    private readonly AblyConfig _config;
    public AblyService(
      AblyConfig config
    )
    {
      _config = config;
    }

    public async Task PublishMessageAsync(Guid channelId, Guid userId, object message)
    {
      var client = new ClientOptions(_config.ApiKey) { ClientId = userId.ToString() };
      AblyRealtime realtime = new AblyRealtime(client);
      var channel = realtime.Channels.Get(channelId.ToString());
      await channel.PublishAsync(ChatEventName.NEW_MESSAGE, message);
    }
  }
}

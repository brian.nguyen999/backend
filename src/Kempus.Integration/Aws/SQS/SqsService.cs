﻿using Amazon;
using Amazon.Runtime;
using Amazon.Runtime.Internal.Util;
using Amazon.SQS;
using Amazon.SQS.Model;
using Kempus.Core.Models.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Quartz.Logging;

namespace Kempus.Integration.Aws.SQS
{
  public class SqsService : ISqsService
  {
    private readonly IAmazonSQS _sqsClient;
    private readonly SqsConfig _sqsConfig;
    private readonly ILogger<SqsService> _logger;
    public SqsService(
      AwsConfig awsConfig, 
      ILogger<SqsService> logger
      )
    {
      _logger = logger;
      _sqsConfig = awsConfig.Sqs;
      _logger.LogInformation("AccessKey={0}, SecretKey={1},QueueUrl={2}",_sqsConfig.AccessKey, _sqsConfig.SecretKey,
        _sqsConfig.Queues.OpenSearchDataSyncQueueUrl);
      _sqsClient = new AmazonSQSClient(new BasicAWSCredentials(_sqsConfig.AccessKey, _sqsConfig.SecretKey),
        RegionEndpoint.GetBySystemName(_sqsConfig.Region));
    }

    public async Task<SendMessageResponse> SendMessageAsync(string queueUrl, object message)
    {
      var sendMessageRequest = new SendMessageRequest
      {
        MessageBody = JsonConvert.SerializeObject(message),
        QueueUrl = queueUrl,
      };

      var response = await _sqsClient.SendMessageAsync(sendMessageRequest);
      return response;
    }

    public async Task<ReceiveMessageResponse> GetMessagesAsync(string queueUrl, int numberOfMessage, int waitTime)
    {
      return await _sqsClient.ReceiveMessageAsync(new ReceiveMessageRequest
      {
        QueueUrl = queueUrl,
        MaxNumberOfMessages = numberOfMessage,
        WaitTimeSeconds = waitTime
      });
    }

    public async Task DeleteMessageAsync(string queueUrl, Message message)
    {
      await _sqsClient.DeleteMessageAsync(queueUrl, message.ReceiptHandle);
    }
  }
}

﻿using Amazon.SQS.Model;

namespace Kempus.Integration.Aws.SQS
{
  public interface ISqsService
    {
      Task<SendMessageResponse> SendMessageAsync(string queueUrl, object message);
      Task<ReceiveMessageResponse> GetMessagesAsync(string queueUrl, int numberOfMessage, int waitTime);
      Task DeleteMessageAsync(string queueUrl, Message message);
    }
}

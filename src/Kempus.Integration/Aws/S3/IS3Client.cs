﻿using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Integration.Aws.S3
{
  public interface IS3Client
  {
    string GeneratePreSignedUploadURL(string fileName);
    string GeneratePreSignedUploadURL(string fileName, string prefix);
    string GeneratePreSignedGetURL(string objectKey);
    string GeneratePreSignedGetURL(string bucket, string objectKey);
    string GetAbsoluteLink(string bucket, string objectKey);
    Task<PutObjectResponse> PutObjectToS3(Stream stream, string bucketName, string objectKey, string fileContent, bool isPublic);
    Task<PutObjectResponse> PutObjectToS3(string filePath, string objectKey, string contentType);
    Task<PutObjectResponse> PutBase64ContentToS3(string base64String, string bucketName, string objectKey, string fileContent, bool isPublic);
    Task<string> UploadFileToS3Async(string filePath, string objectKey);
    Task<GetObjectResponse> GetObject(string bucket, string objectKey);
    Task<ListObjectsResponse> GetListObject(string bucket, string objectKey);
    Task<long> GetObjectSize(string bucket, string objectKey);
    Task<List<string>> GetFilesFromFolder(string folderPath);
    Task DeleteObject(string bucket, string objectKey);
    Task DeleteObjects(string bucket, string objectKey);
    Task DeleteObjects(string bucket, IEnumerable<string> listObjectKeys);
    Task DeleteObjects(params string[] fileLinks);
  }
}

﻿using Kempus.Core.Models.Configuration;
using Kempus.Integration.Aws.SQS;
using Microsoft.Extensions.Logging;
using OpenSearch.Client;
using OpenSearch.Net;

namespace Kempus.Integration.Aws.OpenSearch
{
  public class OpenSearchService<T> : IFullTextSearchService<T> where T : class
  {
    private readonly OpenSearchClient _client;
    private readonly ILogger<OpenSearchService<T>> _logger;
    public OpenSearchService(
        OpenSearchConfig openSearch,
        ILogger<OpenSearchService<T>> logger
        )
    {
      _logger = logger;
      var openSearchConfig = openSearch;
      if (!openSearchConfig.Url.Contains("https://"))
      {
        openSearchConfig.Url = "https://" + openSearchConfig.Url;
      }

      _logger.LogInformation("Url={0}, UserName={1},Password={2}", openSearchConfig.Url,
        openSearchConfig.UserName, openSearchConfig.Password);
      var pool = new SingleNodeConnectionPool(
                new Uri(openSearchConfig.Url));
      var settings = new ConnectionSettings(pool)
          .BasicAuthentication(openSearchConfig.UserName, openSearchConfig.Password);
      _client = new OpenSearchClient(settings);
    }

    public async Task<List<T>> SearchDocumentAsync(Func<SearchDescriptor<T>, ISearchRequest> selector = null)
    {
      var response = await _client.SearchAsync<T>(selector);
      return response.Documents.ToList();
    }

    public async Task<long> CountDocumentAsync(Func<CountDescriptor<T>, ICountRequest> selector = null)
    {
      var response = await _client.CountAsync<T>(selector);
      return response.Count;
    }

    public async Task IndexDocumentAsync(IEnumerable<T> obj, string index)
    {
      await EnsureIndexCreatedAsync(index);
      var result = await _client.IndexManyAsync(obj, index);
      if (!result.IsValid || result.Errors) throw new Exception(result.DebugInformation);
    }

    public async Task EnsureIndexCreatedAsync(string index, Func<CreateIndexDescriptor, ICreateIndexRequest> indexDescription = null)
    {
      var isExisted = (await _client.Indices.ExistsAsync(index)).Exists;
      if (!isExisted)
      {
        var result = await _client.Indices.CreateAsync(index, indexDescription = null);
        if (!result.IsValid) throw new Exception(result.DebugInformation);
      }
    }

    public async Task DeleteIndexAsync(string index)
    {
      var result = await _client.Indices.DeleteAsync(index);
      if (!result.IsValid) throw new Exception(result.DebugInformation);
    }

    public async Task DeleteDocumentAsync(DocumentPath<T> id, Func<DeleteDescriptor<T>, IDeleteRequest> selector = null)
    {
      var result = await _client.DeleteAsync<T>(id, selector);
      if (!result.IsValid) throw new Exception(result.DebugInformation);
    }
  }
}

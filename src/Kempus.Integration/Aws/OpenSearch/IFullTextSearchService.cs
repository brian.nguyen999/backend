﻿using OpenSearch.Client;

namespace Kempus.Integration.Aws.OpenSearch
{
  public interface IFullTextSearchService<T> where T : class
  {
    Task<List<T>> SearchDocumentAsync(Func<SearchDescriptor<T>, ISearchRequest> selector = null);
    Task<long> CountDocumentAsync(Func<CountDescriptor<T>, ICountRequest> selector = null);
    Task IndexDocumentAsync(IEnumerable<T> obj, string index);
    Task EnsureIndexCreatedAsync(string index, Func<CreateIndexDescriptor, ICreateIndexRequest> indexDescription = null);
    Task DeleteDocumentAsync(DocumentPath<T> id, Func<DeleteDescriptor<T>, IDeleteRequest> selector = null);
    Task DeleteIndexAsync(string index);
  }
}

﻿using System.Net;
using System.Net.Mail;
using Kempus.Core.Models.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;

namespace Kempus.Integration.Aws.SES
{
  public class SESService : ISESService
  {
    private readonly SESConfig _sesConfig;
    private readonly ILogger _logger;
    private IHostingEnvironment _currentEnvironment { get; set; }

    public SESService(
      AwsConfig awsConfig,
      ILogger<SESService> logger,
      IHostingEnvironment currentEnvironment
    )
    {
      _sesConfig = awsConfig.SES;
      _logger = logger;
      _currentEnvironment = currentEnvironment;
    }

    /// <summary>
    /// Send single email
    /// </summary>
    /// <param name="to"></param>
    /// <param name="toName"></param>
    /// <param name="subject"></param>
    /// <param name="body"></param>
    /// <returns></returns>
    public async Task SendMailAsync(string to, string toName, string subject, string body)
    {
      // Create and build a new MailMessage object
      MailMessage message = new MailMessage();
      message.IsBodyHtml = true;
      message.From = new MailAddress(_sesConfig.MailFrom, _sesConfig.MailName);

      if (_currentEnvironment.IsDevelopment())
      {
        message.To.Add("kempus.test001@gmail.com");
      }
      else
      {
        message.To.Add(to);
      }

      message.Subject = subject;
      message.Body = body;
      // Comment or delete the next line if you are not using a configuration set
      //message.Headers.Add("X-SES-CONFIGURATION-SET", CONFIGSET);

      using (var client = new System.Net.Mail.SmtpClient(_sesConfig.Host, _sesConfig.Port))
      {
        // Pass SMTP credentials
        client.Credentials =
          new NetworkCredential(_sesConfig.SMTPUsername, _sesConfig.SMTPPassword);

        // Enable SSL encryption
        client.EnableSsl = true;

        // Try to send the message. Show status in console.
        try
        {
          _logger.LogInformation($"Attempting to send email {subject} to {to}");
          client.Send(message);
          _logger.LogInformation("Email sent!");
        }
        catch (Exception ex)
        {
          string error = $"Send email failed. (exception: {ex.Message}.";
          _logger.LogError(error);
        }
      }
    }
  }
}
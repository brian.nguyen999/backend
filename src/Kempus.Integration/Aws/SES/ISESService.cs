﻿namespace Kempus.Integration.Aws.SES
{
    public interface ISESService
    {
        Task SendMailAsync(string to, string toName, string subject, string body);
    }
}

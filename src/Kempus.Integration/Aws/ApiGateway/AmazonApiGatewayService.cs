﻿using Amazon.ApiGatewayManagementApi;
using Amazon.ApiGatewayManagementApi.Model;
using Kempus.Core.Models.Configuration;
using Kempus.Core.Utils;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Integration.Aws.ApiGateway
{
  public class AmazonApiGatewayService : IAmazonApiGatewayService
  {
    private static AmazonApiGatewayManagementApiClient _webSocketApiClient;
    private ApiGatewayConfig _apiGatewayConfig;
    private readonly ILogger _logger;

    public AmazonApiGatewayService(
      AwsConfig awsConfig,
      ILogger<AmazonApiGatewayService> logger
      )
    {
      _apiGatewayConfig = awsConfig.ApiGateway;
      _logger = logger;

      var apiConfig = new AmazonApiGatewayManagementApiConfig { ServiceURL = _apiGatewayConfig.WebSocketEndpoint };
      _webSocketApiClient = new AmazonApiGatewayManagementApiClient(_apiGatewayConfig.AccessKeyId, _apiGatewayConfig.SecretAccessKey, apiConfig);
    }

    public async Task PostToConnection(string connectionId, string action, object payload)
    {
      _logger.LogInformation($"PostToConnection: connectionId={connectionId}, action={action}");
      var json = JsonUtils.ToJson(new WebSocketMessageModel(action, payload));
      _logger.LogInformation($"PostToConnection: json={json}");

      try
      {
        var response = await _webSocketApiClient.PostToConnectionAsync(new PostToConnectionRequest
        {
          ConnectionId = connectionId,
          Data = new MemoryStream(Encoding.UTF8.GetBytes(json))
        });

        _logger.LogInformation($"PostToConnection: response={JsonUtils.ToJson(response)}");
      }
      catch(Exception ex )
      {
        _logger.LogError($"PostToConnection [Error]: {JsonUtils.ToJson(ex)}");
      }
    }
  }

  public class WebSocketMessageModel
  {
    public WebSocketMessageModel(string action, object payload)
    {
      Action = action;
      Payload = payload;
    }

    public string Action { get; set; }
    public object Payload { get; set; }
  }
}

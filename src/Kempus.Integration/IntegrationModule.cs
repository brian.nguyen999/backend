﻿using Kempus.Core.Models.Configuration;
using Kempus.Integration.Ably;
using Kempus.Integration.Aws.ApiGateway;
using Kempus.Integration.Aws.OpenSearch;
using Kempus.Integration.Aws.S3;
using Kempus.Integration.Aws.SES;
using Kempus.Integration.Aws.SQS;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Kempus.Core.Models.Configuration;

namespace Kempus.Integration
{
  public static class IntegrationModule
  {
    public static async void ConfigureServices(this IServiceCollection services, IConfiguration configuration)
    {
      // AwsConfig
      var awsConfig = configuration.GetSection(nameof(AwsConfig)).Get<AwsConfig>();
      services.AddSingleton(awsConfig);

      // SES
      services.AddScoped<ISESService, SESService>();

      // OpenSearch
      services.AddTransient(typeof(IFullTextSearchService<>), typeof(OpenSearchService<>));

      // Sqs
      services.AddTransient<ISqsService, SqsService>();

      // S3
      services.AddTransient<IS3Client, S3Client>();

      // ApiGateway
      services.AddTransient<IAmazonApiGatewayService, AmazonApiGatewayService>();

      // OpenSearch
      var openSearchConfig = configuration.GetSection("OpenSearch").Get<OpenSearchConfig>();
      services.AddSingleton(openSearchConfig);

      // AblyService
      services.AddTransient<IAblyService, AblyService>();
    }
  }
}
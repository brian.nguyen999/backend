﻿using Kempus.Core.Constants;
using Microsoft.AspNetCore.SignalR;

namespace Kempus.ApiGateway.HubConfig.Provider
{
  public class UserIdProvider : IUserIdProvider
  {
    public string GetUserId(HubConnectionContext connection)
    {
      return connection.User?.FindFirst(CoreConstants.Claims.UserId)?.Value;
    }
  }
}
﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Kempus.ApiGateway.HubConfig
{
  [Authorize]
  public class DataRealTimeUpdateHub : AbsRealTimeUpdateHub
  {
  }
}
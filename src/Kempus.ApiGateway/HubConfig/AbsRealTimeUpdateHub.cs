﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System.Text.RegularExpressions;

namespace Kempus.ApiGateway.HubConfig
{
  [Authorize]
  public abstract class AbsRealTimeUpdateHub : Hub
  {
    public async Task JoinGroup(int objectTypeId)
    {
      await Groups.AddToGroupAsync(Context.ConnectionId, objectTypeId.ToString());
    }

    public async Task LeaveGroup(int objectTypeId)
    {
      await Groups.RemoveFromGroupAsync(Context.ConnectionId, objectTypeId.ToString());
    }
  }
}
﻿using Kempus.Core.Constants;
using Kempus.Core.Models.Configuration;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Kempus.ApiGateway.Attributes
{
  public class KempusInternalApiAttribute : ActionFilterAttribute
  {
    public override void OnActionExecuting(ActionExecutingContext actionContext)
    {
      var internalApiKey = actionContext.HttpContext.Request
        .Headers[CoreConstants.Headers.KempusInternalApiKey].FirstOrDefault();
      if (!string.IsNullOrEmpty(internalApiKey))
      {
        var privateAppInfo = actionContext.HttpContext.RequestServices.GetService<PrivateAppInfo>();
        if (privateAppInfo.KempusInternalApiKey.Equals(internalApiKey)) return;
      }

      actionContext.Result = new NotFoundResult();
    }
  }
}
﻿using AutoWrapper.Wrappers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System.Net;
using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Newtonsoft.Json.Serialization;
using Kempus.Core.Utils;
using OpenSearch.Client;

namespace Kempus.ApiGateway.Middlewares
{
  public class ExceptionHandlingMiddleware
  {
    private readonly RequestDelegate _next;

    public ExceptionHandlingMiddleware(RequestDelegate next)
    {
      _next = next;
    }

    public async Task Invoke(HttpContext context, ILogger<ExceptionHandlingMiddleware> logger /* other dependencies */)
    {
      try
      {
        await _next(context);
      }
      catch (Exception ex)
      {
        await HandleExceptionAsync(context, ex, logger);
      }
    }

    private async Task HandleExceptionAsync(HttpContext context, Exception ex, ILogger logger)
    {
      logger.LogError(JsonUtils.ToJson(ex));
      var code = (int)HttpStatusCode.InternalServerError; // 500 if unexpected
      if (ex is NotFoundException) code = (int)HttpStatusCode.NotFound;
      else if (ex is BadRequestException) code = (int)HttpStatusCode.BadRequest;
      else if (ex is ForbiddenException) code = (int)HttpStatusCode.Forbidden;
      else if (ex is UnAuthorizeException) code = (int)HttpStatusCode.Unauthorized;
      else if (ex is UnprocessableEntityException)
      {
        throw ex;
      }

      throw new ApiException(ex.Message, code);
    }
  }
}
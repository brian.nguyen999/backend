﻿using Kempus.Core.Errors;
using Kempus.Services.Public.User;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Text;

namespace Kempus.ApiGateway.Middlewares
{
  public class ExpiredTokenMiddleware
  {
    private readonly RequestDelegate _next;

    public ExpiredTokenMiddleware(RequestDelegate next)
    {
      _next = next;
    }

    public async Task Invoke(HttpContext context)
    {
      if (context.Response.Headers["Token-Expired"] == "True")
      {
        context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
        await context.Response.WriteAsync(ErrorMessages.User_TokenExpired);
        // DO NOT CALL NEXT. THIS SHORTCIRCUITS THE PIPELINE
      }
      else
      {
        await _next(context);
      }
    }
  }
}
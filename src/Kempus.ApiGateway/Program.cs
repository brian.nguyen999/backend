//using Kempus.Job.Jobs;
using AutoWrapper;
using Kempus.ApiGateway.Authorization.Handlers;
using Kempus.ApiGateway.Authorization.Requirements;
using Kempus.ApiGateway.Authorization.Transformer;
using Kempus.ApiGateway.Helpers;
using Kempus.ApiGateway.HubConfig.Provider;
using Kempus.ApiGateway.Middlewares;
using Kempus.Core.Constants;
using Kempus.Core.Extensions;
using Kempus.Core.Models.Configuration;
using Kempus.EntityFramework;
using Kempus.Integration;
using Kempus.Job;
using Kempus.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Serialization;
using Serilog;
using StackExchange.Redis;
using System.Net.Security;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;

const string _apiVersion = "v1";
const string SIGNALR_HUB_PREFIX = "/hub-signalR";
const string _kempusAllowSpecificOrigins = "KempusAllowSpecificOrigins";

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddCors(options =>
{
  options.AddPolicy(name: _kempusAllowSpecificOrigins,
    policy => { policy.WithOrigins("*").AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader(); });
});

builder.Services.AddLazyResolution();

// Config overload appsetting
IWebHostEnvironment environment = builder.Environment;
builder.Configuration.AddJsonFile("appsettings.json", optional: true, reloadOnChange: false);
builder.Configuration.AddJsonFile($"appsettings.{environment.EnvironmentName}.json", optional: true,
  reloadOnChange: true);
builder.Configuration.AddJsonFile("appsettings.Override.json", optional: true, reloadOnChange: true);
builder.Configuration.AddEnvironmentVariables();

builder.Services.AddSignalR();
builder.Services.AddSingleton<IUserIdProvider, UserIdProvider>();

builder.Services.AddAutoMapper(typeof(MapHelper));

var redisConfiguration = builder.Configuration.GetSection(nameof(JwtConfiguration)).Get<JwtConfiguration>();
var configurationOptions = new ConfigurationOptions
{
  AbortOnConnectFail = false,
  Password = builder.Configuration["AwsConfig:Redis:Password"],
  EndPoints = { $"{builder.Configuration["AwsConfig:Redis:Endpoint"]}:{builder.Configuration["AwsConfig:Redis:Port"]}" },
  Ssl = true
};
configurationOptions.CertificateValidation += ValidateServerCertificate;

builder.Services.AddStackExchangeRedisCache(options =>
{
  options.ConfigurationOptions = configurationOptions;
});
var multiplexer = ConnectionMultiplexer.Connect(configurationOptions);
builder.Services.AddSingleton<IConnectionMultiplexer>(multiplexer);

// Config SeriLog
builder.Logging.ClearProviders();
builder.Logging.AddSerilog(new LoggerConfiguration()
  .ReadFrom.Configuration(builder.Configuration)
  .CreateLogger());

var logger = builder.Logging.Services.BuildServiceProvider().GetRequiredService<ILogger<Program>>();

// Add services to the container
builder.Services.AddScoped<IAuthorizationHandler, AdminPolicyHandler>();
builder.Services.AddScoped<IAuthorizationHandler, UserPolicyHandler>();
EntityFrameworkModule.ConfigureServices(builder.Services, builder.Configuration, logger);
IntegrationModule.ConfigureServices(builder.Services, builder.Configuration);
ServiceModule.ConfigureServices(builder.Services, builder.Configuration);
JobModule.ConfigureServices(builder.Services, builder.Configuration);

builder.Services.AddSingleton(builder.Configuration.GetSection(nameof(PrivateAppInfo)).Get<PrivateAppInfo>());

var jwtConfiguration = builder.Configuration.GetSection(nameof(JwtConfiguration)).Get<JwtConfiguration>();
builder.Services.AddSingleton(jwtConfiguration);

builder.Services.AddAuthentication(options =>
  {
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
  })
  .AddJwtBearer(options =>
  {
    options.RequireHttpsMetadata = false;
    options.SaveToken = true;
    options.TokenValidationParameters = new TokenValidationParameters
    {
      ValidateLifetime = true,
      ValidateIssuerSigningKey = true,
      IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(jwtConfiguration.Secret)),
      ValidateIssuer = false,
      ValidateAudience = false,
      ClockSkew = TimeSpan.FromSeconds(0)
    };
    options.Events = new JwtBearerEvents
    {
      OnMessageReceived = context =>
      {
        var accessToken = context.Request.Query["access_token"];
        if (!string.IsNullOrEmpty(accessToken) &&
            context.HttpContext.Request.Path.StartsWithSegments(SIGNALR_HUB_PREFIX))
        {
          context.Token = accessToken;
        }

        return Task.CompletedTask;
      },
      OnAuthenticationFailed = context =>
      {
        if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
        {
          context.Response.Headers.Add("Token-Expired", "True");
        }

        return Task.CompletedTask;
      },
    };
  });

builder.Services
  .AddControllers(options => { options.Filters.Add<ValidationFilterAttribute>(); })
  .AddNewtonsoftJson(options =>
  {
    options.SerializerSettings.ContractResolver = new DefaultContractResolver
    {
      SerializeCompilerGeneratedMembers = true,
      NamingStrategy = new CamelCaseNamingStrategy()
    };
  });

builder.Services.AddAuthorization(options =>
  {
    options.AddPolicy(CoreConstants.PolicyName.AdminPolicy, policy =>
      policy.Requirements.Add(new AdminPolicyRequirement()));
    options.AddPolicy(CoreConstants.PolicyName.UserPolicy, policy =>
      policy.Requirements.Add(new UserPolicyRequirement()));
  })
  .AddSingleton<IAuthorizationMiddlewareResultHandler, AuthorizationResultTransformer>();
;

builder.Services.Configure<ApiBehaviorOptions>(options
  => options.SuppressModelStateInvalidFilter = true);
builder.Services.AddScoped<ValidationFilterAttribute>();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

// Add Xml Comment
var hostXmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
var hostXmlPath = Path.Combine(AppContext.BaseDirectory, hostXmlFile);

builder.Services.AddSwaggerGen(
  c =>
  {
    c.SwaggerDoc(name: CoreConstants.SwaggerGroupApi.AdminApi,
      new OpenApiInfo { Title = "Admin API", Version = _apiVersion });
    c.SwaggerDoc(name: CoreConstants.SwaggerGroupApi.PublicApi,
      new OpenApiInfo { Title = "Public API", Version = _apiVersion });
    c.IncludeXmlComments(hostXmlPath);

    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
      Description = "JWT Authorization header using the Bearer scheme (Example: 'Bearer 12345abcdef')",
      Name = "Authorization",
      In = ParameterLocation.Header,
      Type = SecuritySchemeType.ApiKey,
      Scheme = "Bearer"
    });
    c.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
      {
        new OpenApiSecurityScheme
        {
          Reference = new OpenApiReference
          {
            Type = ReferenceType.SecurityScheme,
            Id = "Bearer"
          }
        },
        Array.Empty<string>()
      }
    });
  });


// Add default Token Life span
builder.Services.Configure<DataProtectionTokenProviderOptions>(o =>
  o.TokenLifespan = TimeSpan.FromMinutes(builder.Configuration.GetValue<int>("DefaultTokenLifeSpanInMinutes")));

var app = builder.Build();

// Multiple database migrations
EntityFrameworkModule.UpdateDatabase(app, typeof(MasterDbContext), logger, builder.Configuration);
EntityFrameworkModule.UpdateDatabase(app, typeof(ReplicaDbContext), logger, builder.Configuration);

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
  // Enable middleware to serve generated Swagger as a JSON endpoint
  app.UseSwagger();
  app.UseSwaggerUI(options =>
  {
    options.SwaggerEndpoint(url: "/swagger/" + CoreConstants.SwaggerGroupApi.PublicApi + "/swagger.json",
      name: $"Public APIs {_apiVersion}");
    options.SwaggerEndpoint(url: "/swagger/" + CoreConstants.SwaggerGroupApi.AdminApi + "/swagger.json",
      name: $"Admin APIs {_apiVersion}");
    options.DisplayRequestDuration(); // Controls the display of the request duration (in milliseconds) for "Try it out" requests.  
  });
}


app.UseApiResponseAndExceptionWrapper();

app.UseHttpsRedirection();

app.UseCors(_kempusAllowSpecificOrigins);

app.UseAuthentication();
app.UseMiddleware<ExpiredTokenMiddleware>();
app.UseAuthorization();

app.MapControllers();


app.UseMiddleware<ExceptionHandlingMiddleware>(); // Must be at the last position

app.Run();

static bool ValidateServerCertificate(
        object sender,
        X509Certificate certificate,
        X509Chain chain,
        SslPolicyErrors sslPolicyErrors)
{
  if (sslPolicyErrors == SslPolicyErrors.None)
    return true;

  Console.WriteLine("Certificate error: {0}", sslPolicyErrors);

  return false;
}
﻿using Kempus.ApiGateway.Authorization.Requirements;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Policy;
using System.Net;

namespace Kempus.ApiGateway.Authorization.Transformer
{
  public class AuthorizationResultTransformer : IAuthorizationMiddlewareResultHandler
  {
    private readonly IAuthorizationMiddlewareResultHandler _handler;

    public AuthorizationResultTransformer()
    {
      _handler = new AuthorizationMiddlewareResultHandler();
    }

    public async Task HandleAsync(
      RequestDelegate requestDelegate,
      HttpContext httpContext,
      AuthorizationPolicy authorizationPolicy,
      PolicyAuthorizationResult policyAuthorizationResult)
    {
      if (policyAuthorizationResult.Forbidden && policyAuthorizationResult.AuthorizationFailure != null)
      {
        if (policyAuthorizationResult.AuthorizationFailure.FailedRequirements.Any(requirement =>
              requirement is AdminPolicyRequirement))
        {
          httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
          return;
        }

        if (policyAuthorizationResult.AuthorizationFailure.FailedRequirements.Any(requirement =>
              requirement is UserPolicyRequirement))
        {
          httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
          return;
        }

        // Other transformations here
      }

      await _handler.HandleAsync(requestDelegate, httpContext, authorizationPolicy, policyAuthorizationResult);
    }
  }
}
﻿using Kempus.ApiGateway.Authorization.Requirements;
using Kempus.Core.Constants;
using Kempus.Services.Common.HttpsContext;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.ApiGateway.Authorization.Handlers
{
  public class UserPolicyHandler : AuthorizationHandler<UserPolicyRequirement>
  {
    private IHttpContextAccessor _httpContextAccessor;

    public UserPolicyHandler(IHttpContextAccessor httpContextAccessor)
    {
      _httpContextAccessor = httpContextAccessor;
    }

    protected override Task HandleRequirementAsync(
      AuthorizationHandlerContext context, UserPolicyRequirement requirement)
    {
      try
      {
        var token = _httpContextAccessor.HttpContext.GetTokenAsync("access_token").Result;

        var userTypeClaim = context.User.FindFirst(
          c => c.Type == CoreConstants.Claims.UserType);

        if (userTypeClaim != null)
        {
          var userType = Convert.ToByte(userTypeClaim.Value);
          if (userType == (byte)UserType.User || userType == (byte)UserType.Ambassador)
          {
            context.Succeed(requirement);
          }
        }

        return Task.CompletedTask;
      }
      catch (Exception ex)
      {
        return Task.CompletedTask;
      }
    }
  }
}
﻿using Kempus.ApiGateway.Authorization.Requirements;
using Kempus.Core.Constants;
using Kempus.Services.Common.HttpsContext;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.ApiGateway.Authorization.Handlers
{
  public class AdminPolicyHandler : AuthorizationHandler<AdminPolicyRequirement>
  {
    private IHttpContextAccessor _httpContextAccessor;

    public AdminPolicyHandler(IHttpContextAccessor httpContextAccessor)
    {
      _httpContextAccessor = httpContextAccessor;
    }

    protected override Task HandleRequirementAsync(
      AuthorizationHandlerContext context, AdminPolicyRequirement requirement)
    {
      try
      {
        var httpContext = _httpContextAccessor.HttpContext;
        var token = _httpContextAccessor.HttpContext.GetTokenAsync("access_token").Result;

        var userTypeClaim = context.User.FindFirst(
          c => c.Type == CoreConstants.Claims.UserType);

        if (userTypeClaim != null)
        {
          var userType = Convert.ToByte(userTypeClaim.Value);
          if (userType == (byte)UserType.Admin)
          {
            context.Succeed(requirement);
          }
        }

        return Task.CompletedTask;
      }
      catch (Exception ex)
      {
        return Task.CompletedTask;
      }
    }
  }
}
﻿using AutoMapper;
using Kempus.Entities;
using Kempus.Models.Public.User.Request;

namespace Kempus.ApiGateway.Helpers
{
  public class MapHelper : Profile
  {
    public MapHelper()
    {
      CreateMap<RegisterRequestDto, ApplicationUser>();
    }
  }
}
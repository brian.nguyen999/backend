﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using AutoWrapper.Wrappers;
using AutoWrapper.Extensions;
using Kempus.Core.Errors;

namespace Kempus.ApiGateway.Helpers
{
  public class ValidationFilterAttribute : IActionFilter
  {
    public void OnActionExecuting(ActionExecutingContext context)
    {
      if (!context.ModelState.IsValid)
      {
        throw new UnprocessableEntityException(context.ModelState.AllErrors());
      }
    }

    public void OnActionExecuted(ActionExecutedContext context)
    {
    }
  }
}
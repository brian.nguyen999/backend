﻿using Kempus.ApiGateway.Attributes;
using Kempus.ApiGateway.HubConfig;
using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Models.SignalR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel.DataAnnotations;

namespace Kempus.ApiGateway.Controllers
{
  /// <summary>
  /// SignalRHandlerController
  /// </summary>
  [Route("api/SignalR/[controller]")]
  [ApiController]
  [KempusInternalApiAttribute]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.PublicApi)]
  public class SignalRHandlerController
  {
    private readonly IHubContext<DataRealTimeUpdateHub> _dataRealTimeUpdateHub;

    /// <summary>
    /// SignalRHandlerController
    /// </summary>
    /// <param name="dataRealTimeUpdateHub"></param>
    public SignalRHandlerController(IHubContext<DataRealTimeUpdateHub> dataRealTimeUpdateHub)
    {
      _dataRealTimeUpdateHub = dataRealTimeUpdateHub;
    }

    /// <summary>
    /// SendAll
    /// </summary>
    /// <param name="hubName"></param>
    /// <param name="methodName"></param>
    /// <param name="list"></param>
    [HttpPost("all")]
    [SwaggerOperation(Summary = "Send to all in a hub")]
    public void SendAll(
      [FromQuery(Name = "hubName")] [Required]
      string hubName,
      [FromQuery(Name = "methodName")] [Required]
      string methodName,
      [FromBody] List<object> list)
    {
      if (list == null || !list.Any()) return;
      foreach (var item in list)
      {
        GetIHubClients(hubName).All.SendAsync(methodName, item).Wait();
      }
    }

    /// <summary>
    /// SendGroupWithDataList
    /// </summary>
    /// <param name="hubName"></param>
    /// <param name="groupName"></param>
    /// <param name="methodName"></param>
    /// <param name="list"></param>
    [HttpPost("group")]
    [SwaggerOperation(Summary = "Send signalR to a group")]
    public void SendGroupWithDataList(
      [FromQuery(Name = "hubName")] [Required]
      string hubName,
      [FromQuery(Name = "groupName")] [Required]
      string groupName,
      [FromQuery(Name = "methodName")] [Required]
      string methodName,
      [FromBody] List<object> list)
    {
      if (list == null || !list.Any()) return;
      foreach (var item in list)
      {
        GetIHubClients(hubName).Groups(groupName).SendAsync(methodName, item).Wait();
      }
    }

    /// <summary>
    /// SendUsersWithSameData
    /// </summary>
    /// <param name="hubName"></param>
    /// <param name="methodName"></param>
    /// <param name="requestBody"></param>
    [HttpPost("users")]
    [SwaggerOperation(Summary = "Send signalR to multiple users")]
    public void SendUsersWithSameData(
      [FromQuery(Name = "hubName")] [Required]
      string hubName,
      [FromQuery(Name = "methodName")] [Required]
      string methodName,
      [FromBody] SignalrSendUsersModel requestBody)
    {
      if (requestBody?.UserIds == null || !requestBody.UserIds.Any()) return;
      var userIds = requestBody.UserIds.Select(x => x.ToString()).ToList();
      GetIHubClients(hubName).Users(userIds).SendAsync(methodName, requestBody.Data).Wait();
    }

    /// <summary>
    /// SendUsersWithDifferentDataForEachUser
    /// </summary>
    /// <param name="hubName"></param>
    /// <param name="methodName"></param>
    /// <param name="list"></param>
    [HttpPost("users-different-data")]
    [SwaggerOperation(Summary = "Send signalR to multiple users")]
    public void SendUsersWithDifferentDataForEachUser(
      [FromQuery(Name = "hubName")] [Required]
      string hubName,
      [FromQuery(Name = "methodName")] [Required]
      string methodName,
      [FromBody] List<SignalrSendUserModel> list)
    {
      if (list == null || !list.Any()) return;
      var hubClients = GetIHubClients(hubName);
      foreach (var item in list)
      {
        hubClients.User(item.UserId.ToString()).SendAsync(methodName, item.Data).Wait();
      }
    }

    private IHubClients GetIHubClients(string hubName)
    {
      if (hubName.Equals(nameof(DataRealTimeUpdateHub))) return _dataRealTimeUpdateHub.Clients;
      throw new BadRequestException("Can not get " + hubName);
    }
  }
}
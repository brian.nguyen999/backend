﻿using Kempus.Core.Constants;
using Kempus.Models.Public.Tracking;
using Kempus.Services.Common.HttpsContext;
using Kempus.Services.Public.UserTracking;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Kempus.ApiGateway.Controllers
{  /// <summary>
   /// PollController
   /// </summary>
  [Route("api/tracking")]
  [ApiController]
  [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.PublicApi)]
  public class TrackingController
  {
    private readonly IUserTrackingService _userTrackingService;
    private readonly IHttpContextService _httpContextService;
    /// <summary>
    /// TrackingController
    /// </summary>
    /// <param name="userTrackingService"></param>
    /// <param name="httpContextService"></param>
    public TrackingController(
      IUserTrackingService userTrackingService,
      IHttpContextService httpContextService
      )
    {
      _userTrackingService = userTrackingService;
      _httpContextService = httpContextService;
    }

    /// <summary>
    /// TrackingUserActivity
    /// </summary>
    /// <param name="model"></param>
    [AllowAnonymous]
    [HttpPost("user-activity")]
    public void TrackingUserActivity([FromBody] UserTrackingInputModel model)
    {
      _userTrackingService.Create(model);
    }

    /// <summary>
    /// TrackingView
    /// </summary>
    /// <param name="model"></param>
    [HttpPost("view")]
    public void TrackingView([FromBody] ViewTrackingInputModel model)
    {
      model.UserId = _httpContextService.GetCurrentUserId();
      _userTrackingService.Create(model);
    }
  }
}

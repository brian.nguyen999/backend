﻿using Kempus.Core.Constants;
using Kempus.Core.Models;
using Kempus.Entities;
using Kempus.Models.Public.Degree;
using Kempus.Services.Public.Degree;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Kempus.ApiGateway.Controllers
{  /// <summary>
   /// MajorController
   /// </summary>
  [Route("api/degrees")]
  [ApiController]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.PublicApi)]
  public class DegreeController : ControllerBase
  {
    private readonly IDegreeService _degreeService;

    /// <summary>
    /// DegreeController
    /// </summary>
    /// <param name="degreeService"></param>
    public DegreeController(
      IDegreeService degreeService
      )
    {
      _degreeService = degreeService;
    }

    /// <summary>
    /// Search
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="sortedField"></param>
    /// <param name="isDescending"></param>
    /// <param name="filterModel"></param>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpPost("search")]
    public object Search(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      [FromBody] DegreeFilterModel filterModel)
    {

      isDescending = sortedField != null ? isDescending : false;
      sortedField = sortedField ?? nameof(DegreeEntity.Name);
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      filterModel.IsActivated = true;
      return _degreeService.Search(filterModel, pagingModel, useMasterDb: true);
    }
  }
}

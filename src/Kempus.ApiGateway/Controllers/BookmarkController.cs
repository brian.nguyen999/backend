﻿using Kempus.Core.Constants;
using Kempus.Core.Enums;
using Kempus.Core.Models;
using Kempus.Models.Public.Bookmark;
using Kempus.Models.Public.Bookmark.Request;
using Kempus.Services.Common.HttpsContext;
using Kempus.Services.Public.Bookmark;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Kempus.ApiGateway.Controllers
{
  /// <summary>
  /// BookmarkController
  /// </summary>
  [Route("api/bookmarks")]
  [ApiController]
  [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.PublicApi)]
  public class BookmarkController : ControllerBase
  {
    private readonly IBookmarkService _bookmarkService;
    private readonly IHttpContextService _httpContextService;

    /// <summary>
    /// BookmarkController
    /// </summary>
    /// <param name="bookmarkService"></param>
    /// <param name="httpContextService"></param>
    public BookmarkController(
      IBookmarkService bookmarkService,
      IHttpContextService httpContextService)
    {
      _bookmarkService = bookmarkService;
      _httpContextService = httpContextService;
    }

    /// <summary>
    /// Search bookmark
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="sortedField"></param>
    /// <param name="isDescending"></param>
    /// <returns></returns>
    [HttpPost("search")]
    public object Search(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending)
    {
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);

      var filterModel = new BookmarkFilterModel
      {
        UserId = _httpContextService.GetCurrentUserId()
      };

      return _bookmarkService.Search(filterModel, pagingModel, true);
    }

    /// <summary>
    /// Create book mark
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public object Create(
      [FromBody] CreateBookMarkRequestDto model
      )
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      var inputModel = new BookmarkInputModel
      {
        UserId = currentUserId,
        ObjectId = model.ObjectId,
        ObjectType = (CoreEnums.BookmarkObjectType)(model.ObjectType)
      };

      return _bookmarkService.CreateIfNotExist(inputModel);
    }

    /// <summary>
    /// Delete bookmark
    /// </summary>
    /// <returns></returns>
    [HttpDelete]
    public void Delete(
      [FromBody] DeleteBookmarkRequestDto model
      )
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      _bookmarkService.Delete(model.ObjectId, (CoreEnums.BookmarkObjectType)(model.ObjectType), currentUserId);
    }
  }
}

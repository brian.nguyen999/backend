﻿using Kempus.Core.Constants;
using Kempus.Core.Enums;
using Kempus.Core.Models;
using Kempus.Models.Public.CourseReviewActivity;
using Kempus.Models.Public.PostPollActivitySummary;
using Kempus.Services.Common.HttpsContext;
using Kempus.Services.Public.Trending;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Kempus.ApiGateway.Controllers
{
  /// <summary>
  /// TrendingController
  /// </summary>
  [Route("api/trending")]
  [ApiController]
  [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.PublicApi)]
  public class TrendingController : ControllerBase
  {
    private readonly IHttpContextService _httpContextService;
    private readonly ITrendingService _trendingService;
    /// <summary>
    /// PollController
    /// </summary>
    /// <param name="httpContextService"></param>
    public TrendingController(
      IHttpContextService httpContextService,
      ITrendingService trendingService
      )
    {
      _httpContextService = httpContextService;
      _trendingService = trendingService;
    }

    /// <summary>
    /// Get Trending Course Review
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("course-review")]
    public object GetListCourseReviewTrending(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending
    )
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      var currentSchoolId = _httpContextService.GetCurrentSchoolId();
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      var filterModel = new CourseReviewActivitySummaryFilterModel()
      {
        SchoolId = currentSchoolId,
        Status = (byte)CoreEnums.CourseReviewStatus.Published
      };

      return _trendingService.GetTrendingCourseReview(filterModel, pagingModel, false, currentUserId);
    }

    /// <summary>
    /// Get Trending Post and Poll
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("post-poll")]
    public object GetListPostPollTrending(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending
    )
    {
      var currentSchoolId = _httpContextService.GetCurrentSchoolId();
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      var filterModel = new PostPollActivitySummaryFilterModel()
      {
        SchoolId = currentSchoolId,
        IsPublished = true
      };

      if (sortedField == "MostLiked")
      {
        var commentPagingModel = Pageable.GetPageable(pageSize, pageIndex, "MostComment", isDescending);
        var trendingComment = _trendingService.GetTrendingPostPoll(filterModel, commentPagingModel, false);
        filterModel.ExcludedIds = trendingComment?.Data?.Select(x => x.ObjectId)?.ToList();
      }

      return _trendingService.GetTrendingPostPoll(filterModel, pagingModel, false); ;
    }
  }
}
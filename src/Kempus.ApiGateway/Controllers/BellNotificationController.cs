﻿using Kempus.Core.Constants;
using Kempus.Core.Models;
using Kempus.Models.Public.BellNotification;
using Kempus.Services.Common.HttpsContext;
using Kempus.Services.Public.BellNotification;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Kempus.ApiGateway.Controllers
{
  /// <summary>
  /// Bell Notification Controller
  /// </summary>
  [Route("api/bell-notification")]
  [ApiController]
  [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.PublicApi)]
  public class BellNotificationController
  {
    private readonly IHttpContextService _httpContextService;
    private readonly IBellNotificationService _bellNotificationService;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="bellNotificationService"></param>
    /// <param name="httpContextService"></param>
    public BellNotificationController(
      IHttpContextService httpContextService,
      IBellNotificationService bellNotificationService
      )
    {
      _httpContextService = httpContextService;
      _bellNotificationService = bellNotificationService;
    }

    /// <summary>
    /// Search BellNotification
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="sortedField"></param>
    /// <param name="isDescending"></param>
    /// <param name="filterModel"></param>
    /// <returns></returns>
    [HttpPost("search")]
    public object Search(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      [FromBody] BellNotificationFilterModel filterModel)
    {
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      return _bellNotificationService.Search(filterModel, _httpContextService.GetCurrentUserId(), pagingModel, false);
    }

    /// <summary>
    /// Mark as read
    /// </summary>
    /// <param name="bellNotificationUserId"></param>
    [HttpPost("mark-as-read")]
    public void MarkAsRead([Required] Guid bellNotificationUserId)
    {
      _bellNotificationService.MarkAsRead(bellNotificationUserId);
    }

    /// <summary>
    /// Mark all as read
    /// </summary>
    [HttpPost("mark-all-as-read")]
    public void MarkAllAsRead()
    {
      _bellNotificationService.MarkAllAsRead(_httpContextService.GetCurrentUserId());
    }

    /// <summary>
    /// Update last time click on the Belll
    /// </summary>
    [HttpPost("update-last-view")]
    public void UpdateLastViewBellNotification()
    {
      _bellNotificationService.UpdateLastViewTime(_httpContextService.GetCurrentUserId());
    }

    /// <summary>
    /// Check new notification from last view
    /// </summary>
    /// <returns></returns>
    [HttpPost("check-new-notification-from-last-view")]
    public object CheckNewNotificationFromLastView()
    {
      return _bellNotificationService.CheckNewNotificationFromLastView(_httpContextService.GetCurrentUserId());
    }
  }
}

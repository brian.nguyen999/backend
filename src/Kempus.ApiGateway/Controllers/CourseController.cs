﻿using Kempus.Core.Constants;
using Kempus.Core.Models;
using Kempus.Models.Public.Course;
using Kempus.Services.Common.HttpsContext;
using Kempus.Services.Public.Course;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Kempus.ApiGateway.Controllers
{
  /// <summary>
  /// Course Controller
  /// </summary>
  [Route("api/courses")]
  [ApiController]
  [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.PublicApi)]
  public class CourseController
  {
    private readonly ICourseService _courseService;
    private readonly IHttpContextService _httpContextService;

    /// <summary>
    /// CourseController
    /// </summary>
    /// <param name="courseService"></param>
    /// <param name="httpContextService"></param>
    public CourseController(
      ICourseService courseService,
      IHttpContextService httpContextService)
    {
      _courseService = courseService;
      _httpContextService = httpContextService;
    }

    /// <summary>
    /// Search Course
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="sortedField"></param>
    /// <param name="isDescending"></param>
    /// <param name="filterModel"></param>
    /// <returns></returns>
    [HttpPost("search")]
    public object Search(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      [FromBody] CourseFilterModel filterModel)
    {
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      var currentSchoolId = _httpContextService.GetCurrentSchoolId();
      filterModel.SchoolId = currentSchoolId;
      return _courseService.Search(filterModel, pagingModel, useMasterDb: true, useFullTextSearch: true);
    }

    /// <summary>
    /// Get course detail
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public object GetDetail(Guid id)
    {
      return _courseService.GetDetail(id);
    }
  }
}
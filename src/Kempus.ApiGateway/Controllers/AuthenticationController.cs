﻿using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Core.Extensions;
using Kempus.Models.Authentication.Response;
using Kempus.Models.Public.Authentication;
using Kempus.Models.Public.Authentication.Request;
using Kempus.Services.Common.HttpsContext;
using Kempus.Services.Public.Authentication;
using Kempus.Services.Public.User;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Specialized;
using System.Net.Http;
using System.Net.Http.Headers;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.ApiGateway.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  [Authorize(Policy = CoreConstants.PolicyName.UserPolicy)]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.PublicApi)]
  public class AuthenticationController : ControllerBase
  {
    private readonly IHttpContextService _httpContextService;
    private readonly IAuthenticationService _authenticationService;
    private readonly IUserService _userService;

    public AuthenticationController(
      IHttpContextService httpContextService, IAuthenticationService authenticationService, IUserService userService)
    {
      _httpContextService = httpContextService;
      _authenticationService = authenticationService;
      _userService = userService;
    }

    [AllowAnonymous]
    [HttpPost("login")]
    [SwaggerOperation(Summary = "Login")]
    public async Task<IActionResult> Login([FromBody] LoginRequestDto request)
    {
      var input = new LoginInputModel()
      {
        Username = request.Username,
        Password = request.Password,
      };

      // Check User login
      var loginSuccess = await _userService.SigninAsync(input);
      if (!loginSuccess)
      {
        //var faildLoginCount = await _userService.HandleSigninFailedAsync(request.Username);
        throw new BadRequestException(ErrorMessages.User_FailedLogin);
      }
      else
      {
        //var userId = await _userService.HandleSigninSuccessAsync(request.Username);

        // Generate JWT
        var userTypes = new List<byte>() { (byte)UserType.User, (byte)UserType.Ambassador };

        return Ok(await _authenticationService.GetAuthenticationResultAsync(request, userTypes));
      }
    }

    [HttpPost("logout")]
    [SwaggerOperation(Summary = "Logout")]
    public async Task Logout([FromBody] RefreshAuthenticationRequestDto model)
    {
      await _authenticationService.LogoutAsync(_httpContextService.GetCurrentUserId(), model.RefreshToken);
    }

    [HttpPost("refresh-authentication-result")]
    [AllowAnonymous]
    [SwaggerOperation(Summary = "GetNewAuthenticationResult")]
    public async Task<IActionResult> GetNewAuthenticationResult([FromBody] RefreshAuthenticationRequestDto model)
    {
      return Ok(await _authenticationService.RefreshAuthenticationResultAsync(model.RefreshToken));
    }
  }
}
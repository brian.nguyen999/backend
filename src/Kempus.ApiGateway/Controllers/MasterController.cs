﻿using Kempus.Core.Constants;
using Microsoft.AspNetCore.Mvc;

namespace Kempus.ApiGateway.Controllers
{
  /// <summary>
  /// MasterController
  /// </summary>
  [Route("api/master")]
  [ApiController]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.PublicApi)]
  public class MasterController : ControllerBase
  {
    /// <summary>
    /// MasterController
    /// </summary>
    /// <param name="postService"></param>
    public MasterController()
    {
    }

    /// <summary>
    /// GetMasterAsync
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public async Task<IActionResult> GetMasterAsync(Guid id)
    {
      return Ok();
    }
  }
}
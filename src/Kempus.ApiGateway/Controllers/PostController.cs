﻿using Kempus.Core.Constants;
using Kempus.Core.Models;
using Kempus.Models.Public.CourseReviewLike;
using Kempus.Models.Public.Post;
using Kempus.Models.Public.Post.Request;
using Kempus.Services.Common.HttpsContext;
using Kempus.Services.Public.Post;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Kempus.Core.Enums;
using Kempus.Models.Public.PostPollLike;
using Kempus.Services.Public.PostPollLike;

namespace Kempus.ApiGateway.Controllers
{
  /// <summary>
  /// Post Controller
  /// </summary>
  [Route("api/post")]
  [ApiController]
  [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.PublicApi)]
  public class PostController : ControllerBase
  {
    private readonly IPostService _postService;
    private readonly IHttpContextService _httpContextService;
    private readonly IPostPollLikeService _postPollLikeService;
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="postService"></param>
    /// <param name="httpContextService"></param>
    /// <param name="postPollLikeService"></param>
    public PostController(
      IPostService postService,
      IHttpContextService httpContextService,
      IPostPollLikeService postPollLikeService
      )
    {
      _postService = postService;
      _httpContextService = httpContextService;
      _postPollLikeService = postPollLikeService;
    }

    /// <summary>
    /// Search post
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("search")]
    public object Search(
      [FromBody] SearchPostRequestDto model
      )
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      var currentSchoolId = _httpContextService.GetCurrentSchoolId();
      var pagingModel = Pageable.GetPageable(model.PageSize, model.PageIndex, model.SortedField, model.IsDescending);
      var filterModel = new PostFilterModel
      {
        PostIds = model.PostId != null ? new List<Guid> { model.PostId.Value } : new List<Guid>(),
        Keyword = model.Keyword,
        TopicId = model.TopicId,
        UserId = model.UserId,
        SchoolId = currentSchoolId
      };

      return _postService.Search(filterModel, pagingModel, true, currentUserId);
    }

    /// <summary>
    /// Get detail post
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id}")]
    public object GetDetail([Required] Guid id)
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      return _postService.GetDetail(id, currentUserId);
    }

    /// <summary>
    /// Create post
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPost]
    public object Create([FromBody] PostInputModel request)
    {
      return _postService.Create(request);
    }

    /// <summary>
    /// Update post
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPut]
    public void Update([FromBody] PostInputModel dto)
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      dto.UpdatedBy = currentUserId;
      _postService.Update(dto);
    }

    /// <summary>
    /// GetViewDetail
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("view/{id}")]
    public object GetViewDetail([Required] Guid id)
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      return _postService.GetViewDetail(id, currentUserId);
    }


    /// <summary>
    /// Like Post
    /// </summary>
    /// <returns></returns>
    [HttpPost("{id:guid}/like")]
    public void LikeCourseReview(Guid id)
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      var currentSchoolId = _httpContextService.GetCurrentSchoolId();
      _postPollLikeService.CreateIfNotExits(new PostPollLikeInputModel
      {
        ObjectId = id,
        ObjectType = CoreEnums.PostType.Post,
        UserId = currentUserId,
        SchoolId = currentSchoolId
      });
    }

    /// <summary>
    /// UnLike Post
    /// </summary>
    /// <returns></returns>
    [HttpDelete("{id:guid}/like")]
    public void UnLikeCourseReview(Guid id)
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      var currentSchoolId = _httpContextService.GetCurrentSchoolId();
      _postPollLikeService.Delete(new PostPollLikeInputModel
      {
        ObjectId = id,
        ObjectType = CoreEnums.PostType.Post,
        UserId = currentUserId,
        SchoolId = currentSchoolId
      });
    }
  }
}
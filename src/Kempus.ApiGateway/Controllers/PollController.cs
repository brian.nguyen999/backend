using Kempus.Core.Constants;
using Kempus.Core.Enums;
using Kempus.Core.Models;
using Kempus.Models.Public.Poll;
using Kempus.Models.Public.Poll.Request;
using Kempus.Models.Public.PostPollLike;
using Kempus.Services.Common.HttpsContext;
using Kempus.Services.Public.Poll;
using Kempus.Services.Public.PostPollLike;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Kempus.Services.Public.PostPollLike;

namespace Kempus.ApiGateway.Controllers
{
  /// <summary>
  /// PollController
  /// </summary>
  [Route("api/polls")]
  [ApiController]
  [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.PublicApi)]
  public class PollController : ControllerBase
  {
    private readonly IPollService _pollService;
    private readonly IHttpContextService _httpContextService;
    private readonly IPostPollLikeService _postPollLikeService;

    /// <summary>
    /// PollController
    /// </summary>
    /// <param name="pollService"></param>
    /// <param name="httpContextService"></param>
    /// <param name="postPollLikeService"></param>
    public PollController(
      IPollService pollService,
      IHttpContextService httpContextService, 
      IPostPollLikeService postPollLikeService
      )
    {
      _pollService = pollService;
      _httpContextService = httpContextService;
      _postPollLikeService = postPollLikeService;
    }

    /// <summary>
    /// Search poll
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("search")]
    public object Search(
      [FromBody] SearchPollRequestDto model
      )
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      var currentSchoolId = _httpContextService.GetCurrentSchoolId();
      var pagingModel = Pageable.GetPageable(model.PageSize, model.PageIndex, model.SortedField, model.IsDescending);
      var filterModel = new PollFilterModel()
      {
        PostIds = model.PostId != null ? new List<Guid> { model.PostId.Value } : new List<Guid>(),
        Keyword = model.Keyword,
        UserId = model.UserId,
        PollStatus = model.PollStatus,
        SchoolId = currentSchoolId
      };

      return _pollService.Search(filterModel, pagingModel, true, currentUserId);
    }

    /// <summary>
    /// Create new poll
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPost]
    public object Create([FromBody] PollInputModel request)
    {
      return _pollService.Create(request);
    }

    /// <summary>
    /// Update poll
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPut]
    public void Update([FromBody] PollUpdateInputModel request)
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      request.UpdatedBy = currentUserId;
      _pollService.Update(request);
    }

    /// <summary>
    /// Get poll detail
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public object GetDetail(Guid id)
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      return _pollService.GetDetail(id, currentUserId);
    }

    /// <summary>
    /// Get View Poll Details
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("view/{id}")]
    public object GetViewDetail([Required] Guid id)
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      return _pollService.GetViewDetail(id, currentUserId);
    }

    /// <summary>
    /// Like Poll
    /// </summary>
    /// <returns></returns>
    [HttpPost("{id:guid}/like")]
    public void LikePoll(Guid id)
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      var currentSchoolId = _httpContextService.GetCurrentSchoolId();
      _postPollLikeService.CreateIfNotExits(new PostPollLikeInputModel
      {
        ObjectId = id,
        ObjectType = CoreEnums.PostType.Poll,
        UserId = currentUserId,
        SchoolId = currentSchoolId
      });
    }

    /// <summary>
    /// UnLike Poll
    /// </summary>
    /// <returns></returns>
    [HttpDelete("{id:guid}/like")]
    public void UnLikePoll(Guid id)
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      var currentSchoolId = _httpContextService.GetCurrentSchoolId();
      _postPollLikeService.Delete(new PostPollLikeInputModel
      {
        ObjectId = id,
        ObjectType = CoreEnums.PostType.Poll,
        UserId = currentUserId,
        SchoolId = currentSchoolId
      });
    }
  }
}
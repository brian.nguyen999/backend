﻿using Kempus.Core.Constants;
using Kempus.Core.Models;
using Kempus.Models.Public.CourseReview;
using Kempus.Models.Public.CourseReview.Request;
using Kempus.Services.Common.HttpsContext;
using Kempus.Services.Public.CourseReview;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Kempus.Entities;
using Kempus.Models.Public.CourseReviewActivity;
using Kempus.Models.Public.CourseReviewLike;
using Kempus.Services.Public.CourseReviewLike;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.ApiGateway.Controllers
{
  /// <summary>
  /// Course Review Controller
  /// </summary>
  [Route("api/course-review")]
  [ApiController]
  [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.PublicApi)]
  public class CourseReviewController : ControllerBase
  {
    private readonly ICourseReviewService _courseReviewService;
    private readonly ICourseReviewLikeService _courseReviewLikeService;
    private readonly IHttpContextService _httpContextService;

    /// <summary>
    /// Course Review Controller
    /// </summary>
    public CourseReviewController(
      ICourseReviewService courseReviewService,
      IHttpContextService httpContextService,
      ICourseReviewLikeService courseReviewLikeService
      )
    {
      _courseReviewService = courseReviewService;
      _httpContextService = httpContextService;
      _courseReviewLikeService = courseReviewLikeService;
    }

    /// <summary>
    /// Create new Course Review
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    public object Create(
      [FromBody] CourseReviewInputModel input
      )
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      var currentSchoolId = _httpContextService.GetCurrentSchoolId();
      return _courseReviewService.Create(input, currentUserId, currentSchoolId);
    }

    /// <summary>
    /// Search course review
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("search")]
    public object Search(
      [FromBody] SearchCourseReviewRequestDto model
      )
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      var currentSchoolId = _httpContextService.GetCurrentSchoolId();
      var pagingModel = Pageable.GetPageable(model.PageSize, model.PageIndex, model.SortedField, model.IsDescending);
      var filterModel = new CourseReviewFilterModel
      {
        Keyword = model.Keyword,
        CourseId = model.CourseId,
        SchoolId = currentSchoolId,
        Status = (byte)CourseReviewStatus.Published
      };

      return _courseReviewService.Search(filterModel, pagingModel, false, true, currentUserId);
    }

    /// <summary>
    /// Get Most Recent Course Review
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("most-recent")]
    public object GetMostRecentCourseReview(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending
    )
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      var currentSchoolId = _httpContextService.GetCurrentSchoolId();
      isDescending = true;
      sortedField = nameof(CourseReviewEntity.CreatedDate);
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      var filterModel = new CourseReviewFilterModel()
      {
        SchoolId = currentSchoolId,
        Status = (byte)CourseReviewStatus.Published
      };

      return _courseReviewService.Search(filterModel, pagingModel, false, true, currentUserId);
    }

    /// <summary>
    /// Search course my review
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("search-my-reviews")]
    public object SearchMyReviews(
      [FromBody] SearchCourseReviewRequestDto model
      )
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      var currentSchoolId = _httpContextService.GetCurrentSchoolId();
      var pagingModel = Pageable.GetPageable(model.PageSize, model.PageIndex, model.SortedField, model.IsDescending);
      var filterModel = new CourseReviewFilterModel
      {
        Keyword = model.Keyword,
        CourseId = model.CourseId,
        UserId = currentUserId,
        SchoolId = currentSchoolId,
        Status = (byte)CourseReviewStatus.Published
      };

      return _courseReviewService.Search(filterModel, pagingModel, false, true, currentUserId);
    }

    /// <summary>
    /// Get detail course review
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id:Guid}")]
    public object GetDetail([Required] Guid id)
    {
      return _courseReviewService.GetDetail(id);
    }

    /// <summary>
    /// Like Course Review
    /// </summary>
    /// <returns></returns>
    [HttpPost("{id:guid}/like")]
    public void LikeCourseReview(Guid id)
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      var currentSchoolId = _httpContextService.GetCurrentSchoolId();
      _courseReviewLikeService.CreateIfNotExits(new CourseReviewLikeInputModel
      {
        CourseReviewId = id,
        CreatorId = currentUserId,
        SchoolId = currentSchoolId
      });
    }

    /// <summary>
    /// UnLike Course Review
    /// </summary>
    /// <returns></returns>
    [HttpDelete("{id:guid}/like")]
    public void UnLikeCourseReview(Guid id)
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      var currentSchoolId = _httpContextService.GetCurrentSchoolId();
      _courseReviewLikeService.Delete(new CourseReviewLikeInputModel
      {
        CourseReviewId = id,
        CreatorId = currentUserId,
        SchoolId = currentSchoolId
      });
    }
  }
}
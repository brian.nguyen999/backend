﻿using Kempus.Core.Constants;
using Kempus.Models.Public.Flag;
using Kempus.Services.Common.HttpsContext;
using Kempus.Services.Public.Flag;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Kempus.ApiGateway.Controllers
{
  /// <summary>
  /// Flag Controller
  /// </summary>
  [Route("api/flag")]
  [ApiController]
  [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.PublicApi)]
  public class FlagController : Controller
  {
    private readonly IFlagService _flagService;
    private readonly IHttpContextService _httpContextService;

    public FlagController(
      IFlagService flagService,
      IHttpContextService httpContextService
    )
    {
      _flagService = flagService;
      _httpContextService = httpContextService;
    }

    /// <summary>
    /// Insert new Report
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    public object Create([FromBody] FlagInputRequestDto input)
    {
      var userId = _httpContextService.GetCurrentUserId();
      var currentSchoolId = _httpContextService.GetCurrentSchoolId();

      var inputModel = new FlagInputModel()
      {
        ObjectId = input.ObjectId,
        ObjectType = input.ObjectType,
        ContentUrl = input.ContentUrl,
        Reason = input.Reason,
        Description = input.Description,
        SchoolId = currentSchoolId,
        CreatedBy = userId
      };

      return _flagService.Create(inputModel);
    }
  }
}

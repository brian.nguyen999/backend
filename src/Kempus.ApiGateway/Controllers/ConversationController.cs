using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Models.Public.Conversation;
using Kempus.Services.Common.HttpsContext;
using Kempus.Services.Public.Conversation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.ApiGateway.Controllers
{
  /// <summary>
  /// Conversation Controller
  /// </summary>
  [Route("api/conversations")]
  [ApiController]
  [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.PublicApi)]
  public class ConversationController : ControllerBase
  {
    private readonly IConversationService _conversationService;
    private readonly IHttpContextService _httpContextService;
    private readonly IConfiguration _configuration;

    /// <summary>
    /// ConversationController
    /// </summary>
    /// <param name="configuration"></param>
    /// <param name="conversationService"></param>
    /// <param name="httpContextService"></param>
    public ConversationController(
      IConfiguration configuration,
      IConversationService conversationService,
      IHttpContextService httpContextService)
    {
      _configuration = configuration;
      _conversationService = conversationService;
      _httpContextService = httpContextService;
    }

    /// <summary>
    /// Create
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    public object Create([FromBody] CreateConversationRequestModel input)
    {
      if (input == null) throw new BadRequestException(ErrorMessages.Invalid_Request);
      var currentSchoolId = _httpContextService.GetCurrentSchoolId();
      var currentUserId = _httpContextService.GetCurrentUserId();
      var inputModel = new ConversationInputModel
      {
        FromUserId = currentUserId,
        ToUserId = input.ToUserId,
        SchoolId = currentSchoolId
      };
      return _conversationService.Create(inputModel);
    }

    /// <summary>
    /// Get conversation detail
    /// </summary>
    /// <param name="conversationId"></param>
    /// <returns></returns>
    [HttpGet("{conversationId:guid}")]
    public object GetDetail(Guid conversationId)
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      var currentSchoolId = _httpContextService.GetCurrentSchoolId();
      return _conversationService.GetDetail(conversationId, currentUserId, currentSchoolId);
    }

    /// <summary>
    /// Search Conversation
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="sortedField"></param>
    /// <param name="isDescending"></param>
    /// <param name="model"></param>
    /// <exception cref="BadRequestException"></exception>
    [HttpPost("search")]
    public object SearchConversation(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      [FromBody] ConversationListRequestModel model
      )
    {
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      var currentSchoolId = _httpContextService.GetCurrentSchoolId();
      var currentUserId = _httpContextService.GetCurrentUserId();

      var filterModel = new ConversationFilterModel
      {
        SchoolId = currentSchoolId,
        UserId = currentUserId,
        Status = new List<byte>() { model.Status }
      };

      return _conversationService.SearchConversations(filterModel, pagingModel, currentUserId, false);
    }

    /// <summary>
    /// Send Message
    /// </summary>
    /// <param name="conversationId"></param>
    /// <param name="input"></param>
    /// <exception cref="BadRequestException"></exception>
    [HttpPost("{conversationId:guid}/messages")]
    public object SendMessage(Guid conversationId,
      [FromBody] CreateConversationMessageRequestModel input)
    {
      if (input == null) throw new BadRequestException(ErrorMessages.Invalid_Request);
      var currentUserId = _httpContextService.GetCurrentUserId();
      var currentSchoolId = _httpContextService.GetCurrentSchoolId();
      var message = new ConversationMessageInputModel
      {
        Message = input.Message,
        ConversationId = conversationId,
        FromUserId = currentUserId,
        SchoolId = currentSchoolId
      };
      return _conversationService.SendMessage(message);
    }

    /// <summary>
    /// Search Messages
    /// </summary>
    /// <param name="conversationId"></param>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="sortedField"></param>
    /// <param name="isDescending"></param>
    /// <param name="filterModel"></param>
    /// <exception cref="BadRequestException"></exception>
    [HttpPost("{conversationId:guid}/messages/search")]
    public object Search(Guid conversationId,
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      [FromBody] ConversationMessageFilterModel filterModel)
    {
      if (filterModel == null) throw new BadRequestException(ErrorMessages.Invalid_Request);
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      var currentSchoolId = _httpContextService.GetCurrentSchoolId();
      var currentUserId = _httpContextService.GetCurrentUserId();
      filterModel.SchoolId = currentSchoolId;
      filterModel.ConversationId = conversationId;
      return _conversationService.SearchMessages(filterModel, pagingModel, currentUserId, false);
    }

    /// <summary>
    /// Get Requests
    /// </summary>
    /// <returns></returns>
    [HttpGet("total-requests")]
    public object GetTotalPending()
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      var totalPending = _conversationService.GetTotalPending(currentUserId); ;
      return new ConversationTotalPendingModel
      {
        TotalPending = totalPending
      };
    }

    /// <summary>
    /// Get Requests
    /// </summary>
    /// <returns></returns>
    [HttpGet("total-unread")]
    public object GetTotalUnreadMessage()
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      var totalUnread = _conversationService.GetTotalUnread(currentUserId); ;
      return new ConversationTotalUnreadMessageModel
      {
        TotalUnread = totalUnread
      };
    }

    /// <summary>
    /// Delete Conversation
    /// </summary>
    /// <param name="conversationId"></param>
    /// <returns></returns>
    [HttpDelete("{conversationId:guid}")]
    public object Delete(Guid conversationId)
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      return _conversationService.Delete(conversationId, currentUserId);
    }

    [HttpPost("blocked/search")]
    public object SearchConversationBlocked(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      [FromBody] ConversationFilterRequestModel requestModel)
    {
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      var currentUserId = _httpContextService.GetCurrentUserId();

      var filterModel = new ConversationBlockFilterModel
      {
        UserId = currentUserId,
        Keyword = requestModel.Keyword
      };

      return _conversationService.SearchConversationBlocked(filterModel, pagingModel, false);
    }

    /// <summary>
    /// Block User
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpPost("block")]
    public object Block(ConversationBlockRequestModel request)
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      var schoolId = _httpContextService.GetCurrentSchoolId();

      var input = new ConversationBlockInputModel()
      {
        ConversationId = request.ConversationId,
        IsDeleteChat = request.IsDeleteChat,
        BlockerUserId = currentUserId,
        SchoolId = schoolId
      };

      return _conversationService.Block(input);
    }

    /// <summary>
    /// Unblock some user
    /// </summary>
    /// <param name="conversationId"></param>
    /// <returns></returns>
    [HttpPut("{conversationId:guid}/unblock")]
    public object Unblock(Guid conversationId)
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      return _conversationService.Unblock(conversationId, currentUserId);
    }

    /// <summary>
    /// Accept pending conversation
    /// </summary>
    /// <param name="conversationId"></param>
    /// <returns></returns>
    [HttpPost("{conversationId:guid}/accept")]
    public object AcceptPendingConversation(Guid conversationId)
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      return _conversationService.AcceptPendingConversation(conversationId, currentUserId);
    }

    /// <summary>
    /// Get Requests
    /// </summary>
    /// <returns></returns>
    [HttpGet("{conversationId:guid}/total-messages")]
    public object GetTotalMessage(Guid conversationId)
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      var totalMessages = _conversationService.GetTotalMessages(conversationId, currentUserId); ;
      return new ConversationTotalMessageModel()
      {
        TotalMessages = totalMessages,
        NumberOfPendingMessage = int.Parse(_configuration["ChatConfig:NumberOfPendingMessage"])
      };
    }
  }
}

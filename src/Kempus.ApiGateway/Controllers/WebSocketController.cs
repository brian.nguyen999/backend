﻿using Kempus.Core.Constants;
using Kempus.Models.Common.WebSocket;
using Kempus.Services.Common.HttpsContext;
using Kempus.Services.Common.WebSocket;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace Kempus.ApiGateway.Controllers
{
  [Route("api/websocket")]
  [ApiController]
  [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.PublicApi)]
  public class WebSocketController : ControllerBase
  {
    private readonly IWebSocketService _webSocketService;
    private readonly IHttpContextService _httpContextService;

    public WebSocketController(
      IWebSocketService webSocketService,
      IHttpContextService httpContextService)
    {
      _webSocketService = webSocketService;
      _httpContextService = httpContextService;
    }

    [HttpPost("connect")]
    public async Task<bool> Connect([FromQuery] string connectionId)
    {
      var input = new ConnectionInputModel()
      {
        ConnectionId = connectionId,
        UserId = _httpContextService.GetCurrentUserId(),
        SchoolId = _httpContextService.GetCurrentSchoolId(),
      };

      // Store connectionId in redis
      return await _webSocketService.AddConnectionAsync(input);

      // The connection.UserId can be used to identify which user using that connectionId
    }

    [AllowAnonymous]
    [HttpPost("disconnect")]
    public async Task Disconnect([FromQuery] string connectionId)
    {
      var input = new ConnectionInputModel()
      {
        ConnectionId = connectionId,
      };

      // Remove connectionId out of memory data
      await _webSocketService.RemoveConnectionAsync(input);
    }
    [HttpPost("message")]
    public async Task SendMessage([FromQuery] string message)
    {
      var schoolId = _httpContextService.GetCurrentSchoolId();
      // Get the connectionIds of other users in the chat room
      var connectionIds = await _webSocketService.GetConnectionIdsBySchoolIdAsync(schoolId);

      // Sends the message to all connected clients using the new API Gateway Management API 
      // excepts the current connection Id
      await _webSocketService.SendMessage(connectionIds, "newMessage", message);
    }

    //[HttpPost("message")]
    //public async Task SendNotification([FromBody] NotificationRequestDto request)
    //{
    //  var input = new NotificationInputModel()
    //  {
    //    ConnectionId = request.ConnectionId,
    //    Action = request.Action,
    //    Payload = request.Payload,
    //    UserId = _httpContextService.GetCurrentUserId(),
    //    SchoolId = _httpContextService.GetCurrentSchoolId(),
    //  };

    //  // Sends the message to all connected clients using the new API Gateway Management API
    //  // that excepts the current connectionId
    //  //await _webSocketService.SendMessage(connectionIds, "newMessage", input.Message);
    //  await _webSocketService.SendNotification(input);
    //}
  }
}

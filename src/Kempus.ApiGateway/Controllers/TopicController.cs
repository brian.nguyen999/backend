﻿using Kempus.Core.Constants;
using Kempus.Core.Models;
using Kempus.Models.Public.Poll;
using Kempus.Models.Public.Topic;
using Kempus.Models.Public.Topic.Request;
using Kempus.Services.Common.HttpsContext;
using Kempus.Services.Public.Topic;
using Microsoft.AspNetCore.Mvc;

namespace Kempus.ApiGateway.Controllers
{
  /// <summary>
  /// Topic Controller
  /// </summary>
  [Route("api/topic")]
  [ApiController]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.PublicApi)]
  public class TopicController : ControllerBase
  {
    private readonly IHttpContextService _httpContextService;
    private readonly ITopicService _topicService;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="topicService"></param>
    public TopicController(
      ITopicService topicService,
      IHttpContextService httpContextService
    )
    {
      _httpContextService = httpContextService;
      _topicService = topicService;
    }

    /// <summary>
    /// Search topic
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("search")]
    public object Search(
      [FromBody] SearchTopicRequestDto model
      )
    {
      var currentSchoolId = _httpContextService.GetCurrentSchoolId();
      var pagingModel = Pageable.GetPageable(model.PageSize, model.PageIndex, model.SortedField, model.IsDescending);
      var filterModel = new TopicFilterModel
      {
        TopicId = model.TopicId,
        Keyword = model.Keyword,
        SchoolId = currentSchoolId,
        Status = true
      };

      return _topicService.Search(filterModel, pagingModel, true);
    }
  }
}
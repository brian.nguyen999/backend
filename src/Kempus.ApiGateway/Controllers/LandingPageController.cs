﻿using Kempus.Core.Constants;
using Kempus.Core.Models;
using Kempus.Entities;
using Kempus.Models.Public.School;
using Kempus.Services.Public.LandingPage;
using Kempus.Services.Public.School;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Kempus.ApiGateway.Controllers
{
  /// <summary>
  /// LandingPageController
  /// </summary>
  [Route("api/landing-page")]
  [ApiController]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.PublicApi)]
  public class LandingPageController : ControllerBase
  {
    private readonly ILandingPageService _landingPageService;

    /// <summary>
    /// LandingPageController
    /// </summary>
    /// <param name="landingPageService"></param>
    public LandingPageController(
      ILandingPageService landingPageService
    )
    {
      _landingPageService = landingPageService;
    }

    /// <summary>
    /// Get school data for landing page
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpGet("school-data")]
    public object GetSchoolData()
    {
      return _landingPageService.GetSchoolCount();
    }

    /// <summary>
    /// Search
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="sortedField"></param>
    /// <param name="isDescending"></param>
    /// <param name="filterModel"></param>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpPost("school-data/search")]
    public object Search(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      [FromBody] SchoolFilterModel filterModel)
    {

      isDescending = sortedField != null && isDescending;
      sortedField = sortedField ?? nameof(MajorEntity.Name);
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      return _landingPageService.Search(filterModel, pagingModel, useMasterDb: true);
    }
  }
}
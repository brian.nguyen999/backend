﻿using Kempus.Core.Constants;
using Kempus.Models.Public.Status.Response;
using Kempus.Services.Common.Redis;
using Kempus.Services.Common.WebSocket;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StackExchange.Redis;
using System.ComponentModel.DataAnnotations;

namespace Kempus.ApiGateway.Controllers
{
  /// <summary>
  /// StatusController
  /// </summary>
  [Route("api/status")]
  [ApiController]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.PublicApi)]
  public class StatusController : ControllerBase
  {
    private readonly IConfiguration _configuration;
    private readonly IRedisService _redisService;
    private readonly IConnectionMultiplexer _redis;
    private readonly IWebSocketService _webSocketService;

    /// <summary>
    /// StatusController
    /// </summary>
    /// <param name="configuration"></param>
    public StatusController(
      IConfiguration configuration,
      IRedisService redisService,
      IConnectionMultiplexer redis,
      IWebSocketService webSocketService)
    {
      _configuration = configuration;
      _redisService = redisService;
      _redis = redis;
      _webSocketService = webSocketService;
    }

    /// <summary>
    /// HealthCheck
    /// </summary>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpGet("health-check")]
    public IActionResult HealthCheck()
    {
      return Ok("release-1.0.0-dev");
    }

    /// <summary>
    /// GetAppSettingConfig
    /// </summary>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpGet("config")]
    public IActionResult GetAppSettingConfig()
    {
      return Ok(new AppSettingConfig() { NickNameLockdownMinutes = _configuration["UserConfig:NickNameLockdownMinutes"] });
    }
  }
}
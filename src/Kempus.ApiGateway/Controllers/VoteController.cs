﻿using Kempus.Core.Constants;
using Kempus.Models.Public.Vote;
using Kempus.Models.Public.Vote.Request;
using Kempus.Services.Common.HttpsContext;
using Kempus.Services.Public.Vote;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Kempus.ApiGateway.Controllers
{
  /// <summary>
  /// Vote Controller
  /// </summary>
  [Route("api/vote")]
  [ApiController]
  [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.PublicApi)]
  public class VoteController
  {
    private readonly IVoteService _voteService;
    private readonly IHttpContextService _httpContextService;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="voteService"></param>
    /// <param name="httpContextService"></param>
    public VoteController(
      IVoteService voteService,
      IHttpContextService httpContextService
      )
    {
      _voteService = voteService;
      _httpContextService = httpContextService;
    }

    /// <summary>
    /// Create vote
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPost]
    public object Create([FromBody] CreateVoteRequestDto dto)
    {
      var inputModel = new VoteInputModel
      {
        PollId = dto.PollId,
        PollOptionIds = dto.PollOptionIds,
        CreatorId = _httpContextService.GetCurrentUserId()
      };

      return _voteService.Create(inputModel);
    }

    /// <summary>
    /// Check is voted
    /// </summary>
    /// <param name="pollId"></param>
    /// <returns></returns>
    [HttpPost("check-is-voted")]
    public object CheckIsVoted([Required] Guid pollId)
    {
      return _voteService.CheckVoted(_httpContextService.GetCurrentUserId(), pollId);
    }
  }
}

﻿using Kempus.Core.Constants;
using Kempus.Core.Models;
using Kempus.Models.Public.Comment;
using Kempus.Models.Public.Comment.Request;
using Kempus.Models.Public.PostComments;
using Kempus.Services.Common.HttpsContext;
using Kempus.Services.Public.Comment;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;

namespace Kempus.ApiGateway.Controllers
{
  /// <summary>
  /// PostComment Controller
  /// </summary>
  [Route("api/comment")]
  [ApiController]
  [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.PublicApi)]
  public class CommentController : ControllerBase
  {
    private readonly IHttpContextService _httpContextService;
    private readonly IPostCommentService _postCommentService;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="httpContextService"></param>
    /// <param name="postCommentService"></param>
    public CommentController(
      IHttpContextService httpContextService,
      IPostCommentService postCommentService
      )
    {
      _httpContextService = httpContextService;
      _postCommentService = postCommentService;
    }

    [HttpPost("search")]
    public object Search(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      [FromBody] PostCommentFilterModel filterModel)
    {
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      return _postCommentService.Search(filterModel, pagingModel, useMasterDb: false);
    }

    /// <summary>
    /// Create
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPost]
    public object Create(CreateCommentRequestDto dto)
    {
      var model = new PostCommentInputModel
      {
        PostId = dto.PostId,
        ParentId = dto.ParentId,
        Content = dto.Content,
        Type = dto.Type
      };

      return _postCommentService.Create(model, _httpContextService.GetCurrentUserId(), _httpContextService.GetCurrentSchoolId());
    }
  }
}
﻿using System.ComponentModel.DataAnnotations;
using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Entities;
using Kempus.Models.Admin.Poll;
using Kempus.Models.Admin.Post;
using Kempus.Services.Admin.Poll;
using Kempus.Services.Common.HttpsContext;
using Microsoft.AspNetCore.Mvc;

namespace Kempus.ApiGateway.Controllers.Admin
{
  /// <summary>
  /// PollAdminController
  /// </summary>
  [Route("api/admin/polls")]
  public class PollAdminController : BaseAdminController
  {
    public readonly IPollAdminService _pollAdminService;
    private readonly IHttpContextService _httpContextService;

    /// <summary>
    /// PollAdminController
    /// </summary>
    /// <param name="pollAdminService"></param>
    /// <param name="httpContextService"></param>
    public PollAdminController(
      IPollAdminService pollAdminService,
        IHttpContextService httpContextService
        )
    {
      _pollAdminService = pollAdminService;
      _httpContextService = httpContextService;
    }

    /// <summary>
    /// Search poll
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("search")]
    public object Search(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      [FromBody] PollFilterModel filterModel
      )
    {
      //if (!_httpContextService.HasPermissions(PermissionId.ForumView, PermissionId.PollsView))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      return _pollAdminService.Search(filterModel, pagingModel, false, null);
    }

    /// <summary>
    /// Update poll
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPut]
    public void Update([FromBody] PollUpdateInputModel input)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.ForumCreateEdit, PermissionId.PollsCreateEdit))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      var currentUserId = _httpContextService.GetCurrentUserId();
      input.UpdatedBy = currentUserId;
      _pollAdminService.Update(input);
    }

    /// <summary>
    /// Get detail poll
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id}")]
    public object GetDetail([Required] Guid id)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.ForumCreateEdit, PermissionId.PollsCreateEdit))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      var currentUserId = _httpContextService.GetCurrentUserId();
      return _pollAdminService.GetDetail(id, currentUserId);
    }

    /// <summary>
    /// Create new poll
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPost]
    public object Create([FromBody] PollInputModel request)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.ForumCreateEdit, PermissionId.PollsCreateEdit))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      return _pollAdminService.Create(request);
    }

  }
}

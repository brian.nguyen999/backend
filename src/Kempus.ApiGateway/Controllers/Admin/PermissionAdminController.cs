﻿using Kempus.Models.Public.Permission;
using Kempus.Services.Public.Permission;
using Microsoft.AspNetCore.Mvc;

namespace Kempus.ApiGateway.Controllers.Admin
{
  /// <summary>
  /// PermissionAdminController
  /// </summary>
  [Route("api/admin/permissions")]
  public class PermissionAdminController : BaseAdminController
  {
    private readonly IPermissionService _permissionService;

    /// <summary>
    /// PermissionAdminController
    /// </summary>
    /// <param name="permissionService"></param>
    public PermissionAdminController(IPermissionService permissionService)
    {
      _permissionService = permissionService;
    }

    /// <summary>
    /// Get all permissions
    /// </summary>
    /// <param name="moduleApplicationId"></param>
    /// <returns></returns>
    [HttpGet("get-all")]
    public List<PermissionDetailModel> GetAll(int? moduleApplicationId)
    {
      return _permissionService.GetAll(moduleApplicationId);
    }
  }
}
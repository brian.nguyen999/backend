﻿using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Models.Admin.Topic;
using Kempus.Services.Admin.Topic;
using Kempus.Services.Common.HttpsContext;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Kempus.ApiGateway.Controllers.Admin
{
  /// <summary>
  /// Topic controller
  /// </summary>
  [Route("api/admin/topics")]
  public class TopicAdminController : BaseAdminController
  {
    private readonly IHttpContextService _httpContextService;
    private readonly ITopicAdminService _topicAdminService;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="httpContextService"></param>
    /// <param name="topicAdminService"></param>
    public TopicAdminController(
      IHttpContextService httpContextService,
      ITopicAdminService topicAdminService
      )
    {
      _httpContextService = httpContextService;
      _topicAdminService = topicAdminService;
    }

    /// <summary>
    /// Search Topic
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="sortedField"></param>
    /// <param name="isDescending"></param>
    /// <param name="filterModel"></param>
    /// <returns></returns>
    [HttpPost("search")]
    public object Search(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      [FromBody] TopicFilterModel filterModel)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.SchoolDataView))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      return _topicAdminService.Search(filterModel, pagingModel, false);
    }

    /// <summary>
    /// Create Topic
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPost]
    public object Create(TopicCreateRequestDto dto)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.SchoolDataCreateEdit))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      var model = new TopicInputModel
      {
        SchoolId = dto.SchoolId,
        Name = dto.Name,
        IsActivated = dto.IsActivated,
        CreatorId = _httpContextService.GetCurrentUserId()
      };

      return _topicAdminService.Create(model);
    }

    /// <summary>
    /// Update Topic
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPut]
    public void Update(TopicUpdateRequestDto dto)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.SchoolDataCreateEdit))
        //throw new ForbiddenException(ErrorMessages.NoPermission);
      var model = new TopicInputModel
      {
        Id = dto.Id,
        IsActivated = dto.IsActivated,
        CreatorId = _httpContextService.GetCurrentUserId()
      };

      _topicAdminService.Update(model);
    }

    /// <summary>
    /// Get detail Topic
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public object GetDetail([Required] Guid id)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.SchoolDataCreateEdit))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      return _topicAdminService.GetDetail(id);
    }
  }
}

﻿using Kempus.Models.Admin.Course;
using Kempus.Models.Admin.Course.Request;
using Kempus.Services.Admin.Course;
using Kempus.Services.Common.HttpsContext;
using Microsoft.AspNetCore.Mvc;

namespace Kempus.ApiGateway.Controllers.Admin
{
  /// <summary>
  /// Course Admin controller
  /// </summary>
  [Route("api/admin/courses")]
  public class CourseAdminController : BaseAdminController
  {
    private readonly IHttpContextService _httpContextService;
    private readonly ICourseAdminService _courseAdminService;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="httpContextService"></param>
    /// <param name="courseAdminService"></param>
    public CourseAdminController(
      IHttpContextService httpContextService,
      ICourseAdminService courseAdminService)
    {
      _httpContextService = httpContextService;
      _courseAdminService = courseAdminService;
    }

    /// <summary>
    /// Create Course
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPost]
    public object Create(CourseCreateRequestDto dto)
    {
      var model = new CourseInputCmsModel
      {
        SchoolId = dto.SchoolId,
        Name = dto.Name,
        DepartmentProgramId = dto.DepartmentProgramId,
        InstructorIds = dto.InstructorIds,
        Status = dto.Status,
        CreatorId = _httpContextService.GetCurrentUserId()
      };

      return _courseAdminService.Create(model);
    }

    /// <summary>
    /// Update Course
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPut]
    public void Update(CourseUpdateRequestDto dto)
    {
      var model = new CourseInputCmsModel
      {
        Id = dto.Id,
        Name = dto.Name,
        DepartmentProgramId = dto.DepartmentProgramId,
        InstructorIds = dto.InstructorIds,
        Status = dto.Status,
        UpdaterId = _httpContextService.GetCurrentUserId()
      };

      _courseAdminService.Update(model);
    }
  }
}

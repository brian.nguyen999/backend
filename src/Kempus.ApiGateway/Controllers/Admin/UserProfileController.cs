﻿using Kempus.Core.Constants;
using Kempus.Services.Common.HttpsContext;
using Kempus.Services.Public.User;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Kempus.ApiGateway.Controllers.Admin
{
  /// <summary>
  /// UserProfileController
  /// </summary>
  [Route("api/admin")]
  [ApiController]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.AdminApi)]
  public class UserProfileController : BaseAdminController
  {
    private readonly IUserService _userService;
    private readonly IHttpContextService _httpContextService;

    /// <summary>
    /// UserProfileController
    /// </summary>
    /// <param name="userService"></param>
    /// <param name="httpContextService"></param>
    public UserProfileController(
      IUserService userService,
      IHttpContextService httpContextService
      )
    {
      _userService = userService;
      _httpContextService = httpContextService;
    }

    /// <summary>
    /// GetCurrentProfile
    /// </summary>
    /// <returns></returns>
    [HttpGet("user-profile")]
    public object GetCurrentProfile()
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      return _userService.GetAdminDetail(currentUserId);
    }
  }
}

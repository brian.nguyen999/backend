﻿using Kempus.Core.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Kempus.ApiGateway.Controllers.Admin
{
  [Authorize(Policy = CoreConstants.PolicyName.AdminPolicy)]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.AdminApi)]
  [ApiController]
  public class BaseAdminController : ControllerBase
  {
    public BaseAdminController()
    {
    }
  }
}
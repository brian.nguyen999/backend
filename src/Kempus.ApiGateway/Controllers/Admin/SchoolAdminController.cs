using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Models.Admin.Course;
using Kempus.Models.Admin.DepartmentProgram;
using Kempus.Models.Admin.Instructor;
using Kempus.Models.Common;
using Kempus.Models.Public.Course;
using Kempus.Models.Public.DepartmentProgram;
using Kempus.Models.Public.School;
using Kempus.Services.Common.HttpsContext;
using Kempus.Services.FullTextSearch.Course;
using Kempus.Services.Public.Course;
using Kempus.Services.Public.DepartmentProgram;
using Kempus.Services.Public.Instructor;
using Kempus.Services.Public.School;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Kempus.ApiGateway.Controllers.Admin
{
  /// <summary>
  /// School Admin controller
  /// </summary>
  [Route("api/admin/schools")]
  public class SchoolAdminController : BaseAdminController
  {
    private readonly ISchoolService _schoolService;
    private readonly IHttpContextService _httpContextService;
    private readonly IInstructorService _instructorService;
    private readonly ICourseService _courseService;
    private readonly IDepartmentProgramService _departmentProgramService;
    private readonly ICourseFtsService _courseFtsService;

    /// <summary>
    /// SchoolAdminController
    /// </summary>
    /// <param name="schoolService"></param>
    /// <param name="instructorService"></param>
    /// <param name="courseService"></param>
    /// <param name="departmentProgramService"></param>
    /// <param name="httpContextService"></param>
    /// <param name="courseFtsService"></param>
    public SchoolAdminController(
      ISchoolService schoolService,
      IInstructorService instructorService,
      ICourseService courseService,
      IDepartmentProgramService departmentProgramService,
      IHttpContextService httpContextService,
      ICourseFtsService courseFtsService)
    {
      _schoolService = schoolService;
      _instructorService = instructorService;
      _courseService = courseService;
      _departmentProgramService = departmentProgramService;
      _httpContextService = httpContextService;
      _courseFtsService = courseFtsService;
    }

    /// <summary>
    /// Search School
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="sortedField"></param>
    /// <param name="isDescending"></param>
    /// <param name="filterModel"></param>
    /// <returns></returns>
    [HttpPost("search")]
    public PagingResult<SchoolListViewModel> Search(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      [FromBody] SchoolFilterModel filterModel)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.SchoolDataView))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      return _schoolService.Search(filterModel, pagingModel, true);
    }

    /// <summary>
    /// Get detail School
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id}")]
    public SchoolDetailModel GetDetail([Required] Guid id)
    {
      if (!_httpContextService.HasPermissions(PermissionId.SchoolDataView))
        throw new ForbiddenException(ErrorMessages.NoPermission);
      return _schoolService.GetDetail(id);
    }

    /// <summary>
    /// Get download template link
    /// </summary>
    [HttpGet("get-import-template-link")]
    public async Task<string> GetImportTemplateLink()
    {
      if (!_httpContextService.HasPermissions(PermissionId.SchoolDataView))
        throw new ForbiddenException(ErrorMessages.NoPermission);
      return await _schoolService.GetImportTemplateLink();
    }

    /// <summary>
    /// Create School
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost]
    public object Create(SchoolInputModel model)
    {
      if (!_httpContextService.HasPermissions(PermissionId.SchoolDataCreateEdit))
        throw new ForbiddenException(ErrorMessages.NoPermission);
      return _schoolService.Create(model);
    }

    /// <summary>
    /// Update School
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPut]
    public void Update(SchoolInputModel model)
    {
      if (!_httpContextService.HasPermissions(PermissionId.SchoolDataCreateEdit))
        throw new ForbiddenException(ErrorMessages.NoPermission);
      _schoolService.Update(model);
    }

    /// <summary>
    /// Import School data
    /// </summary>
    /// <param name="requestDto"></param>
    /// <returns></returns>
    [HttpPost("upload")]
    public async Task<object> UploadFile([FromForm] UploadFileRequestDto requestDto)
    {
      if (!_httpContextService.HasPermissions(PermissionId.SchoolDataCreateEdit))
        throw new ForbiddenException(ErrorMessages.NoPermission);

      var creatorId = _httpContextService.GetCurrentUserId();
      var input = new SchoolUploadInputModel()
      {
        File = requestDto.File,
        SchoolId = requestDto.SchoolId,
        CreatorId = creatorId
      };

      return await _schoolService.UploadAsync(input);
    }

    [HttpGet("index-course")]
    public async Task IndexCourse([FromQuery] Guid schoolId)
    {
      if (!_httpContextService.HasPermissions(PermissionId.SchoolDataCreateEdit))
        throw new ForbiddenException(ErrorMessages.NoPermission);
      await _schoolService.IndexCourseBySchoolIdAsync(schoolId);
    }

    /// <summary>
    /// Search Instructor
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="sortedField"></param>
    /// <param name="isDescending"></param>
    /// <param name="filterModel"></param>
    /// <returns></returns>
    [HttpPost("instructor/search")]
    public PagingResult<InstructorListViewModel> SearchInstructor(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      [FromBody] InstructorFilterModel filterModel)
    {
      if (!_httpContextService.HasPermissions(PermissionId.SchoolDataView))
        throw new ForbiddenException(ErrorMessages.NoPermission);
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      return _instructorService.Search(filterModel, pagingModel, true);
    }

    /// <summary>
    /// Get Instructor Detail
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("instructor/{id}")]
    public InstructorDetailCmsModel GetInstructorDetail([Required] Guid id)
    {
      if (!_httpContextService.HasPermissions(PermissionId.SchoolDataView))
        throw new ForbiddenException(ErrorMessages.NoPermission);
      return _instructorService.GetDetail(id);
    }

    /// <summary>
    /// Search Course
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="sortedField"></param>
    /// <param name="isDescending"></param>
    /// <param name="filterModel"></param>
    /// <returns></returns>
    [HttpPost("course/search")]
    public PagingResult<CmsCourseListViewModel> SearchCourse(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      [FromBody] CmsCourseFilterModel filterModel)
    {
      if (!_httpContextService.HasPermissions(PermissionId.SchoolDataView))
        throw new ForbiddenException(ErrorMessages.NoPermission);
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      return _courseService.CmsSearch(filterModel, pagingModel, true);
    }

    /// <summary>
    /// Get Course `Detail
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("course/{id}")]
    public CourseDetailCmsModel GetCourseDetail([Required] Guid id)
    {
      if (!_httpContextService.HasPermissions(PermissionId.SchoolDataView))
        throw new ForbiddenException(ErrorMessages.NoPermission);
      return _courseService.GetDetailCms(id);
    }

    /// <summary>
    /// Search Department/Program
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="sortedField"></param>
    /// <param name="isDescending"></param>
    /// <param name="filterModel"></param>
    /// <returns></returns>
    [HttpPost("department-program/search")]
    public PagingResult<DepartmentProgramListViewModel> SearchDepartmentProgram(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      [FromBody] DepartmentProgramFilterModel filterModel)
    {
      if (!_httpContextService.HasPermissions(PermissionId.SchoolDataView))
        throw new ForbiddenException(ErrorMessages.NoPermission);
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      return _departmentProgramService.CmsSearch(filterModel, pagingModel, true);
    }

    /// <summary>
    /// Get Department/Program Detail
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("department-program/{id}")]
    public DepartmentProgramDetailCmsModel GetDepartmentProgramDetail([Required] Guid id)
    {
      if (!_httpContextService.HasPermissions(PermissionId.SchoolDataView))
        throw new ForbiddenException(ErrorMessages.NoPermission);
      return _departmentProgramService.GetDetailCms(id);
    }

    [HttpDelete("school-index/{indexName}")]
    public async Task DeleteIndex(string indexName)
    {
      if (!_httpContextService.HasPermissions(PermissionId.SchoolDataCreateEdit))
        throw new ForbiddenException(ErrorMessages.NoPermission);
      await _courseFtsService.DeleteIndex(indexName);
    }
  }
}
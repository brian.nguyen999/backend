﻿using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Models.Admin.BellNotification;
using Kempus.Models.Admin.BellNotification.Request;
using Kempus.Models.Public.BellNotification;
using Kempus.Services.Admin.BellNotification;
using Kempus.Services.Common.HttpsContext;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Kempus.ApiGateway.Controllers.Admin
{
  /// <summary>
  /// Bell Notification Admin controller
  /// </summary>
  [Route("api/admin/bell-notifications")]
  public class BellNotificationAdminController : BaseAdminController
  {
    private readonly IHttpContextService _httpContextService;
    private readonly IBellNotificationAdminService _bellNotificationAdminService;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="httpContextService"></param>
    /// <param name="bellNotificationAdminService"></param>
    public BellNotificationAdminController(
      IHttpContextService httpContextService,
      IBellNotificationAdminService bellNotificationAdminService
      )
    {
      _httpContextService = httpContextService;
      _bellNotificationAdminService = bellNotificationAdminService;
    }

    /// <summary>
    /// Search Bell Notification
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="sortedField"></param>
    /// <param name="isDescending"></param>
    /// <param name="filterModel"></param>
    /// <returns></returns>
    [HttpPost("search")]
    public object Search(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      [FromBody] BellNotificationFilterCmsModel filterModel)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.AdminView, PermissionId.SendNotificationView))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      return _bellNotificationAdminService.Search(filterModel, pagingModel, false);
    }

    /// <summary>
    /// Get detail Bell Notification
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id}")]
    public object GetDetail([Required] Guid id)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.AdminView, PermissionId.SendNotificationView))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      return _bellNotificationAdminService.GetDetail(id);
    }

    /// <summary>
    /// Create Bell Notification
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPost]
    public object Create(BellNotificationCreateRequestDto dto)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.AdminView, PermissionId.SendNotificationCreateEdit))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      var model = new BellNotificationInputModel
      {
        SchoolIds = dto.SchoolIds,
        Title = dto.Title,
        UrlLink = dto.UrlLink,
        BellNotificationStatus = dto.BellNotificationStatus,
        ScheduledDate = dto.ScheduledDate,
        CreatedUser = _httpContextService.GetCurrentUserId()
      };

      return _bellNotificationAdminService.Create(model);
    }

    /// <summary>
    /// Update Bell Notification
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPut]
    public void Update(BellNotificationUpdateRequestDto dto)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.AdminView, PermissionId.SendNotificationView))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      var model = new BellNotificationInputModel
      {
        Id = dto.Id,
        SchoolIds = dto.SchoolIds,
        Title = dto.Title,
        UrlLink = dto.UrlLink,
        BellNotificationStatus = dto.BellNotificationStatus,
        ScheduledDate = dto.ScheduledDate,
        UpdatedUser = _httpContextService.GetCurrentUserId()
      };

      _bellNotificationAdminService.Update(model);
    }
  }
}

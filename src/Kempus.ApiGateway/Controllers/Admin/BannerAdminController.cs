﻿using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Models.Admin.Banner;
using Kempus.Models.Admin.Review;
using Kempus.Services.Admin.Banner;
using Kempus.Services.Common.HttpsContext;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Kempus.ApiGateway.Controllers.Admin
{
  /// <summary>
  /// Banner Management Controller
  /// </summary>
  [Route("api/admin/banners")]
  public class BannerAdminController : BaseAdminController
  {
    private readonly IHttpContextService _httpContextService;
    private readonly IBannerAdminService _bannerAdminService;

    public BannerAdminController(
      IBannerAdminService bannerAdminService,
      IHttpContextService httpContextService
    )
    {
      _bannerAdminService = bannerAdminService;
      _httpContextService = httpContextService;
    }

    /// <summary>
    /// Search Review
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="sortedField"></param>
    /// <param name="isDescending"></param>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("search")]
    public PagingResult<BannerListViewModel> Search(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      [FromBody] BannerSearchRequestDto model)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.ForumView, PermissionId.BannerContentView))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      var filterModel = new BannerCmsFilterModel()
      {
        Id = model.Id,
        Keyword = model.Keyword,
        Status = model.Status,
        SchoolId = model.SchoolId,
      };
      return _bannerAdminService.Search(filterModel, pagingModel, true);
    }

    /// <summary>
    /// Create Banner
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost]
    public object Create([FromForm] BannerCreateRequestDto model)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.ForumCreateEdit, PermissionId.BannerContentCreateEdit))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      var creatorId = _httpContextService.GetCurrentUserId();
      var input = new BannerInputModel()
      {
        SchoolId = model.SchoolId,
        WebBanner = model.WebBanner,
        MobileBanner = model.MobileBanner,
        Url = model.Url,
        UrlType = model.UrlType,
        Status = model.Status,
        CreatedBy = creatorId
      };

      return _bannerAdminService.Create(input);
    }

    /// <summary>
    /// Get Banner Detail
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id}")]
    public object GetDetail([Required] Guid id)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.ForumView, PermissionId.BannerContentView))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      return _bannerAdminService.GetDetail(id);
    }

    /// <summary>
    /// Update Banner
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPut]
    public object Update([FromForm] BannerEditRequestDto request)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.ForumCreateEdit, PermissionId.BannerContentCreateEdit))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      var editorId = _httpContextService.GetCurrentUserId();
      var input = new BannerInputModel()
      {
        Id = request.Id,
        WebBanner = request.WebBanner,
        MobileBanner = request.MobileBanner,
        Url = request.Url,
        UrlType = request.UrlType,
        Status = request.Status,
        UpdatedBy = editorId
      };

      return _bannerAdminService.Update(input);
    }
  }
}

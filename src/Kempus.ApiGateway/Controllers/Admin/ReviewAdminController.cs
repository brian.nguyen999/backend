﻿using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Models.Admin.Review;
using Kempus.Services.Admin.Review;
using Kempus.Services.Common.HttpsContext;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Kempus.ApiGateway.Controllers.Admin
{
  /// <summary>
  /// Course Review Management Controller
  /// </summary>
  [Route("api/admin/reviews")]
  public class ReviewAdminController : BaseAdminController
  {
    private readonly IHttpContextService _httpContextService;
    private readonly IReviewAdminService _reviewAdminService;

    public ReviewAdminController(
      IReviewAdminService reviewAdminService,
      IHttpContextService httpContextService
    )
    {
      _reviewAdminService = reviewAdminService;
      _httpContextService = httpContextService;
    }

    /// <summary>
    /// Search Review
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="sortedField"></param>
    /// <param name="isDescending"></param>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("search")]
    public PagingResult<ReviewListViewModel> Search(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      [FromBody] ReviewSearchRequestDto model)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.CourseReviewsView))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      var filterModel = new ReviewFilterModel()
      {
        Id = model.Id,
        Keyword = model.Keyword,
        Status = model.Status,
        SchoolId = model.SchoolId,
      };
      return _reviewAdminService.Search(filterModel, pagingModel, true);
    }

    /// <summary>
    /// Get Review Detail
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id}")]
    public ReviewDetailModel GetDetail([Required] Guid id)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.CourseReviewsView))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      return _reviewAdminService.GetDetail(id);
    }

    /// <summary>
    /// Update Review
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPut]
    public object Update(ReviewEditRequestDto request)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.CourseReviewsCreateEdit))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      var editorId = _httpContextService.GetCurrentUserId();
      var input = new ReviewInputModel()
      {
        Id = request.Id,
        Status = request.Status,
        UpdatedBy = editorId
      };

      return _reviewAdminService.Update(input);
    }
  }
}

﻿using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Models.Admin.Major;
using Kempus.Services.Admin.Major;
using Kempus.Services.Admin.Topic;
using Kempus.Services.Common.HttpsContext;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Kempus.ApiGateway.Controllers.Admin
{
  /// <summary>
  /// MajorAdminController
  /// </summary>
  [Route("api/admin/majors")]
  public class MajorAdminController : BaseAdminController
  {
    private readonly IHttpContextService _httpContextService;
    private readonly IMajorAdminService _majorService;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="httpContextService"></param>
    /// <param name="MajorService"></param>
    public MajorAdminController(
      IHttpContextService httpContextService,
      IMajorAdminService MajorService
    )
    {
      _httpContextService = httpContextService;
      _majorService = MajorService;
    }

    /// <summary>
    /// Search Major
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="sortedField"></param>
    /// <param name="isDescending"></param>
    /// <param name="filterModel"></param>
    /// <returns></returns>
    [HttpPost("search")]
    public object Search(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      [FromBody] MajorFilterModel filterModel)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.SchoolDataView))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      return _majorService.Search(filterModel, pagingModel, false);
    }

    /// <summary>
    /// Create Major
    /// </summary>
    /// <param name="model</param>
    /// <returns></returns>
    [HttpPost]
    public object Create(MajorInputModel model)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.SchoolDataCreateEdit))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      model.CreatedBy = _httpContextService.GetCurrentUserId();
      return _majorService.Create(model);
    }

    /// <summary>
    /// Update Major
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPut]
    public void Update(MajorUpdateInputModel model
    )
    {
      //if (!_httpContextService.HasPermissions(PermissionId.SchoolDataCreateEdit))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      model.UpdatedBy = _httpContextService.GetCurrentUserId();
      _majorService.Update(model);
    }

    /// <summary>
    /// Get Detail Major
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public object GetDetail(Guid id)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.SchoolDataView))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      return _majorService.GetDetail(id);
    }
  }
}

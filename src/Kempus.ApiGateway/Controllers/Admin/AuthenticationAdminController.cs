﻿using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Models.Public.Authentication;
using Kempus.Models.Public.Authentication.Request;
using Kempus.Models.Public.User.Request;
using Kempus.Services.Common.HttpsContext;
using Kempus.Services.Public.Authentication;
using Kempus.Services.Public.Otp;
using Kempus.Services.Public.User;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel.DataAnnotations;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.ApiGateway.Controllers.Admin
{
  [Route("api/admin/authentication")]
  public class AuthenticationAdminController : BaseAdminController
  {
    private readonly IHttpContextService _httpContextService;
    private readonly IAuthenticationService _authenticationService;
    private readonly IUserService _userService;
    private readonly IOtpService _otpService;

    public AuthenticationAdminController(
      IHttpContextService httpContextService,
      IAuthenticationService authenticationService,
      IUserService userService,
      IOtpService otpService)
    {
      _httpContextService = httpContextService;
      _authenticationService = authenticationService;
      _userService = userService;
      _otpService = otpService;
    }

    [AllowAnonymous]
    [HttpPost("login")]
    [SwaggerOperation(Summary = "Login")]
    public async Task<IActionResult> Login([FromBody] LoginRequestDto request)
    {
      var input = new LoginInputModel()
      {
        Username = request.Username,
        Password = request.Password,
      };

      // Check User login
      var loginSuccess = await _userService.SigninAsync(input);
      if (!loginSuccess)
      {
        //var faildLoginCount = await _userService.HandleSigninFailedAsync(request.Username);
        throw new BadRequestException(ErrorMessages.User_FailedLogin);
      }

      else
      {
        //var userId = await _userService.HandleSigninSuccessAsync(request.Username);

        // Generate JWT
        var userTypes = new List<byte>() { (byte)UserType.Admin };

        return Ok(await _authenticationService.GetAuthenticationResultAsync(request, userTypes));
      }
    }

    [HttpPost("logout")]
    [SwaggerOperation(Summary = "Logout")]
    public async Task Logout([FromBody] RefreshAuthenticationRequestDto model)
    {
      await _authenticationService.LogoutAsync(_httpContextService.GetCurrentUserId(), model.RefreshToken);
    }

    [HttpPost("refresh-authentication-result")]
    [AllowAnonymous]
    [SwaggerOperation(Summary = "GetNewAuthenticationResult")]
    public async Task<IActionResult> GetNewAuthenticationResult([FromBody] RefreshAuthenticationRequestDto model)
    {
      return Ok(await _authenticationService.RefreshAuthenticationResultAsync(model.RefreshToken));
    }

    /// <summary>
    /// Verify Email
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpPost("verify-email")]
    public async Task<object> VerifyEmail([FromBody] VerifyEmailRequestDto request)
    {
      var objectOtp = new OtpRequestDto()
      {
        ObjectType = (byte)OtpType.Email,
        ObjectId = request.Email,
        OtpCode = request.OtpCode
      };

      return await _otpService.CheckOtpValidAsync(objectOtp);
    }

    /// <summary>
    /// Resend Otp
    /// </summary>
    /// <param name="email"></param>
    /// <returns></returns>
    /// <exception cref="BadRequestException"></exception>
    [AllowAnonymous]
    [HttpPost("resend-otp")]
    public async Task<object> ResendOtp([FromQuery(Name = "email")][Required] string email)
    {
      var objectOtp = new OtpRequestDto()
      {
        ObjectType = (byte)OtpType.Email,
        ObjectId = email,
      };

      var otpEntity = await _otpService.GetByIdAndTypeAsync(objectOtp);
      if (otpEntity != null)
      {
        if (_otpService.CheckOtpIsExpired(otpEntity.CreatedDate))
        {
          await _otpService.DeleteOtpAsync(objectOtp);
        }
        else
        {
          throw new BadRequestException(ErrorMessages.User_OtpStillValid);
        }
      }

      var newOtp = _otpService.Create(objectOtp);
      return new
      {
        ExpirationTime = newOtp.ExpirationTime
      };
    }

    /// <summary>
    /// Reset password
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpPost("reset-password-by-email")]
    public async Task<object> ResetPasswordByEmailAsync([FromBody] ResetPasswordRequestDto dto)
    {
      dto.UserTypes = new List<byte>() { (byte)UserType.Admin };
      return await _userService.ResetPasswordByEmailAsync(dto);
    }
  }
}
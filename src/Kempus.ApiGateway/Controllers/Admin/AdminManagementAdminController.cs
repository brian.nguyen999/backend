﻿using Kempus.Core.Constants;
using Kempus.Core.Models;
using Kempus.Models.Public.User;
using Kempus.Models.Public.User.Request;
using Kempus.Services.Common.HttpsContext;
using Kempus.Services.Public.User;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Kempus.Core.Errors;
using ForbiddenException = Amazon.ApiGatewayManagementApi.Model.ForbiddenException;

namespace Kempus.ApiGateway.Controllers.Admin
{
  /// <summary>
  /// Admin Management Controller
  /// </summary>
  [Route("api/admin/admins")]
  public class AdminManagementAdminController : BaseAdminController
  {
    private readonly IHttpContextService _httpContextService;
    private readonly IUserService _userService;

    public AdminManagementAdminController(
      IHttpContextService httpContextService,
      IUserService userService
    )
    {
      _httpContextService = httpContextService;
      _userService = userService;
    }

    /// <summary>
    /// Create new Admin
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<Guid> Create([FromBody] AdminCreateRequestDto request)
    {
      if (!_httpContextService.HasPermissions(PermissionId.AccountCreateEdit, PermissionId.AdminCreateEdit))
        throw new ForbiddenException(ErrorMessages.NoPermission);
      var creatorId = _httpContextService.GetCurrentUserId();
      var input = new AdminInputModel()
      {
        Name = request.Name,
        Email = request.Email,
        RoleId = request.RoleId,
        Status = request.Status,
        CreatedBy = creatorId
      };

      return await _userService.CreateAdminAsync(input);
    }

    [HttpPost("search")]
    public PagingResult<AdminListViewModel> Search(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      [FromBody] AdminSearchRequestDto model)
    {
      if (!_httpContextService.HasPermissions(PermissionId.AccountView, PermissionId.AdminView))
        throw new ForbiddenException(ErrorMessages.NoPermission);
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      var filterModel = new AdminFilterModel()
      {
        Id = model.Id,
        Keyword = model.Keyword,
        Status = model.Status,
        RoleIds = model.RoleIds,
      };
      return _userService.SearchAdmin(filterModel, pagingModel, true);
    }

    /// <summary>
    /// Get Admin Detail
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id}")]
    public AdminDetailModel GetAdminDetail([Required] Guid id)
    {
      if (!_httpContextService.HasPermissions(PermissionId.AccountView, PermissionId.AdminView))
        throw new ForbiddenException(ErrorMessages.NoPermission);

      return _userService.GetAdminDetail(id);
    }

    /// <summary>
    /// Update Admin
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPut]
    public async Task<object> Update(AdminEditRequestDto request)
    {
      if (!_httpContextService.HasPermissions(PermissionId.AccountCreateEdit, PermissionId.AdminCreateEdit))
        throw new ForbiddenException(ErrorMessages.NoPermission);


      var editorId = _httpContextService.GetCurrentUserId();
      var input = new AdminInputModel()
      {
        Id = request.Id,
        Name = request.Name,
        Email = request.Email,
        RoleId = request.RoleId,
        Status = request.Status,
        UpdatedBy = editorId
      };

      return await _userService.UpdateAdminAsync(input);
    }

    [HttpPost("reset-password")]
    public async Task ResetPassword([FromQuery] Guid userId)
    {
      if (!_httpContextService.HasPermissions(PermissionId.AccountCreateEdit, PermissionId.AdminCreateEdit))
        throw new ForbiddenException(ErrorMessages.NoPermission);
      await _userService.ResetPasswordAsync(userId);
    }
  }
}

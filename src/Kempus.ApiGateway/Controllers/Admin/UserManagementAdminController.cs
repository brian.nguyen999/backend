﻿using Kempus.Core.Models;
using Kempus.Models.Public.User.Request;
using Kempus.Models.Public.User;
using Microsoft.AspNetCore.Mvc;
using Kempus.Services.Common.HttpsContext;
using Kempus.Services.Public.User;
using System.ComponentModel.DataAnnotations;
using Kempus.Models.Public.User.Response;
using Kempus.Core.Constants;
using Kempus.Core.Errors;

namespace Kempus.ApiGateway.Controllers.Admin
{
  /// <summary>
  /// User Management Controller
  /// </summary>
  [Route("api/admin/users")]
  public class UserManagementAdminController : BaseAdminController
  {
    private readonly IHttpContextService _httpContextService;
    private readonly IUserService _userService;

    public UserManagementAdminController(
      IHttpContextService httpContextService,
      IUserService userService
    )
    {
      _httpContextService = httpContextService;
      _userService = userService;
    }

    /// <summary>
    /// Search User
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="sortedField"></param>
    /// <param name="isDescending"></param>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("search")]
    public PagingResult<UserListViewModel> Search(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      [FromBody] UserSearchRequestDto model)
    {
      if (!_httpContextService.HasPermissions(PermissionId.AccountView, PermissionId.UserView))
        throw new ForbiddenException(ErrorMessages.NoPermission);
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      var filterModel = new UserFilterModel()
      {
        Id = model.Id,
        Keyword = model.Keyword,
        Status = model.Status,
        SchoolId = model.SchoolId,
        Group = model.Group
      };
      return _userService.SearchUser(filterModel, pagingModel, true);
    }

    /// <summary>
    /// Get Admin Detail
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id}")]
    public UserDetailModel GetUserDetail([Required] Guid id)
    {
      if (!_httpContextService.HasPermissions(PermissionId.AccountView, PermissionId.UserView))
        throw new ForbiddenException(ErrorMessages.NoPermission);
      return _userService.GetUserDetail(id);
    }

    /// <summary>
    /// Update User
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPut]
    public object Update(UserEditRequestDto request)
    {
      if (!_httpContextService.HasPermissions(PermissionId.AccountView, PermissionId.UserCreateEdit))
        throw new ForbiddenException(ErrorMessages.NoPermission);
      var editorId = _httpContextService.GetCurrentUserId();
      var input = new CmsUserInputModel()
      {
        Id = request.Id,
        Memory = request.Memory,
        Status = request.Status,
        UpdatedBy = editorId,
        UserType = request.Group
      };

      return _userService.UpdateUser(input);
    }

    /// <summary>
    /// Create new User
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPost]
    public object Create(UserCreateRequestDto request)
    {
      if (!_httpContextService.HasPermissions(PermissionId.AccountView, PermissionId.UserCreateEdit))
        throw new ForbiddenException(ErrorMessages.NoPermission);
      var creatorId = _httpContextService.GetCurrentUserId();
      var input = new CmsUserInputModel()
      {
        Username = request.Username,
        Email = request.Email,
        Password = request.Password,
        SchoolId = request.SchoolId,
        ClassYear = request.ClassYear,
        DegreeId = request.DegreeId,
        MajorId = request.MajorId,
        UserType = request.Group,
        Status = request.Status,
        CreatedBy = creatorId
      };

      return _userService.CreateUser(input);
    }
  }
}

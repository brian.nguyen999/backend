﻿using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Entities;
using Kempus.Models.Admin.Post;
using Kempus.Services.Admin.Post;
using Kempus.Services.Common.HttpsContext;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Kempus.ApiGateway.Controllers.Admin
{
  /// <summary>
  /// PostAdminController
  /// </summary>
  [Route("api/admin/posts")]
  public class PostAdminController : BaseAdminController
  {
    public readonly IPostAdminService _postAdminService;
    private readonly IHttpContextService _httpContextService;

    /// <summary>
    /// PostAdminController
    /// </summary>
    /// <param name="postAdminService"></param>
    /// <param name="httpContextService"></param>
    public PostAdminController(
      IPostAdminService postAdminService,
      IHttpContextService httpContextService
      )
    {
      _postAdminService = postAdminService;
      _httpContextService = httpContextService;
    }

    /// <summary>
    /// Search post
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("search")]
    public object Search(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      [FromBody] PostFilterModel filterModel
      )
    {
      //if (!_httpContextService.HasPermissions(PermissionId.ForumView, PermissionId.PostsView))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      var currentUserId = _httpContextService.GetCurrentUserId();
      sortedField = nameof(CourseReviewEntity.CreatedDate);
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      return _postAdminService.Search(filterModel, pagingModel, false, currentUserId);
    }

    /// <summary>
    /// Update post
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPut]
    public void Update([FromBody] PostInputModel input)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.ForumCreateEdit, PermissionId.PostsCreateEdit))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      var currentUserId = _httpContextService.GetCurrentUserId();
      input.UpdatedBy = currentUserId;
      _postAdminService.Update(input);
    }

    /// <summary>
    /// Get detail post
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id}")]
    public object GetDetail([Required] Guid id)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.ForumView, PermissionId.PostsView))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      var currentUserId = _httpContextService.GetCurrentUserId();
      return _postAdminService.GetDetail(id, currentUserId);
    }
  }
}

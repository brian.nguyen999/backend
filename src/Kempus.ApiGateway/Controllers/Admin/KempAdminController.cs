﻿using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Models.Admin.Kemp;
using Kempus.Models.Common;
using Kempus.Services.Admin.Kemp;
using Kempus.Services.Common.HttpsContext;
using Microsoft.AspNetCore.Mvc;

namespace Kempus.ApiGateway.Controllers.Admin
{
  [Route("api/admin/kemps")]
  public class KempAdminController : BaseAdminController
  {
    private readonly IHttpContextService _httpContextService;
    private readonly IKempAdminService _kempAdminService;

    public KempAdminController(
      IHttpContextService httpContextService, 
      IKempAdminService kempAdminService)
    {
      _httpContextService = httpContextService;
      _kempAdminService = kempAdminService;
    }

    /// <summary>
    /// Search Kemp transaction
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="sortedField"></param>
    /// <param name="isDescending"></param>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("search")]
    public PagingResult<KempListViewModel> Search(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      [FromBody] KempSearchRequestDto model)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.TransactionView))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      var filterModel = new KempFilterModel()
      {
        Id = model.Id,
        Keyword = model.Keyword,
        Type = model.Type,
        SchoolId = model.SchoolId,
      };
      return _kempAdminService.Search(filterModel, pagingModel, true);
    }

    /// <summary>
    /// Manually Create Kemp transaction
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPost]
    public object Create(KempTransactionCreateRequestDto request)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.TransactionCreateEdit))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      var creatorId = _httpContextService.GetCurrentUserId();
      var input = new KempTransactionInputModel()
      {
        Action = request.Action,
        Amount = request.Amount,
        TransactionType = request.TransactionType,
        SchoolId = request.SchoolId,
        UserIds = request.UserIds,
        IsAdminCreated = true,
        CreatorId = creatorId,
        IsSelectAllUser = request.IsSelectAllUser,
      };

      return _kempAdminService.Create(input);
    }
  }
}

﻿using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Models.Admin.DepartmentProgram;
using Kempus.Models.Admin.DepartmentProgram.Request;
using Kempus.Services.Admin.DepartmentProgram;
using Kempus.Services.Common.HttpsContext;
using Microsoft.AspNetCore.Mvc;

namespace Kempus.ApiGateway.Controllers.Admin
{
  /// <summary>
  /// DepartmentProgram Admin controller
  /// </summary>
  [Route("api/admin/department-programs")]
  public class DepartmentProgramAdminController : BaseAdminController
  {
    private readonly IHttpContextService _httpContextService;
    private readonly IDepartmentProgramAdminService _departmentProgramAdminService;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="httpContextService"></param>
    /// <param name="departmentProgramAdminService"></param>
    public DepartmentProgramAdminController(
      IHttpContextService httpContextService,
      IDepartmentProgramAdminService departmentProgramAdminService)
    {
      _httpContextService = httpContextService;
      _departmentProgramAdminService = departmentProgramAdminService;
    }

    /// <summary>
    /// Create DepartmentProgram
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPost]
    public object Create(DepartmentProgramCreateRequestDto dto)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.SchoolDataCreateEdit))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      var model = new DepartmentProgramInputCmsModel
      {
        SchoolId = dto.SchoolId,
        Name = dto.Name,
        CreatorId = _httpContextService.GetCurrentUserId()
      };

      return _departmentProgramAdminService.Create(model);
    }

    /// <summary>
    /// Update DepartmentProgram
    /// </summary>
    /// <param name="dto"></param>
    [HttpPut]
    public void Update(DepartmentProgramUpdateRequestDto dto)
    {
      var model = new DepartmentProgramInputCmsModel
      {
        Id = dto.Id,
        Name = dto.Name,
        UpdaterId = _httpContextService.GetCurrentUserId()
      };

      _departmentProgramAdminService.Update(model);
    }
  }
}

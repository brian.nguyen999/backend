using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Models.Public.Poll;
using Kempus.Models.Public.Role;
using Kempus.Models.Public.Role.Request;
using Kempus.Services.Admin.Role;
using Kempus.Services.Common.HttpsContext;
using Microsoft.AspNetCore.Mvc;

namespace Kempus.ApiGateway.Controllers.Admin
{
  /// <summary>
  /// RoleAdminController
  /// </summary>
  [Route("api/admin/roles")]
  public class RoleAdminController : BaseAdminController
  {
    private readonly IRoleService _roleService;
    private readonly IHttpContextService _httpContextService;

    /// <summary>
    /// RoleAdminController
    /// </summary>
    /// <param name="roleService"></param>
    public RoleAdminController(
      IRoleService roleService,
      IHttpContextService httpContextService
      )
    {
      _roleService = roleService;
      _httpContextService = httpContextService;
    }

    /// <summary>
    /// Search role
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("search")]
    public object Search(
      [FromBody] SearchRoleRequestDto model
      )
    {
      //if (!_httpContextService.HasPermissions(PermissionId.AccountView, PermissionId.AdminSettingsView))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      var pagingModel = Pageable.GetPageable(model.PageSize, model.PageIndex, model.SortedField, model.IsDescending);
      var filterModel = new RoleFilterModel
      {
        RoleId = model.RoleId,
        Keyword = model.Keyword,
        IsActive = model.IsActive
      };

      return _roleService.Search(filterModel, pagingModel, true);
    }

    /// <summary>
    /// Get role detail
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public object GetDetail(Guid id)
    {
      if (!_httpContextService.HasPermissions(PermissionId.AccountView, PermissionId.AdminSettingsView))
        throw new ForbiddenException(ErrorMessages.NoPermission);
      return _roleService.GetDetail(id);
    }

    /// <summary>
    /// Create role
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost]
    public Guid Create(
      [FromBody] RoleInputModel model)
    {
      if (!_httpContextService.HasPermissions(PermissionId.AccountCreateEdit, PermissionId.AdminSettingsCreateEdit))
        throw new ForbiddenException(ErrorMessages.NoPermission);
      return _roleService.Create(model);
    }

    /// <summary>
    /// Update role
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPut]
    public void Update(
      [FromBody] RoleUpdateInputModel model
      )
    {
      if (!_httpContextService.HasPermissions(PermissionId.AccountCreateEdit, PermissionId.AdminSettingsCreateEdit))
        throw new ForbiddenException(ErrorMessages.NoPermission);
      _roleService.Update(model);
    }
  }
}
﻿using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Models.Admin.Flag;
using Kempus.Models.Public.School;
using Kempus.Services.Admin.Flag;
using Kempus.Services.Common.HttpsContext;
using Microsoft.AspNetCore.Mvc;

namespace Kempus.ApiGateway.Controllers.Admin
{
  /// <summary>
  /// Flag Management Admin Controller
  /// </summary>
  [Route("api/admin/flags")]
  public class FlagAdminController : BaseAdminController
  {
    private readonly IHttpContextService _httpContextService;
    private readonly IFlagAdminService _flagAdminService;

    public FlagAdminController(IHttpContextService httpContextService, IFlagAdminService flagAdminService)
    {
      _httpContextService = httpContextService;
      _flagAdminService = flagAdminService;
    }

    /// <summary>
    /// Flag Search
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="sortedField"></param>
    /// <param name="isDescending"></param>
    /// <param name="filterModel"></param>
    /// <returns></returns>
    [HttpPost("search")]
    public PagingResult<FlagListViewModel> Search(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      [FromBody] FlagFilterModel filterModel)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.FlagsView))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      return _flagAdminService.Search(filterModel, pagingModel, false);
    }

    /// <summary>
    /// Get Total Flags are Pending
    /// </summary>
    /// <returns></returns>
    [HttpGet("total-pending")]
    public object GetTotalFlagPending()
    {
      return _flagAdminService.GetTotalFlagPending();
    }

    /// <summary>
    /// Update Flag
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPut]
    public object Update(FlagEditRequestDto request)
    {
      //if (!_httpContextService.HasPermissions(PermissionId.FlagsCreateEdit))
      //  throw new ForbiddenException(ErrorMessages.NoPermission);

      var editorId = _httpContextService.GetCurrentUserId();
      var input = new FlagCmsInputModel()
      {
        Id = request.Id,
        Status = request.Status,
        UpdatedBy = editorId
      };

      return _flagAdminService.Update(input);
    }
  }
}

﻿using Kempus.Models.Admin.Instructor;
using Kempus.Models.Admin.Instructor.Request;
using Kempus.Services.Admin.Instructor;
using Kempus.Services.Common.HttpsContext;
using Microsoft.AspNetCore.Mvc;

namespace Kempus.ApiGateway.Controllers.Admin
{
  /// <summary>
  /// Instructor Admin controller
  /// </summary>
  [Route("api/admin/instructors")]
  public class InstructorAdminController : BaseAdminController
  {
    private readonly IHttpContextService _httpContextService;
    private readonly IInstructorAdminService _instructorAdminService;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="httpContextService"></param>
    /// <param name="instructorAdminService"></param>
    public InstructorAdminController(
      IHttpContextService httpContextService,
      IInstructorAdminService instructorAdminService)
    {
      _httpContextService = httpContextService;
      _instructorAdminService = instructorAdminService;
    }

    /// <summary>
    /// Create Instructor
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPost]
    public object Create(InstructorCreateRequestDto dto)
    {
      var model = new InstructorInputCmsModel
      {
        SchoolId = dto.SchoolId,
        Name = dto.Name,
        CreatorId = _httpContextService.GetCurrentUserId()
      };

      return _instructorAdminService.Create(model);
    }

    /// <summary>
    /// Update Instructor
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPut]
    public void Update(InstructorUpdateRequestDto dto)
    {
      var model = new InstructorInputCmsModel
      {
        Id = dto.Id,
        Name = dto.Name,
        UpdaterId = _httpContextService.GetCurrentUserId()
      };

      _instructorAdminService.Update(model);
    }
  }
}

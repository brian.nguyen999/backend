﻿using Amazon.Runtime;
using Kempus.Core.Constants;
using Kempus.Core.Models;
using Kempus.Entities;
using Kempus.Models.Public.Course;
using Kempus.Models.Public.Major;
using Kempus.Services.Public.Major;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Kempus.ApiGateway.Controllers
{
  /// <summary>
  /// MajorController
  /// </summary>
  [Route("api/majors")]
  [ApiController]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.PublicApi)]
  public class MajorController : ControllerBase
  {
    private readonly IMajorService _majorService;

    /// <summary>
    /// MajorController
    /// </summary>
    /// <param name="majorService"></param>
    public MajorController(
      IMajorService majorService
      )
    {
      _majorService = majorService;
    }

    /// <summary>
    /// Search
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="sortedField"></param>
    /// <param name="isDescending"></param>
    /// <param name="filterModel"></param>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpPost("search")]
    public object Search(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      [FromBody] MajorFilterModel filterModel)
    {

      isDescending = sortedField != null ? isDescending : false;
      sortedField = sortedField ?? nameof(MajorEntity.Name);
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      filterModel.IsActivated = true;
      return _majorService.Search(filterModel, pagingModel, useMasterDb: true);
    }
  }
}

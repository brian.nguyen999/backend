﻿using Kempus.Core.Constants;
using Kempus.Core.Models;
using Kempus.Models.Public.Banner;
using Kempus.Models.Public.Course;
using Kempus.Services.Common.HttpsContext;
using Kempus.Services.Public.Banner;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using static Kempus.Core.Enums.CoreEnums;

namespace Kempus.ApiGateway.Controllers
{
  /// <summary>
  /// Banner Controller
  /// </summary>
  [Route("api/banners")]
  [ApiController]
  [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.PublicApi)]
  public class BannerController : Controller
  {
    private readonly IHttpContextService _httpContextService;
    private readonly IBannerService _bannerService;

    /// <summary>
    /// BannerController
    /// </summary>
    /// <param name="bannerService"></param>
    /// <param name="httpContextService"></param>
    public BannerController(
      IBannerService bannerService,
      IHttpContextService httpContextService
    )
    {
      _bannerService = bannerService;
      _httpContextService = httpContextService;
    }

    /// <summary>
    /// Search
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="sortedField"></param>
    /// <param name="isDescending"></param>
    /// <returns></returns>
    [HttpPost("search")]
    public object Search(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending)
    {
      var currentSchoolId = _httpContextService.GetCurrentSchoolId();
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      var filterModel = new BannerFilterModel
      {
        SchoolId = currentSchoolId,
        Status = (byte)BannerStatus.Active,
      };

      return _bannerService.Search(filterModel, pagingModel, false);
    }
  }
}

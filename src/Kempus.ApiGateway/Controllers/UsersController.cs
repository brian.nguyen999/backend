using Kempus.Core.Constants;
using Kempus.Core.Errors;
using Kempus.Core.Models;
using Kempus.Core.Models.Configuration;
using Kempus.Core.Utils;
using Kempus.Models.Admin.Kemp;
using Kempus.Models.Public.Poll;
using Kempus.Models.Public.Post;
using Kempus.Models.Public.User;
using Kempus.Models.Public.User.Request;
using Kempus.Models.Public.User.Response;
using Kempus.Services.Common.HttpsContext;
using Kempus.Services.Public.Notification;
using Kempus.Services.Public.Otp;
using Kempus.Services.Public.Poll;
using Kempus.Services.Public.Post;
using Kempus.Services.Public.School;
using Kempus.Services.Public.SchoolLifeTimeAnalytic;
using Kempus.Services.Public.User;
using Kempus.Services.Public.WaitList;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Net.Mail;
using Kempus.Services.Public.KempTransaction;
using static Kempus.Core.Enums.CoreEnums;
using Microsoft.AspNetCore.Authorization;

namespace Kempus.ApiGateway.Controllers
{
  [Route("api/users")]
  [ApiController]
  [ApiExplorerSettings(GroupName = CoreConstants.SwaggerGroupApi.PublicApi)]
  public class UsersController : ControllerBase
  {
    private readonly IUserService _userService;
    private readonly IOtpService _otpService;
    private readonly IEmailNotificationService _notificationService;
    private readonly IHttpContextService _httpContextService;
    private readonly ISchoolService _schoolService;
    private readonly IWaitListService _waitListService;
    private readonly UserConfig _userConfig;
    private readonly IPollService _pollService;
    private readonly IPostService _postService;
    private readonly ISchoolLifeTimeAnalyticService _schoolLifeTimeAnalyticService;
    private readonly IKempTransactionService _kempTransactionService;

    /// <summary>
    /// UserController
    /// </summary>
    /// <param name="userService"></param>
    /// <param name="otpService"></param>
    /// <param name="notificationService"></param>
    /// <param name="httpContextService"></param>
    /// <param name="schoolService"></param>
    /// <param name="userConfig"></param>
    /// <param name="waitListService"></param>
    /// <param name="pollService"></param>
    /// <param name="postService"></param>
    public UsersController(
      IUserService userService,
      IOtpService otpService,
      IEmailNotificationService notificationService,
      IHttpContextService httpContextService,
      ISchoolService schoolService,
      UserConfig userConfig, IWaitListService waitListService,
      IPollService pollService,
      IPostService postService,
      ISchoolLifeTimeAnalyticService schoolLifeTimeAnalyticService,
      IKempTransactionService kempTransactionService
      )
    {
      _userService = userService;
      _otpService = otpService;
      _notificationService = notificationService;
      _httpContextService = httpContextService;
      _schoolService = schoolService;
      _userConfig = userConfig;
      _waitListService = waitListService;
      _pollService = pollService;
      _postService = postService;
      _schoolLifeTimeAnalyticService = schoolLifeTimeAnalyticService;
      _kempTransactionService = kempTransactionService;
    }

    /// <summary>
    /// SignUp
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    /// <exception cref="BadRequestException"></exception>
    [HttpPost("sign-up")]
    public void SignUp([FromBody] SignupRequestDto request)
    {
      var input = new UserInputModel()
      {
        ReferralCode = request.ReferralCode,
        Email = request.Email,
        UserType = (byte)UserType.User,
        IsRegister = false
      };

      _userService.ValidateUserInput(input);

      var newObjectOtp = new OtpRequestDto()
      {
        ObjectType = (byte)OtpType.Email,
        ObjectId = request.Email
      };

      _otpService.Create(newObjectOtp);
    }

    /// <summary>
    /// VerifyEmail
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPost("verify-email")]
    public async Task<object> VerifyEmail([FromBody] VerifyEmailRequestDto request)
    {
      // Check School
      MailAddress address = new MailAddress(request.Email);
      string host = address.Host;
      var currentSchool = await _schoolService.GetSchoolByDomain(host);
      if (currentSchool == null)
      {
        throw new BadRequestException(ErrorMessages.User_DomainIsNotSupported);
      }

      var objectOtp = new OtpRequestDto()
      {
        ObjectType = (byte)OtpType.Email,
        ObjectId = request.Email,
        OtpCode = request.OtpCode
      };

      var verifyResult = await _otpService.CheckOtpValidAsync(objectOtp);
      if (!currentSchool.IsActivated)
      {
        _waitListService.InsertIfNotExist(request.Email, currentSchool.Guid);

        // Delete old OTP
        await _otpService.DeleteOtpAsync(objectOtp);

        var totalWaitList = _schoolLifeTimeAnalyticService.UpdateNumberOfWaitList(currentSchool.Guid);

        return new UserSignUpResponseDto
        {
          IsInWaitList = true,
          SchoolName = currentSchool.Name,
          TotalWaitList = totalWaitList,
          ReferralCode = StringHelper.GenerateReferralCodeFromEmail(request.Email),
          IsVerified = verifyResult
        };
      }

      return new UserSignUpResponseDto
      {
        IsInWaitList = false,
        SchoolName = currentSchool.Name,
        TotalWaitList = 0,
        ReferralCode = StringHelper.GenerateReferralCodeFromEmail(request.Email),
        IsVerified = verifyResult
      };
    }

    /// <summary>
    /// Resend Otp
    /// </summary>
    /// <param name="email"></param>
    /// <returns></returns>
    /// <exception cref="BadRequestException"></exception>
    [HttpPost("resend-otp")]
    public async Task<object> ResendOtp([FromQuery(Name = "email")][Required] string email)
    {
      var objectOtp = new OtpRequestDto()
      {
        ObjectType = (byte)OtpType.Email,
        ObjectId = email,
      };

      var otpEntity = await _otpService.GetByIdAndTypeAsync(objectOtp);
      if (otpEntity != null)
      {
        if (_otpService.CheckOtpIsExpired(otpEntity.CreatedDate))
        {
          await _otpService.DeleteOtpAsync(objectOtp);
        }
        else
        {
          throw new BadRequestException(ErrorMessages.User_OtpStillValid);
        }
      }

      var newOtp = _otpService.Create(objectOtp);
      return new
      {
        ExpirationTime = newOtp.ExpirationTime
      };
    }

    /// <summary>
    /// Register
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    /// <exception cref="BadRequestException"></exception>
    [HttpPost("register")]
    public async Task<object> Register([FromBody] RegisterRequestDto request)
    {
      var objectOtp = new OtpRequestDto()
      {
        ObjectType = (byte)OtpType.Email,
        ObjectId = request.Email,
        OtpCode = request.OtpCode
      };
      await _otpService.CheckOtpValidAsync(objectOtp);

      var input = new UserInputModel()
      {
        Email = request.Email,
        UserType = (byte)UserType.User,
        DegreeId = request.DegreeId,
        MajorId = request.MajorId,
        ClassYear = request.ClassYear,
        NickName = request.NickName,
        UserName = request.UserName,
        Password = request.Password,
        ConfirmPassword = request.ConfirmPassword,
        OtpCode = request.OtpCode,
        ReferralCode = request.ReferralCode,
        OtpDto = objectOtp,
        IsRegister = true
      };

      return await _userService.InsertAsync(input);
    }

    /// <summary>
    /// GenerateNickname
    /// </summary>
    /// <returns></returns>
    [HttpGet("generate-nickname")]
    public async Task<object> GenerateNickname()
    {
      return await _userService.CreateNickNameAsync();
    }

    /// <summary>
    /// CheckNicknameExist
    /// </summary>
    /// <param name="nickName"></param>
    /// <returns></returns>
    [HttpGet("{nickName}/check-nickname-exist")]
    public async Task<object> CheckNicknameExist([Required] string nickName)
    {
      return await _userService.CheckNickNameExistAsync(nickName);
    }

    /// <summary>
    /// Forgot password
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPost("forgot-password")]
    public async Task<IActionResult> ForgotPasswordAsync([FromBody] ForgotPasswordRequestDto dto)
    {
      dto.UserTypes = new List<byte>() { (byte)UserType.User, (byte)UserType.Ambassador };
      await _userService.ForgotPasswordAsync(dto);
      return Ok();
    }

    /// <summary>
    /// Change password
    /// </summary>
    /// <returns></returns>
    [HttpPost("change-password")]
    public async Task ChangePasswordAsync()
    {
      await _userService.ResetSitePasswordAsync(_httpContextService.GetCurrentUserId());
    }

    /// <summary>
    /// Reset password
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPost("reset-password-by-email")]
    public async Task<object> ResetPasswordByEmailAsync([FromBody] ResetPasswordRequestDto dto)
    {
      dto.UserTypes = new List<byte> () { (byte)UserType.User, (byte)UserType.Ambassador };
      return await _userService.ResetPasswordByEmailAsync(dto);
    }

    /// <summary>
    /// Get User profile details
    /// </summary>
    /// <returns></returns>
    [HttpGet("user-profile")]
    public object GetCurrentUserProfile()
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      return _userService.GetUserProfile(currentUserId);
    }

    /// <summary>
    /// Get User Referral Details
    /// </summary>
    /// <returns></returns>
    [HttpGet("user-referral-details")]
    public object GetUserRefferalDetails()
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      return _userService.GetUserReferralDetail(currentUserId);
    }

    /// <summary>
    /// Change NickName
    /// </summary>
    /// <returns></returns>
    [HttpPost("change-nickname")]
    public async Task<object> ChangeNickName()
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      return await _userService.UpdateNicknameAsync(currentUserId);
    }

    /// <summary>
    /// GetUserPolls
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="sortedField"></param>
    /// <param name="isDescending"></param>
    /// <returns></returns>
    [HttpPost("polls")]
    public object GetUserPolls(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending
      )
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      return _pollService.Search(new PollFilterModel() { UserId = currentUserId }, pagingModel, true, currentUserId);
    }

    /// <summary>
    /// GetUserPolls
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="sortedField"></param>
    /// <param name="isDescending"></param>
    /// <returns></returns>
    [HttpPost("posts")]
    public object GetUserPosts(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending,
      PostFilterModel filterModel
    )
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      filterModel.UserId = currentUserId;
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      return _postService.Search(filterModel, pagingModel, true, currentUserId);
    }

    /// <summary>
    /// UpdateProfile
    /// </summary>
    /// <returns></returns>
    [HttpPut("user-profile")]
    public async Task UpdateProfile(
      [FromBody] UpdateProfileRequestDto input)
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      await _userService.UpdateAsync(currentUserId, input);
    }

    /// <summary>
    /// Search Kemp transaction
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="sortedField"></param>
    /// <param name="isDescending"></param>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("kemp-transactions")]
    public object Search(
      [FromQuery(Name = "pageSize")] int? pageSize,
      [FromQuery(Name = "pageIndex")] int? pageIndex,
      [FromQuery(Name = "sortedField")] string? sortedField,
      [FromQuery(Name = "isDescending")] bool isDescending
      )
    {
      var currentUserId = _httpContextService.GetCurrentUserId();
      var currentSchoolId = _httpContextService.GetCurrentSchoolId();
      var pagingModel = Pageable.GetPageable(pageSize, pageIndex, sortedField, isDescending);
      var filterModel = new KempFilterModel()
      {
        SchoolId = currentSchoolId,
        UserId = currentUserId
      };

      return _kempTransactionService.Search(filterModel, pagingModel, true);
    }

    [AllowAnonymous]
    [HttpGet("check-otp-expired-time")]
    public async Task<object> CheckOtpExpiredTime([FromQuery][Required] string email)
    {
      var objectOtp = new OtpRequestDto()
      {
        ObjectType = (byte)OtpType.Email,
        ObjectId = email,
      };

      var entity = await _otpService.GetByIdAndTypeAsync(objectOtp);
      DateTime? result = entity != null ? entity.CreatedDate.AddMinutes(_userConfig.OtpValidityInMinutes) : null;
      return new { ExpiredTime = result };
    }
  }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Entities
{
  public class UserRoleEntity
  {
    [Key] public long Id { get; set; }
    public long RoleId { get; set; }
    public Guid UserId { get; set; }

  }
}
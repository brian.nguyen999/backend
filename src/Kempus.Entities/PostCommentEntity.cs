﻿using Kempus.Core.Models;
using Kempus.Core.Models.ModelContracts;
using Microsoft.EntityFrameworkCore;

namespace Kempus.Entities
{
  [Index(nameof(Guid), nameof(ParentId))]
  public class PostCommentEntity : Entity<long>, ICreationAudited<Guid>
  {
    public Guid? ParentId { get; set; }
    public Guid PostId { get; set; }
    public string Content { get; set; }
    public bool IsPublished { get; set; } = true;
    public byte Type { get; set; }
    public Guid CreatedBy { get; set; }
    public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
  }
}
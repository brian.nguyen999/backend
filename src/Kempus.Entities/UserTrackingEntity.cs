﻿using Kempus.Core.Models;
using Kempus.Core.Models.ModelContracts;

namespace Kempus.Entities
{
  public class UserTrackingEntity : Entity<long>, IHasCreatedDate
  {
    public string? ActionName { get; set; }

    public Guid? UserId { get; set; }

    public string? Page { get; set; }

    public string? Platform { get; set; }

    public Guid? ObjectId { get; set; }

    public string? ObjectType { get; set; }

    public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
  }
}

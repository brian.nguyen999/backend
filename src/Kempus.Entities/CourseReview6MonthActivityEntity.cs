﻿using Kempus.Core.Models;

namespace Kempus.Entities
{
  public class CourseReview6MonthActivityEntity : Entity<long>
  {
    public Guid CourseReviewId { get; set; }

    public Guid UserId { get; set; }

    public byte ActionId { get; set; }

    public Guid SchoolId { get; set; }

    public DateTime CreatedDate { get; set; }
  }
}

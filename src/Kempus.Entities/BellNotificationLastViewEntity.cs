﻿using System.ComponentModel.DataAnnotations;

namespace Kempus.Entities
{
  public class BellNotificationLastViewEntity
  {
    [Key]
    public Guid CreatedUser { get; set; }
    public DateTime LastViewDateTime { get; set; }
  }
}

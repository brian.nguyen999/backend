﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Entities
{
  public class UserReferralStatisticEntity
  {
    [Key]
    public Guid Guid { get; set; } = Guid.NewGuid();
    public Guid UserId { get; set; }
    public long NumberFriendsInvited { get; set; }
    public long NumberFirstReviewsWrote { get; set; }

    public virtual DateTime CreatedDate { get; set; } = DateTime.UtcNow;
    public virtual Guid? CreatedBy { get; set; }
    public virtual DateTime? UpdatedDate { get; set; }
    public virtual Guid? UpdatedBy { get; set; }
  }
}

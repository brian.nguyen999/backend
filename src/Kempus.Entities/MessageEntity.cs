﻿using Kempus.Core.Models.ModelContracts;

namespace Kempus.Entities
{
  public class MessageEntity : IHasCreatedDate
  {
    public Guid Guid { get; set; } = Guid.NewGuid();
    public Guid ConversationId { get; set; }
    public string? Content { get; set; }
    public DateTime CreatedDate { get; set; }
    public Guid CreatedUser { get; set; }
    public Guid SchoolId { get; set; }
  }
}

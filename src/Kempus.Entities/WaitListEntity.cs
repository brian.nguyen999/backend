﻿using Microsoft.EntityFrameworkCore;

namespace Kempus.Entities
{
  public class WaitListEntity
  {
    public Guid Guid { get; set; }

    public string Email { get; set; }

    public Guid SchoolId { get; set; }
  }
}

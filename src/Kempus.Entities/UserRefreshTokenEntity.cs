﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Entities
{
  public class UserRefreshTokenEntity
  {
    [Key] public long Id { get; set; }
    public Guid UserId { get; set; }
    [StringLength(100)] public string? RefreshToken { get; set; }
    public DateTime ExpirationTime { get; set; }
    public string? UserIp { get; set; }
    public string? UserAgent { get; set; }
  }
}
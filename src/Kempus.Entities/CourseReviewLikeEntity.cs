﻿using Kempus.Core.Models;
using Kempus.Core.Models.ModelContracts;

namespace Kempus.Entities
{
  public class CourseReviewLikeEntity : Entity<long>, IHasCreatedDate, IMayHaveSchool<Guid>
  {
    public Guid UserId { get; set; }

    public Guid CourseReviewId { get; set; }

    public DateTime CreatedDate { get; set; }

    public Guid SchoolId { get; set; }
  }
}

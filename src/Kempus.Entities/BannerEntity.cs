﻿using Kempus.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Entities
{
  public class BannerEntity : FullAudited<Guid>
  {
    public string? WebUrl { get; set; }
    public string? WebBannerFileName { get; set; }
    public string? MobileUrl { get; set; }
    public string? MobileBannerFileName { get; set; }
    public string? RedirectUrl { get; set; }
    public byte UrlType { get; set; }
    public byte Status { get; set; }
    public Guid SchoolId { get; set; }

  }
}

﻿using Kempus.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace Kempus.Entities
{
  [Index(nameof(Guid))]
  public class PollEntity : FullAudited<Guid>
  {
    public int Type { get; set; }
    public Guid PostId { get; set; }
  }
}
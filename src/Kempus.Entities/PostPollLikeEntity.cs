﻿using Kempus.Core.Models;
using Kempus.Core.Models.ModelContracts;

namespace Kempus.Entities
{
  public class PostPollLikeEntity : Entity<long>, IHasCreatedDate
  {
    public Guid ObjectId { get; set; }

    public byte ObjectType { get; set; }

    public Guid UserId { get; set; }

    public DateTime CreatedDate { get; set; }
  }
}

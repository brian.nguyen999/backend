﻿using System.ComponentModel.DataAnnotations;

namespace Kempus.Entities
{
  public class PostPoll7daysActivityEntity
  {
    [Key]
    public Guid Id { get; set; }

    public Guid ObjectId { get; set; }

    public byte ObjectType { get; set; }

    public byte ActionId { get; set; }

    public Guid SchoolId { get; set; }

    public DateTime CreatedDate { get; set; }
  }
}

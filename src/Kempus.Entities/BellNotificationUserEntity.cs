﻿using Kempus.Core.Models;
using Kempus.Core.Models.ModelContracts;

namespace Kempus.Entities
{
  public class BellNotificationUserEntity : Entity<long>, ISoftDelete
  {
    public Guid BellNotificationId { get; set; }
    public Guid CreatedUser { get; set; }
    public bool IsRead { get; set; }
    public DateTime? ReadDate { get; set; }
    public bool IsDeleted { get; set; }
    public DateTime? DeletedDate { get; set; }
  }
}

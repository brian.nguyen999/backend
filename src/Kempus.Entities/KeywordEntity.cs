﻿using Kempus.Core.Models;
using Kempus.Core.Models.ModelContracts;
using Microsoft.EntityFrameworkCore;

namespace Kempus.Entities
{
  [Index(nameof(Guid), nameof(SchoolId))]
  public class KeywordEntity : Entity<long>, ISoftDelete
  {
    public string Name { get; set; }
    public string NormalizedName { get; set; }
    public bool IsDeleted { get; set; }

    public virtual Guid? SchoolId { get; set; }

  }
}
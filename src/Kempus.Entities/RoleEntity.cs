using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace Kempus.Entities
{
  public class RoleEntity : IdentityRole<long>
  {
    [Key] public override long Id { get; set; }
    public Guid Guid { get; set; } = Guid.NewGuid();
    [StringLength(200, MinimumLength = 1)] public override string Name { get; set; }
    public bool IsActive { get; set; }

    public virtual DateTime CreatedDate { get; set; } = DateTime.UtcNow;
    public virtual Guid CreatedBy { get; set; }
    public virtual bool IsDeleted { get; set; }
    public virtual DateTime? DeletedDate { get; set; }
    public virtual Guid? DeletedBy { get; set; }
    public virtual DateTime? UpdatedDate { get; set; }
    public virtual Guid? UpdatedBy { get; set; }

    public RoleEntity()
    {
    }
  }
}
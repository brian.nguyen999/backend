﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Entities
{
  public class CourseReviewActivitySummaryEntity
  {
    public Guid CourseReviewId { get; set; }

    public Guid SchoolId { get; set; }

    public DateTime CreatedDate { get; set; }

    public long TotalActivitiesIn6Month { get; set; }

    public DateTime? LatestViewOn { get; set; }

    public DateTime? LatestLikeOn { get; set; }
  }
}

﻿using Kempus.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Entities
{
  [Index(nameof(Guid))]
  public class FlagEntity : FullAudited<Guid>
  {
    public Guid ObjectId { get; set; }
    public byte ObjectType { get; set; }
    public string? ContentUrl { get; set; }
    public string Reason { get; set; }
    public byte Status { get; set; }
    public string Description { get; set; }
    public string? AdminDescription { get; set; }
    public Guid SchoolId { get; set; }
    public Guid FlaggedUser { get; set; }
  }
}

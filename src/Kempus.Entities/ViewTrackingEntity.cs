﻿using Kempus.Core.Models;
using Kempus.Core.Models.ModelContracts;

namespace Kempus.Entities
{
  public class ViewTrackingEntity : Entity<long>, IHasCreatedDate
  {
    public Guid UserId { get; set; }

    public Guid? ObjectId { get; set; }

    public string? ObjectType { get; set; }

    public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
  }
}

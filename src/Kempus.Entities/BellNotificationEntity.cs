﻿using Kempus.Core.Models;
using Kempus.Core.Models.ModelContracts;

namespace Kempus.Entities
{
  public class BellNotificationEntity : FullAudited<Guid>, ISoftDelete
  {
    public string Title { get; set; }
    public string? Description { get; set; }
    public string? UrlLink { get; set; }
    public byte? ObjectTypeId { get; set; }
    public Guid? ObjectId { get; set; }
    public bool IsDeleted { get; set; }
    public byte Type { get; set; }
    public byte? Status { get; set; }
    public string? SchoolIds { get; set; }
    public DateTime? ScheduledDate { get; set; }
  }
}

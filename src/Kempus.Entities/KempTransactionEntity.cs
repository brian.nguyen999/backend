﻿using Kempus.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Entities
{
  public class KempTransactionEntity : FullAudited<Guid>
  {
    public byte Type { get; set; }
    public Guid? ReferenceGuid { get; set; }
    public Guid UserId { get; set; }
    public int Amount { get; set; }
    public byte Action { get; set; }
    public Guid SchoolId { get; set; }
    public bool IsAdminCreated { get; set; } = false;
  }
}

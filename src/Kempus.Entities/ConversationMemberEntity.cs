﻿namespace Kempus.Entities
{
  public class ConversationMemberEntity
  {
    public Guid Guid { get; set; } = Guid.NewGuid();
    public Guid ConversationId { get; set; }
    public Guid MemberId { get; set; }
    public byte Status { get; set; }
    public byte? PreviousStatus { get; set; }
    public DateTime? DeletionTime { get; set; }
    public bool IsHasNewMessage { get; set; }
    public DateTime? LastTimeViewConversation { get; set; }
    public DateTime CreatedDate { get; set; }
    public Guid SchoolId { get; set; }
    public DateTime? LatestActivityDateTime { get; set; }
    public DateTime? UpdatedDate { get; set; }
    public Guid? UpdatedBy { get; set; }
  }
}

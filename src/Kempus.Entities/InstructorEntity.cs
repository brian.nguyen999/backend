﻿using Kempus.Core.Models;
using Kempus.Core.Models.ModelContracts;

namespace Kempus.Entities
{
  public class InstructorEntity : FullAudited<Guid>, IHasActive
  {
    public string Name { get; set; }

    public bool IsActivated { get; set; }

    public Guid SchoolId { get; set; }

    public byte GeneratedTypeId { get; set; }
  }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Entities
{
  public class PermissionEntity
  {
    [Key] public int Id { get; set; }
    public string Name { get; set; }
    public int ModuleApplicationId { get; set; }
    public int Order { get; set; }
    public int? ParentId { get; set; }

  }
}
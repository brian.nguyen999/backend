﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kempus.Core.Models;

namespace Kempus.Entities
{
  public class SchoolLifeTimeAnalyticEntity : FullAudited<long>
  {
    public Guid SchoolId { get; set; }

    public int Users { get; set; }

    public long NoOfWaitList { get; set; }

  }
}
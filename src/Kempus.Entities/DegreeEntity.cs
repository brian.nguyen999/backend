﻿using Kempus.Core.Models;

namespace Kempus.Entities
{
  public class DegreeEntity : FullAudited<Guid>
  {
    public string? Name { get; set; }

    public bool IsActivated { get; set; }

    public virtual Guid? SchoolId { get; set; }

  }
}
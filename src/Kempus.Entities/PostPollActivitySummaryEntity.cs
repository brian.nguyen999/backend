﻿using System.ComponentModel.DataAnnotations;

namespace Kempus.Entities
{
  public class PostPollActivitySummaryEntity
  {
    [Key]
    public Guid Id { get; set; }

    public Guid ObjectId { get; set; }

    public byte ObjectType { get; set; }

    public long TotalLikeIn7days { get; set; }

    public long TotalCommentIn7days { get; set; }

    public Guid SchoolId { get; set; }

    public DateTime CreatedDate { get; set; }
  }
}

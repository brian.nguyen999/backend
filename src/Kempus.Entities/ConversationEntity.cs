﻿using Kempus.Core.Models;
using Kempus.Core.Models.ModelContracts;

namespace Kempus.Entities
{
  public class ConversationEntity : Entity<long>, IHasCreatedDate
  {
    public string? Name { get; set; }
    public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
    public Guid CreatedUser { get; set; }
    public byte Type { get; set; }
    public Guid SchoolId { get; set; }
    public Guid? UpdatedBy { get; set; }
    public DateTime? UpdatedDate { get; set; }
  }
}

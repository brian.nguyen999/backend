﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Kempus.Entities
{
  public class ApplicationUser : IdentityUser<long>
  {
    public Guid UserId { get; set; }
    public byte Status { get; set; }
    public DateTime? LastAccessTime { get; set; }
    public int ConsecutiveLogin { get; set; }
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public string? NickName { get; set; }
    public string? ResetPasswordCode { get; set; }
    public string? ReferralCode { get; set; }
    public DateTime? LastChangeNickname { get; set; }
    public Guid? DegreeId { get; set; }
    public DateTime? LastChangedDegree { get; set; }
    public bool IsHiddenDegree { get; set; }
    public Guid? MajorId { get; set; }
    public bool IsHiddenMajor { get; set; }
    public DateTime? LastChangedMajor { get; set; }
    public int? ClassYear { get; set; }
    public bool? IsHiddenClassYear { get; set; }
    public DateTime? LastChangeClassYear { get; set; }
    public Guid? UserWalletId { get; set; }
    public string? OtpCode { get; set; }
    public DateTime? OtpCreatedTime { get; set; }
    public Guid SchoolId { get; set; }
    public int FailedLoginCount { get; set; }

    public virtual DateTime CreatedDate { get; set; }
    public virtual Guid? CreatedBy { get; set; }
    public virtual bool IsDeleted { get; set; }
    public virtual DateTime? DeletedDate { get; set; }
    public virtual Guid? DeletedBy { get; set; }
    public virtual DateTime? UpdatedDate { get; set; }
    public virtual Guid? UpdatedBy { get; set; }

    public byte UserType { get; set; }
    public string? InviterReferralCode { get; set; }
    public Guid? InviterGuid { get; set; }
    public string? AdminMemory { get; set; }
    public string? LastLoginIP { get; set; }
  }
}
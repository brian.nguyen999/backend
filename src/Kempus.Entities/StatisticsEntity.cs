﻿using Kempus.Core.Models;

namespace Kempus.Entities
{
  public class StatisticsEntity : Entity<long>
  {
    public Guid CreatedUser { get; set; }
    public Guid ObjectId { get; set; }
    public byte ObjectType { get; set; }
    public long NumberOfView { get; set; }
    public long NumberOfLike { get; set; }
    public long NumberOfComment { get; set; }
    public long NumberOfVote { get; set; }
  }
}

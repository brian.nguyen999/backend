﻿using Kempus.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace Kempus.Entities
{
  [Index(nameof(Guid), nameof(PostId), nameof(KeywordId))]
  public class PostKeywordEntity : Entity<long>
  {
    public virtual Guid PostId { get; set; }

    public virtual Guid KeywordId { get; set; }
  }
}
﻿using Kempus.Core.Models;
using Kempus.Core.Models.ModelContracts;

namespace Kempus.Entities
{
  public class VoteUserEntity : Entity<long>, IHasCreatedDate
  {
    public Guid CreatedUser { get; set; }
    public Guid PollId { get; set; }
    public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
  }
}

﻿using Kempus.Core.Models.ModelContracts;

namespace Kempus.Entities
{
  public class BookmarkEntity : IHasCreatedDate
  {
    public Guid Guid { get; set; }

    public Guid UserId { get; set; }

    public Guid ObjectId { get; set; }

    public byte ObjectType { get; set; }

    public DateTime CreatedDate { get; set; }
  }
}

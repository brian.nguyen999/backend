﻿using Kempus.Core.Models;
using Kempus.Core.Models.ModelContracts;

namespace Kempus.Entities
{
  public class CourseEntity : FullAudited<Guid>, IHasActive
  {
    public string? Name { get; set; }

    public bool IsActivated { get; set; }

    public Guid? MajorId { get; set; }

    public Guid? DegreeId { get; set; }

    public Guid? SchoolId { get; set; }

    public Guid? DepartmentId { get; set; }

    public Guid? FileUploadTrackingId { get; set; }

    public long CsvRowIndex { get; set; }

    public byte GeneratedTypeId { get; set; }
    public string? IdImported { get; set; }
  }
}
using Kempus.Core.Models;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace Kempus.Entities
{
  [Index(nameof(Guid), nameof(TopicId), nameof(SchoolId))]
  public class PostEntity : FullAudited<Guid>
  {
    public string Title { get; set; }

    [StringLength(10000)] 
    public string Content { get; set; }

    public string Keywords { get; set; }

    public byte Type { get; set; }

    public bool IsPublished { get; set; } = true;

    public virtual Guid? SchoolId { get; set; }

    public virtual Guid? TopicId { get; set; }

    public bool IsPinned { get; set; }

    public DateTime? PinnedDate { get; set; }
  }
}
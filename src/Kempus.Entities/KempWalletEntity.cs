﻿using Kempus.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Entities
{
  public class KempWalletEntity : FullAudited<Guid>
  {
    public Guid UserId { get; set; }
    public long Amount { get; set; }
  }
}

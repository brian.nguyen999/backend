﻿using Kempus.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Entities
{
  [Index(nameof(Guid))]
  public class EmailTemplateEntity : Entity<long>
  {
    public string Title { get; set; }
    public string Content { get; set; }
    public string Type { get; set; }
  }
}
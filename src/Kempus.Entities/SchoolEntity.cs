﻿using Kempus.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace Kempus.Entities
{
  [Index(nameof(Guid))]
  public class SchoolEntity : FullAudited<Guid>
  {
    public string Name { get; set; }
    public bool IsActivated { get; set; }
    public bool IsRequiredReferralCode { get; set; }
    public string PrimaryColor { get; set; }
    public string SecondaryColor { get; set; }
    public string EmailDomain { get; set; }
    public DateTime? LastUpdateStatusDateTime { get; set; }
    public bool IsShowHomePage { get; set; }
  }
}
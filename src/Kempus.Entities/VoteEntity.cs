﻿using Kempus.Core.Models;

namespace Kempus.Entities
{
  public class VoteEntity : Entity<long>
  {
    public Guid VoteUserId { get; set; }

    public Guid PollOptionId { get; set; }
  }
}

﻿using Kempus.Core.Models;

namespace Kempus.Entities
{
  public class CourseInstructorEntity : FullAudited<Guid>
  {
    public virtual Guid CourseId { get; set; }

    public virtual Guid InstructorId { get; set; }
  }
}
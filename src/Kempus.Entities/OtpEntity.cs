﻿using Kempus.Core.Models;
using Kempus.Core.Models.ModelContracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Entities
{
  [Index(nameof(Guid))]
  public class OtpEntity : Entity<long>, ISoftDelete, IHasCreatedDate
  {
    public string OtpCode { get; set; }
    public string ObjectId { get; set; }
    public byte ObjectType { get; set; }
    public DateTime? ExpirationTime { get; set; }
    public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
    public bool IsDeleted { get; set; }
  }
}
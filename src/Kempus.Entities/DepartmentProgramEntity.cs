﻿using Kempus.Core.Models;

namespace Kempus.Entities
{
  public class DepartmentProgramEntity : FullAudited<Guid>
  {
    public string Name { get; set; }
    public byte Status { get; set; }
    public Guid SchoolId { get; set; }
    public byte GeneratedTypeId { get; set; }
  }
}

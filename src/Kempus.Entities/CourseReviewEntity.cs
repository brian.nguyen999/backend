﻿using Kempus.Core.Models;

namespace Kempus.Entities
{
  public class CourseReviewEntity : FullAudited<Guid>
  {
    // Question 1: Is the course text book required for this course?
    public bool IsRequiredTextBook { get; set; }

    // Question 2: How difficult will you rate this course?
    public byte DifficultRating { get; set; }

    // Question 3: On average, how many hours per week do you spend on this course (excluding attending classes)
    public double HoursSpend { get; set; }

    // Question 4: Number of exams (e.g. mid / final terms)
    public byte NumberOfExams { get; set; }

    // Question 5: Is group projects or presentation required for this course?
    public bool IsRequiredGroupProject { get; set; }

    // Question 6: Is the grading criteria clear?
    public byte GradingCriteriaRating { get; set; }

    // Question 7: What was your grade?
    public byte UserGrade { get; set; }

    // Question 8: Did you achieve the goals as stated on the course syllabus?
    public byte UserAchievementRating { get; set; }

    // Question 9: What are some characteristics of the instructor?
    public string? Characteristics { get; set; }

    // Question 10: Would you recommend this course to others?
    public byte RecommendRating { get; set; }

    // Question 11: What do you want other students to know about this course?
    public string? Comment { get; set; }

    public virtual Guid? SchoolId { get; set; }

    public virtual Guid? CourseId { get; set; }

    public Guid? UserId { get; set; }

    public byte Status { get; set; }
  }
}
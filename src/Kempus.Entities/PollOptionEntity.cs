﻿using Kempus.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace Kempus.Entities
{
  [Index(nameof(Guid))]
  public class PollOptionEntity : FullAudited<Guid>
  {
    public Guid PollId { get; set; }

    public string Content { get; set; }
  }
}
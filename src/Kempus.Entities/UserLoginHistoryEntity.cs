﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Entities
{
  public class UserLoginHistoryEntity
  {
    [Key]
    public Guid Guid { get; set; } = Guid.NewGuid();
    public Guid UserId { get; set; }
    public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
    public string? IP { get; set; }
    public string? Device { get; set; }
  }
}

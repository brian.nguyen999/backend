﻿using Kempus.Core.Models;
using Kempus.Core.Models.ModelContracts;
using Microsoft.EntityFrameworkCore;

namespace Kempus.Entities
{
  [Index(nameof(Guid), nameof(SchoolId))]
  public class TopicEntity : FullAudited<Guid>, IHasActive, ISoftDelete
  {
    public string Name { get; set; }
    public bool IsActivated { get; set; }
    public bool IsDeleted { get; set; }

    public virtual Guid SchoolId { get; set; }
  }
}
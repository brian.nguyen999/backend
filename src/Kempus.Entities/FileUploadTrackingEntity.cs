﻿using Kempus.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kempus.Entities
{
  public class FileUploadTrackingEntity : FullAudited<Guid>
  {
    public Guid SchoolId { get; set; }

    public string FileName { get; set; }

    public string Type { get; set; }

    public string FileUrl { get; set; }

    public bool IsUsing { get; set; }

    public int Height { get; set; }

    public int Width { get; set; }

    public long FileSize { get; set; }
  }
}

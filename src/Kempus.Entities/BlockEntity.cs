﻿using Kempus.Core.Models.ModelContracts;

namespace Kempus.Entities
{
  public class BlockEntity : ISoftDelete
  {
    public Guid Guid { get; set; }
    public Guid ConversationId { get; set; }
    public Guid BlockedUser { get; set; }
    public Guid Blocker { get; set; }
    public virtual bool IsDeleted { get; set; }
    public virtual DateTime? DeletedDate { get; set; }
    public virtual Guid? DeletedBy { get; set; }
    public Guid CreatedBy { get; set; }
    public DateTime CreatedDate { get; set; }
  }
}

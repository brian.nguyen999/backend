﻿using Kempus.Core.Models;

namespace Kempus.Entities
{
  public class CourseSummaryEntity : Entity<long>
  {
    public Guid CourseId { get; set; }
    public int TotalReviews { get; set; }
    public bool IsTextbookRequired { get; set; }
    public byte NumberOfExams { get; set; }
    public bool IsGroupProjects { get; set; }
    public double RecommendedRate { get; set; }
    public double GradingCriteriaRate { get; set; }
    public double CourseDifficultyRate { get; set; }
    public string TopThreeCharacteristicInstructors { get; set; }
    public string StudentGradePercents { get; set; }
    public double HoursPerWeekRate { get; set; }
    public double GoalsAchivedRate { get; set; }
  }
}
